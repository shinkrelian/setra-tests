﻿Imports ComSEToursBE2
Imports ComSEToursBL2
Public Class frmMantCotizacionVuelosVentas

    Public vintID As String
    Public vintIDCAB As Integer
    Public vstrCotizacion As String
    Public vstrVuelo As String
    Public FormRefCotizacion As frmIngCotizacionDato
    Public strCoTicket As String = ""
    Public bytNuSegmento As Byte = 0

    Public strModoEdicion As Char
    Dim blnNuevoEnabled As Boolean
    Dim blnGrabarEnabled As Boolean
    Dim blnDeshacerEnabled As Boolean
    Dim blnEditarEnabled As Boolean = False
    Dim blnEliminarEnabled As Boolean = False
    Dim blnSalirEnabled As Boolean
    Dim blnImprimirEnabled As Boolean = False
    Dim blnLoad As Boolean = False
    Dim blnCambios As Boolean = False

    Dim blnGrabarEnabledBD As Boolean = False
    Dim blnEditarEnabledBD As Boolean = False
    Dim blnEliminarEnabledBD As Boolean = False
    Public blnDgvFilaVacia As Boolean
    Public blnSoloConsulta As Boolean = False

    Public vstrTipoTransfer As Char ' V,B,T
    Public strIDProveedor As String = ""
    Public FormRefReserv As Object 'frmIngReservasControlporFileDato
    Dim dtprv As New DataTable
    Dim strCoLineaAereaSel As String = ""
    Dim blnUpdExcelPRail As Boolean = False
    Dim vdouCostoAnt As Double = 0  ''PPMG20151209
    Public strModulo As String = "" ''PPMG20151209
    Public blnEsGuia As Boolean = False ''PPMG20160420
    Dim dcValoresIniciales As New Dictionary(Of String, String)

    Dim frmmain As Object = Nothing

    Private Sub frmMantCotizacionVuelosDato_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        frmMain.DesabledBtns()
        'ActivarDesactivarBotones()
    End Sub

    Private Sub frmMantCotizacionVuelosDato_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        Select Case e.KeyCode
            Case Keys.F6
                If btnAceptar.Enabled Then pGrabar()
            Case Keys.Escape
                pSalir(Nothing, Nothing)
        End Select
    End Sub

    Private Sub frmMantUsuarios_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Activated
        'AddHandler frmMain.btnSalir.Click, AddressOf pSalir
        'frmMain.btnSalir.Enabled = blnSalirEnabled
        ''AddHandler frmMain.btnNuevo.Click, AddressOf pNuevo
        'frmMain.btnNuevo.Enabled = blnNuevoEnabled
        ''AddHandler frmMain.btnEliminar.Click, AddressOf pEliminar
        'frmMain.btnEliminar.Enabled = blnEliminarEnabled
        ''AddHandler frmMain.btnEditar.Click, AddressOf pEditar
        'frmMain.btnEditar.Enabled = blnEditarEnabled
        ''AddHandler frmMain.btnImprimir.Click, AddressOf pImprimir
        'frmMain.btnImprimir.Enabled = blnImprimirEnabled

        'frmMain.btnDeshacer.Enabled = blnDeshacerEnabled
        'frmMain.btnGrabar.Enabled = blnGrabarEnabled
        'blnLoad = True


    End Sub

    Private Sub frmMantCotizacionVuelosVentas_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try

            'MessageBox.Show(FormRefReserv.DgvDetalle.Rows.Count.ToString)

            CargarCombos()
            CargarConsultar(blnSoloConsulta)
            If vintID = "0" Or vintID Is Nothing Then
                'CargarValorID()
                ModoNuevo()
            Else
                ModoEditar()
                CargarControles()
            End If

            BloquearControlesPorIngTickets()
            dcValoresIniciales.Add("txtVuelo", txtVuelo.Text.Trim)
            dcValoresIniciales.Add("txtRuta", txtRuta.Text.Trim)
            dcValoresIniciales.Add("dtpDia", dtpDia.Value.ToShortDateString)
        Catch ex As Exception
            MessageBox.Show(ex.Message, ex.Source, MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub BloquearControlesPorIngTickets()
        Try
            If strCoTicket <> "" And bytNuSegmento <> 0 Then
                cboRutaO.Enabled = False
                cboRutaD.Enabled = False
            End If
        Catch ex As Exception
            Throw
        End Try
    End Sub

    Private Sub CargarControles()
        ''PPMG20151209-->
        Me.MostrarMotivo(False)
        chkMotivo.Checked = False
        txtDescMotivo.Text = ""
        ''PPMG20151209<--
        If Not FormRefCotizacion Is Nothing Then
            Select Case vstrTipoTransfer
                Case "V"
                    lblProv.Text = "Linea Aerea"
                    lblProveedor.Visible = False
                    btnProveedor.Visible = False
                    cboLineaP.Visible = True
                    cboLineaP.Location = New Point(87, 15)
                    dtpDia.Enabled = True
                    lblNroVueloEtiq.Text = "Vuelo Nro."
                    With FormRefCotizacion.dgvVuelos
                        dtpDia.Value = .Item("DiaV", .CurrentRow.Index).Value
                        txtRuta.Text = If(.Item("RutaVuelo", .CurrentRow.Index).Value Is Nothing, "", .Item("RutaVuelo", .CurrentRow.Index).Value.ToString)
                        'If Not .Item("IdLineaA", .CurrentRow.Index).Value Is Nothing Then

                        If Not .Item("DescripcionV", .CurrentRow.Index).Value Is Nothing Then
                            txtServicio.Text = .Item("DescripcionV", .CurrentRow.Index).Value.ToString
                        Else
                            txtServicio.Text = ""
                        End If

                        If Not .Item("RutaD", .CurrentRow.Index).Value Is Nothing Then
                            cboRutaD.SelectedValue = .Item("RutaD", .CurrentRow.Index).Value.ToString
                        End If
                        If Not .Item("RutaO", .CurrentRow.Index).Value Is Nothing Then
                            cboRutaO.SelectedValue = .Item("RutaO", .CurrentRow.Index).Value.ToString
                        End If
                        cboRutaO_SelectionChangeCommitted(Nothing, Nothing)

                        If Not .Item("Vuelo", .CurrentRow.Index).Value Is Nothing Then
                            txtVuelo.Text = .Item("Vuelo", .CurrentRow.Index).Value.ToString
                        Else
                            txtVuelo.Text = ""
                        End If

                        If Not .Item("Salida", .CurrentRow.Index).Value Is Nothing Then
                            dtpSalida.Text = .Item("Salida", .CurrentRow.Index).Value
                        Else
                            dtpSalida.Text = "00:00:00"
                        End If
                        If Not .Item("Llegada", .CurrentRow.Index).Value Is Nothing Then
                            dtpLlegada.Text = .Item("Llegada", .CurrentRow.Index).Value.ToString.Substring(0, 5)
                            If .Item("Llegada", .CurrentRow.Index).Value.ToString.Trim.Length > 5 Then
                                nudMasLlegada.Value = Replace(Mid(.Item("Llegada", .CurrentRow.Index).Value.ToString.Trim, 6), ":", "")
                            End If
                        Else
                            dtpLlegada.Text = "00:00:00"
                        End If
                        If Not .Item("Tarifa", .CurrentRow.Index).Value Is Nothing Then
                            txtTarifa.Text = .Item("Tarifa", .CurrentRow.Index).Value
                        Else
                            txtTarifa.Text = ""
                        End If
                        If Not .Item("CodReserva", .CurrentRow.Index).Value Is Nothing Then
                            txtCodReserva.Text = .Item("CodReserva", .CurrentRow.Index).Value.ToString
                        Else
                            txtCodReserva.Text = ""
                        End If

                        If Not .Item("EmitidoSetoursV", .CurrentRow.Index).Value Is Nothing Then
                            chkEmitidoSetours.Checked = .Item("EmitidoSetoursV", .CurrentRow.Index).Value
                        Else
                            chkEmitidoSetours.Checked = False
                        End If
                        CargarCombosLinAereas()
                        If Not .Item("IdLineaA", .CurrentRow.Index).Value Is Nothing Then
                            'cboLineaP.SelectedValue = .Item("IdLineaA", .CurrentRow.Index).Value.ToString
                            cboLineaP.SelectedValue = .Item("IdLineaA", .CurrentRow.Index).Value.ToString
                        End If

                        If Not .Item("EmitirOperadorV", .CurrentRow.Index).Value Is Nothing Then
                            chkEmitidoOperador.Checked = .Item("EmitirOperadorV", .CurrentRow.Index).Value
                        Else
                            chkEmitidoOperador.Checked = False
                        End If
                        'chkEmitidoOperador_CheckedChanged(Nothing, Nothing)
                        If Not .Item("IDProveedorOperV", .CurrentRow.Index).Value Is Nothing Then
                            cboOperador.SelectedValue = If(.Item("IDProveedorOperV", .CurrentRow.Index).Value Is Nothing, "", .Item("IDProveedorOperV", .CurrentRow.Index).Value.ToString)
                        End If
                        txtCostosOperador.Text = If(.Item("CostoOperadorV", .CurrentRow.Index).Value Is Nothing, "", .Item("CostoOperadorV", .CurrentRow.Index).Value.ToString)


                    End With
                Case "B"
                    ''OcultarEmitidoOperador()
                    lblNroVueloEtiq.Text = "Bus Nro."
                    lblTarifaVentasEtiq.Visible = False
                    txtTarifa.Visible = False

                    With FormRefCotizacion.dgvBuss
                        dtpDia.Value = .Item("DiaB", .CurrentRow.Index).Value
                        txtRuta.Text = If(.Item("RutaB", .CurrentRow.Index).Value Is Nothing, "", .Item("RutaB", .CurrentRow.Index).Value.ToString)
                        If Not .Item("IdLineaAB", .CurrentRow.Index).Value Is Nothing Then
                            strIDProveedor = .Item("IdLineaAB", .CurrentRow.Index).Value.ToString
                        End If
                        If Not .Item("DescProveedorB", .CurrentRow.Index).Value Is Nothing Then
                            'cboLineaP.SelectedValue = .Item("IdLineaAB", .CurrentRow.Index).Value.ToString
                            lblProveedor.Text = .Item("DescProveedorB", .CurrentRow.Index).Value.ToString
                        End If
                        If Not .Item("ServicioB", .CurrentRow.Index).Value Is Nothing Then
                            txtServicio.Text = .Item("ServicioB", .CurrentRow.Index).Value.ToString
                        Else
                            txtServicio.Text = ""
                        End If

                        If Not .Item("RutaDB", .CurrentRow.Index).Value Is Nothing Then
                            cboRutaD.SelectedValue = .Item("RutaDB", .CurrentRow.Index).Value.ToString
                        End If
                        If Not .Item("RutaOB", .CurrentRow.Index).Value Is Nothing Then
                            cboRutaO.SelectedValue = .Item("RutaOB", .CurrentRow.Index).Value.ToString
                        End If
                        cboRutaO_SelectionChangeCommitted(Nothing, Nothing)
                        If Not .Item("VueloB", .CurrentRow.Index).Value Is Nothing Then
                            txtVuelo.Text = .Item("VueloB", .CurrentRow.Index).Value.ToString
                        Else
                            txtVuelo.Text = ""
                        End If
                        'If Not .Item("Fec_SalidaB", .CurrentRow.Index).Value Is Nothing Then
                        '    dtpFecSalida.Value = .Item("Fec_SalidaB", .CurrentRow.Index).Value
                        'Else
                        '    dtpFecSalida.Value = "01/01/1900"
                        'End If
                        'If Not .Item("Fec_RetornoB", .CurrentRow.Index).Value Is Nothing Then
                        '    DtpFecRetorno.Value = .Item("Fec_RetornoB", .CurrentRow.Index).Value
                        'Else
                        '    DtpFecRetorno.Value = "01/01/1900"
                        'End If
                        If Not .Item("SalidaB", .CurrentRow.Index).Value Is Nothing Then
                            dtpSalida.Text = .Item("SalidaB", .CurrentRow.Index).Value
                        Else
                            dtpSalida.Text = "00:00:00"
                        End If
                        If Not .Item("LlegadaB", .CurrentRow.Index).Value Is Nothing Then
                            dtpLlegada.Text = .Item("LlegadaB", .CurrentRow.Index).Value.ToString.Substring(0, 5)
                            If .Item("LlegadaB", .CurrentRow.Index).Value.ToString.Trim.Length > 5 Then
                                nudMasLlegada.Value = Replace(Mid(.Item("LlegadaB", .CurrentRow.Index).Value.ToString.Trim, 6), ":", "")
                            End If
                        Else
                            dtpLlegada.Text = "00:00:00"
                        End If
                        If Not .Item("TarifaB", .CurrentRow.Index).Value Is Nothing Then
                            txtTarifa.Text = .Item("TarifaB", .CurrentRow.Index).Value
                        Else
                            txtTarifa.Text = ""
                        End If
                        If Not .Item("CodReservaB", .CurrentRow.Index).Value Is Nothing Then
                            txtCodReserva.Text = .Item("CodReservaB", .CurrentRow.Index).Value.ToString
                        Else
                            txtCodReserva.Text = ""
                        End If
                        'If Not .Item("HrRecojoB", .CurrentRow.Index).Value Is Nothing Then
                        '    dtpHrRecojo.Text = .Item("HrRecojoB", .CurrentRow.Index).Value
                        'Else
                        '    dtpHrRecojo.Text = "00:00:00"
                        'End If

                        If Not .Item("EmitidoSetoursB", .CurrentRow.Index).Value Is Nothing Then
                            chkEmitidoSetours.Checked = .Item("EmitidoSetoursB", .CurrentRow.Index).Value
                        Else
                            chkEmitidoSetours.Checked = False
                        End If
                        CargarCombosLinAereas()
                        txtCostosOperador.Text = If(.Item("CostoOperadorB", .CurrentRow.Index).Value Is Nothing, "", .Item("CostoOperadorB", .CurrentRow.Index).Value.ToString)
                    End With
                Case "T"
                    ''OcultarEmitidoOperador()
                    lblNroVueloEtiq.Text = "Tren Nro."
                    lblTarifaVentasEtiq.Visible = False
                    txtTarifa.Visible = False

                    With FormRefCotizacion.dgvTren
                        dtpDia.Value = .Item("DiaT", .CurrentRow.Index).Value
                        txtRuta.Text = If(.Item("RutaT", .CurrentRow.Index).Value Is Nothing, "", .Item("RutaT", .CurrentRow.Index).Value.ToString)
                        If Not .Item("IdLineaAT", .CurrentRow.Index).Value Is Nothing Then
                            strIDProveedor = .Item("IdLineaAT", .CurrentRow.Index).Value.ToString
                        End If
                        If Not .Item("DescProveedorT", .CurrentRow.Index).Value Is Nothing Then
                            'cboLineaP.SelectedValue = .Item("IdLineaAT", .CurrentRow.Index).Value.ToString
                            lblProveedor.Text = .Item("DescProveedorT", .CurrentRow.Index).Value.ToString
                        End If
                        If Not .Item("ServicioTren", .CurrentRow.Index).Value Is Nothing Then
                            txtServicio.Text = .Item("ServicioTren", .CurrentRow.Index).Value.ToString
                        Else
                            txtServicio.Text = ""
                        End If
                        If Not .Item("RutaDT", .CurrentRow.Index).Value Is Nothing Then
                            cboRutaD.SelectedValue = .Item("RutaDT", .CurrentRow.Index).Value.ToString
                        End If
                        If Not .Item("RutaOT", .CurrentRow.Index).Value Is Nothing Then
                            cboRutaO.SelectedValue = .Item("RutaOT", .CurrentRow.Index).Value.ToString
                        End If
                        cboRutaO_SelectionChangeCommitted(Nothing, Nothing)
                        If Not .Item("VueloT", .CurrentRow.Index).Value Is Nothing Then
                            txtVuelo.Text = .Item("VueloT", .CurrentRow.Index).Value.ToString
                        Else
                            txtVuelo.Text = ""
                        End If
                        'If Not .Item("Fec_SalidaT", .CurrentRow.Index).Value Is Nothing Then
                        '    dtpFecSalida.Value = .Item("Fec_SalidaT", .CurrentRow.Index).Value
                        'Else
                        '    dtpFecSalida.Value = "01/01/1900"
                        'End If
                        'If Not .Item("Fec_RetornoT", .CurrentRow.Index).Value Is Nothing Then
                        '    DtpFecRetorno.Value = .Item("Fec_RetornoT", .CurrentRow.Index).Value
                        'Else
                        '    DtpFecRetorno.Value = "01/01/1900"
                        'End If
                        If Not .Item("SalidaT", .CurrentRow.Index).Value Is Nothing Then
                            dtpSalida.Text = .Item("SalidaT", .CurrentRow.Index).Value
                        Else
                            dtpSalida.Text = "00:00:00"
                        End If
                        If Not .Item("LlegadaT", .CurrentRow.Index).Value Is Nothing Then
                            dtpLlegada.Text = .Item("LlegadaT", .CurrentRow.Index).Value.ToString.Substring(0, 5)
                            If .Item("LlegadaT", .CurrentRow.Index).Value.ToString.Trim.Length > 5 Then
                                nudMasLlegada.Value = Replace(Mid(.Item("LlegadaT", .CurrentRow.Index).Value.ToString.Trim, 6), ":", "")
                            End If
                        Else
                            dtpLlegada.Text = "00:00:00"
                        End If
                        If Not .Item("TarifaT", .CurrentRow.Index).Value Is Nothing Then
                            txtTarifa.Text = .Item("TarifaT", .CurrentRow.Index).Value
                        Else
                            txtTarifa.Text = ""
                        End If
                        If Not .Item("CodReservaT", .CurrentRow.Index).Value Is Nothing Then
                            txtCodReserva.Text = .Item("CodReservaT", .CurrentRow.Index).Value.ToString
                        Else
                            txtCodReserva.Text = ""
                        End If
                        'If Not .Item("HrRecojoT", .CurrentRow.Index).Value Is Nothing Then
                        '    dtpHrRecojo.Text = .Item("HrRecojoT", .CurrentRow.Index).Value
                        'Else
                        '    dtpHrRecojo.Text = "00:00:00"
                        'End If
                        If Not .Item("EmitidoSetoursT", .CurrentRow.Index).Value Is Nothing Then
                            chkEmitidoSetours.Checked = .Item("EmitidoSetoursT", .CurrentRow.Index).Value
                        Else
                            chkEmitidoSetours.Checked = False
                        End If
                        txtCostosOperador.Text = If(.Item("CostoOperadorT", .CurrentRow.Index).Value Is Nothing, "", .Item("CostoOperadorT", .CurrentRow.Index).Value.ToString)
                        blnUpdExcelPRail = .Item("FlUpdExcelPRailT", .CurrentRow.Index).Value
                        If blnUpdExcelPRail Then
                            txtCostosOperador.Enabled = False
                            ErrPrvCosto.SetError(txtCostosOperador, "Costo Bloqueado: Ha sido actualizado importando el archivo excel de PeruRail.")
                        End If
                        CargarCombosLinAereas()
                        'cboIdaVuelta.SelectedValue = .Item("IdaVuelta", .CurrentRow.Index).Value.ToString
                    End With
            End Select
        ElseIf FormRefReserv IsNot Nothing Then
            Select Case vstrTipoTransfer
                Case "V"
                    lblProv.Text = "Linea Aerea"
                    lblProveedor.Visible = False
                    btnProveedor.Visible = False
                    cboLineaP.Visible = True
                    cboLineaP.Location = New Point(87, 15)
                    dtpDia.Enabled = True
                    lblNroVueloEtiq.Text = "Vuelo Nro."
                    With FormRefReserv.dgvVuelos
                        dtpDia.Value = .Item("DiaV", .CurrentRow.Index).Value
                        txtRuta.Text = If(.Item("RutaVuelo", .CurrentRow.Index).Value Is Nothing, "", .Item("RutaVuelo", .CurrentRow.Index).Value.ToString)


                        If Not .Item("DescripcionV", .CurrentRow.Index).Value Is Nothing Then
                            txtServicio.Text = .Item("DescripcionV", .CurrentRow.Index).Value.ToString
                        Else
                            txtServicio.Text = ""
                        End If

                        If Not .Item("RutaD", .CurrentRow.Index).Value Is Nothing Then
                            cboRutaD.SelectedValue = .Item("RutaD", .CurrentRow.Index).Value.ToString
                        End If
                        If Not .Item("RutaO", .CurrentRow.Index).Value Is Nothing Then
                            cboRutaO.SelectedValue = .Item("RutaO", .CurrentRow.Index).Value.ToString
                        End If
                        cboRutaO_SelectionChangeCommitted(Nothing, Nothing)

                        If Not .Item("Vuelo", .CurrentRow.Index).Value Is Nothing Then
                            txtVuelo.Text = .Item("Vuelo", .CurrentRow.Index).Value.ToString
                        Else
                            txtVuelo.Text = ""
                        End If

                        If Not .Item("Salida", .CurrentRow.Index).Value Is Nothing Then
                            dtpSalida.Text = .Item("Salida", .CurrentRow.Index).Value
                        Else
                            dtpSalida.Text = "00:00:00"
                        End If
                        If Not .Item("Llegada", .CurrentRow.Index).Value Is Nothing Then
                            dtpLlegada.Text = .Item("Llegada", .CurrentRow.Index).Value.ToString.Substring(0, 5)
                            If .Item("Llegada", .CurrentRow.Index).Value.ToString.Trim.Length > 5 Then
                                nudMasLlegada.Value = Replace(Mid(.Item("Llegada", .CurrentRow.Index).Value.ToString.Trim, 6), ":", "")
                            End If
                        Else
                            dtpLlegada.Text = "00:00:00"
                        End If
                        If Not .Item("Tarifa", .CurrentRow.Index).Value Is Nothing Then
                            txtTarifa.Text = .Item("Tarifa", .CurrentRow.Index).Value
                        Else
                            txtTarifa.Text = ""
                        End If
                        If Not .Item("CodReserva", .CurrentRow.Index).Value Is Nothing Then
                            txtCodReserva.Text = .Item("CodReserva", .CurrentRow.Index).Value.ToString
                        Else
                            txtCodReserva.Text = ""
                        End If

                        If Not .Item("EmitidoSetoursV", .CurrentRow.Index).Value Is Nothing Then
                            chkEmitidoSetours.Checked = .Item("EmitidoSetoursV", .CurrentRow.Index).Value
                        Else
                            chkEmitidoSetours.Checked = False
                        End If
                        CargarCombosLinAereas()

                        If Not .Item("IdLineaA", .CurrentRow.Index).Value Is Nothing Then
                            'cboLineaP.SelectedValue = .Item("IdLineaA", .CurrentRow.Index).Value.ToString
                            cboLineaP.SelectedValue = .Item("IdLineaA", .CurrentRow.Index).Value.ToString
                        End If

                        If Not .Item("EmitidoOperadorV", .CurrentRow.Index).Value Is Nothing Then
                            chkEmitidoOperador.Checked = .Item("EmitidoOperadorV", .CurrentRow.Index).Value
                        Else
                            chkEmitidoOperador.Checked = False
                        End If

                        If Not .Item("IDProveedorOperadorV", .CurrentRow.Index).Value Is Nothing Then
                            cboOperador.SelectedValue = If(.Item("IDProveedorOperadorV", .CurrentRow.Index).Value Is Nothing, "", .Item("IDProveedorOperadorV", .CurrentRow.Index).Value.ToString)
                        End If
                        txtCostosOperador.Text = If(.Item("CostoOperadorV", .CurrentRow.Index).Value Is Nothing, "", .Item("CostoOperadorV", .CurrentRow.Index).Value.ToString)

                    End With
                Case "B"
                    ''OcultarEmitidoOperador()
                    lblNroVueloEtiq.Text = "Bus Nro."
                    lblTarifaVentasEtiq.Visible = False
                    txtTarifa.Visible = False

                    With FormRefReserv.dgvBuss
                        dtpDia.Value = .Item("DiaB", .CurrentRow.Index).Value
                        txtRuta.Text = If(.Item("RutaB", .CurrentRow.Index).Value Is Nothing, "", .Item("RutaB", .CurrentRow.Index).Value.ToString)
                        If Not .Item("IdLineaAB", .CurrentRow.Index).Value Is Nothing Then
                            strIDProveedor = .Item("IdLineaAB", .CurrentRow.Index).Value.ToString
                        End If
                        If Not .Item("DescProveedorB", .CurrentRow.Index).Value Is Nothing Then
                            'cboLineaP.SelectedValue = .Item("IdLineaAB", .CurrentRow.Index).Value.ToString
                            lblProveedor.Text = .Item("DescProveedorB", .CurrentRow.Index).Value.ToString
                        End If
                        If Not .Item("ServicioB", .CurrentRow.Index).Value Is Nothing Then
                            txtServicio.Text = .Item("ServicioB", .CurrentRow.Index).Value.ToString
                        Else
                            txtServicio.Text = ""
                        End If

                        If Not .Item("RutaDB", .CurrentRow.Index).Value Is Nothing Then
                            cboRutaD.SelectedValue = .Item("RutaDB", .CurrentRow.Index).Value.ToString
                        End If
                        If Not .Item("RutaOB", .CurrentRow.Index).Value Is Nothing Then
                            cboRutaO.SelectedValue = .Item("RutaOB", .CurrentRow.Index).Value.ToString
                        End If
                        cboRutaO_SelectionChangeCommitted(Nothing, Nothing)
                        If Not .Item("VueloB", .CurrentRow.Index).Value Is Nothing Then
                            txtVuelo.Text = .Item("VueloB", .CurrentRow.Index).Value.ToString
                        Else
                            txtVuelo.Text = ""
                        End If
                        'If Not .Item("Fec_SalidaB", .CurrentRow.Index).Value Is Nothing Then
                        '    dtpFecSalida.Value = .Item("Fec_SalidaB", .CurrentRow.Index).Value
                        'Else
                        '    dtpFecSalida.Value = "01/01/1900"
                        'End If
                        'If Not .Item("Fec_RetornoB", .CurrentRow.Index).Value Is Nothing Then
                        '    DtpFecRetorno.Value = .Item("Fec_RetornoB", .CurrentRow.Index).Value
                        'Else
                        '    DtpFecRetorno.Value = "01/01/1900"
                        'End If
                        If Not .Item("SalidaB", .CurrentRow.Index).Value Is Nothing Then
                            dtpSalida.Text = .Item("SalidaB", .CurrentRow.Index).Value
                        Else
                            dtpSalida.Text = "00:00:00"
                        End If
                        If Not .Item("LlegadaB", .CurrentRow.Index).Value Is Nothing Then
                            dtpLlegada.Text = .Item("LlegadaB", .CurrentRow.Index).Value.ToString.Substring(0, 5)
                            If .Item("LlegadaB", .CurrentRow.Index).Value.ToString.Trim.Length > 5 Then
                                nudMasLlegada.Value = Replace(Mid(.Item("LlegadaB", .CurrentRow.Index).Value.ToString.Trim, 6), ":", "")
                            End If
                        Else
                            dtpLlegada.Text = "00:00:00"
                        End If
                        If Not .Item("TarifaB", .CurrentRow.Index).Value Is Nothing Then
                            txtTarifa.Text = .Item("TarifaB", .CurrentRow.Index).Value
                        Else
                            txtTarifa.Text = ""
                        End If
                        If Not .Item("CodReservaB", .CurrentRow.Index).Value Is Nothing Then
                            txtCodReserva.Text = .Item("CodReservaB", .CurrentRow.Index).Value.ToString
                        Else
                            txtCodReserva.Text = ""
                        End If
                        'If Not .Item("HrRecojoB", .CurrentRow.Index).Value Is Nothing Then
                        '    dtpHrRecojo.Text = .Item("HrRecojoB", .CurrentRow.Index).Value
                        'Else
                        '    dtpHrRecojo.Text = "00:00:00"
                        'End If

                        If Not .Item("EmitidoSetoursB", .CurrentRow.Index).Value Is Nothing Then
                            chkEmitidoSetours.Checked = .Item("EmitidoSetoursB", .CurrentRow.Index).Value
                        Else
                            chkEmitidoSetours.Checked = False
                        End If
                        CargarCombosLinAereas()
                        txtCostosOperador.Text = If(.Item("CostoOperadorB", .CurrentRow.Index).Value Is Nothing, "", .Item("CostoOperadorB", .CurrentRow.Index).Value.ToString)
                    End With
                Case "T"
                    ''OcultarEmitidoOperador()
                    lblNroVueloEtiq.Text = "Tren Nro."
                    lblTarifaVentasEtiq.Visible = False
                    txtTarifa.Visible = False
                    With FormRefReserv.dgvTren
                        dtpDia.Value = .Item("DiaT", .CurrentRow.Index).Value
                        txtRuta.Text = If(.Item("RutaT", .CurrentRow.Index).Value Is Nothing, "", .Item("RutaT", .CurrentRow.Index).Value.ToString)
                        If Not .Item("IdLineaAT", .CurrentRow.Index).Value Is Nothing Then
                            strIDProveedor = .Item("IdLineaAT", .CurrentRow.Index).Value.ToString
                        End If
                        If Not .Item("DescProveedorT", .CurrentRow.Index).Value Is Nothing Then
                            'cboLineaP.SelectedValue = .Item("IdLineaAT", .CurrentRow.Index).Value.ToString
                            lblProveedor.Text = .Item("DescProveedorT", .CurrentRow.Index).Value.ToString
                        End If
                        If Not .Item("ServicioTren", .CurrentRow.Index).Value Is Nothing Then
                            txtServicio.Text = .Item("ServicioTren", .CurrentRow.Index).Value.ToString
                        Else
                            txtServicio.Text = ""
                        End If
                        If Not .Item("RutaDT", .CurrentRow.Index).Value Is Nothing Then
                            cboRutaD.SelectedValue = .Item("RutaDT", .CurrentRow.Index).Value.ToString
                        End If
                        If Not .Item("RutaOT", .CurrentRow.Index).Value Is Nothing Then
                            cboRutaO.SelectedValue = .Item("RutaOT", .CurrentRow.Index).Value.ToString
                        End If
                        cboRutaO_SelectionChangeCommitted(Nothing, Nothing)
                        If Not .Item("VueloT", .CurrentRow.Index).Value Is Nothing Then
                            txtVuelo.Text = .Item("VueloT", .CurrentRow.Index).Value.ToString
                        Else
                            txtVuelo.Text = ""
                        End If
                        'If Not .Item("Fec_SalidaT", .CurrentRow.Index).Value Is Nothing Then
                        '    dtpFecSalida.Value = .Item("Fec_SalidaT", .CurrentRow.Index).Value
                        'Else
                        '    dtpFecSalida.Value = "01/01/1900"
                        'End If
                        'If Not .Item("Fec_RetornoT", .CurrentRow.Index).Value Is Nothing Then
                        '    DtpFecRetorno.Value = .Item("Fec_RetornoT", .CurrentRow.Index).Value
                        'Else
                        '    DtpFecRetorno.Value = "01/01/1900"
                        'End If
                        If Not .Item("SalidaT", .CurrentRow.Index).Value Is Nothing Then
                            dtpSalida.Text = .Item("SalidaT", .CurrentRow.Index).Value
                        Else
                            dtpSalida.Text = "00:00:00"
                        End If
                        If Not .Item("LlegadaT", .CurrentRow.Index).Value Is Nothing Then
                            dtpLlegada.Text = .Item("LlegadaT", .CurrentRow.Index).Value.ToString.Substring(0, 5)
                            If .Item("LlegadaT", .CurrentRow.Index).Value.ToString.Trim.Length > 5 Then
                                nudMasLlegada.Value = Replace(Mid(.Item("LlegadaT", .CurrentRow.Index).Value.ToString.Trim, 6), ":", "")
                            End If
                        Else
                            dtpLlegada.Text = "00:00:00"
                        End If
                        If Not .Item("TarifaT", .CurrentRow.Index).Value Is Nothing Then
                            txtTarifa.Text = .Item("TarifaT", .CurrentRow.Index).Value
                        Else
                            txtTarifa.Text = ""
                        End If
                        If Not .Item("CodReservaT", .CurrentRow.Index).Value Is Nothing Then
                            txtCodReserva.Text = .Item("CodReservaT", .CurrentRow.Index).Value.ToString
                        Else
                            txtCodReserva.Text = ""
                        End If
                        'If Not .Item("HrRecojoT", .CurrentRow.Index).Value Is Nothing Then
                        '    dtpHrRecojo.Text = .Item("HrRecojoT", .CurrentRow.Index).Value
                        'Else
                        '    dtpHrRecojo.Text = "00:00:00"
                        'End If
                        If Not .Item("EmitidoSetoursT", .CurrentRow.Index).Value Is Nothing Then
                            chkEmitidoSetours.Checked = .Item("EmitidoSetoursT", .CurrentRow.Index).Value
                        Else
                            chkEmitidoSetours.Checked = False
                        End If
                        txtCostosOperador.Text = If(.Item("CostoOperadorT", .CurrentRow.Index).Value Is Nothing, "", .Item("CostoOperadorT", .CurrentRow.Index).Value.ToString)
                        blnUpdExcelPRail = .Item("FlUpdExcelPRailT", .CurrentRow.Index).Value
                        If blnUpdExcelPRail Then
                            ''PPMG20151209-->
                            If strModulo = "R" Then
                                Me.MostrarMotivo(True)
                                If .Item("FlMotivoT", .CurrentRow.Index).Value Is Nothing Then
                                    chkMotivo.Checked = False
                                    txtDescMotivo.Text = ""
                                    txtDescMotivo.Enabled = False
                                Else
                                    chkMotivo.Checked = .Item("FlMotivoT", .CurrentRow.Index).Value
                                    If Not .Item("DescMotivoT", .CurrentRow.Index).Value Is Nothing Then
                                        txtDescMotivo.Text = .Item("DescMotivoT", .CurrentRow.Index).Value.ToString
                                    Else
                                        txtDescMotivo.Text = ""
                                    End If
                                End If
                                If chkMotivo.Checked Then
                                    txtCostosOperador.Enabled = True
                                    ErrPrvCosto.SetError(txtCostosOperador, "")
                                Else
                                    txtCostosOperador.Enabled = False
                                    ErrPrvCosto.SetError(txtCostosOperador, "Costo Bloqueado: Ha sido actualizado importando el archivo excel de PeruRail.")
                                End If
                                ''vdouCostoAnt = Double.Parse(If(Not IsNumeric(txtCostosOperador.Text), 0, txtCostosOperador.Text))
                                vdouCostoAnt = douMontoOriginal()
                            Else
                                ''PPMG20151209<--
                                txtCostosOperador.Enabled = False
                                ErrPrvCosto.SetError(txtCostosOperador, "Costo Bloqueado: Ha sido actualizado importando el archivo excel de PeruRail.")
                            End If
                        End If
                        CargarCombosLinAereas()
                        'cboIdaVuelta.SelectedValue = .Item("IdaVuelta", .CurrentRow.Index).Value.ToString
                    End With
                    If blnEsGuia = True And strModulo = "R" Then    ''PPMG20160420
                        dtpDia.Enabled = True
                    End If
            End Select
        End If
        If cboLineaP.Text <> "" Then
            strCoLineaAereaSel = cboLineaP.SelectedValue.ToString
        End If


        If chkEmitidoOperador.Checked = False Then
            cboOperador.Enabled = False
            txtCostosOperador.Enabled = False
            If chkEmitidoSetours.Checked Then
                If Not blnUpdExcelPRail Then
                    txtCostosOperador.Enabled = True
                End If
            End If
        End If
        ''PPMG20151209-->
        If chkMotivo.Visible Then
            If chkMotivo.Checked Then
                txtCostosOperador.Enabled = True
                ErrPrvCosto.SetError(txtCostosOperador, "")
            Else
                If blnUpdExcelPRail Then
                    txtCostosOperador.Enabled = False
                    ErrPrvCosto.SetError(txtCostosOperador, "Costo Bloqueado: Ha sido actualizado importando el archivo excel de PeruRail.")
                End If
            End If
        End If
        ''PPMG20151209<--
    End Sub

    ''PPMG20151209-->
    Private Sub MostrarMotivo(ByVal pbVisible As Boolean)
        chkMotivo.Visible = pbVisible
        txtDescMotivo.Visible = pbVisible
        If pbVisible Then
            grbOperador.Height += 60
            gpGeneral.Height += 50
            Me.Height += 50
        End If
    End Sub
    ''PPMG20151209<--

    Private Sub OcultarEmitidoOperador()
        Me.Size = New Size(607, 250)
        chkEmitidoOperador.Visible = False
        grbOperador.Visible = False
    End Sub

    'Private Sub CargarValorID()
    '    If FormRefCotizacion Is Nothing Then Exit Sub
    '    Dim obj As New clsCotizacionBN.clsCotizacionVuelosBN
    '    Dim dtGeneral As DataTable = obj.ConsultarList(0, 0, "", "", "", "", "", "", "")
    '    Dim dt As DataTable = obj.ConsultarList(0, vintIDCAB, FormRefCotizacion.strCotizacion, "", "", "", "", "", "")
    '    If dt.Rows.Count = 0 And FormRefCotizacion.dgvVuelos.Rows.Count = 0 Then
    '        vintID = CInt(dtGeneral.Compute("MAX(ID)", "")) + 1
    '    Else
    '        If FormRefCotizacion.dgvVuelos.Rows.Count >= 1 Then
    '            vintID = CInt(FormRefCotizacion.dgvVuelos.Item("ID", FormRefCotizacion.dgvVuelos.Rows.Count - 1).Value) + 1
    '        End If
    '    End If
    'End Sub

    Private Sub ModoConsulta()
        strModoEdicion = "C"
        'Colocar Titulo para Reconocimiento de TItulo


        blnNuevoEnabled = True
        blnDeshacerEnabled = False
        blnGrabarEnabled = False
        blnSalirEnabled = True
        ActivarDesactivarBotones()
        pEnableControls(Me, False)
    End Sub

    Private Sub ModoNuevo()
        strModoEdicion = "N"


        Select Case vstrTipoTransfer
            Case "T"
                Me.Text = "Nuevo Tren" : lblTitulo.Text = "Nuevo Tren"
                lblNroVueloEtiq.Text = "Tren Nro."
                lblTarifaVentasEtiq.Visible = False
                txtTarifa.Visible = False
                'lblIdaVuelta.Enabled = True : cboIdaVuelta.Enabled = True
            Case "B"
                Me.Text = "Nuevo Bus" : lblTitulo.Text = "Nuevo Bus"
                lblNroVueloEtiq.Text = "Bus Nro."
                lblTarifaVentasEtiq.Visible = False
                txtTarifa.Visible = False
                'lblIdaVuelta.Enabled = False : cboIdaVuelta.Enabled = False
            Case "V"
                Me.Text = "Nuevo Vuelo" : lblTitulo.Text = "Nuevo Vuelo"
                lblNroVueloEtiq.Text = "Vuelo Nro."
                dtpDia.Enabled = True
                'lblIdaVuelta.Enabled = False : cboIdaVuelta.Enabled = False
        End Select

        'dtpFecSalida.Value = FormRefCotizacion.dtpFecInicio.Value
        'DtpFecRetorno.Value = FormRefCotizacion.dtpFechaOut.Value
        dtpLlegada.Text = "00:00"
        dtpSalida.Text = "00:00"
        'dtpHrRecojo.Text = "00:00"

        blnNuevoEnabled = False
        blnDeshacerEnabled = True
        blnGrabarEnabled = True
        blnEditarEnabled = False
        blnEliminarEnabled = False
        blnSalirEnabled = False
        ActivarDesactivarBotones()

        pEnableControls(Me, True)

        lblProv.Text = "Linea Aerea"
        lblProveedor.Visible = False
        btnProveedor.Visible = False

        cboLineaP.Location = New Point(87, 15)


        txtRuta.Focus()
    End Sub
    Private Sub CargarCombosLinAereas()
        
        Try
            If chkEmitidoSetours.Checked Then
                Dim obj As New clsTicketBN
                pCargaCombosBox(cboLineaP, obj.ConsultarCboLineas(False, "", False), False)
            Else
                Dim obj As New clsCotizacionBN.clsCotizacionVuelosBN
                pCargaCombosBox(cboLineaP, obj.ConsultarCboLineas(False, "015"), False)
            End If
            If strCoLineaAereaSel <> "" Then
                cboLineaP.SelectedValue = strCoLineaAereaSel
                cboLineaP_SelectionChangeCommitted(Nothing, Nothing)
            End If
        Catch ex As Exception
            Throw
        End Try
    End Sub
    Private Sub CargarCombos()

        Try
            'Dim dttIdentificador As New DataTable("TipoTransfer")
            'With dttIdentificador
            '    .Columns.Add("Codigo")
            '    .Columns.Add("Desriptivo")
            '    .Rows.Add("", "-------")
            '    .Rows.Add("B", "BUSS")
            '    .Rows.Add("T", "TREN")
            '    .Rows.Add("V", "VUELO")
            'End With

            'pCargaCombosBox(cboIdentificador, dttIdentificador, True)

            'Dim objUb As New clsTablasApoyoBN.clsUbigeoBN
            'With cboRutaO
            '    .DataSource = objUb.ConsultarCboconCodigo("CIUDAD", "", True)
            '    .DisplayMember = "Descripcion"
            '    .ValueMember = "IDUbigeo"
            'End With
            'With cboRutaD
            '    .DataSource = objUb.ConsultarCboconCodigo("CIUDAD", "", True)
            '    .DisplayMember = "Descripcion"
            '    .ValueMember = "IDUbigeo"
            'End With

            Dim objUbi As New clsTablasApoyoBN.clsUbigeoBN
            Dim dttOri As DataTable = objUbi.ConsultarCboconCodigo("CIUDAD", "", True)
            'With cboRutaO
            '    .DataSource = dttOri
            '    .DisplayMember = "Descripcion"
            '    .ValueMember = "IDUbigeo"
            'End With
            pCargaCombosBox(cboRutaO, dttOri)

            Dim dttDes As DataTable = dttOri.Clone
            For i As Int32 = 0 To dttOri.Rows.Count - 1
                dttDes.ImportRow(dttOri.Rows(i))
            Next

            pCargaCombosBox(cboRutaD, dttDes)


            'Dim obj As New clsCotizacionBN.clsCotizacionVuelosBN
            'Dim dt As New DataTable

            'dt = obj.ConsultarCboLineas(False, "015")
            'pCargaCombosBox(cboLineaP, dt, False)
            CargarCombosLinAereas()

            Dim objPrv As New clsProveedorBN
            dtprv = objPrv.ConsultarCbo(gstrTipoProveeOperadores)
            pCargaCombosBox(cboOperador, dtprv, False)

            'Dim dttIdaVuelta As New DataTable("IdaVuelta")
            'With dttIdaVuelta
            '    .Columns.Add("Codigo")
            '    .Columns.Add("Descriptivo")

            '    .Rows.Add("I", "IDA")
            '    .Rows.Add("V", "VUELTA")
            'End With
            'pCargaCombosBox(cboIdaVuelta, dttIdaVuelta)
        Catch ex As Exception
            MessageBox.Show(ex.Message, ex.Source, MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

   
    Private Sub cboRutaO_SelectionChangeCommitted(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboRutaO.SelectionChangeCommitted, _
                                                                                    cboRutaD.SelectionChangeCommitted
        'Dim obju As New clsTablasApoyoBN.clsUbigeoBN
        Try
            'If cboRutaO.SelectedIndex <> 0 And cboRutaO.SelectedIndex <> -1 Then
            '    Dim dt As DataTable = obju.ConsultarCboconCodigo("", "", False)
            '    For i As Integer = 0 To dt.Rows.Count - 1
            '        If dt(i)("IDUbigeo").ToString = cboRutaO.SelectedValue Then
            '            txtRuta.Text = dt(i)("Codigo").ToString
            '            Exit For
            '        End If
            '    Next
            '    ErrPrv.SetError(cboRutaO, "")
            'End If
            'If cboRutaD.SelectedIndex <> 0 And cboRutaD.SelectedIndex <> -1 Then
            '    Dim dt As DataTable = obju.ConsultarCboconCodigo("", "", False)
            '    For i As Integer = 0 To dt.Rows.Count - 1
            '        If dt(i)("IDUbigeo").ToString = cboRutaD.SelectedValue Then
            '            txtRuta.Text = dt(i)("Codigo").ToString
            '            Exit For
            '        End If
            '    Next
            '    ErrPrv.SetError(cboRutaD, "")
            'End If
            'If cboRutaO.SelectedIndex <> 0 And cboRutaO.SelectedIndex <> -1 And _
            '        cboRutaD.SelectedIndex <> 0 And cboRutaD.SelectedIndex <> -1 Then
            '    Dim dt As DataTable = obju.ConsultarCboconCodigo("", "", False)
            '    Dim strRuta As String = ""
            '    For i As Integer = 0 To dt.Rows.Count - 1
            '        If dt(i)("IDUbigeo").ToString = cboRutaO.SelectedValue Then
            '            strRuta += dt(i)("Codigo").ToString
            '            Exit For
            '        End If
            '    Next
            '    For i As Integer = 0 To dt.Rows.Count - 1
            '        If dt(i)("IDUbigeo").ToString = cboRutaD.SelectedValue Then
            '            strRuta += " / " & dt(i)("Codigo").ToString
            '            Exit For
            '        End If
            '    Next
            '    txtRuta.Text = strRuta
            'End If
            'If cboRutaO.SelectedIndex = 0 And cboRutaD.SelectedIndex = 0 Then
            '    txtRuta.Text = ""
            'End If

            Dim strRutaOrigen As String = cboRutaO.Text
            Dim strRutaDestino As String = cboRutaD.Text
            Dim strRutaOAbrev As String = ""
            Dim strRutaDAbrev As String = ""

            'If strRutaOrigen <> "" And strRutaOrigen <> "-------" Then
            '    If strRutaOrigen.Substring(strRutaOrigen.Length - 2, 2) <> "()" Then
            '        strRutaOAbrev = Mid(strRutaOrigen, InStr(strRutaOrigen, "("))
            '        strRutaOAbrev = Replace(strRutaOAbrev, "(", "")
            '        strRutaOAbrev = Replace(strRutaOAbrev, ")", "")
            '    Else
            '        strRutaOAbrev = ""
            '    End If
            'End If
            strRutaOAbrev = strDevuelveCadenaOcultaDerecha(strRutaOrigen)
            'If strRutaDestino <> "" And strRutaDestino <> "-------" Then
            '    If strRutaDestino.Substring(strRutaDestino.Length - 2, 2) <> "()" Then
            '        strRutaDAbrev = Mid(strRutaDestino, InStr(strRutaDestino, "("))
            '        strRutaDAbrev = Replace(strRutaDAbrev, "(", "")
            '        strRutaDAbrev = Replace(strRutaDAbrev, ")", "")
            '    Else
            '        strRutaDAbrev = ""
            '    End If
            'End If
            strRutaDAbrev = strDevuelveCadenaOcultaDerecha(strRutaDestino)
            txtRuta.Text = strRutaOAbrev & " / " & strRutaDAbrev
        Catch ex As Exception
            Throw
        End Try
    End Sub

    Private Sub ModoEditar()
        If vstrVuelo <> "" Then
            Select Case vstrTipoTransfer
                Case "V" : Me.Text = "Edición de Vuelo Nro. " & vstrVuelo : lblTitulo.Text = "Edición de Vuelo Nro. " & vstrVuelo
                Case "B" : Me.Text = "Edición de Bus Nro. " & vstrVuelo : lblTitulo.Text = "Edición de Bus Nro. " & vstrVuelo
                Case "T" : Me.Text = "Edición de Tren Nro. " & vstrVuelo : lblTitulo.Text = "Edición de Tren Nro. " & vstrVuelo
            End Select

        Else
            Select Case vstrTipoTransfer
                Case "V" : Me.Text = "Edición de Vuelo" & vstrVuelo : lblTitulo.Text = "Edición de Vuelo" & vstrVuelo
                Case "B" : Me.Text = "Edición de Bus" & vstrVuelo : lblTitulo.Text = "Edición de Bus" & vstrVuelo
                Case "T" : Me.Text = "Edición de Tren" & vstrVuelo : lblTitulo.Text = "Edición de Tren" & vstrVuelo
            End Select

        End If

        If FormRefCotizacion IsNot Nothing Then
            dtpDia.Enabled = True
        End If


        strModoEdicion = "E"

        blnNuevoEnabled = False
        blnDeshacerEnabled = True
        blnGrabarEnabled = True
        blnSalirEnabled = False
        ActivarDesactivarBotones()

        pEnableControls(Me, True)

        cboLineaP.Visible = False

        txtRuta.Focus()
    End Sub

    Private Sub ActivarDesactivarBotones()

        'frmMain.btnNuevo.Enabled = blnNuevoEnabled
        If blnNuevoEnabled = True Then
            If blnGrabarEnabledBD Then
                frmMain.btnNuevo.Enabled = blnNuevoEnabled
            Else
                frmMain.btnNuevo.Enabled = False
                blnNuevoEnabled = blnGrabarEnabledBD
            End If
        Else
            frmMain.btnNuevo.Enabled = blnNuevoEnabled
        End If

        'frmMain.btnGrabar.Enabled = blnGrabarEnabled
        If blnGrabarEnabled = True Then
            If blnGrabarEnabledBD Then
                frmMain.btnGrabar.Enabled = blnGrabarEnabled
            Else
                frmMain.btnGrabar.Enabled = False
                blnGrabarEnabled = blnGrabarEnabledBD
            End If
        Else
            frmMain.btnGrabar.Enabled = blnGrabarEnabled
        End If

        frmMain.btnDeshacer.Enabled = blnDeshacerEnabled

        'frmMain.btnEditar.Enabled = blnEditarEnabled
        If blnEditarEnabled = True Then
            If blnEditarEnabledBD Then
                frmMain.btnEditar.Enabled = blnEditarEnabled
            Else
                frmMain.btnEditar.Enabled = False
                blnEditarEnabled = blnEditarEnabledBD
            End If
        Else
            frmMain.btnEditar.Enabled = blnEditarEnabled
        End If

        'frmMain.btnEliminar.Enabled = blnEliminarEnabled
        If blnEliminarEnabled = True Then
            If blnEliminarEnabledBD Then
                frmMain.btnEliminar.Enabled = blnEliminarEnabled
            Else
                frmMain.btnEliminar.Enabled = False
                blnEliminarEnabled = blnEliminarEnabledBD
            End If
        Else
            frmMain.btnEliminar.Enabled = blnEliminarEnabled
        End If

        frmMain.btnSalir.Enabled = blnSalirEnabled

    End Sub


    Private Sub pGrabar()
        If Not blnValidarIngresos() Then Exit Sub
        Try


            If strModoEdicion = "N" Then
 
                Grabar()
                If Not FormRefReserv Is Nothing Then
                    FormRefReserv.blnAgregoVuelo = True
                End If

                'MessageBox.Show("Vuelo insertado exitosamente", gstrTituloMsg, MessageBoxButtons.OK, MessageBoxIcon.Information)

            ElseIf strModoEdicion = "E" Then

                Actualizar()

                'Select Case vstrTipoTransfer
                '    Case "V" : MessageBox.Show("Vuelo Actualizado exitosamente", gstrTituloMsg, MessageBoxButtons.OK, MessageBoxIcon.Information)
                '    Case "T" : MessageBox.Show("Tren Actualizado exitosamente", gstrTituloMsg, MessageBoxButtons.OK, MessageBoxIcon.Information)
                '    Case "B" : MessageBox.Show("Bus Actuzalizado exitosamente", gstrTituloMsg, MessageBoxButtons.OK, MessageBoxIcon.Information)
                'End Select
            Else

            End If

            If Not FormRefCotizacion Is Nothing Then
                If txtRuta.Text.Trim <> dcValoresIniciales("txtRuta") Or txtVuelo.Text.Trim <> dcValoresIniciales("txtVuelo") Or dtpDia.Value.ToShortDateString <> dcValoresIniciales("dtpDia") Then
                    FormRefCotizacion.blnActualizoTransporte = True
                End If
            ElseIf Not FormRefReserv Is Nothing Then
                FormRefReserv.blnActualizoTransporte = True
            End If

            blnCambios = False
            Me.Close()

        Catch ex As Exception
            Throw
        End Try
    End Sub

    Private Sub Actualizar()
        Try
            If Not FormRefCotizacion Is Nothing Then
                Select Case vstrTipoTransfer
                    Case "V"
                        With FormRefCotizacion.dgvVuelos
                            Dim byFilaDgv As Byte = .CurrentRow.Index
                            '.Item("ID", byFilaDgv).Value = vintID
                            .Item("DiaV", byFilaDgv).Value = dtpDia.Text
                            .Item("IDCAB", byFilaDgv).Value = FormRefCotizacion.intIDCab
                            .Item("CotizacionVuelo", byFilaDgv).Value = FormRefCotizacion.strCotizacion
                            .Item("RutaVuelo", byFilaDgv).Value = txtRuta.Text.Trim
                            .Item("OrigenVuelo", byFilaDgv).Value = strDevuelveCadenaNoOcultaIzquierda(cboRutaO.Text.Trim)
                            .Item("Destino", byFilaDgv).Value = strDevuelveCadenaNoOcultaIzquierda(cboRutaD.Text.Trim)
                            .Item("RutaD", byFilaDgv).Value = cboRutaD.SelectedValue.ToString
                            .Item("RutaO", byFilaDgv).Value = cboRutaO.SelectedValue.ToString

                            .Item("RutaDAbrV", byFilaDgv).Value = strDevuelveCadenaOcultaDerecha(cboRutaD.Text)
                            .Item("RutaOAbrV", byFilaDgv).Value = strDevuelveCadenaOcultaDerecha(cboRutaO.Text)

                            .Item("IdLineaA", byFilaDgv).Value = cboLineaP.SelectedValue.ToString
                            .Item("DescProveedorV", byFilaDgv).Value = strDevuelveCadenaNoOcultaIzquierda(cboLineaP.Text)

                            .Item("DescripcionV", byFilaDgv).Value = txtServicio.Text

                            .Item("Vuelo", byFilaDgv).Value = txtVuelo.Text.Trim
                            '.Item("FechaSalida", byFilaDgv).Value = CDate(dtpFecSalida.Value).ToShortDateString
                            '.Item("FechaSalida", byFilaDgv).Value = "01/01/1900"
                            .Item("FechaSalida", byFilaDgv).Value = dtpDia.Text
                            '.Item("FechaRetorno", byFilaDgv).Value = CDate(DtpFecRetorno.Value).ToShortDateString
                            .Item("FechaRetorno", byFilaDgv).Value = "01/01/1900"
                            .Item("Salida", byFilaDgv).Value = dtpSalida.Text 'CDate(dtpSalida.Value.ToString).ToShortTimeString
                            '.Item("HrRecojo", byFilaDgv).Value = dtpHrRecojo.Text 'CDate(dtpHrRecojo.Value.ToString).ToShortTimeString
                            .Item("HrRecojo", byFilaDgv).Value = ""
                            If nudMasLlegada.Value = 0 Then
                                .Item("Llegada", byFilaDgv).Value = dtpLlegada.Text
                            Else
                                .Item("Llegada", byFilaDgv).Value = dtpLlegada.Text & "+" & nudMasLlegada.Value.ToString
                            End If


                            .Item("TipoTransporteV", byFilaDgv).Value = vstrTipoTransfer
                            .Item("CodReserva", byFilaDgv).Value = txtCodReserva.Text.Trim
                            .Item("Tarifa", byFilaDgv).Value = txtTarifa.Text.Trim
                            .Item("EmitidoSetoursV", byFilaDgv).Value = chkEmitidoSetours.Checked
                            .Item("EmitirOperadorV", byFilaDgv).Value = chkEmitidoOperador.Checked
                            .Item("IDProveedorOperV", byFilaDgv).Value = If(cboOperador.SelectedValue Is Nothing, "", cboOperador.SelectedValue.ToString)
                            .Item("CostoOperadorV", byFilaDgv).Value = If(txtCostosOperador.Text.Trim = "", "0.00", txtCostosOperador.Text.Trim)
                        End With
                    Case "B"
                        With FormRefCotizacion.dgvBuss
                            Dim byFilaDgv As Byte = .CurrentRow.Index
                            '.Item("IDB", byFilaDgv).Value = vintID
                            .Item("DiaB", byFilaDgv).Value = dtpDia.Text
                            .Item("IDCABB", byFilaDgv).Value = FormRefCotizacion.intIDCab
                            .Item("CotizacionB", byFilaDgv).Value = FormRefCotizacion.strCotizacion
                            .Item("RutaB", byFilaDgv).Value = txtRuta.Text.Trim
                            .Item("OrigenB", byFilaDgv).Value = strDevuelveCadenaNoOcultaIzquierda(cboRutaO.Text.Trim)
                            .Item("DestinoB", byFilaDgv).Value = strDevuelveCadenaNoOcultaIzquierda(cboRutaD.Text.Trim)
                            .Item("RutaDB", byFilaDgv).Value = cboRutaD.SelectedValue.ToString
                            .Item("RutaOB", byFilaDgv).Value = cboRutaO.SelectedValue.ToString

                            .Item("RutaDAbrB", byFilaDgv).Value = strDevuelveCadenaOcultaDerecha(cboRutaD.Text)
                            .Item("RutaOAbrB", byFilaDgv).Value = strDevuelveCadenaOcultaDerecha(cboRutaO.Text)

                            '.Item("IdLineaAB", byFilaDgv).Value = cboLineaP.SelectedValue.ToString
                            '.Item("DescProveedorB", byFilaDgv).Value = cboLineaP.Text
                            .Item("IdLineaAB", byFilaDgv).Value = strIDProveedor
                            .Item("DescProveedorB", byFilaDgv).Value = lblProveedor.Text

                            .Item("ServicioB", byFilaDgv).Value = txtServicio.Text

                            .Item("VueloB", byFilaDgv).Value = txtVuelo.Text.Trim
                            '.Item("Fec_SalidaB", byFilaDgv).Value = CDate(dtpFecSalida.Value).ToShortDateString
                            .Item("Fec_SalidaB", byFilaDgv).Value = dtpDia.Text
                            '.Item("Fec_RetornoB", byFilaDgv).Value = CDate(DtpFecRetorno.Value).ToShortDateString
                            .Item("Fec_RetornoB", byFilaDgv).Value = "01/01/1900"
                            .Item("SalidaB", byFilaDgv).Value = dtpSalida.Text 'CDate(dtpSalida.Value.ToString).ToShortTimeString
                            '.Item("HrRecojoB", byFilaDgv).Value = dtpHrRecojo.Text 'CDate(dtpHrRecojo.Value.ToString).ToShortTimeString
                            .Item("HrRecojoB", byFilaDgv).Value = ""
                            '.Item("LlegadaB", byFilaDgv).Value = dtpLlegada.Text & "+" & nudMasLlegada.Value.ToString
                            If nudMasLlegada.Value = 0 Then
                                .Item("LlegadaB", byFilaDgv).Value = dtpLlegada.Text
                            Else
                                .Item("LlegadaB", byFilaDgv).Value = dtpLlegada.Text & "+" & nudMasLlegada.Value.ToString
                            End If

                            .Item("TipoTransporteB", byFilaDgv).Value = vstrTipoTransfer
                            .Item("CodReservaB", byFilaDgv).Value = txtCodReserva.Text.Trim
                            .Item("TarifaB", byFilaDgv).Value = txtTarifa.Text.Trim
                            .Item("EmitidoSetoursB", byFilaDgv).Value = chkEmitidoSetours.Checked
                            .Item("CostoOperadorB", byFilaDgv).Value = If(txtCostosOperador.Text.Trim = "", "0.00", txtCostosOperador.Text.Trim)
                        End With
                    Case "T"
                        With FormRefCotizacion.dgvTren
                            Dim byFilaDgv As Byte = .CurrentRow.Index
                            '.Item("IDT", byFilaDgv).Value = vintID
                            .Item("DiaT", byFilaDgv).Value = dtpDia.Text
                            .Item("IDCABT", byFilaDgv).Value = FormRefCotizacion.intIDCab
                            .Item("CotizacionT", byFilaDgv).Value = FormRefCotizacion.strCotizacion
                            .Item("RutaT", byFilaDgv).Value = txtRuta.Text.Trim
                            .Item("OrigenT", byFilaDgv).Value = strDevuelveCadenaNoOcultaIzquierda(cboRutaO.Text.Trim)
                            .Item("DestinoT", byFilaDgv).Value = strDevuelveCadenaNoOcultaIzquierda(cboRutaD.Text.Trim)
                            .Item("RutaDT", byFilaDgv).Value = cboRutaD.SelectedValue.ToString
                            .Item("RutaOT", byFilaDgv).Value = cboRutaO.SelectedValue.ToString

                            .Item("RutaDAbrT", byFilaDgv).Value = strDevuelveCadenaOcultaDerecha(cboRutaD.Text)
                            .Item("RutaOAbrT", byFilaDgv).Value = strDevuelveCadenaOcultaDerecha(cboRutaO.Text)

                            '.Item("IdLineaAT", byFilaDgv).Value = cboLineaP.SelectedValue.ToString
                            '.Item("DescProveedorT", byFilaDgv).Value = cboLineaP.Text
                            .Item("IdLineaAT", byFilaDgv).Value = strIDProveedor
                            .Item("DescProveedorT", byFilaDgv).Value = lblProveedor.Text

                            .Item("ServicioTren", byFilaDgv).Value = txtServicio.Text

                            .Item("VueloT", byFilaDgv).Value = txtVuelo.Text.Trim
                            '.Item("Fec_SalidaT", byFilaDgv).Value = CDate(dtpFecSalida.Value).ToShortDateString
                            .Item("Fec_SalidaT", byFilaDgv).Value = dtpDia.Text
                            '.Item("Fec_RetornoT", byFilaDgv).Value = CDate(DtpFecRetorno.Value).ToShortDateString
                            .Item("Fec_RetornoT", byFilaDgv).Value = "01/01/1900"
                            .Item("SalidaT", byFilaDgv).Value = dtpSalida.Text 'CDate(dtpSalida.Value.ToString).ToShortTimeString
                            '.Item("HrRecojoT", byFilaDgv).Value = dtpHrRecojo.Text 'CDate(dtpHrRecojo.Value.ToString).ToShortTimeString
                            .Item("HrRecojoT", byFilaDgv).Value = ""
                            '.Item("LlegadaT", byFilaDgv).Value = dtpLlegada.Text & "+" & nudMasLlegada.Value.ToString
                            If nudMasLlegada.Value = 0 Then
                                .Item("LlegadaT", byFilaDgv).Value = dtpLlegada.Text
                            Else
                                .Item("LlegadaT", byFilaDgv).Value = dtpLlegada.Text & "+" & nudMasLlegada.Value.ToString
                            End If

                            .Item("TipoTransporteT", byFilaDgv).Value = vstrTipoTransfer
                            .Item("CodReservaT", byFilaDgv).Value = txtCodReserva.Text.Trim
                            .Item("TarifaT", byFilaDgv).Value = txtTarifa.Text.Trim
                            .Item("EmitidoSetoursT", byFilaDgv).Value = chkEmitidoSetours.Checked
                            .Item("CostoOperadorT", byFilaDgv).Value = If(txtCostosOperador.Text.Trim = "", "0.00", txtCostosOperador.Text.Trim)
                            '.Item("IdaVuelta", byFilaDgv).Value = cboIdaVuelta.SelectedValue.ToString
                        End With
                End Select
            ElseIf Not FormRefReserv Is Nothing Then
                Select Case vstrTipoTransfer
                    Case "V"
                        With FormRefReserv.dgvVuelos
                            Dim byFilaDgv As Byte = .CurrentRow.Index
                            '.Item("ID", byFilaDgv).Value = vintID
                            .Item("DiaV", byFilaDgv).Value = dtpDia.Text
                            .Item("IDCAB", byFilaDgv).Value = FormRefReserv.intIDCab
                            .Item("CotizacionVuelo", byFilaDgv).Value = FormRefReserv.lblCotizacion.Text
                            .Item("RutaVuelo", byFilaDgv).Value = txtRuta.Text.Trim
                            .Item("OrigenVuelo", byFilaDgv).Value = strDevuelveCadenaNoOcultaIzquierda(cboRutaO.Text.Trim)
                            .Item("Destino", byFilaDgv).Value = strDevuelveCadenaNoOcultaIzquierda(cboRutaD.Text.Trim)
                            .Item("RutaD", byFilaDgv).Value = cboRutaD.SelectedValue.ToString
                            .Item("RutaO", byFilaDgv).Value = cboRutaO.SelectedValue.ToString

                            .Item("RutaDAbrV", byFilaDgv).Value = strDevuelveCadenaOcultaDerecha(cboRutaD.Text)
                            .Item("RutaOAbrV", byFilaDgv).Value = strDevuelveCadenaOcultaDerecha(cboRutaO.Text)

                            .Item("IdLineaA", byFilaDgv).Value = cboLineaP.SelectedValue.ToString
                            .Item("DescProveedorV", byFilaDgv).Value = strDevuelveCadenaNoOcultaIzquierda(cboLineaP.Text)

                            .Item("DescripcionV", byFilaDgv).Value = txtServicio.Text

                            .Item("Vuelo", byFilaDgv).Value = txtVuelo.Text.Trim
                            '.Item("FechaSalida", byFilaDgv).Value = CDate(dtpFecSalida.Value).ToShortDateString
                            '.Item("FechaSalida", byFilaDgv).Value = "01/01/1900"
                            .Item("FechaSalida", byFilaDgv).Value = dtpDia.Text
                            '.Item("FechaRetorno", byFilaDgv).Value = CDate(DtpFecRetorno.Value).ToShortDateString
                            .Item("FechaRetorno", byFilaDgv).Value = "01/01/1900"
                            .Item("Salida", byFilaDgv).Value = dtpSalida.Text 'CDate(dtpSalida.Value.ToString).ToShortTimeString
                            '.Item("HrRecojo", byFilaDgv).Value = dtpHrRecojo.Text 'CDate(dtpHrRecojo.Value.ToString).ToShortTimeString
                            .Item("HrRecojo", byFilaDgv).Value = ""
                            If nudMasLlegada.Value = 0 Then
                                .Item("Llegada", byFilaDgv).Value = dtpLlegada.Text
                            Else
                                .Item("Llegada", byFilaDgv).Value = dtpLlegada.Text & "+" & nudMasLlegada.Value.ToString
                            End If


                            .Item("TipoTransporteV", byFilaDgv).Value = vstrTipoTransfer
                            .Item("CodReserva", byFilaDgv).Value = txtCodReserva.Text.Trim
                            .Item("Tarifa", byFilaDgv).Value = txtTarifa.Text.Trim
                            .Item("EmitidoSetoursV", byFilaDgv).Value = chkEmitidoSetours.Checked
                            .Item("EmitidoOperadorv", byFilaDgv).Value = chkEmitidoOperador.Checked
                            .Item("IDProveedorOperadorV", byFilaDgv).Value = If(cboOperador.SelectedValue Is Nothing, "", cboOperador.SelectedValue.ToString)
                            .Item("CostoOperadorV", byFilaDgv).Value = If(txtCostosOperador.Text.Trim = "", "0.00", txtCostosOperador.Text.Trim)
                        End With
                    Case "B"
                        With FormRefReserv.dgvBuss
                            Dim byFilaDgv As Byte = .CurrentRow.Index
                            '.Item("IDB", byFilaDgv).Value = vintID
                            .Item("IDCABB", byFilaDgv).Value = FormRefReserv.intIDCab
                            .Item("CotizacionB", byFilaDgv).Value = FormRefReserv.lblCotizacion.Text
                            .Item("RutaB", byFilaDgv).Value = txtRuta.Text.Trim
                            .Item("OrigenB", byFilaDgv).Value = strDevuelveCadenaNoOcultaIzquierda(cboRutaO.Text.Trim)
                            .Item("DestinoB", byFilaDgv).Value = strDevuelveCadenaNoOcultaIzquierda(cboRutaD.Text.Trim)
                            .Item("RutaDB", byFilaDgv).Value = cboRutaD.SelectedValue.ToString
                            .Item("RutaOB", byFilaDgv).Value = cboRutaO.SelectedValue.ToString

                            .Item("RutaDAbrB", byFilaDgv).Value = strDevuelveCadenaOcultaDerecha(cboRutaD.Text)
                            .Item("RutaOAbrB", byFilaDgv).Value = strDevuelveCadenaOcultaDerecha(cboRutaO.Text)

                            '.Item("IdLineaAB", byFilaDgv).Value = cboLineaP.SelectedValue.ToString
                            '.Item("DescProveedorB", byFilaDgv).Value = cboLineaP.Text
                            .Item("IdLineaAB", byFilaDgv).Value = strIDProveedor
                            .Item("DescProveedorB", byFilaDgv).Value = lblProveedor.Text

                            .Item("ServicioB", byFilaDgv).Value = txtServicio.Text

                            .Item("VueloB", byFilaDgv).Value = txtVuelo.Text.Trim
                            '.Item("Fec_SalidaB", byFilaDgv).Value = CDate(dtpFecSalida.Value).ToShortDateString
                            .Item("Fec_SalidaB", byFilaDgv).Value = dtpDia.Text
                            '.Item("Fec_RetornoB", byFilaDgv).Value = CDate(DtpFecRetorno.Value).ToShortDateString
                            .Item("Fec_RetornoB", byFilaDgv).Value = "01/01/1900"
                            .Item("SalidaB", byFilaDgv).Value = dtpSalida.Text 'CDate(dtpSalida.Value.ToString).ToShortTimeString
                            '.Item("HrRecojoB", byFilaDgv).Value = dtpHrRecojo.Text 'CDate(dtpHrRecojo.Value.ToString).ToShortTimeString
                            .Item("HrRecojoB", byFilaDgv).Value = ""
                            '.Item("LlegadaB", byFilaDgv).Value = dtpLlegada.Text & "+" & nudMasLlegada.Value.ToString
                            If nudMasLlegada.Value = 0 Then
                                .Item("LlegadaB", byFilaDgv).Value = dtpLlegada.Text
                            Else
                                .Item("LlegadaB", byFilaDgv).Value = dtpLlegada.Text & "+" & nudMasLlegada.Value.ToString
                            End If

                            .Item("TipoTransporteB", byFilaDgv).Value = vstrTipoTransfer
                            .Item("CodReservaB", byFilaDgv).Value = txtCodReserva.Text.Trim
                            .Item("TarifaB", byFilaDgv).Value = txtTarifa.Text.Trim
                            .Item("EmitidoSetoursB", byFilaDgv).Value = chkEmitidoSetours.Checked
                            .Item("CostoOperadorB", byFilaDgv).Value = If(txtCostosOperador.Text.Trim = "", "0.00", txtCostosOperador.Text.Trim)
                        End With
                    Case "T"
                        With FormRefReserv.dgvTren
                            Dim byFilaDgv As Byte = .CurrentRow.Index
                            '.Item("IDT", byFilaDgv).Value = vintID
                            .Item("DiaT", byFilaDgv).Value = dtpDia.Text    ''PPMG20160420
                            .Item("IDCABT", byFilaDgv).Value = FormRefReserv.intIDCab
                            .Item("CotizacionT", byFilaDgv).Value = FormRefReserv.lblCotizacion.Text.Trim
                            .Item("RutaT", byFilaDgv).Value = txtRuta.Text.Trim
                            .Item("OrigenT", byFilaDgv).Value = strDevuelveCadenaNoOcultaIzquierda(cboRutaO.Text.Trim)
                            .Item("DestinoT", byFilaDgv).Value = strDevuelveCadenaNoOcultaIzquierda(cboRutaD.Text.Trim)
                            .Item("RutaDT", byFilaDgv).Value = cboRutaD.SelectedValue.ToString
                            .Item("RutaOT", byFilaDgv).Value = cboRutaO.SelectedValue.ToString

                            .Item("RutaDAbrT", byFilaDgv).Value = strDevuelveCadenaOcultaDerecha(cboRutaD.Text)
                            .Item("RutaOAbrT", byFilaDgv).Value = strDevuelveCadenaOcultaDerecha(cboRutaO.Text)

                            '.Item("IdLineaAT", byFilaDgv).Value = cboLineaP.SelectedValue.ToString
                            '.Item("DescProveedorT", byFilaDgv).Value = cboLineaP.Text
                            .Item("IdLineaAT", byFilaDgv).Value = strIDProveedor
                            .Item("DescProveedorT", byFilaDgv).Value = lblProveedor.Text

                            .Item("ServicioTren", byFilaDgv).Value = txtServicio.Text

                            .Item("VueloT", byFilaDgv).Value = txtVuelo.Text.Trim
                            '.Item("Fec_SalidaT", byFilaDgv).Value = CDate(dtpFecSalida.Value).ToShortDateString
                            .Item("Fec_SalidaT", byFilaDgv).Value = dtpDia.Text
                            '.Item("Fec_RetornoT", byFilaDgv).Value = CDate(DtpFecRetorno.Value).ToShortDateString
                            .Item("Fec_RetornoT", byFilaDgv).Value = "01/01/1900"
                            .Item("SalidaT", byFilaDgv).Value = dtpSalida.Text 'CDate(dtpSalida.Value.ToString).ToShortTimeString
                            '.Item("HrRecojoT", byFilaDgv).Value = dtpHrRecojo.Text 'CDate(dtpHrRecojo.Value.ToString).ToShortTimeString
                            .Item("HrRecojoT", byFilaDgv).Value = ""
                            '.Item("LlegadaT", byFilaDgv).Value = dtpLlegada.Text & "+" & nudMasLlegada.Value.ToString
                            If nudMasLlegada.Value = 0 Then
                                .Item("LlegadaT", byFilaDgv).Value = dtpLlegada.Text
                            Else
                                .Item("LlegadaT", byFilaDgv).Value = dtpLlegada.Text & "+" & nudMasLlegada.Value.ToString
                            End If

                            .Item("TipoTransporteT", byFilaDgv).Value = vstrTipoTransfer
                            .Item("CodReservaT", byFilaDgv).Value = txtCodReserva.Text.Trim
                            .Item("TarifaT", byFilaDgv).Value = txtTarifa.Text.Trim
                            .Item("EmitidoSetoursT", byFilaDgv).Value = chkEmitidoSetours.Checked
                            .Item("CostoOperadorT", byFilaDgv).Value = If(txtCostosOperador.Text.Trim = "", "0.00", txtCostosOperador.Text.Trim)
                            '.Item("IdaVuelta", byFilaDgv).Value = cboIdaVuelta.SelectedValue.ToString
                            .Item("FlMotivoT", byFilaDgv).Value = chkMotivo.Checked
                            .Item("DescMotivoT", byFilaDgv).Value = txtDescMotivo.Text
                        End With
                End Select
            End If
            
        Catch ex As Exception
            Throw
        End Try
    End Sub

    Private Sub Grabar()
        Try
            If Not FormRefCotizacion Is Nothing Then
                'A la Grilla Vuelos Coti
                With FormRefCotizacion.dgvVuelos
                    Dim byFilaDgv As Byte = .RowCount
                    If Not blnDgvFilaVacia Then
                        .Rows.Add("")
                    Else
                        byFilaDgv -= 1
                    End If
                    .Item("ID", byFilaDgv).Value = "V" & byFilaDgv
                    .Item("DiaV", byFilaDgv).Value = dtpDia.Text
                    .Item("IDCAB", byFilaDgv).Value = FormRefCotizacion.intIDCab
                    .Item("CotizacionVuelo", byFilaDgv).Value = FormRefCotizacion.strCotizacion
                    .Item("DescripcionV", byFilaDgv).Value = txtServicio.Text
                    .Item("NombrePax", byFilaDgv).Value = "PAX"
                    .Item("RutaVuelo", byFilaDgv).Value = txtRuta.Text.Trim
                    .Item("Num_Boleto", byFilaDgv).Value = ""
                    .Item("OrigenVuelo", byFilaDgv).Value = strDevuelveCadenaNoOcultaIzquierda(cboRutaO.Text.Trim)
                    .Item("Destino", byFilaDgv).Value = strDevuelveCadenaNoOcultaIzquierda(cboRutaD.Text.Trim)
                    .Item("RutaD", byFilaDgv).Value = If(cboRutaD.SelectedValue Is Nothing, "000001", cboRutaD.SelectedValue.ToString)
                    .Item("RutaO", byFilaDgv).Value = If(cboRutaO.SelectedValue Is Nothing, "000001", cboRutaO.SelectedValue.ToString)

                    .Item("RutaDAbrV", byFilaDgv).Value = strDevuelveCadenaOcultaDerecha(cboRutaD.Text)
                    .Item("RutaOAbrV", byFilaDgv).Value = strDevuelveCadenaOcultaDerecha(cboRutaO.Text)

                    .Item("Vuelo", byFilaDgv).Value = txtVuelo.Text.Trim

                    .Item("Fec_Emision", byFilaDgv).Value = "01/01/1900"
                    .Item("Fec_Documento", byFilaDgv).Value = "01/01/1900"
                    '.Item("FechaSalida", byFilaDgv).Value = CDate(dtpFecSalida.Value).ToShortDateString
                    '.Item("FechaSalida", byFilaDgv).Value = "01/01/1900"
                    .Item("FechaSalida", byFilaDgv).Value = dtpDia.Text
                    '.Item("FechaRetorno", byFilaDgv).Value = CDate(DtpFecRetorno.Value).ToShortDateString
                    .Item("FechaRetorno", byFilaDgv).Value = "01/01/1900"
                    .Item("Salida", byFilaDgv).Value = dtpSalida.Text 'CDate(dtpSalida.Value.ToString).ToShortTimeString
                    '.Item("HrRecojo", byFilaDgv).Value = dtpHrRecojo.Text 'CDate(dtpHrRecojo.Value.ToString).ToShortTimeString
                    .Item("HrRecojo", byFilaDgv).Value = ""
                    .Item("PaxC", byFilaDgv).Value = 0
                    .Item("PCotizado", byFilaDgv).Value = 0
                    .Item("TCotizado", byFilaDgv).Value = 0
                    .Item("PaxV", byFilaDgv).Value = 0
                    .Item("PVolado", byFilaDgv).Value = 0
                    .Item("TVolado", byFilaDgv).Value = 0
                    .Item("Status", byFilaDgv).Value = ""
                    .Item("Comentario", byFilaDgv).Value = ""
                    .Item("CantPaxV", byFilaDgv).Value = CInt(FormRefCotizacion.txtNroPax.Text) + CInt(If(FormRefCotizacion.txtNroLiberados.Text = "", 0, CInt(FormRefCotizacion.txtNroLiberados.Text)))
                    '.Item("Llegada", byFilaDgv).Value = dtpLlegada.Text & "+" & nudMasLlegada.Value.ToString
                    If nudMasLlegada.Value = 0 Then
                        .Item("Llegada", byFilaDgv).Value = dtpLlegada.Text
                    Else
                        .Item("Llegada", byFilaDgv).Value = dtpLlegada.Text & "+" & nudMasLlegada.Value.ToString
                    End If

                    .Item("Emitido", byFilaDgv).Value = ""
                    .Item("TipoTransporteV", byFilaDgv).Value = "V"
                    .Item("CodReserva", byFilaDgv).Value = txtCodReserva.Text.Trim
                    .Item("Tarifa", byFilaDgv).Value = txtTarifa.Text.Trim
                    .Item("PTA", byFilaDgv).Value = 0
                    .Item("ImpuestosExt", byFilaDgv).Value = 0
                    .Item("Penalidad", byFilaDgv).Value = 0
                    .Item("IGV", byFilaDgv).Value = 0
                    .Item("Otros", byFilaDgv).Value = 0
                    .Item("PorcComm", byFilaDgv).Value = 0
                    .Item("MontoContado", byFilaDgv).Value = 0
                    .Item("MontoTarjeta", byFilaDgv).Value = 0
                    .Item("IdTarjCred", byFilaDgv).Value = ""
                    .Item("NumeroTarjeta", byFilaDgv).Value = ""
                    .Item("Aprobacion", byFilaDgv).Value = ""
                    .Item("PorcOver", byFilaDgv).Value = 0
                    .Item("PorcOverInCom", byFilaDgv).Value = 0
                    .Item("MontComm", byFilaDgv).Value = 0
                    .Item("MontCommOver", byFilaDgv).Value = 0
                    .Item("IGVComm", byFilaDgv).Value = 0
                    .Item("IGVCommOver", byFilaDgv).Value = 0
                    .Item("NetoPagar", byFilaDgv).Value = 0
                    .Item("PorcTarifa", byFilaDgv).Value = 0
                    .Item("Descuento", byFilaDgv).Value = 0
                    .Item("IdLineaA", byFilaDgv).Value = cboLineaP.SelectedValue.ToString
                    .Item("DescProveedorV", byFilaDgv).Value = strDevuelveCadenaNoOcultaIzquierda(cboLineaP.Text)
                    .Item("MontoFEE", byFilaDgv).Value = 0
                    .Item("IGVFEE", byFilaDgv).Value = 0
                    .Item("TotalFEE", byFilaDgv).Value = 0
                    .Item("EmitidoSetoursV", byFilaDgv).Value = chkEmitidoSetours.Checked
                    .Item("EmitirOperadorV", byFilaDgv).Value = chkEmitidoOperador.Checked
                    .Item("IDProveedorOperV", byFilaDgv).Value = If(cboOperador.SelectedValue Is Nothing, "", cboOperador.SelectedValue.ToString)
                    .Item("CostoOperadorV", byFilaDgv).Value = If(txtCostosOperador.Text.Trim = "", "0.00", txtCostosOperador.Text.Trim)
                    .Item("iddetV", byFilaDgv).Value = "0"
                    .Item("CoTicketV", byFilaDgv).Value = String.Empty
                    .Item("NuSegmentoV", byFilaDgv).Value = 0
                End With
            ElseIf Not FormRefReserv Is Nothing Then
                'A la Grilla Vuelos Coti
                With FormRefReserv.dgvVuelos
                    Dim byFilaDgv As Byte = .RowCount
                    If Not blnDgvFilaVacia Then
                        .Rows.Add("")
                    Else
                        byFilaDgv -= 1
                    End If
                    .Item("ID", byFilaDgv).Value = "V" & byFilaDgv
                    .Item("DiaV", byFilaDgv).Value = dtpDia.Text
                    .Item("IDCAB", byFilaDgv).Value = FormRefReserv.intIDCab
                    .Item("CotizacionVuelo", byFilaDgv).Value = FormRefReserv.lblCotizacion.Text.Trim
                    .Item("DescripcionV", byFilaDgv).Value = txtServicio.Text
                    .Item("NombrePax", byFilaDgv).Value = "PAX"
                    .Item("RutaVuelo", byFilaDgv).Value = txtRuta.Text.Trim
                    .Item("Num_Boleto", byFilaDgv).Value = ""
                    .Item("OrigenVuelo", byFilaDgv).Value = strDevuelveCadenaNoOcultaIzquierda(cboRutaO.Text.Trim)
                    .Item("Destino", byFilaDgv).Value = strDevuelveCadenaNoOcultaIzquierda(cboRutaD.Text.Trim)
                    .Item("RutaD", byFilaDgv).Value = If(cboRutaD.SelectedValue Is Nothing, "000001", cboRutaD.SelectedValue.ToString)
                    .Item("RutaO", byFilaDgv).Value = If(cboRutaO.SelectedValue Is Nothing, "000001", cboRutaO.SelectedValue.ToString)

                    .Item("RutaDAbrV", byFilaDgv).Value = strDevuelveCadenaOcultaDerecha(cboRutaD.Text)
                    .Item("RutaOAbrV", byFilaDgv).Value = strDevuelveCadenaOcultaDerecha(cboRutaO.Text)

                    .Item("Vuelo", byFilaDgv).Value = txtVuelo.Text.Trim

                    .Item("Fec_Emision", byFilaDgv).Value = "01/01/1900"
                    .Item("Fec_Documento", byFilaDgv).Value = "01/01/1900"
                    '.Item("FechaSalida", byFilaDgv).Value = CDate(dtpFecSalida.Value).ToShortDateString
                    '.Item("FechaSalida", byFilaDgv).Value = "01/01/1900"
                    .Item("FechaSalida", byFilaDgv).Value = dtpDia.Text

                    '.Item("FechaRetorno", byFilaDgv).Value = CDate(DtpFecRetorno.Value).ToShortDateString
                    .Item("FechaRetorno", byFilaDgv).Value = "01/01/1900"
                    .Item("Salida", byFilaDgv).Value = dtpSalida.Text 'CDate(dtpSalida.Value.ToString).ToShortTimeString
                    '.Item("HrRecojo", byFilaDgv).Value = dtpHrRecojo.Text 'CDate(dtpHrRecojo.Value.ToString).ToShortTimeString
                    .Item("HrRecojo", byFilaDgv).Value = ""
                    .Item("PaxC", byFilaDgv).Value = 0
                    .Item("PCotizado", byFilaDgv).Value = 0
                    .Item("TCotizado", byFilaDgv).Value = 0
                    .Item("PaxV", byFilaDgv).Value = 0
                    .Item("PVolado", byFilaDgv).Value = 0
                    .Item("TVolado", byFilaDgv).Value = 0
                    .Item("Status", byFilaDgv).Value = ""
                    .Item("Comentario", byFilaDgv).Value = ""
                    '.Item("Llegada", byFilaDgv).Value = dtpLlegada.Text & "+" & nudMasLlegada.Value.ToString
                    If nudMasLlegada.Value = 0 Then
                        .Item("Llegada", byFilaDgv).Value = dtpLlegada.Text
                    Else
                        .Item("Llegada", byFilaDgv).Value = dtpLlegada.Text & "+" & nudMasLlegada.Value.ToString
                    End If
                    .Item("CantPaxV", byFilaDgv).Value = CInt(FormRefReserv.txtNroPax.Text) + CInt(If(FormRefReserv.txtNroLiberados.Text = "", 0, CInt(FormRefReserv.txtNroLiberados.Text)))
                    .Item("Emitido", byFilaDgv).Value = ""
                    .Item("TipoTransporteV", byFilaDgv).Value = "V"
                    .Item("CodReserva", byFilaDgv).Value = txtCodReserva.Text.Trim
                    .Item("Tarifa", byFilaDgv).Value = txtTarifa.Text.Trim
                    .Item("PTA", byFilaDgv).Value = 0
                    .Item("ImpuestosExt", byFilaDgv).Value = 0
                    .Item("Penalidad", byFilaDgv).Value = 0
                    .Item("IGV", byFilaDgv).Value = 0
                    .Item("Otros", byFilaDgv).Value = 0
                    .Item("PorcComm", byFilaDgv).Value = 0
                    .Item("MontoContado", byFilaDgv).Value = 0
                    .Item("MontoTarjeta", byFilaDgv).Value = 0
                    .Item("IdTarjCred", byFilaDgv).Value = ""
                    .Item("NumeroTarjeta", byFilaDgv).Value = ""
                    .Item("Aprobacion", byFilaDgv).Value = ""
                    .Item("PorcOver", byFilaDgv).Value = 0
                    .Item("PorcOverInCom", byFilaDgv).Value = 0
                    .Item("MontComm", byFilaDgv).Value = 0
                    .Item("MontCommOver", byFilaDgv).Value = 0
                    .Item("IGVComm", byFilaDgv).Value = 0
                    .Item("IGVCommOver", byFilaDgv).Value = 0
                    .Item("NetoPagar", byFilaDgv).Value = 0
                    .Item("PorcTarifa", byFilaDgv).Value = 0
                    .Item("Descuento", byFilaDgv).Value = 0
                    .Item("IdLineaA", byFilaDgv).Value = cboLineaP.SelectedValue.ToString
                    .Item("DescProveedorV", byFilaDgv).Value = strDevuelveCadenaNoOcultaIzquierda(cboLineaP.Text)
                    .Item("MontoFEE", byFilaDgv).Value = 0
                    .Item("IGVFEE", byFilaDgv).Value = 0
                    .Item("TotalFEE", byFilaDgv).Value = 0
                    .Item("EmitidoSetoursV", byFilaDgv).Value = chkEmitidoSetours.Checked
                    .Item("EmitidoOperadorV", byFilaDgv).Value = chkEmitidoOperador.Checked
                    .Item("IDProveedorOperadorV", byFilaDgv).Value = If(cboOperador.SelectedValue Is Nothing, "", cboOperador.SelectedValue.ToString)
                    .Item("CostoOperadorV", byFilaDgv).Value = If(txtCostosOperador.Text.Trim = "", "0.00", txtCostosOperador.Text.Trim)
                    .Item("iddetV", byFilaDgv).Value = "0"
                    .Item("CoTicketV", byFilaDgv).Value = String.Empty
                    .Item("NuSegmentoV", byFilaDgv).Value = 0
                End With
            End If



        Catch ex As Exception
            Throw
        End Try
    End Sub

    Private Function blnValidarIngresos() As Boolean
        Dim blnOk As Boolean = True

        'If cboRutaD.Text = "NO APLICA" Or strDevuelveCadenaNoOcultaIzquierda(cboRutaD.Text) = "-------" Then
        '    ErrPrv.SetError(cboRutaD, "Debe seleccionar un Destino")
        '    cboRutaD.Focus()
        '    blnOk = False
        'End If

        'If cboRutaO.Text = "NO APLICA" Or strDevuelveCadenaNoOcultaIzquierda(cboRutaO.Text) = "-------" Then
        '    ErrPrv.SetError(cboRutaO, "Debe seleccionar un Origen")
        '    cboRutaO.Focus()
        '    blnOk = False
        'End If

        If cboLineaP.Visible Then
            If cboLineaP.Text.Trim = "" Or cboLineaP.Text.Trim = "NO APLICA" Then
                ErrPrv.SetError(cboLineaP, "Debe seleccionar linea Aerea")
                cboLineaP.Focus()
                blnOk = False
            End If
        End If

        If txtTarifa.Text <> "" Then
            If Not IsNumeric(txtTarifa.Text) Then
                ErrPrv.SetError(txtTarifa, "Debe ingresar Tarifa Ventas en formato numérico correcto")

                blnOk = False
            End If
        End If

        If chkEmitidoOperador.Checked Then
            If cboOperador.Text.Trim = "" Then
                ErrPrv.SetError(cboOperador, "Debe seleccionar un operador")
                cboOperador.Focus()
                blnOk = False
            End If

            If txtCostosOperador.Text.Trim = "" Then
                ErrPrv.SetError(txtCostosOperador, "Debe ingresar un costo para el operador")
                txtCostosOperador.Focus()
                blnOk = False
            End If
        End If

        'If vstrTipoTransfer = "T" Then
        '    If cboIdaVuelta.Text.Trim = "" Then
        '        ErrPrv.SetError(cboIdaVuelta, "Debe seleccionar un Ida o Vuelta")
        '        cboIdaVuelta.Focus()
        '        blnOk = False
        '    End If
        'End If
        If FormRefReserv IsNot Nothing Then
            If vstrTipoTransfer = "T" Then
                If chkMotivo.Checked And txtDescMotivo.Text.Trim.Length < 5 Then
                    ErrPrv.SetError(txtDescMotivo, "Debe ingresar una descripción valida.")
                    txtDescMotivo.Focus()
                    blnOk = False
                End If
            End If
        End If


        If Not blnOk Then
            Return False
        Else
            Return True
        End If
    End Function

    Private Sub btnSalir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSalir.Click
        'MessageBox.Show(FormRefReserv.DgvDetalle.Rows.Count.ToString)
        pSalir(Nothing, Nothing)
    End Sub

    Private Sub frmMantCotizacionVuelosVentas_MouseDown(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles Me.MouseDown
        Select Case Keys.KeyCode
            Case Keys.Escape
                pSalir(Nothing, Nothing)
        End Select
    End Sub

    Private Sub pSalir(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Me.Close()
    End Sub

    Private Sub CargarSeguridad()
        Try
            Dim strNombreForm As String
            strNombreForm = Replace(Me.Name, "frm", "mnu")
            strNombreForm = Replace(strNombreForm, "Dato", "")

            Using objSeg As New clsSeguridadBN.clsOpcionesBN.clsUsuarioBN
                Using dr As SqlClient.SqlDataReader = objSeg.ConsultarOpcionesForm(gstrUser, strNombreForm)
                    If dr.HasRows Then
                        dr.Read()
                        blnGrabarEnabledBD = dr("Grabar")
                        blnEditarEnabledBD = dr("Actualizar")
                        blnEliminarEnabledBD = dr("Eliminar")
                    End If
                    dr.Close()
                End Using
            End Using
        Catch ex As Exception
            Throw
        End Try
    End Sub

    Private Sub btnAceptar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAceptar.Click
        Try
            pGrabar()
        Catch ex As Exception
            If InStr(ex.Message, "PK_") > 0 Then
                MessageBox.Show("Ya existe un registro con el mismo Id", gstrTituloMsg, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            ElseIf InStr(ex.Message, "IX_") > 0 Then
                MessageBox.Show("Ya existe un registro con la misma Descripción", gstrTituloMsg, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            Else
                MessageBox.Show(ex.Message, ex.Source, MessageBoxButtons.OK, MessageBoxIcon.Error)
            End If
        End Try
    End Sub


    Private Sub cboLineaP_SelectionChangeCommitted(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboLineaP.SelectionChangeCommitted
        ErrPrv.SetError(cboLineaP, "")
        
        'If txtVuelo.Text.Trim.Length <= 3 Then
        txtVuelo.Text = strDevuelveCadenaOcultaDerecha(cboLineaP.Text)
        txtVuelo.Focus()
        txtVuelo.SelectionStart = txtVuelo.Text.Length
        'End If
        If Not cboLineaP.SelectedValue Is Nothing Then
            strCoLineaAereaSel = cboLineaP.SelectedValue.ToString
        End If

    End Sub

    Private Sub DtpFecRetorno_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub

    Private Sub cboRutaO_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboRutaO.SelectedIndexChanged

    End Sub

    Private Sub Label6_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Label6.Click

    End Sub

    Private Sub gpFechas_Enter(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles gpFechas.Enter

    End Sub

    Private Sub btnProveedor_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnProveedor.Click
        Dim frmAyuda As New frmAyudas
        With frmAyuda
            .strCaller = Me.Name
            .frmRefMantCotizVuelosVtas = Me
            .strTipoBusqueda = "Proveedor"

            .strPar = ""
            .strModoEdicion = strModoEdicion
            .Text = "Consulta de Proveedores"
            .ShowDialog()
        End With
    End Sub

    Private Sub cboLineaP_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboLineaP.SelectedIndexChanged

    End Sub

    Private Sub txtTarifa_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtTarifa.KeyPress, txtCostosOperador.KeyPress
        If Asc(e.KeyChar) <> 8 Then
            If InStr("0123456789.", e.KeyChar) = 0 Then
                e.KeyChar = ""
            End If
        End If
    End Sub

    Private Sub txtTarifa_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtTarifa.TextChanged
        ErrPrv.SetError(txtTarifa, "")
    End Sub

    Private Sub CargarConsultar(ByVal vblnConsulta As Boolean)
        Try
            'btnAceptar.Enabled = Not vblnConsulta
            pEnableControlsGroupBox(gpGeneral, Not vblnConsulta)
        Catch ex As Exception
            MessageBox.Show(ex.Message, ex.Source, MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub chkEmitidoOperador_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkEmitidoOperador.CheckedChanged
        'grbOperador.Enabled = chkEmitidoOperador.Checked
        'pEnableControlsGroupBox(grbOperador, chkEmitidoOperador.Checked)
        cboOperador.Enabled = chkEmitidoOperador.Checked
        txtCostosOperador.Enabled = chkEmitidoOperador.Checked
        If chkEmitidoSetours.Checked Then
            If Not blnUpdExcelPRail Then
                txtCostosOperador.Enabled = True
            End If
        End If
        If chkEmitidoOperador.Checked = False Then
            cboOperador.DataSource = Nothing
            'txtCostosOperador.Text = ""
        Else
            pCargaCombosBox(cboOperador, dtprv)
        End If
    End Sub

    Private Sub cboOperador_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboOperador.SelectedIndexChanged
        ErrPrv.SetError(cboOperador, "")
    End Sub

    Private Sub txtCostosOperador_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtCostosOperador.TextChanged
        ErrPrv.SetError(txtCostosOperador, "")
    End Sub

    Private Sub dtpDia_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dtpDia.ValueChanged, dtpSalida.ValueChanged, dtpLlegada.ValueChanged, _
        nudMasLlegada.ValueChanged, chkEmitidoOperador.CheckedChanged, chkEmitidoSetours.CheckedChanged, lblProveedor.TextChanged, cboLineaP.SelectionChangeCommitted, _
        txtVuelo.TextChanged, txtServicio.TextChanged, cboRutaD.SelectionChangeCommitted, cboRutaO.SelectionChangeCommitted, txtCodReserva.TextChanged, txtTarifa.TextChanged, _
        cboOperador.SelectionChangeCommitted, txtCostosOperador.TextChanged, txtRuta.TextChanged, dtpDia.ValueChanged, txtDescMotivo.TextChanged
        If Not blnLoad Then Exit Sub

        Try
            blnCambios = True
            ErrPrv.SetError(CType(sender, Control), "")
            If sender.name = "chkEmitidoSetours" Or sender.name = "chkEmitidoOperador" Then
                txtCostosOperador.Enabled = chkEmitidoSetours.Checked
                If chkEmitidoOperador.Checked Then
                    If Not blnUpdExcelPRail Then
                        txtCostosOperador.Enabled = True
                    End If
                End If
                If Not chkEmitidoSetours.Checked And Not chkEmitidoOperador.Checked Then
                    txtCostosOperador.Text = "0.00"
                End If
            End If
            If sender.name = "txtDescMotivo" Then
                ErrPrv.SetError(txtDescMotivo, "")
                ErrPrv.SetError(txtCostosOperador, "")
                txtCostosOperador.Enabled = chkMotivo.Checked
            End If
            If sender.name = "chkEmitidoSetours" Then
                CargarCombosLinAereas()
            End If
            btnAceptar.Enabled = Not blnSoloConsulta ' True

        Catch ex As Exception
            MessageBox.Show(ex.Message, ex.Source, MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub frmMantCotizacionVuelosVentas_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        If blnCambios = True Then
            If MessageBox.Show("Se han realizado cambios. ¿Está Ud. seguro de salir sin Aceptar?", "Confirmar", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = Windows.Forms.DialogResult.Yes Then

                e.Cancel = False
            Else
                e.Cancel = True
            End If

        End If
    End Sub

    Private Sub chkEmitidoSetours_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkEmitidoSetours.Click

    End Sub

    Private Sub chkMotivo_CheckedChanged(sender As Object, e As EventArgs) Handles chkMotivo.CheckedChanged
        If chkMotivo.Checked Then
            txtDescMotivo.Enabled = True
            txtDescMotivo.Text = ""
            txtCostosOperador.Enabled = False
            ErrPrvCosto.SetError(txtCostosOperador, "")
        Else
            txtDescMotivo.Enabled = False
            txtDescMotivo.Text = ""
            txtCostosOperador.Enabled = False
            ErrPrvCosto.SetError(txtCostosOperador, "Costo Bloqueado: Ha sido actualizado importando el archivo excel de PeruRail.")
            txtCostosOperador.Text = vdouCostoAnt
        End If
    End Sub

    Private Function douMontoOriginal()
        Dim dMonto As Double = 0
        Dim lclsCotiVuelo As New clsCotizacionBN.clsCotizacionVuelosBN
        dMonto = lclsCotiVuelo.ConsultarMontoOriginal_Pk(Integer.Parse(vintID))
        douMontoOriginal = dMonto
    End Function

End Class