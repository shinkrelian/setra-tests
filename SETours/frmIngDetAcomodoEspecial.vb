﻿Imports ComSEToursBL2

Public Class frmIngDetAcomodoEspecial

    Private Structure stAcodomodoEspecial_tmp
        Dim IDDet As String
        Dim CoCapacidad As String
        Dim QtPax As Int16
        Dim Accion As Char
    End Structure

    Public intIDCab As String
    Public strIDDet As String
    Public intPax As Int16
    Public chrModulo As Char = ""

    Public blnCambios As Boolean = False
    Dim strMensajeError As String = ""
    Dim ArrTreeViewDinamico() As TreeView

    Public blnConsultaDesdeReserva As Boolean = False
    Public dttPaxSeleccDinam As New DataTable

    Private Sub frmIngDetAcomodoEspecial_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        Select Case e.KeyCode
            Case Keys.F6
                btnAceptar_Click(Nothing, Nothing)
            Case Keys.Escape
                btnSalir_Click(Nothing, Nothing)
        End Select
    End Sub

    Private Sub frmIngDetAcomodoEspecial_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        AcomodosEspeciales_CargarGrid()
        CargarComboBoxColumns()

        ReDim ArrTreeViewDinamico(0)
        If objLstAcodomodoEspecialDistribucion.Count = 0 Then
            If objLstAcodomodoEspecial.Count = 0 Then TabControlAcomodoEspecial.TabPages.Item(1).Parent = Nothing
        Else
            CargarObjetosDistribucion()
        End If

        If blnConsultaDesdeReserva Then
            btnAceptar.Enabled = False
            TabControlAcomodoEspecial.SelectTab(1)
        End If
    End Sub

    Private Sub AcomodosEspeciales_CargarGrid()
        Try
            dgvAcomodoEspecial.Rows.Clear()

            If objLstAcodomodoEspecial.Count = 0 Then
                For int_vContador As Int16 = 0 To intPax - 1
                    dgvAcomodoEspecial.Rows.Add()
                    dgvAcomodoEspecial.Item(0, dgvAcomodoEspecial.Rows.Count - 1).Value = strIDDet
                    dgvAcomodoEspecial.Item(1, dgvAcomodoEspecial.Rows.Count - 1).Value = "N" 'FILA SIN DATO, N:NUEVO
                    dgvAcomodoEspecial.Item(2, dgvAcomodoEspecial.Rows.Count - 1).Value = Nothing
                    dgvAcomodoEspecial.Item(3, dgvAcomodoEspecial.Rows.Count - 1).Value = Nothing
                    dgvAcomodoEspecial.Item(4, dgvAcomodoEspecial.Rows.Count - 1).Value = Nothing
                    dgvAcomodoEspecial.Item(5, dgvAcomodoEspecial.Rows.Count - 1).Value = Nothing
                    dgvAcomodoEspecial.Item(6, dgvAcomodoEspecial.Rows.Count - 1).Value = Nothing
                Next
            Else
                Dim int_vCargados As Int16 = 0
                For int_vContador As Int16 = 0 To objLstAcodomodoEspecial.Count - 1
                    If int_vContador < objLstAcodomodoEspecial.Count Then
                        If objLstAcodomodoEspecial(int_vContador).Accion <> "B" And objLstAcodomodoEspecial(int_vContador).IDDet = strIDDet Then
                            dgvAcomodoEspecial.Rows.Add()
                            dgvAcomodoEspecial.Item(0, dgvAcomodoEspecial.Rows.Count - 1).Value = objLstAcodomodoEspecial(int_vContador).IDDet
                            dgvAcomodoEspecial.Item(1, dgvAcomodoEspecial.Rows.Count - 1).Value = objLstAcodomodoEspecial(int_vContador).Almacenado
                            dgvAcomodoEspecial.Item(2, dgvAcomodoEspecial.Rows.Count - 1).Value = objLstAcodomodoEspecial(int_vContador).QtPax
                            dgvAcomodoEspecial.Item(3, dgvAcomodoEspecial.Rows.Count - 1).Value = objLstAcodomodoEspecial(int_vContador).QtPax
                            dgvAcomodoEspecial.Item(4, dgvAcomodoEspecial.Rows.Count - 1).Value = objLstAcodomodoEspecial(int_vContador).CoCapacidad
                            dgvAcomodoEspecial.Item(5, dgvAcomodoEspecial.Rows.Count - 1).Value = objLstAcodomodoEspecial(int_vContador).CoCapacidad
                            dgvAcomodoEspecial.Item(6, dgvAcomodoEspecial.Rows.Count - 1).Value = objLstAcodomodoEspecial(int_vContador).CoCapacidad

                            dgvAcomodoEspecial.Item(3, dgvAcomodoEspecial.Rows.Count - 1).ReadOnly = True
                            dgvAcomodoEspecial.Item(6, dgvAcomodoEspecial.Rows.Count - 1).ReadOnly = True

                            int_vCargados = int_vCargados + 1
                        End If
                    End If
                Next
                If int_vCargados - 1 < intPax - 1 Then
                    For int_vNuevo As Int16 = int_vCargados To intPax - 1
                        dgvAcomodoEspecial.Rows.Add()
                        dgvAcomodoEspecial.Item(0, dgvAcomodoEspecial.Rows.Count - 1).Value = strIDDet
                        dgvAcomodoEspecial.Item(1, dgvAcomodoEspecial.Rows.Count - 1).Value = "N" 'FILA SIN DATO, N:NUEVO
                        dgvAcomodoEspecial.Item(2, dgvAcomodoEspecial.Rows.Count - 1).Value = Nothing
                        dgvAcomodoEspecial.Item(3, dgvAcomodoEspecial.Rows.Count - 1).Value = Nothing
                        dgvAcomodoEspecial.Item(4, dgvAcomodoEspecial.Rows.Count - 1).Value = Nothing
                        dgvAcomodoEspecial.Item(5, dgvAcomodoEspecial.Rows.Count - 1).Value = Nothing
                        dgvAcomodoEspecial.Item(6, dgvAcomodoEspecial.Rows.Count - 1).Value = Nothing
                    Next
                End If
            End If

        Catch ex As Exception
            Throw
        End Try
    End Sub

    Private Sub CargarComboBoxColumns()
        Try
            Dim objBN As New clsTablasApoyoBN.clsCapacidadHabitacionBN
            Dim dtt As DataTable = objBN.ConsultarCbo()

            Habitacion.DataSource = dtt
            Habitacion.ValueMember = "CoCapacidad"
            Habitacion.DisplayMember = "NoCapacidad"

            Capacidad.DataSource = dtt
            Capacidad.ValueMember = "CoCapacidad"
            Capacidad.DisplayMember = "QtCapacidad"

        Catch ex As Exception
            Throw
        End Try
    End Sub

    Private Sub btnSalir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSalir.Click
        blnCambios = False
        Me.Close()
    End Sub

    Private Sub btnAceptar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAceptar.Click
        If Not blnValidarMaximos() Then Exit Sub
        CargarLista()

        BorrarDatosAcomodosDistribAntiguos()
        AgregarEditarValorStrucAcomodoEspecialDistribucion()

        blnCambios = True
        Me.Close()
    End Sub

    Private Sub BorrarDatosAcomodosDistribAntiguos()
        Try
            Dim RegValDel As stAcodomodoEspecialDistribucion
            Dim Temp As New List(Of stAcodomodoEspecialDistribucion)
MoverItems:
            For Each Item As stAcodomodoEspecialDistribucion In objLstAcodomodoEspecialDistribucion
                If Item.IDCab = intIDCab Then
                    RegValDel = New stAcodomodoEspecialDistribucion
                    With RegValDel
                        .CoCapacidad = Item.CoCapacidad
                        .IDCab = Item.CoCapacidad
                        .IDDet = Item.IDDet
                        .IDHabit = Item.IDHabit
                        .IDPax = Item.IDPax
                        .UserMod = Item.UserMod
                        .Accion = "B"
                    End With
                    Temp.Add(RegValDel)
                    objLstAcodomodoEspecialDistribucion.Remove(Item)
                    GoTo MoverItems
                End If
            Next

            For Each Item2 As stAcodomodoEspecialDistribucion In Temp
                objLstAcodomodoEspecialDistribucion.Add(Item2)
            Next
        Catch ex As Exception
            Throw
        End Try
    End Sub

    Private Function blnValidarMaximos() As Boolean
        Dim bln_vInformacionCorrecta As Boolean = True
        Dim int_vRegistrosAceptados As Int16 = 0
        Dim intCurr As Int16 = 0

        ErrPrv.Clear()
        intCurr = 0
        For Each dgvRow As DataGridViewRow In dgvAcomodoEspecial.Rows
            If Not dgvRow.Cells("Pax").Value Is Nothing And Not dgvRow.Cells("Habitacion").Value Is Nothing Then
                int_vRegistrosAceptados = int_vRegistrosAceptados + 1
                dgvAcomodoEspecial.Item("Habitacion", intCurr).ErrorText = ""
                dgvAcomodoEspecial.Item("Pax", intCurr).ErrorText = ""
            ElseIf Not dgvRow.Cells("Pax").Value Is Nothing And dgvRow.Cells("Habitacion").Value Is Nothing Then
                dgvAcomodoEspecial.Item("Pax", intCurr).ErrorText = ""
                dgvAcomodoEspecial.Item("Habitacion", intCurr).ErrorText = "Debe seleccionar el tipo de habitación."
                Return False
            ElseIf dgvRow.Cells("Pax").Value Is Nothing And Not dgvRow.Cells("Habitacion").Value Is Nothing Then
                dgvAcomodoEspecial.Item("Habitacion", intCurr).ErrorText = ""
                dgvAcomodoEspecial.Item("Pax", intCurr).ErrorText = "Debe Ingresar un número de Pax."
                Return False
            End If
            intCurr = intCurr + 1
        Next

        If int_vRegistrosAceptados = 0 Then
            ErrPrv.SetError(dgvAcomodoEspecial, "Debe ingresar hasta el máximo permitido por la cantidad de pax")
            Return False
        End If


        Dim str_vPax As String
        Dim int_vNroHabitantes As Integer = 0
        Dim int_vTotalPax As Integer = 0
        Dim str_vAccion As String = ""

        intCurr = 0
        str_vPax = ""
        For Each dgvRow As DataGridViewRow In dgvAcomodoEspecial.Rows

            If Not dgvRow.Cells("Pax").Value Is Nothing And Not dgvRow.Cells("Habitacion").Value Is Nothing Then

                str_vPax = (If(dgvRow.Cells("Pax").Value = Nothing, "", dgvRow.Cells("Pax").Value.ToString))
                int_vNroHabitantes = Integer.Parse(If(dgvAcomodoEspecial.Rows(intCurr).Cells("Capacidad").FormattedValue.ToString = "", "0", dgvAcomodoEspecial.Rows(intCurr).Cells("Capacidad").FormattedValue.ToString))

                int_vTotalPax = int_vTotalPax + (Integer.Parse(str_vPax) * int_vNroHabitantes)

            End If
            intCurr = intCurr + 1
        Next
        If int_vTotalPax < intPax Then
            ErrPrv.SetError(dgvAcomodoEspecial, "La cantidad de habitaciones no esta completa.")
            Return False
        End If
        If int_vTotalPax > intPax Then
            ErrPrv.SetError(dgvAcomodoEspecial, "La cantidad de habitaciones es superior a lo permitido en este servicio.")
            Return False
        End If

        Return True

    End Function

    Private Function CargarLista()
        Dim oDatos As stAcodomodoEspecial
        Dim str_vId As String
        Dim str_vPax, str_vPaxActual As String
        Dim int_vNroHabitantes As Integer = 0
        Dim int_vTotalPax As Integer = 0
        Dim str_vHabitacion, str_vHabitacionActual, str_vEnBD As String
        Dim str_vAccion As String = ""
        Dim intCurr As Int16 = 0

        str_vPax = "" : str_vPaxActual = ""
        str_vHabitacion = "" : str_vHabitacionActual = "" : str_vEnBD = ""
        For Each dgvRow As DataGridViewRow In dgvAcomodoEspecial.Rows
            str_vId = dgvRow.Cells("ID").Value.ToString

            If Not dgvRow.Cells("Pax").Value Is Nothing And Not dgvRow.Cells("Habitacion").Value Is Nothing Then
                str_vEnBD = (If(dgvRow.Cells("EnBD").Value = Nothing, "", dgvRow.Cells("EnBD").Value.ToString))

                str_vPax = (If(dgvRow.Cells("Pax").Value = Nothing, "", dgvRow.Cells("Pax").Value.ToString))
                str_vPaxActual = (If(dgvRow.Cells("PaxActual").Value = Nothing, "", dgvRow.Cells("PaxActual").Value.ToString))

                int_vNroHabitantes = Integer.Parse(If(dgvAcomodoEspecial.Rows(intCurr).Cells("Capacidad").FormattedValue.ToString = "", "0", dgvAcomodoEspecial.Rows(intCurr).Cells("Capacidad").FormattedValue.ToString))
                str_vHabitacion = (If(dgvRow.Cells("Habitacion").Value = Nothing, "", dgvRow.Cells("Habitacion").Value.ToString))
                str_vHabitacionActual = (If(dgvRow.Cells("HabitacionActual").Value = Nothing, "", dgvRow.Cells("HabitacionActual").Value.ToString))

                int_vTotalPax = int_vTotalPax + (Integer.Parse(str_vPax) * int_vNroHabitantes)

                oDatos = New stAcodomodoEspecial
                If str_vEnBD = "N" Then
                    str_vAccion = "N"

                    If str_vPaxActual <> "" Then
                        For int_vContador As Int16 = 0 To objLstAcodomodoEspecial.Count - 1
                            With objLstAcodomodoEspecial(int_vContador)
                                If str_vHabitacionActual = .CoCapacidad And Integer.Parse(str_vPaxActual) = .QtPax Then
                                    objLstAcodomodoEspecial.RemoveAt(int_vContador)
                                    Exit For
                                End If
                            End With
                        Next
                    End If

                    oDatos.IDCab = intIDCab
                    oDatos.IDDet = strIDDet
                    oDatos.CoCapacidad = str_vHabitacion
                    oDatos.QtPax = Integer.Parse(str_vPax)
                    oDatos.Almacenado = str_vAccion
                    oDatos.Accion = str_vAccion
                    objLstAcodomodoEspecial.Add(oDatos)

                Else
                    If Integer.Parse(str_vPax) <> Integer.Parse(str_vPaxActual) Then str_vAccion = "M"
                    If str_vHabitacion <> str_vHabitacionActual Then str_vAccion = "M"

                    If str_vAccion = "M" Then
                        For int_vContador As Int16 = 0 To objLstAcodomodoEspecial.Count - 1
                            With objLstAcodomodoEspecial(int_vContador)
                                If str_vHabitacionActual = .CoCapacidad And Integer.Parse(str_vPaxActual) = .QtPax Then
                                    objLstAcodomodoEspecial.RemoveAt(int_vContador)

                                    oDatos.IDCab = intIDCab
                                    oDatos.IDDet = strIDDet
                                    oDatos.CoCapacidad = str_vHabitacion
                                    oDatos.QtPax = Integer.Parse(str_vPax)
                                    oDatos.Accion = str_vAccion
                                    objLstAcodomodoEspecial.Add(oDatos)

                                    Exit For
                                End If
                            End With
                        Next
                    End If
                End If
            End If
            intCurr = intCurr + 1
        Next
        'If int_vTotalPax < intPax Then
        '    ErrPrv.SetError(dgvAcomodoEspecial, "La cantidad de Pax no esta completa.")
        '    Return False
        'End If
        'If int_vTotalPax > intPax Then
        '    ErrPrv.SetError(dgvAcomodoEspecial, "La cantidad de Pax es superior a lo permitido en esta cotización.")
        '    Return False
        'End If

        Return True

    End Function

    Private Sub dgvAcomodoEspecial_CellBeginEdit(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellCancelEventArgs) Handles dgvAcomodoEspecial.CellBeginEdit
        If e.RowIndex = -1 Then Exit Sub
        If e.ColumnIndex = -1 Then Exit Sub
        Try
            Dim blnSelDel As Boolean = CType(dgvAcomodoEspecial.Item("btnDelDet", dgvAcomodoEspecial.CurrentRow.Index), DataGridViewButtonCell).Selected
        Catch ex As Exception
            MessageBox.Show(ex.Message, ex.Source, MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub dgvAcomodoEspecial_CellClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvAcomodoEspecial.CellClick
        Try
            If e.ColumnIndex < 0 Then Exit Sub
            If e.RowIndex < 0 Then Exit Sub

            If dgvAcomodoEspecial.Columns(e.ColumnIndex).Name = "btnDelDet" Then
                ErrPrv.Clear()
                If MessageBox.Show("¿Está Ud. seguro de eliminar el registro de la cuadrícula?", gstrTituloMsg, MessageBoxButtons.YesNo, MessageBoxIcon.Question) = Windows.Forms.DialogResult.No Then Exit Sub

                'For intColumna As Integer = 0 To dgvAcomodoEspecial.Columns.Count - 1
                'Next
                blnCambios = True
                If Not blnConsultaDesdeReserva Then btnAceptar.Enabled = True
                If objLstAcodomodoEspecial.Count > 0 Then
                    For int_vContador As Int16 = 0 To objLstAcodomodoEspecial.Count - 1
                        If Not dgvAcomodoEspecial.Item(2, e.RowIndex).Value Is Nothing Then
                            If Int16.Parse(dgvAcomodoEspecial.Item(2, e.RowIndex).Value) = objLstAcodomodoEspecial(int_vContador).QtPax And _
                                dgvAcomodoEspecial.Item(5, e.RowIndex).Value = objLstAcodomodoEspecial(int_vContador).CoCapacidad Then

                                objLstAcodomodoEspecial.RemoveAt(int_vContador)
                                If dgvAcomodoEspecial.Item(1, e.RowIndex).Value = "S" Then
                                    Dim oDatos As stAcodomodoEspecial = New stAcodomodoEspecial
                                    oDatos.IDCab = intIDCab
                                    oDatos.IDDet = strIDDet
                                    oDatos.CoCapacidad = dgvAcomodoEspecial.Item(5, e.RowIndex).Value
                                    oDatos.QtPax = Integer.Parse(dgvAcomodoEspecial.Item(2, e.RowIndex).Value)
                                    oDatos.Accion = "B"
                                    oDatos.Almacenado = dgvAcomodoEspecial.Item(1, e.RowIndex).Value
                                    objLstAcodomodoEspecial.Add(oDatos)
                                End If

                                Exit For
                            End If
                        End If
                    Next
                End If

                If TabControlDistribucion.TabPages.Count > 0 Then
                    EliminarTab(dgvAcomodoEspecial.Item(6, e.RowIndex).Value, Integer.Parse(If(dgvAcomodoEspecial.Rows(e.RowIndex).Cells("Capacidad").FormattedValue.ToString = "", "0", dgvAcomodoEspecial.Rows(e.RowIndex).Cells("Capacidad").FormattedValue.ToString)), e.RowIndex)
                End If

                dgvAcomodoEspecial.Item(3, e.RowIndex).Value = Nothing
                dgvAcomodoEspecial.Item(6, e.RowIndex).Value = Nothing

                Dim oDatos_tmp As stAcodomodoEspecial_tmp
                Dim objLstAcodomodoEspecial_tmp As New List(Of stAcodomodoEspecial_tmp)
                For Each dgvRow As DataGridViewRow In dgvAcomodoEspecial.Rows
                    If Not dgvRow.Cells("Pax").Value Is Nothing And Not dgvRow.Cells("Habitacion").Value Is Nothing Then
                        oDatos_tmp = New stAcodomodoEspecial_tmp
                        oDatos_tmp.IDDet = Integer.Parse(If(dgvRow.Cells("ID").Value = Nothing, "0", dgvRow.Cells("ID").Value.ToString))
                        oDatos_tmp.CoCapacidad = (If(dgvRow.Cells("Habitacion").Value = Nothing, "", dgvRow.Cells("Habitacion").Value.ToString))
                        oDatos_tmp.QtPax = (If(dgvRow.Cells("Pax").Value = Nothing, "", dgvRow.Cells("Pax").Value.ToString))
                        oDatos_tmp.Accion = (If(dgvRow.Cells("EnBD").Value = Nothing, "", dgvRow.Cells("EnBD").Value.ToString))
                        objLstAcodomodoEspecial_tmp.Add(oDatos_tmp)
                    End If
                Next

                dgvAcomodoEspecial.Rows.Clear()
                For int_vContador As Int16 = 0 To intPax - 1
                    dgvAcomodoEspecial.Rows.Add()
                    If int_vContador < objLstAcodomodoEspecial_tmp.Count Then
                        dgvAcomodoEspecial.Item(0, dgvAcomodoEspecial.Rows.Count - 1).Value = strIDDet
                        dgvAcomodoEspecial.Item(1, dgvAcomodoEspecial.Rows.Count - 1).Value = objLstAcodomodoEspecial_tmp(int_vContador).Accion
                        dgvAcomodoEspecial.Item(2, dgvAcomodoEspecial.Rows.Count - 1).Value = objLstAcodomodoEspecial_tmp(int_vContador).QtPax
                        dgvAcomodoEspecial.Item(3, dgvAcomodoEspecial.Rows.Count - 1).Value = objLstAcodomodoEspecial_tmp(int_vContador).QtPax
                        dgvAcomodoEspecial.Item(4, dgvAcomodoEspecial.Rows.Count - 1).Value = objLstAcodomodoEspecial_tmp(int_vContador).CoCapacidad
                        dgvAcomodoEspecial.Item(5, dgvAcomodoEspecial.Rows.Count - 1).Value = objLstAcodomodoEspecial_tmp(int_vContador).CoCapacidad
                        dgvAcomodoEspecial.Item(6, dgvAcomodoEspecial.Rows.Count - 1).Value = objLstAcodomodoEspecial_tmp(int_vContador).CoCapacidad

                        dgvAcomodoEspecial.Item(3, dgvAcomodoEspecial.Rows.Count - 1).ReadOnly = True
                        dgvAcomodoEspecial.Item(6, dgvAcomodoEspecial.Rows.Count - 1).ReadOnly = True
                    Else
                        dgvAcomodoEspecial.Item(0, dgvAcomodoEspecial.Rows.Count - 1).Value = strIDDet
                        dgvAcomodoEspecial.Item(1, dgvAcomodoEspecial.Rows.Count - 1).Value = "N" 'FILA SIN DATO, N:NUEVO
                        dgvAcomodoEspecial.Item(2, dgvAcomodoEspecial.Rows.Count - 1).Value = Nothing
                        dgvAcomodoEspecial.Item(3, dgvAcomodoEspecial.Rows.Count - 1).Value = Nothing
                        dgvAcomodoEspecial.Item(4, dgvAcomodoEspecial.Rows.Count - 1).Value = Nothing
                        dgvAcomodoEspecial.Item(5, dgvAcomodoEspecial.Rows.Count - 1).Value = Nothing
                        dgvAcomodoEspecial.Item(6, dgvAcomodoEspecial.Rows.Count - 1).Value = Nothing

                        dgvAcomodoEspecial.Item(3, dgvAcomodoEspecial.Rows.Count - 1).ReadOnly = False
                        dgvAcomodoEspecial.Item(6, dgvAcomodoEspecial.Rows.Count - 1).ReadOnly = False
                    End If
                Next
                objLstAcodomodoEspecial_tmp.Clear()
                MostrarTabDistribucion()
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message, ex.Source, MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub dgvAcomodoEspecial_CellEndEdit(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvAcomodoEspecial.CellEndEdit

        If e.ColumnIndex = 6 Then
            Dim cboHabitacion As DataGridViewComboBoxCell = TryCast(dgvAcomodoEspecial(e.ColumnIndex, e.RowIndex), DataGridViewComboBoxCell)
            If cboHabitacion IsNot Nothing Then
                Dim cboCapacidad As DataGridViewComboBoxCell = TryCast(dgvAcomodoEspecial(4, e.RowIndex), DataGridViewComboBoxCell)
                If cboCapacidad IsNot Nothing Then
                    cboCapacidad.Value = cboHabitacion.Value
                    blnCambios = True
                    If Not blnConsultaDesdeReserva Then btnAceptar.Enabled = True
                    MostrarTabDistribucion()
                End If
            End If
        End If
    End Sub

    Private Sub dgvAcomodoEspecial_CellFormatting(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellFormattingEventArgs) Handles dgvAcomodoEspecial.CellFormatting
        If e.RowIndex = -1 Then Exit Sub
        If e.ColumnIndex = -1 Then Exit Sub
        Try
            If e.ColumnIndex = dgvAcomodoEspecial.Columns("btnDelDet").Index Then
                dgvAcomodoEspecial.Item("btnDelDet", e.RowIndex).ToolTipText = "Eliminar"
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message, ex.Source, MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub dgvAcomodoEspecial_CellPainting(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellPaintingEventArgs) Handles dgvAcomodoEspecial.CellPainting
        If e.ColumnIndex < 0 Then Exit Sub
        If e.RowIndex < 0 Then Exit Sub
        Try
            If dgvAcomodoEspecial.Columns(e.ColumnIndex).Name = "btnDelDet" Then
                e.Paint(e.CellBounds, DataGridViewPaintParts.All)
                Dim celBoton As DataGridViewButtonCell = CType(Me.dgvAcomodoEspecial.Rows(e.RowIndex).Cells("btnDelDet"), DataGridViewButtonCell)
                Dim icoAtomico As Image = My.Resources.Resources.eliminarpng
                e.Graphics.DrawImage(icoAtomico, e.CellBounds.Left + 3, e.CellBounds.Top + 3)
                Me.dgvAcomodoEspecial.Rows(e.RowIndex).Height = 21
                Me.dgvAcomodoEspecial.Columns(e.ColumnIndex).Width = icoAtomico.Width + 5
                e.Handled = True
            End If
        Catch ex As Exception

        End Try
    End Sub

    Private Sub dgvAcomodoEspecial_EditingControlShowing(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewEditingControlShowingEventArgs) Handles dgvAcomodoEspecial.EditingControlShowing
        If DirectCast(sender, DataGridView).CurrentCell.ColumnIndex = 3 Then
            Dim txtPax As TextBox = DirectCast(e.Control, TextBox)
            If txtPax IsNot Nothing Then
                AddHandler txtPax.KeyPress, AddressOf txtPax_KeyPress
            End If
            MostrarTabDistribucion()
        End If
    End Sub

    Private Sub txtPax_KeyPress(ByVal sender As Object, ByVal e As KeyPressEventArgs)
        If dgvAcomodoEspecial.CurrentCell.ColumnIndex = 3 Then
            blnCambios = True
            If Not blnConsultaDesdeReserva Then btnAceptar.Enabled = True
            If ("0123456789\b".IndexOf(e.KeyChar) = -1) Then
                If e.KeyChar <> Convert.ToChar(Keys.Back) Then
                    e.Handled = True
                End If
            End If
        End If
    End Sub

    Private Sub TabControlAcomodoEspecial_Selected(ByVal sender As Object, ByVal e As System.Windows.Forms.TabControlEventArgs) Handles TabControlAcomodoEspecial.Selected
        If e.TabPageIndex = 1 Then
            Dim intCurr As Int16 = 0
            Dim int_vCapacidad As Int16
            Dim str_vHabitancionNombre As String
            Dim blnExisteTab As Boolean = False

            For Each dgvRow As DataGridViewRow In dgvAcomodoEspecial.Rows
                If Not dgvRow.Cells("Pax").Value Is Nothing And Not dgvRow.Cells("Habitacion").Value Is Nothing Then
                    str_vHabitancionNombre = CStr(If(dgvAcomodoEspecial.Rows(intCurr).Cells("Habitacion").FormattedValue.ToString = "", "", dgvAcomodoEspecial.Rows(intCurr).Cells("Habitacion").FormattedValue.ToString))
                    int_vCapacidad = CInt(If(dgvAcomodoEspecial.Rows(intCurr).Cells("Capacidad").FormattedValue.ToString = "", "0", dgvAcomodoEspecial.Rows(intCurr).Cells("Capacidad").FormattedValue.ToString))

                    blnExisteTab = False
                    If TabControlDistribucion.TabPages.Count > 0 Then
                        For int_Tabs As Int16 = 0 To TabControlDistribucion.TabPages.Count - 1
                            If TabControlDistribucion.TabPages(int_Tabs).Name = "TabPage_" & dgvRow.Cells("HabitacionActual").Value Then
                                blnExisteTab = True
                                Exit For
                            End If
                        Next
                    End If
                    If Not blnExisteTab Then
                        dgvRow.Cells("PaxActual").Value = dgvRow.Cells("Pax").Value
                        dgvRow.Cells("HabitacionActual").Value = dgvRow.Cells("Habitacion").Value
                        CrearObjetosDistribucion(dgvRow.Cells("Habitacion").Value, str_vHabitancionNombre, int_vCapacidad, dgvRow.Cells("Pax").Value, intCurr)
                    Else
                        If dgvRow.Cells("PaxActual").Value <> dgvRow.Cells("Pax").Value Or dgvRow.Cells("HabitacionActual").Value <> dgvRow.Cells("Habitacion").Value Then
                            EliminarTab(dgvRow.Cells("HabitacionActual").Value, int_vCapacidad, intCurr)
                            CrearObjetosDistribucion(dgvRow.Cells("Habitacion").Value, str_vHabitancionNombre, int_vCapacidad, dgvRow.Cells("Pax").Value, intCurr)
                            dgvRow.Cells("PaxActual").Value = dgvRow.Cells("Pax").Value
                            dgvRow.Cells("HabitacionActual").Value = dgvRow.Cells("Habitacion").Value
                        End If
                    End If
                End If
                intCurr = intCurr + 1
            Next

        End If
    End Sub

    'Private Function ConsultarPaxxIDDetTmp(ByVal vstrIDDet As String) As DataTable
    '    Try
    '        Dim dttPaxNd As New DataTable
    '        With dttPaxNd.Columns
    '            .Add("IDPax")
    '            .Add("Nombre")
    '        End With


    '        Return dttPaxNd
    '    Catch ex As Exception
    '        Throw
    '    End Try
    'End Function

    Private Sub CrearObjetosDistribucion(ByVal str_pHabitacionId As String, _
                                         ByVal str_pHabitacionNombre As String, _
                                         ByVal int_pCapacidad As Integer, _
                                         ByVal int_pPaxNro As Integer, _
                                         ByVal int_pIndex As Integer)
        ReDim Preserve ArrTreeViewDinamico(int_pIndex)

        Dim oLabel As Label = New Label
        oLabel.Name = "label_" & str_pHabitacionId
        oLabel.Text = "Habitación " & str_pHabitacionNombre
        oLabel.AutoSize = True
        oLabel.BackColor = Color.White
        oLabel.Font = New Font("Tahoma", 8.25, FontStyle.Bold, GraphicsUnit.Point, 0, False)
        oLabel.ForeColor = Color.Gray
        oLabel.Location = New Point(11, 10)

        Dim oTreeView As TreeView = New TreeView
        oTreeView.Name = "TreeView_" & str_pHabitacionId & "_" & CStr(int_pCapacidad)
        oTreeView.Location = New Point(13, 31)
        oTreeView.Height = 265
        oTreeView.Width = 440
        oTreeView.BackColor = Color.White
        oTreeView.BorderStyle = BorderStyle.Fixed3D
        oTreeView.CheckBoxes = True
        oTreeView.ImageList = ImageList1
        oTreeView.SelectedImageIndex = 0
        If blnConsultaDesdeReserva Then
            oTreeView.CheckBoxes = False
        End If

        'Dim objBN As New clsReservaBN.clsAcomodoPaxBN
        Dim objBN As New clsCotizacionBN.clsDetalleCotizacionBN.clsDetalleCotizacionAcomodoEspecialBN.clsDistribucAcomodoEspecialBN
        'Dim dttPaxNd As DataTable = objBN.ConsultarTvwNdPax(intIDCab)
        Dim dttPaxNd As New DataTable
        If dttPaxSeleccDinam.Rows.Count = 0 Then 'IsNumeric(strIDDet) Then
            dttPaxNd = objBN.ConsultarPaxxIDDet(strIDDet)
        Else
            dttPaxNd = dttPaxSeleccDinam
        End If

        With oTreeView
            For i = 1 To int_pPaxNro
                Dim ParentNode As TreeNode
                ParentNode = .Nodes.Add(i, str_pHabitacionNombre & " " & i)

                Dim childNode As TreeNode
                Dim bln_vAgregarPax As Boolean = False
                For Each PaxRow As DataRow In dttPaxNd.Rows
                    If ArrTreeViewDinamico(0) Is Nothing Then
                        childNode = ParentNode.Nodes.Add(PaxRow("IDPax").ToString, PaxRow("Nombre").ToString, 1)
                        childNode.SelectedImageIndex = 1
                    Else
                        For int_vIndexTreeView As Int16 = 0 To ArrTreeViewDinamico.Count - 1
                            If Not ArrTreeViewDinamico(int_vIndexTreeView) Is Nothing Then
                                For Each NodoPadre As TreeNode In ArrTreeViewDinamico(int_vIndexTreeView).Nodes
                                    For Each NodoHijo As TreeNode In NodoPadre.Nodes

                                        If NodoHijo.Name = PaxRow("IDPax").ToString And NodoHijo.Checked = False Then
                                            'TREEVIEW A CREAR EN ESTE MOMENTO
                                            bln_vAgregarPax = True
                                            'For Each NodoPadre1 As TreeNode In oTreeView.Nodes
                                            For Each NodoHijo1 As TreeNode In ParentNode.Nodes
                                                If NodoHijo1.Name = PaxRow("IDPax").ToString Then
                                                    If NodoHijo1.Checked Then
                                                        bln_vAgregarPax = True
                                                    Else
                                                        bln_vAgregarPax = False
                                                    End If
                                                    Exit For
                                                End If
                                            Next
                                            If bln_vAgregarPax Then
                                                childNode = ParentNode.Nodes.Add(PaxRow("IDPax").ToString, PaxRow("Nombre").ToString, 1)
                                                childNode.SelectedImageIndex = 1
                                            End If
                                            'Next
                                        End If

                                    Next
                                Next
                            End If
                        Next
                    End If
                Next
            Next
            .ExpandAll()
            .Sort()
        End With
        ArrTreeViewDinamico.SetValue(oTreeView, int_pIndex)

        AddHandler oTreeView.AfterCheck, AddressOf TreeViews_AfterCheck

        Dim oTabPage As TabPage
        oTabPage = New TabPage
        oTabPage.Text = str_pHabitacionNombre
        oTabPage.Name = "TabPage_" & str_pHabitacionId
        oTabPage.BackColor = Color.White

        oTabPage.Controls.Add(oLabel)
        oTabPage.Controls.Add(oTreeView)

        TabControlDistribucion.TabPages.Add(oTabPage)
    End Sub

    Private Sub CargarObjetosDistribucion()

        Dim intCurr As Int16 = 0
        Dim int_vCapacidad, int_pPaxNro As Int16
        Dim str_vHabitancionId, str_vHabitancionNombre As String

        Dim oLabel As Label
        Dim oTreeView As TreeView

        For Each dgvRow As DataGridViewRow In dgvAcomodoEspecial.Rows
            If Not dgvRow.Cells("Pax").Value Is Nothing And Not dgvRow.Cells("Habitacion").Value Is Nothing Then
                ReDim Preserve ArrTreeViewDinamico(intCurr)

                str_vHabitancionId = CStr(dgvRow.Cells("Habitacion").Value)
                str_vHabitancionNombre = CStr(If(dgvAcomodoEspecial.Rows(intCurr).Cells("Habitacion").FormattedValue.ToString = "", "", dgvAcomodoEspecial.Rows(intCurr).Cells("Habitacion").FormattedValue.ToString))
                int_vCapacidad = CInt(If(dgvAcomodoEspecial.Rows(intCurr).Cells("Capacidad").FormattedValue.ToString = "", "0", dgvAcomodoEspecial.Rows(intCurr).Cells("Capacidad").FormattedValue.ToString))
                int_pPaxNro = dgvRow.Cells("Pax").Value

                'CrearObjetosDistribucion(dgvRow.Cells("Habitacion").Value, str_vHabitancionNombre, int_vCapacidad, dgvRow.Cells("Pax").Value, intCurr)

                oLabel = New Label
                oLabel.Name = "label_" & str_vHabitancionId
                oLabel.Text = "Habitación " & str_vHabitancionNombre
                oLabel.AutoSize = True
                oLabel.BackColor = Color.White
                oLabel.Font = New Font("Tahoma", 8.25, FontStyle.Bold, GraphicsUnit.Point, 0, False)
                oLabel.ForeColor = Color.Gray
                oLabel.Location = New Point(11, 10)

                oTreeView = New TreeView
                oTreeView.Name = "TreeView_" & str_vHabitancionId & "_" & CStr(int_vCapacidad)
                oTreeView.Location = New Point(13, 31)
                oTreeView.Height = 265
                oTreeView.Width = 440
                oTreeView.BackColor = Color.White
                oTreeView.BorderStyle = BorderStyle.Fixed3D
                oTreeView.CheckBoxes = True
                oTreeView.ImageList = ImageList1
                oTreeView.SelectedImageIndex = 0

                'Dim objBN As New clsReservaBN.clsAcomodoPaxBN
                Dim objBN As New clsCotizacionBN.clsDetalleCotizacionBN.clsDetalleCotizacionAcomodoEspecialBN.clsDistribucAcomodoEspecialBN
                'Dim dttPaxNd As DataTable = objBN.ConsultarTvwNdPax(intIDCab)
                Dim dttPaxNd As DataTable = dttPaxSeleccDinam 'objBN.ConsultarPaxxIDDet(strIDDet)

                Dim blnPaxNoExiste As Boolean = False
                With oTreeView
                    For i = 1 To int_pPaxNro
                        Dim ParentNode As TreeNode
                        ParentNode = .Nodes.Add(i, str_vHabitancionNombre & " " & i)

                        Dim childNode As TreeNode
                        For Each PaxRow As DataRow In dttPaxNd.Rows
                            For Each st As stAcodomodoEspecialDistribucion In objLstAcodomodoEspecialDistribucion
                                If st.CoCapacidad = str_vHabitancionId And st.IDPax = PaxRow("IDPax").ToString And st.IDHabit = i Then
                                    childNode = ParentNode.Nodes.Add(PaxRow("IDPax").ToString, PaxRow("Nombre").ToString, 1)
                                    childNode.SelectedImageIndex = 1
                                    If st.Accion = "B" Then
                                        childNode.Checked = False
                                        ParentNode.Checked = False
                                    Else
                                        childNode.Checked = True
                                        ParentNode.Checked = True
                                    End If
                                    Exit For
                                End If
                            Next
                        Next
                        If ParentNode.Nodes.Count = 0 Then
                            Dim bln_vAgregaPax As Boolean

                            For Each PaxRow As DataRow In dttPaxNd.Rows
                                bln_vAgregaPax = False
                                For int_vIndexTreeView As Int16 = 0 To ArrTreeViewDinamico.Count - 1
                                    If Not ArrTreeViewDinamico(int_vIndexTreeView) Is Nothing Then
                                        For Each NodoPadre As TreeNode In ArrTreeViewDinamico(int_vIndexTreeView).Nodes
                                            For Each NodoHijo As TreeNode In NodoPadre.Nodes
                                                If NodoHijo.Name = PaxRow("IDPax").ToString And NodoHijo.Checked Then
                                                    bln_vAgregaPax = True
                                                End If
                                            Next
                                        Next
                                        ''For Each NodoPadre As TreeNode In ArrTreeViewDinamico(int_vIndexTreeView).Nodes
                                        ''    For Each NodoHijo As TreeNode In NodoPadre.Nodes

                                        ''        For Each NodoPadreActual As TreeNode In oTreeView.Nodes
                                        ''            For Each NodoHijoActual As TreeNode In NodoPadreActual.Nodes

                                        ''                If NodoHijo.Name = NodoHijoActual.Name Then
                                        ''                    MessageBox.Show("hola")
                                        ''                End If

                                        ''            Next
                                        ''        Next

                                        ''        If NodoHijo.Name = PaxRow("IDPax").ToString Then
                                        ''            If NodoHijo.Checked Then
                                        ''                bln_vAgregaPax = False
                                        ''            Else
                                        ''                For Each Nodo As TreeNode In ParentNode.Nodes
                                        ''                    If Nodo.Name = PaxRow("IDPax").ToString Then
                                        ''                        bln_vAgregaPax = False
                                        ''                    End If
                                        ''                Next
                                        ''            End If
                                        ''        End If

                                        ''    Next
                                        ''Next
                                    End If
                                Next
                                If Not bln_vAgregaPax Then
                                    Dim bln_vAgregar As Boolean = True
                                    For Each NodoPadreActual As TreeNode In oTreeView.Nodes
                                        For Each NodoHijoActual As TreeNode In NodoPadreActual.Nodes
                                            If NodoHijoActual.Name = PaxRow("IDPax").ToString Then
                                                If NodoHijoActual.Checked Then
                                                    bln_vAgregar = False
                                                End If
                                            End If
                                        Next
                                    Next
                                    If bln_vAgregar Then
                                        childNode = ParentNode.Nodes.Add(PaxRow("IDPax").ToString, PaxRow("Nombre").ToString, 1)
                                        childNode.SelectedImageIndex = 1
                                    End If
                                End If
                            Next
                        End If
                        .ExpandAll()
                        .Sort()
                    Next
                End With
                ArrTreeViewDinamico.SetValue(oTreeView, intCurr)

                AddHandler oTreeView.AfterCheck, AddressOf TreeViews_AfterCheck

                Dim oTabPage As TabPage
                oTabPage = New TabPage
                oTabPage.Text = str_vHabitancionNombre
                oTabPage.Name = "TabPage_" & str_vHabitancionId
                oTabPage.BackColor = Color.White

                oTabPage.Controls.Add(oLabel)
                oTabPage.Controls.Add(oTreeView)

                TabControlDistribucion.TabPages.Add(oTabPage)

            End If
            intCurr = intCurr + 1
        Next

    End Sub

    Private Sub EliminarTab(ByVal str_pHabitacionId As String, ByVal int_pCapacidad As Int16, ByVal int_pIndex As Integer)
        Dim int_vContador As Long
        Dim int_vCantidad As Long

        int_vCantidad = UBound(ArrTreeViewDinamico)
        If int_pIndex <= int_vCantidad And int_pIndex >= LBound(ArrTreeViewDinamico) Then
            For int_vNroNodos As Int16 = 0 To ArrTreeViewDinamico(int_pIndex).Nodes.Count - 1
                Dim strNameRaiz As String = ArrTreeViewDinamico(int_pIndex).Nodes.Item(int_vNroNodos).Name
                IngresarFilasTodas("", Nothing, strNameRaiz, ArrTreeViewDinamico(int_pIndex))
            Next

            For int_vContador = int_pIndex To int_vCantidad - 1
                ArrTreeViewDinamico(int_vContador) = ArrTreeViewDinamico(int_vContador + 1)
            Next
            ReDim Preserve ArrTreeViewDinamico(int_vCantidad - 1)
        End If

        TabControlDistribucion.TabPages.RemoveByKey("TabPage_" & str_pHabitacionId)
    End Sub

    Private Sub MostrarTabDistribucion()
        Dim blnCorrecto As Boolean = False
        For Each dgvRow As DataGridViewRow In dgvAcomodoEspecial.Rows
            If Not dgvRow.Cells("Pax").Value Is Nothing And Not dgvRow.Cells("Habitacion").Value Is Nothing Then
                TabPageDistribucion.Parent = TabControlAcomodoEspecial
                blnCorrecto = True
                Exit For
            End If
        Next
        If Not blnCorrecto Then
            If TabControlAcomodoEspecial.TabPages.Count = 2 Then TabControlAcomodoEspecial.TabPages.Item(1).Parent = Nothing
        End If

    End Sub

    Private Sub TreeViews_AfterCheck(ByVal sender As Object, ByVal e As System.Windows.Forms.TreeViewEventArgs)
        Try
            If e.Action <> TreeViewAction.Unknown Then
                If e.Node.Nodes.Count > 0 Then
                    CheckAllChildNodes(e.Node, e.Node.Checked)
                End If
                PintarChecksPadres(e.Node, e.Node.Checked)
                If Not e.Node.Parent Is Nothing Then
                    PintarChecksPadres(e.Node.Parent, e.Node.Parent.Checked)
                    If Not blnValidarMarcado(CType(sender, TreeView)) Then
                        e.Node.Checked = False
                        MessageBox.Show(strMensajeError, gstrEmpresa, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                        Exit Sub
                    End If

                    blnCambios = True
                    If Not blnConsultaDesdeReserva Then btnAceptar.Enabled = True
                    If e.Node.Checked Then
                        EliminarFilaSelect(e.Node.Name, e.Node.Parent.Name, CType(sender, TreeView).Name)
                    Else
                        IngresarFilaSelect(e.Node.Parent.Name, e.Node, CType(sender, TreeView).Name)
                    End If
                ElseIf e.Node.Parent Is Nothing Then
                    Dim blnValorNodo As Boolean = e.Node.Checked
                    Dim strNameNode As String = "", strTextNode As String = ""
                    Dim strNameRaiz As String = e.Node.Name

                    blnCambios = True
                    If Not blnConsultaDesdeReserva Then btnAceptar.Enabled = True
                    If blnValorNodo Then
                        EliminarFilaTodas(e.Node.Name, e, strNameRaiz, CType(sender, TreeView), CType(sender, TreeView).Name)
                    Else
                        IngresarFilasTodas(e.Node.Name, e, strNameRaiz, CType(sender, TreeView))
                    End If
                End If
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message, ex.Source, MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Function blnValidarMarcado(ByVal vTvw As TreeView) As Boolean
        Try
            Dim blnOk As Boolean = True
            Dim strName As String = vTvw.Name
            Dim vtnTreeView As String() = Split(strName, "_")
            Dim bytMaximo As Byte = vtnTreeView(2)

            Dim cntCheck As Byte = 0
            For Each Nodo As TreeNode In vTvw.Nodes
                For Each Nodo2 As TreeNode In Nodo.Nodes
                    If Nodo2.Checked Then cntCheck += 1
                Next
                If cntCheck > bytMaximo Then
                    blnOk = False
                    Exit For
                End If
                cntCheck = 0
            Next
            If blnOk = False Then strMensajeError = "El acomodo en " & vTvw.TopNode.Text & " excede en uno a lo permitido."

            Return blnOk
        Catch ex As Exception
            Throw
        End Try
    End Function

    Private Sub EliminarFilaSelect(ByVal vstrIDNode As String, ByVal vstrIDNodePadre As String, ByVal str_pTreeViewNombre As String)
        Try
            For Each oTreeView As TreeView In ArrTreeViewDinamico
                If oTreeView.Name = str_pTreeViewNombre Then
                    For Each Nodo As TreeNode In oTreeView.Nodes
                        For Each Nodo2 As TreeNode In Nodo.Nodes
                            If Nodo2.Name = vstrIDNode And Nodo.Name <> vstrIDNodePadre Then
                                Nodo2.Remove()
                                GoTo Limpiar1
                            End If
                        Next
Limpiar1:
                    Next
                End If
            Next

            For Each oTreeView As TreeView In ArrTreeViewDinamico
                If oTreeView.Name <> str_pTreeViewNombre Then
                    For Each Nodo As TreeNode In oTreeView.Nodes
                        For Each Nodo2 As TreeNode In Nodo.Nodes
                            If Nodo2.Name = vstrIDNode Then
                                Nodo2.Remove()
                                GoTo Limpiar2
                            End If
                        Next
Limpiar2:
                    Next
                End If
            Next
        Catch ex As Exception
            Throw
        End Try
    End Sub

    Private Sub IngresarFilaSelect(ByVal vstrIDNodeParent As String, ByVal vndoNode As TreeNode, ByVal str_pTreeViewNombre As String)
        Try
            For Each oTreeView As TreeView In ArrTreeViewDinamico
                If oTreeView.Name = str_pTreeViewNombre Then
                    For Each Nodo As TreeNode In oTreeView.Nodes
                        For Each Nodo2 As TreeNode In Nodo.Nodes
                            If Nodo.Name <> vstrIDNodeParent Then
                                If Not blnExisteEnTvw(Nodo, vndoNode.Name) Then
                                    Nodo.Nodes.Add(vndoNode.Name, vndoNode.Text, 1).SelectedImageIndex = 1
                                    Exit For
                                End If
                            End If
                        Next
                    Next
                    oTreeView.Sort()
                End If
            Next

            For Each oTreeView As TreeView In ArrTreeViewDinamico
                If oTreeView.Name <> str_pTreeViewNombre Then
                    For Each Nodo As TreeNode In oTreeView.Nodes
                        If Not blnExisteEnTvw(Nodo, vndoNode.Name) Then
                            Nodo.Nodes.Add(vndoNode.Name, vndoNode.Text, 1).SelectedImageIndex = 1
                        End If
                    Next
                    oTreeView.Sort()
                End If
            Next

        Catch ex As Exception
            Throw
        End Try
    End Sub

    Private Function blnExisteEnTvw(ByVal vTNode As TreeNode, ByVal vstrID As String) As Boolean
        Try
            Dim blnOk As Boolean = False
            For Each Nodo As TreeNode In vTNode.Nodes
                If Nodo.Name = vstrID Then
                    blnOk = True
                    GoTo FinBuscar
                End If
            Next
FinBuscar:
            Return blnOk
        Catch ex As Exception
            Throw
        End Try
    End Function

    Private Sub EliminarFilaTodas(ByVal vstrIDNode As String, ByVal e As System.Windows.Forms.TreeViewEventArgs, _
                                  ByVal vstrIDNodePadre As String, ByVal vTvw As TreeView, ByVal str_pTreeViewNombre As String)
        Try

            If blnValidarNroMaximoItems(vstrIDNodePadre, e, vTvw) Then
                For Each Nodo As TreeNode In vTvw.Nodes
                    If Nodo.Name = vstrIDNodePadre Then
                        For Each Nodo2 As TreeNode In Nodo.Nodes
                            For Each oTreeView As TreeView In ArrTreeViewDinamico
                                EliminarItemsNodoRaiz(Nodo2.Name, oTreeView, vstrIDNodePadre, If(vTvw.Name = str_pTreeViewNombre, True, False))
                            Next
                        Next
                    End If
                Next
            End If

        Catch ex As Exception
            Throw
        End Try
    End Sub

    Private Function blnValidarNroMaximoItems(ByVal strNameRaiz As String, ByVal e As System.Windows.Forms.TreeViewEventArgs, ByVal vTvw As TreeView)
        Try
            Dim vtnTreeView As String() = Split(vTvw.Name, "_")
            Dim bytNroMax As Byte = vtnTreeView(2)

            Dim blnOk As Boolean = True
            For Each Nodo As TreeNode In vTvw.Nodes
                If Nodo.Name = strNameRaiz Then
                    For Each Nodo2 As TreeNode In Nodo.Nodes
                        If Nodo.Nodes.Count > bytNroMax Then
                            e.Node.Checked = False
                            CheckAllChildNodes(e.Node, False)
                            blnOk = False
                            GoTo Final
                        End If
                    Next
                End If
            Next
Final:
            vTvw.Sort()
            Return blnOk
        Catch ex As Exception
            Throw
        End Try
    End Function

    Private Sub EliminarItemsNodoRaiz(ByVal vstrName As String, ByVal vTvw As TreeView, _
                                      ByVal vstrNameNodeRaiz As String, ByVal vblnMismoTreeView As Boolean)
        Try
Eliminado:
            For Each Nodo As TreeNode In vTvw.Nodes
                If vblnMismoTreeView Then
                    If Nodo.Name <> vstrNameNodeRaiz Then
                        For Each Nodo2 As TreeNode In Nodo.Nodes
                            If vstrName = Nodo2.Name Then
                                Nodo2.Remove()
                                'Exit Sub
                                GoTo Eliminado
                            End If
                        Next
                    End If
                Else
                    For Each Nodo2 As TreeNode In Nodo.Nodes
                        If vstrName = Nodo2.Name Then
                            Nodo2.Remove()
                            'Exit Sub
                            GoTo Eliminado
                        End If
                    Next
                End If
            Next
        Catch ex As Exception
            Throw
        End Try
    End Sub

    Private Sub IngresarFilasTodas(ByVal vstrIDNode As String, ByVal e As System.Windows.Forms.TreeViewEventArgs, _
                                   ByVal vstrIDNodePadre As String, ByVal vTvw As TreeView)
        Try

            For Each Nodo As TreeNode In vTvw.Nodes
                If Nodo.Name = vstrIDNodePadre Then
                    For Each Nodo2 As TreeNode In Nodo.Nodes
                        For Each oTreeView As TreeView In ArrTreeViewDinamico
                            AgregarItemsNodoRaiz(Nodo2.Name, Nodo2.Text, oTreeView)
                        Next
                    Next
                End If
            Next

        Catch ex As Exception
            Throw
        End Try
    End Sub

    Private Sub AgregarItemsNodoRaiz(ByVal vstrName As String, ByVal vstrText As String, _
                                     ByVal vTvw As TreeView)
        'If vNodeRaiz.Nodes Is Nothing Then Exit Sub
        Try
            Dim blnExiste As Boolean = False
            For Each Nodo As TreeNode In vTvw.Nodes
                blnExiste = blnExisteEnTvw(Nodo, vstrName)

                If Nodo.Nodes.Count = 0 Then
                    Nodo.Nodes.Add(vstrName, vstrText, 1).SelectedImageIndex = 1
                Else
                    If Not blnExiste Then
                        Nodo.Nodes.Add(vstrName, vstrText, 1).SelectedImageIndex = 1
                    End If
                End If
                blnExiste = False
            Next
            vTvw.ExpandAll()

        Catch ex As Exception
            'MessageBox.Show(ex.Message, ex.Source, MessageBoxButtons.OK, MessageBoxIcon.Error)
            Throw
        End Try
    End Sub

    Private Sub AgregarEditarValorStrucAcomodoEspecialDistribucion()
        Try
            Dim int_Cont As Int16 = 0
            Dim strAccion As String = "N"
            Dim str_vHabitacionId As String
            Dim int_vPaxId, int_vHabitacionSecuencia As Integer
            Dim str_vPaxIdVerificados As String = ""
            Dim bln_vPrimeraVez As Boolean = If(objLstAcodomodoEspecialDistribucion.Count = 0, True, False)
            Dim bln_vExistePax As Boolean = False

            Dim RegValor As stAcodomodoEspecialDistribucion
            For int_vIndexTreeView As Int16 = 0 To ArrTreeViewDinamico.Count - 1
                If Not ArrTreeViewDinamico(int_vIndexTreeView) Is Nothing Then
                    For Each nodoPadre As TreeNode In ArrTreeViewDinamico(int_vIndexTreeView).Nodes
                        For Each nodoHijo As TreeNode In nodoPadre.Nodes

                            str_vHabitacionId = Split(ArrTreeViewDinamico(int_vIndexTreeView).Name, "_")(1)
                            int_vPaxId = ArrTreeViewDinamico(int_vIndexTreeView).Nodes(nodoPadre.Index).Nodes(nodoHijo.Index).Name
                            int_vHabitacionSecuencia = nodoPadre.Name

                            'If int_vPaxId = 63015 Or int_vPaxId = 63063 Then
                            '    Dim n As Integer = 0
                            'End If

                            If bln_vPrimeraVez Then
                                If nodoHijo.Checked Then
                                    RegValor = New stAcodomodoEspecialDistribucion
                                    With RegValor
                                        .IDCab = intIDCab
                                        .IDDet = strIDDet
                                        .IDPax = int_vPaxId
                                        .IDHabit = int_vHabitacionSecuencia
                                        .CoCapacidad = str_vHabitacionId
                                        .UserMod = gstrUser
                                        .Accion = "N"
                                    End With
                                    objLstAcodomodoEspecialDistribucion.Add(RegValor)
                                End If
                            Else
                                int_Cont = 0
                                bln_vExistePax = False
                                For Each ST As stAcodomodoEspecialDistribucion In objLstAcodomodoEspecialDistribucion
                                    If ST.IDPax = int_vPaxId And ST.CoCapacidad = str_vHabitacionId And ST.IDHabit = int_vHabitacionSecuencia Then
                                        If nodoHijo.Checked Then
                                            RegValor = New stAcodomodoEspecialDistribucion
                                            With RegValor
                                                .IDCab = intIDCab
                                                .IDDet = strIDDet
                                                .IDPax = ST.IDPax
                                                .IDHabit = ST.IDHabit
                                                .CoCapacidad = ST.CoCapacidad
                                                .UserMod = gstrUser
                                                .Accion = "N"
                                            End With
                                            objLstAcodomodoEspecialDistribucion.Add(RegValor)
                                        Else
                                            RegValor = New stAcodomodoEspecialDistribucion
                                            With RegValor
                                                .IDCab = intIDCab
                                                .IDDet = strIDDet
                                                .IDPax = ST.IDPax
                                                .IDHabit = ST.IDHabit
                                                .CoCapacidad = ST.CoCapacidad
                                                .UserMod = gstrUser
                                                .Accion = "B"
                                            End With
                                            objLstAcodomodoEspecialDistribucion.RemoveAt(int_Cont)
                                            objLstAcodomodoEspecialDistribucion.Add(RegValor)
                                        End If
                                        bln_vExistePax = True
                                        Exit For
                                    End If
                                    int_Cont += 1
                                Next
                                If Not bln_vExistePax Then
                                    If nodoHijo.Checked Then
                                        RegValor = New stAcodomodoEspecialDistribucion
                                        With RegValor
                                            .IDCab = intIDCab
                                            .IDDet = strIDDet
                                            .IDPax = int_vPaxId
                                            .IDHabit = int_vHabitacionSecuencia
                                            .CoCapacidad = str_vHabitacionId
                                            .UserMod = gstrUser
                                            .Accion = "N"
                                        End With
                                        objLstAcodomodoEspecialDistribucion.Add(RegValor)
                                    End If
                                End If
                            End If
                        Next
                    Next
                End If
            Next
        Catch ex As Exception
            Throw
        End Try
    End Sub

End Class