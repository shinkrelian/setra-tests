﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmConsLogCotizaciones
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle5 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle6 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle7 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle8 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle9 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle10 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle11 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle12 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.grb = New System.Windows.Forms.GroupBox()
        Me.dgvDespues = New System.Windows.Forms.DataGridView()
        Me.NuViaticoTC2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.IDReserva_Det2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.IDOperacion_Det2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn4 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn5 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn6 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.IDDet2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn8 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Dia2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DiaFormat2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn11 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.FechaOutFormat2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Hora2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn14 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn25 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn15 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn16 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn17 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn18 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn19 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn20 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn21 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn22 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn23 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn24 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn26 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.IDServicio_Det2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DescServicioDet2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn29 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn30 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn31 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn32 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn33 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn34 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn35 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn36 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn37 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn38 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn39 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn40 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn41 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn42 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn43 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn44 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn45 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn46 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn47 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn48 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn49 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn50 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn51 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn52 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn53 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn54 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn55 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn56 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn57 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn58 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn59 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn60 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn61 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn62 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn63 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn64 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn65 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn66 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn67 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn68 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn69 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn70 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn71 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn72 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn73 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn74 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn75 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.CapacidadHab2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.EsMatrimonial2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn78 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn79 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewCheckBoxColumn1 = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        Me.DataGridViewCheckBoxColumn2 = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        Me.DataGridViewCheckBoxColumn3 = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        Me.DataGridViewTextBoxColumn80 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn81 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn82 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn83 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn84 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn85 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn86 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn87 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn88 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn89 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn90 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn91 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn92 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn93 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn94 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn95 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn96 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn97 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn98 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.CoEstadoTarifa2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.NoEstadoTarifa2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.IDReserva_DetOrigAcomVehi2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.FlagOMEN2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.FlagComparado2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.chkRealizarAcc2 = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        Me.dgvAntes = New System.Windows.Forms.DataGridView()
        Me.NuViaticoTC = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.IDReserva_Det = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.IDOperacion_Det = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.IDReserva = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.IDOperacion = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.IDFile = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.IDDet = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Item = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Dia = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DiaFormat = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.FechaOut = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.FechaOutFormat = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Hora = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Ciudad = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.PaxMasLiberados = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Noches = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.IDTipoProv = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Dias = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.IDProveedor = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Proveedor = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.CorreoProveedor = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.IDPais = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.IDubigeo = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Cantidad = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.NroPax = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.IDServicio = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.IDServicio_Det = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DescServicioDet = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DescServicioDetBup = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Especial = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.MotivoEspecial = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.RutaDocSustento = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Desayuno = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Lonche = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Almuerzo = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Cena = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Transfer = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.IDDetTransferOri = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.IDDetTransferDes = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.CamaMat = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.TipoTransporte = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.IDUbigeoOri = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DescUbigeoOri = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.IDUbigeoDes = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DescUbigeoDes = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.IDIdioma = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.IDTipoServ = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Anio = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Tipo = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DetaTipo = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.CantidadAPagar = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.NroLiberados = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Tipo_Lib = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.CostoRealAnt = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Margen = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.MargenAplicado = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.MargenAplicadoAnt = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.MargenLiberado = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.CostoRealImpto = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.CostoLiberadoImpto = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.MargenImpto = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.MargenLiberadoImpto = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.TotImpto = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.IDDetRel = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Total = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.IDVoucher = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.IDReserva_DetCopia = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.IDReserva_Det_Rel = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.IDEmailEdit = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.IDEmailNew = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.IDEmailRef = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ObservVoucher = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ObservBiblia = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ObservInterno = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.CapacidadHab = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.EsMatrimonial = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.PlanAlimenticio = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.IncGuia = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.chkServicioExportacion = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        Me.VerObserVoucher = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        Me.VerObserBiblia = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        Me.CostoReal = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.CostoLiberado = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.NetoHab = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.IgvHab = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.TotalHab = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.NetoGen = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.IgvGen = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.IDMoneda = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.SimboloMoneda = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.TipoCambio = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.TotalGen = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.IDGuiaProveedor = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.NuVehiculo = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.idTipoOC = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.InactivoDet = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ServAlimentPuro = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ConAlojamiento = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.SSCR = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.PNR = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.CoEstadoTarifa = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.NoEstadoTarifa = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.IDReserva_DetOrigAcomVehi = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.FlagOMEN = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.FlagComparado = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.chkRealizaAcc = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        Me.lblTituloAntes = New System.Windows.Forms.Label()
        Me.lblTituloDespues = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.dgvDetModificado = New System.Windows.Forms.DataGridView()
        Me.FechaLogMod = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.UsuarioMod = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.AccionMod = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.IDDetMod = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.FechaMod = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.UbigeoMod = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ServicioMod = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ProveedorMod = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.TipoServMod = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.PaxMod = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.IDIdiomaMod = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.AnioMod = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.VarianteMod = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.TotalMod = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.AtendidoMod = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DescAtendidoMod = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.dgvDetOriginal = New System.Windows.Forms.DataGridView()
        Me.FechaLogOrig = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.UsuarioOrig = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.AccionOrig = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.IDDetOrig = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DiaFormatOrig = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DescCiudadOrig = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DescServicioDetOrig = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DescProveedorOrig = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DescTipoServOrig = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.NroPaxOrig = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.IDIdiomaOrig = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.AnioOrig = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.TipoOrig = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.TotalOrig = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.AtendidoOrig = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DescAtendidoOrig = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.btnModificarReservas = New System.Windows.Forms.Button()
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.btnCrearCorreo = New System.Windows.Forms.Button()
        Me.btnAceptar = New System.Windows.Forms.Button()
        Me.btnSalir = New System.Windows.Forms.Button()
        Me.grb.SuspendLayout()
        CType(Me.dgvDespues, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvAntes, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvDetModificado, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvDetOriginal, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'grb
        '
        Me.grb.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.grb.BackColor = System.Drawing.Color.White
        Me.grb.Controls.Add(Me.dgvDespues)
        Me.grb.Controls.Add(Me.dgvAntes)
        Me.grb.Controls.Add(Me.lblTituloAntes)
        Me.grb.Controls.Add(Me.lblTituloDespues)
        Me.grb.Controls.Add(Me.Label2)
        Me.grb.Controls.Add(Me.Label1)
        Me.grb.Controls.Add(Me.dgvDetModificado)
        Me.grb.Controls.Add(Me.dgvDetOriginal)
        Me.grb.Location = New System.Drawing.Point(36, 0)
        Me.grb.Name = "grb"
        Me.grb.Size = New System.Drawing.Size(989, 612)
        Me.grb.TabIndex = 0
        Me.grb.TabStop = False
        '
        'dgvDespues
        '
        Me.dgvDespues.AllowUserToAddRows = False
        Me.dgvDespues.AllowUserToDeleteRows = False
        Me.dgvDespues.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.ControlDarkDark
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.Color.White
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvDespues.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.dgvDespues.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvDespues.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.NuViaticoTC2, Me.IDReserva_Det2, Me.IDOperacion_Det2, Me.DataGridViewTextBoxColumn4, Me.DataGridViewTextBoxColumn5, Me.DataGridViewTextBoxColumn6, Me.IDDet2, Me.DataGridViewTextBoxColumn8, Me.Dia2, Me.DiaFormat2, Me.DataGridViewTextBoxColumn11, Me.FechaOutFormat2, Me.Hora2, Me.DataGridViewTextBoxColumn14, Me.DataGridViewTextBoxColumn25, Me.DataGridViewTextBoxColumn15, Me.DataGridViewTextBoxColumn16, Me.DataGridViewTextBoxColumn17, Me.DataGridViewTextBoxColumn18, Me.DataGridViewTextBoxColumn19, Me.DataGridViewTextBoxColumn20, Me.DataGridViewTextBoxColumn21, Me.DataGridViewTextBoxColumn22, Me.DataGridViewTextBoxColumn23, Me.DataGridViewTextBoxColumn24, Me.DataGridViewTextBoxColumn26, Me.IDServicio_Det2, Me.DescServicioDet2, Me.DataGridViewTextBoxColumn29, Me.DataGridViewTextBoxColumn30, Me.DataGridViewTextBoxColumn31, Me.DataGridViewTextBoxColumn32, Me.DataGridViewTextBoxColumn33, Me.DataGridViewTextBoxColumn34, Me.DataGridViewTextBoxColumn35, Me.DataGridViewTextBoxColumn36, Me.DataGridViewTextBoxColumn37, Me.DataGridViewTextBoxColumn38, Me.DataGridViewTextBoxColumn39, Me.DataGridViewTextBoxColumn40, Me.DataGridViewTextBoxColumn41, Me.DataGridViewTextBoxColumn42, Me.DataGridViewTextBoxColumn43, Me.DataGridViewTextBoxColumn44, Me.DataGridViewTextBoxColumn45, Me.DataGridViewTextBoxColumn46, Me.DataGridViewTextBoxColumn47, Me.DataGridViewTextBoxColumn48, Me.DataGridViewTextBoxColumn49, Me.DataGridViewTextBoxColumn50, Me.DataGridViewTextBoxColumn51, Me.DataGridViewTextBoxColumn52, Me.DataGridViewTextBoxColumn53, Me.DataGridViewTextBoxColumn54, Me.DataGridViewTextBoxColumn55, Me.DataGridViewTextBoxColumn56, Me.DataGridViewTextBoxColumn57, Me.DataGridViewTextBoxColumn58, Me.DataGridViewTextBoxColumn59, Me.DataGridViewTextBoxColumn60, Me.DataGridViewTextBoxColumn61, Me.DataGridViewTextBoxColumn62, Me.DataGridViewTextBoxColumn63, Me.DataGridViewTextBoxColumn64, Me.DataGridViewTextBoxColumn65, Me.DataGridViewTextBoxColumn66, Me.DataGridViewTextBoxColumn67, Me.DataGridViewTextBoxColumn68, Me.DataGridViewTextBoxColumn69, Me.DataGridViewTextBoxColumn70, Me.DataGridViewTextBoxColumn71, Me.DataGridViewTextBoxColumn72, Me.DataGridViewTextBoxColumn73, Me.DataGridViewTextBoxColumn74, Me.DataGridViewTextBoxColumn75, Me.CapacidadHab2, Me.EsMatrimonial2, Me.DataGridViewTextBoxColumn78, Me.DataGridViewTextBoxColumn79, Me.DataGridViewCheckBoxColumn1, Me.DataGridViewCheckBoxColumn2, Me.DataGridViewCheckBoxColumn3, Me.DataGridViewTextBoxColumn80, Me.DataGridViewTextBoxColumn81, Me.DataGridViewTextBoxColumn82, Me.DataGridViewTextBoxColumn83, Me.DataGridViewTextBoxColumn84, Me.DataGridViewTextBoxColumn85, Me.DataGridViewTextBoxColumn86, Me.DataGridViewTextBoxColumn87, Me.DataGridViewTextBoxColumn88, Me.DataGridViewTextBoxColumn89, Me.DataGridViewTextBoxColumn90, Me.DataGridViewTextBoxColumn91, Me.DataGridViewTextBoxColumn92, Me.DataGridViewTextBoxColumn93, Me.DataGridViewTextBoxColumn94, Me.DataGridViewTextBoxColumn95, Me.DataGridViewTextBoxColumn96, Me.DataGridViewTextBoxColumn97, Me.DataGridViewTextBoxColumn98, Me.CoEstadoTarifa2, Me.NoEstadoTarifa2, Me.IDReserva_DetOrigAcomVehi2, Me.FlagOMEN2, Me.FlagComparado2, Me.chkRealizarAcc2})
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgvDespues.DefaultCellStyle = DataGridViewCellStyle2
        Me.dgvDespues.Location = New System.Drawing.Point(6, 325)
        Me.dgvDespues.Name = "dgvDespues"
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.ControlDarkDark
        DataGridViewCellStyle3.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        DataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.ControlDarkDark
        DataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvDespues.RowHeadersDefaultCellStyle = DataGridViewCellStyle3
        Me.dgvDespues.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect
        Me.dgvDespues.Size = New System.Drawing.Size(972, 280)
        Me.dgvDespues.TabIndex = 8
        Me.dgvDespues.Tag = ""
        '
        'NuViaticoTC2
        '
        Me.NuViaticoTC2.HeaderText = "NuViaticoTC2"
        Me.NuViaticoTC2.Name = "NuViaticoTC2"
        Me.NuViaticoTC2.Visible = False
        '
        'IDReserva_Det2
        '
        Me.IDReserva_Det2.HeaderText = "IDReserva_Det"
        Me.IDReserva_Det2.Name = "IDReserva_Det2"
        Me.IDReserva_Det2.Visible = False
        '
        'IDOperacion_Det2
        '
        Me.IDOperacion_Det2.HeaderText = "IDOperacion_Det"
        Me.IDOperacion_Det2.Name = "IDOperacion_Det2"
        Me.IDOperacion_Det2.Visible = False
        '
        'DataGridViewTextBoxColumn4
        '
        Me.DataGridViewTextBoxColumn4.HeaderText = "IDReserva"
        Me.DataGridViewTextBoxColumn4.Name = "DataGridViewTextBoxColumn4"
        Me.DataGridViewTextBoxColumn4.Visible = False
        '
        'DataGridViewTextBoxColumn5
        '
        Me.DataGridViewTextBoxColumn5.HeaderText = "IDOperacion"
        Me.DataGridViewTextBoxColumn5.Name = "DataGridViewTextBoxColumn5"
        Me.DataGridViewTextBoxColumn5.Visible = False
        '
        'DataGridViewTextBoxColumn6
        '
        Me.DataGridViewTextBoxColumn6.HeaderText = "IDFile"
        Me.DataGridViewTextBoxColumn6.Name = "DataGridViewTextBoxColumn6"
        Me.DataGridViewTextBoxColumn6.Visible = False
        '
        'IDDet2
        '
        Me.IDDet2.HeaderText = "IDDet"
        Me.IDDet2.Name = "IDDet2"
        Me.IDDet2.Visible = False
        '
        'DataGridViewTextBoxColumn8
        '
        Me.DataGridViewTextBoxColumn8.HeaderText = "Item"
        Me.DataGridViewTextBoxColumn8.Name = "DataGridViewTextBoxColumn8"
        Me.DataGridViewTextBoxColumn8.Visible = False
        '
        'Dia2
        '
        Me.Dia2.HeaderText = "Día"
        Me.Dia2.Name = "Dia2"
        Me.Dia2.Visible = False
        '
        'DiaFormat2
        '
        Me.DiaFormat2.HeaderText = "Fecha In"
        Me.DiaFormat2.Name = "DiaFormat2"
        Me.DiaFormat2.ReadOnly = True
        '
        'DataGridViewTextBoxColumn11
        '
        Me.DataGridViewTextBoxColumn11.HeaderText = "Fecha Out"
        Me.DataGridViewTextBoxColumn11.Name = "DataGridViewTextBoxColumn11"
        Me.DataGridViewTextBoxColumn11.Visible = False
        '
        'FechaOutFormat2
        '
        Me.FechaOutFormat2.HeaderText = "Fecha Out"
        Me.FechaOutFormat2.Name = "FechaOutFormat2"
        Me.FechaOutFormat2.ReadOnly = True
        '
        'Hora2
        '
        Me.Hora2.HeaderText = "Hora"
        Me.Hora2.MaxInputLength = 5
        Me.Hora2.Name = "Hora2"
        Me.Hora2.ReadOnly = True
        '
        'DataGridViewTextBoxColumn14
        '
        Me.DataGridViewTextBoxColumn14.HeaderText = "Ciudad"
        Me.DataGridViewTextBoxColumn14.Name = "DataGridViewTextBoxColumn14"
        Me.DataGridViewTextBoxColumn14.ReadOnly = True
        '
        'DataGridViewTextBoxColumn25
        '
        Me.DataGridViewTextBoxColumn25.HeaderText = "Pax"
        Me.DataGridViewTextBoxColumn25.Name = "DataGridViewTextBoxColumn25"
        '
        'DataGridViewTextBoxColumn15
        '
        Me.DataGridViewTextBoxColumn15.HeaderText = "Noches"
        Me.DataGridViewTextBoxColumn15.Name = "DataGridViewTextBoxColumn15"
        Me.DataGridViewTextBoxColumn15.ReadOnly = True
        '
        'DataGridViewTextBoxColumn16
        '
        Me.DataGridViewTextBoxColumn16.HeaderText = "IDTipoProv"
        Me.DataGridViewTextBoxColumn16.Name = "DataGridViewTextBoxColumn16"
        Me.DataGridViewTextBoxColumn16.Visible = False
        '
        'DataGridViewTextBoxColumn17
        '
        Me.DataGridViewTextBoxColumn17.HeaderText = "Dias"
        Me.DataGridViewTextBoxColumn17.Name = "DataGridViewTextBoxColumn17"
        Me.DataGridViewTextBoxColumn17.Visible = False
        '
        'DataGridViewTextBoxColumn18
        '
        Me.DataGridViewTextBoxColumn18.HeaderText = "IDProveedor"
        Me.DataGridViewTextBoxColumn18.Name = "DataGridViewTextBoxColumn18"
        Me.DataGridViewTextBoxColumn18.Visible = False
        '
        'DataGridViewTextBoxColumn19
        '
        Me.DataGridViewTextBoxColumn19.HeaderText = "Proveedor"
        Me.DataGridViewTextBoxColumn19.Name = "DataGridViewTextBoxColumn19"
        Me.DataGridViewTextBoxColumn19.Visible = False
        '
        'DataGridViewTextBoxColumn20
        '
        Me.DataGridViewTextBoxColumn20.HeaderText = "CorreoProveedor"
        Me.DataGridViewTextBoxColumn20.Name = "DataGridViewTextBoxColumn20"
        Me.DataGridViewTextBoxColumn20.Visible = False
        '
        'DataGridViewTextBoxColumn21
        '
        Me.DataGridViewTextBoxColumn21.HeaderText = "IDPais"
        Me.DataGridViewTextBoxColumn21.Name = "DataGridViewTextBoxColumn21"
        Me.DataGridViewTextBoxColumn21.Visible = False
        '
        'DataGridViewTextBoxColumn22
        '
        Me.DataGridViewTextBoxColumn22.HeaderText = "IDubigeo"
        Me.DataGridViewTextBoxColumn22.Name = "DataGridViewTextBoxColumn22"
        Me.DataGridViewTextBoxColumn22.Visible = False
        '
        'DataGridViewTextBoxColumn23
        '
        Me.DataGridViewTextBoxColumn23.HeaderText = "Cantidad"
        Me.DataGridViewTextBoxColumn23.Name = "DataGridViewTextBoxColumn23"
        Me.DataGridViewTextBoxColumn23.ReadOnly = True
        '
        'DataGridViewTextBoxColumn24
        '
        Me.DataGridViewTextBoxColumn24.HeaderText = "Pax"
        Me.DataGridViewTextBoxColumn24.Name = "DataGridViewTextBoxColumn24"
        Me.DataGridViewTextBoxColumn24.ReadOnly = True
        Me.DataGridViewTextBoxColumn24.Visible = False
        '
        'DataGridViewTextBoxColumn26
        '
        Me.DataGridViewTextBoxColumn26.HeaderText = "IDServicio"
        Me.DataGridViewTextBoxColumn26.Name = "DataGridViewTextBoxColumn26"
        Me.DataGridViewTextBoxColumn26.Visible = False
        '
        'IDServicio_Det2
        '
        Me.IDServicio_Det2.HeaderText = "IDServicio_Det"
        Me.IDServicio_Det2.Name = "IDServicio_Det2"
        Me.IDServicio_Det2.Visible = False
        '
        'DescServicioDet2
        '
        Me.DescServicioDet2.HeaderText = "Servicio"
        Me.DescServicioDet2.Name = "DescServicioDet2"
        Me.DescServicioDet2.ReadOnly = True
        '
        'DataGridViewTextBoxColumn29
        '
        Me.DataGridViewTextBoxColumn29.HeaderText = "DescServicioDetBup"
        Me.DataGridViewTextBoxColumn29.Name = "DataGridViewTextBoxColumn29"
        Me.DataGridViewTextBoxColumn29.Visible = False
        '
        'DataGridViewTextBoxColumn30
        '
        Me.DataGridViewTextBoxColumn30.HeaderText = "Especial"
        Me.DataGridViewTextBoxColumn30.Name = "DataGridViewTextBoxColumn30"
        Me.DataGridViewTextBoxColumn30.Visible = False
        '
        'DataGridViewTextBoxColumn31
        '
        Me.DataGridViewTextBoxColumn31.HeaderText = "MotivoEspecial"
        Me.DataGridViewTextBoxColumn31.Name = "DataGridViewTextBoxColumn31"
        Me.DataGridViewTextBoxColumn31.Visible = False
        '
        'DataGridViewTextBoxColumn32
        '
        Me.DataGridViewTextBoxColumn32.HeaderText = "RutaDocSustento"
        Me.DataGridViewTextBoxColumn32.Name = "DataGridViewTextBoxColumn32"
        Me.DataGridViewTextBoxColumn32.Visible = False
        '
        'DataGridViewTextBoxColumn33
        '
        Me.DataGridViewTextBoxColumn33.HeaderText = "Desayuno"
        Me.DataGridViewTextBoxColumn33.Name = "DataGridViewTextBoxColumn33"
        Me.DataGridViewTextBoxColumn33.Visible = False
        '
        'DataGridViewTextBoxColumn34
        '
        Me.DataGridViewTextBoxColumn34.HeaderText = "Lonche"
        Me.DataGridViewTextBoxColumn34.Name = "DataGridViewTextBoxColumn34"
        Me.DataGridViewTextBoxColumn34.Visible = False
        '
        'DataGridViewTextBoxColumn35
        '
        Me.DataGridViewTextBoxColumn35.HeaderText = "Almuerzo"
        Me.DataGridViewTextBoxColumn35.Name = "DataGridViewTextBoxColumn35"
        Me.DataGridViewTextBoxColumn35.Visible = False
        '
        'DataGridViewTextBoxColumn36
        '
        Me.DataGridViewTextBoxColumn36.HeaderText = "Cena"
        Me.DataGridViewTextBoxColumn36.Name = "DataGridViewTextBoxColumn36"
        Me.DataGridViewTextBoxColumn36.Visible = False
        '
        'DataGridViewTextBoxColumn37
        '
        Me.DataGridViewTextBoxColumn37.HeaderText = "Transfer"
        Me.DataGridViewTextBoxColumn37.Name = "DataGridViewTextBoxColumn37"
        Me.DataGridViewTextBoxColumn37.Visible = False
        '
        'DataGridViewTextBoxColumn38
        '
        Me.DataGridViewTextBoxColumn38.HeaderText = "IDDetTransferOri"
        Me.DataGridViewTextBoxColumn38.Name = "DataGridViewTextBoxColumn38"
        Me.DataGridViewTextBoxColumn38.Visible = False
        '
        'DataGridViewTextBoxColumn39
        '
        Me.DataGridViewTextBoxColumn39.HeaderText = "IDDetTransferDes"
        Me.DataGridViewTextBoxColumn39.Name = "DataGridViewTextBoxColumn39"
        Me.DataGridViewTextBoxColumn39.Visible = False
        '
        'DataGridViewTextBoxColumn40
        '
        Me.DataGridViewTextBoxColumn40.HeaderText = "CamaMat"
        Me.DataGridViewTextBoxColumn40.Name = "DataGridViewTextBoxColumn40"
        Me.DataGridViewTextBoxColumn40.Visible = False
        '
        'DataGridViewTextBoxColumn41
        '
        Me.DataGridViewTextBoxColumn41.HeaderText = "TipoTransporte"
        Me.DataGridViewTextBoxColumn41.Name = "DataGridViewTextBoxColumn41"
        Me.DataGridViewTextBoxColumn41.Visible = False
        '
        'DataGridViewTextBoxColumn42
        '
        Me.DataGridViewTextBoxColumn42.HeaderText = "IDUbigeoOri"
        Me.DataGridViewTextBoxColumn42.Name = "DataGridViewTextBoxColumn42"
        Me.DataGridViewTextBoxColumn42.Visible = False
        '
        'DataGridViewTextBoxColumn43
        '
        Me.DataGridViewTextBoxColumn43.HeaderText = "DescUbigeoOri"
        Me.DataGridViewTextBoxColumn43.Name = "DataGridViewTextBoxColumn43"
        Me.DataGridViewTextBoxColumn43.Visible = False
        '
        'DataGridViewTextBoxColumn44
        '
        Me.DataGridViewTextBoxColumn44.HeaderText = "IDUbigeoDes"
        Me.DataGridViewTextBoxColumn44.Name = "DataGridViewTextBoxColumn44"
        Me.DataGridViewTextBoxColumn44.Visible = False
        '
        'DataGridViewTextBoxColumn45
        '
        Me.DataGridViewTextBoxColumn45.HeaderText = "DescUbigeoDes"
        Me.DataGridViewTextBoxColumn45.Name = "DataGridViewTextBoxColumn45"
        Me.DataGridViewTextBoxColumn45.Visible = False
        '
        'DataGridViewTextBoxColumn46
        '
        Me.DataGridViewTextBoxColumn46.HeaderText = "Idioma"
        Me.DataGridViewTextBoxColumn46.Name = "DataGridViewTextBoxColumn46"
        '
        'DataGridViewTextBoxColumn47
        '
        Me.DataGridViewTextBoxColumn47.HeaderText = "IDTipoServ"
        Me.DataGridViewTextBoxColumn47.Name = "DataGridViewTextBoxColumn47"
        Me.DataGridViewTextBoxColumn47.ReadOnly = True
        Me.DataGridViewTextBoxColumn47.Visible = False
        Me.DataGridViewTextBoxColumn47.Width = 50
        '
        'DataGridViewTextBoxColumn48
        '
        Me.DataGridViewTextBoxColumn48.HeaderText = "Año"
        Me.DataGridViewTextBoxColumn48.Name = "DataGridViewTextBoxColumn48"
        Me.DataGridViewTextBoxColumn48.ReadOnly = True
        '
        'DataGridViewTextBoxColumn49
        '
        Me.DataGridViewTextBoxColumn49.HeaderText = "Variante Servicio"
        Me.DataGridViewTextBoxColumn49.Name = "DataGridViewTextBoxColumn49"
        Me.DataGridViewTextBoxColumn49.ReadOnly = True
        '
        'DataGridViewTextBoxColumn50
        '
        Me.DataGridViewTextBoxColumn50.HeaderText = "DetaTipo"
        Me.DataGridViewTextBoxColumn50.Name = "DataGridViewTextBoxColumn50"
        Me.DataGridViewTextBoxColumn50.Visible = False
        '
        'DataGridViewTextBoxColumn51
        '
        Me.DataGridViewTextBoxColumn51.HeaderText = "Cant. a Pagar"
        Me.DataGridViewTextBoxColumn51.Name = "DataGridViewTextBoxColumn51"
        Me.DataGridViewTextBoxColumn51.ReadOnly = True
        '
        'DataGridViewTextBoxColumn52
        '
        Me.DataGridViewTextBoxColumn52.HeaderText = "NroLiberados"
        Me.DataGridViewTextBoxColumn52.Name = "DataGridViewTextBoxColumn52"
        Me.DataGridViewTextBoxColumn52.Visible = False
        '
        'DataGridViewTextBoxColumn53
        '
        Me.DataGridViewTextBoxColumn53.HeaderText = "Tipo_Lib"
        Me.DataGridViewTextBoxColumn53.Name = "DataGridViewTextBoxColumn53"
        Me.DataGridViewTextBoxColumn53.Visible = False
        '
        'DataGridViewTextBoxColumn54
        '
        Me.DataGridViewTextBoxColumn54.HeaderText = "CostoRealAnt"
        Me.DataGridViewTextBoxColumn54.Name = "DataGridViewTextBoxColumn54"
        Me.DataGridViewTextBoxColumn54.Visible = False
        '
        'DataGridViewTextBoxColumn55
        '
        Me.DataGridViewTextBoxColumn55.HeaderText = "Margen"
        Me.DataGridViewTextBoxColumn55.Name = "DataGridViewTextBoxColumn55"
        Me.DataGridViewTextBoxColumn55.Visible = False
        '
        'DataGridViewTextBoxColumn56
        '
        Me.DataGridViewTextBoxColumn56.HeaderText = "MargenAplicado"
        Me.DataGridViewTextBoxColumn56.Name = "DataGridViewTextBoxColumn56"
        Me.DataGridViewTextBoxColumn56.Visible = False
        '
        'DataGridViewTextBoxColumn57
        '
        Me.DataGridViewTextBoxColumn57.HeaderText = "MargenAplicadoAnt"
        Me.DataGridViewTextBoxColumn57.Name = "DataGridViewTextBoxColumn57"
        Me.DataGridViewTextBoxColumn57.Visible = False
        '
        'DataGridViewTextBoxColumn58
        '
        Me.DataGridViewTextBoxColumn58.HeaderText = "MargenLiberado"
        Me.DataGridViewTextBoxColumn58.Name = "DataGridViewTextBoxColumn58"
        Me.DataGridViewTextBoxColumn58.Visible = False
        '
        'DataGridViewTextBoxColumn59
        '
        Me.DataGridViewTextBoxColumn59.HeaderText = "TotalOrig"
        Me.DataGridViewTextBoxColumn59.Name = "DataGridViewTextBoxColumn59"
        Me.DataGridViewTextBoxColumn59.Visible = False
        '
        'DataGridViewTextBoxColumn60
        '
        Me.DataGridViewTextBoxColumn60.HeaderText = "CostoRealImpto"
        Me.DataGridViewTextBoxColumn60.Name = "DataGridViewTextBoxColumn60"
        Me.DataGridViewTextBoxColumn60.Visible = False
        '
        'DataGridViewTextBoxColumn61
        '
        Me.DataGridViewTextBoxColumn61.HeaderText = "CostoLiberadoImpto"
        Me.DataGridViewTextBoxColumn61.Name = "DataGridViewTextBoxColumn61"
        Me.DataGridViewTextBoxColumn61.Visible = False
        '
        'DataGridViewTextBoxColumn62
        '
        Me.DataGridViewTextBoxColumn62.HeaderText = "MargenImpto"
        Me.DataGridViewTextBoxColumn62.Name = "DataGridViewTextBoxColumn62"
        Me.DataGridViewTextBoxColumn62.Visible = False
        '
        'DataGridViewTextBoxColumn63
        '
        Me.DataGridViewTextBoxColumn63.HeaderText = "MargenLiberadoImpto"
        Me.DataGridViewTextBoxColumn63.Name = "DataGridViewTextBoxColumn63"
        Me.DataGridViewTextBoxColumn63.Visible = False
        '
        'DataGridViewTextBoxColumn64
        '
        Me.DataGridViewTextBoxColumn64.HeaderText = "TotImpto"
        Me.DataGridViewTextBoxColumn64.Name = "DataGridViewTextBoxColumn64"
        Me.DataGridViewTextBoxColumn64.Visible = False
        '
        'DataGridViewTextBoxColumn65
        '
        Me.DataGridViewTextBoxColumn65.HeaderText = "IDDetRel"
        Me.DataGridViewTextBoxColumn65.Name = "DataGridViewTextBoxColumn65"
        Me.DataGridViewTextBoxColumn65.Visible = False
        '
        'DataGridViewTextBoxColumn66
        '
        Me.DataGridViewTextBoxColumn66.HeaderText = "Total"
        Me.DataGridViewTextBoxColumn66.Name = "DataGridViewTextBoxColumn66"
        Me.DataGridViewTextBoxColumn66.ReadOnly = True
        Me.DataGridViewTextBoxColumn66.Visible = False
        '
        'DataGridViewTextBoxColumn67
        '
        Me.DataGridViewTextBoxColumn67.HeaderText = "Nro. Voucher"
        Me.DataGridViewTextBoxColumn67.Name = "DataGridViewTextBoxColumn67"
        Me.DataGridViewTextBoxColumn67.ReadOnly = True
        Me.DataGridViewTextBoxColumn67.Visible = False
        '
        'DataGridViewTextBoxColumn68
        '
        Me.DataGridViewTextBoxColumn68.HeaderText = "IDReserva_DetCopia"
        Me.DataGridViewTextBoxColumn68.Name = "DataGridViewTextBoxColumn68"
        Me.DataGridViewTextBoxColumn68.Visible = False
        '
        'DataGridViewTextBoxColumn69
        '
        Me.DataGridViewTextBoxColumn69.HeaderText = "IDReserva_Det_Rel"
        Me.DataGridViewTextBoxColumn69.Name = "DataGridViewTextBoxColumn69"
        Me.DataGridViewTextBoxColumn69.Visible = False
        '
        'DataGridViewTextBoxColumn70
        '
        Me.DataGridViewTextBoxColumn70.HeaderText = "IDEmailEdit"
        Me.DataGridViewTextBoxColumn70.Name = "DataGridViewTextBoxColumn70"
        Me.DataGridViewTextBoxColumn70.Visible = False
        '
        'DataGridViewTextBoxColumn71
        '
        Me.DataGridViewTextBoxColumn71.HeaderText = "IDEmailNew"
        Me.DataGridViewTextBoxColumn71.Name = "DataGridViewTextBoxColumn71"
        Me.DataGridViewTextBoxColumn71.Visible = False
        '
        'DataGridViewTextBoxColumn72
        '
        Me.DataGridViewTextBoxColumn72.HeaderText = "IDEmailRef"
        Me.DataGridViewTextBoxColumn72.Name = "DataGridViewTextBoxColumn72"
        Me.DataGridViewTextBoxColumn72.Visible = False
        '
        'DataGridViewTextBoxColumn73
        '
        Me.DataGridViewTextBoxColumn73.HeaderText = "ObservVoucher"
        Me.DataGridViewTextBoxColumn73.Name = "DataGridViewTextBoxColumn73"
        Me.DataGridViewTextBoxColumn73.Visible = False
        '
        'DataGridViewTextBoxColumn74
        '
        Me.DataGridViewTextBoxColumn74.HeaderText = "ObservBiblia"
        Me.DataGridViewTextBoxColumn74.Name = "DataGridViewTextBoxColumn74"
        Me.DataGridViewTextBoxColumn74.Visible = False
        '
        'DataGridViewTextBoxColumn75
        '
        Me.DataGridViewTextBoxColumn75.HeaderText = "ObservInterno"
        Me.DataGridViewTextBoxColumn75.Name = "DataGridViewTextBoxColumn75"
        Me.DataGridViewTextBoxColumn75.Visible = False
        '
        'CapacidadHab2
        '
        Me.CapacidadHab2.HeaderText = "CapacidadHab"
        Me.CapacidadHab2.Name = "CapacidadHab2"
        Me.CapacidadHab2.Visible = False
        '
        'EsMatrimonial2
        '
        Me.EsMatrimonial2.HeaderText = "EsMatrimonial"
        Me.EsMatrimonial2.Name = "EsMatrimonial2"
        Me.EsMatrimonial2.Visible = False
        '
        'DataGridViewTextBoxColumn78
        '
        Me.DataGridViewTextBoxColumn78.HeaderText = "PlanAlimenticio"
        Me.DataGridViewTextBoxColumn78.Name = "DataGridViewTextBoxColumn78"
        Me.DataGridViewTextBoxColumn78.Visible = False
        '
        'DataGridViewTextBoxColumn79
        '
        Me.DataGridViewTextBoxColumn79.HeaderText = "IncGuia"
        Me.DataGridViewTextBoxColumn79.Name = "DataGridViewTextBoxColumn79"
        Me.DataGridViewTextBoxColumn79.Visible = False
        '
        'DataGridViewCheckBoxColumn1
        '
        Me.DataGridViewCheckBoxColumn1.HeaderText = "Exp. de Servicio"
        Me.DataGridViewCheckBoxColumn1.Name = "DataGridViewCheckBoxColumn1"
        Me.DataGridViewCheckBoxColumn1.Visible = False
        '
        'DataGridViewCheckBoxColumn2
        '
        Me.DataGridViewCheckBoxColumn2.HeaderText = "Ver Voucher"
        Me.DataGridViewCheckBoxColumn2.Name = "DataGridViewCheckBoxColumn2"
        Me.DataGridViewCheckBoxColumn2.Visible = False
        '
        'DataGridViewCheckBoxColumn3
        '
        Me.DataGridViewCheckBoxColumn3.HeaderText = "Ver Biblia"
        Me.DataGridViewCheckBoxColumn3.Name = "DataGridViewCheckBoxColumn3"
        Me.DataGridViewCheckBoxColumn3.Visible = False
        '
        'DataGridViewTextBoxColumn80
        '
        Me.DataGridViewTextBoxColumn80.HeaderText = "CostoReal"
        Me.DataGridViewTextBoxColumn80.Name = "DataGridViewTextBoxColumn80"
        Me.DataGridViewTextBoxColumn80.ReadOnly = True
        Me.DataGridViewTextBoxColumn80.Visible = False
        '
        'DataGridViewTextBoxColumn81
        '
        Me.DataGridViewTextBoxColumn81.HeaderText = "CostoLiberado"
        Me.DataGridViewTextBoxColumn81.Name = "DataGridViewTextBoxColumn81"
        Me.DataGridViewTextBoxColumn81.ReadOnly = True
        Me.DataGridViewTextBoxColumn81.Visible = False
        '
        'DataGridViewTextBoxColumn82
        '
        Me.DataGridViewTextBoxColumn82.HeaderText = "Neto Hab."
        Me.DataGridViewTextBoxColumn82.Name = "DataGridViewTextBoxColumn82"
        Me.DataGridViewTextBoxColumn82.ReadOnly = True
        '
        'DataGridViewTextBoxColumn83
        '
        Me.DataGridViewTextBoxColumn83.HeaderText = "Igv Hab."
        Me.DataGridViewTextBoxColumn83.Name = "DataGridViewTextBoxColumn83"
        Me.DataGridViewTextBoxColumn83.ReadOnly = True
        '
        'DataGridViewTextBoxColumn84
        '
        Me.DataGridViewTextBoxColumn84.HeaderText = "Total Hab."
        Me.DataGridViewTextBoxColumn84.Name = "DataGridViewTextBoxColumn84"
        Me.DataGridViewTextBoxColumn84.ReadOnly = True
        '
        'DataGridViewTextBoxColumn85
        '
        Me.DataGridViewTextBoxColumn85.HeaderText = "Neto Total"
        Me.DataGridViewTextBoxColumn85.Name = "DataGridViewTextBoxColumn85"
        Me.DataGridViewTextBoxColumn85.ReadOnly = True
        '
        'DataGridViewTextBoxColumn86
        '
        Me.DataGridViewTextBoxColumn86.HeaderText = "Igv Total"
        Me.DataGridViewTextBoxColumn86.Name = "DataGridViewTextBoxColumn86"
        Me.DataGridViewTextBoxColumn86.ReadOnly = True
        '
        'DataGridViewTextBoxColumn87
        '
        Me.DataGridViewTextBoxColumn87.HeaderText = "IDMoneda"
        Me.DataGridViewTextBoxColumn87.Name = "DataGridViewTextBoxColumn87"
        Me.DataGridViewTextBoxColumn87.Visible = False
        '
        'DataGridViewTextBoxColumn88
        '
        Me.DataGridViewTextBoxColumn88.HeaderText = "Mon"
        Me.DataGridViewTextBoxColumn88.Name = "DataGridViewTextBoxColumn88"
        '
        'DataGridViewTextBoxColumn89
        '
        Me.DataGridViewTextBoxColumn89.HeaderText = "TipoCambio"
        Me.DataGridViewTextBoxColumn89.Name = "DataGridViewTextBoxColumn89"
        Me.DataGridViewTextBoxColumn89.Visible = False
        '
        'DataGridViewTextBoxColumn90
        '
        Me.DataGridViewTextBoxColumn90.HeaderText = "Total"
        Me.DataGridViewTextBoxColumn90.Name = "DataGridViewTextBoxColumn90"
        Me.DataGridViewTextBoxColumn90.ReadOnly = True
        '
        'DataGridViewTextBoxColumn91
        '
        Me.DataGridViewTextBoxColumn91.HeaderText = "IDGuiaProveedor"
        Me.DataGridViewTextBoxColumn91.Name = "DataGridViewTextBoxColumn91"
        Me.DataGridViewTextBoxColumn91.ReadOnly = True
        Me.DataGridViewTextBoxColumn91.Visible = False
        '
        'DataGridViewTextBoxColumn92
        '
        Me.DataGridViewTextBoxColumn92.HeaderText = "DescBus"
        Me.DataGridViewTextBoxColumn92.Name = "DataGridViewTextBoxColumn92"
        Me.DataGridViewTextBoxColumn92.ReadOnly = True
        Me.DataGridViewTextBoxColumn92.Visible = False
        '
        'DataGridViewTextBoxColumn93
        '
        Me.DataGridViewTextBoxColumn93.HeaderText = "idTipoOC"
        Me.DataGridViewTextBoxColumn93.Name = "DataGridViewTextBoxColumn93"
        Me.DataGridViewTextBoxColumn93.Visible = False
        '
        'DataGridViewTextBoxColumn94
        '
        Me.DataGridViewTextBoxColumn94.HeaderText = "InactivoDet"
        Me.DataGridViewTextBoxColumn94.Name = "DataGridViewTextBoxColumn94"
        Me.DataGridViewTextBoxColumn94.Visible = False
        '
        'DataGridViewTextBoxColumn95
        '
        Me.DataGridViewTextBoxColumn95.HeaderText = "ServAlimentPuro"
        Me.DataGridViewTextBoxColumn95.Name = "DataGridViewTextBoxColumn95"
        Me.DataGridViewTextBoxColumn95.Visible = False
        '
        'DataGridViewTextBoxColumn96
        '
        Me.DataGridViewTextBoxColumn96.HeaderText = "ConAlojamiento"
        Me.DataGridViewTextBoxColumn96.Name = "DataGridViewTextBoxColumn96"
        Me.DataGridViewTextBoxColumn96.Visible = False
        '
        'DataGridViewTextBoxColumn97
        '
        Me.DataGridViewTextBoxColumn97.HeaderText = "SSCR"
        Me.DataGridViewTextBoxColumn97.Name = "DataGridViewTextBoxColumn97"
        Me.DataGridViewTextBoxColumn97.Visible = False
        '
        'DataGridViewTextBoxColumn98
        '
        Me.DataGridViewTextBoxColumn98.HeaderText = "PNR"
        Me.DataGridViewTextBoxColumn98.Name = "DataGridViewTextBoxColumn98"
        Me.DataGridViewTextBoxColumn98.Visible = False
        '
        'CoEstadoTarifa2
        '
        Me.CoEstadoTarifa2.HeaderText = "CoEstadoTarifa2"
        Me.CoEstadoTarifa2.Name = "CoEstadoTarifa2"
        Me.CoEstadoTarifa2.Visible = False
        '
        'NoEstadoTarifa2
        '
        Me.NoEstadoTarifa2.HeaderText = "Estado Tarifa"
        Me.NoEstadoTarifa2.Name = "NoEstadoTarifa2"
        Me.NoEstadoTarifa2.Visible = False
        '
        'IDReserva_DetOrigAcomVehi2
        '
        Me.IDReserva_DetOrigAcomVehi2.HeaderText = "IDReserva_DetOrigAcomVehi"
        Me.IDReserva_DetOrigAcomVehi2.Name = "IDReserva_DetOrigAcomVehi2"
        Me.IDReserva_DetOrigAcomVehi2.Visible = False
        '
        'FlagOMEN2
        '
        Me.FlagOMEN2.HeaderText = "Acción"
        Me.FlagOMEN2.Name = "FlagOMEN2"
        Me.FlagOMEN2.ReadOnly = True
        '
        'FlagComparado2
        '
        Me.FlagComparado2.HeaderText = "FlagComparado2"
        Me.FlagComparado2.Name = "FlagComparado2"
        Me.FlagComparado2.Visible = False
        '
        'chkRealizarAcc2
        '
        Me.chkRealizarAcc2.HeaderText = ""
        Me.chkRealizarAcc2.Name = "chkRealizarAcc2"
        Me.chkRealizarAcc2.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.chkRealizarAcc2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic
        Me.chkRealizarAcc2.Width = 21
        '
        'dgvAntes
        '
        Me.dgvAntes.AllowUserToAddRows = False
        Me.dgvAntes.AllowUserToDeleteRows = False
        Me.dgvAntes.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        DataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.ControlDarkDark
        DataGridViewCellStyle4.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle4.ForeColor = System.Drawing.Color.White
        DataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvAntes.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle4
        Me.dgvAntes.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvAntes.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.NuViaticoTC, Me.IDReserva_Det, Me.IDOperacion_Det, Me.IDReserva, Me.IDOperacion, Me.IDFile, Me.IDDet, Me.Item, Me.Dia, Me.DiaFormat, Me.FechaOut, Me.FechaOutFormat, Me.Hora, Me.Ciudad, Me.PaxMasLiberados, Me.Noches, Me.IDTipoProv, Me.Dias, Me.IDProveedor, Me.Proveedor, Me.CorreoProveedor, Me.IDPais, Me.IDubigeo, Me.Cantidad, Me.NroPax, Me.IDServicio, Me.IDServicio_Det, Me.DescServicioDet, Me.DescServicioDetBup, Me.Especial, Me.MotivoEspecial, Me.RutaDocSustento, Me.Desayuno, Me.Lonche, Me.Almuerzo, Me.Cena, Me.Transfer, Me.IDDetTransferOri, Me.IDDetTransferDes, Me.CamaMat, Me.TipoTransporte, Me.IDUbigeoOri, Me.DescUbigeoOri, Me.IDUbigeoDes, Me.DescUbigeoDes, Me.IDIdioma, Me.IDTipoServ, Me.Anio, Me.Tipo, Me.DetaTipo, Me.CantidadAPagar, Me.NroLiberados, Me.Tipo_Lib, Me.CostoRealAnt, Me.Margen, Me.MargenAplicado, Me.MargenAplicadoAnt, Me.MargenLiberado, Me.DataGridViewTextBoxColumn1, Me.CostoRealImpto, Me.CostoLiberadoImpto, Me.MargenImpto, Me.MargenLiberadoImpto, Me.TotImpto, Me.IDDetRel, Me.Total, Me.IDVoucher, Me.IDReserva_DetCopia, Me.IDReserva_Det_Rel, Me.IDEmailEdit, Me.IDEmailNew, Me.IDEmailRef, Me.ObservVoucher, Me.ObservBiblia, Me.ObservInterno, Me.CapacidadHab, Me.EsMatrimonial, Me.PlanAlimenticio, Me.IncGuia, Me.chkServicioExportacion, Me.VerObserVoucher, Me.VerObserBiblia, Me.CostoReal, Me.CostoLiberado, Me.NetoHab, Me.IgvHab, Me.TotalHab, Me.NetoGen, Me.IgvGen, Me.IDMoneda, Me.SimboloMoneda, Me.TipoCambio, Me.TotalGen, Me.IDGuiaProveedor, Me.NuVehiculo, Me.idTipoOC, Me.InactivoDet, Me.ServAlimentPuro, Me.ConAlojamiento, Me.SSCR, Me.PNR, Me.CoEstadoTarifa, Me.NoEstadoTarifa, Me.IDReserva_DetOrigAcomVehi, Me.FlagOMEN, Me.FlagComparado, Me.chkRealizaAcc})
        DataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle5.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle5.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle5.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle5.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgvAntes.DefaultCellStyle = DataGridViewCellStyle5
        Me.dgvAntes.Location = New System.Drawing.Point(6, 31)
        Me.dgvAntes.Name = "dgvAntes"
        DataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle6.BackColor = System.Drawing.SystemColors.ControlDarkDark
        DataGridViewCellStyle6.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle6.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        DataGridViewCellStyle6.SelectionBackColor = System.Drawing.SystemColors.ControlDarkDark
        DataGridViewCellStyle6.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvAntes.RowHeadersDefaultCellStyle = DataGridViewCellStyle6
        Me.dgvAntes.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect
        Me.dgvAntes.Size = New System.Drawing.Size(972, 272)
        Me.dgvAntes.TabIndex = 7
        Me.dgvAntes.Tag = ""
        '
        'NuViaticoTC
        '
        Me.NuViaticoTC.HeaderText = "NuViaticoTC"
        Me.NuViaticoTC.Name = "NuViaticoTC"
        Me.NuViaticoTC.Visible = False
        '
        'IDReserva_Det
        '
        Me.IDReserva_Det.HeaderText = "IDReserva_Det"
        Me.IDReserva_Det.Name = "IDReserva_Det"
        Me.IDReserva_Det.Visible = False
        '
        'IDOperacion_Det
        '
        Me.IDOperacion_Det.HeaderText = "IDOperacion_Det"
        Me.IDOperacion_Det.Name = "IDOperacion_Det"
        Me.IDOperacion_Det.Visible = False
        '
        'IDReserva
        '
        Me.IDReserva.HeaderText = "IDReserva"
        Me.IDReserva.Name = "IDReserva"
        Me.IDReserva.Visible = False
        '
        'IDOperacion
        '
        Me.IDOperacion.HeaderText = "IDOperacion"
        Me.IDOperacion.Name = "IDOperacion"
        Me.IDOperacion.Visible = False
        '
        'IDFile
        '
        Me.IDFile.HeaderText = "IDFile"
        Me.IDFile.Name = "IDFile"
        Me.IDFile.Visible = False
        '
        'IDDet
        '
        Me.IDDet.HeaderText = "IDDet"
        Me.IDDet.Name = "IDDet"
        Me.IDDet.Visible = False
        '
        'Item
        '
        Me.Item.HeaderText = "Item"
        Me.Item.Name = "Item"
        Me.Item.Visible = False
        '
        'Dia
        '
        Me.Dia.HeaderText = "Día"
        Me.Dia.Name = "Dia"
        Me.Dia.Visible = False
        '
        'DiaFormat
        '
        Me.DiaFormat.HeaderText = "Fecha In"
        Me.DiaFormat.Name = "DiaFormat"
        Me.DiaFormat.ReadOnly = True
        '
        'FechaOut
        '
        Me.FechaOut.HeaderText = "Fecha Out"
        Me.FechaOut.Name = "FechaOut"
        Me.FechaOut.Visible = False
        '
        'FechaOutFormat
        '
        Me.FechaOutFormat.HeaderText = "Fecha Out"
        Me.FechaOutFormat.Name = "FechaOutFormat"
        Me.FechaOutFormat.ReadOnly = True
        '
        'Hora
        '
        Me.Hora.HeaderText = "Hora"
        Me.Hora.MaxInputLength = 5
        Me.Hora.Name = "Hora"
        Me.Hora.ReadOnly = True
        Me.Hora.Width = 40
        '
        'Ciudad
        '
        Me.Ciudad.HeaderText = "Ciudad"
        Me.Ciudad.Name = "Ciudad"
        Me.Ciudad.ReadOnly = True
        Me.Ciudad.Width = 50
        '
        'PaxMasLiberados
        '
        Me.PaxMasLiberados.HeaderText = "Pax"
        Me.PaxMasLiberados.Name = "PaxMasLiberados"
        Me.PaxMasLiberados.Width = 30
        '
        'Noches
        '
        Me.Noches.HeaderText = "Noc"
        Me.Noches.Name = "Noches"
        Me.Noches.ReadOnly = True
        Me.Noches.Width = 35
        '
        'IDTipoProv
        '
        Me.IDTipoProv.HeaderText = "IDTipoProv"
        Me.IDTipoProv.Name = "IDTipoProv"
        Me.IDTipoProv.Visible = False
        '
        'Dias
        '
        Me.Dias.HeaderText = "Dias"
        Me.Dias.Name = "Dias"
        Me.Dias.Visible = False
        '
        'IDProveedor
        '
        Me.IDProveedor.HeaderText = "IDProveedor"
        Me.IDProveedor.Name = "IDProveedor"
        Me.IDProveedor.Visible = False
        '
        'Proveedor
        '
        Me.Proveedor.HeaderText = "Proveedor"
        Me.Proveedor.Name = "Proveedor"
        Me.Proveedor.Visible = False
        '
        'CorreoProveedor
        '
        Me.CorreoProveedor.HeaderText = "CorreoProveedor"
        Me.CorreoProveedor.Name = "CorreoProveedor"
        Me.CorreoProveedor.Visible = False
        '
        'IDPais
        '
        Me.IDPais.HeaderText = "IDPais"
        Me.IDPais.Name = "IDPais"
        Me.IDPais.Visible = False
        '
        'IDubigeo
        '
        Me.IDubigeo.HeaderText = "IDubigeo"
        Me.IDubigeo.Name = "IDubigeo"
        Me.IDubigeo.Visible = False
        '
        'Cantidad
        '
        Me.Cantidad.HeaderText = "Cant"
        Me.Cantidad.Name = "Cantidad"
        Me.Cantidad.ReadOnly = True
        Me.Cantidad.Width = 40
        '
        'NroPax
        '
        Me.NroPax.HeaderText = "Pax"
        Me.NroPax.Name = "NroPax"
        Me.NroPax.ReadOnly = True
        Me.NroPax.Visible = False
        '
        'IDServicio
        '
        Me.IDServicio.HeaderText = "IDServicio"
        Me.IDServicio.Name = "IDServicio"
        Me.IDServicio.Visible = False
        '
        'IDServicio_Det
        '
        Me.IDServicio_Det.HeaderText = "IDServicio_Det"
        Me.IDServicio_Det.Name = "IDServicio_Det"
        Me.IDServicio_Det.Visible = False
        '
        'DescServicioDet
        '
        Me.DescServicioDet.HeaderText = "Servicio"
        Me.DescServicioDet.Name = "DescServicioDet"
        Me.DescServicioDet.ReadOnly = True
        Me.DescServicioDet.Width = 250
        '
        'DescServicioDetBup
        '
        Me.DescServicioDetBup.HeaderText = "DescServicioDetBup"
        Me.DescServicioDetBup.Name = "DescServicioDetBup"
        Me.DescServicioDetBup.Visible = False
        '
        'Especial
        '
        Me.Especial.HeaderText = "Especial"
        Me.Especial.Name = "Especial"
        Me.Especial.Visible = False
        '
        'MotivoEspecial
        '
        Me.MotivoEspecial.HeaderText = "MotivoEspecial"
        Me.MotivoEspecial.Name = "MotivoEspecial"
        Me.MotivoEspecial.Visible = False
        '
        'RutaDocSustento
        '
        Me.RutaDocSustento.HeaderText = "RutaDocSustento"
        Me.RutaDocSustento.Name = "RutaDocSustento"
        Me.RutaDocSustento.Visible = False
        '
        'Desayuno
        '
        Me.Desayuno.HeaderText = "Desayuno"
        Me.Desayuno.Name = "Desayuno"
        Me.Desayuno.Visible = False
        '
        'Lonche
        '
        Me.Lonche.HeaderText = "Lonche"
        Me.Lonche.Name = "Lonche"
        Me.Lonche.Visible = False
        '
        'Almuerzo
        '
        Me.Almuerzo.HeaderText = "Almuerzo"
        Me.Almuerzo.Name = "Almuerzo"
        Me.Almuerzo.Visible = False
        '
        'Cena
        '
        Me.Cena.HeaderText = "Cena"
        Me.Cena.Name = "Cena"
        Me.Cena.Visible = False
        '
        'Transfer
        '
        Me.Transfer.HeaderText = "Transfer"
        Me.Transfer.Name = "Transfer"
        Me.Transfer.Visible = False
        '
        'IDDetTransferOri
        '
        Me.IDDetTransferOri.HeaderText = "IDDetTransferOri"
        Me.IDDetTransferOri.Name = "IDDetTransferOri"
        Me.IDDetTransferOri.Visible = False
        '
        'IDDetTransferDes
        '
        Me.IDDetTransferDes.HeaderText = "IDDetTransferDes"
        Me.IDDetTransferDes.Name = "IDDetTransferDes"
        Me.IDDetTransferDes.Visible = False
        '
        'CamaMat
        '
        Me.CamaMat.HeaderText = "CamaMat"
        Me.CamaMat.Name = "CamaMat"
        Me.CamaMat.Visible = False
        '
        'TipoTransporte
        '
        Me.TipoTransporte.HeaderText = "TipoTransporte"
        Me.TipoTransporte.Name = "TipoTransporte"
        Me.TipoTransporte.Visible = False
        '
        'IDUbigeoOri
        '
        Me.IDUbigeoOri.HeaderText = "IDUbigeoOri"
        Me.IDUbigeoOri.Name = "IDUbigeoOri"
        Me.IDUbigeoOri.Visible = False
        '
        'DescUbigeoOri
        '
        Me.DescUbigeoOri.HeaderText = "DescUbigeoOri"
        Me.DescUbigeoOri.Name = "DescUbigeoOri"
        Me.DescUbigeoOri.Visible = False
        '
        'IDUbigeoDes
        '
        Me.IDUbigeoDes.HeaderText = "IDUbigeoDes"
        Me.IDUbigeoDes.Name = "IDUbigeoDes"
        Me.IDUbigeoDes.Visible = False
        '
        'DescUbigeoDes
        '
        Me.DescUbigeoDes.HeaderText = "DescUbigeoDes"
        Me.DescUbigeoDes.Name = "DescUbigeoDes"
        Me.DescUbigeoDes.Visible = False
        '
        'IDIdioma
        '
        Me.IDIdioma.HeaderText = "Idioma"
        Me.IDIdioma.Name = "IDIdioma"
        '
        'IDTipoServ
        '
        Me.IDTipoServ.HeaderText = "IDTipoServ"
        Me.IDTipoServ.Name = "IDTipoServ"
        Me.IDTipoServ.ReadOnly = True
        Me.IDTipoServ.Visible = False
        Me.IDTipoServ.Width = 50
        '
        'Anio
        '
        Me.Anio.HeaderText = "Año"
        Me.Anio.Name = "Anio"
        Me.Anio.ReadOnly = True
        Me.Anio.Width = 35
        '
        'Tipo
        '
        Me.Tipo.HeaderText = "Var"
        Me.Tipo.Name = "Tipo"
        Me.Tipo.ReadOnly = True
        Me.Tipo.Width = 30
        '
        'DetaTipo
        '
        Me.DetaTipo.HeaderText = "DetaTipo"
        Me.DetaTipo.Name = "DetaTipo"
        Me.DetaTipo.Visible = False
        '
        'CantidadAPagar
        '
        Me.CantidadAPagar.HeaderText = "Cant. a Pagar"
        Me.CantidadAPagar.Name = "CantidadAPagar"
        Me.CantidadAPagar.ReadOnly = True
        Me.CantidadAPagar.Width = 50
        '
        'NroLiberados
        '
        Me.NroLiberados.HeaderText = "NroLiberados"
        Me.NroLiberados.Name = "NroLiberados"
        Me.NroLiberados.Visible = False
        '
        'Tipo_Lib
        '
        Me.Tipo_Lib.HeaderText = "Tipo_Lib"
        Me.Tipo_Lib.Name = "Tipo_Lib"
        Me.Tipo_Lib.Visible = False
        '
        'CostoRealAnt
        '
        Me.CostoRealAnt.HeaderText = "CostoRealAnt"
        Me.CostoRealAnt.Name = "CostoRealAnt"
        Me.CostoRealAnt.Visible = False
        '
        'Margen
        '
        Me.Margen.HeaderText = "Margen"
        Me.Margen.Name = "Margen"
        Me.Margen.Visible = False
        '
        'MargenAplicado
        '
        Me.MargenAplicado.HeaderText = "MargenAplicado"
        Me.MargenAplicado.Name = "MargenAplicado"
        Me.MargenAplicado.Visible = False
        '
        'MargenAplicadoAnt
        '
        Me.MargenAplicadoAnt.HeaderText = "MargenAplicadoAnt"
        Me.MargenAplicadoAnt.Name = "MargenAplicadoAnt"
        Me.MargenAplicadoAnt.Visible = False
        '
        'MargenLiberado
        '
        Me.MargenLiberado.HeaderText = "MargenLiberado"
        Me.MargenLiberado.Name = "MargenLiberado"
        Me.MargenLiberado.Visible = False
        '
        'DataGridViewTextBoxColumn1
        '
        Me.DataGridViewTextBoxColumn1.HeaderText = "TotalOrig"
        Me.DataGridViewTextBoxColumn1.Name = "DataGridViewTextBoxColumn1"
        Me.DataGridViewTextBoxColumn1.Visible = False
        '
        'CostoRealImpto
        '
        Me.CostoRealImpto.HeaderText = "CostoRealImpto"
        Me.CostoRealImpto.Name = "CostoRealImpto"
        Me.CostoRealImpto.Visible = False
        '
        'CostoLiberadoImpto
        '
        Me.CostoLiberadoImpto.HeaderText = "CostoLiberadoImpto"
        Me.CostoLiberadoImpto.Name = "CostoLiberadoImpto"
        Me.CostoLiberadoImpto.Visible = False
        '
        'MargenImpto
        '
        Me.MargenImpto.HeaderText = "MargenImpto"
        Me.MargenImpto.Name = "MargenImpto"
        Me.MargenImpto.Visible = False
        '
        'MargenLiberadoImpto
        '
        Me.MargenLiberadoImpto.HeaderText = "MargenLiberadoImpto"
        Me.MargenLiberadoImpto.Name = "MargenLiberadoImpto"
        Me.MargenLiberadoImpto.Visible = False
        '
        'TotImpto
        '
        Me.TotImpto.HeaderText = "TotImpto"
        Me.TotImpto.Name = "TotImpto"
        Me.TotImpto.Visible = False
        '
        'IDDetRel
        '
        Me.IDDetRel.HeaderText = "IDDetRel"
        Me.IDDetRel.Name = "IDDetRel"
        Me.IDDetRel.Visible = False
        '
        'Total
        '
        Me.Total.HeaderText = "Total"
        Me.Total.Name = "Total"
        Me.Total.ReadOnly = True
        Me.Total.Visible = False
        '
        'IDVoucher
        '
        Me.IDVoucher.HeaderText = "Nro. Voucher"
        Me.IDVoucher.Name = "IDVoucher"
        Me.IDVoucher.ReadOnly = True
        Me.IDVoucher.Visible = False
        '
        'IDReserva_DetCopia
        '
        Me.IDReserva_DetCopia.HeaderText = "IDReserva_DetCopia"
        Me.IDReserva_DetCopia.Name = "IDReserva_DetCopia"
        Me.IDReserva_DetCopia.Visible = False
        '
        'IDReserva_Det_Rel
        '
        Me.IDReserva_Det_Rel.HeaderText = "IDReserva_Det_Rel"
        Me.IDReserva_Det_Rel.Name = "IDReserva_Det_Rel"
        Me.IDReserva_Det_Rel.Visible = False
        '
        'IDEmailEdit
        '
        Me.IDEmailEdit.HeaderText = "IDEmailEdit"
        Me.IDEmailEdit.Name = "IDEmailEdit"
        Me.IDEmailEdit.Visible = False
        '
        'IDEmailNew
        '
        Me.IDEmailNew.HeaderText = "IDEmailNew"
        Me.IDEmailNew.Name = "IDEmailNew"
        Me.IDEmailNew.Visible = False
        '
        'IDEmailRef
        '
        Me.IDEmailRef.HeaderText = "IDEmailRef"
        Me.IDEmailRef.Name = "IDEmailRef"
        Me.IDEmailRef.Visible = False
        '
        'ObservVoucher
        '
        Me.ObservVoucher.HeaderText = "ObservVoucher"
        Me.ObservVoucher.Name = "ObservVoucher"
        Me.ObservVoucher.Visible = False
        '
        'ObservBiblia
        '
        Me.ObservBiblia.HeaderText = "ObservBiblia"
        Me.ObservBiblia.Name = "ObservBiblia"
        Me.ObservBiblia.Visible = False
        '
        'ObservInterno
        '
        Me.ObservInterno.HeaderText = "ObservInterno"
        Me.ObservInterno.Name = "ObservInterno"
        Me.ObservInterno.Visible = False
        '
        'CapacidadHab
        '
        Me.CapacidadHab.HeaderText = "CapacidadHab"
        Me.CapacidadHab.Name = "CapacidadHab"
        Me.CapacidadHab.Visible = False
        '
        'EsMatrimonial
        '
        Me.EsMatrimonial.HeaderText = "EsMatrimonial"
        Me.EsMatrimonial.Name = "EsMatrimonial"
        Me.EsMatrimonial.Visible = False
        '
        'PlanAlimenticio
        '
        Me.PlanAlimenticio.HeaderText = "PlanAlimenticio"
        Me.PlanAlimenticio.Name = "PlanAlimenticio"
        Me.PlanAlimenticio.Visible = False
        '
        'IncGuia
        '
        Me.IncGuia.HeaderText = "IncGuia"
        Me.IncGuia.Name = "IncGuia"
        Me.IncGuia.Visible = False
        '
        'chkServicioExportacion
        '
        Me.chkServicioExportacion.HeaderText = "Exp. de Servicio"
        Me.chkServicioExportacion.Name = "chkServicioExportacion"
        Me.chkServicioExportacion.ReadOnly = True
        Me.chkServicioExportacion.Visible = False
        '
        'VerObserVoucher
        '
        Me.VerObserVoucher.HeaderText = "Ver Voucher"
        Me.VerObserVoucher.Name = "VerObserVoucher"
        Me.VerObserVoucher.ReadOnly = True
        Me.VerObserVoucher.Visible = False
        '
        'VerObserBiblia
        '
        Me.VerObserBiblia.HeaderText = "Ver Biblia"
        Me.VerObserBiblia.Name = "VerObserBiblia"
        Me.VerObserBiblia.ReadOnly = True
        Me.VerObserBiblia.Visible = False
        '
        'CostoReal
        '
        Me.CostoReal.HeaderText = "CostoReal"
        Me.CostoReal.Name = "CostoReal"
        Me.CostoReal.ReadOnly = True
        Me.CostoReal.Visible = False
        '
        'CostoLiberado
        '
        Me.CostoLiberado.HeaderText = "CostoLiberado"
        Me.CostoLiberado.Name = "CostoLiberado"
        Me.CostoLiberado.ReadOnly = True
        Me.CostoLiberado.Visible = False
        '
        'NetoHab
        '
        Me.NetoHab.HeaderText = "Neto Hab/Pax"
        Me.NetoHab.Name = "NetoHab"
        Me.NetoHab.ReadOnly = True
        Me.NetoHab.Width = 50
        '
        'IgvHab
        '
        Me.IgvHab.HeaderText = "Igv Hab/Pax"
        Me.IgvHab.Name = "IgvHab"
        Me.IgvHab.ReadOnly = True
        Me.IgvHab.Width = 30
        '
        'TotalHab
        '
        Me.TotalHab.HeaderText = "Total Hab/Pax"
        Me.TotalHab.Name = "TotalHab"
        Me.TotalHab.ReadOnly = True
        Me.TotalHab.Width = 60
        '
        'NetoGen
        '
        Me.NetoGen.HeaderText = "Neto Total"
        Me.NetoGen.Name = "NetoGen"
        Me.NetoGen.ReadOnly = True
        Me.NetoGen.Width = 60
        '
        'IgvGen
        '
        Me.IgvGen.HeaderText = "Igv Total"
        Me.IgvGen.Name = "IgvGen"
        Me.IgvGen.ReadOnly = True
        Me.IgvGen.Width = 30
        '
        'IDMoneda
        '
        Me.IDMoneda.HeaderText = "IDMoneda"
        Me.IDMoneda.Name = "IDMoneda"
        Me.IDMoneda.Visible = False
        '
        'SimboloMoneda
        '
        Me.SimboloMoneda.HeaderText = "Mon"
        Me.SimboloMoneda.Name = "SimboloMoneda"
        Me.SimboloMoneda.Width = 30
        '
        'TipoCambio
        '
        Me.TipoCambio.HeaderText = "TipoCambio"
        Me.TipoCambio.Name = "TipoCambio"
        Me.TipoCambio.Visible = False
        '
        'TotalGen
        '
        Me.TotalGen.HeaderText = "Total"
        Me.TotalGen.Name = "TotalGen"
        Me.TotalGen.ReadOnly = True
        Me.TotalGen.Width = 50
        '
        'IDGuiaProveedor
        '
        Me.IDGuiaProveedor.HeaderText = "IDGuiaProveedor"
        Me.IDGuiaProveedor.Name = "IDGuiaProveedor"
        Me.IDGuiaProveedor.ReadOnly = True
        Me.IDGuiaProveedor.Visible = False
        '
        'NuVehiculo
        '
        Me.NuVehiculo.HeaderText = "DescBus"
        Me.NuVehiculo.Name = "NuVehiculo"
        Me.NuVehiculo.ReadOnly = True
        Me.NuVehiculo.Visible = False
        '
        'idTipoOC
        '
        Me.idTipoOC.HeaderText = "idTipoOC"
        Me.idTipoOC.Name = "idTipoOC"
        Me.idTipoOC.Visible = False
        '
        'InactivoDet
        '
        Me.InactivoDet.HeaderText = "InactivoDet"
        Me.InactivoDet.Name = "InactivoDet"
        Me.InactivoDet.Visible = False
        '
        'ServAlimentPuro
        '
        Me.ServAlimentPuro.HeaderText = "ServAlimentPuro"
        Me.ServAlimentPuro.Name = "ServAlimentPuro"
        Me.ServAlimentPuro.Visible = False
        '
        'ConAlojamiento
        '
        Me.ConAlojamiento.HeaderText = "ConAlojamiento"
        Me.ConAlojamiento.Name = "ConAlojamiento"
        Me.ConAlojamiento.Visible = False
        '
        'SSCR
        '
        Me.SSCR.HeaderText = "SSCR"
        Me.SSCR.Name = "SSCR"
        Me.SSCR.Visible = False
        '
        'PNR
        '
        Me.PNR.HeaderText = "PNR"
        Me.PNR.Name = "PNR"
        Me.PNR.Visible = False
        '
        'CoEstadoTarifa
        '
        Me.CoEstadoTarifa.HeaderText = "CoEstadoTarifa"
        Me.CoEstadoTarifa.Name = "CoEstadoTarifa"
        Me.CoEstadoTarifa.Visible = False
        '
        'NoEstadoTarifa
        '
        Me.NoEstadoTarifa.HeaderText = "Estado Tarifa"
        Me.NoEstadoTarifa.Name = "NoEstadoTarifa"
        Me.NoEstadoTarifa.ReadOnly = True
        '
        'IDReserva_DetOrigAcomVehi
        '
        Me.IDReserva_DetOrigAcomVehi.HeaderText = "IDReserva_DetOrigAcomVehi"
        Me.IDReserva_DetOrigAcomVehi.Name = "IDReserva_DetOrigAcomVehi"
        Me.IDReserva_DetOrigAcomVehi.Visible = False
        '
        'FlagOMEN
        '
        Me.FlagOMEN.HeaderText = "Acción"
        Me.FlagOMEN.Name = "FlagOMEN"
        Me.FlagOMEN.ReadOnly = True
        '
        'FlagComparado
        '
        Me.FlagComparado.HeaderText = "FlagComparado"
        Me.FlagComparado.Name = "FlagComparado"
        Me.FlagComparado.Visible = False
        '
        'chkRealizaAcc
        '
        Me.chkRealizaAcc.HeaderText = ""
        Me.chkRealizaAcc.Name = "chkRealizaAcc"
        Me.chkRealizaAcc.Width = 21
        '
        'lblTituloAntes
        '
        Me.lblTituloAntes.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblTituloAntes.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.lblTituloAntes.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTituloAntes.Location = New System.Drawing.Point(6, 11)
        Me.lblTituloAntes.Name = "lblTituloAntes"
        Me.lblTituloAntes.Size = New System.Drawing.Size(972, 16)
        Me.lblTituloAntes.TabIndex = 6
        Me.lblTituloAntes.Text = "Actualmente..."
        '
        'lblTituloDespues
        '
        Me.lblTituloDespues.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblTituloDespues.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.lblTituloDespues.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTituloDespues.Location = New System.Drawing.Point(6, 306)
        Me.lblTituloDespues.Name = "lblTituloDespues"
        Me.lblTituloDespues.Size = New System.Drawing.Size(918, 16)
        Me.lblTituloDespues.TabIndex = 5
        Me.lblTituloDespues.Text = "Por Actualizar..."
        '
        'Label2
        '
        Me.Label2.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.Label2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(60, 306)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(918, 16)
        Me.Label2.TabIndex = 4
        Me.Label2.Text = "Cambios (Nuevo, Modificado, Eliminado)"
        Me.Label2.Visible = False
        '
        'Label1
        '
        Me.Label1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.Label1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(60, 11)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(972, 16)
        Me.Label1.TabIndex = 3
        Me.Label1.Text = "Original"
        Me.Label1.Visible = False
        '
        'dgvDetModificado
        '
        Me.dgvDetModificado.AllowUserToAddRows = False
        Me.dgvDetModificado.AllowUserToDeleteRows = False
        Me.dgvDetModificado.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        DataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle7.BackColor = System.Drawing.SystemColors.ControlDarkDark
        DataGridViewCellStyle7.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle7.ForeColor = System.Drawing.Color.White
        DataGridViewCellStyle7.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle7.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle7.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvDetModificado.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle7
        Me.dgvDetModificado.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvDetModificado.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.FechaLogMod, Me.UsuarioMod, Me.AccionMod, Me.IDDetMod, Me.FechaMod, Me.UbigeoMod, Me.ServicioMod, Me.ProveedorMod, Me.TipoServMod, Me.PaxMod, Me.IDIdiomaMod, Me.AnioMod, Me.VarianteMod, Me.TotalMod, Me.AtendidoMod, Me.DescAtendidoMod})
        DataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle8.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle8.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle8.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle8.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle8.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle8.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgvDetModificado.DefaultCellStyle = DataGridViewCellStyle8
        Me.dgvDetModificado.Location = New System.Drawing.Point(6, 325)
        Me.dgvDetModificado.Name = "dgvDetModificado"
        Me.dgvDetModificado.ReadOnly = True
        DataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle9.BackColor = System.Drawing.SystemColors.ControlDarkDark
        DataGridViewCellStyle9.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle9.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        DataGridViewCellStyle9.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle9.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle9.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvDetModificado.RowHeadersDefaultCellStyle = DataGridViewCellStyle9
        Me.dgvDetModificado.Size = New System.Drawing.Size(972, 281)
        Me.dgvDetModificado.TabIndex = 2
        Me.dgvDetModificado.Visible = False
        '
        'FechaLogMod
        '
        Me.FechaLogMod.HeaderText = "Fecha Log"
        Me.FechaLogMod.Name = "FechaLogMod"
        Me.FechaLogMod.ReadOnly = True
        '
        'UsuarioMod
        '
        Me.UsuarioMod.HeaderText = "Usuario"
        Me.UsuarioMod.Name = "UsuarioMod"
        Me.UsuarioMod.ReadOnly = True
        '
        'AccionMod
        '
        Me.AccionMod.HeaderText = "Acción"
        Me.AccionMod.Name = "AccionMod"
        Me.AccionMod.ReadOnly = True
        '
        'IDDetMod
        '
        Me.IDDetMod.HeaderText = "IDDet"
        Me.IDDetMod.Name = "IDDetMod"
        Me.IDDetMod.ReadOnly = True
        Me.IDDetMod.Visible = False
        '
        'FechaMod
        '
        Me.FechaMod.HeaderText = "Fecha"
        Me.FechaMod.Name = "FechaMod"
        Me.FechaMod.ReadOnly = True
        Me.FechaMod.Width = 120
        '
        'UbigeoMod
        '
        Me.UbigeoMod.HeaderText = "Ubigeo"
        Me.UbigeoMod.Name = "UbigeoMod"
        Me.UbigeoMod.ReadOnly = True
        '
        'ServicioMod
        '
        Me.ServicioMod.HeaderText = "Servicio"
        Me.ServicioMod.Name = "ServicioMod"
        Me.ServicioMod.ReadOnly = True
        '
        'ProveedorMod
        '
        Me.ProveedorMod.HeaderText = "Proveedor"
        Me.ProveedorMod.Name = "ProveedorMod"
        Me.ProveedorMod.ReadOnly = True
        '
        'TipoServMod
        '
        Me.TipoServMod.HeaderText = "Tipo Servicio"
        Me.TipoServMod.Name = "TipoServMod"
        Me.TipoServMod.ReadOnly = True
        '
        'PaxMod
        '
        Me.PaxMod.HeaderText = "Pax"
        Me.PaxMod.Name = "PaxMod"
        Me.PaxMod.ReadOnly = True
        '
        'IDIdiomaMod
        '
        Me.IDIdiomaMod.HeaderText = "Idioma"
        Me.IDIdiomaMod.Name = "IDIdiomaMod"
        Me.IDIdiomaMod.ReadOnly = True
        '
        'AnioMod
        '
        Me.AnioMod.HeaderText = "Año"
        Me.AnioMod.Name = "AnioMod"
        Me.AnioMod.ReadOnly = True
        '
        'VarianteMod
        '
        Me.VarianteMod.HeaderText = "Variante Servicio"
        Me.VarianteMod.Name = "VarianteMod"
        Me.VarianteMod.ReadOnly = True
        '
        'TotalMod
        '
        Me.TotalMod.HeaderText = "Total"
        Me.TotalMod.Name = "TotalMod"
        Me.TotalMod.ReadOnly = True
        '
        'AtendidoMod
        '
        Me.AtendidoMod.HeaderText = "Atendido"
        Me.AtendidoMod.Name = "AtendidoMod"
        Me.AtendidoMod.ReadOnly = True
        Me.AtendidoMod.Visible = False
        '
        'DescAtendidoMod
        '
        Me.DescAtendidoMod.HeaderText = "Atendido"
        Me.DescAtendidoMod.Name = "DescAtendidoMod"
        Me.DescAtendidoMod.ReadOnly = True
        '
        'dgvDetOriginal
        '
        Me.dgvDetOriginal.AllowUserToAddRows = False
        Me.dgvDetOriginal.AllowUserToDeleteRows = False
        Me.dgvDetOriginal.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        DataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle10.BackColor = System.Drawing.SystemColors.ControlDarkDark
        DataGridViewCellStyle10.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle10.ForeColor = System.Drawing.Color.White
        DataGridViewCellStyle10.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle10.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle10.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvDetOriginal.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle10
        Me.dgvDetOriginal.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvDetOriginal.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.FechaLogOrig, Me.UsuarioOrig, Me.AccionOrig, Me.IDDetOrig, Me.DiaFormatOrig, Me.DescCiudadOrig, Me.DescServicioDetOrig, Me.DescProveedorOrig, Me.DescTipoServOrig, Me.NroPaxOrig, Me.IDIdiomaOrig, Me.AnioOrig, Me.TipoOrig, Me.TotalOrig, Me.AtendidoOrig, Me.DescAtendidoOrig})
        DataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle11.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle11.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle11.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle11.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle11.SelectionForeColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle11.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgvDetOriginal.DefaultCellStyle = DataGridViewCellStyle11
        Me.dgvDetOriginal.Location = New System.Drawing.Point(232, 31)
        Me.dgvDetOriginal.Name = "dgvDetOriginal"
        Me.dgvDetOriginal.ReadOnly = True
        DataGridViewCellStyle12.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle12.BackColor = System.Drawing.SystemColors.ControlDarkDark
        DataGridViewCellStyle12.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle12.ForeColor = System.Drawing.Color.White
        DataGridViewCellStyle12.SelectionBackColor = System.Drawing.SystemColors.ControlDarkDark
        DataGridViewCellStyle12.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle12.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvDetOriginal.RowHeadersDefaultCellStyle = DataGridViewCellStyle12
        Me.dgvDetOriginal.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect
        Me.dgvDetOriginal.Size = New System.Drawing.Size(746, 272)
        Me.dgvDetOriginal.TabIndex = 1
        '
        'FechaLogOrig
        '
        Me.FechaLogOrig.HeaderText = "Fecha Log"
        Me.FechaLogOrig.Name = "FechaLogOrig"
        Me.FechaLogOrig.ReadOnly = True
        Me.FechaLogOrig.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'UsuarioOrig
        '
        Me.UsuarioOrig.HeaderText = "Usuario"
        Me.UsuarioOrig.Name = "UsuarioOrig"
        Me.UsuarioOrig.ReadOnly = True
        Me.UsuarioOrig.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'AccionOrig
        '
        Me.AccionOrig.HeaderText = "Acción"
        Me.AccionOrig.Name = "AccionOrig"
        Me.AccionOrig.ReadOnly = True
        Me.AccionOrig.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'IDDetOrig
        '
        Me.IDDetOrig.HeaderText = "IDDet"
        Me.IDDetOrig.Name = "IDDetOrig"
        Me.IDDetOrig.ReadOnly = True
        Me.IDDetOrig.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.IDDetOrig.Visible = False
        '
        'DiaFormatOrig
        '
        Me.DiaFormatOrig.HeaderText = "Fecha"
        Me.DiaFormatOrig.Name = "DiaFormatOrig"
        Me.DiaFormatOrig.ReadOnly = True
        Me.DiaFormatOrig.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DiaFormatOrig.Width = 120
        '
        'DescCiudadOrig
        '
        Me.DescCiudadOrig.HeaderText = "Ubigeo"
        Me.DescCiudadOrig.Name = "DescCiudadOrig"
        Me.DescCiudadOrig.ReadOnly = True
        Me.DescCiudadOrig.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'DescServicioDetOrig
        '
        Me.DescServicioDetOrig.HeaderText = "Servicio"
        Me.DescServicioDetOrig.Name = "DescServicioDetOrig"
        Me.DescServicioDetOrig.ReadOnly = True
        Me.DescServicioDetOrig.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'DescProveedorOrig
        '
        Me.DescProveedorOrig.HeaderText = "Proveedor"
        Me.DescProveedorOrig.Name = "DescProveedorOrig"
        Me.DescProveedorOrig.ReadOnly = True
        Me.DescProveedorOrig.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'DescTipoServOrig
        '
        Me.DescTipoServOrig.HeaderText = "Tipo Servicio"
        Me.DescTipoServOrig.Name = "DescTipoServOrig"
        Me.DescTipoServOrig.ReadOnly = True
        Me.DescTipoServOrig.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'NroPaxOrig
        '
        Me.NroPaxOrig.HeaderText = "Pax"
        Me.NroPaxOrig.Name = "NroPaxOrig"
        Me.NroPaxOrig.ReadOnly = True
        Me.NroPaxOrig.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'IDIdiomaOrig
        '
        Me.IDIdiomaOrig.HeaderText = "Idioma"
        Me.IDIdiomaOrig.Name = "IDIdiomaOrig"
        Me.IDIdiomaOrig.ReadOnly = True
        Me.IDIdiomaOrig.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'AnioOrig
        '
        Me.AnioOrig.HeaderText = "Año"
        Me.AnioOrig.Name = "AnioOrig"
        Me.AnioOrig.ReadOnly = True
        Me.AnioOrig.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'TipoOrig
        '
        Me.TipoOrig.HeaderText = "Variante Servicio"
        Me.TipoOrig.Name = "TipoOrig"
        Me.TipoOrig.ReadOnly = True
        Me.TipoOrig.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'TotalOrig
        '
        Me.TotalOrig.HeaderText = "Total"
        Me.TotalOrig.Name = "TotalOrig"
        Me.TotalOrig.ReadOnly = True
        Me.TotalOrig.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'AtendidoOrig
        '
        Me.AtendidoOrig.HeaderText = "Atendido"
        Me.AtendidoOrig.Name = "AtendidoOrig"
        Me.AtendidoOrig.ReadOnly = True
        Me.AtendidoOrig.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.AtendidoOrig.Visible = False
        '
        'DescAtendidoOrig
        '
        Me.DescAtendidoOrig.HeaderText = "Atendido"
        Me.DescAtendidoOrig.Name = "DescAtendidoOrig"
        Me.DescAtendidoOrig.ReadOnly = True
        Me.DescAtendidoOrig.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'btnModificarReservas
        '
        Me.btnModificarReservas.Enabled = False
        Me.btnModificarReservas.Image = Global.SETours.My.Resources.Resources.page_gear
        Me.btnModificarReservas.Location = New System.Drawing.Point(5, 525)
        Me.btnModificarReservas.Name = "btnModificarReservas"
        Me.btnModificarReservas.Size = New System.Drawing.Size(27, 27)
        Me.btnModificarReservas.TabIndex = 261
        Me.ToolTip1.SetToolTip(Me.btnModificarReservas, "Modificar Reserva")
        Me.btnModificarReservas.UseVisualStyleBackColor = True
        Me.btnModificarReservas.Visible = False
        '
        'btnCrearCorreo
        '
        Me.btnCrearCorreo.Enabled = False
        Me.btnCrearCorreo.Image = Global.SETours.My.Resources.Resources.email_edit
        Me.btnCrearCorreo.Location = New System.Drawing.Point(5, 558)
        Me.btnCrearCorreo.Name = "btnCrearCorreo"
        Me.btnCrearCorreo.Size = New System.Drawing.Size(27, 27)
        Me.btnCrearCorreo.TabIndex = 262
        Me.ToolTip1.SetToolTip(Me.btnCrearCorreo, "Generar Correo")
        Me.btnCrearCorreo.UseVisualStyleBackColor = True
        Me.btnCrearCorreo.Visible = False
        '
        'btnAceptar
        '
        Me.btnAceptar.Enabled = False
        Me.btnAceptar.Image = Global.SETours.My.Resources.Resources.disk
        Me.btnAceptar.Location = New System.Drawing.Point(5, 5)
        Me.btnAceptar.Name = "btnAceptar"
        Me.btnAceptar.Size = New System.Drawing.Size(27, 27)
        Me.btnAceptar.TabIndex = 263
        Me.ToolTip1.SetToolTip(Me.btnAceptar, "Actualizar cambios de Ventas a Reserva (F6)")
        Me.btnAceptar.UseVisualStyleBackColor = True
        '
        'btnSalir
        '
        Me.btnSalir.Image = Global.SETours.My.Resources.Resources.door_out
        Me.btnSalir.Location = New System.Drawing.Point(5, 35)
        Me.btnSalir.Name = "btnSalir"
        Me.btnSalir.Size = New System.Drawing.Size(27, 27)
        Me.btnSalir.TabIndex = 264
        Me.ToolTip1.SetToolTip(Me.btnSalir, "Salir (ESC)")
        Me.btnSalir.UseVisualStyleBackColor = True
        '
        'frmConsLogCotizaciones
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1020, 612)
        Me.Controls.Add(Me.btnSalir)
        Me.Controls.Add(Me.btnAceptar)
        Me.Controls.Add(Me.btnCrearCorreo)
        Me.Controls.Add(Me.btnModificarReservas)
        Me.Controls.Add(Me.grb)
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmConsLogCotizaciones"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Consulta de Log Cotizaciones"
        Me.grb.ResumeLayout(False)
        CType(Me.dgvDespues, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvAntes, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvDetModificado, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvDetOriginal, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents grb As System.Windows.Forms.GroupBox
    Friend WithEvents dgvDetOriginal As System.Windows.Forms.DataGridView
    Friend WithEvents btnModificarReservas As System.Windows.Forms.Button
    Friend WithEvents ToolTip1 As System.Windows.Forms.ToolTip
    Friend WithEvents btnCrearCorreo As System.Windows.Forms.Button
    Friend WithEvents dgvDetModificado As System.Windows.Forms.DataGridView
    Friend WithEvents btnAceptar As System.Windows.Forms.Button
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents btnSalir As System.Windows.Forms.Button
    Friend WithEvents FechaLogMod As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents UsuarioMod As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents AccionMod As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents IDDetMod As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents FechaMod As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents UbigeoMod As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ServicioMod As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ProveedorMod As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents TipoServMod As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents PaxMod As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents IDIdiomaMod As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents AnioMod As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents VarianteMod As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents TotalMod As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents AtendidoMod As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DescAtendidoMod As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents FechaLogOrig As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents UsuarioOrig As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents AccionOrig As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents IDDetOrig As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DiaFormatOrig As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DescCiudadOrig As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DescServicioDetOrig As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DescProveedorOrig As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DescTipoServOrig As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents NroPaxOrig As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents IDIdiomaOrig As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents AnioOrig As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents TipoOrig As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents TotalOrig As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents AtendidoOrig As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DescAtendidoOrig As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents lblTituloAntes As System.Windows.Forms.Label
    Friend WithEvents lblTituloDespues As System.Windows.Forms.Label
    Friend WithEvents dgvAntes As System.Windows.Forms.DataGridView
    Friend WithEvents dgvDespues As System.Windows.Forms.DataGridView
    Friend WithEvents NuViaticoTC As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents IDReserva_Det As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents IDOperacion_Det As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents IDReserva As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents IDOperacion As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents IDFile As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents IDDet As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Item As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Dia As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DiaFormat As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents FechaOut As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents FechaOutFormat As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Hora As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Ciudad As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents PaxMasLiberados As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Noches As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents IDTipoProv As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Dias As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents IDProveedor As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Proveedor As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents CorreoProveedor As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents IDPais As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents IDubigeo As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Cantidad As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents NroPax As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents IDServicio As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents IDServicio_Det As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DescServicioDet As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DescServicioDetBup As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Especial As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents MotivoEspecial As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents RutaDocSustento As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Desayuno As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Lonche As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Almuerzo As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Cena As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Transfer As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents IDDetTransferOri As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents IDDetTransferDes As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents CamaMat As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents TipoTransporte As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents IDUbigeoOri As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DescUbigeoOri As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents IDUbigeoDes As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DescUbigeoDes As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents IDIdioma As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents IDTipoServ As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Anio As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Tipo As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DetaTipo As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents CantidadAPagar As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents NroLiberados As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Tipo_Lib As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents CostoRealAnt As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Margen As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents MargenAplicado As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents MargenAplicadoAnt As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents MargenLiberado As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents CostoRealImpto As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents CostoLiberadoImpto As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents MargenImpto As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents MargenLiberadoImpto As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents TotImpto As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents IDDetRel As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Total As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents IDVoucher As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents IDReserva_DetCopia As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents IDReserva_Det_Rel As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents IDEmailEdit As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents IDEmailNew As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents IDEmailRef As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ObservVoucher As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ObservBiblia As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ObservInterno As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents CapacidadHab As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents EsMatrimonial As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents PlanAlimenticio As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents IncGuia As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents chkServicioExportacion As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents VerObserVoucher As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents VerObserBiblia As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents CostoReal As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents CostoLiberado As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents NetoHab As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents IgvHab As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents TotalHab As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents NetoGen As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents IgvGen As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents IDMoneda As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents SimboloMoneda As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents TipoCambio As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents TotalGen As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents IDGuiaProveedor As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents NuVehiculo As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents idTipoOC As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents InactivoDet As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ServAlimentPuro As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ConAlojamiento As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents SSCR As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents PNR As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents CoEstadoTarifa As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents NoEstadoTarifa As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents IDReserva_DetOrigAcomVehi As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents FlagOMEN As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents FlagComparado As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents chkRealizaAcc As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents NuViaticoTC2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents IDReserva_Det2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents IDOperacion_Det2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn4 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn5 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn6 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents IDDet2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn8 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Dia2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DiaFormat2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn11 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents FechaOutFormat2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Hora2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn14 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn25 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn15 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn16 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn17 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn18 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn19 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn20 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn21 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn22 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn23 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn24 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn26 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents IDServicio_Det2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DescServicioDet2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn29 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn30 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn31 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn32 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn33 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn34 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn35 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn36 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn37 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn38 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn39 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn40 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn41 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn42 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn43 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn44 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn45 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn46 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn47 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn48 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn49 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn50 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn51 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn52 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn53 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn54 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn55 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn56 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn57 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn58 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn59 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn60 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn61 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn62 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn63 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn64 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn65 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn66 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn67 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn68 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn69 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn70 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn71 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn72 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn73 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn74 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn75 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents CapacidadHab2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents EsMatrimonial2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn78 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn79 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewCheckBoxColumn1 As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents DataGridViewCheckBoxColumn2 As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents DataGridViewCheckBoxColumn3 As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn80 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn81 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn82 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn83 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn84 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn85 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn86 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn87 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn88 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn89 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn90 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn91 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn92 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn93 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn94 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn95 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn96 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn97 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn98 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents CoEstadoTarifa2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents NoEstadoTarifa2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents IDReserva_DetOrigAcomVehi2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents FlagOMEN2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents FlagComparado2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents chkRealizarAcc2 As System.Windows.Forms.DataGridViewCheckBoxColumn
End Class
