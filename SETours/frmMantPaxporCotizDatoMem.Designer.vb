﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmMantPaxporCotizDatoMem
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.grb = New System.Windows.Forms.GroupBox()
        Me.txtNumberAPT = New System.Windows.Forms.TextBox()
        Me.lblNumberAPT = New System.Windows.Forms.Label()
        Me.chkEntradaAdulto = New System.Windows.Forms.CheckBox()
        Me.chkNoShow = New System.Windows.Forms.CheckBox()
        Me.chkTC = New System.Windows.Forms.CheckBox()
        Me.cboTituloAcademico = New System.Windows.Forms.ComboBox()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.chkPassValidado = New System.Windows.Forms.CheckBox()
        Me.dtpFechaIngresoPais = New System.Windows.Forms.DateTimePicker()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.cboIDPaisResidencia = New System.Windows.Forms.ComboBox()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.chkResidente = New System.Windows.Forms.CheckBox()
        Me.txtOrden = New System.Windows.Forms.TextBox()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.txtPeso = New System.Windows.Forms.TextBox()
        Me.txtObservEspecial = New System.Windows.Forms.TextBox()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.dtpFecNacimiento = New System.Windows.Forms.DateTimePicker()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.cboNacionalidad = New System.Windows.Forms.ComboBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.cboIDIdentidad = New System.Windows.Forms.ComboBox()
        Me.Label23 = New System.Windows.Forms.Label()
        Me.txtNumIdentidad = New System.Windows.Forms.TextBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.txtApellidos = New System.Windows.Forms.TextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.txtNombres = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.cboTitulo = New System.Windows.Forms.ComboBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.lblTitulo = New System.Windows.Forms.Label()
        Me.btnSalir = New System.Windows.Forms.Button()
        Me.btnAceptar = New System.Windows.Forms.Button()
        Me.ErrPrv = New System.Windows.Forms.ErrorProvider(Me.components)
        Me.grb.SuspendLayout()
        CType(Me.ErrPrv, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'grb
        '
        Me.grb.BackColor = System.Drawing.Color.White
        Me.grb.Controls.Add(Me.txtNumberAPT)
        Me.grb.Controls.Add(Me.lblNumberAPT)
        Me.grb.Controls.Add(Me.chkEntradaAdulto)
        Me.grb.Controls.Add(Me.chkNoShow)
        Me.grb.Controls.Add(Me.chkTC)
        Me.grb.Controls.Add(Me.cboTituloAcademico)
        Me.grb.Controls.Add(Me.Label11)
        Me.grb.Controls.Add(Me.chkPassValidado)
        Me.grb.Controls.Add(Me.dtpFechaIngresoPais)
        Me.grb.Controls.Add(Me.Label10)
        Me.grb.Controls.Add(Me.cboIDPaisResidencia)
        Me.grb.Controls.Add(Me.Label9)
        Me.grb.Controls.Add(Me.chkResidente)
        Me.grb.Controls.Add(Me.txtOrden)
        Me.grb.Controls.Add(Me.Label8)
        Me.grb.Controls.Add(Me.txtPeso)
        Me.grb.Controls.Add(Me.txtObservEspecial)
        Me.grb.Controls.Add(Me.Label7)
        Me.grb.Controls.Add(Me.Label6)
        Me.grb.Controls.Add(Me.dtpFecNacimiento)
        Me.grb.Controls.Add(Me.Label13)
        Me.grb.Controls.Add(Me.cboNacionalidad)
        Me.grb.Controls.Add(Me.Label1)
        Me.grb.Controls.Add(Me.cboIDIdentidad)
        Me.grb.Controls.Add(Me.Label23)
        Me.grb.Controls.Add(Me.txtNumIdentidad)
        Me.grb.Controls.Add(Me.Label5)
        Me.grb.Controls.Add(Me.txtApellidos)
        Me.grb.Controls.Add(Me.Label4)
        Me.grb.Controls.Add(Me.txtNombres)
        Me.grb.Controls.Add(Me.Label3)
        Me.grb.Controls.Add(Me.cboTitulo)
        Me.grb.Controls.Add(Me.Label2)
        Me.grb.Controls.Add(Me.lblTitulo)
        Me.grb.Dock = System.Windows.Forms.DockStyle.Right
        Me.grb.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.grb.Location = New System.Drawing.Point(38, 0)
        Me.grb.Name = "grb"
        Me.grb.Size = New System.Drawing.Size(472, 483)
        Me.grb.TabIndex = 550
        Me.grb.TabStop = False
        '
        'txtNumberAPT
        '
        Me.txtNumberAPT.BackColor = System.Drawing.SystemColors.Window
        Me.txtNumberAPT.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtNumberAPT.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtNumberAPT.Location = New System.Drawing.Point(130, 61)
        Me.txtNumberAPT.MaxLength = 20
        Me.txtNumberAPT.Name = "txtNumberAPT"
        Me.txtNumberAPT.Size = New System.Drawing.Size(129, 21)
        Me.txtNumberAPT.TabIndex = 136
        Me.txtNumberAPT.Tag = "Borrar"
        Me.txtNumberAPT.Visible = False
        '
        'lblNumberAPT
        '
        Me.lblNumberAPT.AutoSize = True
        Me.lblNumberAPT.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblNumberAPT.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.lblNumberAPT.Location = New System.Drawing.Point(127, 45)
        Me.lblNumberAPT.Name = "lblNumberAPT"
        Me.lblNumberAPT.Size = New System.Drawing.Size(76, 13)
        Me.lblNumberAPT.TabIndex = 135
        Me.lblNumberAPT.Tag = "Borrar"
        Me.lblNumberAPT.Text = "Number APT"
        Me.lblNumberAPT.Visible = False
        '
        'chkEntradaAdulto
        '
        Me.chkEntradaAdulto.AutoSize = True
        Me.chkEntradaAdulto.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkEntradaAdulto.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.chkEntradaAdulto.Location = New System.Drawing.Point(204, 379)
        Me.chkEntradaAdulto.Name = "chkEntradaAdulto"
        Me.chkEntradaAdulto.Size = New System.Drawing.Size(196, 17)
        Me.chkEntradaAdulto.TabIndex = 134
        Me.chkEntradaAdulto.Tag = "Borrar"
        Me.chkEntradaAdulto.Text = "Considerar entrada adulto INC"
        Me.chkEntradaAdulto.UseVisualStyleBackColor = True
        '
        'chkNoShow
        '
        Me.chkNoShow.AutoSize = True
        Me.chkNoShow.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkNoShow.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.chkNoShow.Location = New System.Drawing.Point(17, 381)
        Me.chkNoShow.Name = "chkNoShow"
        Me.chkNoShow.Size = New System.Drawing.Size(73, 17)
        Me.chkNoShow.TabIndex = 133
        Me.chkNoShow.Tag = "Borrar"
        Me.chkNoShow.Text = "No Show"
        Me.chkNoShow.UseVisualStyleBackColor = True
        '
        'chkTC
        '
        Me.chkTC.AutoSize = True
        Me.chkTC.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkTC.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.chkTC.Location = New System.Drawing.Point(219, 104)
        Me.chkTC.Name = "chkTC"
        Me.chkTC.Size = New System.Drawing.Size(40, 17)
        Me.chkTC.TabIndex = 4
        Me.chkTC.Tag = "Borrar"
        Me.chkTC.Text = "TC"
        Me.chkTC.UseVisualStyleBackColor = True
        '
        'cboTituloAcademico
        '
        Me.cboTituloAcademico.BackColor = System.Drawing.Color.Moccasin
        Me.cboTituloAcademico.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboTituloAcademico.FormattingEnabled = True
        Me.cboTituloAcademico.Location = New System.Drawing.Point(130, 102)
        Me.cboTituloAcademico.Name = "cboTituloAcademico"
        Me.cboTituloAcademico.Size = New System.Drawing.Size(83, 21)
        Me.cboTituloAcademico.TabIndex = 3
        Me.cboTituloAcademico.Tag = "Borrar"
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label11.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.Label11.Location = New System.Drawing.Point(126, 86)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(104, 13)
        Me.Label11.TabIndex = 132
        Me.Label11.Text = "Título Académico"
        '
        'chkPassValidado
        '
        Me.chkPassValidado.AutoSize = True
        Me.chkPassValidado.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkPassValidado.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.chkPassValidado.Location = New System.Drawing.Point(204, 356)
        Me.chkPassValidado.Name = "chkPassValidado"
        Me.chkPassValidado.Size = New System.Drawing.Size(250, 17)
        Me.chkPassValidado.TabIndex = 15
        Me.chkPassValidado.Tag = "Borrar"
        Me.chkPassValidado.Text = "Doc. de Identidad recibida y confirmada"
        Me.chkPassValidado.UseVisualStyleBackColor = True
        '
        'dtpFechaIngresoPais
        '
        Me.dtpFechaIngresoPais.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpFechaIngresoPais.Location = New System.Drawing.Point(204, 313)
        Me.dtpFechaIngresoPais.Name = "dtpFechaIngresoPais"
        Me.dtpFechaIngresoPais.ShowCheckBox = True
        Me.dtpFechaIngresoPais.Size = New System.Drawing.Size(114, 21)
        Me.dtpFechaIngresoPais.TabIndex = 13
        Me.dtpFechaIngresoPais.Tag = "Borrar"
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label10.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.Label10.Location = New System.Drawing.Point(201, 297)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(102, 13)
        Me.Label10.TabIndex = 129
        Me.Label10.Tag = "Borrar"
        Me.Label10.Text = "Fecha de ingreso"
        '
        'cboIDPaisResidencia
        '
        Me.cboIDPaisResidencia.BackColor = System.Drawing.SystemColors.Window
        Me.cboIDPaisResidencia.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboIDPaisResidencia.FormattingEnabled = True
        Me.cboIDPaisResidencia.Location = New System.Drawing.Point(204, 271)
        Me.cboIDPaisResidencia.Name = "cboIDPaisResidencia"
        Me.cboIDPaisResidencia.Size = New System.Drawing.Size(158, 21)
        Me.cboIDPaisResidencia.TabIndex = 11
        Me.cboIDPaisResidencia.Tag = "Borrar"
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label9.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.Label9.Location = New System.Drawing.Point(201, 255)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(108, 13)
        Me.Label9.TabIndex = 127
        Me.Label9.Text = "País de residencia"
        '
        'chkResidente
        '
        Me.chkResidente.AutoSize = True
        Me.chkResidente.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkResidente.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.chkResidente.Location = New System.Drawing.Point(308, 185)
        Me.chkResidente.Name = "chkResidente"
        Me.chkResidente.Size = New System.Drawing.Size(83, 17)
        Me.chkResidente.TabIndex = 7
        Me.chkResidente.Tag = "Borrar"
        Me.chkResidente.Text = "Residente"
        Me.chkResidente.UseVisualStyleBackColor = True
        '
        'txtOrden
        '
        Me.txtOrden.BackColor = System.Drawing.SystemColors.Window
        Me.txtOrden.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtOrden.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtOrden.Location = New System.Drawing.Point(18, 62)
        Me.txtOrden.MaxLength = 5
        Me.txtOrden.Name = "txtOrden"
        Me.txtOrden.Size = New System.Drawing.Size(54, 21)
        Me.txtOrden.TabIndex = 1
        Me.txtOrden.Tag = "Borrar"
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.Label8.Location = New System.Drawing.Point(15, 45)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(66, 13)
        Me.Label8.TabIndex = 125
        Me.Label8.Tag = "Borrar"
        Me.Label8.Text = "Nro. Orden"
        '
        'txtPeso
        '
        Me.txtPeso.BackColor = System.Drawing.SystemColors.Window
        Me.txtPeso.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtPeso.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtPeso.Location = New System.Drawing.Point(18, 354)
        Me.txtPeso.MaxLength = 3
        Me.txtPeso.Name = "txtPeso"
        Me.txtPeso.Size = New System.Drawing.Size(58, 21)
        Me.txtPeso.TabIndex = 14
        Me.txtPeso.Tag = "Borrar"
        Me.txtPeso.Text = "0"
        '
        'txtObservEspecial
        '
        Me.txtObservEspecial.BackColor = System.Drawing.SystemColors.Window
        Me.txtObservEspecial.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtObservEspecial.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtObservEspecial.Location = New System.Drawing.Point(18, 424)
        Me.txtObservEspecial.MaxLength = 200
        Me.txtObservEspecial.Multiline = True
        Me.txtObservEspecial.Name = "txtObservEspecial"
        Me.txtObservEspecial.Size = New System.Drawing.Size(373, 49)
        Me.txtObservEspecial.TabIndex = 16
        Me.txtObservEspecial.Tag = "Borrar"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.Label7.Location = New System.Drawing.Point(15, 408)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(125, 13)
        Me.Label7.TabIndex = 123
        Me.Label7.Tag = "Borrar"
        Me.Label7.Text = "Observación Especial"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.Label6.Location = New System.Drawing.Point(15, 337)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(61, 13)
        Me.Label6.TabIndex = 122
        Me.Label6.Tag = "Borrar"
        Me.Label6.Text = "Peso (Kg)"
        '
        'dtpFecNacimiento
        '
        Me.dtpFecNacimiento.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpFecNacimiento.Location = New System.Drawing.Point(18, 313)
        Me.dtpFecNacimiento.Name = "dtpFecNacimiento"
        Me.dtpFecNacimiento.ShowCheckBox = True
        Me.dtpFecNacimiento.Size = New System.Drawing.Size(114, 21)
        Me.dtpFecNacimiento.TabIndex = 12
        Me.dtpFecNacimiento.Tag = "Borrar"
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label13.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.Label13.Location = New System.Drawing.Point(15, 297)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(106, 13)
        Me.Label13.TabIndex = 120
        Me.Label13.Tag = "Borrar"
        Me.Label13.Text = "Fecha Nacimiento"
        '
        'cboNacionalidad
        '
        Me.cboNacionalidad.BackColor = System.Drawing.SystemColors.Window
        Me.cboNacionalidad.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboNacionalidad.FormattingEnabled = True
        Me.cboNacionalidad.Location = New System.Drawing.Point(18, 271)
        Me.cboNacionalidad.Name = "cboNacionalidad"
        Me.cboNacionalidad.Size = New System.Drawing.Size(158, 21)
        Me.cboNacionalidad.TabIndex = 10
        Me.cboNacionalidad.Tag = "Borrar"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.Label1.Location = New System.Drawing.Point(15, 255)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(78, 13)
        Me.Label1.TabIndex = 119
        Me.Label1.Text = "Nacionalidad"
        '
        'cboIDIdentidad
        '
        Me.cboIDIdentidad.BackColor = System.Drawing.SystemColors.Window
        Me.cboIDIdentidad.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboIDIdentidad.FormattingEnabled = True
        Me.cboIDIdentidad.Location = New System.Drawing.Point(18, 229)
        Me.cboIDIdentidad.Name = "cboIDIdentidad"
        Me.cboIDIdentidad.Size = New System.Drawing.Size(158, 21)
        Me.cboIDIdentidad.TabIndex = 8
        Me.cboIDIdentidad.Tag = "Borrar"
        '
        'Label23
        '
        Me.Label23.AutoSize = True
        Me.Label23.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label23.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.Label23.Location = New System.Drawing.Point(15, 213)
        Me.Label23.Name = "Label23"
        Me.Label23.Size = New System.Drawing.Size(134, 13)
        Me.Label23.TabIndex = 117
        Me.Label23.Text = "Tipo Docum. Identidad"
        '
        'txtNumIdentidad
        '
        Me.txtNumIdentidad.BackColor = System.Drawing.SystemColors.Window
        Me.txtNumIdentidad.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtNumIdentidad.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtNumIdentidad.Location = New System.Drawing.Point(204, 229)
        Me.txtNumIdentidad.MaxLength = 25
        Me.txtNumIdentidad.Name = "txtNumIdentidad"
        Me.txtNumIdentidad.Size = New System.Drawing.Size(187, 21)
        Me.txtNumIdentidad.TabIndex = 9
        Me.txtNumIdentidad.Tag = "Borrar"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.Label5.Location = New System.Drawing.Point(201, 209)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(132, 13)
        Me.Label5.TabIndex = 115
        Me.Label5.Text = "Nro. Docum. Identidad"
        '
        'txtApellidos
        '
        Me.txtApellidos.BackColor = System.Drawing.Color.Moccasin
        Me.txtApellidos.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtApellidos.Location = New System.Drawing.Point(18, 142)
        Me.txtApellidos.MaxLength = 50
        Me.txtApellidos.Name = "txtApellidos"
        Me.txtApellidos.Size = New System.Drawing.Size(245, 21)
        Me.txtApellidos.TabIndex = 5
        Me.txtApellidos.Tag = "Borrar"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.Label4.Location = New System.Drawing.Point(15, 126)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(58, 13)
        Me.Label4.TabIndex = 112
        Me.Label4.Text = "Apellidos"
        '
        'txtNombres
        '
        Me.txtNombres.BackColor = System.Drawing.Color.Moccasin
        Me.txtNombres.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtNombres.Location = New System.Drawing.Point(18, 183)
        Me.txtNombres.MaxLength = 50
        Me.txtNombres.Name = "txtNombres"
        Me.txtNombres.Size = New System.Drawing.Size(245, 21)
        Me.txtNombres.TabIndex = 6
        Me.txtNombres.Tag = "Borrar"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.Label3.Location = New System.Drawing.Point(15, 167)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(57, 13)
        Me.Label3.TabIndex = 110
        Me.Label3.Text = "Nombres"
        '
        'cboTitulo
        '
        Me.cboTitulo.BackColor = System.Drawing.Color.Moccasin
        Me.cboTitulo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboTitulo.FormattingEnabled = True
        Me.cboTitulo.Location = New System.Drawing.Point(18, 102)
        Me.cboTitulo.Name = "cboTitulo"
        Me.cboTitulo.Size = New System.Drawing.Size(83, 21)
        Me.cboTitulo.TabIndex = 2
        Me.cboTitulo.Tag = "Borrar"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.Label2.Location = New System.Drawing.Point(14, 86)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(39, 13)
        Me.Label2.TabIndex = 108
        Me.Label2.Text = "Título"
        '
        'lblTitulo
        '
        Me.lblTitulo.BackColor = System.Drawing.SystemColors.ControlDarkDark
        Me.lblTitulo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.lblTitulo.Dock = System.Windows.Forms.DockStyle.Top
        Me.lblTitulo.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTitulo.ForeColor = System.Drawing.Color.White
        Me.lblTitulo.Location = New System.Drawing.Point(3, 17)
        Me.lblTitulo.Name = "lblTitulo"
        Me.lblTitulo.Size = New System.Drawing.Size(466, 22)
        Me.lblTitulo.TabIndex = 107
        Me.lblTitulo.Text = "Nuevo"
        '
        'btnSalir
        '
        Me.btnSalir.Image = Global.SETours.My.Resources.Resources.door_out
        Me.btnSalir.Location = New System.Drawing.Point(5, 34)
        Me.btnSalir.Name = "btnSalir"
        Me.btnSalir.Size = New System.Drawing.Size(27, 27)
        Me.btnSalir.TabIndex = 163
        Me.btnSalir.UseVisualStyleBackColor = True
        '
        'btnAceptar
        '
        Me.btnAceptar.Enabled = False
        Me.btnAceptar.Image = Global.SETours.My.Resources.Resources.accept
        Me.btnAceptar.Location = New System.Drawing.Point(5, 5)
        Me.btnAceptar.Name = "btnAceptar"
        Me.btnAceptar.Size = New System.Drawing.Size(27, 27)
        Me.btnAceptar.TabIndex = 162
        Me.btnAceptar.UseVisualStyleBackColor = True
        '
        'ErrPrv
        '
        Me.ErrPrv.ContainerControl = Me
        '
        'frmMantPaxporCotizDatoMem
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(510, 483)
        Me.Controls.Add(Me.btnSalir)
        Me.Controls.Add(Me.btnAceptar)
        Me.Controls.Add(Me.grb)
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmMantPaxporCotizDatoMem"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Ingreso de Pax por Cotización"
        Me.grb.ResumeLayout(False)
        Me.grb.PerformLayout()
        CType(Me.ErrPrv, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents btnSalir As System.Windows.Forms.Button
    Friend WithEvents btnAceptar As System.Windows.Forms.Button
    Friend WithEvents grb As System.Windows.Forms.GroupBox
    Friend WithEvents lblTitulo As System.Windows.Forms.Label
    Friend WithEvents txtApellidos As System.Windows.Forms.TextBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents txtNombres As System.Windows.Forms.TextBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents cboTitulo As System.Windows.Forms.ComboBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents cboIDIdentidad As System.Windows.Forms.ComboBox
    Friend WithEvents Label23 As System.Windows.Forms.Label
    Friend WithEvents txtNumIdentidad As System.Windows.Forms.TextBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents cboNacionalidad As System.Windows.Forms.ComboBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents dtpFecNacimiento As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents ErrPrv As System.Windows.Forms.ErrorProvider
    Friend WithEvents txtPeso As System.Windows.Forms.TextBox
    Friend WithEvents txtObservEspecial As System.Windows.Forms.TextBox
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents txtOrden As System.Windows.Forms.TextBox
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents chkResidente As System.Windows.Forms.CheckBox
    Friend WithEvents cboIDPaisResidencia As System.Windows.Forms.ComboBox
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents dtpFechaIngresoPais As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents chkPassValidado As System.Windows.Forms.CheckBox
    Friend WithEvents cboTituloAcademico As System.Windows.Forms.ComboBox
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents chkTC As System.Windows.Forms.CheckBox
    Friend WithEvents chkNoShow As System.Windows.Forms.CheckBox
    Friend WithEvents chkEntradaAdulto As System.Windows.Forms.CheckBox
    Friend WithEvents txtNumberAPT As System.Windows.Forms.TextBox
    Friend WithEvents lblNumberAPT As System.Windows.Forms.Label
End Class
