﻿Imports ComSEToursBL2
Public Class frmIngPaxTransportMem
    Public intIDTransporte As Integer
    Public intIDCab As Integer
    Dim blnCargaIniStruct As Boolean = True
    Dim blnLoad As Boolean = False
    Dim blnCambios As Boolean = False
    Public FormRefCotiz As frmIngCotizacionDato
    'Public FormRefReserva As frmIngReservasControlporFileDato
    Public chrTipoTranporte As Char
    'Dim FormRefReservas As frmIngReservasControlporFile

    Private Structure stTransportPaxTemp
        Dim IDCab As Integer
        Dim IDTransporte As Integer
        Dim IDPaxTransp As String
        Dim IDPax As String
        Dim NombrePax As String
        Dim CodReservaTransp As String
        Dim IDProveedorGuia As String
        Dim NombreGuia As String
        Dim Selecc As Boolean
        Dim Reembolso As Double
        Dim UserMod As String
        Dim Accion As Char
    End Structure
    Private objLstTransportPaxTemp As New List(Of stTransportPaxTemp)
    Dim blnActualizarST As Boolean = True
    Private Sub ConsultarPaxxIDTransporte()

        Try
            If Not IsNumeric(intIDTransporte) Then
                Dim _Stop As Byte = 0
            End If
            Dim objBN As New clsPaxTransporteBN
            Dim dtt As DataTable = objBN.ConsultarxTransporte(intIDTransporte)

            dgvPax.Rows.Clear()

            For Each dr As DataRow In dtt.Rows
                dgvPax.Rows.Add()
                For i As Byte = 0 To dtt.Columns.Count - 1
                    dgvPax.Item(i, dgvPax.Rows.Count - 1).Value = dr(i)
                Next
            Next
        Catch ex As Exception
            Throw
        End Try
    End Sub

    Private Sub frmIngPaxTransportMem_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Activated
        blnLoad = True
    End Sub

    Private Function blnHayDatosSTxIDTransporte() As Boolean
        For Each ST As stTransportPax In objLstTransportPax
            If ST.IDTransporte = intIDTransporte Then
                Return True
            End If
        Next
        Return False
    End Function

    Private Sub frmIngPaxTransportMem_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            If Not blnHayDatosSTxIDTransporte() Then
                ConsultarPaxxIDTransporte()
            Else
                ConsultarPaxxIDTransporteST()
            End If
            CargarTextValueCheckMarcado()
            CargarReadOnlyReembolso()

            'BorrarstTransportPax(intIDTransporte)
            For Each dgvRow As DataGridViewRow In dgvPax.Rows
                AgregarEditarValorStructTransportPax(dgvRow.Index, False)
            Next
            blnCargaIniStruct = False


        Catch ex As Exception
            MessageBox.Show(ex.Message, ex.Source, MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub CargarTextValueCheckMarcado()
        Try
            Dim blnTodosMarcados As Boolean = True
            For Each dgvItem As DataGridViewRow In dgvPax.Rows
                If Not CBool(dgvItem.Cells("Sel").Value) Then
                    blnTodosMarcados = False
                    Exit For
                End If
            Next

            If blnTodosMarcados Then
                chkMarcarTodos.Text = "Desmarcar todos"
            Else
                chkMarcarTodos.Text = "Marcar todos"
            End If

            RemoveHandler chkMarcarTodos.CheckedChanged, AddressOf chkMarcarTodos_CheckedChanged
            chkMarcarTodos.Checked = blnTodosMarcados
            AddHandler chkMarcarTodos.CheckedChanged, AddressOf chkMarcarTodos_CheckedChanged
        Catch ex As Exception
            Throw
        End Try
    End Sub

    Private Sub CargarReadOnlyReembolso()
        Try
            For Each Item As DataGridViewRow In dgvPax.Rows
                Item.Cells("Reembolso").ReadOnly = Not Convert.ToBoolean(Item.Cells("NoShow").Value)
            Next
        Catch ex As Exception
            Throw
        End Try
    End Sub

    Private Sub ConsultarPaxxIDTransporteST()
        Try
            dgvPax.Rows.Clear()

            'Dim intInd As Int16 = 0
            For Each ST As stTransportPax In objLstTransportPax
                If ST.IDTransporte = intIDTransporte Then 'And ST.Accion <> "B" Then

                    PintarSTenDgv(ST.IDPaxTransp)

                End If
                'intInd += 1
            Next
        Catch ex As Exception
            Throw
        End Try
    End Sub

    Private Sub PintarSTenDgv(ByVal vstrIDPaxTransp As String)
        Try
            'Dim intInd As Int16 = 0
            Dim intFilaDgv As Byte = dgvPax.Rows.Count
            For Each ST As stTransportPax In objLstTransportPax
                If vstrIDPaxTransp = ST.IDPaxTransp And intIDTransporte = ST.IDTransporte Then
                    With dgvPax
                        .Rows.Add()

                        .Item("IDPaxTransp", intFilaDgv).Value = ST.IDPaxTransp
                        .Item("IDPax", intFilaDgv).Value = ST.IDPax
                        .Item("NombrePax", intFilaDgv).Value = ST.NombrePax
                        .Item("CodReservaTransp", intFilaDgv).Value = ST.CodReservaTransp
                        .Item("IDProveedorGuia", intFilaDgv).Value = ST.IDProveedorGuia
                        .Item("NombreGuia", intFilaDgv).Value = ST.NombreGuia
                        .Item("Sel", intFilaDgv).Value = ST.Selecc

                    End With
                    intFilaDgv += 1
                End If

            Next

        Catch ex As Exception
            Throw
        End Try
    End Sub
    Private Function bytPaxSeleccionados() As Byte
        Dim bytSel As Byte = 0
        For Each dgvRow As DataGridViewRow In dgvPax.Rows
            If Convert.ToBoolean(dgvRow.Cells("Sel").Value) = True Then
                bytSel += 1
            End If
        Next
        Return bytSel
    End Function

    Private Sub btnAceptar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAceptar.Click
        Try

            ActualizarSTPublico()
            If chrTipoTranporte = "V" Then
                If Not FormRefCotiz Is Nothing Then
                    FormRefCotiz.dgvVuelos.CurrentRow.Cells("CantPaxV").Value = bytPaxSeleccionados()
                    'ElseIf Not FormRefReserva Is Nothing Then
                    '    FormRefReserva.dgvVuelos.CurrentRow.Cells("CantPaxV").Value = bytPaxSeleccionados()
                End If

            ElseIf chrTipoTranporte = "B" Then
                If Not FormRefCotiz Is Nothing Then
                    FormRefCotiz.dgvBuss.CurrentRow.Cells("CantPaxB").Value = bytPaxSeleccionados()
                    'ElseIf Not FormRefReserva Is Nothing Then
                    '    FormRefReserva.dgvBuss.CurrentRow.Cells("CantPaxB").Value = bytPaxSeleccionados()
                End If

            ElseIf chrTipoTranporte = "T" Then
                If Not FormRefCotiz Is Nothing Then
                    FormRefCotiz.dgvTren.CurrentRow.Cells("CantPaxT").Value = bytPaxSeleccionados()
                    'ElseIf Not FormRefReserva Is Nothing Then
                    '    FormRefReserva.dgvTren.CurrentRow.Cells("CantPaxT").Value = bytPaxSeleccionados()
                End If
            End If

            'If blnCambios And Not FormRefReserva Is Nothing Then
            '    FormRefReserva.blnActualizoTransportePax = True
            'End If

            blnCambios = False
            Me.Close()
        Catch ex As Exception
            MessageBox.Show(ex.Message, ex.Source, MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub btnSalir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSalir.Click
        Me.Close()
    End Sub
    Private Sub ActualizarSTPublico()
        Try
            For Each STTemp As stTransportPaxTemp In objLstTransportPaxTemp

                For Each ST As stTransportPax In objLstTransportPax
                    If ST.IDPaxTransp = STTemp.IDPaxTransp And ST.IDTransporte = STTemp.IDTransporte And _
                    ST.IDPax = STTemp.IDPax Then

                        objLstTransportPax.Remove(ST)
                        Exit For
                    End If
                Next

                Dim RegValor As New stTransportPax
                With RegValor
                    .IDCab = intIDCab
                    .IDTransporte = STTemp.IDTransporte
                    .IDPaxTransp = STTemp.IDPaxTransp
                    .IDPax = STTemp.IDPax
                    .NombrePax = STTemp.NombrePax
                    .IDProveedorGuia = STTemp.IDProveedorGuia
                    .NombreGuia = STTemp.NombreGuia
                    .CodReservaTransp = STTemp.CodReservaTransp
                    .Reembolso = STTemp.Reembolso
                    .UserMod = gstrUser
                    .Accion = STTemp.Accion
                    .Selecc = STTemp.Selecc
                End With
                objLstTransportPax.Add(RegValor)

            Next
        Catch ex As Exception
            Throw
        End Try
    End Sub
    Private Sub frmIngDebitMemoDatoMem_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        Select Case e.KeyCode
            Case Keys.F6
                If btnAceptar.Enabled Then btnAceptar_Click(Nothing, Nothing)
            Case Keys.Escape
                btnSalir_Click(Nothing, Nothing)
        End Select
    End Sub

    Private Sub AgregarEditarValorStructTransportPax(ByVal vbytNroFila As Byte, ByVal blnCheck As Boolean)
        If Not blnActualizarST Then Exit Sub
        Try
            dgvPax.CommitEdit(DataGridViewDataErrorContexts.LeaveControl)

            Dim strAccion As String = "" 'N,B,M
            Dim strID As String = dgvPax.Item("IDPaxTransp", vbytNroFila).Value.ToString
            Dim strIDPax As String = ""
            If dgvPax.Item("IDPax", vbytNroFila).Value Is Nothing Then
                strIDPax = "0"
            Else
                strIDPax = dgvPax.Item("IDPax", vbytNroFila).Value.ToString
            End If

            For Each ST As stTransportPaxTemp In objLstTransportPaxTemp

                If ST.IDPaxTransp = strID And ST.IDTransporte = intIDTransporte And ST.IDPax = strIDPax Then

                    Dim blnSel As Boolean = Convert.ToBoolean(dgvPax.Item("Sel", vbytNroFila).Value)

                    If blnSel Then
                        strAccion = "N"
                    Else
                        strAccion = "B"
                    End If
                    objLstTransportPaxTemp.Remove(ST)
                    Exit For
                End If
            Next

            Dim RegValor As New stTransportPaxTemp
            With RegValor
                .IDCab = intIDCab
                .IDTransporte = intIDTransporte
                .IDPaxTransp = dgvPax.Item("IDPaxTransp", vbytNroFila).Value.ToString
                If Not dgvPax.Item("IDPax", vbytNroFila).Value Is Nothing Then
                    .IDPax = dgvPax.Item("IDPax", vbytNroFila).Value.ToString
                Else
                    .IDPax = 0
                End If
                If Not dgvPax.Item("NombrePax", vbytNroFila).Value Is Nothing Then
                    .NombrePax = dgvPax.Item("NombrePax", vbytNroFila).Value.ToString.Trim
                Else
                    .NombrePax = ""
                End If
                If Not dgvPax.Item("IDProveedorGuia", vbytNroFila).Value Is Nothing Then
                    .IDProveedorGuia = dgvPax.Item("IDProveedorGuia", vbytNroFila).Value.ToString.Trim
                Else
                    .IDProveedorGuia = ""
                End If
                If Not dgvPax.Item("NombreGuia", vbytNroFila).Value Is Nothing Then
                    .NombreGuia = dgvPax.Item("NombreGuia", vbytNroFila).Value.ToString.Trim
                Else
                    .NombreGuia = ""
                End If
                If Not dgvPax.Item("CodReservaTransp", vbytNroFila).Value Is Nothing Then
                    .CodReservaTransp = dgvPax.Item("CodReservaTransp", vbytNroFila).Value.ToString
                Else
                    .CodReservaTransp = ""
                End If
                Dim blnSel As Boolean = Convert.ToBoolean(dgvPax.Item("Sel", vbytNroFila).Value)
                .Selecc = blnSel
                .UserMod = gstrUser

                If Not dgvPax.Item("Reembolso", vbytNroFila).Value Is Nothing Then
                    If dgvPax.Item("Reembolso", vbytNroFila).Value.ToString.Trim = String.Empty Then
                        .Reembolso = 0
                    Else
                        .Reembolso = Convert.ToDouble(dgvPax.Item("Reembolso", vbytNroFila).Value)
                    End If
                Else
                    .Reembolso = 0
                End If
                dgvPax.Item("Reembolso", vbytNroFila).Value = Format(.Reembolso, "###,##0.00")

                If strAccion = "" Then 'si no existe en struct
                    If Not blnCargaIniStruct Then
                        .Accion = "N"
                    Else
                        .Accion = strAccion
                    End If
                Else
                    .Accion = strAccion
                End If

            End With
            objLstTransportPaxTemp.Add(RegValor)

            If blnLoad Then
                blnCambios = True
                ErrPrv.SetError(dgvPax, "")
            End If


        Catch ex As Exception
            Throw
        End Try

    End Sub

    Private Sub dgvPax_CellBeginEdit(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellCancelEventArgs) Handles dgvPax.CellBeginEdit
        If e.ColumnIndex = dgvPax.Columns("Sel").Index Then Exit Sub

        If Convert.ToBoolean(dgvPax.CurrentRow.Cells("Sel").Value) = False Then
            ErrPrv.SetError(dgvPax, "Debe seleccionar el check de la izquierda antes de hacer cambios a la celda")
            e.Cancel = True
        End If
    End Sub

    Private Sub dgvPax_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvPax.CellContentClick
        If e.RowIndex < 0 Then Exit Sub
        If e.ColumnIndex < 0 Then Exit Sub
        Try
            If dgvPax.CurrentCell.ColumnIndex = dgvPax.Columns("Sel").Index Then
                dgvPax.CommitEdit(DataGridViewDataErrorContexts.LeaveControl)
                AgregarEditarValorStructTransportPax(e.RowIndex, True)

                If Convert.ToBoolean(dgvPax.CurrentCell.Value) = True Then
                    blnActualizarST = False
                    dgvPax.CurrentRow.Cells("CodReservaTransp").Value = ""
                    dgvPax.CurrentRow.Cells("IDProveedorGuia").Value = ""
                    dgvPax.CurrentRow.Cells("NombreGuia").Value = ""
                    blnActualizarST = True
                    AgregarEditarValorStructTransportPax(e.RowIndex, True)
                End If
                CargarTextValueCheckMarcado()
            End If


        Catch ex As Exception
            MessageBox.Show(ex.Message, ex.Source, MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    'Private Sub dgvPax_CellFormatting(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellFormattingEventArgs) Handles dgvPax.CellFormatting
    '    If e.RowIndex = -1 Then Exit Sub
    '    If e.ColumnIndex = -1 Then Exit Sub
    '    Try
    '        If e.ColumnIndex = dgvPax.Columns("btnBuscarGuia").Index Then
    '            dgvPax.Item("btnBuscarGuia", e.RowIndex).ToolTipText = "Consultar Guía"
    '            dgvPax.Item("btnBuscarGuia", e.RowIndex).
    '        End If
    '    Catch ex As Exception
    '        MessageBox.Show(ex.Message, ex.Source, MessageBoxButtons.OK, MessageBoxIcon.Error)
    '    End Try
    'End Sub

    'Private Sub dgvPax_CellPainting(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellPaintingEventArgs) Handles dgvPax.CellPainting
    '    If e.ColumnIndex = -1 Then Exit Sub
    '    If e.RowIndex = -1 Then Exit Sub

    '    If dgvPax.Columns(e.ColumnIndex).Name = "btnBuscarGuia" Then
    '        e.Paint(e.CellBounds, DataGridViewPaintParts.All)
    '        Dim celBoton As DataGridViewButtonCell = CType(dgvPax.Rows(e.RowIndex).Cells("btnBuscarGuia"), DataGridViewButtonCell)
    '        Dim icoAtomico As Image = My.Resources.Resources.zoom
    '        e.Graphics.DrawImage(icoAtomico, e.CellBounds.Left + 3, e.CellBounds.Top + 3)
    '        dgvPax.Rows(e.RowIndex).Height = 21
    '        dgvPax.Columns(e.ColumnIndex).Width = icoAtomico.Width + 5
    '        e.Handled = True
    '    End If
    'End Sub

    'Private Sub dgvPax_CellEndEdit(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvPax.CellEndEdit
    '    If Not e Is Nothing Then
    '        If e.RowIndex < 0 Then Exit Sub

    '        AgregarEditarValorStructTransportPax(e.RowIndex)
    '    Else
    '        If Not dgvPax.CurrentCell Is Nothing Then
    '            AgregarEditarValorStructTransportPax(dgvPax.CurrentCell.RowIndex)
    '        End If
    '    End If

    'End Sub

    'Private Sub dgvPax_CellValidated(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvPax.CellValidated

    'End Sub

    Private Sub dgvPax_CellValueChanged(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvPax.CellValueChanged
        If e.RowIndex = -1 Then Exit Sub
        If Not blnLoad Then Exit Sub
        If e.ColumnIndex = dgvPax.Columns("Sel").Index Then Exit Sub
        Try
            AgregarEditarValorStructTransportPax(e.RowIndex, False)
        Catch ex As Exception
            MessageBox.Show(ex.Message, ex.Source, MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub frmIngPaxTransportMem_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        If blnCambios = True Then
            If MessageBox.Show("Se han realizado cambios. ¿Está Ud. seguro de salir sin Aceptar?", "Confirmar", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = Windows.Forms.DialogResult.Yes Then

                e.Cancel = False
            Else
                e.Cancel = True
            End If

        End If

    End Sub

    Private Sub LlamarConsProveedores()
        Dim frmAyuda As New frmAyudas
        With frmAyuda
            .strCaller = Me.Name
            .frmRefIngPaxTransportMem = Me
            .strTipoBusqueda = "Proveedor"

            .strPar = gstrTipoProveeGuias

            .Text = "Consulta de Proveedores"
            .ShowDialog()
        End With
    End Sub

    Private Sub btnNuevo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNuevo.Click
        Try
            LlamarConsProveedores()
            CargarReadOnlyReembolso()
        Catch ex As Exception
            MessageBox.Show(ex.Message, ex.Source, MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try

    End Sub

    Private Sub chkMarcarTodos_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkMarcarTodos.CheckedChanged
        Try
            For Each DgvItem As DataGridViewRow In dgvPax.Rows
                DgvItem.Cells("Sel").Value = chkMarcarTodos.Checked
                AgregarEditarValorStructTransportPax(DgvItem.Index, chkMarcarTodos.Checked)
            Next

            CargarTextValueCheckMarcado()
        Catch ex As Exception
            MessageBox.Show(ex.Message, ex.Source, MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub dgvPax_EditingControlShowing(sender As Object, e As DataGridViewEditingControlShowingEventArgs) Handles dgvPax.EditingControlShowing
        If InStr(e.Control.ToString, "System.Windows.Forms.DataGridViewTextBoxEditingControl") = 0 Then Exit Sub
        Try
            Dim txtDecimal As TextBox = CType(e.Control, TextBox)
            AddHandler txtDecimal.KeyPress, AddressOf ValidarNrosEnteros_Keypress

        Catch ex As Exception
            MessageBox.Show(ex.Message, ex.Source, MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub


    Private Sub ValidarNrosEnteros_Keypress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs)
        Try
            Dim intColumna As Integer = dgvPax.CurrentCell.ColumnIndex
            Dim txtDecimal As TextBox = CType(sender, TextBox)
            If intColumna = dgvPax.Columns("Reembolso").Index Then
                Dim Caracter As Char = e.KeyChar
                If Not IsNumeric(Caracter) And (Caracter = ChrW(Keys.Back)) = False Then
                    If InStr(txtDecimal.Text.Trim, ".") > 0 Then
                        e.KeyChar = Chr(0)
                    End If
                    If e.KeyChar = "." And txtDecimal.Text.Length = 0 Then
                        e.KeyChar = Chr(0)
                    End If
                End If
            End If
        Catch ex As Exception
            Throw
        End Try
    End Sub
End Class