﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmIngPedido
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle5 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle6 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.dgvDet = New System.Windows.Forms.DataGridView()
        Me.EjecutivoVtasDet = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Fecha = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.FecInicio = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Pax = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Cotizacion = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.NuFile = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.IDCab = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Titulo = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Ruta = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.CoEstadoCot = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DescEstadoCot = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.btnEditDet = New System.Windows.Forms.DataGridViewButtonColumn()
        Me.btnBuscar = New System.Windows.Forms.Button()
        Me.dgv = New System.Windows.Forms.DataGridView()
        Me.EjecutivoVtas = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.FePedido = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.NuPedido = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.NuPedInt = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.RazonComercial = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.TxTitulo = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.CoEstado = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DescEstado = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.btnEdit = New System.Windows.Forms.DataGridViewButtonColumn()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.txtNuCotizacion = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.txtNuPedido = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.txtNuFile = New System.Windows.Forms.TextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.txtCliente = New System.Windows.Forms.TextBox()
        Me.chkFilesArchivados = New System.Windows.Forms.CheckBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.txtTxTitulo = New System.Windows.Forms.TextBox()
        Me.chkActivos = New System.Windows.Forms.CheckBox()
        Me.cboCoEjeVta = New System.Windows.Forms.ComboBox()
        Me.GroupBox1.SuspendLayout()
        CType(Me.dgvDet, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgv, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.Label6)
        Me.GroupBox1.Controls.Add(Me.dgvDet)
        Me.GroupBox1.Controls.Add(Me.btnBuscar)
        Me.GroupBox1.Controls.Add(Me.dgv)
        Me.GroupBox1.Controls.Add(Me.GroupBox2)
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Controls.Add(Me.txtNuCotizacion)
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.Controls.Add(Me.txtNuPedido)
        Me.GroupBox1.Controls.Add(Me.Label3)
        Me.GroupBox1.Controls.Add(Me.txtNuFile)
        Me.GroupBox1.Controls.Add(Me.Label4)
        Me.GroupBox1.Controls.Add(Me.txtCliente)
        Me.GroupBox1.Controls.Add(Me.chkFilesArchivados)
        Me.GroupBox1.Controls.Add(Me.Label5)
        Me.GroupBox1.Controls.Add(Me.txtTxTitulo)
        Me.GroupBox1.Controls.Add(Me.chkActivos)
        Me.GroupBox1.Controls.Add(Me.cboCoEjeVta)
        Me.GroupBox1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.GroupBox1.Location = New System.Drawing.Point(0, 0)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(948, 519)
        Me.GroupBox1.TabIndex = 0
        Me.GroupBox1.TabStop = False
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label6.Location = New System.Drawing.Point(6, 323)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(87, 13)
        Me.Label6.TabIndex = 327
        Me.Label6.Text = "Cotizaciones/File"
        '
        'dgvDet
        '
        Me.dgvDet.AllowUserToAddRows = False
        Me.dgvDet.AllowUserToDeleteRows = False
        Me.dgvDet.AllowUserToOrderColumns = True
        Me.dgvDet.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.ControlDarkDark
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.Color.White
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvDet.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.dgvDet.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvDet.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.EjecutivoVtasDet, Me.Fecha, Me.FecInicio, Me.Pax, Me.Cotizacion, Me.NuFile, Me.IDCab, Me.Titulo, Me.Ruta, Me.CoEstadoCot, Me.DescEstadoCot, Me.btnEditDet})
        Me.dgvDet.Location = New System.Drawing.Point(6, 340)
        Me.dgvDet.Name = "dgvDet"
        Me.dgvDet.ReadOnly = True
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.ControlDarkDark
        DataGridViewCellStyle3.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle3.ForeColor = System.Drawing.Color.White
        DataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvDet.RowHeadersDefaultCellStyle = DataGridViewCellStyle3
        Me.dgvDet.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvDet.Size = New System.Drawing.Size(936, 173)
        Me.dgvDet.TabIndex = 326
        '
        'EjecutivoVtasDet
        '
        Me.EjecutivoVtasDet.DataPropertyName = "EjecutivoVtas"
        Me.EjecutivoVtasDet.HeaderText = "Ejecutivo de Ventas"
        Me.EjecutivoVtasDet.Name = "EjecutivoVtasDet"
        Me.EjecutivoVtasDet.ReadOnly = True
        '
        'Fecha
        '
        Me.Fecha.DataPropertyName = "Fecha"
        DataGridViewCellStyle2.Format = "d"
        DataGridViewCellStyle2.NullValue = Nothing
        Me.Fecha.DefaultCellStyle = DataGridViewCellStyle2
        Me.Fecha.HeaderText = "Fecha"
        Me.Fecha.Name = "Fecha"
        Me.Fecha.ReadOnly = True
        '
        'FecInicio
        '
        Me.FecInicio.DataPropertyName = "FecInicio"
        Me.FecInicio.HeaderText = "Fec. Inicio"
        Me.FecInicio.Name = "FecInicio"
        Me.FecInicio.ReadOnly = True
        '
        'Pax
        '
        Me.Pax.DataPropertyName = "Pax"
        Me.Pax.HeaderText = "Pax"
        Me.Pax.Name = "Pax"
        Me.Pax.ReadOnly = True
        '
        'Cotizacion
        '
        Me.Cotizacion.DataPropertyName = "Cotizacion"
        Me.Cotizacion.HeaderText = "Cotización"
        Me.Cotizacion.Name = "Cotizacion"
        Me.Cotizacion.ReadOnly = True
        '
        'NuFile
        '
        Me.NuFile.DataPropertyName = "NuFile"
        Me.NuFile.HeaderText = "Nro. File"
        Me.NuFile.Name = "NuFile"
        Me.NuFile.ReadOnly = True
        '
        'IDCab
        '
        Me.IDCab.DataPropertyName = "IDCab"
        Me.IDCab.HeaderText = "IDCab"
        Me.IDCab.Name = "IDCab"
        Me.IDCab.ReadOnly = True
        Me.IDCab.Visible = False
        '
        'Titulo
        '
        Me.Titulo.DataPropertyName = "Titulo"
        Me.Titulo.HeaderText = "Título"
        Me.Titulo.Name = "Titulo"
        Me.Titulo.ReadOnly = True
        '
        'Ruta
        '
        Me.Ruta.DataPropertyName = "Ruta"
        Me.Ruta.HeaderText = "Ruta"
        Me.Ruta.Name = "Ruta"
        Me.Ruta.ReadOnly = True
        '
        'CoEstadoCot
        '
        Me.CoEstadoCot.DataPropertyName = "CoEstado"
        Me.CoEstadoCot.HeaderText = "CoEstado"
        Me.CoEstadoCot.Name = "CoEstadoCot"
        Me.CoEstadoCot.ReadOnly = True
        Me.CoEstadoCot.Visible = False
        '
        'DescEstadoCot
        '
        Me.DescEstadoCot.DataPropertyName = "DescEstado"
        Me.DescEstadoCot.HeaderText = "Estado"
        Me.DescEstadoCot.Name = "DescEstadoCot"
        Me.DescEstadoCot.ReadOnly = True
        '
        'btnEditDet
        '
        Me.btnEditDet.HeaderText = ""
        Me.btnEditDet.Name = "btnEditDet"
        Me.btnEditDet.ReadOnly = True
        '
        'btnBuscar
        '
        Me.btnBuscar.Image = Global.SETours.My.Resources.Resources.find
        Me.btnBuscar.Location = New System.Drawing.Point(607, 64)
        Me.btnBuscar.Name = "btnBuscar"
        Me.btnBuscar.Size = New System.Drawing.Size(32, 26)
        Me.btnBuscar.TabIndex = 325
        Me.btnBuscar.UseVisualStyleBackColor = True
        '
        'dgv
        '
        Me.dgv.AllowUserToAddRows = False
        Me.dgv.AllowUserToDeleteRows = False
        Me.dgv.AllowUserToOrderColumns = True
        Me.dgv.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        DataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.ControlDarkDark
        DataGridViewCellStyle4.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle4.ForeColor = System.Drawing.Color.White
        DataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgv.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle4
        Me.dgv.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgv.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.EjecutivoVtas, Me.FePedido, Me.NuPedido, Me.NuPedInt, Me.RazonComercial, Me.TxTitulo, Me.CoEstado, Me.DescEstado, Me.btnEdit})
        DataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle5.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle5.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle5.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle5.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgv.DefaultCellStyle = DataGridViewCellStyle5
        Me.dgv.Location = New System.Drawing.Point(6, 109)
        Me.dgv.Name = "dgv"
        Me.dgv.ReadOnly = True
        DataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle6.BackColor = System.Drawing.SystemColors.ControlDarkDark
        DataGridViewCellStyle6.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle6.ForeColor = System.Drawing.Color.White
        DataGridViewCellStyle6.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle6.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgv.RowHeadersDefaultCellStyle = DataGridViewCellStyle6
        Me.dgv.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgv.Size = New System.Drawing.Size(936, 211)
        Me.dgv.TabIndex = 324
        '
        'EjecutivoVtas
        '
        Me.EjecutivoVtas.DataPropertyName = "EjecutivoVtas"
        Me.EjecutivoVtas.HeaderText = "Ejecutivo de Ventas"
        Me.EjecutivoVtas.Name = "EjecutivoVtas"
        Me.EjecutivoVtas.ReadOnly = True
        '
        'FePedido
        '
        Me.FePedido.DataPropertyName = "FePedido"
        Me.FePedido.HeaderText = "Fecha"
        Me.FePedido.Name = "FePedido"
        Me.FePedido.ReadOnly = True
        '
        'NuPedido
        '
        Me.NuPedido.DataPropertyName = "NuPedido"
        Me.NuPedido.HeaderText = "Nro. Pedido"
        Me.NuPedido.Name = "NuPedido"
        Me.NuPedido.ReadOnly = True
        '
        'NuPedInt
        '
        Me.NuPedInt.DataPropertyName = "NuPedInt"
        Me.NuPedInt.HeaderText = "NuPedInt"
        Me.NuPedInt.Name = "NuPedInt"
        Me.NuPedInt.ReadOnly = True
        Me.NuPedInt.Visible = False
        '
        'RazonComercial
        '
        Me.RazonComercial.DataPropertyName = "RazonComercial"
        Me.RazonComercial.HeaderText = "Cliente"
        Me.RazonComercial.Name = "RazonComercial"
        Me.RazonComercial.ReadOnly = True
        '
        'TxTitulo
        '
        Me.TxTitulo.DataPropertyName = "TxTitulo"
        Me.TxTitulo.HeaderText = "Título"
        Me.TxTitulo.Name = "TxTitulo"
        Me.TxTitulo.ReadOnly = True
        '
        'CoEstado
        '
        Me.CoEstado.DataPropertyName = "CoEstado"
        Me.CoEstado.HeaderText = "CoEstado"
        Me.CoEstado.Name = "CoEstado"
        Me.CoEstado.ReadOnly = True
        Me.CoEstado.Visible = False
        '
        'DescEstado
        '
        Me.DescEstado.DataPropertyName = "DescEstado"
        Me.DescEstado.HeaderText = "Estado"
        Me.DescEstado.Name = "DescEstado"
        Me.DescEstado.ReadOnly = True
        '
        'btnEdit
        '
        Me.btnEdit.HeaderText = ""
        Me.btnEdit.Name = "btnEdit"
        Me.btnEdit.ReadOnly = True
        '
        'GroupBox2
        '
        Me.GroupBox2.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GroupBox2.Location = New System.Drawing.Point(6, 95)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(936, 8)
        Me.GroupBox2.TabIndex = 323
        Me.GroupBox2.TabStop = False
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label2.Location = New System.Drawing.Point(324, 71)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(60, 13)
        Me.Label2.TabIndex = 322
        Me.Label2.Text = "Cotización:"
        '
        'txtNuCotizacion
        '
        Me.txtNuCotizacion.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtNuCotizacion.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtNuCotizacion.Location = New System.Drawing.Point(395, 68)
        Me.txtNuCotizacion.MaxLength = 8
        Me.txtNuCotizacion.Name = "txtNuCotizacion"
        Me.txtNuCotizacion.Size = New System.Drawing.Size(98, 21)
        Me.txtNuCotizacion.TabIndex = 321
        Me.txtNuCotizacion.Tag = "Borrar"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label1.Location = New System.Drawing.Point(12, 44)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(67, 13)
        Me.Label1.TabIndex = 320
        Me.Label1.Text = "Nro. Pedido:"
        '
        'txtNuPedido
        '
        Me.txtNuPedido.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtNuPedido.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtNuPedido.Location = New System.Drawing.Point(125, 41)
        Me.txtNuPedido.MaxLength = 8
        Me.txtNuPedido.Name = "txtNuPedido"
        Me.txtNuPedido.Size = New System.Drawing.Size(97, 21)
        Me.txtNuPedido.TabIndex = 319
        Me.txtNuPedido.Tag = "Borrar"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label3.Location = New System.Drawing.Point(12, 71)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(51, 13)
        Me.Label3.TabIndex = 318
        Me.Label3.Text = "Nro. File:"
        '
        'txtNuFile
        '
        Me.txtNuFile.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtNuFile.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtNuFile.Location = New System.Drawing.Point(125, 68)
        Me.txtNuFile.MaxLength = 8
        Me.txtNuFile.Name = "txtNuFile"
        Me.txtNuFile.Size = New System.Drawing.Size(97, 21)
        Me.txtNuFile.TabIndex = 317
        Me.txtNuFile.Tag = "Borrar"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label4.Location = New System.Drawing.Point(324, 44)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(44, 13)
        Me.Label4.TabIndex = 316
        Me.Label4.Text = "Cliente:"
        '
        'txtCliente
        '
        Me.txtCliente.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtCliente.Location = New System.Drawing.Point(395, 41)
        Me.txtCliente.MaxLength = 60
        Me.txtCliente.Name = "txtCliente"
        Me.txtCliente.Size = New System.Drawing.Size(244, 21)
        Me.txtCliente.TabIndex = 315
        Me.txtCliente.Tag = "Borrar"
        '
        'chkFilesArchivados
        '
        Me.chkFilesArchivados.AutoSize = True
        Me.chkFilesArchivados.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkFilesArchivados.ForeColor = System.Drawing.SystemColors.ControlText
        Me.chkFilesArchivados.Location = New System.Drawing.Point(649, 14)
        Me.chkFilesArchivados.Name = "chkFilesArchivados"
        Me.chkFilesArchivados.Size = New System.Drawing.Size(140, 17)
        Me.chkFilesArchivados.TabIndex = 314
        Me.chkFilesArchivados.Text = "Mostrar files archivados"
        Me.chkFilesArchivados.UseVisualStyleBackColor = True
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label5.Location = New System.Drawing.Point(324, 16)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(37, 13)
        Me.Label5.TabIndex = 313
        Me.Label5.Text = "Título:"
        '
        'txtTxTitulo
        '
        Me.txtTxTitulo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtTxTitulo.Location = New System.Drawing.Point(395, 13)
        Me.txtTxTitulo.MaxLength = 100
        Me.txtTxTitulo.Name = "txtTxTitulo"
        Me.txtTxTitulo.Size = New System.Drawing.Size(227, 21)
        Me.txtTxTitulo.TabIndex = 312
        Me.txtTxTitulo.Tag = "Borrar"
        '
        'chkActivos
        '
        Me.chkActivos.AutoSize = True
        Me.chkActivos.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkActivos.ForeColor = System.Drawing.SystemColors.ControlText
        Me.chkActivos.Location = New System.Drawing.Point(15, 15)
        Me.chkActivos.Name = "chkActivos"
        Me.chkActivos.Size = New System.Drawing.Size(94, 17)
        Me.chkActivos.TabIndex = 311
        Me.chkActivos.Text = "Ejecutivo Vtas"
        Me.chkActivos.UseVisualStyleBackColor = True
        '
        'cboCoEjeVta
        '
        Me.cboCoEjeVta.BackColor = System.Drawing.SystemColors.Window
        Me.cboCoEjeVta.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboCoEjeVta.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboCoEjeVta.FormattingEnabled = True
        Me.cboCoEjeVta.Location = New System.Drawing.Point(125, 12)
        Me.cboCoEjeVta.Name = "cboCoEjeVta"
        Me.cboCoEjeVta.Size = New System.Drawing.Size(186, 21)
        Me.cboCoEjeVta.TabIndex = 309
        '
        'frmIngPedido
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(948, 519)
        Me.Controls.Add(Me.GroupBox1)
        Me.KeyPreview = True
        Me.Name = "frmIngPedido"
        Me.Tag = "Pedido"
        Me.Text = "Pedidos"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        CType(Me.dgvDet, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgv, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents chkActivos As System.Windows.Forms.CheckBox
    Friend WithEvents cboCoEjeVta As System.Windows.Forms.ComboBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents txtTxTitulo As System.Windows.Forms.TextBox
    Friend WithEvents chkFilesArchivados As System.Windows.Forms.CheckBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents txtCliente As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents txtNuPedido As System.Windows.Forms.TextBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents txtNuFile As System.Windows.Forms.TextBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents txtNuCotizacion As System.Windows.Forms.TextBox
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents dgv As System.Windows.Forms.DataGridView
    Friend WithEvents btnBuscar As System.Windows.Forms.Button
    Friend WithEvents EjecutivoVtas As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents FePedido As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents NuPedido As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents NuPedInt As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents RazonComercial As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents TxTitulo As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents CoEstado As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DescEstado As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents btnEdit As System.Windows.Forms.DataGridViewButtonColumn
    Friend WithEvents dgvDet As System.Windows.Forms.DataGridView
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents EjecutivoVtasDet As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Fecha As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents FecInicio As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Pax As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Cotizacion As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents NuFile As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents IDCab As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Titulo As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Ruta As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents CoEstadoCot As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DescEstadoCot As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents btnEditDet As System.Windows.Forms.DataGridViewButtonColumn
End Class
