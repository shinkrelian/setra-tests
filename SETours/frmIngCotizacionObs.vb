﻿Imports ComSEToursBL2
Imports ComSEToursBE2

Public Class frmIngCotizacionObs
    Dim blnLoad As Boolean = False
    Dim blnCambios As Boolean = False
    Dim intIDCab As Int32
    Dim strObservacion As String = ""

    Public Property IDCab As Int32
        Set(value As Int32)
            intIDCab = value
        End Set
        Get
            Return intIDCab
        End Get
    End Property

    Public Property Observacion As String
        Set(value As String)
            strObservacion = value
        End Set
        Get
            Return strObservacion
        End Get
    End Property

    Private Sub frmIngCotizacionObs_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        blnCambios = False
        Me.Cargar_Datos()
        blnLoad = True
    End Sub

    Private Sub Cargar_Datos()
        Try
            Using objBL As New clsCotizacionBN
                Using dr As SqlClient.SqlDataReader = objBL.ConsultarObservaciones_Pk(intIDCab)
                    dr.Read()
                    If dr.HasRows Then
                        txtIDCotizacion.Text = dr("Cotizacion").ToString.Trim
                        If strObservacion <> "" Then
                            txtObservaciones.Text = strObservacion
                        Else
                            If IsDBNull(dr("ObservacionVentas")) Then
                                txtObservaciones.Text = ""
                            Else
                                txtObservaciones.Text = dr("ObservacionVentas").ToString.Trim
                            End If
                        End If
                    Else
                        MessageBox.Show("El registro no existe.", gstrTituloMsg, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                        dr.Close()
                    End If
                End Using
            End Using
        Catch ex As Exception
            Throw
        End Try
    End Sub

    Private Sub frmIngCotizacionObs_KeyDown(sender As Object, e As KeyEventArgs) Handles MyBase.KeyDown
        Select Case e.KeyCode
            Case Keys.F6
                If btnAceptar.Enabled Then btnAceptar_Click(Nothing, Nothing)
            Case Keys.Escape
                btnSalir_Click(Nothing, Nothing)
        End Select
    End Sub

    Private Sub frmIngCotizacionObs_FormClosing(sender As Object, e As FormClosingEventArgs) Handles MyBase.FormClosing
        If blnCambios = True Then
            If MessageBox.Show("Se han realizado cambios. ¿Está Ud. seguro de salir sin Aceptar?", "Confirmar", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = Windows.Forms.DialogResult.Yes Then
                e.Cancel = False
            Else
                e.Cancel = True
                Me.Show()
            End If
        End If
    End Sub

    Private Sub btnAceptar_Click(sender As Object, e As EventArgs) Handles btnAceptar.Click
        Try
            blnCambios = False
            strObservacion = txtObservaciones.Text.Trim
            DialogResult = Windows.Forms.DialogResult.OK
            Me.Hide()
        Catch ex As Exception
            MessageBox.Show(ex.Message, ex.Source, MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub btnSalir_Click(sender As Object, e As EventArgs) Handles btnSalir.Click

        Me.Hide()
        DialogResult = Windows.Forms.DialogResult.Cancel
    End Sub

    Private Sub txtObservaciones_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtObservaciones.KeyPress
        If blnLoad Then blnCambios = True
    End Sub

    Private Sub txtObservaciones_KeyDown(sender As Object, e As KeyEventArgs) Handles txtObservaciones.KeyDown
        If blnLoad Then blnCambios = True
    End Sub
End Class