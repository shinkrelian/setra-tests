﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmMantCotizacionVuelosVentas
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmMantCotizacionVuelosVentas))
        Me.btnSalir = New System.Windows.Forms.Button()
        Me.btnAceptar = New System.Windows.Forms.Button()
        Me.gpGeneral = New System.Windows.Forms.GroupBox()
        Me.grbOperador = New System.Windows.Forms.GroupBox()
        Me.txtCostosOperador = New System.Windows.Forms.TextBox()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.cboOperador = New System.Windows.Forms.ComboBox()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.chkEmitidoOperador = New System.Windows.Forms.CheckBox()
        Me.chkEmitidoSetours = New System.Windows.Forms.CheckBox()
        Me.lblTitulo = New System.Windows.Forms.Label()
        Me.GroupBox6 = New System.Windows.Forms.GroupBox()
        Me.txtServicio = New System.Windows.Forms.TextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.btnProveedor = New System.Windows.Forms.Button()
        Me.lblProveedor = New System.Windows.Forms.Label()
        Me.cboLineaP = New System.Windows.Forms.ComboBox()
        Me.lblProv = New System.Windows.Forms.Label()
        Me.lblNroVueloEtiq = New System.Windows.Forms.Label()
        Me.txtVuelo = New System.Windows.Forms.TextBox()
        Me.gpFechas = New System.Windows.Forms.GroupBox()
        Me.dtpDia = New System.Windows.Forms.DateTimePicker()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.nudMasLlegada = New System.Windows.Forms.NumericUpDown()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.dtpSalida = New System.Windows.Forms.DateTimePicker()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.dtpLlegada = New System.Windows.Forms.DateTimePicker()
        Me.Label41 = New System.Windows.Forms.Label()
        Me.GroupBox4 = New System.Windows.Forms.GroupBox()
        Me.lblIdaVuelta = New System.Windows.Forms.Label()
        Me.cboIdaVuelta = New System.Windows.Forms.ComboBox()
        Me.txtTarifa = New System.Windows.Forms.TextBox()
        Me.lblTarifaVentasEtiq = New System.Windows.Forms.Label()
        Me.txtRuta = New System.Windows.Forms.TextBox()
        Me.txtCodReserva = New System.Windows.Forms.TextBox()
        Me.Label18 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.cboRutaO = New System.Windows.Forms.ComboBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.cboRutaD = New System.Windows.Forms.ComboBox()
        Me.Label38 = New System.Windows.Forms.Label()
        Me.ErrPrv = New System.Windows.Forms.ErrorProvider(Me.components)
        Me.ErrPrvCosto = New System.Windows.Forms.ErrorProvider(Me.components)
        Me.chkMotivo = New System.Windows.Forms.CheckBox()
        Me.txtDescMotivo = New System.Windows.Forms.TextBox()
        Me.gpGeneral.SuspendLayout()
        Me.grbOperador.SuspendLayout()
        Me.GroupBox6.SuspendLayout()
        Me.gpFechas.SuspendLayout()
        CType(Me.nudMasLlegada, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox4.SuspendLayout()
        CType(Me.ErrPrv, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ErrPrvCosto, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'btnSalir
        '
        Me.btnSalir.Image = Global.SETours.My.Resources.Resources.door_out
        Me.btnSalir.Location = New System.Drawing.Point(3, 33)
        Me.btnSalir.Name = "btnSalir"
        Me.btnSalir.Size = New System.Drawing.Size(27, 27)
        Me.btnSalir.TabIndex = 23
        Me.btnSalir.UseVisualStyleBackColor = True
        '
        'btnAceptar
        '
        Me.btnAceptar.Enabled = False
        Me.btnAceptar.Image = Global.SETours.My.Resources.Resources.accept
        Me.btnAceptar.Location = New System.Drawing.Point(3, 4)
        Me.btnAceptar.Name = "btnAceptar"
        Me.btnAceptar.Size = New System.Drawing.Size(27, 27)
        Me.btnAceptar.TabIndex = 22
        Me.btnAceptar.UseVisualStyleBackColor = True
        '
        'gpGeneral
        '
        Me.gpGeneral.BackColor = System.Drawing.Color.White
        Me.gpGeneral.Controls.Add(Me.grbOperador)
        Me.gpGeneral.Controls.Add(Me.chkEmitidoOperador)
        Me.gpGeneral.Controls.Add(Me.chkEmitidoSetours)
        Me.gpGeneral.Controls.Add(Me.lblTitulo)
        Me.gpGeneral.Controls.Add(Me.GroupBox6)
        Me.gpGeneral.Controls.Add(Me.gpFechas)
        Me.gpGeneral.Controls.Add(Me.GroupBox4)
        Me.gpGeneral.Location = New System.Drawing.Point(36, 0)
        Me.gpGeneral.Name = "gpGeneral"
        Me.gpGeneral.Size = New System.Drawing.Size(607, 316)
        Me.gpGeneral.TabIndex = 21
        Me.gpGeneral.TabStop = False
        '
        'grbOperador
        '
        Me.grbOperador.Controls.Add(Me.txtDescMotivo)
        Me.grbOperador.Controls.Add(Me.chkMotivo)
        Me.grbOperador.Controls.Add(Me.txtCostosOperador)
        Me.grbOperador.Controls.Add(Me.Label8)
        Me.grbOperador.Controls.Add(Me.cboOperador)
        Me.grbOperador.Controls.Add(Me.Label7)
        Me.grbOperador.Location = New System.Drawing.Point(7, 256)
        Me.grbOperador.Name = "grbOperador"
        Me.grbOperador.Size = New System.Drawing.Size(590, 46)
        Me.grbOperador.TabIndex = 20
        Me.grbOperador.TabStop = False
        Me.grbOperador.Tag = "Borrar"
        '
        'txtCostosOperador
        '
        Me.txtCostosOperador.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtCostosOperador.Location = New System.Drawing.Point(467, 18)
        Me.txtCostosOperador.MaxLength = 8
        Me.txtCostosOperador.Name = "txtCostosOperador"
        Me.txtCostosOperador.Size = New System.Drawing.Size(79, 21)
        Me.txtCostosOperador.TabIndex = 16
        Me.txtCostosOperador.Tag = "Borrar"
        Me.txtCostosOperador.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.Label8.Location = New System.Drawing.Point(422, 21)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(39, 13)
        Me.Label8.TabIndex = 285
        Me.Label8.Text = "Costo"
        '
        'cboOperador
        '
        Me.cboOperador.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboOperador.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboOperador.FormattingEnabled = True
        Me.cboOperador.Location = New System.Drawing.Point(87, 18)
        Me.cboOperador.Name = "cboOperador"
        Me.cboOperador.Size = New System.Drawing.Size(236, 21)
        Me.cboOperador.TabIndex = 15
        Me.cboOperador.Tag = "Borrar"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.Label7.Location = New System.Drawing.Point(6, 21)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(60, 13)
        Me.Label7.TabIndex = 200
        Me.Label7.Text = "Operador"
        '
        'chkEmitidoOperador
        '
        Me.chkEmitidoOperador.AutoSize = True
        Me.chkEmitidoOperador.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkEmitidoOperador.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.chkEmitidoOperador.Location = New System.Drawing.Point(438, 66)
        Me.chkEmitidoOperador.Name = "chkEmitidoOperador"
        Me.chkEmitidoOperador.Size = New System.Drawing.Size(115, 17)
        Me.chkEmitidoOperador.TabIndex = 6
        Me.chkEmitidoOperador.Tag = "Borrar"
        Me.chkEmitidoOperador.Text = "Emitir Operador"
        Me.chkEmitidoOperador.UseVisualStyleBackColor = True
        '
        'chkEmitidoSetours
        '
        Me.chkEmitidoSetours.AutoSize = True
        Me.chkEmitidoSetours.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkEmitidoSetours.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.chkEmitidoSetours.Location = New System.Drawing.Point(438, 43)
        Me.chkEmitidoSetours.Name = "chkEmitidoSetours"
        Me.chkEmitidoSetours.Size = New System.Drawing.Size(106, 17)
        Me.chkEmitidoSetours.TabIndex = 5
        Me.chkEmitidoSetours.Tag = "Borrar"
        Me.chkEmitidoSetours.Text = "Emitir Setours"
        Me.chkEmitidoSetours.UseVisualStyleBackColor = True
        '
        'lblTitulo
        '
        Me.lblTitulo.BackColor = System.Drawing.SystemColors.ControlDarkDark
        Me.lblTitulo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.lblTitulo.Dock = System.Windows.Forms.DockStyle.Top
        Me.lblTitulo.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTitulo.ForeColor = System.Drawing.Color.White
        Me.lblTitulo.Location = New System.Drawing.Point(3, 16)
        Me.lblTitulo.Name = "lblTitulo"
        Me.lblTitulo.Size = New System.Drawing.Size(601, 22)
        Me.lblTitulo.TabIndex = 196
        Me.lblTitulo.Text = "Nuevo"
        '
        'GroupBox6
        '
        Me.GroupBox6.Controls.Add(Me.txtServicio)
        Me.GroupBox6.Controls.Add(Me.Label4)
        Me.GroupBox6.Controls.Add(Me.btnProveedor)
        Me.GroupBox6.Controls.Add(Me.lblProveedor)
        Me.GroupBox6.Controls.Add(Me.cboLineaP)
        Me.GroupBox6.Controls.Add(Me.lblProv)
        Me.GroupBox6.Controls.Add(Me.lblNroVueloEtiq)
        Me.GroupBox6.Controls.Add(Me.txtVuelo)
        Me.GroupBox6.Location = New System.Drawing.Point(7, 83)
        Me.GroupBox6.Name = "GroupBox6"
        Me.GroupBox6.Size = New System.Drawing.Size(556, 73)
        Me.GroupBox6.TabIndex = 18
        Me.GroupBox6.TabStop = False
        Me.GroupBox6.Tag = "Borrar"
        '
        'txtServicio
        '
        Me.txtServicio.BackColor = System.Drawing.SystemColors.Window
        Me.txtServicio.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtServicio.Location = New System.Drawing.Point(252, 40)
        Me.txtServicio.MaxLength = 250
        Me.txtServicio.Name = "txtServicio"
        Me.txtServicio.Size = New System.Drawing.Size(298, 21)
        Me.txtServicio.TabIndex = 9
        Me.txtServicio.Tag = "Borrar"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.Label4.Location = New System.Drawing.Point(194, 43)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(52, 13)
        Me.Label4.TabIndex = 298
        Me.Label4.Text = "Servicio"
        '
        'btnProveedor
        '
        Me.btnProveedor.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnProveedor.Location = New System.Drawing.Point(318, 15)
        Me.btnProveedor.Name = "btnProveedor"
        Me.btnProveedor.Size = New System.Drawing.Size(28, 22)
        Me.btnProveedor.TabIndex = 6
        Me.btnProveedor.Tag = "Borrar"
        Me.btnProveedor.Text = "..."
        Me.btnProveedor.UseVisualStyleBackColor = True
        '
        'lblProveedor
        '
        Me.lblProveedor.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.lblProveedor.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblProveedor.Location = New System.Drawing.Point(87, 15)
        Me.lblProveedor.Name = "lblProveedor"
        Me.lblProveedor.Size = New System.Drawing.Size(225, 18)
        Me.lblProveedor.TabIndex = 6
        '
        'cboLineaP
        '
        Me.cboLineaP.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboLineaP.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboLineaP.FormattingEnabled = True
        Me.cboLineaP.Location = New System.Drawing.Point(349, 15)
        Me.cboLineaP.Name = "cboLineaP"
        Me.cboLineaP.Size = New System.Drawing.Size(201, 21)
        Me.cboLineaP.TabIndex = 7
        Me.cboLineaP.Tag = "Borrar"
        '
        'lblProv
        '
        Me.lblProv.AutoSize = True
        Me.lblProv.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblProv.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.lblProv.Location = New System.Drawing.Point(6, 16)
        Me.lblProv.Name = "lblProv"
        Me.lblProv.Size = New System.Drawing.Size(66, 13)
        Me.lblProv.TabIndex = 295
        Me.lblProv.Text = "Proveedor"
        '
        'lblNroVueloEtiq
        '
        Me.lblNroVueloEtiq.AutoSize = True
        Me.lblNroVueloEtiq.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblNroVueloEtiq.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.lblNroVueloEtiq.Location = New System.Drawing.Point(6, 43)
        Me.lblNroVueloEtiq.Name = "lblNroVueloEtiq"
        Me.lblNroVueloEtiq.Size = New System.Drawing.Size(66, 13)
        Me.lblNroVueloEtiq.TabIndex = 203
        Me.lblNroVueloEtiq.Text = "Vuelo Nro. "
        '
        'txtVuelo
        '
        Me.txtVuelo.BackColor = System.Drawing.SystemColors.Window
        Me.txtVuelo.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtVuelo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtVuelo.Location = New System.Drawing.Point(87, 40)
        Me.txtVuelo.MaxLength = 25
        Me.txtVuelo.Name = "txtVuelo"
        Me.txtVuelo.Size = New System.Drawing.Size(96, 21)
        Me.txtVuelo.TabIndex = 8
        Me.txtVuelo.Tag = "Borrar"
        '
        'gpFechas
        '
        Me.gpFechas.Controls.Add(Me.dtpDia)
        Me.gpFechas.Controls.Add(Me.Label5)
        Me.gpFechas.Controls.Add(Me.nudMasLlegada)
        Me.gpFechas.Controls.Add(Me.Label3)
        Me.gpFechas.Controls.Add(Me.dtpSalida)
        Me.gpFechas.Controls.Add(Me.Label6)
        Me.gpFechas.Controls.Add(Me.dtpLlegada)
        Me.gpFechas.Controls.Add(Me.Label41)
        Me.gpFechas.Location = New System.Drawing.Point(7, 36)
        Me.gpFechas.Name = "gpFechas"
        Me.gpFechas.Size = New System.Drawing.Size(424, 45)
        Me.gpFechas.TabIndex = 17
        Me.gpFechas.TabStop = False
        Me.gpFechas.Tag = "Borrar"
        '
        'dtpDia
        '
        Me.dtpDia.CalendarFont = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpDia.Enabled = False
        Me.dtpDia.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpDia.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpDia.Location = New System.Drawing.Point(35, 14)
        Me.dtpDia.Name = "dtpDia"
        Me.dtpDia.Size = New System.Drawing.Size(77, 21)
        Me.dtpDia.TabIndex = 1
        Me.dtpDia.Tag = "Borrar"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.Label5.Location = New System.Drawing.Point(6, 16)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(25, 13)
        Me.Label5.TabIndex = 297
        Me.Label5.Text = "Día"
        '
        'nudMasLlegada
        '
        Me.nudMasLlegada.BackColor = System.Drawing.SystemColors.Window
        Me.nudMasLlegada.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.nudMasLlegada.Location = New System.Drawing.Point(382, 15)
        Me.nudMasLlegada.Maximum = New Decimal(New Integer() {2, 0, 0, 0})
        Me.nudMasLlegada.Name = "nudMasLlegada"
        Me.nudMasLlegada.Size = New System.Drawing.Size(32, 21)
        Me.nudMasLlegada.TabIndex = 4
        Me.nudMasLlegada.Tag = "Borrar"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.Label3.Location = New System.Drawing.Point(369, 15)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(16, 13)
        Me.Label3.TabIndex = 281
        Me.Label3.Text = "+"
        '
        'dtpSalida
        '
        Me.dtpSalida.CustomFormat = "HH:mm"
        Me.dtpSalida.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.dtpSalida.Location = New System.Drawing.Point(165, 15)
        Me.dtpSalida.Name = "dtpSalida"
        Me.dtpSalida.ShowUpDown = True
        Me.dtpSalida.Size = New System.Drawing.Size(69, 20)
        Me.dtpSalida.TabIndex = 2
        Me.dtpSalida.Tag = "Borrar"
        Me.dtpSalida.Value = New Date(2012, 8, 23, 0, 0, 0, 0)
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.Label6.Location = New System.Drawing.Point(240, 15)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(51, 13)
        Me.Label6.TabIndex = 210
        Me.Label6.Text = "Llegada"
        '
        'dtpLlegada
        '
        Me.dtpLlegada.CustomFormat = "HH:mm"
        Me.dtpLlegada.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.dtpLlegada.Location = New System.Drawing.Point(298, 15)
        Me.dtpLlegada.Name = "dtpLlegada"
        Me.dtpLlegada.ShowUpDown = True
        Me.dtpLlegada.Size = New System.Drawing.Size(69, 20)
        Me.dtpLlegada.TabIndex = 3
        Me.dtpLlegada.Tag = "Borrar"
        Me.dtpLlegada.Value = New Date(2012, 8, 23, 0, 0, 0, 0)
        '
        'Label41
        '
        Me.Label41.AutoSize = True
        Me.Label41.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label41.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.Label41.Location = New System.Drawing.Point(118, 15)
        Me.Label41.Name = "Label41"
        Me.Label41.Size = New System.Drawing.Size(41, 13)
        Me.Label41.TabIndex = 280
        Me.Label41.Text = "Salida"
        '
        'GroupBox4
        '
        Me.GroupBox4.Controls.Add(Me.lblIdaVuelta)
        Me.GroupBox4.Controls.Add(Me.cboIdaVuelta)
        Me.GroupBox4.Controls.Add(Me.txtTarifa)
        Me.GroupBox4.Controls.Add(Me.lblTarifaVentasEtiq)
        Me.GroupBox4.Controls.Add(Me.txtRuta)
        Me.GroupBox4.Controls.Add(Me.txtCodReserva)
        Me.GroupBox4.Controls.Add(Me.Label18)
        Me.GroupBox4.Controls.Add(Me.Label1)
        Me.GroupBox4.Controls.Add(Me.cboRutaO)
        Me.GroupBox4.Controls.Add(Me.Label2)
        Me.GroupBox4.Controls.Add(Me.cboRutaD)
        Me.GroupBox4.Controls.Add(Me.Label38)
        Me.GroupBox4.Location = New System.Drawing.Point(7, 155)
        Me.GroupBox4.Name = "GroupBox4"
        Me.GroupBox4.Size = New System.Drawing.Size(590, 101)
        Me.GroupBox4.TabIndex = 19
        Me.GroupBox4.TabStop = False
        Me.GroupBox4.Tag = "Borrar"
        '
        'lblIdaVuelta
        '
        Me.lblIdaVuelta.AutoSize = True
        Me.lblIdaVuelta.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblIdaVuelta.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.lblIdaVuelta.Location = New System.Drawing.Point(6, 76)
        Me.lblIdaVuelta.Name = "lblIdaVuelta"
        Me.lblIdaVuelta.Size = New System.Drawing.Size(75, 13)
        Me.lblIdaVuelta.TabIndex = 285
        Me.lblIdaVuelta.Text = "Ida o Vuelta"
        Me.lblIdaVuelta.Visible = False
        '
        'cboIdaVuelta
        '
        Me.cboIdaVuelta.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboIdaVuelta.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboIdaVuelta.FormattingEnabled = True
        Me.cboIdaVuelta.Location = New System.Drawing.Point(87, 73)
        Me.cboIdaVuelta.Name = "cboIdaVuelta"
        Me.cboIdaVuelta.Size = New System.Drawing.Size(107, 21)
        Me.cboIdaVuelta.TabIndex = 15
        Me.cboIdaVuelta.Tag = "Borrar"
        Me.cboIdaVuelta.Visible = False
        '
        'txtTarifa
        '
        Me.txtTarifa.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtTarifa.Location = New System.Drawing.Point(467, 46)
        Me.txtTarifa.Name = "txtTarifa"
        Me.txtTarifa.Size = New System.Drawing.Size(79, 21)
        Me.txtTarifa.TabIndex = 14
        Me.txtTarifa.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lblTarifaVentasEtiq
        '
        Me.lblTarifaVentasEtiq.AutoSize = True
        Me.lblTarifaVentasEtiq.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTarifaVentasEtiq.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.lblTarifaVentasEtiq.Location = New System.Drawing.Point(380, 49)
        Me.lblTarifaVentasEtiq.Name = "lblTarifaVentasEtiq"
        Me.lblTarifaVentasEtiq.Size = New System.Drawing.Size(82, 13)
        Me.lblTarifaVentasEtiq.TabIndex = 229
        Me.lblTarifaVentasEtiq.Text = "Tarifa Ventas"
        '
        'txtRuta
        '
        Me.txtRuta.BackColor = System.Drawing.SystemColors.Window
        Me.txtRuta.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtRuta.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtRuta.Location = New System.Drawing.Point(87, 46)
        Me.txtRuta.MaxLength = 25
        Me.txtRuta.Name = "txtRuta"
        Me.txtRuta.Size = New System.Drawing.Size(287, 21)
        Me.txtRuta.TabIndex = 13
        Me.txtRuta.Tag = "Borrar"
        '
        'txtCodReserva
        '
        Me.txtCodReserva.BackColor = System.Drawing.SystemColors.Window
        Me.txtCodReserva.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtCodReserva.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtCodReserva.Location = New System.Drawing.Point(467, 19)
        Me.txtCodReserva.MaxLength = 20
        Me.txtCodReserva.Name = "txtCodReserva"
        Me.txtCodReserva.Size = New System.Drawing.Size(117, 21)
        Me.txtCodReserva.TabIndex = 12
        Me.txtCodReserva.Tag = "Borrar"
        Me.txtCodReserva.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label18
        '
        Me.Label18.AutoSize = True
        Me.Label18.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label18.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.Label18.Location = New System.Drawing.Point(380, 22)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(81, 13)
        Me.Label18.TabIndex = 228
        Me.Label18.Text = "Cod. Reserva"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.Label1.Location = New System.Drawing.Point(6, 22)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(44, 13)
        Me.Label1.TabIndex = 199
        Me.Label1.Text = "Origen"
        '
        'cboRutaO
        '
        Me.cboRutaO.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboRutaO.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboRutaO.FormattingEnabled = True
        Me.cboRutaO.Location = New System.Drawing.Point(87, 19)
        Me.cboRutaO.Name = "cboRutaO"
        Me.cboRutaO.Size = New System.Drawing.Size(107, 21)
        Me.cboRutaO.TabIndex = 10
        Me.cboRutaO.Tag = "Borrar"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.Label2.Location = New System.Drawing.Point(200, 22)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(50, 13)
        Me.Label2.TabIndex = 202
        Me.Label2.Text = "Destino"
        '
        'cboRutaD
        '
        Me.cboRutaD.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboRutaD.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboRutaD.FormattingEnabled = True
        Me.cboRutaD.Location = New System.Drawing.Point(252, 19)
        Me.cboRutaD.Name = "cboRutaD"
        Me.cboRutaD.Size = New System.Drawing.Size(107, 21)
        Me.cboRutaD.TabIndex = 11
        Me.cboRutaD.Tag = "Borrar"
        '
        'Label38
        '
        Me.Label38.AutoSize = True
        Me.Label38.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label38.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.Label38.Location = New System.Drawing.Point(6, 49)
        Me.Label38.Name = "Label38"
        Me.Label38.Size = New System.Drawing.Size(34, 13)
        Me.Label38.TabIndex = 283
        Me.Label38.Text = "Ruta"
        '
        'ErrPrv
        '
        Me.ErrPrv.ContainerControl = Me
        '
        'ErrPrvCosto
        '
        Me.ErrPrvCosto.ContainerControl = Me
        Me.ErrPrvCosto.Icon = CType(resources.GetObject("ErrPrvCosto.Icon"), System.Drawing.Icon)
        '
        'chkMotivo
        '
        Me.chkMotivo.AutoSize = True
        Me.chkMotivo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkMotivo.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.chkMotivo.Location = New System.Drawing.Point(9, 50)
        Me.chkMotivo.Name = "chkMotivo"
        Me.chkMotivo.Size = New System.Drawing.Size(68, 17)
        Me.chkMotivo.TabIndex = 286
        Me.chkMotivo.Tag = "Borrar"
        Me.chkMotivo.Text = "Motivo:"
        Me.chkMotivo.UseVisualStyleBackColor = True
        Me.chkMotivo.Visible = False
        '
        'txtDescMotivo
        '
        Me.txtDescMotivo.BackColor = System.Drawing.SystemColors.Window
        Me.txtDescMotivo.Enabled = False
        Me.txtDescMotivo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtDescMotivo.Location = New System.Drawing.Point(87, 48)
        Me.txtDescMotivo.MaxLength = 200
        Me.txtDescMotivo.Multiline = True
        Me.txtDescMotivo.Name = "txtDescMotivo"
        Me.txtDescMotivo.Size = New System.Drawing.Size(302, 46)
        Me.txtDescMotivo.TabIndex = 288
        Me.txtDescMotivo.Tag = "Borrar"
        Me.txtDescMotivo.Visible = False
        '
        'frmMantCotizacionVuelosVentas
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(645, 320)
        Me.Controls.Add(Me.gpGeneral)
        Me.Controls.Add(Me.btnSalir)
        Me.Controls.Add(Me.btnAceptar)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmMantCotizacionVuelosVentas"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Ingreso de Vuelos de Cotización"
        Me.gpGeneral.ResumeLayout(False)
        Me.gpGeneral.PerformLayout()
        Me.grbOperador.ResumeLayout(False)
        Me.grbOperador.PerformLayout()
        Me.GroupBox6.ResumeLayout(False)
        Me.GroupBox6.PerformLayout()
        Me.gpFechas.ResumeLayout(False)
        Me.gpFechas.PerformLayout()
        CType(Me.nudMasLlegada, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox4.ResumeLayout(False)
        Me.GroupBox4.PerformLayout()
        CType(Me.ErrPrv, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ErrPrvCosto, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents btnSalir As System.Windows.Forms.Button
    Friend WithEvents btnAceptar As System.Windows.Forms.Button
    Friend WithEvents gpGeneral As System.Windows.Forms.GroupBox
    Friend WithEvents GroupBox6 As System.Windows.Forms.GroupBox
    Friend WithEvents GroupBox4 As System.Windows.Forms.GroupBox
    Friend WithEvents txtRuta As System.Windows.Forms.TextBox
    Friend WithEvents lblNroVueloEtiq As System.Windows.Forms.Label
    Friend WithEvents txtVuelo As System.Windows.Forms.TextBox
    Friend WithEvents txtCodReserva As System.Windows.Forms.TextBox
    Friend WithEvents Label18 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents cboRutaO As System.Windows.Forms.ComboBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents cboRutaD As System.Windows.Forms.ComboBox
    Friend WithEvents Label38 As System.Windows.Forms.Label
    Friend WithEvents gpFechas As System.Windows.Forms.GroupBox
    Friend WithEvents dtpSalida As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents dtpLlegada As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label41 As System.Windows.Forms.Label
    Friend WithEvents txtTarifa As System.Windows.Forms.TextBox
    Friend WithEvents lblTarifaVentasEtiq As System.Windows.Forms.Label
    Friend WithEvents lblTitulo As System.Windows.Forms.Label
    Friend WithEvents ErrPrv As System.Windows.Forms.ErrorProvider
    Friend WithEvents cboLineaP As System.Windows.Forms.ComboBox
    Friend WithEvents lblProv As System.Windows.Forms.Label
    Friend WithEvents lblProveedor As System.Windows.Forms.Label
    Friend WithEvents chkEmitidoSetours As System.Windows.Forms.CheckBox
    Friend WithEvents nudMasLlegada As System.Windows.Forms.NumericUpDown
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents dtpDia As System.Windows.Forms.DateTimePicker
    Friend WithEvents btnProveedor As System.Windows.Forms.Button
    Friend WithEvents txtServicio As System.Windows.Forms.TextBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents chkEmitidoOperador As System.Windows.Forms.CheckBox
    Friend WithEvents grbOperador As System.Windows.Forms.GroupBox
    Friend WithEvents cboOperador As System.Windows.Forms.ComboBox
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents txtCostosOperador As System.Windows.Forms.TextBox
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents lblIdaVuelta As System.Windows.Forms.Label
    Friend WithEvents cboIdaVuelta As System.Windows.Forms.ComboBox
    Friend WithEvents ErrPrvCosto As System.Windows.Forms.ErrorProvider
    Friend WithEvents chkMotivo As System.Windows.Forms.CheckBox
    Friend WithEvents txtDescMotivo As System.Windows.Forms.TextBox
End Class
