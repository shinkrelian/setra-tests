﻿Imports ComSEToursBL2
Imports ComSEToursBE2
Module modVentas
    Public gintIDCabCopiaraPedido As Integer = 0
    Public gdatFechaCopiaraPedido As Date = "01/01/1900"
    Public gblnTodosVuelosCopiaraPedido As Boolean = False
    Public Sub CargarCombosTransferOrigenDestino(ByVal vfrmRef As frmIngCotizacionDato, ByVal vdatFechaDiaCotiDet As Date, ByRef rCboOrigen As ComboBox, ByRef rCboDestino As ComboBox, ByVal vintIDCab As Integer)
        Try
            Dim dttOrigen As DataTable = dttComboTransferOrigenDestino(vfrmRef, vdatFechaDiaCotiDet, vintIDCab, True)

            If dttOrigen.Rows.Count > 0 Then
                dttOrigen.Rows.Add("", "")
                pCargaCombosBox(rCboOrigen, dttOrigen)

                'Dim dttDestino As DataTable = dttOrigen.Clone
                'For i As Byte = 0 To dttOrigen.Rows.Count - 1

                '    dttDestino.ImportRow(dttOrigen.Rows(i))
                '    If dttOrigen(i)(0) <> "" Then
                '        dttDestino.Rows(i)(1) = dttOrigen.Rows(i)(1) & " - E2"
                '    End If
                'Next
                'pCargaCombosBox(rCboDestino, dttDestino)
            End If

            Dim dttDestino As DataTable = dttComboTransferOrigenDestino(vfrmRef, vdatFechaDiaCotiDet, vintIDCab, False)
            If dttDestino.Rows.Count > 0 Then
                dttDestino.Rows.Add("", "")
                pCargaCombosBox(rCboDestino, dttDestino)
            End If

        Catch ex As System.Exception
            Throw
        End Try
    End Sub
    '    Private Function dttComboTransferOrigenDestino(ByVal vfrmRef As frmIngCotizacionDato, ByVal vbytFila As Byte) As DataTable
    '        Try

    '            Dim dttOrigenDestino As New DataTable("Transfer")
    '            With dttOrigenDestino
    '                .Columns.Add("IDDet")
    '                .Columns.Add("TextoVer")
    '            End With

    '            'Dim datFechaDiaCotiDet As Date = vfrmRef.dgvDet.CurrentRow.Cells("Dia").Value.ToString.Substring(0, 10)
    '            'Dim datFechaDiaCotiDetBusqueda As Date = DateAdd(DateInterval.Day, -1, datFechaDiaCotiDet)
    '            Dim datFechaDiaCotiDet As Date = vfrmRef.dgvDet.Item("Dia", vbytFila).Value.ToString.Substring(0, 10)
    '            Dim datFechaDiaCotiDetBusqueda As Date = DateAdd(DateInterval.Day, -1, datFechaDiaCotiDet)


    '            Dim strTextoVer As String, strTextoDesc As String = ""
    '            Dim strProveedor As String, strOrigen As String, strDestino As String
    '            Dim strVuelo As String, strHora As String
    '            Dim blnUbicado As Boolean
    '            Dim bytContGrid As Byte = 1
    '            'VUELOS
    'Vuelos:
    '            With vfrmRef.dgvVuelos
    '                For Each DgvFila As DataGridViewRow In .Rows
    '                    blnUbicado = False
    '                    If DgvFila.Cells("DiaV").Value Is Nothing Then
    '                        If Not DgvFila.Cells("FechaRetorno").Value Is Nothing Then
    '                            If Format(DgvFila.Cells("FechaRetorno").Value, "dd/MM/yyyy") = datFechaDiaCotiDetBusqueda Then
    '                                blnUbicado = True
    '                            End If
    '                        End If
    '                    Else
    '                        If Not DgvFila.Cells("DiaV").Value Is Nothing Then
    '                            If DgvFila.Cells("DiaV").Value.ToString.Length > 10 Then
    '                                If DgvFila.Cells("DiaV").Value.ToString.Substring(4, 10) = datFechaDiaCotiDetBusqueda Then
    '                                    blnUbicado = True
    '                                End If
    '                            Else
    '                                If DgvFila.Cells("DiaV").Value.ToString = datFechaDiaCotiDetBusqueda Then
    '                                    blnUbicado = True
    '                                End If
    '                            End If

    '                        End If
    '                    End If
    '                    If blnUbicado Then
    '                        'If Not DgvFila.Cells("DescProveedorV").Value Is Nothing Then
    '                        '    strProveedor = DgvFila.Cells("DescProveedorV").Value.ToString & " ("
    '                        'Else
    '                        '    strProveedor = ""
    '                        'End If
    '                        If Not DgvFila.Cells("RutaOAbrV").Value Is Nothing Then
    '                            strOrigen = DgvFila.Cells("RutaOAbrV").Value.ToString & "/"
    '                            'strOrigen = Replace(strOrigen, "NO APLICA/", "")
    '                        Else
    '                            strOrigen = ""
    '                        End If
    '                        If Not DgvFila.Cells("RutaDAbrV").Value Is Nothing Then
    '                            strDestino = DgvFila.Cells("RutaDAbrV").Value.ToString & ")"
    '                            'strDestino = Replace(strDestino, "NO APLICA", "")
    '                        Else
    '                            strDestino = ""
    '                        End If
    '                        'strTextoVer = strProveedor & strOrigen & strDestino
    '                        If Not DgvFila.Cells("Vuelo").Value Is Nothing Then
    '                            strVuelo = DgvFila.Cells("Vuelo").Value.ToString
    '                        Else
    '                            strVuelo = ""
    '                        End If
    '                        If Not DgvFila.Cells("HrRecojo").Value Is Nothing Then
    '                            strHora = DgvFila.Cells("HrRecojo").Value.ToString
    '                        Else
    '                            strHora = ""
    '                        End If
    '                        'strTextoDesc = "AEROPUERTO " & strProveedor & strVuelo & " " & strOrigen & strDestino & " " & strHora
    '                        'strTextoDesc = strProveedor & strVuelo & " " & strOrigen & strDestino & " " & strHora
    '                        strTextoDesc = "Airport (" & strVuelo & " " & strOrigen & strDestino '& " " & strHora

    '                        If strTextoDesc.Trim <> "" Then
    '                            'If strTextoDesc.Trim.Substring(strTextoDesc.Trim.Length - 3, 3) = "(/)" Then
    '                            '    strTextoDesc = strTextoDesc.Substring(0, strTextoDesc.Length - 4)
    '                            'End If

    '                            'Dim dttTemp As DataTable = dtFiltrarDataTable(dttOrigenDestino, "TextoVer='" & strTextoDesc & "'")
    '                            'If dttTemp.Rows.Count = 0 Then
    '                            dttOrigenDestino.Rows.Add(DgvFila.Cells("IDDetV").Value.ToString, strTextoDesc)
    '                            'End If
    '                        End If
    '                    End If
    '                Next

    '            End With
    '            bytContGrid += 1
    '            If bytContGrid = 2 Then
    '                datFechaDiaCotiDetBusqueda = datFechaDiaCotiDet
    '                GoTo Vuelos
    '            ElseIf bytContGrid = 3 Then
    '                datFechaDiaCotiDetBusqueda = DateAdd(DateInterval.Day, 1, datFechaDiaCotiDet)
    '                GoTo Vuelos
    '            End If

    '            'BUS
    '            bytContGrid = 1
    '            datFechaDiaCotiDetBusqueda = DateAdd(DateInterval.Day, -1, datFechaDiaCotiDet)
    'Buses:
    '            With vfrmRef.dgvBuss
    '                For Each DgvFila As DataGridViewRow In .Rows
    '                    blnUbicado = False
    '                    If DgvFila.Cells("DiaB").Value Is Nothing Then
    '                        If Not DgvFila.Cells("Fec_RetornoB").Value Is Nothing Then
    '                            If Format(DgvFila.Cells("Fec_RetornoB").Value, "dd/MM/yyyy") = datFechaDiaCotiDetBusqueda Then
    '                                blnUbicado = True
    '                            End If
    '                        End If
    '                    Else
    '                        If Not DgvFila.Cells("DiaB").Value Is Nothing Then
    '                            If DgvFila.Cells("DiaB").Value.ToString.Length > 10 Then
    '                                If DgvFila.Cells("DiaB").Value.ToString.Substring(4, 10) = datFechaDiaCotiDetBusqueda Then
    '                                    blnUbicado = True
    '                                End If
    '                            Else
    '                                If DgvFila.Cells("DiaB").Value.ToString = datFechaDiaCotiDetBusqueda Then
    '                                    blnUbicado = True
    '                                End If
    '                            End If
    '                        End If
    '                    End If
    '                    If blnUbicado Then
    '                        If Not DgvFila.Cells("DescProveedorB").Value Is Nothing Then
    '                            strProveedor = DgvFila.Cells("DescProveedorB").Value.ToString
    '                        Else
    '                            strProveedor = ""
    '                        End If
    '                        If Not DgvFila.Cells("RutaOAbrB").Value Is Nothing Then
    '                            strOrigen = DgvFila.Cells("RutaOAbrB").Value.ToString
    '                            'strOrigen = Replace(strOrigen, "NO APLICA/", "")
    '                        Else
    '                            strOrigen = ""
    '                        End If
    '                        If Not DgvFila.Cells("RutaDAbrB").Value Is Nothing Then
    '                            strDestino = DgvFila.Cells("RutaDAbrB").Value.ToString
    '                            'strDestino = Replace(strDestino, "NO APLICA", "")
    '                        Else
    '                            strDestino = ""
    '                        End If

    '                        'strTextoVer = strProveedor & strOrigen & strDestino

    '                        If Not DgvFila.Cells("SalidaB").Value Is Nothing Then
    '                            strHora = DgvFila.Cells("SalidaB").Value.ToString
    '                        Else
    '                            strHora = ""
    '                        End If
    '                        strTextoDesc = strProveedor.ToUpper & " Bus Station (" & strOrigen & "/" & strDestino & " " & strHora & ")"
    '                        If strTextoDesc.Trim <> "" Then
    '                            'If strTextoDesc.Trim.Substring(strTextoDesc.Trim.Length - 3, 3) = "(/)" Then
    '                            '    strTextoDesc = strTextoDesc.Substring(0, strTextoDesc.Length - 4)
    '                            'End If

    '                            'dttOrigenDestino.Rows.Add(strTextoDesc, strTextoVer)
    '                            'Dim dttTemp As DataTable = dtFiltrarDataTable(dttOrigenDestino, "TextoVer='" & strTextoDesc & "'")
    '                            'If dttTemp.Rows.Count = 0 Then
    '                            dttOrigenDestino.Rows.Add(DgvFila.Cells("IDDetB").Value.ToString, strTextoDesc)
    '                            'End If
    '                        End If
    '                    End If
    '                Next

    '            End With
    '            bytContGrid += 1
    '            If bytContGrid = 2 Then
    '                datFechaDiaCotiDetBusqueda = datFechaDiaCotiDet
    '                GoTo Buses
    '            ElseIf bytContGrid = 3 Then
    '                datFechaDiaCotiDetBusqueda = DateAdd(DateInterval.Day, 1, datFechaDiaCotiDet)
    '                GoTo Buses
    '            End If

    '            'TREN
    '            bytContGrid = 1
    '            datFechaDiaCotiDetBusqueda = DateAdd(DateInterval.Day, -1, datFechaDiaCotiDet)
    'Trenes:
    '            With vfrmRef.dgvTren
    '                For Each DgvFila As DataGridViewRow In .Rows
    '                    blnUbicado = False
    '                    If DgvFila.Cells("DiaT").Value Is Nothing Then
    '                        If Not DgvFila.Cells("Fec_RetornoT").Value Is Nothing Then
    '                            If Format(DgvFila.Cells("Fec_RetornoT").Value, "dd/MM/yyyy") = datFechaDiaCotiDetBusqueda Then
    '                                blnUbicado = True
    '                            End If
    '                        End If
    '                    Else
    '                        If Not DgvFila.Cells("DiaT").Value Is Nothing Then
    '                            If DgvFila.Cells("DiaT").Value.ToString.Length > 10 Then
    '                                If DgvFila.Cells("DiaT").Value.ToString.Substring(4, 10) = datFechaDiaCotiDetBusqueda Then
    '                                    blnUbicado = True
    '                                End If
    '                            Else
    '                                If DgvFila.Cells("DiaT").Value.ToString = datFechaDiaCotiDetBusqueda Then
    '                                    blnUbicado = True
    '                                End If
    '                            End If
    '                        End If
    '                    End If
    '                    If blnUbicado Then
    '                        'If Not DgvFila.Cells("DescProveedorT").Value Is Nothing Then
    '                        '    strProveedor = DgvFila.Cells("DescProveedorT").Value.ToString & " ("
    '                        'Else
    '                        '    strProveedor = ""
    '                        'End If
    '                        'If Not DgvFila.Cells("RutaOAbrT").Value Is Nothing Then
    '                        '    strOrigen = DgvFila.Cells("RutaOAbrT").Value.ToString & "/"
    '                        '    'strOrigen = Replace(strOrigen, "NO APLICA/", "")
    '                        'Else
    '                        '    strOrigen = ""
    '                        'End If
    '                        'If Not DgvFila.Cells("RutaDAbrT").Value Is Nothing Then
    '                        '    strDestino = DgvFila.Cells("RutaDAbrT").Value.ToString & ")"
    '                        '    'strDestino = Replace(strDestino, "NO APLICA", "")
    '                        'Else
    '                        '    strDestino = ""
    '                        'End If
    '                        If Not DgvFila.Cells("OrigenT").Value Is Nothing Then
    '                            strOrigen = DgvFila.Cells("OrigenT").Value.ToString
    '                        Else
    '                            strOrigen = ""
    '                        End If
    '                        If Not DgvFila.Cells("VueloT").Value Is Nothing Then
    '                            strVuelo = DgvFila.Cells("VueloT").Value.ToString
    '                        Else
    '                            strVuelo = ""
    '                        End If
    '                        'strTextoVer = strProveedor & strOrigen & strDestino


    '                        If Not DgvFila.Cells("SalidaT").Value Is Nothing Then
    '                            strHora = DgvFila.Cells("SalidaT").Value.ToString
    '                        Else
    '                            strHora = ""
    '                        End If
    '                        'strTextoDesc = strProveedor & strOrigen & strDestino & " " & strHora
    '                        strTextoDesc = strOrigen.ToUpper & " Train Station (" & strVuelo & " " & strHora & ")"

    '                        If strTextoDesc.Trim <> "" Then
    '                            'If strTextoDesc.Trim.Substring(strTextoDesc.Trim.Length - 3, 3) = "(/)" Then
    '                            '    strTextoDesc = strTextoDesc.Substring(0, strTextoDesc.Length - 4)
    '                            'End If

    '                            'dttOrigenDestino.Rows.Add(strTextoDesc, strTextoVer)
    '                            'Dim dttTemp As DataTable = dtFiltrarDataTable(dttOrigenDestino, "TextoVer='" & strTextoDesc & "'")
    '                            'If dttTemp.Rows.Count = 0 Then
    '                            dttOrigenDestino.Rows.Add(DgvFila.Cells("IDDetT").Value.ToString, strTextoDesc)
    '                            'End If
    '                        End If
    '                    End If
    '                Next

    '            End With
    '            bytContGrid += 1
    '            If bytContGrid = 2 Then
    '                datFechaDiaCotiDetBusqueda = datFechaDiaCotiDet
    '                GoTo Trenes
    '            ElseIf bytContGrid = 3 Then
    '                datFechaDiaCotiDetBusqueda = DateAdd(DateInterval.Day, 1, datFechaDiaCotiDet)
    '                GoTo Trenes
    '            End If

    '            'HOTELES

    '            bytContGrid = 1
    '            datFechaDiaCotiDetBusqueda = DateAdd(DateInterval.Day, -1, datFechaDiaCotiDet)
    'Hoteles:
    '            With vfrmRef.dgvDet
    '                For Each DgvFila As DataGridViewRow In .Rows
    '                    blnUbicado = False
    '                    If DgvFila.Cells("IDTipoProv").Value.ToString = gstrTipoProveeHoteles Then
    '                        If DgvFila.Cells("DiaFormat").Value.ToString.Substring(4, 10) = datFechaDiaCotiDetBusqueda Then
    '                            blnUbicado = True
    '                        End If
    '                    End If
    '                    If blnUbicado Then
    '                        If Not DgvFila.Cells("DescProveedor").Value Is Nothing Then
    '                            strProveedor = DgvFila.Cells("DescProveedor").Value.ToString
    '                        Else
    '                            strProveedor = ""
    '                        End If

    '                        If strProveedor <> "" Then
    '                            If strProveedor.Length >= 5 Then
    '                                If strProveedor.Substring(0, 5) = "HOTEL" Then
    '                                    strTextoVer = Mid(strProveedor, 6)
    '                                    strTextoVer = "HTL " & strTextoVer.Trim
    '                                ElseIf strProveedor.Substring(0, 3) = "HTL" Then
    '                                    strTextoVer = Mid(strProveedor, 4)
    '                                    strTextoVer = "HTL " & strTextoVer.Trim

    '                                Else
    '                                    strTextoVer = "HTL " & strProveedor
    '                                End If


    '                            ElseIf strProveedor.Length >= 3 Then

    '                                If strProveedor.Substring(0, 3) = "HTL" Then
    '                                    strTextoVer = Mid(strProveedor, 4)
    '                                    strTextoVer = "HTL " & strTextoVer.Trim
    '                                ElseIf strProveedor.Substring(0, 5) = "HOTEL" Then
    '                                    strTextoVer = Mid(strProveedor, 6)
    '                                    strTextoVer = "HTL " & strTextoVer.Trim
    '                                Else
    '                                    strTextoVer = "HTL " & strProveedor
    '                                End If


    '                            Else
    '                                strTextoVer = strProveedor
    '                            End If


    '                        Else
    '                            strTextoVer = ""
    '                        End If

    '                        strTextoDesc = strTextoVer
    '                        If strTextoDesc.Trim <> "" Then
    '                            'dttOrigenDestino.Rows.Add(strTextoDesc, strTextoVer)
    '                            If strTextoDesc.Trim.Substring(strTextoDesc.Trim.Length - 3, 3) = "- /" Then
    '                                strTextoDesc = strTextoDesc.Substring(0, strTextoDesc.Length - 4)
    '                            End If

    '                            Dim dttTemp As DataTable = dtFiltrarDataTable(dttOrigenDestino, "TextoVer='" & strTextoDesc & "'")
    '                            If dttTemp.Rows.Count = 0 Then
    '                                dttOrigenDestino.Rows.Add(If(DgvFila.Cells("IDDet").Value Is Nothing, "X" & (DgvFila.Index + 1).ToString, DgvFila.Cells("IDDet").Value.ToString), strTextoDesc)
    '                            End If
    '                        End If
    '                    End If
    '                Next

    '            End With
    '            bytContGrid += 1
    '            If bytContGrid = 2 Then
    '                datFechaDiaCotiDetBusqueda = datFechaDiaCotiDet
    '                GoTo Hoteles
    '            ElseIf bytContGrid = 3 Then
    '                datFechaDiaCotiDetBusqueda = DateAdd(DateInterval.Day, 1, datFechaDiaCotiDet)
    '                GoTo Hoteles
    '            End If


    '            'RESTAURANTE

    '            bytContGrid = 1
    '            datFechaDiaCotiDetBusqueda = DateAdd(DateInterval.Day, -1, datFechaDiaCotiDet)
    'Restaurante:
    '            With vfrmRef.dgvDet
    '                For Each DgvFila As DataGridViewRow In .Rows
    '                    blnUbicado = False
    '                    If DgvFila.Cells("IDTipoProv").Value.ToString = gstrTipoProveeRestaurantes Then
    '                        If DgvFila.Cells("DiaFormat").Value.ToString.Substring(4, 10) = datFechaDiaCotiDetBusqueda Then
    '                            blnUbicado = True
    '                        End If
    '                    End If
    '                    If blnUbicado Then
    '                        If Not DgvFila.Cells("DescProveedor").Value Is Nothing Then
    '                            strProveedor = DgvFila.Cells("DescProveedor").Value.ToString
    '                        Else
    '                            strProveedor = ""
    '                        End If
    '                        If strProveedor <> "" Then
    '                            '    If strProveedor.Length >= 10 Then
    '                            '        If strProveedor.Substring(0, 10) = "RESTAURANT" Then
    '                            '            strTextoVer = strProveedor
    '                            '        Else
    '                            '            strTextoVer = "RESTAURANT " & strProveedor
    '                            '        End If
    '                            '    Else
    '                            '        strTextoVer = ""
    '                            '    End If
    '                            'Else
    '                            '    strTextoVer = ""
    '                            strTextoDesc = strProveedor
    '                        End If

    '                        If strTextoDesc.Trim <> "" Then
    '                            'dttOrigenDestino.Rows.Add(strTextoDesc, strTextoVer)
    '                            'Dim dttTemp As DataTable = dtFiltrarDataTable(dttOrigenDestino, "TextoVer='" & strTextoDesc & "'")
    '                            'If dttTemp.Rows.Count = 0 Then
    '                            dttOrigenDestino.Rows.Add(If(DgvFila.Cells("IDDet").Value Is Nothing, "X" & (DgvFila.Index + 1).ToString, DgvFila.Cells("IDDet").Value.ToString), strTextoDesc)
    '                            'End If
    '                        End If
    '                    End If
    '                Next

    '            End With
    '            bytContGrid += 1
    '            If bytContGrid = 2 Then
    '                datFechaDiaCotiDetBusqueda = datFechaDiaCotiDet
    '                GoTo Restaurante
    '            ElseIf bytContGrid = 3 Then
    '                datFechaDiaCotiDetBusqueda = DateAdd(DateInterval.Day, 1, datFechaDiaCotiDet)
    '                GoTo Restaurante
    '            End If

    '            Return dttOrigenDestino

    '        Catch ex As System.Exception
    '            Throw
    '        End Try

    '    End Function

    Private Function dttComboTransferOrigenDestino(ByVal vfrmRef As frmIngCotizacionDato, _
                                                   ByVal vdatFechaDiaCotiDet As Date, _
                                                   ByVal vintIDCab As Integer, _
                                                   ByVal vblnOrigen As Boolean) As DataTable
        Try

            Dim dttOrigenDestino As New DataTable("Transfer")
            With dttOrigenDestino
                .Columns.Add("IDDet")
                .Columns.Add("TextoVer")
            End With

            'Dim datFechaDiaCotiDet As Date = vfrmRef.dgvDet.Item("Dia", vbytFila).Value.ToString.Substring(0, 10)
            Dim datFechaDiaCotiDetBusqueda As Date = DateAdd(DateInterval.Day, -1, vdatFechaDiaCotiDet)
            datFechaDiaCotiDetBusqueda = Format(datFechaDiaCotiDetBusqueda, "dd/MM/yyyy")

            Dim strTextoVer As String, strTextoDesc As String = ""
            Dim strProveedor As String, strOrigen As String, strDestino As String
            Dim strVuelo As String, strHora As String
            Dim blnUbicado As Boolean
            Dim bytContGrid As Byte = 1
            'VUELOS

            Dim dgvVuelos As DataGridView = Nothing
            If vfrmRef Is Nothing Then
                Dim objBNTransp As New clsCotizacionBN.clsCotizacionVuelosBN
                dgvVuelos = dgvColumnasTransportes("V")
                Dim dtt As DataTable = objBNTransp.ConsultarList(0, vintIDCab, "", "", "", "", "V", "", "")
                dgvVuelos = dgvCargarGridFilaaFila(dgvVuelos, dtt)
            Else
                dgvVuelos = vfrmRef.dgvVuelos
            End If
Vuelos:

            With dgvVuelos
                For Each DgvFila As DataGridViewRow In .Rows
                    If Not DgvFila.IsNewRow Then
                        blnUbicado = False
                        If DgvFila.Cells("DiaV").Value Is Nothing Then
                            If Not DgvFila.Cells("FechaRetorno").Value Is Nothing Then
                                If Format(DgvFila.Cells("FechaRetorno").Value, "dd/MM/yyyy") = datFechaDiaCotiDetBusqueda Then
                                    blnUbicado = True
                                End If
                            End If
                        Else
                            If Not DgvFila.Cells("DiaV").Value Is Nothing Then
                                'If DgvFila.Cells("DiaV").Value.ToString.Length > 10 Then
                                '    If DgvFila.Cells("DiaV").Value.ToString.Substring(4, 10) = datFechaDiaCotiDetBusqueda Then
                                '        blnUbicado = True
                                '    End If
                                'Else
                                '    If DgvFila.Cells("DiaV").Value.ToString = datFechaDiaCotiDetBusqueda Then
                                '        blnUbicado = True
                                '    End If
                                'End If
                                If Format(Convert.ToDateTime(DgvFila.Cells("DiaV").Value), "dd/MM/yyyy") = datFechaDiaCotiDetBusqueda Then
                                    blnUbicado = True
                                End If
                            End If
                        End If
                        If blnUbicado Then
                            'If Not DgvFila.Cells("DescProveedorV").Value Is Nothing Then
                            '    strProveedor = DgvFila.Cells("DescProveedorV").Value.ToString & " ("
                            'Else
                            '    strProveedor = ""
                            'End If
                            If Not DgvFila.Cells("RutaOAbrV").Value Is Nothing Then
                                strOrigen = DgvFila.Cells("RutaOAbrV").Value.ToString '& "/"
                                'strOrigen = Replace(strOrigen, "NO APLICA/", "")
                            Else
                                strOrigen = ""
                            End If
                            If Not DgvFila.Cells("RutaDAbrV").Value Is Nothing Then
                                strDestino = DgvFila.Cells("RutaDAbrV").Value.ToString '& ")"
                                'strDestino = Replace(strDestino, "NO APLICA", "")
                            Else
                                strDestino = ""
                            End If
                            'strTextoVer = strProveedor & strOrigen & strDestino
                            If Not DgvFila.Cells("Vuelo").Value Is Nothing Then
                                strVuelo = DgvFila.Cells("Vuelo").Value.ToString
                            Else
                                strVuelo = ""
                            End If
                            If vblnOrigen Then
                                If Not DgvFila.Cells("Llegada").Value Is Nothing Then
                                    strHora = DgvFila.Cells("Llegada").Value.ToString
                                Else
                                    strHora = ""
                                End If
                                'strTextoDesc = "Airport (" & strVuelo & If(strVuelo = "", "", " ") & strOrigen & strDestino & " " & strHora & ")" & Space(100) & "|" & strHora
                                strTextoDesc = "Airport (" & strVuelo & If(strVuelo = "", "", " ") & strOrigen & "/" & strDestino & " " & strHora & ")" & Space(100) & "|" & strHora
                            Else
                                If Not DgvFila.Cells("Salida").Value Is Nothing Then
                                    strHora = DgvFila.Cells("Salida").Value.ToString
                                Else
                                    strHora = ""
                                End If
                                'strTextoDesc = "Airport (" & strVuelo & If(strVuelo = "", "", " ") & strOrigen & strDestino & " " & strHora & ")"
                                strTextoDesc = "Airport (" & strVuelo & If(strVuelo = "", "", " ") & strOrigen & "/" & strDestino & " " & strHora & ")"
                            End If
                            'strTextoDesc = "AEROPUERTO " & strProveedor & strVuelo & " " & strOrigen & strDestino & " " & strHora
                            'strTextoDesc = strProveedor & strVuelo & " " & strOrigen & strDestino & " " & strHora
                            'strTextoDesc = "Airport (" & If(strVuelo = "", "", " ") & strOrigen & strDestino & " " & strHora & ")"


                            If strTextoDesc.Trim <> "" Then
                                'If strTextoDesc.Trim.Substring(strTextoDesc.Trim.Length - 3, 3) = "(/)" Then
                                '    strTextoDesc = strTextoDesc.Substring(0, strTextoDesc.Length - 4)
                                'End If

                                'Dim dttTemp As DataTable = dtFiltrarDataTable(dttOrigenDestino, "TextoVer='" & strTextoDesc & "'")
                                'If dttTemp.Rows.Count = 0 Then
                                Dim intIDDetV As Integer = DgvFila.Cells("IDDetV").Value
                                If intIDDetV = 0 Then
                                    intIDDetV = DgvFila.Cells("ID").Value
                                End If
                                'dttOrigenDestino.Rows.Add(DgvFila.Cells("IDDetV").Value.ToString, strTextoDesc)
                                dttOrigenDestino.Rows.Add(intIDDetV, strTextoDesc)
                                'End If
                            End If
                        End If
                    End If
                Next

            End With
            bytContGrid += 1
            If bytContGrid = 2 Then
                datFechaDiaCotiDetBusqueda = vdatFechaDiaCotiDet
                datFechaDiaCotiDetBusqueda = Format(datFechaDiaCotiDetBusqueda, "dd/MM/yyyy")
                GoTo Vuelos
            ElseIf bytContGrid = 3 Then
                datFechaDiaCotiDetBusqueda = DateAdd(DateInterval.Day, 1, vdatFechaDiaCotiDet)
                datFechaDiaCotiDetBusqueda = Format(datFechaDiaCotiDetBusqueda, "dd/MM/yyyy")
                GoTo Vuelos
            End If


            'BUS
            bytContGrid = 1
            datFechaDiaCotiDetBusqueda = DateAdd(DateInterval.Day, -1, vdatFechaDiaCotiDet)
            datFechaDiaCotiDetBusqueda = Format(datFechaDiaCotiDetBusqueda, "dd/MM/yyyy")

            Dim dgvBus As DataGridView = Nothing
            If vfrmRef Is Nothing Then
                Dim objBNTransp As New clsCotizacionBN.clsCotizacionVuelosBN
                dgvBus = dgvColumnasTransportes("B")
                Dim dtt As DataTable = objBNTransp.ConsultarList(0, vintIDCab, "", "", "", "", "B", "", "")
                dgvBus = dgvCargarGridFilaaFila(dgvBus, dtt)
            Else
                dgvBus = vfrmRef.dgvBuss
            End If
Buses:

            With dgvBus
                For Each DgvFila As DataGridViewRow In .Rows
                    If Not DgvFila.IsNewRow Then
                        blnUbicado = False
                        If DgvFila.Cells("DiaB").Value Is Nothing Then
                            If Not DgvFila.Cells("Fec_RetornoB").Value Is Nothing Then
                                If Format(DgvFila.Cells("Fec_RetornoB").Value, "dd/MM/yyyy") = datFechaDiaCotiDetBusqueda Then
                                    blnUbicado = True
                                End If
                            End If
                        Else
                            If Not DgvFila.Cells("DiaB").Value Is Nothing Then
                                'If DgvFila.Cells("DiaB").Value.ToString.Length > 10 Then
                                '    If DgvFila.Cells("DiaB").Value.ToString.Substring(4, 10) = datFechaDiaCotiDetBusqueda Then
                                '        blnUbicado = True
                                '    End If
                                'Else
                                '    If DgvFila.Cells("DiaB").Value.ToString = datFechaDiaCotiDetBusqueda Then
                                '        blnUbicado = True
                                '    End If
                                'End If
                                If Format(Convert.ToDateTime(DgvFila.Cells("DiaB").Value), "dd/MM/yyyy") = datFechaDiaCotiDetBusqueda Then
                                    'If Convert.ToDateTime(DgvFila.Cells("DiaB").Value.ToString) = datFechaDiaCotiDetBusqueda Then
                                    blnUbicado = True
                                End If
                            End If
                        End If
                        If blnUbicado Then
                            If Not DgvFila.Cells("DescProveedorB").Value Is Nothing Then
                                strProveedor = DgvFila.Cells("DescProveedorB").Value.ToString
                            Else
                                strProveedor = ""
                            End If
                            If Not DgvFila.Cells("RutaOAbrB").Value Is Nothing Then
                                strOrigen = DgvFila.Cells("RutaOAbrB").Value.ToString
                                'strOrigen = Replace(strOrigen, "NO APLICA/", "")
                            Else
                                strOrigen = ""
                            End If
                            If Not DgvFila.Cells("RutaDAbrB").Value Is Nothing Then
                                strDestino = DgvFila.Cells("RutaDAbrB").Value.ToString
                                'strDestino = Replace(strDestino, "NO APLICA", "")
                            Else
                                strDestino = ""
                            End If

                            'strTextoVer = strProveedor & strOrigen & strDestino

                            If vblnOrigen Then
                                If Not DgvFila.Cells("LlegadaB").Value Is Nothing Then
                                    strHora = DgvFila.Cells("LlegadaB").Value.ToString
                                Else
                                    strHora = ""
                                End If
                                'strTextoDesc = strProveedor.ToUpper & " Bus Station (" & strOrigen & "/" & strDestino & " " & strHora & ")" & Space(100) & "|" & strHora
                                strTextoDesc = strProveedor.ToUpper & " Bus Station (" & strOrigen & "/" & strDestino & " " & strHora & ")" & Space(100) & "|" & strHora
                            Else
                                If Not DgvFila.Cells("SalidaB").Value Is Nothing Then
                                    strHora = DgvFila.Cells("SalidaB").Value.ToString
                                Else
                                    strHora = ""
                                End If
                                'strTextoDesc = strProveedor.ToUpper & " Bus Station (" & strOrigen & "/" & strDestino & " " & strHora & ")"
                                strTextoDesc = strProveedor.ToUpper & " Bus Station (" & strOrigen & "/" & strDestino & " " & strHora & ")"
                            End If

                            If strTextoDesc.Trim <> "" Then
                                'If strTextoDesc.Trim.Substring(strTextoDesc.Trim.Length - 3, 3) = "(/)" Then
                                '    strTextoDesc = strTextoDesc.Substring(0, strTextoDesc.Length - 4)
                                'End If

                                'dttOrigenDestino.Rows.Add(strTextoDesc, strTextoVer)
                                'Dim dttTemp As DataTable = dtFiltrarDataTable(dttOrigenDestino, "TextoVer='" & strTextoDesc & "'")
                                'If dttTemp.Rows.Count = 0 Then

                                dttOrigenDestino.Rows.Add(DgvFila.Cells("IDDetB").Value.ToString, strTextoDesc)
                                'End If
                            End If
                        End If
                    End If
                Next

            End With
            bytContGrid += 1
            If bytContGrid = 2 Then
                datFechaDiaCotiDetBusqueda = vdatFechaDiaCotiDet
                datFechaDiaCotiDetBusqueda = Format(datFechaDiaCotiDetBusqueda, "dd/MM/yyyy")
                GoTo Buses
            ElseIf bytContGrid = 3 Then
                datFechaDiaCotiDetBusqueda = DateAdd(DateInterval.Day, 1, vdatFechaDiaCotiDet)
                datFechaDiaCotiDetBusqueda = Format(datFechaDiaCotiDetBusqueda, "dd/MM/yyyy")
                GoTo Buses
            End If

            'TREN
            bytContGrid = 1
            datFechaDiaCotiDetBusqueda = DateAdd(DateInterval.Day, -1, vdatFechaDiaCotiDet)
            datFechaDiaCotiDetBusqueda = Format(datFechaDiaCotiDetBusqueda, "dd/MM/yyyy")

            Dim dgvTren As DataGridView = Nothing
            If vfrmRef Is Nothing Then
                Dim objBNTransp As New clsCotizacionBN.clsCotizacionVuelosBN
                dgvTren = dgvColumnasTransportes("T")
                Dim dtt As DataTable = objBNTransp.ConsultarList(0, vintIDCab, "", "", "", "", "T", "", "")
                dgvTren = dgvCargarGridFilaaFila(dgvTren, dtt)
            Else
                dgvTren = vfrmRef.dgvTren
            End If
Trenes:


            With dgvTren
                For Each DgvFila As DataGridViewRow In .Rows
                    If Not DgvFila.IsNewRow Then
                        blnUbicado = False
                        If DgvFila.Cells("DiaT").Value Is Nothing Then
                            If Not DgvFila.Cells("Fec_RetornoT").Value Is Nothing Then
                                If Format(DgvFila.Cells("Fec_RetornoT").Value, "dd/MM/yyyy") = datFechaDiaCotiDetBusqueda Then
                                    blnUbicado = True
                                End If
                            End If
                        Else
                            If Not DgvFila.Cells("DiaT").Value Is Nothing Then
                                'If DgvFila.Cells("DiaT").Value.ToString.Length > 10 Then
                                '    If DgvFila.Cells("DiaT").Value.ToString.Substring(4, 10) = datFechaDiaCotiDetBusqueda Then
                                '        blnUbicado = True
                                '    End If
                                'Else
                                '    If DgvFila.Cells("DiaT").Value.ToString = datFechaDiaCotiDetBusqueda Then
                                '        blnUbicado = True
                                '    End If
                                'End If
                                If Format(Convert.ToDateTime(DgvFila.Cells("DiaT").Value), "dd/MM/yyyy") = datFechaDiaCotiDetBusqueda Then
                                    'If Convert.ToDateTime(DgvFila.Cells("DiaT").Value.ToString) = datFechaDiaCotiDetBusqueda Then
                                    blnUbicado = True
                                End If
                            End If
                        End If
                        If blnUbicado Then
                            'If Not DgvFila.Cells("DescProveedorT").Value Is Nothing Then
                            '    strProveedor = DgvFila.Cells("DescProveedorT").Value.ToString & " ("
                            'Else
                            '    strProveedor = ""
                            'End If
                            'If Not DgvFila.Cells("RutaOAbrT").Value Is Nothing Then
                            '    strOrigen = DgvFila.Cells("RutaOAbrT").Value.ToString & "/"
                            '    'strOrigen = Replace(strOrigen, "NO APLICA/", "")
                            'Else
                            '    strOrigen = ""
                            'End If
                            'If Not DgvFila.Cells("RutaDAbrT").Value Is Nothing Then
                            '    strDestino = DgvFila.Cells("RutaDAbrT").Value.ToString & ")"
                            '    'strDestino = Replace(strDestino, "NO APLICA", "")
                            'Else
                            '    strDestino = ""
                            'End If
                            If Not DgvFila.Cells("OrigenT").Value Is Nothing Then
                                strOrigen = DgvFila.Cells("OrigenT").Value.ToString
                            Else
                                strOrigen = ""
                            End If
                            If Not DgvFila.Cells("DestinoT").Value Is Nothing Then
                                strDestino = DgvFila.Cells("DestinoT").Value.ToString
                            Else
                                strDestino = ""
                            End If
                            If Not DgvFila.Cells("VueloT").Value Is Nothing Then
                                strVuelo = DgvFila.Cells("VueloT").Value.ToString
                            Else
                                strVuelo = ""
                            End If
                            'strTextoVer = strProveedor & strOrigen & strDestino

                            'Dim blnIda As Boolean = If(DgvFila.Cells("IdaVuelta").Value.ToString = "I", True, False)
                            If vblnOrigen Then
                                If Not DgvFila.Cells("LlegadaT").Value Is Nothing Then
                                    strHora = DgvFila.Cells("LlegadaT").Value.ToString
                                Else
                                    strHora = ""
                                End If
                                'If blnIda Then
                                '    strTextoDesc = strOrigen.ToUpper & " Train Station (" & strVuelo & If(strVuelo = "", "", " ") & strHora & ")" & Space(100) & "|" & strHora
                                'Else
                                '    strTextoDesc = strDestino.ToUpper & " Train Station (" & strVuelo & If(strVuelo = "", "", " ") & strHora & ")" & Space(100) & "|" & strHora
                                'End If
                                strTextoDesc = strDestino.ToUpper & " Train Station (" & strVuelo & If(strVuelo = "", "", " ") & strHora & ")" & Space(100) & "|" & strHora
                            Else
                                If Not DgvFila.Cells("SalidaT").Value Is Nothing Then
                                    strHora = DgvFila.Cells("SalidaT").Value.ToString
                                Else
                                    strHora = ""
                                End If
                                strTextoDesc = strOrigen.ToUpper & " Train Station (" & strVuelo & If(strVuelo = "", "", " ") & strHora & ")"
                            End If
                            'strTextoDesc = strProveedor & strOrigen & strDestino & " " & strHora


                            If strTextoDesc.Trim <> "" Then
                                'If strTextoDesc.Trim.Substring(strTextoDesc.Trim.Length - 3, 3) = "(/)" Then
                                '    strTextoDesc = strTextoDesc.Substring(0, strTextoDesc.Length - 4)
                                'End If

                                'dttOrigenDestino.Rows.Add(strTextoDesc, strTextoVer)
                                'Dim dttTemp As DataTable = dtFiltrarDataTable(dttOrigenDestino, "TextoVer='" & strTextoDesc & "'")
                                'If dttTemp.Rows.Count = 0 Then

                                dttOrigenDestino.Rows.Add(DgvFila.Cells("IDDetT").Value.ToString, strTextoDesc)
                                'End If
                            End If
                        End If
                    End If
                Next

            End With
            bytContGrid += 1
            If bytContGrid = 2 Then
                datFechaDiaCotiDetBusqueda = vdatFechaDiaCotiDet
                datFechaDiaCotiDetBusqueda = Format(datFechaDiaCotiDetBusqueda, "dd/MM/yyyy")
                GoTo Trenes
            ElseIf bytContGrid = 3 Then
                datFechaDiaCotiDetBusqueda = DateAdd(DateInterval.Day, 1, vdatFechaDiaCotiDet)
                datFechaDiaCotiDetBusqueda = Format(datFechaDiaCotiDetBusqueda, "dd/MM/yyyy")
                GoTo Trenes
            End If

            'HOTELES

            bytContGrid = 1
            datFechaDiaCotiDetBusqueda = DateAdd(DateInterval.Day, -1, vdatFechaDiaCotiDet)
            datFechaDiaCotiDetBusqueda = Format(datFechaDiaCotiDetBusqueda, "dd/MM/yyyy")
Hoteles:

            Dim dgvHoteles As DataGridView = Nothing
            If vfrmRef Is Nothing Then
                Dim objBTCotiDet As New clsCotizacionBT.clsDetalleCotizacionBT
                dgvHoteles = dgvColumnasTransportes("X")
                Dim dtt As DataTable = objBTCotiDet.ConsultarListxIDCab(vintIDCab, True, False)
                dgvHoteles = dgvCargarGridFilaaFila(dgvHoteles, dtt)
            Else
                dgvHoteles = vfrmRef.dgvDet
            End If

            With dgvHoteles
                For Each DgvFila As DataGridViewRow In .Rows
                    If Not DgvFila.IsNewRow Then
                        blnUbicado = False
                        If DgvFila.Cells("IDTipoProv").Value.ToString = gstrTipoProveeHoteles Then
                            If DgvFila.Cells("DiaFormat").Value.ToString.Substring(4, 10) = datFechaDiaCotiDetBusqueda Then
                                blnUbicado = True
                            End If

                        Else 'jorge
                            If Not vfrmRef Is Nothing Then
                                If (DgvFila.Cells("IDTipoProv").Value.ToString = gstrTipoProveeOperadores And DgvFila.Cells("ConAlojamiento").Value = True) Then
                                    If DgvFila.Cells("DiaFormat").Value.ToString.Substring(4, 10) = datFechaDiaCotiDetBusqueda Then
                                        blnUbicado = True
                                    End If
                                End If
                            End If

                        End If
                        If blnUbicado Then
                            If Not DgvFila.Cells("DescProveedor").Value Is Nothing Then
                                strProveedor = DgvFila.Cells("DescProveedor").Value.ToString
                            Else
                                strProveedor = ""
                            End If

                            If strProveedor <> "" Then
                                'If strProveedor.Length >= 5 Then
                                '    If strProveedor.Substring(0, 5) = "HOTEL" Then
                                '        strTextoVer = Mid(strProveedor, 6)
                                '        strTextoVer = "HTL " & strTextoVer.Trim
                                '    ElseIf strProveedor.Substring(0, 3) = "HTL" Then
                                '        strTextoVer = Mid(strProveedor, 4)
                                '        strTextoVer = "HTL " & strTextoVer.Trim

                                '    Else
                                '        strTextoVer = "HTL " & strProveedor
                                '    End If


                                'ElseIf strProveedor.Length >= 3 Then

                                '    If strProveedor.Substring(0, 3) = "HTL" Then
                                '        strTextoVer = Mid(strProveedor, 4)
                                '        strTextoVer = "HTL " & strTextoVer.Trim
                                '    ElseIf strProveedor.Substring(0, 5) = "HOTEL" Then
                                '        strTextoVer = Mid(strProveedor, 6)
                                '        strTextoVer = "HTL " & strTextoVer.Trim
                                '    Else
                                '        strTextoVer = "HTL " & strProveedor
                                '    End If


                                'Else
                                strTextoVer = strProveedor
                                'End If


                            Else
                                strTextoVer = ""
                            End If

                            strTextoDesc = strTextoVer
                            If strTextoDesc.Trim <> "" Then
                                'dttOrigenDestino.Rows.Add(strTextoDesc, strTextoVer)
                                If strTextoDesc.Trim.Substring(strTextoDesc.Trim.Length - 3, 3) = "- /" Then
                                    strTextoDesc = strTextoDesc.Substring(0, strTextoDesc.Length - 4)
                                End If

                                Dim dttTemp As DataTable = dtFiltrarDataTable(dttOrigenDestino, "TextoVer='" & strTextoDesc & "'")
                                If dttTemp.Rows.Count = 0 Then

                                    dttOrigenDestino.Rows.Add(If(DgvFila.Cells("IDDet").Value Is Nothing, "X" & (DgvFila.Index + 1).ToString, DgvFila.Cells("IDDet").Value.ToString), strTextoDesc)
                                End If
                            End If
                        End If
                    End If
                Next

            End With
            bytContGrid += 1
            If bytContGrid = 2 Then
                datFechaDiaCotiDetBusqueda = vdatFechaDiaCotiDet
                datFechaDiaCotiDetBusqueda = Format(datFechaDiaCotiDetBusqueda, "dd/MM/yyyy")
                GoTo Hoteles
            ElseIf bytContGrid = 3 Then
                datFechaDiaCotiDetBusqueda = DateAdd(DateInterval.Day, 1, vdatFechaDiaCotiDet)
                datFechaDiaCotiDetBusqueda = Format(datFechaDiaCotiDetBusqueda, "dd/MM/yyyy")
                GoTo Hoteles
            End If


            'RESTAURANTE

            bytContGrid = 1
            datFechaDiaCotiDetBusqueda = DateAdd(DateInterval.Day, -1, vdatFechaDiaCotiDet)
            datFechaDiaCotiDetBusqueda = Format(datFechaDiaCotiDetBusqueda, "dd/MM/yyyy")

            Dim dgvRestaurant As DataGridView = Nothing
            If vfrmRef Is Nothing Then
                'dgvRestaurant = New DataGridView
                'dgvRestaurant.DataSource = dgvHoteles.DataSource
                Dim objBTCotiDet As New clsCotizacionBT.clsDetalleCotizacionBT
                dgvRestaurant = dgvColumnasTransportes("X")
                Dim dtt As DataTable = objBTCotiDet.ConsultarListxIDCab(vintIDCab, True, False)
                dgvRestaurant = dgvCargarGridFilaaFila(dgvRestaurant, dtt)
            Else
                dgvRestaurant = vfrmRef.dgvDet
            End If
Restaurante:
            With dgvRestaurant
                For Each DgvFila As DataGridViewRow In .Rows
                    If Not DgvFila.IsNewRow Then
                        blnUbicado = False
                        If DgvFila.Cells("IDTipoProv").Value.ToString = gstrTipoProveeRestaurantes Then
                            If DgvFila.Cells("DiaFormat").Value.ToString.Substring(4, 10) = datFechaDiaCotiDetBusqueda Then
                                blnUbicado = True
                            End If
                        End If
                        If blnUbicado Then
                            If Not DgvFila.Cells("DescProveedor").Value Is Nothing Then
                                strProveedor = DgvFila.Cells("DescProveedor").Value.ToString
                            Else
                                strProveedor = ""
                            End If
                            If strProveedor <> "" Then
                                '    If strProveedor.Length >= 10 Then
                                '        If strProveedor.Substring(0, 10) = "RESTAURANT" Then
                                '            strTextoVer = strProveedor
                                '        Else
                                '            strTextoVer = "RESTAURANT " & strProveedor
                                '        End If
                                '    Else
                                '        strTextoVer = ""
                                '    End If
                                'Else
                                '    strTextoVer = ""
                                strTextoDesc = strProveedor
                            End If

                            If strTextoDesc.Trim <> "" Then
                                'dttOrigenDestino.Rows.Add(strTextoDesc, strTextoVer)
                                Dim dttTemp As DataTable = dtFiltrarDataTable(dttOrigenDestino, "TextoVer='" & strTextoDesc & "'")
                                If dttTemp.Rows.Count = 0 Then

                                    dttOrigenDestino.Rows.Add(If(DgvFila.Cells("IDDet").Value Is Nothing, "X" & (DgvFila.Index + 1).ToString, DgvFila.Cells("IDDet").Value.ToString), strTextoDesc)
                                End If
                            End If
                        End If
                    End If
                Next

            End With
            bytContGrid += 1
            If bytContGrid = 2 Then
                datFechaDiaCotiDetBusqueda = vdatFechaDiaCotiDet
                datFechaDiaCotiDetBusqueda = Format(datFechaDiaCotiDetBusqueda, "dd/MM/yyyy")
                GoTo Restaurante
            ElseIf bytContGrid = 3 Then
                datFechaDiaCotiDetBusqueda = DateAdd(DateInterval.Day, 1, vdatFechaDiaCotiDet)
                datFechaDiaCotiDetBusqueda = Format(datFechaDiaCotiDetBusqueda, "dd/MM/yyyy")
                GoTo Restaurante
            End If

            Return dttOrigenDestino

        Catch ex As System.Exception
            Throw
        End Try

    End Function

    Public Function dgvCargarGridFilaaFila(ByVal vDgv As DataGridView, ByVal vDtt As DataTable) As DataGridView
        Try
            Dim bytContFila As Byte = 0
            vDgv.Rows.Clear()
            For Each dr As DataRow In vDtt.Rows
                'If bytContFila > 0 Then vDgv.Rows.Add()
                vDgv.Rows.Add()
                For iDgv As Byte = 0 To vDgv.Columns.Count - 1
                    'MessageBox.Show(vDgv.Columns(iDgv).HeaderText)

                    For iDtt As Byte = 0 To vDtt.Columns.Count - 1

                        If vDgv.Columns(iDgv).HeaderText.ToUpper = vDtt.Columns(iDtt).ColumnName.ToUpper Then
                            vDgv.Item(iDgv, bytContFila).Value = dr(iDtt)
                        End If
                    Next
                Next
                bytContFila += 1
            Next

            Return vDgv

        Catch ex As Exception
            Throw
        End Try

    End Function

    Public Function dgvColumnasTransportes(ByVal vchrTipoTransporte As Char) As DataGridView
        Try
            Dim Dgv As New DataGridView
            Select Case vchrTipoTransporte
                Case "V"
                    With Dgv.Columns
                        .Add("DiaV", "Dia")
                        .Add("FechaRetorno", "Fec_Retorno")
                        .Add("RutaOAbrV", "RutaOAbr")
                        .Add("RutaDAbrV", "RutaDAbr")
                        .Add("Vuelo", "Vuelo")
                        .Add("Salida", "Salida")
                        .Add("Llegada", "Llegada")
                        .Add("IDDetV", "IDDet")
                        .Add("ID", "ID")
                    End With
                Case "B"
                    With Dgv.Columns
                        .Add("DiaB", "Dia")
                        .Add("Fec_RetornoB", "Fec_Retorno")
                        .Add("DescProveedorB", "DescProveedor")
                        .Add("RutaOAbrB", "RutaOAbr")
                        .Add("RutaDAbrB", "RutaDAbr")
                        .Add("SalidaB", "Salida")
                        .Add("LlegadaB", "Llegada")
                        .Add("IDDetB", "IDDet")
                    End With
                Case "T"
                    With Dgv.Columns
                        .Add("DiaT", "Dia")
                        .Add("Fec_RetornoT", "Fec_Retorno")
                        .Add("OrigenT", "Origen")
                        .Add("DestinoT", "Destino")
                        .Add("VueloT", "Vuelo")
                        .Add("SalidaT", "Salida")
                        .Add("LlegadaT", "Llegada")
                        '.Add("IdaVuelta", "IdaVuelta")
                        .Add("IDDetT", "IDDet")

                    End With

                Case "X" 'hoteles y restaurantes
                    With Dgv.Columns
                        .Add("IDTipoProv", "IDTipoProv")
                        .Add("DiaFormat", "DiaFormat")
                        .Add("DescProveedor", "DescProveedor")
                        .Add("IDDet", "IDDet")

                    End With
            End Select

            Return Dgv
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Function lstTransferCambiados(ByVal vfrmRef As frmIngCotizacionDato, _
                                         ByVal vchrTipoTransp As Char, ByVal vbytFilaT As Byte, _
                                         ByVal vintIDCab As Integer, _
                                         ByVal vdatFechaDiaCotiDet As Date) As List(Of Byte)
        Try
            Dim ListaTransfer As New List(Of Byte)
            Dim strIDDetCambiado As String = ""
            If vchrTipoTransp = "X" Then
                strIDDetCambiado = vfrmRef.dgvDet.Item("IDDet", vbytFilaT).Value.ToString
            ElseIf vchrTipoTransp = "V" Then
                strIDDetCambiado = vfrmRef.dgvVuelos.Item("IDDetV", vbytFilaT).Value.ToString
            ElseIf vchrTipoTransp = "B" Then
                strIDDetCambiado = vfrmRef.dgvBuss.Item("IDDetB", vbytFilaT).Value.ToString
            ElseIf vchrTipoTransp = "T" Then
                strIDDetCambiado = vfrmRef.dgvTren.Item("IDDetT", vbytFilaT).Value.ToString
            End If

            Dim blnOk As Boolean = False
            Dim strIDDetTransOrigen As String = ""
            Dim strIDDetTransDestino As String = ""

            Dim bytFilaCotiDetOri As Byte
            For Each DgvFila As DataGridViewRow In vfrmRef.dgvDet.Rows
                If DgvFila.Cells("IDDetTransferOri").Value = strIDDetCambiado Then
                    blnOk = True
                    strIDDetTransOrigen = DgvFila.Cells("IDDetTransferOri").Value.ToString
                    strIDDetTransDestino = DgvFila.Cells("IDDetTransferDes").Value.ToString
                    bytFilaCotiDetOri = DgvFila.Index
                    GoTo Destino
                End If
            Next
Destino:

            Dim bytFilaCotiDetDes As Byte
            For Each DgvFila As DataGridViewRow In vfrmRef.dgvDet.Rows
                If DgvFila.Cells("IDDetTransferDes").Value = strIDDetCambiado Then
                    blnOk = True
                    strIDDetTransDestino = DgvFila.Cells("IDDetTransferDes").Value.ToString
                    strIDDetTransOrigen = DgvFila.Cells("IDDetTransferOri").Value.ToString
                    bytFilaCotiDetDes = DgvFila.Index
                    Exit For
                End If
            Next

            If blnOk Then

                Dim dttOrigenDestino As DataTable = dttComboTransferOrigenDestino(vfrmRef, vdatFechaDiaCotiDet, vintIDCab, True)

                Dim strTextoServicioOri As String = ""
                Dim strTextoServicioDes As String = ""
                For Each dr As DataRow In dttOrigenDestino.Rows
                    If dr("IDDet").ToString = strIDDetTransOrigen And dr("IDDet").ToString <> "0" Then
                        strTextoServicioOri = dr("TextoVer")

                        For Each dr2 As DataRow In dttOrigenDestino.Rows
                            If dr2("IDDet").ToString = strIDDetTransDestino And dr2("IDDet").ToString <> "0" Then
                                strTextoServicioDes = dr2("TextoVer")
                                Exit For
                            End If
                        Next
                        Exit For
                    End If
                Next


                dttOrigenDestino = dttComboTransferOrigenDestino(vfrmRef, vdatFechaDiaCotiDet, vintIDCab, False)


                For Each dr As DataRow In dttOrigenDestino.Rows
                    If dr("IDDet").ToString = strIDDetTransDestino And dr("IDDet").ToString <> "0" Then
                        strTextoServicioDes = dr("TextoVer")

                        For Each dr2 As DataRow In dttOrigenDestino.Rows
                            If dr2("IDDet").ToString = strIDDetTransOrigen And dr2("IDDet").ToString <> "0" Then
                                strTextoServicioOri = dr2("TextoVer")
                                Exit For
                            End If
                        Next
                        Exit For
                    End If
                Next

                If strTextoServicioOri <> "" Then 'Or strTextoServicioDes <> "" Then
                    For Each DgvFila As DataGridViewRow In vfrmRef.dgvDet.Rows
                        Dim blnTransfer As Boolean = DgvFila.Cells("Transfer").Value
                        If DgvFila.Cells("IDDetTransferOri").Value = strIDDetCambiado Then 'Or DgvFila.Cells("IDDetTransferDes").Value = strIDDetCambiado Then
                            Dim bytFilaaCambiar As Byte
                            If DgvFila.Cells("IDDetTransferOri").Value = strIDDetCambiado Then
                                bytFilaaCambiar = bytFilaCotiDetOri
                                'Else
                                '    bytFilaaCambiar = bytFilaCotiDetDes
                            End If
                            'Dim strNuevoTextoServicio As String = strTransferOrigenDestino(strTextoServicioOri, strTextoServicioDes, "", bytFilaaCambiar)
                            Dim strNuevoTextoServicio As String = strPintarTransferOrigenDestino(strTextoServicioOri, strTextoServicioDes, "", bytFilaaCambiar, Nothing, Nothing, True, "")
                            If DgvFila.Cells("Servicio").Value.ToString.Trim <> strNuevoTextoServicio.Trim And blnTransfer Then
                                DgvFila.Cells("Servicio").Value = strNuevoTextoServicio
                                DgvFila.Cells("DescServicioDet").Value = strNuevoTextoServicio

                                ListaTransfer.Add(DgvFila.Index)
                            End If

                        End If
                    Next
                End If

                If strTextoServicioDes <> "" Then
                    For Each DgvFila As DataGridViewRow In vfrmRef.dgvDet.Rows
                        Dim blnTransfer As Boolean = DgvFila.Cells("Transfer").Value
                        If DgvFila.Cells("IDDetTransferDes").Value = strIDDetCambiado Then
                            Dim bytFilaaCambiar As Byte
                            If DgvFila.Cells("IDDetTransferDes").Value = strIDDetCambiado Then
                                '    bytFilaaCambiar = bytFilaCotiDetOri
                                'Else
                                bytFilaaCambiar = bytFilaCotiDetDes
                            End If
                            'Dim strNuevoTextoServicio As String = strTransferOrigenDestino(strTextoServicioOri, strTextoServicioDes, "", bytFilaaCambiar)
                            Dim strNuevoTextoServicio As String = strPintarTransferOrigenDestino(strTextoServicioOri, strTextoServicioDes, "", bytFilaaCambiar, Nothing, Nothing, False, "")
                            If DgvFila.Cells("Servicio").Value.ToString.Trim <> strNuevoTextoServicio.Trim And blnTransfer Then
                                DgvFila.Cells("Servicio").Value = strNuevoTextoServicio
                                DgvFila.Cells("DescServicioDet").Value = strNuevoTextoServicio

                                ListaTransfer.Add(DgvFila.Index)
                            End If

                        End If
                    Next
                End If

            End If

            Return ListaTransfer
        Catch ex As System.Exception
            Throw
        End Try
    End Function

    Public Function strPintarTransferOrigenDestino(ByVal vstrOrigenTransfer As String, _
                                             ByVal vstrDestinoTransfer As String, _
                                             ByVal vstrDescServicioDetBup As String, _
                                             ByVal vbytNroFila As Byte, _
                                             ByRef rtxtTextBox As TextBox, _
                                             ByRef rdtpPicker As DateTimePicker, _
                                             ByVal vblnOrigen As Boolean, _
                                             ByVal vstrHoraAnt As String) As String

        Try

            Dim strOrigenTransfer As String = strDevuelveCadenaNoOcultaIzquierda(vstrOrigenTransfer)
            If InStr(vstrOrigenTransfer, "|") = 0 Then
                strOrigenTransfer = vstrOrigenTransfer
            End If
            Dim strTextoaPintar As String = strTransferOrigenDestino(strOrigenTransfer, _
                                                                     vstrDestinoTransfer, _
                                                                     vstrDescServicioDetBup, vbytNroFila)
            Dim strAgregarDescOutFormat = String.Empty
            If Not rtxtTextBox Is Nothing Then ''PPMG20160427
                If rtxtTextBox.Text.Trim <> String.Empty Then
                    If rtxtTextBox.Text.IndexOf("(via ") > -1 Or rtxtTextBox.Text.IndexOf("(vía ") > -1 Then
                        strAgregarDescOutFormat = " " & rtxtTextBox.Text.Substring(rtxtTextBox.Text.IndexOf("(via "), rtxtTextBox.Text.Length - rtxtTextBox.Text.IndexOf("(via "))
                    End If

                    For i As Byte = 1 To 3
                        If rtxtTextBox.Text.IndexOf(" - E" & i.ToString()) > -1 Or rtxtTextBox.Text.IndexOf(" - E" & i.ToString()) > -1 Then
                            strAgregarDescOutFormat = rtxtTextBox.Text.Substring(rtxtTextBox.Text.IndexOf(" - E" & i.ToString()), rtxtTextBox.Text.Length - rtxtTextBox.Text.IndexOf(" - E" & i.ToString()))
                            Exit For
                        End If
                    Next
                End If
            End If

            If vblnOrigen Then
                Dim strHora As String = strDevuelveCadenaOcultaDerecha(vstrOrigenTransfer)
                If strHora <> "" Then
                    strTextoaPintar = Replace(strTextoaPintar, " " & strHora, "")
                End If

                If Not rtxtTextBox Is Nothing Then
                    If strTextoaPintar <> "" Then
                        rtxtTextBox.Text = strTextoaPintar
                    End If
                End If

                If Not rdtpPicker Is Nothing Then
                    If strHora <> "" Then
                        rdtpPicker.Text = strHora
                    Else
                        rdtpPicker.Text = vstrHoraAnt
                    End If
                End If

            Else
                If vstrDescServicioDetBup.Length >= 2 Then

                    Dim strE As String = vstrDescServicioDetBup.Substring(vstrDescServicioDetBup.Length - 2, 2)
                    If strE = "E1" Or strE = "E2" Or strE = "OT" Then
                        If strTextoaPintar.IndexOf(" - " & strE) = -1 Then
                            strTextoaPintar &= " - " & strE
                        End If
                    End If
                End If
                If Not rtxtTextBox Is Nothing Then
                    Dim strHora As String = strDevuelveCadenaOcultaDerecha(vstrOrigenTransfer)
                    If strHora <> "" Then
                        strTextoaPintar = Replace(strTextoaPintar, " " & strHora, "")
                    End If
                    rtxtTextBox.Text = strTextoaPintar
                End If
            End If
            If strAgregarDescOutFormat <> String.Empty Then
                If rtxtTextBox.Text.IndexOf(strAgregarDescOutFormat.Trim) = -1 Then
                    rtxtTextBox.Text &= strAgregarDescOutFormat
                End If
            End If


            Return strTextoaPintar
        Catch ex As System.Exception
            Throw
        End Try
    End Function

    Public Function strTransferOrigenDestino(ByVal vstrOrigenTransfer As String, _
                                             ByVal vstrDestinoTransfer As String, _
                                             ByVal vstrDescServicioDetBup As String, _
                                             ByVal vbytNroFila As Byte) As String
        Try
            Dim strReturn As String = ""

            If vstrOrigenTransfer = "" And vstrDestinoTransfer = "" Then
                strReturn = ""
                strReturn = vstrDescServicioDetBup
            Else
                'If vbytNroFila > 0 Then
                If vstrDestinoTransfer = "" Then
                    strReturn = "Transfer " & vstrOrigenTransfer & " / Airport"
                Else
                    If vstrOrigenTransfer = "" Then
                        strReturn = "Transfer Airport / " & vstrDestinoTransfer
                    Else
                        strReturn = "Transfer " & vstrOrigenTransfer & " / " & vstrDestinoTransfer
                    End If

                End If

                '    Else
                '    strReturn = "Transfer Airport / " & vstrDestinoTransfer
                'End If
            End If

            Return strReturn

        Catch ex As System.Exception
            Throw
        End Try
    End Function

    Public Function blnValidarTransfer(ByVal vfrmRef As frmIngCotizacionDato, ByVal vbytFila As Byte, ByRef rstrServicios As String) As Boolean
        Dim strIDDet As String = vfrmRef.dgvDet.Item("IDDet", vbytFila).Value.ToString
        rstrServicios = ""
        Dim blnOk As Boolean = True
        For Each DgvFila As DataGridViewRow In vfrmRef.dgvDet.Rows
            If strIDDet = DgvFila.Cells("IDDetTransferOri").Value.ToString Or strIDDet = DgvFila.Cells("IDDetTransferDes").Value.ToString Then
                blnOk = False
                rstrServicios &= DgvFila.Cells("Servicio").Value.ToString & Chr(13)
            End If
        Next
        Return blnOk

    End Function

    Public Structure stTransportPax
        Dim IDCab As Integer

        Dim IDTransporte As String
        Dim IDPaxTransp As String

        Dim IDPax As String
        Dim NombrePax As String
        Dim CodReservaTransp As String
        Dim IDProveedorGuia As String
        Dim NombreGuia As String
        Dim Reembolso As Double
        Dim Selecc As Boolean
        Dim UserMod As String
        Dim Accion As String
    End Structure
    Public objLstTransportPax As New List(Of stTransportPax)

    Public Sub BorrarstTransportPaxxTransporte(ByVal vstrIDTransporte As String, ByVal vintIDCab As Integer)
        If objLstTransportPax Is Nothing Then Exit Sub
Borrar:
        For Each ST As stTransportPax In objLstTransportPax
            If ST.IDTransporte.ToString = vstrIDTransporte And ST.IDCab = vintIDCab Then
                objLstTransportPax.Remove(ST)
                GoTo Borrar
            End If
        Next

    End Sub

    '    Public Sub BorrarstTransportPaxxIDPax(ByVal vintIDPax As Integer)
    '        If objLstTransportPax Is Nothing Then Exit Sub
    'Borrar:
    '        For Each ST As stTransportPax In objLstTransportPax
    '            If ST.IDPax = vintIDPax Then
    '                objLstTransportPax.Remove(ST)
    '                GoTo Borrar
    '            End If
    '        Next

    '    End Sub

    Public Sub ActualizarTransportePaxST(ByVal BE As clsCotizacionBE.clsPaxTransporteBE, ByVal vstrAccion As Char, ByVal vintIDCab As Integer)
        If objLstTransportPax Is Nothing Then Exit Sub
        Dim strAccion As String = ""

        Try
            For Each ST As stTransportPax In objLstTransportPax
                If ST.IDPaxTransp = BE.IDPaxTransp And ST.IDTransporte = BE.IDTransporte And ST.IDCab = vintIDCab Then
                    strAccion = ST.Accion
                    objLstTransportPax.Remove(ST)
                    Exit For
                End If
            Next

            Dim RegValor As New stTransportPax
            With RegValor
                .IDTransporte = BE.IDTransporte
                .IDPaxTransp = If(BE.IDPaxTransp Is Nothing, strObtenerMayorIDTrasnportePaxST().ToString, BE.IDPaxTransp)

                .IDCab = vintIDCab
                .IDPax = BE.IDPax
                .IDProveedorGuia = ""
                .CodReservaTransp = ""
                '.Accion = vstrAccion
                If strAccion = "" Then
                    .Accion = vstrAccion
                Else
                    .Accion = strAccion
                End If
                'If .Accion = "N" Then .Selecc = True Else .Selecc = False
            End With

            objLstTransportPax.Add(RegValor)
        Catch ex As Exception
            Throw
        End Try
    End Sub

    Public Function strObtenerMayorIDTrasnportePaxST() As String
        Dim intReturn As Int16 = 0
        If objLstTransportPax Is Nothing Then Return 1
        Try
            For Each ST As stTransportPax In objLstTransportPax
                Dim strValor As String = ST.IDPaxTransp.ToString
                Dim intValorID As Int16 = Convert.ToInt16(Replace(strValor, "M", ""))
                If intValorID > intReturn Then intReturn = intValorID
            Next

            Return "M" & (intReturn + 1).ToString
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Function intFilaDgvTransportexIDDet(ByVal vFrm As frmIngCotizacionDato, ByVal vstrIDDet As String) As Int16

        Try
            Dim strColName As String = "IDDetV"

            For Each dgvRow As DataGridViewRow In vFrm.dgvVuelos.Rows
                If IsNumeric(vstrIDDet) Then
                    If dgvRow.Cells(strColName).Value = vstrIDDet Then
                        Return dgvRow.Index
                    End If
                Else
                    If dgvRow.Cells(strColName).Value.ToString = "V" & vstrIDDet Then
                        Return dgvRow.Index
                    End If
                End If
            Next

            strColName = "IDDetB"
            For Each dgvRow As DataGridViewRow In vFrm.dgvBuss.Rows
                If IsNumeric(vstrIDDet) Then
                    If dgvRow.Cells(strColName).Value = vstrIDDet Then
                        Return dgvRow.Index
                    End If
                Else
                    If dgvRow.Cells(strColName).Value.ToString = "B" & vstrIDDet Then
                        Return dgvRow.Index
                    End If
                End If
            Next

            strColName = "IDDetT"
            For Each dgvRow As DataGridViewRow In vFrm.dgvTren.Rows
                If IsNumeric(vstrIDDet) Then
                    If dgvRow.Cells(strColName).Value = vstrIDDet Then
                        Return dgvRow.Index
                    End If

                Else
                    If dgvRow.Cells(strColName).Value.ToString = "T" & vstrIDDet Then
                        Return dgvRow.Index
                    End If

                End If
            Next

            Return -1
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Function dblDevuelveMontoxIDServicioDetxNroPax(ByVal vintIDServicio_Det As Integer, ByVal vintNroPax As Byte) As Double
        Try
            Dim dblMonto As Double = 0
            Using objBL As clsServicioProveedorBN.clsDetalleServicioProveedorBN = New clsServicioProveedorBN.clsDetalleServicioProveedorBN
                Using dr As SqlClient.SqlDataReader = objBL.ConsultarPk(vintIDServicio_Det)
                    dr.Read()
                    If dr.HasRows Then
                        If Not IsDBNull(dr("Monto_sgls")) Then
                            dblMonto = dr("Monto_sgls")
                        Else
                            If Not IsDBNull(dr("Monto_sgl")) Then
                                dblMonto = dr("Monto_sgl")
                            Else
                                'Rangos
                                Using objBLCostos As clsServicioProveedorBN.clsDetalleServicioProveedorBN.clsCostosDetalleServicioProveedorBN = _
                                    New clsServicioProveedorBN.clsDetalleServicioProveedorBN.clsCostosDetalleServicioProveedorBN
                                    Dim dttCostos As DataTable = objBLCostos.ConsultarListxIDDetalle(vintIDServicio_Det)

                                    For Each drCostos As DataRow In dttCostos.Rows
                                        If vintNroPax >= drCostos("PaxDesde") And vintNroPax <= drCostos("PaxHasta") Then
                                            dblMonto = drCostos("Monto")
                                            Exit For
                                        End If
                                    Next
                                End Using
                            End If
                        End If
                    End If
                    dr.Close()
                End Using
            End Using
            Return dblMonto
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Function strIDTransporteDgvxIDDet(ByVal vFrm As frmIngCotizacionDato, ByVal vstrIDDet As String) As String
        Try
            Dim strColName As String = "IDDetV"
            Dim strColNameID As String = "ID"

            For Each dgvRow As DataGridViewRow In vFrm.dgvVuelos.Rows
                If IsNumeric(vstrIDDet) Then
                    If dgvRow.Cells(strColName).Value = vstrIDDet Then
                        Return dgvRow.Cells(strColNameID).Value.ToString
                    End If
                Else
                    If dgvRow.Cells(strColName).Value.ToString = "V" & vstrIDDet Then
                        Return dgvRow.Cells(strColNameID).Value.ToString
                    End If
                End If
            Next

            strColName = "IDDetB"
            strColNameID = "IDB"
            For Each dgvRow As DataGridViewRow In vFrm.dgvBuss.Rows
                If IsNumeric(vstrIDDet) Then
                    If dgvRow.Cells(strColName).Value = vstrIDDet Then
                        Return dgvRow.Cells(strColNameID).Value.ToString
                    End If
                Else
                    If dgvRow.Cells(strColName).Value.ToString = "B" & vstrIDDet Then
                        Return dgvRow.Cells(strColNameID).Value.ToString
                    End If
                End If
            Next

            strColName = "IDDetT"
            strColNameID = "IDT"
            For Each dgvRow As DataGridViewRow In vFrm.dgvTren.Rows
                If IsNumeric(vstrIDDet) Then
                    If dgvRow.Cells(strColName).Value = vstrIDDet Then
                        Return dgvRow.Cells(strColNameID).Value.ToString
                    End If

                Else
                    If dgvRow.Cells(strColName).Value.ToString = "T" & vstrIDDet Then
                        Return dgvRow.Cells(strColNameID).Value.ToString
                    End If

                End If
            Next

            Return ""
        Catch ex As Exception
            Throw
        End Try
    End Function

End Module
