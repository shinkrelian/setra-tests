﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmAyudas
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim DataGridViewCellStyle13 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle14 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.grb = New System.Windows.Forms.GroupBox()
        Me.txtNumIdentidad = New System.Windows.Forms.TextBox()
        Me.lblEtiqRUC = New System.Windows.Forms.Label()
        Me.cboTipo = New System.Windows.Forms.ComboBox()
        Me.lblTipo = New System.Windows.Forms.Label()
        Me.grbCiudad = New System.Windows.Forms.GroupBox()
        Me.btnBuscar = New System.Windows.Forms.Button()
        Me.cboCiudad = New System.Windows.Forms.ComboBox()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.cboPais = New System.Windows.Forms.ComboBox()
        Me.lblCategoriaSEToursEtiq = New System.Windows.Forms.Label()
        Me.cboCategSeTours = New System.Windows.Forms.ComboBox()
        Me.lblCategoriaEtiq = New System.Windows.Forms.Label()
        Me.nudCategoria = New System.Windows.Forms.NumericUpDown()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.txtRazonSocial = New System.Windows.Forms.TextBox()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.dgv = New System.Windows.Forms.DataGridView()
        Me.Descripcion = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.CodTipo = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DescTipo = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.IDCiudad = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DescCiudad = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.IDPais = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DescPais = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Codigo = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Margen = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.OperadorInt = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.grb.SuspendLayout()
        Me.grbCiudad.SuspendLayout()
        CType(Me.nudCategoria, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgv, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'grb
        '
        Me.grb.Controls.Add(Me.txtNumIdentidad)
        Me.grb.Controls.Add(Me.lblEtiqRUC)
        Me.grb.Controls.Add(Me.cboTipo)
        Me.grb.Controls.Add(Me.lblTipo)
        Me.grb.Controls.Add(Me.grbCiudad)
        Me.grb.Controls.Add(Me.lblCategoriaSEToursEtiq)
        Me.grb.Controls.Add(Me.cboCategSeTours)
        Me.grb.Controls.Add(Me.lblCategoriaEtiq)
        Me.grb.Controls.Add(Me.nudCategoria)
        Me.grb.Controls.Add(Me.Label2)
        Me.grb.Controls.Add(Me.txtRazonSocial)
        Me.grb.Controls.Add(Me.GroupBox1)
        Me.grb.Controls.Add(Me.dgv)
        Me.grb.Dock = System.Windows.Forms.DockStyle.Fill
        Me.grb.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.grb.Location = New System.Drawing.Point(0, 0)
        Me.grb.Name = "grb"
        Me.grb.Size = New System.Drawing.Size(928, 382)
        Me.grb.TabIndex = 0
        Me.grb.TabStop = False
        '
        'txtNumIdentidad
        '
        Me.txtNumIdentidad.Location = New System.Drawing.Point(169, 23)
        Me.txtNumIdentidad.MaxLength = 15
        Me.txtNumIdentidad.Name = "txtNumIdentidad"
        Me.txtNumIdentidad.Size = New System.Drawing.Size(113, 21)
        Me.txtNumIdentidad.TabIndex = 214
        Me.txtNumIdentidad.Visible = False
        '
        'lblEtiqRUC
        '
        Me.lblEtiqRUC.AutoSize = True
        Me.lblEtiqRUC.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEtiqRUC.Location = New System.Drawing.Point(109, 26)
        Me.lblEtiqRUC.Name = "lblEtiqRUC"
        Me.lblEtiqRUC.Size = New System.Drawing.Size(54, 13)
        Me.lblEtiqRUC.TabIndex = 215
        Me.lblEtiqRUC.Text = "RUC/DNI:"
        Me.lblEtiqRUC.Visible = False
        '
        'cboTipo
        '
        Me.cboTipo.BackColor = System.Drawing.SystemColors.Window
        Me.cboTipo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboTipo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboTipo.FormattingEnabled = True
        Me.cboTipo.Location = New System.Drawing.Point(738, 19)
        Me.cboTipo.Name = "cboTipo"
        Me.cboTipo.Size = New System.Drawing.Size(187, 21)
        Me.cboTipo.TabIndex = 92
        '
        'lblTipo
        '
        Me.lblTipo.AutoSize = True
        Me.lblTipo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTipo.Location = New System.Drawing.Point(701, 22)
        Me.lblTipo.Name = "lblTipo"
        Me.lblTipo.Size = New System.Drawing.Size(31, 13)
        Me.lblTipo.TabIndex = 93
        Me.lblTipo.Text = "Tipo:"
        '
        'grbCiudad
        '
        Me.grbCiudad.Controls.Add(Me.btnBuscar)
        Me.grbCiudad.Controls.Add(Me.cboCiudad)
        Me.grbCiudad.Controls.Add(Me.Label12)
        Me.grbCiudad.Controls.Add(Me.Label13)
        Me.grbCiudad.Controls.Add(Me.cboPais)
        Me.grbCiudad.Location = New System.Drawing.Point(309, 5)
        Me.grbCiudad.Name = "grbCiudad"
        Me.grbCiudad.Size = New System.Drawing.Size(460, 41)
        Me.grbCiudad.TabIndex = 213
        Me.grbCiudad.TabStop = False
        Me.grbCiudad.Text = "Filtro por Ciudad"
        Me.grbCiudad.Visible = False
        '
        'btnBuscar
        '
        Me.btnBuscar.Image = Global.SETours.My.Resources.Resources.find
        Me.btnBuscar.Location = New System.Drawing.Point(425, 11)
        Me.btnBuscar.Name = "btnBuscar"
        Me.btnBuscar.Size = New System.Drawing.Size(32, 26)
        Me.btnBuscar.TabIndex = 213
        Me.btnBuscar.UseVisualStyleBackColor = True
        Me.btnBuscar.Visible = False
        '
        'cboCiudad
        '
        Me.cboCiudad.BackColor = System.Drawing.SystemColors.Window
        Me.cboCiudad.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboCiudad.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboCiudad.FormattingEnabled = True
        Me.cboCiudad.Location = New System.Drawing.Point(250, 15)
        Me.cboCiudad.Name = "cboCiudad"
        Me.cboCiudad.Size = New System.Drawing.Size(171, 21)
        Me.cboCiudad.TabIndex = 210
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label12.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label12.Location = New System.Drawing.Point(205, 18)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(44, 13)
        Me.Label12.TabIndex = 212
        Me.Label12.Text = "Ciudad:"
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label13.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label13.Location = New System.Drawing.Point(6, 18)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(30, 13)
        Me.Label13.TabIndex = 211
        Me.Label13.Text = "País:"
        '
        'cboPais
        '
        Me.cboPais.BackColor = System.Drawing.SystemColors.Window
        Me.cboPais.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboPais.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboPais.FormattingEnabled = True
        Me.cboPais.Location = New System.Drawing.Point(42, 15)
        Me.cboPais.Name = "cboPais"
        Me.cboPais.Size = New System.Drawing.Size(157, 21)
        Me.cboPais.TabIndex = 209
        '
        'lblCategoriaSEToursEtiq
        '
        Me.lblCategoriaSEToursEtiq.AutoSize = True
        Me.lblCategoriaSEToursEtiq.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCategoriaSEToursEtiq.Location = New System.Drawing.Point(462, 22)
        Me.lblCategoriaSEToursEtiq.Name = "lblCategoriaSEToursEtiq"
        Me.lblCategoriaSEToursEtiq.Size = New System.Drawing.Size(100, 13)
        Me.lblCategoriaSEToursEtiq.TabIndex = 111
        Me.lblCategoriaSEToursEtiq.Text = "Categoría SETours:"
        Me.lblCategoriaSEToursEtiq.Visible = False
        '
        'cboCategSeTours
        '
        Me.cboCategSeTours.BackColor = System.Drawing.Color.White
        Me.cboCategSeTours.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboCategSeTours.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboCategSeTours.FormattingEnabled = True
        Me.cboCategSeTours.Location = New System.Drawing.Point(568, 20)
        Me.cboCategSeTours.Name = "cboCategSeTours"
        Me.cboCategSeTours.Size = New System.Drawing.Size(113, 21)
        Me.cboCategSeTours.TabIndex = 3
        Me.cboCategSeTours.Visible = False
        '
        'lblCategoriaEtiq
        '
        Me.lblCategoriaEtiq.AutoSize = True
        Me.lblCategoriaEtiq.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCategoriaEtiq.Location = New System.Drawing.Point(348, 23)
        Me.lblCategoriaEtiq.Name = "lblCategoriaEtiq"
        Me.lblCategoriaEtiq.Size = New System.Drawing.Size(58, 13)
        Me.lblCategoriaEtiq.TabIndex = 109
        Me.lblCategoriaEtiq.Text = "Categoría:"
        Me.lblCategoriaEtiq.Visible = False
        '
        'nudCategoria
        '
        Me.nudCategoria.BackColor = System.Drawing.SystemColors.Window
        Me.nudCategoria.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.nudCategoria.Location = New System.Drawing.Point(412, 20)
        Me.nudCategoria.Maximum = New Decimal(New Integer() {5, 0, 0, 0})
        Me.nudCategoria.Name = "nudCategoria"
        Me.nudCategoria.Size = New System.Drawing.Size(44, 21)
        Me.nudCategoria.TabIndex = 2
        Me.nudCategoria.Visible = False
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(9, 23)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(65, 13)
        Me.Label2.TabIndex = 12
        Me.Label2.Text = "Descripción:"
        '
        'txtRazonSocial
        '
        Me.txtRazonSocial.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtRazonSocial.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtRazonSocial.Location = New System.Drawing.Point(80, 20)
        Me.txtRazonSocial.MaxLength = 60
        Me.txtRazonSocial.Name = "txtRazonSocial"
        Me.txtRazonSocial.Size = New System.Drawing.Size(226, 21)
        Me.txtRazonSocial.TabIndex = 1
        '
        'GroupBox1
        '
        Me.GroupBox1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GroupBox1.Location = New System.Drawing.Point(12, 49)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(903, 9)
        Me.GroupBox1.TabIndex = 4
        Me.GroupBox1.TabStop = False
        '
        'dgv
        '
        Me.dgv.AllowUserToAddRows = False
        Me.dgv.AllowUserToDeleteRows = False
        Me.dgv.AllowUserToOrderColumns = True
        Me.dgv.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        DataGridViewCellStyle13.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle13.BackColor = System.Drawing.SystemColors.ControlDarkDark
        DataGridViewCellStyle13.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle13.ForeColor = System.Drawing.Color.White
        DataGridViewCellStyle13.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle13.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle13.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgv.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle13
        Me.dgv.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgv.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Descripcion, Me.CodTipo, Me.DescTipo, Me.IDCiudad, Me.DescCiudad, Me.IDPais, Me.DescPais, Me.Codigo, Me.Margen, Me.OperadorInt})
        Me.dgv.Location = New System.Drawing.Point(12, 64)
        Me.dgv.Name = "dgv"
        Me.dgv.ReadOnly = True
        DataGridViewCellStyle14.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle14.BackColor = System.Drawing.SystemColors.ControlDarkDark
        DataGridViewCellStyle14.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle14.ForeColor = System.Drawing.Color.White
        DataGridViewCellStyle14.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle14.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle14.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgv.RowHeadersDefaultCellStyle = DataGridViewCellStyle14
        Me.dgv.Size = New System.Drawing.Size(904, 306)
        Me.dgv.TabIndex = 6
        '
        'Descripcion
        '
        Me.Descripcion.DataPropertyName = "Descripcion"
        Me.Descripcion.HeaderText = "Descripción"
        Me.Descripcion.Name = "Descripcion"
        Me.Descripcion.ReadOnly = True
        '
        'CodTipo
        '
        Me.CodTipo.DataPropertyName = "CodTipo"
        Me.CodTipo.HeaderText = "Tipo"
        Me.CodTipo.Name = "CodTipo"
        Me.CodTipo.ReadOnly = True
        Me.CodTipo.Visible = False
        '
        'DescTipo
        '
        Me.DescTipo.DataPropertyName = "DescTipo"
        Me.DescTipo.HeaderText = "Tipo Proveedor"
        Me.DescTipo.Name = "DescTipo"
        Me.DescTipo.ReadOnly = True
        Me.DescTipo.Visible = False
        '
        'IDCiudad
        '
        Me.IDCiudad.DataPropertyName = "IDCiudad"
        Me.IDCiudad.HeaderText = "Ciudad"
        Me.IDCiudad.Name = "IDCiudad"
        Me.IDCiudad.ReadOnly = True
        Me.IDCiudad.Visible = False
        '
        'DescCiudad
        '
        Me.DescCiudad.DataPropertyName = "DescCiudad"
        Me.DescCiudad.HeaderText = "Ciudad"
        Me.DescCiudad.Name = "DescCiudad"
        Me.DescCiudad.ReadOnly = True
        Me.DescCiudad.Visible = False
        '
        'IDPais
        '
        Me.IDPais.DataPropertyName = "IDPais"
        Me.IDPais.HeaderText = "País"
        Me.IDPais.Name = "IDPais"
        Me.IDPais.ReadOnly = True
        Me.IDPais.Visible = False
        '
        'DescPais
        '
        Me.DescPais.DataPropertyName = "DescPais"
        Me.DescPais.HeaderText = "País"
        Me.DescPais.Name = "DescPais"
        Me.DescPais.ReadOnly = True
        Me.DescPais.Visible = False
        '
        'Codigo
        '
        Me.Codigo.DataPropertyName = "Codigo"
        Me.Codigo.HeaderText = "Código"
        Me.Codigo.Name = "Codigo"
        Me.Codigo.ReadOnly = True
        Me.Codigo.Visible = False
        '
        'Margen
        '
        Me.Margen.DataPropertyName = "Margen"
        Me.Margen.HeaderText = "Margen"
        Me.Margen.Name = "Margen"
        Me.Margen.ReadOnly = True
        Me.Margen.Visible = False
        '
        'OperadorInt
        '
        Me.OperadorInt.DataPropertyName = "OperadorInt"
        Me.OperadorInt.HeaderText = "Operador Internacional"
        Me.OperadorInt.Name = "OperadorInt"
        Me.OperadorInt.ReadOnly = True
        Me.OperadorInt.Visible = False
        '
        'frmAyudas
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(928, 382)
        Me.Controls.Add(Me.grb)
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmAyudas"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "frmAyudas"
        Me.grb.ResumeLayout(False)
        Me.grb.PerformLayout()
        Me.grbCiudad.ResumeLayout(False)
        Me.grbCiudad.PerformLayout()
        CType(Me.nudCategoria, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgv, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents grb As System.Windows.Forms.GroupBox
    Friend WithEvents dgv As System.Windows.Forms.DataGridView
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents txtRazonSocial As System.Windows.Forms.TextBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents lblCategoriaSEToursEtiq As System.Windows.Forms.Label
    Friend WithEvents cboCategSeTours As System.Windows.Forms.ComboBox
    Friend WithEvents lblCategoriaEtiq As System.Windows.Forms.Label
    Friend WithEvents nudCategoria As System.Windows.Forms.NumericUpDown
    Friend WithEvents grbCiudad As System.Windows.Forms.GroupBox
    Friend WithEvents cboCiudad As System.Windows.Forms.ComboBox
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents cboPais As System.Windows.Forms.ComboBox
    Friend WithEvents btnBuscar As System.Windows.Forms.Button
    Friend WithEvents cboTipo As System.Windows.Forms.ComboBox
    Friend WithEvents lblTipo As System.Windows.Forms.Label
    Friend WithEvents Descripcion As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents CodTipo As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DescTipo As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents IDCiudad As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DescCiudad As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents IDPais As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DescPais As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Codigo As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Margen As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents OperadorInt As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents txtNumIdentidad As System.Windows.Forms.TextBox
    Friend WithEvents lblEtiqRUC As System.Windows.Forms.Label
End Class
