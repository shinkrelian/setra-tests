﻿Imports ComSEToursBL2
Public Class frmMantPaxporCotizDatoMem

    Public frmRef As frmIngCotizacionDato
    Public frmRefRes As Object 'frmIngReservasControlporFileDato
    Public strModoEdicion As Char
    Public blnDgvFilaVacia As Boolean
    Public blnGeneradoOperacion As Boolean
    Public strIDNacionalidad As String = ""
    Dim blnCambios As Boolean = False
    Dim blnLoad As Boolean = False
    Public blnSoloConsulta As Boolean = False
    Public blnClienteAPT As Boolean = False
    Dim blnTourConductor As Boolean = False
    Dim blnChangedTC As Boolean = False

    Private Sub frmMantPaxporCotizDatoMem_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Activated
        blnLoad = True
    End Sub

    Private Sub frmMantPaxporCotizDatoMem_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        If blnCambios = True Then
            If MessageBox.Show("Se han realizado cambios. ¿Está Ud. seguro de salir sin Aceptar?", "Confirmar", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = Windows.Forms.DialogResult.Yes Then
                e.Cancel = False

            Else
                e.Cancel = True
            End If
        End If

    End Sub

    Private Sub frmMantDatosAdmProveeDatoMem_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            txtOrden.BackColor = gColorTextBoxObligat
            txtApellidos.BackColor = gColorTextBoxObligat
            txtNombres.BackColor = gColorTextBoxObligat
            cboIDIdentidad.BackColor = gColorTextBoxObligat
            'cboNacionalidad.BackColor = gColorTextBoxObligat
            cboTitulo.BackColor = gColorTextBoxObligat
            cboTituloAcademico.BackColor = gColorTextBoxObligat

            lblNumberAPT.Visible = blnClienteAPT
            txtNumberAPT.Visible = blnClienteAPT

            CargarCombos()
            If strModoEdicion = "E" Then
                lblTitulo.Text = "Edición Pax"
                If Not frmRef Is Nothing Then
                    CargarControles()
                ElseIf Not frmRefRes Is Nothing Then
                    CargarControlesparaReservas()
                End If
            ElseIf strModoEdicion = "N" Then
                lblTitulo.Text = "Nuevo Pax"
                cboNacionalidad.SelectedValue = strIDNacionalidad
                blnCambios = True
            End If

            If Not btnAceptar.Enabled Then blnCambios = False
            If Not blnLoad Then blnCambios = False

            If blnSoloConsulta Then CargarblnConsulta(btnAceptar, grb, blnSoloConsulta)
            HabilitarControlesNoActualizadosxApiCont()
            blnTourConductor = chkTC.Checked
        Catch ex As Exception
            MessageBox.Show(ex.Message, ex.Source, MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub HabilitarControlesNoActualizadosxApiCont()
        Try
            Dim blnActualizadoxExt As Boolean = False
            If Not frmRef Is Nothing Then
                blnActualizadoxExt = frmRef.dgvPax.CurrentRow.Cells("FlActualizadoxExtrNet").Value
            ElseIf Not frmRefRes Is Nothing Then
                blnActualizadoxExt = frmRefRes.dgvPax.CurrentRow.Cells("FlActualizadoxExtrNet").Value
            End If
            If blnSoloConsulta And blnActualizadoxExt Then
                CargarblnConsulta(btnAceptar, grb, False)
                cboTitulo.Enabled = False
                cboTituloAcademico.Enabled = False
                txtApellidos.Enabled = False
                txtNombres.Enabled = False
                dtpFecNacimiento.Enabled = False
                txtPeso.Enabled = False
                txtObservEspecial.Enabled = False
                cboIDIdentidad.Enabled = False
                txtNumIdentidad.Enabled = False
                cboNacionalidad.Enabled = False
                cboIDPaisResidencia.Enabled = False
                txtNumberAPT.Enabled = False
            End If
        Catch ex As Exception
            Throw
        End Try
    End Sub

    Private Sub Grabar()
        Try
            With frmRef.dgvPax
                Dim bytFilaDgv As Byte = .RowCount
                If Not blnDgvFilaVacia Then
                    .Rows.Add("", "")
                Else
                    bytFilaDgv -= 1
                End If
                .Item("NroOrd", bytFilaDgv).Value = txtOrden.Text
                .Item("Apellidos", bytFilaDgv).Value = txtApellidos.Text
                .Item("Nombres", bytFilaDgv).Value = txtNombres.Text
                .Item("NumIdentidad", bytFilaDgv).Value = txtNumIdentidad.Text
                .Item("IDIdentidad", bytFilaDgv).Value = cboIDIdentidad.SelectedValue.ToString
                .Item("DescIdentidad", bytFilaDgv).Value = cboIDIdentidad.Text
                If cboNacionalidad.Text = "" Then
                    .Item("IDNacionalidad", bytFilaDgv).Value = "000001"
                Else
                    .Item("IDNacionalidad", bytFilaDgv).Value = cboNacionalidad.SelectedValue.ToString
                End If

                .Item("DescNacionalidad", bytFilaDgv).Value = cboNacionalidad.Text
                .Item("Titulo", bytFilaDgv).Value = cboTitulo.Text
                'If dtpFecNacimiento.Text <> "01/01/1900" Then
                '    .Item("FecNacimiento", bytFilaDgv).Value = dtpFecNacimiento.Text
                'Else
                '    .Item("FecNacimiento", bytFilaDgv).Value = ""
                'End If
                .Item("FecNacimiento", bytFilaDgv).Value = dtDTPickerText(dtpFecNacimiento, True)
                .Item("Peso", bytFilaDgv).Value = If(txtPeso.Text.Trim = "", "0", txtPeso.Text.Trim)
                .Item("ObservEspecial", bytFilaDgv).Value = txtObservEspecial.Text.Trim
                If cboIDPaisResidencia.Text = "" Then
                    .Item("IDPaisResidencia", bytFilaDgv).Value = "000001"
                Else
                    .Item("IDPaisResidencia", bytFilaDgv).Value = cboIDPaisResidencia.SelectedValue.ToString
                End If
                .Item("FecIngresoPais", bytFilaDgv).Value = dtDTPickerText(dtpFechaIngresoPais, True)

                .Item("FlNoShow", bytFilaDgv).Value = chkNoShow.Checked

                .Item("FlAdultoINC", bytFilaDgv).Value = chkEntradaAdulto.Checked

                .Item("NumberAPT", bytFilaDgv).Value = txtNumberAPT.Text

            End With
            frmRef.blnActualizaPax = True
        Catch ex As Exception
            Throw
        End Try
    End Sub

    Private Sub Actualizar()
        Try
            If blnTourConductor <> chkTC.Checked Then
                frmRef.blnUpdateTCxPax = True
            End If

            With frmRef.dgvPax
                Dim bytFilaDgv As Byte = .CurrentRow.Index
                .Item("NroOrd", bytFilaDgv).Value = txtOrden.Text
                .Item("Apellidos", bytFilaDgv).Value = txtApellidos.Text
                .Item("Nombres", bytFilaDgv).Value = txtNombres.Text
                .Item("NumIdentidad", bytFilaDgv).Value = txtNumIdentidad.Text
                .Item("IDIdentidad", bytFilaDgv).Value = cboIDIdentidad.SelectedValue.ToString
                .Item("DescIdentidad", bytFilaDgv).Value = cboIDIdentidad.Text

                If cboNacionalidad.Text = "" Then
                    .Item("IDNacionalidad", bytFilaDgv).Value = "000001"
                Else
                    .Item("IDNacionalidad", bytFilaDgv).Value = cboNacionalidad.SelectedValue.ToString
                End If

                .Item("DescNacionalidad", bytFilaDgv).Value = cboNacionalidad.Text
                .Item("Titulo", bytFilaDgv).Value = cboTitulo.Text

                'Agregado el dia 27-05-14
                .Item("TituloAcademico", bytFilaDgv).Value = cboTituloAcademico.Text
                .Item("TC", bytFilaDgv).Value = chkTC.Checked

                'If dtpFecNacimiento.Text <> "01/01/1900" Then
                '    .Item("FecNacimiento", bytFilaDgv).Value = dtpFecNacimiento.Text
                'Else
                '    .Item("FecNacimiento", bytFilaDgv).Value = ""
                'End If

                .Item("FecNacimiento", bytFilaDgv).Value = dtDTPickerText(dtpFecNacimiento, True)

                .Item("Peso", bytFilaDgv).Value = If(txtPeso.Text.Trim = "", "0", txtPeso.Text.Trim) 'txtPeso.Text.Trim
                .Item("Residente", bytFilaDgv).Value = chkResidente.Checked
                .Item("ObservEspecial", bytFilaDgv).Value = txtObservEspecial.Text.Trim

                If cboIDPaisResidencia.Text = "" Then
                    .Item("IDPaisResidencia", bytFilaDgv).Value = "000001"
                Else
                    .Item("IDPaisResidencia", bytFilaDgv).Value = cboIDPaisResidencia.SelectedValue.ToString
                End If
                .Item("FecIngresoPais", bytFilaDgv).Value = dtDTPickerText(dtpFechaIngresoPais, True)
                .Item("PassValidado", bytFilaDgv).Value = chkPassValidado.Checked

                .Item("FlNoShow", bytFilaDgv).Value = chkNoShow.Checked

                .Item("FlAdultoINC", bytFilaDgv).Value = chkEntradaAdulto.Checked

                .Item("NumberAPT", bytFilaDgv).Value = txtNumberAPT.Text

            End With

            frmRef.blnActualizaPax = True

        Catch ex As Exception
            Throw
        End Try

    End Sub

    Private Sub CargarControles()
        Try
            With frmRef.dgvPax
                Dim bytFilaDgv As Byte = .CurrentRow.Index
                If Not .Item("NroOrd", bytFilaDgv).Value Is Nothing Then
                    txtOrden.Text = .Item("NroOrd", bytFilaDgv).Value.ToString
                End If
                If Not .Item("Apellidos", bytFilaDgv).Value Is Nothing Then
                    txtApellidos.Text = .Item("Apellidos", bytFilaDgv).Value.ToString
                End If
                If Not .Item("Nombres", bytFilaDgv).Value Is Nothing Then
                    txtNombres.Text = .Item("Nombres", bytFilaDgv).Value.ToString
                End If
                If Not .Item("NumIdentidad", bytFilaDgv).Value Is Nothing Then
                    txtNumIdentidad.Text = .Item("NumIdentidad", bytFilaDgv).Value.ToString
                End If
                If Not .Item("IDIdentidad", bytFilaDgv).Value Is Nothing Then
                    cboIDIdentidad.SelectedValue = .Item("IDIdentidad", bytFilaDgv).Value.ToString
                End If
                If Not .Item("IDNacionalidad", bytFilaDgv).Value Is Nothing Then
                    cboNacionalidad.SelectedValue = .Item("IDNacionalidad", bytFilaDgv).Value.ToString
                End If
                If Not .Item("Titulo", bytFilaDgv).Value Is Nothing Then
                    cboTitulo.Text = .Item("Titulo", bytFilaDgv).Value.ToString
                End If
                If Not .Item("FecNacimiento", bytFilaDgv).Value Is Nothing Then
                    dtpFecNacimiento.Value = If(.Item("FecNacimiento", bytFilaDgv).Value.ToString = "", "01/01/1900", .Item("FecNacimiento", bytFilaDgv).Value.ToString)
                End If
                If Not .Item("Peso", bytFilaDgv).Value Is Nothing Then
                    txtPeso.Text = .Item("Peso", bytFilaDgv).Value.ToString
                End If
                If Not .Item("Residente", bytFilaDgv).Value Is Nothing Then
                    chkResidente.Checked = .Item("Residente", bytFilaDgv).Value
                End If
                If Not .Item("ObservEspecial", bytFilaDgv).Value Is Nothing Then
                    txtObservEspecial.Text = .Item("ObservEspecial", bytFilaDgv).Value.ToString
                End If
                If Not .Item("IDPaisResidencia", bytFilaDgv).Value Is Nothing Then
                    cboIDPaisResidencia.SelectedValue = .Item("IDPaisResidencia", bytFilaDgv).Value.ToString
                End If

                If Not .Item("FecIngresoPais", bytFilaDgv).Value Is Nothing Then
                    dtpFechaIngresoPais.Value = If(.Item("FecIngresoPais", bytFilaDgv).Value.ToString = "", "01/01/1900", .Item("FecIngresoPais", bytFilaDgv).Value.ToString)
                End If

                If Not .Item("PassValidado", bytFilaDgv).Value Is Nothing Then
                    chkPassValidado.Checked = .Item("PassValidado", bytFilaDgv).Value
                End If

                If Not .Item("FlNoShow", bytFilaDgv).Value Is Nothing Then
                    chkNoShow.Checked = .Item("FlNoShow", bytFilaDgv).Value
                End If

                'Agregado el dia 27-05-14
                If Not .Item("TituloAcademico", bytFilaDgv).Value Is Nothing Then
                    cboTituloAcademico.Text = .Item("TituloAcademico", bytFilaDgv).Value
                End If
                If Not .Item("TC", bytFilaDgv).Value Is Nothing Then
                    chkTC.Checked = .Item("TC", bytFilaDgv).Value
                End If

                If Not .Item("FlAdultoINC", bytFilaDgv).Value Is Nothing Then
                    chkEntradaAdulto.Checked = .Item("FlAdultoINC", bytFilaDgv).Value
                End If

                If Not .Item("NumberAPT", bytFilaDgv).Value Is Nothing Then
                    txtNumberAPT.Text = .Item("NumberAPT", bytFilaDgv).Value.ToString
                End If

            End With

        Catch ex As Exception
            Throw
        End Try
    End Sub

    Private Sub btnAceptar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAceptar.Click
        If Not btnAceptar.Enabled Then Exit Sub
        Try
            If Not blnValidarIngresos() Then Exit Sub

            If strModoEdicion = "N" Then
                If Not frmRef Is Nothing Then
                    Grabar()
                ElseIf Not frmRefRes Is Nothing Then
                    GrabarparaReservas()
                End If

                'MessageBox.Show("Se creó el pax exitosamente", gstrTituloMsg, MessageBoxButtons.OK, MessageBoxIcon.Information)

            ElseIf strModoEdicion = "E" Then
                If Not frmRef Is Nothing Then
                    Actualizar()
                ElseIf Not frmRefRes Is Nothing Then
                    ActualizarparaReservas()
                End If
                'MessageBox.Show("Se actualizó el pax exitosamente", gstrTituloMsg, MessageBoxButtons.OK, MessageBoxIcon.Information)

            End If
            blnCambios = False
            Me.Close()


        Catch ex As Exception
            MessageBox.Show(ex.Message, ex.Source, MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub btnSalir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSalir.Click
        Me.Close()
    End Sub
    Private Sub frmMantBancosDato_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        Select Case e.KeyCode
            Case Keys.F6
                btnAceptar_Click(Nothing, Nothing)
            Case Keys.Escape
                btnSalir_Click(Nothing, Nothing)
        End Select
    End Sub

    Private Function blnValidarIngresos() As Boolean
        Dim blnOk As Boolean = True

        If txtOrden.Text.Trim = "" Then
            ErrPrv.SetError(txtOrden, "Debe ingresar Nro. Orden")
            blnOk = False
        End If
        If cboTitulo.Text = "" Then
            ErrPrv.SetError(cboTitulo, "Debe seleccionar Título")
            blnOk = False
        End If
        If txtNombres.Text.Trim = "" Then
            ErrPrv.SetError(txtNombres, "Debe ingresar Nombres")
            blnOk = False
        End If
        If txtApellidos.Text.Trim = "" Then
            ErrPrv.SetError(txtApellidos, "Debe ingresar Apellidos")
            blnOk = False
        End If
        If cboIDIdentidad.Text = "" Then
            ErrPrv.SetError(cboIDIdentidad, "Debe seleccionar Tipo Docum. Identidad")
            blnOk = False
        End If

        If cboTituloAcademico.Text = "" Then
            ErrPrv.SetError(cboTituloAcademico, "Debe seleccionar Título Académico")
            blnOk = False
        End If
        'If txtNumIdentidad.Text.Trim = "" Then
        '    ErrPrv.SetError(txtNumIdentidad, "Debe ingresar Nro. de Documento")
        '    blnOk = False
        'End If

        If Not blnOk Then
            Return False
        Else
            Return True
        End If
    End Function

    Private Sub txtUsuario_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtNombres.TextChanged, _
    txtApellidos.TextChanged, cboIDIdentidad.TextChanged, cboNacionalidad.TextChanged, txtNumIdentidad.TextChanged, _
    cboTitulo.TextChanged, txtOrden.TextChanged, chkResidente.CheckedChanged, cboIDPaisResidencia.SelectionChangeCommitted, _
    dtpFecNacimiento.ValueChanged, txtObservEspecial.TextChanged, txtPeso.TextChanged, dtpFechaIngresoPais.ValueChanged, chkPassValidado.CheckedChanged, chkNoShow.CheckedChanged, chkEntradaAdulto.CheckedChanged, txtNumberAPT.TextChanged
        Try
            Select Case sender.name
                Case "txtNombres"
                    ErrPrv.SetError(txtNombres, "")

                Case "txtApellidos"
                    ErrPrv.SetError(txtApellidos, "")

                Case "cboIDIdentidad"
                    ErrPrv.SetError(cboIDIdentidad, "")

                Case "cboNacionalidad"
                    ErrPrv.SetError(cboNacionalidad, "")

                Case "txtNumIdentidad"
                    ErrPrv.SetError(txtNumIdentidad, "")

                Case "txtNumberAPT"
                    ErrPrv.SetError(txtNumberAPT, "")

                Case "cboTitulo"
                    ErrPrv.SetError(cboTitulo, "")

                Case "txtOrden"
                    ErrPrv.SetError(txtOrden, "")

                Case "chkResidente"

                Case "dtpFecNacimiento"
                    DTPickerValueCheck(dtpFecNacimiento, blnLoad)
                Case "dtpFechaIngresoPais"
                    If Not frmRef Is Nothing Then
                        DTPickerValueCheck(dtpFechaIngresoPais, blnLoad, frmRef.dtpFecInicio.Value)
                    ElseIf Not frmRefRes Is Nothing Then
                        DTPickerValueCheck(dtpFechaIngresoPais, blnLoad, frmRefRes.dtpFecInicio.Value)
                    Else
                        DTPickerValueCheck(dtpFechaIngresoPais, blnLoad)
                    End If
            End Select

            If blnLoad Then
                btnAceptar.Enabled = True
                blnCambios = True
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message, ex.Source, MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub CargarCombos()
        Try
            Dim objBLTdoc As New clsTablasApoyoBN.clsTipoDocIdentBN
            pCargaCombosBox(cboIDIdentidad, objBLTdoc.ConsultarCbo)

            Dim objBLUbig As New clsTablasApoyoBN.clsUbigeoBN
            pCargaCombosBox(cboNacionalidad, objBLUbig.ConsultarNacionalidadCbo())
            pCargaCombosBox(cboIDPaisResidencia, objBLUbig.ConsultarNacionalidadCbo())

            With cboTitulo.Items
                .Clear()
                .Add("Chd.")
                .Add("Inf.")
                .Add("Mr.")
                .Add("Ms.")

            End With

            'Cambobox Titulo Academico
            With cboTituloAcademico.Items
                .Clear()
                .Add("---")
                .Add("Dr.")
                .Add("Ing.")
                .Add("Mag.")

            End With

        Catch ex As Exception
            Throw
        End Try
    End Sub



    Private Sub GrabarparaReservas()
        Try
            With frmRefRes.dgvPax
                Dim bytFilaDgv As Byte = .RowCount

                If Not blnDgvFilaVacia Then
                    .Rows.Add("", "")
                Else
                    bytFilaDgv -= 1
                End If
                .Item("NroOrd", bytFilaDgv).Value = txtOrden.Text
                .Item("Apellidos", bytFilaDgv).Value = txtApellidos.Text.ToUpper
                .Item("Nombres", bytFilaDgv).Value = txtNombres.Text.ToUpper
                .Item("NumIdentidad", bytFilaDgv).Value = txtNumIdentidad.Text
                .Item("IDIdentidad", bytFilaDgv).Value = cboIDIdentidad.SelectedValue.ToString
                .Item("DescIdentidad", bytFilaDgv).Value = cboIDIdentidad.Text
                '.Item("IDNacionalidad", bytFilaDgv).Value = cboNacionalidad.SelectedValue.ToString
                If cboNacionalidad.Text = "" Then
                    .Item("IDNacionalidad", bytFilaDgv).Value = "000001"
                Else
                    .Item("IDNacionalidad", bytFilaDgv).Value = cboNacionalidad.SelectedValue.ToString
                End If

                .Item("DescNacionalidad", bytFilaDgv).Value = cboNacionalidad.Text
                .Item("Titulo", bytFilaDgv).Value = cboTitulo.Text
                'If dtpFecNacimiento.Text <> "01/01/1900" Then
                '    .Item("FecNacimiento", bytFilaDgv).Value = dtpFecNacimiento.Text
                'Else
                '    .Item("FecNacimiento", bytFilaDgv).Value = ""
                'End If
                .Item("FecNacimiento", bytFilaDgv).Value = dtDTPickerText(dtpFecNacimiento, True)
                .Item("Peso", bytFilaDgv).Value = If(txtPeso.Text.Trim = "", "0", txtPeso.Text.Trim) 'txtPeso.Text.Trim
                .Item("Residente", bytFilaDgv).Value = chkResidente.Checked
                .Item("ObservEspecial", bytFilaDgv).Value = txtObservEspecial.Text.Trim
                If cboIDPaisResidencia.Text = "" Then
                    .Item("IDPaisResidencia", bytFilaDgv).Value = "000001"
                Else
                    .Item("IDPaisResidencia", bytFilaDgv).Value = cboIDPaisResidencia.SelectedValue.ToString
                End If
                .Item("FecIngresoPais", bytFilaDgv).Value = dtDTPickerText(dtpFechaIngresoPais, True)

                .Item("FlNoShow", bytFilaDgv).Value = chkNoShow.Checked

                .Item("FlAdultoINC", bytFilaDgv).Value = chkEntradaAdulto.Checked

                .Item("NumberAPT", bytFilaDgv).Value = txtNumberAPT.Text

            End With
            frmRefRes.blnActualizaPax = True
        Catch ex As Exception
            Throw
        End Try
    End Sub

    Private Sub ActualizarparaReservas()
        Try
            With frmRefRes.dgvPax
                Dim bytFilaDgv As Byte = .CurrentRow.Index
                .Item("NroOrd", bytFilaDgv).Value = txtOrden.Text
                .Item("Apellidos", bytFilaDgv).Value = txtApellidos.Text
                .Item("Nombres", bytFilaDgv).Value = txtNombres.Text
                .Item("NumIdentidad", bytFilaDgv).Value = txtNumIdentidad.Text
                .Item("IDIdentidad", bytFilaDgv).Value = cboIDIdentidad.SelectedValue.ToString
                .Item("DescIdentidad", bytFilaDgv).Value = cboIDIdentidad.Text
                '.Item("IDNacionalidad", bytFilaDgv).Value = cboNacionalidad.SelectedValue.ToString
                If cboNacionalidad.Text = "" Then
                    .Item("IDNacionalidad", bytFilaDgv).Value = "000001"
                Else
                    .Item("IDNacionalidad", bytFilaDgv).Value = cboNacionalidad.SelectedValue.ToString
                End If

                .Item("DescNacionalidad", bytFilaDgv).Value = cboNacionalidad.Text
                .Item("Titulo", bytFilaDgv).Value = cboTitulo.Text



                'Agregado el dia 27-05-14
                .Item("TituloAcademico1", bytFilaDgv).Value = cboTituloAcademico.Text
                .Item("TC1", bytFilaDgv).Value = chkTC.Checked

                'Agregado el dia 27-05-14
                '.Item("TituloAcademico", bytFilaDgv).Value = cboTituloAcademico.Text
                '.Item("TC", bytFilaDgv).Value = chkResidente.Checked

                'If dtpFecNacimiento.Text <> "01/01/1900" Then
                '    .Item("FecNacimiento", bytFilaDgv).Value = dtpFecNacimiento.Text
                'Else
                '    .Item("FecNacimiento", bytFilaDgv).Value = ""
                'End If
                .Item("FecNacimiento", bytFilaDgv).Value = dtDTPickerText(dtpFecNacimiento, True)
                .Item("Peso", bytFilaDgv).Value = If(txtPeso.Text.Trim = "", "0", txtPeso.Text.Trim) 'txtPeso.Text.Trim
                .Item("Residente", bytFilaDgv).Value = chkResidente.Checked
                .Item("ObservEspecial", bytFilaDgv).Value = txtObservEspecial.Text.Trim

                If cboIDPaisResidencia.Text = "" Then
                    .Item("IDPaisResidencia", bytFilaDgv).Value = "000001"
                Else
                    .Item("IDPaisResidencia", bytFilaDgv).Value = cboIDPaisResidencia.SelectedValue.ToString
                End If
                .Item("FecIngresoPais", bytFilaDgv).Value = dtDTPickerText(dtpFechaIngresoPais, True)
                .Item("PassValidado", bytFilaDgv).Value = chkPassValidado.Checked
                .Item("FlNoShow", bytFilaDgv).Value = chkNoShow.Checked

                .Item("FlAdultoINC", bytFilaDgv).Value = chkEntradaAdulto.Checked

                .Item("NumberAPT", bytFilaDgv).Value = txtNumberAPT.Text

            End With
            frmRefRes.blnActualizaPax = True
        Catch ex As Exception
            Throw
        End Try
    End Sub

    Private Sub CargarControlesparaReservas()
        Try
            With frmRefRes.dgvPax
                Dim bytFilaDgv As Byte = .CurrentRow.Index

                If Not .Item("NroOrd", bytFilaDgv).Value Is Nothing Then
                    txtOrden.Text = .Item("NroOrd", bytFilaDgv).Value.ToString
                End If
                If Not .Item("Apellidos", bytFilaDgv).Value Is Nothing Then
                    txtApellidos.Text = .Item("Apellidos", bytFilaDgv).Value.ToString
                End If
                If Not .Item("Nombres", bytFilaDgv).Value Is Nothing Then
                    txtNombres.Text = .Item("Nombres", bytFilaDgv).Value.ToString
                End If
                If Not .Item("NumIdentidad", bytFilaDgv).Value Is Nothing Then
                    txtNumIdentidad.Text = .Item("NumIdentidad", bytFilaDgv).Value.ToString
                End If
                If Not .Item("IDIdentidad", bytFilaDgv).Value Is Nothing Then
                    cboIDIdentidad.SelectedValue = .Item("IDIdentidad", bytFilaDgv).Value.ToString
                End If
                If Not .Item("IDNacionalidad", bytFilaDgv).Value Is Nothing Then
                    cboNacionalidad.SelectedValue = .Item("IDNacionalidad", bytFilaDgv).Value.ToString
                End If
                If Not .Item("Titulo", bytFilaDgv).Value Is Nothing Then
                    cboTitulo.Text = .Item("Titulo", bytFilaDgv).Value.ToString
                End If

                If Not .Item("FecNacimiento", bytFilaDgv).Value Is Nothing Then
                    dtpFecNacimiento.Value = If(.Item("FecNacimiento", bytFilaDgv).Value.ToString = "", "01/01/1900", .Item("FecNacimiento", bytFilaDgv).Value.ToString)
                End If
                If Not .Item("Peso", bytFilaDgv).Value Is Nothing Then
                    txtPeso.Text = .Item("Peso", bytFilaDgv).Value.ToString
                End If
                If Not .Item("Residente", bytFilaDgv).Value Is Nothing Then
                    chkResidente.Checked = CBool(.Item("Residente", bytFilaDgv).Value)
                End If
                If Not .Item("ObservEspecial", bytFilaDgv).Value Is Nothing Then
                    txtObservEspecial.Text = .Item("ObservEspecial", bytFilaDgv).Value.ToString
                End If

                If Not .Item("IDPaisResidencia", bytFilaDgv).Value Is Nothing Then
                    cboIDPaisResidencia.SelectedValue = .Item("IDPaisResidencia", bytFilaDgv).Value.ToString
                End If

                If Not .Item("FecIngresoPais", bytFilaDgv).Value Is Nothing Then
                    dtpFechaIngresoPais.Value = If(.Item("FecIngresoPais", bytFilaDgv).Value.ToString = "", "01/01/1900", .Item("FecIngresoPais", bytFilaDgv).Value.ToString)
                End If

                If Not .Item("PassValidado", bytFilaDgv).Value Is Nothing Then
                    chkPassValidado.Checked = .Item("PassValidado", bytFilaDgv).Value
                End If

                If Not .Item("FlNoShow", bytFilaDgv).Value Is Nothing Then
                    chkNoShow.Checked = .Item("FlNoShow", bytFilaDgv).Value
                End If

                'Agregado el dia 27-05-14
                If Not .Item("TituloAcademico1", bytFilaDgv).Value Is Nothing Then
                    cboTituloAcademico.Text = .Item("TituloAcademico1", bytFilaDgv).Value
                End If
                If Not .Item("TC1", bytFilaDgv).Value Is Nothing Then
                    chkTC.Checked = .Item("TC1", bytFilaDgv).Value
                End If


                If Not .Item("FlAdultoINC", bytFilaDgv).Value Is Nothing Then
                    chkEntradaAdulto.Checked = .Item("FlAdultoINC", bytFilaDgv).Value
                End If

                If Not .Item("NumberAPT", bytFilaDgv).Value Is Nothing Then
                    txtNumberAPT.Text = .Item("NumberAPT", bytFilaDgv).Value.ToString
                End If

            End With
        Catch ex As Exception
            Throw
        End Try
    End Sub

    Private Sub txtPeso_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtPeso.KeyPress
        Dim Caracter As Char = e.KeyChar
        If Not IsNumeric(Caracter) And (Caracter = ChrW(Keys.Back)) = False Then
            e.KeyChar = Chr(0)
        End If
    End Sub

   

    Private Sub frmMantPaxporCotizDatoMem_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        Me.Dispose()
    End Sub

    Private Sub txtOrden_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtOrden.KeyPress
        If Asc(e.KeyChar) <> 8 Then
            If Not IsNumeric(e.KeyChar) Then
                e.KeyChar = ""
            End If
        End If
    End Sub

   
    'Private Sub dtpFecNacimiento_ValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dtpFecNacimiento.ValueChanged

    '    DTPickerValueCheck(dtpFecNacimiento, blnLoad)
    'End Sub



    Private Sub txtApellidos_KeyPress(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtApellidos.KeyPress, txtNombres.KeyPress
        'Marco Llapapapasca
        '27-05-14 * se valida para que solo permita letras, espacio y no caracteres
        Dim oHandled As Boolean = True
        Dim Cadena = "ABCDEFGHIJKLMNOPQRSTUVWXYZÁÉÍÓÚ"
        If InStr(Cadena, Convert.ToString(e.KeyChar).ToUpper()) Then
            oHandled = False
        ElseIf Char.IsControl(e.KeyChar) Then

            If Asc(e.KeyChar) <> 8 Then
                Dim iData As IDataObject = Clipboard.GetDataObject()
                'Capturo el valor del portapapeles
                Dim TXT As String = CType(iData.GetData(DataFormats.Text), String)
                TXT = Convert.ToString(If(TXT Is Nothing, "*", TXT)).ToUpper()

                For index As Integer = 0 To TXT.Length - 1
                    'Verifico cada caracter si esta dentro de mi Cadena
                    If InStr(Cadena, TXT.Substring(index, 1).ToString()) Then
                        oHandled = False
                    Else
                        oHandled = True
                        Exit For
                    End If
                Next
            Else
                oHandled = False
            End If
        ElseIf Char.IsSeparator(e.KeyChar) Then
            oHandled = False
        Else
            oHandled = True
        End If
        e.Handled = oHandled
        If oHandled = True Then
            Dim oControl As Control = sender
            If oControl.Name = "txtApellidos" Then
                ErrPrv.SetError(txtApellidos, "Debe ingresar sólo estos caracteres: 'a-z','A-Z'")
            ElseIf oControl.Name = "txtNombres" Then
                ErrPrv.SetError(txtNombres, "Debe ingresar sólo estos caracteres: 'a-z','A-Z'")
            End If
        End If
    End Sub

    Private Sub txtNumIdentidad_KeyPress(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtNumIdentidad.KeyPress
        'Marco Llapapapasca
        '27-05-14 * se valida para que solo permita letras y numeros
        '004= PASAPORTE
        If cboIDIdentidad.SelectedValue.ToString() = "004" Then
            If Char.IsLetter(e.KeyChar) Then
                e.Handled = False
            ElseIf Char.IsControl(e.KeyChar) Then
                e.Handled = False
            ElseIf Char.IsNumber(e.KeyChar) Then
                e.Handled = False
            Else
                e.Handled = True
                ErrPrv.SetError(txtNumIdentidad, "Debe ingresar sólo estos caracteres: 'A-Z','0-9'")
            End If
        End If

    End Sub

    Private Sub chkTC_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkTC.CheckedChanged
        If blnLoad Then
            btnAceptar.Enabled = True
            blnCambios = True
        End If
    End Sub

    Private Sub cboTituloAcademico_SelectionChangeCommitted(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboTituloAcademico.SelectionChangeCommitted
        If blnLoad Then
            btnAceptar.Enabled = True
            blnCambios = True
        End If
    End Sub

    Private Sub txtNombres_MouseDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles txtNombres.MouseDown, txtApellidos.MouseDown
        If (e.Button = Windows.Forms.MouseButtons.Right) Then

            Dim iData As IDataObject = Clipboard.GetDataObject()
            Dim oHandled As Boolean = True

            If iData.GetDataPresent(DataFormats.Text) Then
                'Capturo el valor del portapapeles
                Dim TXT As String = CType(iData.GetData(DataFormats.Text), String)
                TXT = Convert.ToString(If(TXT Is Nothing, "*", TXT)).ToUpper()
                Dim Cadena = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"

                For index As Integer = 0 To TXT.Length - 1
                    'Verifico cada caracter si esta dentro de mi Cadena
                    If InStr(Cadena, TXT.Substring(index, 1).ToString()) Then
                        oHandled = False
                    Else
                        oHandled = True
                        Clipboard.Clear()
                        Exit For
                    End If
                Next
                Dim oControl As Control = sender
                If oHandled = True Then
 
                    If oControl.Name = "txtApellidos" Then
                        ErrPrv.SetError(txtApellidos, "Debe ingresar sólo estos caracteres: 'a-z','A-Z'")
                    ElseIf oControl.Name = "txtNombres" Then
                        ErrPrv.SetError(txtNombres, "Debe ingresar sólo estos caracteres: 'a-z','A-Z'")
                    End If
                End If
 

            End If
        End If
    End Sub

    Private Sub txtNumberAPT_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtNumberAPT.KeyPress
        If Char.IsLetter(e.KeyChar) Then
            e.Handled = False
        ElseIf Char.IsControl(e.KeyChar) Then
            e.Handled = False
        ElseIf Char.IsNumber(e.KeyChar) Then
            e.Handled = False
        Else
            e.Handled = True
            ErrPrv.SetError(txtNumIdentidad, "Debe ingresar sólo estos caracteres: 'A-Z','0-9'")
        End If
    End Sub
End Class