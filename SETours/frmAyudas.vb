﻿Imports ComSEToursBL2
Public Class frmAyudas
    'Public frmRefMantServicios As frmMantServiciosProveedorDato
    'Public frmRefConsDetServiciosProveedor As frmConsDetServiciosProveedor
    Public frmRefIngCotizacionDato As frmIngCotizacionDato
    'Public frmRefIngDetCotiDatoMem As frmIngDetCotizacionDatoMem
    Public frmRefCopiaServicios As frmCopiarServicios
    'Public frmRefGenPresupuesto As frmGenPresupuesto
    Public frmRefMantCotizVuelosVtas As frmMantCotizacionVuelosVentas
    'Public frmRefRepCotizacionGeneral As frmRepCotizacionGeneral
    Public frmRefIngPaxTransportMem As frmIngPaxTransportMem
    'Public frmRefIngDebitMemoDato As frmIngDebitMemoDato
    'Public frmRefSelVoucherSummaryOperaciones As frmSelVoucherSumaryOperaciones
    'Public frmRefServOperadores As frmRepServOperadores
    'Public frmRefServHotelesRestaurantes As frmRepServHotelesRestaurantes
    'Public frmRefIngIngresoFinanzasDato As frmIngIngresoFinanzasDato
    'Public frmRefIngTicketDato As frmIngTicketDato
    'Public frmRefRepFilePorFechaUbigeo As frmRepFilePorFechaUbigeo
    'Public frmRefDocumentosDato As frmDocumentosDato
    'Public frmRefRepGastosTransfer As frmRepGastosTransferencias
    'Public frmRefMantCorreosBibliaProveedorDatoMem As frmMantCorreosBibliaProveedorDatoMem
    'Public frmRefRepCuentaCorrientePorCliente As frmRepCuentaCorrientePorCliente
    ''Public frmRefRepServOperadores As frmRepServOperadores
    'Public frmRefRepPreliquidaciones As frmRepPreliquidaciones
    'Public frmRefIngNotaVentaCounterDato As frmIngNotaVentaCounterDato
    'Public frmRefIngNotaVentaCounterDatoDetalleMem As frmIngNotaVentaCounterDatoDetalleMem
    'Public frmRefIngNotaVentaCounterDatoItemMem As frmIngNotaVentaCounterDatoItemMem
    'Public frmRefRepDebitMemoPorCobrar As frmRepDebitMemoPorCobrar
    'Public frmRefRepPeriodoPromedioCobro As frmRepPeriodoPromedioCobro

    'Public frmRefMantClientesDato As frmMantClientesDato
    'Public frmRefRepComparaVentasCliente As frmRepComparaVentasCliente
    Public frmRefDocumentosProveedorDato As frmDocumentosProveedorDato
    'Public frmRefDocumentosProveedorDatoFF As frmDocumentosProveedorDato_FondoFijo
    'Public frmRefRepControlDeCalidad As frmRepControlDeCalidad
    'Public frmRefOrdenCompraDato As frmOrdenCompraDato
    'Public frmRefRepPagos_Reservas As frmRepPagos_Reservas

    'Public frmRefControlCalidadDato As frmControlCalidadDato

    Public frmRefDocumentosProveedorDatoMem As frmDocumentosProveedorDatoMem
    'Public frmRefDocumentosProveedorDato_Multiple As frmDocumentosProveedorDato_Multiple

    Public frmRefPedidosDato As frmIngPedidoDato

    'Public frmRefRepRecepcionDocumentos As frmRepRecepcionDocumentos

    'Public frmRefRepOrdenesPago_Reservas As frmRepOrdenesPago_Reservas

    Public strPar As String = ""
    Public strPar2 As String = ""
    Public strModoEdicion As Char = ""
    Public strCaller As String
    Public strTipoBusqueda As String
    Public strCoTipoVenta As String = ""
    Public intIDCab As Integer
    Public strIDCiudad As String = ""
    Public strIDPais As String = ""
    Private Sub txtRazonSocial_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtRazonSocial.TextChanged, _
        Me.Load, nudCategoria.ValueChanged, cboCategSeTours.SelectionChangeCommitted, cboCiudad.SelectionChangeCommitted, txtNumIdentidad.TextChanged ', cboPais.SelectionChangeCommitted
        Try

            Select Case strTipoBusqueda
                Case "Proveedor"
                    'If frmRefIngNotaVentaCounterDatoItemMem Is Nothing Then
                    '    BuscarProveedor()
                    'ElseIf Not frmRefControlCalidadDato Is Nothing Then
                    '    BuscarProveedor()
                    'Else
                    '    BuscarProveedor_Nota_Venta_Counter()
                    'End If

                    'If frmRefIngNotaVentaCounterDatoItemMem Is Nothing Then
                    '    BuscarProveedor()
                    'End If

                    'If Not frmRefDocumentosProveedorDato_Multiple Is Nothing Then
                    '    BuscarProveedor()
                    'End If

                    'If Not frmRefRepRecepcionDocumentos Is Nothing Then
                    BuscarProveedor()
                    'End If

                    'If frmRefMantServicios IsNot Nothing Then
                    dgv.Columns("DescCiudad").Visible = True
                    'End If
                Case "Cliente"
                    dgv.Columns("DescPais").Visible = True
                    BuscarCliente()
                Case "Pax"
                    BuscarPax()
                Case "Guia"
                    BuscarProveedor_Guias()
                Case "Producto"
                    BuscarProducto()
            End Select

        Catch ex As Exception
            MessageBox.Show(ex.Message, "Error en " & Err.Source, MessageBoxButtons.OK, MessageBoxIcon.Error)

        End Try
    End Sub

    Private Sub BuscarProveedor()
        Try
            Dim objBL As New clsProveedorBN

            Dim bytCategoria As Byte = nudCategoria.Value
            Dim strCategSeTours As String = If(cboCategSeTours.Text = "", "", cboCategSeTours.SelectedValue.ToString)

                    dgv.DataSource = objBL.ConsultarListAyuda(strPar, txtRazonSocial.Text, strPar2, strCategSeTours, bytCategoria)
               

            pDgvAnchoColumnas(dgv)

         
        Catch ex As Exception
            Throw
        End Try
    End Sub

    Private Sub BuscarProducto()
        Try
            Dim objBL As New clsProductosBN

           
            If Not frmRefDocumentosProveedorDatoMem Is Nothing Then
                dgv.DataSource = objBL.ConsultarListAyuda(txtRazonSocial.Text.Trim, 0)
               
            End If

          

            pDgvAnchoColumnas(dgv)


            For i As Integer = 0 To dgv.Columns.Count - 1
                If dgv.Columns(i).Name <> "Descripcion" And dgv.Columns(i).Name <> "DescTipo" Then
                    dgv.Columns(i).Visible = False
                End If
            Next

            If strTipoBusqueda = "Producto" Then
                dgv.Columns("Descripcion").Visible = True
                dgv.Columns("DescTipo").Visible = True
                dgv.Columns("Descripcion").Width = 200
                dgv.Columns("DescTipo").Width = 200
            End If

       
        Catch ex As Exception
            Throw
        End Try
    End Sub

    Private Sub BuscarProveedor_Nota_Venta_Counter()
       
    End Sub

    Private Sub BuscarProveedor_Guias()
      
    End Sub

    Private Sub BuscarCliente()
       
    End Sub

    Private Sub BuscarPax()
       
    End Sub

    Private Sub dgv_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgv.CellContentClick

    End Sub

    Private Sub dgv_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles dgv.DoubleClick
        If dgv.RowCount = 0 Then Exit Sub
        Try
            Select Case strCaller
                Case "frmIngCotizacionDato"
                    If strTipoBusqueda = "Cliente" Then
                        With frmRefIngCotizacionDato
                            .lblCliente.Text = dgv.CurrentRow.Cells("Descripcion").Value.ToString
                            .strIDCliente = dgv.CurrentRow.Cells("Codigo").Value.ToString
                            '.cboPais.SelectedValue = dgv.CurrentRow.Cells("IDPais").Value.ToString
                            '.cboPais_SelectionChangeCommitted(Nothing, Nothing)
                            '.cboCiudad.SelectedValue = dgv.CurrentRow.Cells("IDCiudad").Value.ToString
                            .strIDPais = dgv.CurrentRow.Cells("IDPais").Value.ToString
                            .blnClienteTop = dgv.CurrentRow.Cells("Margen").Value
                            .bytPosClienteTop = dgv.CurrentRow.Cells("OperadorInt").Value
                        End With
                        'ElseIf strTipoBusqueda = "Proveedor" Then
                        '    With frmRefIngCotizacionDato.dgvCat
                        '        .CurrentCell.Value = dgv.CurrentRow.Cells("Descripcion").Value.ToString
                        '        .CurrentRow.Cells(.CurrentCell.ColumnIndex - 2).Value = dgv.CurrentRow.Cells("Codigo").Value.ToString
                        '    End With
                    End If
                Case "frmIngPaxTransportMem"
                    frmRefIngPaxTransportMem.dgvPax.Rows.Add()
                    Dim intCantFilas As Int16 = frmRefIngPaxTransportMem.dgvPax.Rows.Count - 1

                    frmRefIngPaxTransportMem.dgvPax("IDPaxTransp", intCantFilas).Value = "M" & (intCantFilas + 1).ToString
                    frmRefIngPaxTransportMem.dgvPax("Sel", intCantFilas).Value = True
                    frmRefIngPaxTransportMem.dgvPax("IDProveedorGuia", intCantFilas).Value = dgv.CurrentRow.Cells("Codigo").Value.ToString
                    frmRefIngPaxTransportMem.dgvPax("NombreGuia", intCantFilas).Value = dgv.CurrentRow.Cells("Descripcion").Value.ToString
                    frmRefIngPaxTransportMem.dgvPax("NoShow", intCantFilas).Value = False
                    frmRefIngPaxTransportMem.dgvPax("Reembolso", intCantFilas).Value = "0.00"
                Case "frmCopiarServicios"
                    frmRefCopiaServicios.lblProveedor.Text = dgv.CurrentRow.Cells("Descripcion").Value.ToString
                    frmRefCopiaServicios.strIDProveedor = dgv.CurrentRow.Cells("Codigo").Value.ToString
                Case "frmMantCotizacionVuelosVentas"
                    frmRefMantCotizVuelosVtas.strIDProveedor = dgv.CurrentRow.Cells("Codigo").Value.ToString
                    frmRefMantCotizVuelosVtas.lblProveedor.Text = dgv.CurrentRow.Cells("Descripcion").Value.ToString
                Case "frmDocumentosProveedorDato"
                    frmRefDocumentosProveedorDato.lblProveedor.Text = dgv.CurrentRow.Cells("Descripcion").Value.ToString
                    frmRefDocumentosProveedorDato.strCoProveedorAdminist = dgv.CurrentRow.Cells("Codigo").Value.ToString
                Case "frmDocumentosProveedorDatoMem"
                    If strTipoBusqueda = "Producto" Then
                        frmRefDocumentosProveedorDatoMem.txtProducto.Text = dgv.CurrentRow.Cells("Descripcion").Value.ToString
                        frmRefDocumentosProveedorDatoMem.strCoProducto = dgv.CurrentRow.Cells("Codigo").Value.ToString
                    End If

                Case "frmIngPedidoDato"
                    With frmRefPedidosDato
                        .lblCliente.Text = dgv.CurrentRow.Cells("Descripcion").Value.ToString
                        .strCoCliente = dgv.CurrentRow.Cells("Codigo").Value.ToString
                        .strCoPais = dgv.CurrentRow.Cells("IDPais").Value.ToString
                        .blnClienteTop = dgv.CurrentRow.Cells("Margen").Value
                        .bytPosClienteTop = dgv.CurrentRow.Cells("OperadorInt").Value
                    End With
            End Select
            Me.Close()
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Error en " & Err.Source, MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub frmMantBancosDato_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyDown
        Select Case e.KeyCode
            Case Keys.Escape
                Me.Close()
        End Select
    End Sub

    Private Sub dgv_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles dgv.KeyDown
        Select Case e.KeyCode
            Case Keys.Enter
                dgv_DoubleClick(Nothing, Nothing)
        End Select

    End Sub

    Private Sub frmAyudas_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        dgv.DefaultCellStyle.Font = New Font(dgv.Font, FontStyle.Regular)
        grbCiudad.Visible = False
        lblTipo.Visible = False
        cboTipo.Visible = False
      

    

        Select Case strTipoBusqueda
            Case "Proveedor"
                Me.Text = "Seleccionar Proveedor"
            Case "Cliente"
                Me.Text = "Seleccionar Cliente"
        End Select

        txtRazonSocial.Focus()
    End Sub

    Private Sub txtRazonSocial_KeyDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtRazonSocial.KeyDown
        Select Case e.KeyCode
            Case Keys.Enter
                dgv_DoubleClick(Nothing, Nothing)
        End Select
    End Sub

    Private Sub CargarCombos()
        Try
            
            Dim objBLUbig As New clsTablasApoyoBN.clsUbigeoBN
            pCargaCombosBox(cboPais, objBLUbig.ConsultarCbo("PAIS", , True), True)

            If strIDPais <> "" Then
                cboPais.SelectedValue = strIDPais
            Else
                cboPais.SelectedValue = gstrPeru
            End If

            cboPais_SelectionChangeCommitted(Nothing, Nothing)

            'If Me.strTipoBusqueda = "Proveedor" And Not Me.frmRefIngNotaVentaCounterDatoItemMem Is Nothing Then
            '    Dim objBLTPrv As New clsTablasApoyoBN.clsTipoProveedorBN
            '    pCargaCombosBox(cboTipo, objBLTPrv.ConsultarCbo(True), True)

            'End If

        Catch ex As Exception
            Throw
        End Try
    End Sub

    Private Sub cboPais_SelectionChangeCommitted(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboPais.SelectionChangeCommitted
        If cboPais.SelectedValue Is Nothing Then Exit Sub
        Try
            Dim objBLUbig As New clsTablasApoyoBN.clsUbigeoBN
            pCargaCombosBox(cboCiudad, objBLUbig.ConsultarCboparaProvee(, cboPais.SelectedValue.ToString, True), True)

            If strIDCiudad <> "" Then
                cboCiudad.SelectedValue = strIDCiudad
                BuscarProveedor_Guias()
            End If

        Catch ex As Exception
            MessageBox.Show(ex.Message, ex.Source, MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub cboTipo_SelectionChangeCommitted(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboTipo.SelectionChangeCommitted
        Try
            'If Not frmRefIngNotaVentaCounterDatoItemMem Is Nothing Then
            '    BuscarProveedor_Nota_Venta_Counter()
            'End If
            'If Not frmRefControlCalidadDato Is Nothing Then
            '    strPar = cboTipo.SelectedValue
            '    txtRazonSocial_TextChanged(Nothing, Nothing)
            'End If
            'If Not frmRefDocumentosProveedorDatoFF Is Nothing Then
            '    strPar = cboTipo.SelectedValue
            '    txtRazonSocial_TextChanged(Nothing, Nothing)
            'End If
        Catch ex As Exception
            MessageBox.Show(ex.Message, ex.Source, MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub
End Class