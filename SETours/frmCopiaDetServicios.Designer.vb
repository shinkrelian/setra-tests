﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmCopiaDetServicios
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Me.btnSalir = New System.Windows.Forms.Button
        Me.btnAceptar = New System.Windows.Forms.Button
        Me.GroupBox1 = New System.Windows.Forms.GroupBox
        Me.txtPorcentDcto = New System.Windows.Forms.TextBox
        Me.Label14 = New System.Windows.Forms.Label
        Me.Label1 = New System.Windows.Forms.Label
        Me.txtDetaTipo = New System.Windows.Forms.TextBox
        Me.txtNuevoTipo = New System.Windows.Forms.TextBox
        Me.optTipo = New System.Windows.Forms.RadioButton
        Me.optAnio = New System.Windows.Forms.RadioButton
        Me.dgv = New System.Windows.Forms.DataGridView
        Me.Anio = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Tipo = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DescripcionDet = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Monto_sgl = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Monto_dbl = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Monto_tri = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.IDServicio_Det = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.txtNuevoAnio = New System.Windows.Forms.TextBox
        Me.Label6 = New System.Windows.Forms.Label
        Me.txtTipo = New System.Windows.Forms.TextBox
        Me.Label4 = New System.Windows.Forms.Label
        Me.txtAnio = New System.Windows.Forms.TextBox
        Me.Label5 = New System.Windows.Forms.Label
        Me.Label3 = New System.Windows.Forms.Label
        Me.ErrPrv = New System.Windows.Forms.ErrorProvider(Me.components)
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.GroupBox1.SuspendLayout()
        CType(Me.dgv, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ErrPrv, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'btnSalir
        '
        Me.btnSalir.Image = Global.SETours.My.Resources.Resources.door_out
        Me.btnSalir.Location = New System.Drawing.Point(5, 33)
        Me.btnSalir.Name = "btnSalir"
        Me.btnSalir.Size = New System.Drawing.Size(27, 27)
        Me.btnSalir.TabIndex = 165
        Me.ToolTip1.SetToolTip(Me.btnSalir, "Salir (ESC)")
        Me.btnSalir.UseVisualStyleBackColor = True
        '
        'btnAceptar
        '
        Me.btnAceptar.Image = Global.SETours.My.Resources.Resources.disk
        Me.btnAceptar.Location = New System.Drawing.Point(5, 4)
        Me.btnAceptar.Name = "btnAceptar"
        Me.btnAceptar.Size = New System.Drawing.Size(27, 27)
        Me.btnAceptar.TabIndex = 164
        Me.ToolTip1.SetToolTip(Me.btnAceptar, "Aceptar (F6)")
        Me.btnAceptar.UseVisualStyleBackColor = True
        '
        'GroupBox1
        '
        Me.GroupBox1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GroupBox1.BackColor = System.Drawing.Color.White
        Me.GroupBox1.Controls.Add(Me.txtPorcentDcto)
        Me.GroupBox1.Controls.Add(Me.Label14)
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.Controls.Add(Me.txtDetaTipo)
        Me.GroupBox1.Controls.Add(Me.txtNuevoTipo)
        Me.GroupBox1.Controls.Add(Me.optTipo)
        Me.GroupBox1.Controls.Add(Me.optAnio)
        Me.GroupBox1.Controls.Add(Me.dgv)
        Me.GroupBox1.Controls.Add(Me.txtNuevoAnio)
        Me.GroupBox1.Controls.Add(Me.Label6)
        Me.GroupBox1.Controls.Add(Me.txtTipo)
        Me.GroupBox1.Controls.Add(Me.Label4)
        Me.GroupBox1.Controls.Add(Me.txtAnio)
        Me.GroupBox1.Controls.Add(Me.Label5)
        Me.GroupBox1.Controls.Add(Me.Label3)
        Me.GroupBox1.Location = New System.Drawing.Point(38, 0)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(751, 387)
        Me.GroupBox1.TabIndex = 166
        Me.GroupBox1.TabStop = False
        '
        'txtPorcentDcto
        '
        Me.txtPorcentDcto.BackColor = System.Drawing.SystemColors.Window
        Me.txtPorcentDcto.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtPorcentDcto.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtPorcentDcto.Location = New System.Drawing.Point(586, 26)
        Me.txtPorcentDcto.MaxLength = 6
        Me.txtPorcentDcto.Name = "txtPorcentDcto"
        Me.txtPorcentDcto.Size = New System.Drawing.Size(94, 21)
        Me.txtPorcentDcto.TabIndex = 4
        Me.txtPorcentDcto.Tag = "Borrar"
        Me.txtPorcentDcto.Text = "0.00"
        Me.txtPorcentDcto.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label14.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label14.Location = New System.Drawing.Point(583, 10)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(130, 13)
        Me.Label14.TabIndex = 180
        Me.Label14.Text = "Porcentaje Dscto (%)"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label1.Location = New System.Drawing.Point(221, 26)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(115, 13)
        Me.Label1.TabIndex = 178
        Me.Label1.Text = "Detalle de Variante"
        '
        'txtDetaTipo
        '
        Me.txtDetaTipo.BackColor = System.Drawing.SystemColors.Window
        Me.txtDetaTipo.Enabled = False
        Me.txtDetaTipo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtDetaTipo.Location = New System.Drawing.Point(224, 42)
        Me.txtDetaTipo.MaxLength = 500
        Me.txtDetaTipo.Multiline = True
        Me.txtDetaTipo.Name = "txtDetaTipo"
        Me.txtDetaTipo.Size = New System.Drawing.Size(334, 38)
        Me.txtDetaTipo.TabIndex = 3
        Me.txtDetaTipo.Tag = "Borrar"
        '
        'txtNuevoTipo
        '
        Me.txtNuevoTipo.BackColor = System.Drawing.SystemColors.Window
        Me.txtNuevoTipo.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtNuevoTipo.Enabled = False
        Me.txtNuevoTipo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtNuevoTipo.Location = New System.Drawing.Point(128, 42)
        Me.txtNuevoTipo.MaxLength = 7
        Me.txtNuevoTipo.Name = "txtNuevoTipo"
        Me.txtNuevoTipo.Size = New System.Drawing.Size(70, 21)
        Me.txtNuevoTipo.TabIndex = 2
        Me.txtNuevoTipo.Tag = "Borrar"
        '
        'optTipo
        '
        Me.optTipo.AutoSize = True
        Me.optTipo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.optTipo.Location = New System.Drawing.Point(9, 43)
        Me.optTipo.Name = "optTipo"
        Me.optTipo.Size = New System.Drawing.Size(111, 17)
        Me.optTipo.TabIndex = 175
        Me.optTipo.TabStop = True
        Me.optTipo.Text = "Nueva Variante"
        Me.optTipo.UseVisualStyleBackColor = True
        '
        'optAnio
        '
        Me.optAnio.AutoSize = True
        Me.optAnio.Checked = True
        Me.optAnio.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.optAnio.Location = New System.Drawing.Point(9, 14)
        Me.optAnio.Name = "optAnio"
        Me.optAnio.Size = New System.Drawing.Size(85, 17)
        Me.optAnio.TabIndex = 174
        Me.optAnio.TabStop = True
        Me.optAnio.Text = "Nuevo Año"
        Me.optAnio.UseVisualStyleBackColor = True
        '
        'dgv
        '
        Me.dgv.AllowUserToAddRows = False
        Me.dgv.AllowUserToDeleteRows = False
        Me.dgv.AllowUserToOrderColumns = True
        Me.dgv.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.ControlDarkDark
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.Color.White
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgv.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.dgv.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgv.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Anio, Me.Tipo, Me.DescripcionDet, Me.Monto_sgl, Me.Monto_dbl, Me.Monto_tri, Me.IDServicio_Det})
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgv.DefaultCellStyle = DataGridViewCellStyle2
        Me.dgv.Location = New System.Drawing.Point(9, 171)
        Me.dgv.Name = "dgv"
        Me.dgv.ReadOnly = True
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.ControlDarkDark
        DataGridViewCellStyle3.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle3.ForeColor = System.Drawing.Color.White
        DataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgv.RowHeadersDefaultCellStyle = DataGridViewCellStyle3
        Me.dgv.Size = New System.Drawing.Size(736, 209)
        Me.dgv.TabIndex = 173
        '
        'Anio
        '
        Me.Anio.DataPropertyName = "Anio"
        Me.Anio.HeaderText = "Año"
        Me.Anio.Name = "Anio"
        Me.Anio.ReadOnly = True
        '
        'Tipo
        '
        Me.Tipo.DataPropertyName = "Tipo"
        Me.Tipo.HeaderText = "Tipo"
        Me.Tipo.Name = "Tipo"
        Me.Tipo.ReadOnly = True
        '
        'DescripcionDet
        '
        Me.DescripcionDet.DataPropertyName = "Descripcion"
        Me.DescripcionDet.HeaderText = "Descripción"
        Me.DescripcionDet.Name = "DescripcionDet"
        Me.DescripcionDet.ReadOnly = True
        '
        'Monto_sgl
        '
        Me.Monto_sgl.DataPropertyName = "Monto_sgl"
        Me.Monto_sgl.HeaderText = "Monto Simple"
        Me.Monto_sgl.Name = "Monto_sgl"
        Me.Monto_sgl.ReadOnly = True
        '
        'Monto_dbl
        '
        Me.Monto_dbl.DataPropertyName = "Monto_dbl"
        Me.Monto_dbl.HeaderText = "Monto Doble"
        Me.Monto_dbl.Name = "Monto_dbl"
        Me.Monto_dbl.ReadOnly = True
        '
        'Monto_tri
        '
        Me.Monto_tri.DataPropertyName = "Monto_tri"
        Me.Monto_tri.HeaderText = "Monto Triple"
        Me.Monto_tri.Name = "Monto_tri"
        Me.Monto_tri.ReadOnly = True
        '
        'IDServicio_Det
        '
        Me.IDServicio_Det.DataPropertyName = "IDServicio_Det"
        Me.IDServicio_Det.HeaderText = "Id. Det. Servicio"
        Me.IDServicio_Det.Name = "IDServicio_Det"
        Me.IDServicio_Det.ReadOnly = True
        Me.IDServicio_Det.Visible = False
        '
        'txtNuevoAnio
        '
        Me.txtNuevoAnio.BackColor = System.Drawing.Color.White
        Me.txtNuevoAnio.Location = New System.Drawing.Point(128, 13)
        Me.txtNuevoAnio.MaxLength = 4
        Me.txtNuevoAnio.Name = "txtNuevoAnio"
        Me.txtNuevoAnio.Size = New System.Drawing.Size(62, 20)
        Me.txtNuevoAnio.TabIndex = 1
        Me.txtNuevoAnio.Tag = "Borrar"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Tahoma", 8.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label6.Location = New System.Drawing.Point(4, 86)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(243, 13)
        Me.Label6.TabIndex = 170
        Me.Label6.Text = "Filtros para el Detalle del Servicio a copiar"
        '
        'txtTipo
        '
        Me.txtTipo.BackColor = System.Drawing.SystemColors.Window
        Me.txtTipo.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtTipo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtTipo.Location = New System.Drawing.Point(105, 117)
        Me.txtTipo.MaxLength = 7
        Me.txtTipo.Name = "txtTipo"
        Me.txtTipo.Size = New System.Drawing.Size(70, 21)
        Me.txtTipo.TabIndex = 6
        Me.txtTipo.Tag = "Borrar"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.Label4.Location = New System.Drawing.Point(102, 103)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(55, 13)
        Me.Label4.TabIndex = 169
        Me.Label4.Text = "Variante"
        '
        'txtAnio
        '
        Me.txtAnio.BackColor = System.Drawing.Color.White
        Me.txtAnio.Location = New System.Drawing.Point(8, 117)
        Me.txtAnio.MaxLength = 4
        Me.txtAnio.Name = "txtAnio"
        Me.txtAnio.Size = New System.Drawing.Size(62, 20)
        Me.txtAnio.TabIndex = 5
        Me.txtAnio.Tag = "Borrar"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.Label5.Location = New System.Drawing.Point(5, 103)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(29, 13)
        Me.Label5.TabIndex = 168
        Me.Label5.Text = "Año"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Tahoma", 8.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label3.Location = New System.Drawing.Point(6, 155)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(115, 13)
        Me.Label3.TabIndex = 165
        Me.Label3.Text = "Registros Copiados"
        '
        'ErrPrv
        '
        Me.ErrPrv.ContainerControl = Me
        '
        'frmCopiaDetServicios
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(789, 387)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.btnSalir)
        Me.Controls.Add(Me.btnAceptar)
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmCopiaDetServicios"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "frmCopiaDetServicios"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        CType(Me.dgv, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ErrPrv, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents btnSalir As System.Windows.Forms.Button
    Friend WithEvents btnAceptar As System.Windows.Forms.Button
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents txtNuevoAnio As System.Windows.Forms.TextBox
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents txtTipo As System.Windows.Forms.TextBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents txtAnio As System.Windows.Forms.TextBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents dgv As System.Windows.Forms.DataGridView
    Friend WithEvents Anio As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Tipo As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DescripcionDet As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Monto_sgl As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Monto_dbl As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Monto_tri As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents IDServicio_Det As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ErrPrv As System.Windows.Forms.ErrorProvider
    Friend WithEvents ToolTip1 As System.Windows.Forms.ToolTip
    Friend WithEvents optTipo As System.Windows.Forms.RadioButton
    Friend WithEvents optAnio As System.Windows.Forms.RadioButton
    Friend WithEvents txtNuevoTipo As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents txtDetaTipo As System.Windows.Forms.TextBox
    Friend WithEvents txtPorcentDcto As System.Windows.Forms.TextBox
    Friend WithEvents Label14 As System.Windows.Forms.Label
End Class
