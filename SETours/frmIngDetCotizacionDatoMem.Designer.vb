﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmIngDetCotizacionDatoMem
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmIngDetCotizacionDatoMem))
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle5 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle6 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle7 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle9 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle10 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle8 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.ErrPrv = New System.Windows.Forms.ErrorProvider(Me.components)
        Me.btnQuiebres = New System.Windows.Forms.Button()
        Me.btnVerAcomodoEspecial = New System.Windows.Forms.Button()
        Me.Tab = New System.Windows.Forms.TabControl()
        Me.TabPage1 = New System.Windows.Forms.TabPage()
        Me.grbDatosGenerales = New System.Windows.Forms.GroupBox()
        Me.chkServNoCobrado = New System.Windows.Forms.CheckBox()
        Me.chkAcomVehi = New System.Windows.Forms.CheckBox()
        Me.cboPaisDest = New System.Windows.Forms.ComboBox()
        Me.lblEtiqPaisDest = New System.Windows.Forms.Label()
        Me.cboPaisOrig = New System.Windows.Forms.ComboBox()
        Me.lblEtiqPaisOrig = New System.Windows.Forms.Label()
        Me.chkServicioNoShow = New System.Windows.Forms.CheckBox()
        Me.lnkDocumentoSustento = New System.Windows.Forms.LinkLabel()
        Me.chkAcomodoEspecial = New System.Windows.Forms.CheckBox()
        Me.chkEditarTotalHab = New System.Windows.Forms.CheckBox()
        Me.txtTotalHab = New System.Windows.Forms.TextBox()
        Me.lblTotalR = New System.Windows.Forms.Label()
        Me.chkVerVoucher = New System.Windows.Forms.CheckBox()
        Me.lblTotalMonEtiq = New System.Windows.Forms.Label()
        Me.lblIgvTotal = New System.Windows.Forms.Label()
        Me.lblIgvTotalEtiq = New System.Windows.Forms.Label()
        Me.lblNetoTotal = New System.Windows.Forms.Label()
        Me.lblNetoTotalEtiq = New System.Windows.Forms.Label()
        Me.lblTotalHab = New System.Windows.Forms.Label()
        Me.txtIgvHab = New System.Windows.Forms.TextBox()
        Me.txtNetoHab = New System.Windows.Forms.TextBox()
        Me.lblTotalHabEtiq = New System.Windows.Forms.Label()
        Me.lblIGVHabEtiq = New System.Windows.Forms.Label()
        Me.lblNetoHabEtiq = New System.Windows.Forms.Label()
        Me.Label18 = New System.Windows.Forms.Label()
        Me.lblTipoServicio = New System.Windows.Forms.Label()
        Me.lblDescVariante = New System.Windows.Forms.Label()
        Me.lblVariante = New System.Windows.Forms.Label()
        Me.Label17 = New System.Windows.Forms.Label()
        Me.txtCantidadaPagar = New System.Windows.Forms.TextBox()
        Me.lblCantPagarEtiq = New System.Windows.Forms.Label()
        Me.grbBus_Guia = New System.Windows.Forms.GroupBox()
        Me.CboBusVehiculo = New System.Windows.Forms.ComboBox()
        Me.cboGuia = New System.Windows.Forms.ComboBox()
        Me.lblEtiqBus = New System.Windows.Forms.Label()
        Me.lblEtiqGuia = New System.Windows.Forms.Label()
        Me.lblRedondeo = New System.Windows.Forms.Label()
        Me.lblRedondeoTotal = New System.Windows.Forms.Label()
        Me.chkIncGuia = New System.Windows.Forms.CheckBox()
        Me.grbChecksVoucBiblia = New System.Windows.Forms.GroupBox()
        Me.chkVerBiblia = New System.Windows.Forms.CheckBox()
        Me.dtpDiaHora = New System.Windows.Forms.DateTimePicker()
        Me.txtCantidad = New System.Windows.Forms.TextBox()
        Me.lblCantidadEtiq = New System.Windows.Forms.Label()
        Me.lblNoches = New System.Windows.Forms.Label()
        Me.lblNochesEtiq = New System.Windows.Forms.Label()
        Me.lblFechaOutEtiq = New System.Windows.Forms.Label()
        Me.dtpFechaOut = New System.Windows.Forms.DateTimePicker()
        Me.txtSTTotalOrig = New System.Windows.Forms.TextBox()
        Me.txtSSTotalOrig = New System.Windows.Forms.TextBox()
        Me.lblTotalOrig = New System.Windows.Forms.Label()
        Me.cboDestino = New System.Windows.Forms.ComboBox()
        Me.lblDestinoEtiq = New System.Windows.Forms.Label()
        Me.cboOrigen = New System.Windows.Forms.ComboBox()
        Me.lblOrigenEtiq = New System.Windows.Forms.Label()
        Me.lblTipoTransporteEtiq = New System.Windows.Forms.Label()
        Me.cboTipoTransporte = New System.Windows.Forms.ComboBox()
        Me.chkTransfer = New System.Windows.Forms.CheckBox()
        Me.cboIDUbigeoDes = New System.Windows.Forms.ComboBox()
        Me.lblIDUbigeoDesEtiq = New System.Windows.Forms.Label()
        Me.cboIDUbigeoOri = New System.Windows.Forms.ComboBox()
        Me.lblIDUbigeoOriEtiq = New System.Windows.Forms.Label()
        Me.chkLonche = New System.Windows.Forms.CheckBox()
        Me.chkAlmuerzo = New System.Windows.Forms.CheckBox()
        Me.chkDesayuno = New System.Windows.Forms.CheckBox()
        Me.chkSelPax = New System.Windows.Forms.CheckBox()
        Me.grbPax = New System.Windows.Forms.GroupBox()
        Me.chkSelTodosPax = New System.Windows.Forms.CheckBox()
        Me.cboTipoLib = New System.Windows.Forms.ComboBox()
        Me.lblTipoLibEtiq = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.txtNroLiberados = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.txtNroPax = New System.Windows.Forms.TextBox()
        Me.lvwPax = New System.Windows.Forms.ListView()
        Me.ImageList1 = New System.Windows.Forms.ImageList(Me.components)
        Me.lblTotal = New System.Windows.Forms.Label()
        Me.lblMargenLiberado = New System.Windows.Forms.Label()
        Me.lblCostoLiberado = New System.Windows.Forms.Label()
        Me.lblMargen = New System.Windows.Forms.Label()
        Me.txtCostoRealImpto = New System.Windows.Forms.TextBox()
        Me.btnRutaArchivo = New System.Windows.Forms.Button()
        Me.lblTotImpto = New System.Windows.Forms.Label()
        Me.lblTotImptoEtiq = New System.Windows.Forms.Label()
        Me.txtServicio = New System.Windows.Forms.TextBox()
        Me.lblServicioEtiq = New System.Windows.Forms.Label()
        Me.txtRutaDocSustento = New System.Windows.Forms.TextBox()
        Me.lblRutaDocSustentoEtiq = New System.Windows.Forms.Label()
        Me.lblImpuestosEtiq3 = New System.Windows.Forms.Label()
        Me.lblImpuestosEtiq2 = New System.Windows.Forms.Label()
        Me.lblImpuestosEtiq1 = New System.Windows.Forms.Label()
        Me.lblSTMargenImpto = New System.Windows.Forms.Label()
        Me.lblSTCRImpto = New System.Windows.Forms.Label()
        Me.lblSSMargenLiberadoImpto = New System.Windows.Forms.Label()
        Me.lblSSCLImpto = New System.Windows.Forms.Label()
        Me.lblSSMargenImpto = New System.Windows.Forms.Label()
        Me.lblSSCRImpto = New System.Windows.Forms.Label()
        Me.lblMargenLiberadoImpto = New System.Windows.Forms.Label()
        Me.lblCostoLiberadoImpto = New System.Windows.Forms.Label()
        Me.lblMargenImpto = New System.Windows.Forms.Label()
        Me.Label19 = New System.Windows.Forms.Label()
        Me.dtpDia = New System.Windows.Forms.DateTimePicker()
        Me.txtMotivoEspecial = New System.Windows.Forms.TextBox()
        Me.lblMotivoEspecialEtiq = New System.Windows.Forms.Label()
        Me.chkEspecial = New System.Windows.Forms.CheckBox()
        Me.cboIDIdioma = New System.Windows.Forms.ComboBox()
        Me.lblIdiomaEtiq = New System.Windows.Forms.Label()
        Me.lblSTTotalEtiq = New System.Windows.Forms.Label()
        Me.lblSTMargenEtiq = New System.Windows.Forms.Label()
        Me.lblSTCREtiq = New System.Windows.Forms.Label()
        Me.lblSSTotalEtiq = New System.Windows.Forms.Label()
        Me.lblSSMLEtiq = New System.Windows.Forms.Label()
        Me.lblSSCLEtiq = New System.Windows.Forms.Label()
        Me.lblSSMargenEtiq = New System.Windows.Forms.Label()
        Me.lblSSCREtiq = New System.Windows.Forms.Label()
        Me.lblTotalEtiq = New System.Windows.Forms.Label()
        Me.lblMargenLiberadoEtiq = New System.Windows.Forms.Label()
        Me.lblCostoLiberadoEtiq = New System.Windows.Forms.Label()
        Me.lblMargenEtiq = New System.Windows.Forms.Label()
        Me.lblCostoRealEtiq = New System.Windows.Forms.Label()
        Me.txtSSCR = New System.Windows.Forms.TextBox()
        Me.txtSTMargen = New System.Windows.Forms.TextBox()
        Me.txtSSCL = New System.Windows.Forms.TextBox()
        Me.txtSSMargen = New System.Windows.Forms.TextBox()
        Me.txtSTCR = New System.Windows.Forms.TextBox()
        Me.txtSTTotal = New System.Windows.Forms.TextBox()
        Me.txtSSTotal = New System.Windows.Forms.TextBox()
        Me.txtSSMargenLiberado = New System.Windows.Forms.TextBox()
        Me.txtMargenAplicado = New System.Windows.Forms.TextBox()
        Me.txtCostoReal = New System.Windows.Forms.TextBox()
        Me.cboCiudad = New System.Windows.Forms.ComboBox()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.cboPais = New System.Windows.Forms.ComboBox()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.cboTipoProv = New System.Windows.Forms.ComboBox()
        Me.Label22 = New System.Windows.Forms.Label()
        Me.btnProveedor = New System.Windows.Forms.Button()
        Me.lblProveedor = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.lblTitulo = New System.Windows.Forms.Label()
        Me.chkCena = New System.Windows.Forms.CheckBox()
        Me.TabPage2 = New System.Windows.Forms.TabPage()
        Me.grbQuiebres = New System.Windows.Forms.GroupBox()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.lblRedondeoQ = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.lblSTTotalQ = New System.Windows.Forms.Label()
        Me.lblSTMargenQ = New System.Windows.Forms.Label()
        Me.lblSTCRQ = New System.Windows.Forms.Label()
        Me.lblSSTotalQ = New System.Windows.Forms.Label()
        Me.lblSSMLQ = New System.Windows.Forms.Label()
        Me.lblSSCLQ = New System.Windows.Forms.Label()
        Me.lblSSMargenQ = New System.Windows.Forms.Label()
        Me.lblSSCRQ = New System.Windows.Forms.Label()
        Me.lblTotalQ = New System.Windows.Forms.Label()
        Me.lblMargenLiberadoQ = New System.Windows.Forms.Label()
        Me.lblCostoLiberadoQ = New System.Windows.Forms.Label()
        Me.lblMargenQ = New System.Windows.Forms.Label()
        Me.lblTotImptoQ = New System.Windows.Forms.Label()
        Me.Label27 = New System.Windows.Forms.Label()
        Me.Label24 = New System.Windows.Forms.Label()
        Me.Label25 = New System.Windows.Forms.Label()
        Me.Label26 = New System.Windows.Forms.Label()
        Me.lblSTMargenImptoQ = New System.Windows.Forms.Label()
        Me.lblSTCRImptoQ = New System.Windows.Forms.Label()
        Me.lblSSMLImptoQ = New System.Windows.Forms.Label()
        Me.lblSSCLImptoQ = New System.Windows.Forms.Label()
        Me.lblSSMargenImptoQ = New System.Windows.Forms.Label()
        Me.lblSSCRImptoQ = New System.Windows.Forms.Label()
        Me.lblMargenLiberadoImptoQ = New System.Windows.Forms.Label()
        Me.lblCostoLiberadoImptoQ = New System.Windows.Forms.Label()
        Me.lblMargenImptoQ = New System.Windows.Forms.Label()
        Me.lblCostoRealImptoQ = New System.Windows.Forms.Label()
        Me.Label37 = New System.Windows.Forms.Label()
        Me.Label38 = New System.Windows.Forms.Label()
        Me.Label39 = New System.Windows.Forms.Label()
        Me.Label40 = New System.Windows.Forms.Label()
        Me.Label41 = New System.Windows.Forms.Label()
        Me.Label42 = New System.Windows.Forms.Label()
        Me.Label43 = New System.Windows.Forms.Label()
        Me.Label44 = New System.Windows.Forms.Label()
        Me.Label45 = New System.Windows.Forms.Label()
        Me.Label47 = New System.Windows.Forms.Label()
        Me.Label48 = New System.Windows.Forms.Label()
        Me.Label49 = New System.Windows.Forms.Label()
        Me.Label50 = New System.Windows.Forms.Label()
        Me.txtMargenAplicadoQ = New System.Windows.Forms.TextBox()
        Me.txtCostoRealQ = New System.Windows.Forms.TextBox()
        Me.lblTitulo2 = New System.Windows.Forms.Label()
        Me.dgvQui = New System.Windows.Forms.DataGridView()
        Me.IDCab_Q = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.IDDet_Q = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Accion = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Pax = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Liberados = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Tipo_Lib = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.CostoReal = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.CostoRealImpto = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Margen = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.MargenImpto = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.CostoLiberado = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.CostoLiberadoImpto = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.MargenLiberado = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.MargenLiberadoImpto = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.MargenAplicado = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Total = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.RedondeoTotal = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.SSCR = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.SSCRImpto = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.SSMargen = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.SSMargenImpto = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.SSCL = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.SSCLImpto = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.SSML = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.SSMargenLiberadoImpto = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.SSTotal = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.STCR = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.STCRImpto = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.STMargen = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.STMargenImpto = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.STTotal = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.TotImpto = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.TabPage3 = New System.Windows.Forms.TabPage()
        Me.grbMontos = New System.Windows.Forms.GroupBox()
        Me.txtSTCR2 = New System.Windows.Forms.TextBox()
        Me.txtSSCR2 = New System.Windows.Forms.TextBox()
        Me.lblMargenAplicado4 = New System.Windows.Forms.Label()
        Me.lblSTImpEtiq2 = New System.Windows.Forms.Label()
        Me.lblSTCRImpto2 = New System.Windows.Forms.Label()
        Me.lblMargenAplicado3 = New System.Windows.Forms.Label()
        Me.lblSSImpEtiq2 = New System.Windows.Forms.Label()
        Me.lblRedondeoTotal2 = New System.Windows.Forms.Label()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.grbSep4 = New System.Windows.Forms.GroupBox()
        Me.lblSTCR2 = New System.Windows.Forms.Label()
        Me.lblSTMargen2 = New System.Windows.Forms.Label()
        Me.lblSTTotal2 = New System.Windows.Forms.Label()
        Me.lblSSCL2 = New System.Windows.Forms.Label()
        Me.lblSSMargen2 = New System.Windows.Forms.Label()
        Me.lblSSCR2 = New System.Windows.Forms.Label()
        Me.lblSSMargenLiberado2 = New System.Windows.Forms.Label()
        Me.lblSSTotal2 = New System.Windows.Forms.Label()
        Me.lblMargenAplicado2 = New System.Windows.Forms.Label()
        Me.lblCostoRealImpto2 = New System.Windows.Forms.Label()
        Me.lblTotal2 = New System.Windows.Forms.Label()
        Me.lblCostoLiberado2 = New System.Windows.Forms.Label()
        Me.lblMargen2 = New System.Windows.Forms.Label()
        Me.lblCostoReal2 = New System.Windows.Forms.Label()
        Me.lblMargenLiberado2 = New System.Windows.Forms.Label()
        Me.GroupBox3 = New System.Windows.Forms.GroupBox()
        Me.lblTituloCostoTriple = New System.Windows.Forms.Label()
        Me.lblTituloCostoSimple = New System.Windows.Forms.Label()
        Me.lblCostoenDobleEtiq = New System.Windows.Forms.Label()
        Me.lblTitulo3 = New System.Windows.Forms.Label()
        Me.lblTotImpto2 = New System.Windows.Forms.Label()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.lblImpuestosEtiq22 = New System.Windows.Forms.Label()
        Me.lblImpuestosEtiq12 = New System.Windows.Forms.Label()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.lblSTMargenImpto2 = New System.Windows.Forms.Label()
        Me.lblSSMargenLiberadoImpto2 = New System.Windows.Forms.Label()
        Me.lblSSCLImpto2 = New System.Windows.Forms.Label()
        Me.lblSSMargenImpto2 = New System.Windows.Forms.Label()
        Me.lblSSCRImpto2 = New System.Windows.Forms.Label()
        Me.lblMargenLiberadoImpto2 = New System.Windows.Forms.Label()
        Me.lblCostoLiberadoImpto2 = New System.Windows.Forms.Label()
        Me.lblMargenImpto2 = New System.Windows.Forms.Label()
        Me.lblSTTotalEtiq2 = New System.Windows.Forms.Label()
        Me.lblSTMargenEtiq2 = New System.Windows.Forms.Label()
        Me.lblSTCREtiq2 = New System.Windows.Forms.Label()
        Me.lblSSTotalEtiq2 = New System.Windows.Forms.Label()
        Me.lblSSMLEtiq2 = New System.Windows.Forms.Label()
        Me.lblSSCLEtiq2 = New System.Windows.Forms.Label()
        Me.lblSSMargenEtiq2 = New System.Windows.Forms.Label()
        Me.lblSSCREtiq2 = New System.Windows.Forms.Label()
        Me.Label54 = New System.Windows.Forms.Label()
        Me.Label56 = New System.Windows.Forms.Label()
        Me.Label57 = New System.Windows.Forms.Label()
        Me.Label58 = New System.Windows.Forms.Label()
        Me.Label59 = New System.Windows.Forms.Label()
        Me.TabPage4 = New System.Windows.Forms.TabPage()
        Me.grbTextos = New System.Windows.Forms.GroupBox()
        Me.btnEliminarIdiomaTexto = New System.Windows.Forms.Button()
        Me.lblTitulo4 = New System.Windows.Forms.Label()
        Me.dgvIdiom = New System.Windows.Forms.DataGridView()
        Me.English = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.French = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.German = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Spanish = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.NoHotel = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.TxWebHotel = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.TxDireccHotel = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.TxTelfHotel1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.TxTelfHotel2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.FeHoraChkInHotel = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.FeHoraChkOutHotel = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.CoTipoDesaHotel = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.CoCiudadHotel = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.tpgRecordat = New System.Windows.Forms.TabPage()
        Me.grbRecordObserv = New System.Windows.Forms.GroupBox()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.txtObservInterno = New System.Windows.Forms.TextBox()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.txtObservBiblia = New System.Windows.Forms.TextBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.txtObservVoucher = New System.Windows.Forms.TextBox()
        Me.tpgAcomVehi = New System.Windows.Forms.TabPage()
        Me.GroupBox4 = New System.Windows.Forms.GroupBox()
        Me.lblEtiqAcomVehPax = New System.Windows.Forms.Label()
        Me.grbAcomVehicE = New System.Windows.Forms.GroupBox()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.txtGruposPax = New System.Windows.Forms.TextBox()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.txtPaxTotal = New System.Windows.Forms.TextBox()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.tvwAcoVeh = New System.Windows.Forms.TreeView()
        Me.chkSelCopiaAcoVeh = New System.Windows.Forms.CheckBox()
        Me.lvwServAcomVehi = New System.Windows.Forms.ListView()
        Me.grbAcomVehicI = New System.Windows.Forms.GroupBox()
        Me.dgvAcoVeh = New System.Windows.Forms.DataGridView()
        Me.QtVehiculos = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.NuVehiculo = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.NoVehiculo = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.QtCapacidad = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.QtPax = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.FlGuia = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.btnReCalcular = New System.Windows.Forms.Button()
        Me.btnSalir = New System.Windows.Forms.Button()
        Me.btnAceptar = New System.Windows.Forms.Button()
        Me.ofdRuta = New System.Windows.Forms.OpenFileDialog()
        Me.ErrPrvSS = New System.Windows.Forms.ErrorProvider(Me.components)
        CType(Me.ErrPrv, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Tab.SuspendLayout()
        Me.TabPage1.SuspendLayout()
        Me.grbDatosGenerales.SuspendLayout()
        Me.grbBus_Guia.SuspendLayout()
        Me.grbChecksVoucBiblia.SuspendLayout()
        Me.grbPax.SuspendLayout()
        Me.TabPage2.SuspendLayout()
        Me.grbQuiebres.SuspendLayout()
        CType(Me.dgvQui, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabPage3.SuspendLayout()
        Me.grbMontos.SuspendLayout()
        Me.TabPage4.SuspendLayout()
        Me.grbTextos.SuspendLayout()
        CType(Me.dgvIdiom, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tpgRecordat.SuspendLayout()
        Me.grbRecordObserv.SuspendLayout()
        Me.tpgAcomVehi.SuspendLayout()
        Me.GroupBox4.SuspendLayout()
        Me.grbAcomVehicE.SuspendLayout()
        Me.grbAcomVehicI.SuspendLayout()
        CType(Me.dgvAcoVeh, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ErrPrvSS, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'ErrPrv
        '
        Me.ErrPrv.ContainerControl = Me
        '
        'btnQuiebres
        '
        Me.ErrPrv.SetIconAlignment(Me.btnQuiebres, System.Windows.Forms.ErrorIconAlignment.TopRight)
        Me.btnQuiebres.Image = Global.SETours.My.Resources.Resources.chart_bar_edit
        Me.btnQuiebres.Location = New System.Drawing.Point(5, 65)
        Me.btnQuiebres.Name = "btnQuiebres"
        Me.btnQuiebres.Size = New System.Drawing.Size(27, 27)
        Me.btnQuiebres.TabIndex = 256
        Me.btnQuiebres.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.ToolTip1.SetToolTip(Me.btnQuiebres, "Quiebres")
        Me.btnQuiebres.UseVisualStyleBackColor = True
        '
        'btnVerAcomodoEspecial
        '
        Me.ErrPrv.SetIconAlignment(Me.btnVerAcomodoEspecial, System.Windows.Forms.ErrorIconAlignment.TopRight)
        Me.btnVerAcomodoEspecial.Location = New System.Drawing.Point(929, 60)
        Me.btnVerAcomodoEspecial.Name = "btnVerAcomodoEspecial"
        Me.btnVerAcomodoEspecial.Size = New System.Drawing.Size(27, 22)
        Me.btnVerAcomodoEspecial.TabIndex = 257
        Me.btnVerAcomodoEspecial.Text = "..."
        Me.btnVerAcomodoEspecial.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.ToolTip1.SetToolTip(Me.btnVerAcomodoEspecial, "Ver acomodo especial")
        Me.btnVerAcomodoEspecial.UseVisualStyleBackColor = True
        Me.btnVerAcomodoEspecial.Visible = False
        '
        'Tab
        '
        Me.Tab.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Tab.Controls.Add(Me.TabPage1)
        Me.Tab.Controls.Add(Me.TabPage2)
        Me.Tab.Controls.Add(Me.TabPage3)
        Me.Tab.Controls.Add(Me.TabPage4)
        Me.Tab.Controls.Add(Me.tpgRecordat)
        Me.Tab.Controls.Add(Me.tpgAcomVehi)
        Me.Tab.Location = New System.Drawing.Point(38, -3)
        Me.Tab.Name = "Tab"
        Me.Tab.SelectedIndex = 0
        Me.Tab.Size = New System.Drawing.Size(1146, 610)
        Me.Tab.TabIndex = 163
        '
        'TabPage1
        '
        Me.TabPage1.Controls.Add(Me.grbDatosGenerales)
        Me.TabPage1.Location = New System.Drawing.Point(4, 22)
        Me.TabPage1.Name = "TabPage1"
        Me.TabPage1.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage1.Size = New System.Drawing.Size(1138, 584)
        Me.TabPage1.TabIndex = 0
        Me.TabPage1.Text = "Datos Generales"
        Me.TabPage1.UseVisualStyleBackColor = True
        '
        'grbDatosGenerales
        '
        Me.grbDatosGenerales.BackColor = System.Drawing.Color.White
        Me.grbDatosGenerales.Controls.Add(Me.chkServNoCobrado)
        Me.grbDatosGenerales.Controls.Add(Me.chkAcomVehi)
        Me.grbDatosGenerales.Controls.Add(Me.cboPaisDest)
        Me.grbDatosGenerales.Controls.Add(Me.lblEtiqPaisDest)
        Me.grbDatosGenerales.Controls.Add(Me.cboPaisOrig)
        Me.grbDatosGenerales.Controls.Add(Me.lblEtiqPaisOrig)
        Me.grbDatosGenerales.Controls.Add(Me.chkServicioNoShow)
        Me.grbDatosGenerales.Controls.Add(Me.lnkDocumentoSustento)
        Me.grbDatosGenerales.Controls.Add(Me.btnVerAcomodoEspecial)
        Me.grbDatosGenerales.Controls.Add(Me.chkAcomodoEspecial)
        Me.grbDatosGenerales.Controls.Add(Me.chkEditarTotalHab)
        Me.grbDatosGenerales.Controls.Add(Me.txtTotalHab)
        Me.grbDatosGenerales.Controls.Add(Me.lblTotalR)
        Me.grbDatosGenerales.Controls.Add(Me.chkVerVoucher)
        Me.grbDatosGenerales.Controls.Add(Me.lblTotalMonEtiq)
        Me.grbDatosGenerales.Controls.Add(Me.lblIgvTotal)
        Me.grbDatosGenerales.Controls.Add(Me.lblIgvTotalEtiq)
        Me.grbDatosGenerales.Controls.Add(Me.lblNetoTotal)
        Me.grbDatosGenerales.Controls.Add(Me.lblNetoTotalEtiq)
        Me.grbDatosGenerales.Controls.Add(Me.lblTotalHab)
        Me.grbDatosGenerales.Controls.Add(Me.txtIgvHab)
        Me.grbDatosGenerales.Controls.Add(Me.txtNetoHab)
        Me.grbDatosGenerales.Controls.Add(Me.lblTotalHabEtiq)
        Me.grbDatosGenerales.Controls.Add(Me.lblIGVHabEtiq)
        Me.grbDatosGenerales.Controls.Add(Me.lblNetoHabEtiq)
        Me.grbDatosGenerales.Controls.Add(Me.Label18)
        Me.grbDatosGenerales.Controls.Add(Me.lblTipoServicio)
        Me.grbDatosGenerales.Controls.Add(Me.lblDescVariante)
        Me.grbDatosGenerales.Controls.Add(Me.lblVariante)
        Me.grbDatosGenerales.Controls.Add(Me.Label17)
        Me.grbDatosGenerales.Controls.Add(Me.txtCantidadaPagar)
        Me.grbDatosGenerales.Controls.Add(Me.lblCantPagarEtiq)
        Me.grbDatosGenerales.Controls.Add(Me.grbBus_Guia)
        Me.grbDatosGenerales.Controls.Add(Me.lblRedondeo)
        Me.grbDatosGenerales.Controls.Add(Me.lblRedondeoTotal)
        Me.grbDatosGenerales.Controls.Add(Me.chkIncGuia)
        Me.grbDatosGenerales.Controls.Add(Me.grbChecksVoucBiblia)
        Me.grbDatosGenerales.Controls.Add(Me.dtpDiaHora)
        Me.grbDatosGenerales.Controls.Add(Me.txtCantidad)
        Me.grbDatosGenerales.Controls.Add(Me.lblCantidadEtiq)
        Me.grbDatosGenerales.Controls.Add(Me.lblNoches)
        Me.grbDatosGenerales.Controls.Add(Me.lblNochesEtiq)
        Me.grbDatosGenerales.Controls.Add(Me.lblFechaOutEtiq)
        Me.grbDatosGenerales.Controls.Add(Me.dtpFechaOut)
        Me.grbDatosGenerales.Controls.Add(Me.txtSTTotalOrig)
        Me.grbDatosGenerales.Controls.Add(Me.txtSSTotalOrig)
        Me.grbDatosGenerales.Controls.Add(Me.lblTotalOrig)
        Me.grbDatosGenerales.Controls.Add(Me.cboDestino)
        Me.grbDatosGenerales.Controls.Add(Me.lblDestinoEtiq)
        Me.grbDatosGenerales.Controls.Add(Me.cboOrigen)
        Me.grbDatosGenerales.Controls.Add(Me.lblOrigenEtiq)
        Me.grbDatosGenerales.Controls.Add(Me.lblTipoTransporteEtiq)
        Me.grbDatosGenerales.Controls.Add(Me.cboTipoTransporte)
        Me.grbDatosGenerales.Controls.Add(Me.chkTransfer)
        Me.grbDatosGenerales.Controls.Add(Me.cboIDUbigeoDes)
        Me.grbDatosGenerales.Controls.Add(Me.lblIDUbigeoDesEtiq)
        Me.grbDatosGenerales.Controls.Add(Me.cboIDUbigeoOri)
        Me.grbDatosGenerales.Controls.Add(Me.lblIDUbigeoOriEtiq)
        Me.grbDatosGenerales.Controls.Add(Me.chkLonche)
        Me.grbDatosGenerales.Controls.Add(Me.chkAlmuerzo)
        Me.grbDatosGenerales.Controls.Add(Me.chkDesayuno)
        Me.grbDatosGenerales.Controls.Add(Me.chkSelPax)
        Me.grbDatosGenerales.Controls.Add(Me.grbPax)
        Me.grbDatosGenerales.Controls.Add(Me.lblTotal)
        Me.grbDatosGenerales.Controls.Add(Me.lblMargenLiberado)
        Me.grbDatosGenerales.Controls.Add(Me.lblCostoLiberado)
        Me.grbDatosGenerales.Controls.Add(Me.lblMargen)
        Me.grbDatosGenerales.Controls.Add(Me.txtCostoRealImpto)
        Me.grbDatosGenerales.Controls.Add(Me.btnRutaArchivo)
        Me.grbDatosGenerales.Controls.Add(Me.lblTotImpto)
        Me.grbDatosGenerales.Controls.Add(Me.lblTotImptoEtiq)
        Me.grbDatosGenerales.Controls.Add(Me.txtServicio)
        Me.grbDatosGenerales.Controls.Add(Me.lblServicioEtiq)
        Me.grbDatosGenerales.Controls.Add(Me.txtRutaDocSustento)
        Me.grbDatosGenerales.Controls.Add(Me.lblRutaDocSustentoEtiq)
        Me.grbDatosGenerales.Controls.Add(Me.lblImpuestosEtiq3)
        Me.grbDatosGenerales.Controls.Add(Me.lblImpuestosEtiq2)
        Me.grbDatosGenerales.Controls.Add(Me.lblImpuestosEtiq1)
        Me.grbDatosGenerales.Controls.Add(Me.lblSTMargenImpto)
        Me.grbDatosGenerales.Controls.Add(Me.lblSTCRImpto)
        Me.grbDatosGenerales.Controls.Add(Me.lblSSMargenLiberadoImpto)
        Me.grbDatosGenerales.Controls.Add(Me.lblSSCLImpto)
        Me.grbDatosGenerales.Controls.Add(Me.lblSSMargenImpto)
        Me.grbDatosGenerales.Controls.Add(Me.lblSSCRImpto)
        Me.grbDatosGenerales.Controls.Add(Me.lblMargenLiberadoImpto)
        Me.grbDatosGenerales.Controls.Add(Me.lblCostoLiberadoImpto)
        Me.grbDatosGenerales.Controls.Add(Me.lblMargenImpto)
        Me.grbDatosGenerales.Controls.Add(Me.Label19)
        Me.grbDatosGenerales.Controls.Add(Me.dtpDia)
        Me.grbDatosGenerales.Controls.Add(Me.txtMotivoEspecial)
        Me.grbDatosGenerales.Controls.Add(Me.lblMotivoEspecialEtiq)
        Me.grbDatosGenerales.Controls.Add(Me.chkEspecial)
        Me.grbDatosGenerales.Controls.Add(Me.cboIDIdioma)
        Me.grbDatosGenerales.Controls.Add(Me.lblIdiomaEtiq)
        Me.grbDatosGenerales.Controls.Add(Me.lblSTTotalEtiq)
        Me.grbDatosGenerales.Controls.Add(Me.lblSTMargenEtiq)
        Me.grbDatosGenerales.Controls.Add(Me.lblSTCREtiq)
        Me.grbDatosGenerales.Controls.Add(Me.lblSSTotalEtiq)
        Me.grbDatosGenerales.Controls.Add(Me.lblSSMLEtiq)
        Me.grbDatosGenerales.Controls.Add(Me.lblSSCLEtiq)
        Me.grbDatosGenerales.Controls.Add(Me.lblSSMargenEtiq)
        Me.grbDatosGenerales.Controls.Add(Me.lblSSCREtiq)
        Me.grbDatosGenerales.Controls.Add(Me.lblTotalEtiq)
        Me.grbDatosGenerales.Controls.Add(Me.lblMargenLiberadoEtiq)
        Me.grbDatosGenerales.Controls.Add(Me.lblCostoLiberadoEtiq)
        Me.grbDatosGenerales.Controls.Add(Me.lblMargenEtiq)
        Me.grbDatosGenerales.Controls.Add(Me.lblCostoRealEtiq)
        Me.grbDatosGenerales.Controls.Add(Me.txtSSCR)
        Me.grbDatosGenerales.Controls.Add(Me.txtSTMargen)
        Me.grbDatosGenerales.Controls.Add(Me.txtSSCL)
        Me.grbDatosGenerales.Controls.Add(Me.txtSSMargen)
        Me.grbDatosGenerales.Controls.Add(Me.txtSTCR)
        Me.grbDatosGenerales.Controls.Add(Me.txtSTTotal)
        Me.grbDatosGenerales.Controls.Add(Me.txtSSTotal)
        Me.grbDatosGenerales.Controls.Add(Me.txtSSMargenLiberado)
        Me.grbDatosGenerales.Controls.Add(Me.txtMargenAplicado)
        Me.grbDatosGenerales.Controls.Add(Me.txtCostoReal)
        Me.grbDatosGenerales.Controls.Add(Me.cboCiudad)
        Me.grbDatosGenerales.Controls.Add(Me.Label12)
        Me.grbDatosGenerales.Controls.Add(Me.cboPais)
        Me.grbDatosGenerales.Controls.Add(Me.Label13)
        Me.grbDatosGenerales.Controls.Add(Me.cboTipoProv)
        Me.grbDatosGenerales.Controls.Add(Me.Label22)
        Me.grbDatosGenerales.Controls.Add(Me.btnProveedor)
        Me.grbDatosGenerales.Controls.Add(Me.lblProveedor)
        Me.grbDatosGenerales.Controls.Add(Me.Label1)
        Me.grbDatosGenerales.Controls.Add(Me.lblTitulo)
        Me.grbDatosGenerales.Controls.Add(Me.chkCena)
        Me.grbDatosGenerales.Dock = System.Windows.Forms.DockStyle.Fill
        Me.grbDatosGenerales.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.grbDatosGenerales.Location = New System.Drawing.Point(3, 3)
        Me.grbDatosGenerales.Name = "grbDatosGenerales"
        Me.grbDatosGenerales.Size = New System.Drawing.Size(1132, 578)
        Me.grbDatosGenerales.TabIndex = 1
        Me.grbDatosGenerales.TabStop = False
        '
        'chkServNoCobrado
        '
        Me.chkServNoCobrado.AutoSize = True
        Me.chkServNoCobrado.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkServNoCobrado.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.chkServNoCobrado.Location = New System.Drawing.Point(804, 131)
        Me.chkServNoCobrado.Name = "chkServNoCobrado"
        Me.chkServNoCobrado.Size = New System.Drawing.Size(138, 17)
        Me.chkServNoCobrado.TabIndex = 376
        Me.chkServNoCobrado.Tag = "Borrar"
        Me.chkServNoCobrado.Text = "Servicio No Cobrado"
        Me.chkServNoCobrado.UseVisualStyleBackColor = True
        Me.chkServNoCobrado.Visible = False
        '
        'chkAcomVehi
        '
        Me.chkAcomVehi.AutoSize = True
        Me.chkAcomVehi.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkAcomVehi.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.chkAcomVehi.Location = New System.Drawing.Point(804, 108)
        Me.chkAcomVehi.Name = "chkAcomVehi"
        Me.chkAcomVehi.Size = New System.Drawing.Size(135, 17)
        Me.chkAcomVehi.TabIndex = 375
        Me.chkAcomVehi.Tag = "Borrar"
        Me.chkAcomVehi.Text = "Acomodo Vehículos"
        Me.chkAcomVehi.UseVisualStyleBackColor = True
        Me.chkAcomVehi.Visible = False
        '
        'cboPaisDest
        '
        Me.cboPaisDest.BackColor = System.Drawing.SystemColors.Window
        Me.cboPaisDest.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboPaisDest.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboPaisDest.FormattingEnabled = True
        Me.cboPaisDest.Location = New System.Drawing.Point(584, 513)
        Me.cboPaisDest.Name = "cboPaisDest"
        Me.cboPaisDest.Size = New System.Drawing.Size(137, 21)
        Me.cboPaisDest.TabIndex = 373
        Me.cboPaisDest.Tag = "Borrar"
        '
        'lblEtiqPaisDest
        '
        Me.lblEtiqPaisDest.AutoSize = True
        Me.lblEtiqPaisDest.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEtiqPaisDest.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.lblEtiqPaisDest.Location = New System.Drawing.Point(508, 516)
        Me.lblEtiqPaisDest.Name = "lblEtiqPaisDest"
        Me.lblEtiqPaisDest.Size = New System.Drawing.Size(76, 13)
        Me.lblEtiqPaisDest.TabIndex = 374
        Me.lblEtiqPaisDest.Text = "Pais Destino"
        '
        'cboPaisOrig
        '
        Me.cboPaisOrig.BackColor = System.Drawing.SystemColors.Window
        Me.cboPaisOrig.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboPaisOrig.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboPaisOrig.FormattingEnabled = True
        Me.cboPaisOrig.Location = New System.Drawing.Point(584, 487)
        Me.cboPaisOrig.Name = "cboPaisOrig"
        Me.cboPaisOrig.Size = New System.Drawing.Size(137, 21)
        Me.cboPaisOrig.TabIndex = 371
        Me.cboPaisOrig.Tag = "Borrar"
        '
        'lblEtiqPaisOrig
        '
        Me.lblEtiqPaisOrig.AutoSize = True
        Me.lblEtiqPaisOrig.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEtiqPaisOrig.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.lblEtiqPaisOrig.Location = New System.Drawing.Point(508, 489)
        Me.lblEtiqPaisOrig.Name = "lblEtiqPaisOrig"
        Me.lblEtiqPaisOrig.Size = New System.Drawing.Size(70, 13)
        Me.lblEtiqPaisOrig.TabIndex = 372
        Me.lblEtiqPaisOrig.Text = "Pais Origen"
        '
        'chkServicioNoShow
        '
        Me.chkServicioNoShow.AutoSize = True
        Me.chkServicioNoShow.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkServicioNoShow.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.chkServicioNoShow.Location = New System.Drawing.Point(804, 85)
        Me.chkServicioNoShow.Name = "chkServicioNoShow"
        Me.chkServicioNoShow.Size = New System.Drawing.Size(121, 17)
        Me.chkServicioNoShow.TabIndex = 370
        Me.chkServicioNoShow.Tag = "Borrar"
        Me.chkServicioNoShow.Text = "Servicio No Show"
        Me.chkServicioNoShow.UseVisualStyleBackColor = True
        Me.chkServicioNoShow.Visible = False
        '
        'lnkDocumentoSustento
        '
        Me.lnkDocumentoSustento.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.lnkDocumentoSustento.Font = New System.Drawing.Font("Tahoma", 6.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lnkDocumentoSustento.Location = New System.Drawing.Point(164, 487)
        Me.lnkDocumentoSustento.Name = "lnkDocumentoSustento"
        Me.lnkDocumentoSustento.Size = New System.Drawing.Size(301, 47)
        Me.lnkDocumentoSustento.TabIndex = 369
        '
        'chkAcomodoEspecial
        '
        Me.chkAcomodoEspecial.AutoSize = True
        Me.chkAcomodoEspecial.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkAcomodoEspecial.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.chkAcomodoEspecial.Location = New System.Drawing.Point(804, 63)
        Me.chkAcomodoEspecial.Name = "chkAcomodoEspecial"
        Me.chkAcomodoEspecial.Size = New System.Drawing.Size(127, 17)
        Me.chkAcomodoEspecial.TabIndex = 368
        Me.chkAcomodoEspecial.Tag = "Borrar"
        Me.chkAcomodoEspecial.Text = "Acomodo Especial"
        Me.chkAcomodoEspecial.UseVisualStyleBackColor = True
        '
        'chkEditarTotalHab
        '
        Me.chkEditarTotalHab.AutoSize = True
        Me.chkEditarTotalHab.Location = New System.Drawing.Point(849, 356)
        Me.chkEditarTotalHab.Name = "chkEditarTotalHab"
        Me.chkEditarTotalHab.Size = New System.Drawing.Size(15, 14)
        Me.chkEditarTotalHab.TabIndex = 367
        Me.chkEditarTotalHab.UseVisualStyleBackColor = True
        Me.chkEditarTotalHab.Visible = False
        '
        'txtTotalHab
        '
        Me.txtTotalHab.BackColor = System.Drawing.SystemColors.Window
        Me.txtTotalHab.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtTotalHab.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtTotalHab.Location = New System.Drawing.Point(870, 352)
        Me.txtTotalHab.MaxLength = 8
        Me.txtTotalHab.Name = "txtTotalHab"
        Me.txtTotalHab.Size = New System.Drawing.Size(75, 21)
        Me.txtTotalHab.TabIndex = 366
        Me.txtTotalHab.Tag = "Borrar"
        Me.txtTotalHab.Text = "0.0000"
        Me.txtTotalHab.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtTotalHab.Visible = False
        '
        'lblTotalR
        '
        Me.lblTotalR.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.lblTotalR.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTotalR.Location = New System.Drawing.Point(870, 416)
        Me.lblTotalR.Name = "lblTotalR"
        Me.lblTotalR.Size = New System.Drawing.Size(75, 21)
        Me.lblTotalR.TabIndex = 365
        Me.lblTotalR.Text = "0.00"
        Me.lblTotalR.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.lblTotalR.Visible = False
        '
        'chkVerVoucher
        '
        Me.chkVerVoucher.AutoSize = True
        Me.chkVerVoucher.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkVerVoucher.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.chkVerVoucher.Location = New System.Drawing.Point(555, 66)
        Me.chkVerVoucher.Name = "chkVerVoucher"
        Me.chkVerVoucher.Size = New System.Drawing.Size(94, 17)
        Me.chkVerVoucher.TabIndex = 0
        Me.chkVerVoucher.Text = "Ver Voucher"
        Me.chkVerVoucher.UseVisualStyleBackColor = True
        Me.chkVerVoucher.Visible = False
        '
        'lblTotalMonEtiq
        '
        Me.lblTotalMonEtiq.AutoSize = True
        Me.lblTotalMonEtiq.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTotalMonEtiq.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.lblTotalMonEtiq.Location = New System.Drawing.Point(772, 421)
        Me.lblTotalMonEtiq.Name = "lblTotalMonEtiq"
        Me.lblTotalMonEtiq.Size = New System.Drawing.Size(56, 13)
        Me.lblTotalMonEtiq.TabIndex = 364
        Me.lblTotalMonEtiq.Text = "Total ($)"
        Me.lblTotalMonEtiq.Visible = False
        '
        'lblIgvTotal
        '
        Me.lblIgvTotal.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.lblIgvTotal.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblIgvTotal.Location = New System.Drawing.Point(870, 394)
        Me.lblIgvTotal.Name = "lblIgvTotal"
        Me.lblIgvTotal.Size = New System.Drawing.Size(75, 21)
        Me.lblIgvTotal.TabIndex = 363
        Me.lblIgvTotal.Text = "0.0000"
        Me.lblIgvTotal.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.lblIgvTotal.Visible = False
        '
        'lblIgvTotalEtiq
        '
        Me.lblIgvTotalEtiq.AutoSize = True
        Me.lblIgvTotalEtiq.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblIgvTotalEtiq.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.lblIgvTotalEtiq.Location = New System.Drawing.Point(773, 399)
        Me.lblIgvTotalEtiq.Name = "lblIgvTotalEtiq"
        Me.lblIgvTotalEtiq.Size = New System.Drawing.Size(58, 13)
        Me.lblIgvTotalEtiq.TabIndex = 362
        Me.lblIgvTotalEtiq.Text = "Igv Total"
        Me.lblIgvTotalEtiq.Visible = False
        '
        'lblNetoTotal
        '
        Me.lblNetoTotal.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.lblNetoTotal.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblNetoTotal.Location = New System.Drawing.Point(870, 373)
        Me.lblNetoTotal.Name = "lblNetoTotal"
        Me.lblNetoTotal.Size = New System.Drawing.Size(75, 21)
        Me.lblNetoTotal.TabIndex = 361
        Me.lblNetoTotal.Text = "0.0000"
        Me.lblNetoTotal.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.lblNetoTotal.Visible = False
        '
        'lblNetoTotalEtiq
        '
        Me.lblNetoTotalEtiq.AutoSize = True
        Me.lblNetoTotalEtiq.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblNetoTotalEtiq.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.lblNetoTotalEtiq.Location = New System.Drawing.Point(773, 378)
        Me.lblNetoTotalEtiq.Name = "lblNetoTotalEtiq"
        Me.lblNetoTotalEtiq.Size = New System.Drawing.Size(65, 13)
        Me.lblNetoTotalEtiq.TabIndex = 360
        Me.lblNetoTotalEtiq.Text = "Neto Total"
        Me.lblNetoTotalEtiq.Visible = False
        '
        'lblTotalHab
        '
        Me.lblTotalHab.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.lblTotalHab.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTotalHab.Location = New System.Drawing.Point(870, 352)
        Me.lblTotalHab.Name = "lblTotalHab"
        Me.lblTotalHab.Size = New System.Drawing.Size(75, 21)
        Me.lblTotalHab.TabIndex = 359
        Me.lblTotalHab.Text = "0.00"
        Me.lblTotalHab.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.lblTotalHab.Visible = False
        '
        'txtIgvHab
        '
        Me.txtIgvHab.BackColor = System.Drawing.SystemColors.Window
        Me.txtIgvHab.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtIgvHab.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtIgvHab.Location = New System.Drawing.Point(870, 330)
        Me.txtIgvHab.MaxLength = 8
        Me.txtIgvHab.Name = "txtIgvHab"
        Me.txtIgvHab.ReadOnly = True
        Me.txtIgvHab.Size = New System.Drawing.Size(75, 21)
        Me.txtIgvHab.TabIndex = 358
        Me.txtIgvHab.Tag = "Borrar"
        Me.txtIgvHab.Text = "0.0000"
        Me.txtIgvHab.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtIgvHab.Visible = False
        '
        'txtNetoHab
        '
        Me.txtNetoHab.BackColor = System.Drawing.SystemColors.Window
        Me.txtNetoHab.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtNetoHab.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtNetoHab.Location = New System.Drawing.Point(870, 308)
        Me.txtNetoHab.MaxLength = 8
        Me.txtNetoHab.Name = "txtNetoHab"
        Me.txtNetoHab.ReadOnly = True
        Me.txtNetoHab.Size = New System.Drawing.Size(75, 21)
        Me.txtNetoHab.TabIndex = 357
        Me.txtNetoHab.Tag = "Borrar"
        Me.txtNetoHab.Text = "0.0000"
        Me.txtNetoHab.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtNetoHab.Visible = False
        '
        'lblTotalHabEtiq
        '
        Me.lblTotalHabEtiq.AutoSize = True
        Me.lblTotalHabEtiq.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTotalHabEtiq.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.lblTotalHabEtiq.Location = New System.Drawing.Point(773, 355)
        Me.lblTotalHabEtiq.Name = "lblTotalHabEtiq"
        Me.lblTotalHabEtiq.Size = New System.Drawing.Size(64, 13)
        Me.lblTotalHabEtiq.TabIndex = 356
        Me.lblTotalHabEtiq.Text = "Total Hab."
        Me.lblTotalHabEtiq.Visible = False
        '
        'lblIGVHabEtiq
        '
        Me.lblIGVHabEtiq.AutoSize = True
        Me.lblIGVHabEtiq.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblIGVHabEtiq.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.lblIGVHabEtiq.Location = New System.Drawing.Point(773, 332)
        Me.lblIGVHabEtiq.Name = "lblIGVHabEtiq"
        Me.lblIGVHabEtiq.Size = New System.Drawing.Size(54, 13)
        Me.lblIGVHabEtiq.TabIndex = 355
        Me.lblIGVHabEtiq.Text = "Igv Hab."
        Me.lblIGVHabEtiq.Visible = False
        '
        'lblNetoHabEtiq
        '
        Me.lblNetoHabEtiq.AutoSize = True
        Me.lblNetoHabEtiq.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblNetoHabEtiq.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.lblNetoHabEtiq.Location = New System.Drawing.Point(773, 310)
        Me.lblNetoHabEtiq.Name = "lblNetoHabEtiq"
        Me.lblNetoHabEtiq.Size = New System.Drawing.Size(61, 13)
        Me.lblNetoHabEtiq.TabIndex = 354
        Me.lblNetoHabEtiq.Text = "Neto Hab."
        Me.lblNetoHabEtiq.Visible = False
        '
        'Label18
        '
        Me.Label18.AutoSize = True
        Me.Label18.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label18.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.Label18.Location = New System.Drawing.Point(22, 211)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(96, 13)
        Me.Label18.TabIndex = 352
        Me.Label18.Text = "Tipo de Servicio"
        '
        'lblTipoServicio
        '
        Me.lblTipoServicio.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.lblTipoServicio.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTipoServicio.Location = New System.Drawing.Point(163, 207)
        Me.lblTipoServicio.Name = "lblTipoServicio"
        Me.lblTipoServicio.Size = New System.Drawing.Size(71, 22)
        Me.lblTipoServicio.TabIndex = 351
        '
        'lblDescVariante
        '
        Me.lblDescVariante.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.lblDescVariante.Font = New System.Drawing.Font("Tahoma", 6.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDescVariante.Location = New System.Drawing.Point(241, 234)
        Me.lblDescVariante.Name = "lblDescVariante"
        Me.lblDescVariante.Size = New System.Drawing.Size(408, 31)
        Me.lblDescVariante.TabIndex = 350
        '
        'lblVariante
        '
        Me.lblVariante.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.lblVariante.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblVariante.Location = New System.Drawing.Point(163, 234)
        Me.lblVariante.Name = "lblVariante"
        Me.lblVariante.Size = New System.Drawing.Size(71, 22)
        Me.lblVariante.TabIndex = 348
        '
        'Label17
        '
        Me.Label17.AutoSize = True
        Me.Label17.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label17.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.Label17.Location = New System.Drawing.Point(23, 239)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(55, 13)
        Me.Label17.TabIndex = 349
        Me.Label17.Text = "Variante"
        '
        'txtCantidadaPagar
        '
        Me.txtCantidadaPagar.BackColor = System.Drawing.SystemColors.Window
        Me.txtCantidadaPagar.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtCantidadaPagar.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtCantidadaPagar.Location = New System.Drawing.Point(870, 286)
        Me.txtCantidadaPagar.MaxLength = 2
        Me.txtCantidadaPagar.Name = "txtCantidadaPagar"
        Me.txtCantidadaPagar.ReadOnly = True
        Me.txtCantidadaPagar.Size = New System.Drawing.Size(75, 21)
        Me.txtCantidadaPagar.TabIndex = 347
        Me.txtCantidadaPagar.Tag = "Borrar"
        Me.txtCantidadaPagar.Text = "0"
        Me.txtCantidadaPagar.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtCantidadaPagar.Visible = False
        '
        'lblCantPagarEtiq
        '
        Me.lblCantPagarEtiq.AutoSize = True
        Me.lblCantPagarEtiq.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCantPagarEtiq.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.lblCantPagarEtiq.Location = New System.Drawing.Point(773, 289)
        Me.lblCantPagarEtiq.Name = "lblCantPagarEtiq"
        Me.lblCantPagarEtiq.Size = New System.Drawing.Size(72, 13)
        Me.lblCantPagarEtiq.TabIndex = 346
        Me.lblCantPagarEtiq.Text = "Cant. Pagar"
        Me.lblCantPagarEtiq.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.lblCantPagarEtiq.Visible = False
        '
        'grbBus_Guia
        '
        Me.grbBus_Guia.Controls.Add(Me.CboBusVehiculo)
        Me.grbBus_Guia.Controls.Add(Me.cboGuia)
        Me.grbBus_Guia.Controls.Add(Me.lblEtiqBus)
        Me.grbBus_Guia.Controls.Add(Me.lblEtiqGuia)
        Me.grbBus_Guia.Location = New System.Drawing.Point(240, 352)
        Me.grbBus_Guia.Name = "grbBus_Guia"
        Me.grbBus_Guia.Size = New System.Drawing.Size(409, 78)
        Me.grbBus_Guia.TabIndex = 345
        Me.grbBus_Guia.TabStop = False
        Me.grbBus_Guia.Tag = "Borrar"
        Me.grbBus_Guia.Visible = False
        '
        'CboBusVehiculo
        '
        Me.CboBusVehiculo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.CboBusVehiculo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CboBusVehiculo.FormattingEnabled = True
        Me.CboBusVehiculo.Location = New System.Drawing.Point(66, 41)
        Me.CboBusVehiculo.Name = "CboBusVehiculo"
        Me.CboBusVehiculo.Size = New System.Drawing.Size(322, 21)
        Me.CboBusVehiculo.TabIndex = 350
        Me.CboBusVehiculo.Tag = "Borrar"
        '
        'cboGuia
        '
        Me.cboGuia.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.cboGuia.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboGuia.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboGuia.FormattingEnabled = True
        Me.cboGuia.Location = New System.Drawing.Point(66, 14)
        Me.cboGuia.Name = "cboGuia"
        Me.cboGuia.Size = New System.Drawing.Size(322, 21)
        Me.cboGuia.TabIndex = 349
        Me.cboGuia.Tag = "Borrar"
        '
        'lblEtiqBus
        '
        Me.lblEtiqBus.AutoSize = True
        Me.lblEtiqBus.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEtiqBus.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.lblEtiqBus.Location = New System.Drawing.Point(13, 44)
        Me.lblEtiqBus.Name = "lblEtiqBus"
        Me.lblEtiqBus.Size = New System.Drawing.Size(54, 13)
        Me.lblEtiqBus.TabIndex = 347
        Me.lblEtiqBus.Text = "Vehículo"
        '
        'lblEtiqGuia
        '
        Me.lblEtiqGuia.AutoSize = True
        Me.lblEtiqGuia.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEtiqGuia.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.lblEtiqGuia.Location = New System.Drawing.Point(13, 17)
        Me.lblEtiqGuia.Name = "lblEtiqGuia"
        Me.lblEtiqGuia.Size = New System.Drawing.Size(32, 13)
        Me.lblEtiqGuia.TabIndex = 220
        Me.lblEtiqGuia.Text = "Guía"
        '
        'lblRedondeo
        '
        Me.lblRedondeo.AutoSize = True
        Me.lblRedondeo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblRedondeo.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.lblRedondeo.Location = New System.Drawing.Point(22, 370)
        Me.lblRedondeo.Name = "lblRedondeo"
        Me.lblRedondeo.Size = New System.Drawing.Size(64, 13)
        Me.lblRedondeo.TabIndex = 344
        Me.lblRedondeo.Text = "Redondeo"
        '
        'lblRedondeoTotal
        '
        Me.lblRedondeoTotal.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.lblRedondeoTotal.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblRedondeoTotal.Location = New System.Drawing.Point(163, 366)
        Me.lblRedondeoTotal.Name = "lblRedondeoTotal"
        Me.lblRedondeoTotal.Size = New System.Drawing.Size(71, 21)
        Me.lblRedondeoTotal.TabIndex = 343
        Me.lblRedondeoTotal.Text = "0.0000"
        Me.lblRedondeoTotal.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'chkIncGuia
        '
        Me.chkIncGuia.AutoSize = True
        Me.chkIncGuia.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkIncGuia.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.chkIncGuia.Location = New System.Drawing.Point(804, 42)
        Me.chkIncGuia.Name = "chkIncGuia"
        Me.chkIncGuia.Size = New System.Drawing.Size(141, 17)
        Me.chkIncGuia.TabIndex = 342
        Me.chkIncGuia.Tag = "Borrar"
        Me.chkIncGuia.Text = "Servicio para el Guía"
        Me.chkIncGuia.UseVisualStyleBackColor = True
        Me.chkIncGuia.Visible = False
        '
        'grbChecksVoucBiblia
        '
        Me.grbChecksVoucBiblia.Controls.Add(Me.chkVerBiblia)
        Me.grbChecksVoucBiblia.Location = New System.Drawing.Point(374, 66)
        Me.grbChecksVoucBiblia.Name = "grbChecksVoucBiblia"
        Me.grbChecksVoucBiblia.Size = New System.Drawing.Size(175, 43)
        Me.grbChecksVoucBiblia.TabIndex = 341
        Me.grbChecksVoucBiblia.TabStop = False
        '
        'chkVerBiblia
        '
        Me.chkVerBiblia.AutoSize = True
        Me.chkVerBiblia.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkVerBiblia.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.chkVerBiblia.Location = New System.Drawing.Point(13, 15)
        Me.chkVerBiblia.Name = "chkVerBiblia"
        Me.chkVerBiblia.Size = New System.Drawing.Size(78, 17)
        Me.chkVerBiblia.TabIndex = 1
        Me.chkVerBiblia.Text = "Ver Biblia"
        Me.chkVerBiblia.UseVisualStyleBackColor = True
        '
        'dtpDiaHora
        '
        Me.dtpDiaHora.CustomFormat = "HH:mm"
        Me.dtpDiaHora.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.dtpDiaHora.Location = New System.Drawing.Point(259, 42)
        Me.dtpDiaHora.MinDate = New Date(1990, 1, 1, 0, 0, 0, 0)
        Me.dtpDiaHora.Name = "dtpDiaHora"
        Me.dtpDiaHora.ShowUpDown = True
        Me.dtpDiaHora.Size = New System.Drawing.Size(94, 21)
        Me.dtpDiaHora.TabIndex = 340
        Me.dtpDiaHora.Tag = "Borrar"
        Me.dtpDiaHora.Value = New Date(2012, 5, 21, 0, 0, 0, 0)
        '
        'txtCantidad
        '
        Me.txtCantidad.BackColor = System.Drawing.SystemColors.Window
        Me.txtCantidad.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtCantidad.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtCantidad.Location = New System.Drawing.Point(704, 182)
        Me.txtCantidad.MaxLength = 2
        Me.txtCantidad.Name = "txtCantidad"
        Me.txtCantidad.Size = New System.Drawing.Size(58, 21)
        Me.txtCantidad.TabIndex = 339
        Me.txtCantidad.Tag = "Borrar"
        Me.txtCantidad.Text = "0"
        Me.txtCantidad.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lblCantidadEtiq
        '
        Me.lblCantidadEtiq.AutoSize = True
        Me.lblCantidadEtiq.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCantidadEtiq.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.lblCantidadEtiq.Location = New System.Drawing.Point(640, 185)
        Me.lblCantidadEtiq.Name = "lblCantidadEtiq"
        Me.lblCantidadEtiq.Size = New System.Drawing.Size(57, 13)
        Me.lblCantidadEtiq.TabIndex = 338
        Me.lblCantidadEtiq.Text = "Cantidad"
        Me.lblCantidadEtiq.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblNoches
        '
        Me.lblNoches.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.lblNoches.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblNoches.Location = New System.Drawing.Point(621, 42)
        Me.lblNoches.Name = "lblNoches"
        Me.lblNoches.Size = New System.Drawing.Size(58, 21)
        Me.lblNoches.TabIndex = 337
        Me.lblNoches.Text = "0"
        Me.lblNoches.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblNochesEtiq
        '
        Me.lblNochesEtiq.AutoSize = True
        Me.lblNochesEtiq.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblNochesEtiq.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.lblNochesEtiq.Location = New System.Drawing.Point(555, 45)
        Me.lblNochesEtiq.Name = "lblNochesEtiq"
        Me.lblNochesEtiq.Size = New System.Drawing.Size(47, 13)
        Me.lblNochesEtiq.TabIndex = 336
        Me.lblNochesEtiq.Text = "Noches"
        '
        'lblFechaOutEtiq
        '
        Me.lblFechaOutEtiq.AutoSize = True
        Me.lblFechaOutEtiq.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblFechaOutEtiq.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.lblFechaOutEtiq.Location = New System.Drawing.Point(371, 44)
        Me.lblFechaOutEtiq.Name = "lblFechaOutEtiq"
        Me.lblFechaOutEtiq.Size = New System.Drawing.Size(63, 13)
        Me.lblFechaOutEtiq.TabIndex = 335
        Me.lblFechaOutEtiq.Text = "Fecha Out"
        '
        'dtpFechaOut
        '
        Me.dtpFechaOut.CalendarFont = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpFechaOut.CustomFormat = "dd/MM/yyyy"
        Me.dtpFechaOut.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpFechaOut.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.dtpFechaOut.Location = New System.Drawing.Point(459, 41)
        Me.dtpFechaOut.Name = "dtpFechaOut"
        Me.dtpFechaOut.Size = New System.Drawing.Size(90, 21)
        Me.dtpFechaOut.TabIndex = 334
        Me.dtpFechaOut.Tag = "Borrar"
        '
        'txtSTTotalOrig
        '
        Me.txtSTTotalOrig.BackColor = System.Drawing.SystemColors.Window
        Me.txtSTTotalOrig.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtSTTotalOrig.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtSTTotalOrig.Location = New System.Drawing.Point(988, 292)
        Me.txtSTTotalOrig.MaxLength = 8
        Me.txtSTTotalOrig.Name = "txtSTTotalOrig"
        Me.txtSTTotalOrig.ReadOnly = True
        Me.txtSTTotalOrig.Size = New System.Drawing.Size(72, 21)
        Me.txtSTTotalOrig.TabIndex = 333
        Me.txtSTTotalOrig.Tag = "Borrar"
        Me.txtSTTotalOrig.Text = "0.00"
        Me.txtSTTotalOrig.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtSTTotalOrig.Visible = False
        '
        'txtSSTotalOrig
        '
        Me.txtSSTotalOrig.BackColor = System.Drawing.SystemColors.Window
        Me.txtSSTotalOrig.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtSSTotalOrig.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtSSTotalOrig.Location = New System.Drawing.Point(367, 677)
        Me.txtSSTotalOrig.MaxLength = 8
        Me.txtSSTotalOrig.Name = "txtSSTotalOrig"
        Me.txtSSTotalOrig.ReadOnly = True
        Me.txtSSTotalOrig.Size = New System.Drawing.Size(72, 21)
        Me.txtSSTotalOrig.TabIndex = 332
        Me.txtSSTotalOrig.Tag = "Borrar"
        Me.txtSSTotalOrig.Text = "0.00"
        Me.txtSSTotalOrig.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtSSTotalOrig.Visible = False
        '
        'lblTotalOrig
        '
        Me.lblTotalOrig.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.lblTotalOrig.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTotalOrig.Location = New System.Drawing.Point(20, 706)
        Me.lblTotalOrig.Name = "lblTotalOrig"
        Me.lblTotalOrig.Size = New System.Drawing.Size(58, 21)
        Me.lblTotalOrig.TabIndex = 331
        Me.lblTotalOrig.Text = "0.00"
        Me.lblTotalOrig.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.lblTotalOrig.Visible = False
        '
        'cboDestino
        '
        Me.cboDestino.BackColor = System.Drawing.SystemColors.Window
        Me.cboDestino.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboDestino.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboDestino.FormattingEnabled = True
        Me.cboDestino.Location = New System.Drawing.Point(704, 182)
        Me.cboDestino.Name = "cboDestino"
        Me.cboDestino.Size = New System.Drawing.Size(243, 21)
        Me.cboDestino.TabIndex = 329
        Me.cboDestino.Tag = "Borrar"
        '
        'lblDestinoEtiq
        '
        Me.lblDestinoEtiq.AutoSize = True
        Me.lblDestinoEtiq.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDestinoEtiq.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.lblDestinoEtiq.Location = New System.Drawing.Point(645, 185)
        Me.lblDestinoEtiq.Name = "lblDestinoEtiq"
        Me.lblDestinoEtiq.Size = New System.Drawing.Size(53, 13)
        Me.lblDestinoEtiq.TabIndex = 330
        Me.lblDestinoEtiq.Text = "Destino:"
        '
        'cboOrigen
        '
        Me.cboOrigen.BackColor = System.Drawing.SystemColors.Window
        Me.cboOrigen.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboOrigen.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboOrigen.FormattingEnabled = True
        Me.cboOrigen.Location = New System.Drawing.Point(704, 155)
        Me.cboOrigen.Name = "cboOrigen"
        Me.cboOrigen.Size = New System.Drawing.Size(243, 21)
        Me.cboOrigen.TabIndex = 327
        Me.cboOrigen.Tag = "Borrar"
        '
        'lblOrigenEtiq
        '
        Me.lblOrigenEtiq.AutoSize = True
        Me.lblOrigenEtiq.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblOrigenEtiq.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.lblOrigenEtiq.Location = New System.Drawing.Point(651, 158)
        Me.lblOrigenEtiq.Name = "lblOrigenEtiq"
        Me.lblOrigenEtiq.Size = New System.Drawing.Size(47, 13)
        Me.lblOrigenEtiq.TabIndex = 328
        Me.lblOrigenEtiq.Text = "Origen:"
        '
        'lblTipoTransporteEtiq
        '
        Me.lblTipoTransporteEtiq.AutoSize = True
        Me.lblTipoTransporteEtiq.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTipoTransporteEtiq.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.lblTipoTransporteEtiq.Location = New System.Drawing.Point(456, 125)
        Me.lblTipoTransporteEtiq.Name = "lblTipoTransporteEtiq"
        Me.lblTipoTransporteEtiq.Size = New System.Drawing.Size(100, 13)
        Me.lblTipoTransporteEtiq.TabIndex = 326
        Me.lblTipoTransporteEtiq.Text = "Tipo Transporte:"
        Me.lblTipoTransporteEtiq.Visible = False
        '
        'cboTipoTransporte
        '
        Me.cboTipoTransporte.BackColor = System.Drawing.SystemColors.Window
        Me.cboTipoTransporte.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboTipoTransporte.Enabled = False
        Me.cboTipoTransporte.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboTipoTransporte.FormattingEnabled = True
        Me.cboTipoTransporte.Location = New System.Drawing.Point(558, 122)
        Me.cboTipoTransporte.Name = "cboTipoTransporte"
        Me.cboTipoTransporte.Size = New System.Drawing.Size(117, 21)
        Me.cboTipoTransporte.TabIndex = 325
        Me.cboTipoTransporte.Tag = "Borrar"
        Me.cboTipoTransporte.Visible = False
        '
        'chkTransfer
        '
        Me.chkTransfer.AutoSize = True
        Me.chkTransfer.Enabled = False
        Me.chkTransfer.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkTransfer.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.chkTransfer.Location = New System.Drawing.Point(374, 124)
        Me.chkTransfer.Name = "chkTransfer"
        Me.chkTransfer.Size = New System.Drawing.Size(74, 17)
        Me.chkTransfer.TabIndex = 324
        Me.chkTransfer.Tag = "Borrar"
        Me.chkTransfer.Text = "Transfer"
        Me.chkTransfer.UseVisualStyleBackColor = True
        Me.chkTransfer.Visible = False
        '
        'cboIDUbigeoDes
        '
        Me.cboIDUbigeoDes.BackColor = System.Drawing.SystemColors.Window
        Me.cboIDUbigeoDes.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboIDUbigeoDes.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboIDUbigeoDes.FormattingEnabled = True
        Me.cboIDUbigeoDes.Location = New System.Drawing.Point(818, 513)
        Me.cboIDUbigeoDes.Name = "cboIDUbigeoDes"
        Me.cboIDUbigeoDes.Size = New System.Drawing.Size(129, 21)
        Me.cboIDUbigeoDes.TabIndex = 313
        Me.cboIDUbigeoDes.Tag = "Borrar"
        '
        'lblIDUbigeoDesEtiq
        '
        Me.lblIDUbigeoDesEtiq.AutoSize = True
        Me.lblIDUbigeoDesEtiq.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblIDUbigeoDesEtiq.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.lblIDUbigeoDesEtiq.Location = New System.Drawing.Point(727, 516)
        Me.lblIDUbigeoDesEtiq.Name = "lblIDUbigeoDesEtiq"
        Me.lblIDUbigeoDesEtiq.Size = New System.Drawing.Size(91, 13)
        Me.lblIDUbigeoDesEtiq.TabIndex = 314
        Me.lblIDUbigeoDesEtiq.Text = "Ciudad Destino"
        '
        'cboIDUbigeoOri
        '
        Me.cboIDUbigeoOri.BackColor = System.Drawing.SystemColors.Window
        Me.cboIDUbigeoOri.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboIDUbigeoOri.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboIDUbigeoOri.FormattingEnabled = True
        Me.cboIDUbigeoOri.Location = New System.Drawing.Point(818, 487)
        Me.cboIDUbigeoOri.Name = "cboIDUbigeoOri"
        Me.cboIDUbigeoOri.Size = New System.Drawing.Size(129, 21)
        Me.cboIDUbigeoOri.TabIndex = 311
        Me.cboIDUbigeoOri.Tag = "Borrar"
        '
        'lblIDUbigeoOriEtiq
        '
        Me.lblIDUbigeoOriEtiq.AutoSize = True
        Me.lblIDUbigeoOriEtiq.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblIDUbigeoOriEtiq.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.lblIDUbigeoOriEtiq.Location = New System.Drawing.Point(727, 490)
        Me.lblIDUbigeoOriEtiq.Name = "lblIDUbigeoOriEtiq"
        Me.lblIDUbigeoOriEtiq.Size = New System.Drawing.Size(85, 13)
        Me.lblIDUbigeoOriEtiq.TabIndex = 312
        Me.lblIDUbigeoOriEtiq.Text = "Ciudad Origen"
        '
        'chkLonche
        '
        Me.chkLonche.AutoSize = True
        Me.chkLonche.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkLonche.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.chkLonche.Location = New System.Drawing.Point(566, 436)
        Me.chkLonche.Name = "chkLonche"
        Me.chkLonche.Size = New System.Drawing.Size(83, 17)
        Me.chkLonche.TabIndex = 24
        Me.chkLonche.Tag = "Borrar"
        Me.chkLonche.Text = "Box Lunch"
        Me.chkLonche.UseVisualStyleBackColor = True
        '
        'chkAlmuerzo
        '
        Me.chkAlmuerzo.AutoSize = True
        Me.chkAlmuerzo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkAlmuerzo.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.chkAlmuerzo.Location = New System.Drawing.Point(478, 459)
        Me.chkAlmuerzo.Name = "chkAlmuerzo"
        Me.chkAlmuerzo.Size = New System.Drawing.Size(80, 17)
        Me.chkAlmuerzo.TabIndex = 25
        Me.chkAlmuerzo.Tag = "Borrar"
        Me.chkAlmuerzo.Text = "Almuerzo"
        Me.chkAlmuerzo.UseVisualStyleBackColor = True
        '
        'chkDesayuno
        '
        Me.chkDesayuno.AutoSize = True
        Me.chkDesayuno.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkDesayuno.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.chkDesayuno.Location = New System.Drawing.Point(478, 436)
        Me.chkDesayuno.Name = "chkDesayuno"
        Me.chkDesayuno.Size = New System.Drawing.Size(82, 17)
        Me.chkDesayuno.TabIndex = 23
        Me.chkDesayuno.Tag = "Borrar"
        Me.chkDesayuno.Text = "Desayuno"
        Me.chkDesayuno.UseVisualStyleBackColor = True
        '
        'chkSelPax
        '
        Me.chkSelPax.AutoSize = True
        Me.chkSelPax.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkSelPax.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.chkSelPax.Location = New System.Drawing.Point(241, 267)
        Me.chkSelPax.Name = "chkSelPax"
        Me.chkSelPax.Size = New System.Drawing.Size(115, 17)
        Me.chkSelPax.TabIndex = 306
        Me.chkSelPax.Tag = "Borrar"
        Me.chkSelPax.Text = "Seleccionar Pax"
        Me.chkSelPax.UseVisualStyleBackColor = True
        '
        'grbPax
        '
        Me.grbPax.Controls.Add(Me.chkSelTodosPax)
        Me.grbPax.Controls.Add(Me.cboTipoLib)
        Me.grbPax.Controls.Add(Me.lblTipoLibEtiq)
        Me.grbPax.Controls.Add(Me.Label3)
        Me.grbPax.Controls.Add(Me.txtNroLiberados)
        Me.grbPax.Controls.Add(Me.Label2)
        Me.grbPax.Controls.Add(Me.txtNroPax)
        Me.grbPax.Controls.Add(Me.lvwPax)
        Me.grbPax.Enabled = False
        Me.grbPax.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.grbPax.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.grbPax.Location = New System.Drawing.Point(241, 279)
        Me.grbPax.Name = "grbPax"
        Me.grbPax.Size = New System.Drawing.Size(526, 44)
        Me.grbPax.TabIndex = 305
        Me.grbPax.TabStop = False
        Me.grbPax.Tag = "Borrar"
        '
        'chkSelTodosPax
        '
        Me.chkSelTodosPax.AutoSize = True
        Me.chkSelTodosPax.Location = New System.Drawing.Point(414, 18)
        Me.chkSelTodosPax.Name = "chkSelTodosPax"
        Me.chkSelTodosPax.Size = New System.Drawing.Size(100, 17)
        Me.chkSelTodosPax.TabIndex = 353
        Me.chkSelTodosPax.Tag = "Borrar"
        Me.chkSelTodosPax.Text = "Selecc. todos"
        Me.chkSelTodosPax.UseVisualStyleBackColor = True
        '
        'cboTipoLib
        '
        Me.cboTipoLib.BackColor = System.Drawing.SystemColors.Window
        Me.cboTipoLib.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboTipoLib.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboTipoLib.FormattingEnabled = True
        Me.cboTipoLib.Location = New System.Drawing.Point(279, 16)
        Me.cboTipoLib.Name = "cboTipoLib"
        Me.cboTipoLib.Size = New System.Drawing.Size(129, 21)
        Me.cboTipoLib.TabIndex = 224
        Me.cboTipoLib.Tag = "Borrar"
        '
        'lblTipoLibEtiq
        '
        Me.lblTipoLibEtiq.AutoSize = True
        Me.lblTipoLibEtiq.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTipoLibEtiq.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.lblTipoLibEtiq.Location = New System.Drawing.Point(191, 19)
        Me.lblTipoLibEtiq.Name = "lblTipoLibEtiq"
        Me.lblTipoLibEtiq.Size = New System.Drawing.Size(86, 13)
        Me.lblTipoLibEtiq.TabIndex = 226
        Me.lblTipoLibEtiq.Text = "Tipo Liberado:"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.Label3.Location = New System.Drawing.Point(84, 19)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(65, 13)
        Me.Label3.TabIndex = 225
        Me.Label3.Text = "Liberados:"
        '
        'txtNroLiberados
        '
        Me.txtNroLiberados.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtNroLiberados.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtNroLiberados.Location = New System.Drawing.Point(153, 14)
        Me.txtNroLiberados.MaxLength = 1
        Me.txtNroLiberados.Name = "txtNroLiberados"
        Me.txtNroLiberados.Size = New System.Drawing.Size(33, 21)
        Me.txtNroLiberados.TabIndex = 223
        Me.txtNroLiberados.Tag = "Borrar"
        Me.txtNroLiberados.Text = "0"
        Me.txtNroLiberados.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.Label2.Location = New System.Drawing.Point(6, 19)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(31, 13)
        Me.Label2.TabIndex = 222
        Me.Label2.Text = "Pax:"
        '
        'txtNroPax
        '
        Me.txtNroPax.BackColor = System.Drawing.SystemColors.Window
        Me.txtNroPax.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtNroPax.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtNroPax.Location = New System.Drawing.Point(40, 14)
        Me.txtNroPax.MaxLength = 3
        Me.txtNroPax.Name = "txtNroPax"
        Me.txtNroPax.ReadOnly = True
        Me.txtNroPax.Size = New System.Drawing.Size(38, 21)
        Me.txtNroPax.TabIndex = 221
        Me.txtNroPax.Tag = "Borrar"
        Me.txtNroPax.Text = "0"
        Me.txtNroPax.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lvwPax
        '
        Me.lvwPax.CheckBoxes = True
        Me.lvwPax.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lvwPax.LargeImageList = Me.ImageList1
        Me.lvwPax.Location = New System.Drawing.Point(6, 47)
        Me.lvwPax.Name = "lvwPax"
        Me.lvwPax.Size = New System.Drawing.Size(507, 88)
        Me.lvwPax.SmallImageList = Me.ImageList1
        Me.lvwPax.TabIndex = 0
        Me.lvwPax.Tag = "Borrar"
        Me.lvwPax.UseCompatibleStateImageBehavior = False
        Me.lvwPax.View = System.Windows.Forms.View.Details
        '
        'ImageList1
        '
        Me.ImageList1.ImageStream = CType(resources.GetObject("ImageList1.ImageStream"), System.Windows.Forms.ImageListStreamer)
        Me.ImageList1.TransparentColor = System.Drawing.Color.Transparent
        Me.ImageList1.Images.SetKeyName(0, "contacto_icono.png")
        Me.ImageList1.Images.SetKeyName(1, "car.png")
        Me.ImageList1.Images.SetKeyName(2, "user_suit.png")
        '
        'lblTotal
        '
        Me.lblTotal.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.lblTotal.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTotal.Location = New System.Drawing.Point(163, 391)
        Me.lblTotal.Name = "lblTotal"
        Me.lblTotal.Size = New System.Drawing.Size(71, 21)
        Me.lblTotal.TabIndex = 303
        Me.lblTotal.Text = "0.00"
        Me.lblTotal.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblMargenLiberado
        '
        Me.lblMargenLiberado.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.lblMargenLiberado.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMargenLiberado.Location = New System.Drawing.Point(591, 327)
        Me.lblMargenLiberado.Name = "lblMargenLiberado"
        Me.lblMargenLiberado.Size = New System.Drawing.Size(58, 21)
        Me.lblMargenLiberado.TabIndex = 302
        Me.lblMargenLiberado.Text = "0.0000"
        Me.lblMargenLiberado.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.lblMargenLiberado.Visible = False
        '
        'lblCostoLiberado
        '
        Me.lblCostoLiberado.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.lblCostoLiberado.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCostoLiberado.Location = New System.Drawing.Point(163, 291)
        Me.lblCostoLiberado.Name = "lblCostoLiberado"
        Me.lblCostoLiberado.Size = New System.Drawing.Size(71, 21)
        Me.lblCostoLiberado.TabIndex = 301
        Me.lblCostoLiberado.Text = "0.0000"
        Me.lblCostoLiberado.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblMargen
        '
        Me.lblMargen.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.lblMargen.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMargen.Location = New System.Drawing.Point(163, 342)
        Me.lblMargen.Name = "lblMargen"
        Me.lblMargen.Size = New System.Drawing.Size(71, 21)
        Me.lblMargen.TabIndex = 300
        Me.lblMargen.Text = "0.0000"
        Me.lblMargen.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtCostoRealImpto
        '
        Me.txtCostoRealImpto.BackColor = System.Drawing.SystemColors.Window
        Me.txtCostoRealImpto.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtCostoRealImpto.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtCostoRealImpto.Location = New System.Drawing.Point(20, 593)
        Me.txtCostoRealImpto.MaxLength = 8
        Me.txtCostoRealImpto.Name = "txtCostoRealImpto"
        Me.txtCostoRealImpto.ReadOnly = True
        Me.txtCostoRealImpto.Size = New System.Drawing.Size(58, 21)
        Me.txtCostoRealImpto.TabIndex = 299
        Me.txtCostoRealImpto.Tag = "Borrar"
        Me.txtCostoRealImpto.Text = "0.00"
        Me.txtCostoRealImpto.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtCostoRealImpto.Visible = False
        '
        'btnRutaArchivo
        '
        Me.btnRutaArchivo.Enabled = False
        Me.btnRutaArchivo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnRutaArchivo.Location = New System.Drawing.Point(471, 486)
        Me.btnRutaArchivo.Name = "btnRutaArchivo"
        Me.btnRutaArchivo.Size = New System.Drawing.Size(28, 22)
        Me.btnRutaArchivo.TabIndex = 298
        Me.btnRutaArchivo.Tag = "Borrar"
        Me.btnRutaArchivo.Text = "..."
        Me.btnRutaArchivo.UseVisualStyleBackColor = True
        '
        'lblTotImpto
        '
        Me.lblTotImpto.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.lblTotImpto.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTotImpto.Location = New System.Drawing.Point(163, 317)
        Me.lblTotImpto.Name = "lblTotImpto"
        Me.lblTotImpto.Size = New System.Drawing.Size(71, 21)
        Me.lblTotImpto.TabIndex = 297
        Me.lblTotImpto.Text = "0.0000"
        Me.lblTotImpto.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblTotImptoEtiq
        '
        Me.lblTotImptoEtiq.AutoSize = True
        Me.lblTotImptoEtiq.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTotImptoEtiq.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.lblTotImptoEtiq.Location = New System.Drawing.Point(21, 321)
        Me.lblTotImptoEtiq.Name = "lblTotImptoEtiq"
        Me.lblTotImptoEtiq.Size = New System.Drawing.Size(68, 13)
        Me.lblTotImptoEtiq.TabIndex = 296
        Me.lblTotImptoEtiq.Text = "Impuestos"
        '
        'txtServicio
        '
        Me.txtServicio.BackColor = System.Drawing.SystemColors.Window
        Me.txtServicio.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtServicio.Location = New System.Drawing.Point(163, 182)
        Me.txtServicio.MaxLength = 250
        Me.txtServicio.Name = "txtServicio"
        Me.txtServicio.ReadOnly = True
        Me.txtServicio.Size = New System.Drawing.Size(476, 21)
        Me.txtServicio.TabIndex = 254
        Me.txtServicio.Tag = "Borrar"
        '
        'lblServicioEtiq
        '
        Me.lblServicioEtiq.AutoSize = True
        Me.lblServicioEtiq.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblServicioEtiq.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.lblServicioEtiq.Location = New System.Drawing.Point(23, 185)
        Me.lblServicioEtiq.Name = "lblServicioEtiq"
        Me.lblServicioEtiq.Size = New System.Drawing.Size(52, 13)
        Me.lblServicioEtiq.TabIndex = 255
        Me.lblServicioEtiq.Text = "Servicio"
        '
        'txtRutaDocSustento
        '
        Me.txtRutaDocSustento.BackColor = System.Drawing.SystemColors.Window
        Me.txtRutaDocSustento.Enabled = False
        Me.txtRutaDocSustento.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtRutaDocSustento.Location = New System.Drawing.Point(378, 540)
        Me.txtRutaDocSustento.MaxLength = 200
        Me.txtRutaDocSustento.Multiline = True
        Me.txtRutaDocSustento.Name = "txtRutaDocSustento"
        Me.txtRutaDocSustento.Size = New System.Drawing.Size(301, 27)
        Me.txtRutaDocSustento.TabIndex = 23
        Me.txtRutaDocSustento.Tag = "Borrar"
        Me.txtRutaDocSustento.Visible = False
        '
        'lblRutaDocSustentoEtiq
        '
        Me.lblRutaDocSustentoEtiq.Enabled = False
        Me.lblRutaDocSustentoEtiq.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblRutaDocSustentoEtiq.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.lblRutaDocSustentoEtiq.Location = New System.Drawing.Point(21, 489)
        Me.lblRutaDocSustentoEtiq.Name = "lblRutaDocSustentoEtiq"
        Me.lblRutaDocSustentoEtiq.Size = New System.Drawing.Size(93, 51)
        Me.lblRutaDocSustentoEtiq.TabIndex = 253
        Me.lblRutaDocSustentoEtiq.Text = "Ruta Documento Sustento"
        '
        'lblImpuestosEtiq3
        '
        Me.lblImpuestosEtiq3.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblImpuestosEtiq3.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.lblImpuestosEtiq3.Location = New System.Drawing.Point(992, 215)
        Me.lblImpuestosEtiq3.Name = "lblImpuestosEtiq3"
        Me.lblImpuestosEtiq3.Size = New System.Drawing.Size(73, 13)
        Me.lblImpuestosEtiq3.TabIndex = 252
        Me.lblImpuestosEtiq3.Text = "Impuestos"
        Me.lblImpuestosEtiq3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.lblImpuestosEtiq3.Visible = False
        '
        'lblImpuestosEtiq2
        '
        Me.lblImpuestosEtiq2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblImpuestosEtiq2.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.lblImpuestosEtiq2.Location = New System.Drawing.Point(408, 580)
        Me.lblImpuestosEtiq2.Name = "lblImpuestosEtiq2"
        Me.lblImpuestosEtiq2.Size = New System.Drawing.Size(76, 16)
        Me.lblImpuestosEtiq2.TabIndex = 251
        Me.lblImpuestosEtiq2.Text = "Impuestos"
        Me.lblImpuestosEtiq2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.lblImpuestosEtiq2.Visible = False
        '
        'lblImpuestosEtiq1
        '
        Me.lblImpuestosEtiq1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblImpuestosEtiq1.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.lblImpuestosEtiq1.Location = New System.Drawing.Point(11, 579)
        Me.lblImpuestosEtiq1.Name = "lblImpuestosEtiq1"
        Me.lblImpuestosEtiq1.Size = New System.Drawing.Size(77, 19)
        Me.lblImpuestosEtiq1.TabIndex = 250
        Me.lblImpuestosEtiq1.Text = "Impuestos"
        Me.lblImpuestosEtiq1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.lblImpuestosEtiq1.Visible = False
        '
        'lblSTMargenImpto
        '
        Me.lblSTMargenImpto.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.lblSTMargenImpto.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSTMargenImpto.Location = New System.Drawing.Point(992, 264)
        Me.lblSTMargenImpto.Name = "lblSTMargenImpto"
        Me.lblSTMargenImpto.Size = New System.Drawing.Size(73, 21)
        Me.lblSTMargenImpto.TabIndex = 249
        Me.lblSTMargenImpto.Text = "0.00"
        Me.lblSTMargenImpto.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.lblSTMargenImpto.Visible = False
        '
        'lblSTCRImpto
        '
        Me.lblSTCRImpto.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.lblSTCRImpto.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSTCRImpto.Location = New System.Drawing.Point(992, 237)
        Me.lblSTCRImpto.Name = "lblSTCRImpto"
        Me.lblSTCRImpto.Size = New System.Drawing.Size(73, 21)
        Me.lblSTCRImpto.TabIndex = 248
        Me.lblSTCRImpto.Text = "0.00"
        Me.lblSTCRImpto.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.lblSTCRImpto.Visible = False
        '
        'lblSSMargenLiberadoImpto
        '
        Me.lblSSMargenLiberadoImpto.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.lblSSMargenLiberadoImpto.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSSMargenLiberadoImpto.Location = New System.Drawing.Point(371, 650)
        Me.lblSSMargenLiberadoImpto.Name = "lblSSMargenLiberadoImpto"
        Me.lblSSMargenLiberadoImpto.Size = New System.Drawing.Size(73, 21)
        Me.lblSSMargenLiberadoImpto.TabIndex = 247
        Me.lblSSMargenLiberadoImpto.Text = "0.00"
        Me.lblSSMargenLiberadoImpto.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.lblSSMargenLiberadoImpto.Visible = False
        '
        'lblSSCLImpto
        '
        Me.lblSSCLImpto.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.lblSSCLImpto.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSSCLImpto.Location = New System.Drawing.Point(371, 623)
        Me.lblSSCLImpto.Name = "lblSSCLImpto"
        Me.lblSSCLImpto.Size = New System.Drawing.Size(73, 21)
        Me.lblSSCLImpto.TabIndex = 246
        Me.lblSSCLImpto.Text = "0.00"
        Me.lblSSCLImpto.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.lblSSCLImpto.Visible = False
        '
        'lblSSMargenImpto
        '
        Me.lblSSMargenImpto.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.lblSSMargenImpto.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSSMargenImpto.Location = New System.Drawing.Point(371, 595)
        Me.lblSSMargenImpto.Name = "lblSSMargenImpto"
        Me.lblSSMargenImpto.Size = New System.Drawing.Size(73, 21)
        Me.lblSSMargenImpto.TabIndex = 245
        Me.lblSSMargenImpto.Text = "0.00"
        Me.lblSSMargenImpto.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.lblSSMargenImpto.Visible = False
        '
        'lblSSCRImpto
        '
        Me.lblSSCRImpto.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.lblSSCRImpto.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSSCRImpto.Location = New System.Drawing.Point(371, 590)
        Me.lblSSCRImpto.Name = "lblSSCRImpto"
        Me.lblSSCRImpto.Size = New System.Drawing.Size(73, 21)
        Me.lblSSCRImpto.TabIndex = 244
        Me.lblSSCRImpto.Text = "0.00"
        Me.lblSSCRImpto.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.lblSSCRImpto.Visible = False
        '
        'lblMargenLiberadoImpto
        '
        Me.lblMargenLiberadoImpto.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.lblMargenLiberadoImpto.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMargenLiberadoImpto.Location = New System.Drawing.Point(20, 652)
        Me.lblMargenLiberadoImpto.Name = "lblMargenLiberadoImpto"
        Me.lblMargenLiberadoImpto.Size = New System.Drawing.Size(58, 21)
        Me.lblMargenLiberadoImpto.TabIndex = 243
        Me.lblMargenLiberadoImpto.Text = "0.00"
        Me.lblMargenLiberadoImpto.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.lblMargenLiberadoImpto.Visible = False
        '
        'lblCostoLiberadoImpto
        '
        Me.lblCostoLiberadoImpto.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.lblCostoLiberadoImpto.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCostoLiberadoImpto.Location = New System.Drawing.Point(20, 625)
        Me.lblCostoLiberadoImpto.Name = "lblCostoLiberadoImpto"
        Me.lblCostoLiberadoImpto.Size = New System.Drawing.Size(58, 21)
        Me.lblCostoLiberadoImpto.TabIndex = 242
        Me.lblCostoLiberadoImpto.Text = "0.00"
        Me.lblCostoLiberadoImpto.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.lblCostoLiberadoImpto.Visible = False
        '
        'lblMargenImpto
        '
        Me.lblMargenImpto.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.lblMargenImpto.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMargenImpto.Location = New System.Drawing.Point(20, 598)
        Me.lblMargenImpto.Name = "lblMargenImpto"
        Me.lblMargenImpto.Size = New System.Drawing.Size(58, 21)
        Me.lblMargenImpto.TabIndex = 241
        Me.lblMargenImpto.Text = "0.00"
        Me.lblMargenImpto.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.lblMargenImpto.Visible = False
        '
        'Label19
        '
        Me.Label19.AutoSize = True
        Me.Label19.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label19.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.Label19.Location = New System.Drawing.Point(23, 44)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(40, 13)
        Me.Label19.TabIndex = 239
        Me.Label19.Text = "Fecha"
        '
        'dtpDia
        '
        Me.dtpDia.CalendarFont = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpDia.CustomFormat = "dd/MM/yyyy"
        Me.dtpDia.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpDia.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.dtpDia.Location = New System.Drawing.Point(163, 41)
        Me.dtpDia.Name = "dtpDia"
        Me.dtpDia.Size = New System.Drawing.Size(90, 21)
        Me.dtpDia.TabIndex = 1
        Me.dtpDia.Tag = "Borrar"
        '
        'txtMotivoEspecial
        '
        Me.txtMotivoEspecial.BackColor = System.Drawing.SystemColors.Window
        Me.txtMotivoEspecial.Enabled = False
        Me.txtMotivoEspecial.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtMotivoEspecial.Location = New System.Drawing.Point(163, 434)
        Me.txtMotivoEspecial.MaxLength = 0
        Me.txtMotivoEspecial.Multiline = True
        Me.txtMotivoEspecial.Name = "txtMotivoEspecial"
        Me.txtMotivoEspecial.Size = New System.Drawing.Size(302, 46)
        Me.txtMotivoEspecial.TabIndex = 22
        Me.txtMotivoEspecial.Tag = "Borrar"
        '
        'lblMotivoEspecialEtiq
        '
        Me.lblMotivoEspecialEtiq.AutoSize = True
        Me.lblMotivoEspecialEtiq.Enabled = False
        Me.lblMotivoEspecialEtiq.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMotivoEspecialEtiq.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.lblMotivoEspecialEtiq.Location = New System.Drawing.Point(21, 454)
        Me.lblMotivoEspecialEtiq.Name = "lblMotivoEspecialEtiq"
        Me.lblMotivoEspecialEtiq.Size = New System.Drawing.Size(94, 13)
        Me.lblMotivoEspecialEtiq.TabIndex = 237
        Me.lblMotivoEspecialEtiq.Text = "Motivo Especial"
        '
        'chkEspecial
        '
        Me.chkEspecial.AutoSize = True
        Me.chkEspecial.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkEspecial.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.chkEspecial.Location = New System.Drawing.Point(24, 434)
        Me.chkEspecial.Name = "chkEspecial"
        Me.chkEspecial.Size = New System.Drawing.Size(71, 17)
        Me.chkEspecial.TabIndex = 21
        Me.chkEspecial.Tag = "Borrar"
        Me.chkEspecial.Text = "Especial"
        Me.chkEspecial.UseVisualStyleBackColor = True
        '
        'cboIDIdioma
        '
        Me.cboIDIdioma.BackColor = System.Drawing.Color.Moccasin
        Me.cboIDIdioma.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboIDIdioma.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboIDIdioma.FormattingEnabled = True
        Me.cboIDIdioma.Location = New System.Drawing.Point(163, 544)
        Me.cboIDIdioma.Name = "cboIDIdioma"
        Me.cboIDIdioma.Size = New System.Drawing.Size(191, 21)
        Me.cboIDIdioma.TabIndex = 24
        Me.cboIDIdioma.Tag = "Borrar"
        '
        'lblIdiomaEtiq
        '
        Me.lblIdiomaEtiq.AutoSize = True
        Me.lblIdiomaEtiq.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblIdiomaEtiq.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.lblIdiomaEtiq.Location = New System.Drawing.Point(21, 547)
        Me.lblIdiomaEtiq.Name = "lblIdiomaEtiq"
        Me.lblIdiomaEtiq.Size = New System.Drawing.Size(47, 13)
        Me.lblIdiomaEtiq.TabIndex = 233
        Me.lblIdiomaEtiq.Text = "Idioma"
        '
        'lblSTTotalEtiq
        '
        Me.lblSTTotalEtiq.AutoSize = True
        Me.lblSTTotalEtiq.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSTTotalEtiq.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.lblSTTotalEtiq.Location = New System.Drawing.Point(456, 626)
        Me.lblSTTotalEtiq.Name = "lblSTTotalEtiq"
        Me.lblSTTotalEtiq.Size = New System.Drawing.Size(142, 13)
        Me.lblSTTotalEtiq.TabIndex = 232
        Me.lblSTTotalEtiq.Text = "Suplemento Triple Total"
        Me.lblSTTotalEtiq.Visible = False
        '
        'lblSTMargenEtiq
        '
        Me.lblSTMargenEtiq.AutoSize = True
        Me.lblSTMargenEtiq.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSTMargenEtiq.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.lblSTMargenEtiq.Location = New System.Drawing.Point(456, 598)
        Me.lblSTMargenEtiq.Name = "lblSTMargenEtiq"
        Me.lblSTMargenEtiq.Size = New System.Drawing.Size(156, 13)
        Me.lblSTMargenEtiq.TabIndex = 231
        Me.lblSTMargenEtiq.Text = "Suplemento Triple Margen"
        Me.lblSTMargenEtiq.Visible = False
        '
        'lblSTCREtiq
        '
        Me.lblSTCREtiq.AutoSize = True
        Me.lblSTCREtiq.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSTCREtiq.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.lblSTCREtiq.Location = New System.Drawing.Point(456, 593)
        Me.lblSTCREtiq.Name = "lblSTCREtiq"
        Me.lblSTCREtiq.Size = New System.Drawing.Size(173, 13)
        Me.lblSTCREtiq.TabIndex = 230
        Me.lblSTCREtiq.Text = "Suplemento Triple Costo Real"
        Me.lblSTCREtiq.Visible = False
        '
        'lblSSTotalEtiq
        '
        Me.lblSSTotalEtiq.AutoSize = True
        Me.lblSSTotalEtiq.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSSTotalEtiq.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.lblSSTotalEtiq.Location = New System.Drawing.Point(97, 680)
        Me.lblSSTotalEtiq.Name = "lblSSTotalEtiq"
        Me.lblSSTotalEtiq.Size = New System.Drawing.Size(148, 13)
        Me.lblSSTotalEtiq.TabIndex = 229
        Me.lblSSTotalEtiq.Text = "Suplemento Simple Total"
        Me.lblSSTotalEtiq.Visible = False
        '
        'lblSSMLEtiq
        '
        Me.lblSSMLEtiq.AutoSize = True
        Me.lblSSMLEtiq.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSSMLEtiq.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.lblSSMLEtiq.Location = New System.Drawing.Point(97, 653)
        Me.lblSSMLEtiq.Name = "lblSSMLEtiq"
        Me.lblSSMLEtiq.Size = New System.Drawing.Size(207, 13)
        Me.lblSSMLEtiq.TabIndex = 228
        Me.lblSSMLEtiq.Text = "Suplemento Simple Monto Liberado"
        Me.lblSSMLEtiq.Visible = False
        '
        'lblSSCLEtiq
        '
        Me.lblSSCLEtiq.AutoSize = True
        Me.lblSSCLEtiq.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSSCLEtiq.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.lblSSCLEtiq.Location = New System.Drawing.Point(97, 626)
        Me.lblSSCLEtiq.Name = "lblSSCLEtiq"
        Me.lblSSCLEtiq.Size = New System.Drawing.Size(203, 13)
        Me.lblSSCLEtiq.TabIndex = 227
        Me.lblSSCLEtiq.Text = "Suplemento Simple Costo Liberado"
        Me.lblSSCLEtiq.Visible = False
        '
        'lblSSMargenEtiq
        '
        Me.lblSSMargenEtiq.AutoSize = True
        Me.lblSSMargenEtiq.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSSMargenEtiq.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.lblSSMargenEtiq.Location = New System.Drawing.Point(97, 598)
        Me.lblSSMargenEtiq.Name = "lblSSMargenEtiq"
        Me.lblSSMargenEtiq.Size = New System.Drawing.Size(162, 13)
        Me.lblSSMargenEtiq.TabIndex = 226
        Me.lblSSMargenEtiq.Text = "Suplemento Simple Margen"
        Me.lblSSMargenEtiq.Visible = False
        '
        'lblSSCREtiq
        '
        Me.lblSSCREtiq.AutoSize = True
        Me.lblSSCREtiq.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSSCREtiq.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.lblSSCREtiq.Location = New System.Drawing.Point(97, 593)
        Me.lblSSCREtiq.Name = "lblSSCREtiq"
        Me.lblSSCREtiq.Size = New System.Drawing.Size(179, 13)
        Me.lblSSCREtiq.TabIndex = 225
        Me.lblSSCREtiq.Text = "Suplemento Simple Costo Real"
        Me.lblSSCREtiq.Visible = False
        '
        'lblTotalEtiq
        '
        Me.lblTotalEtiq.AutoSize = True
        Me.lblTotalEtiq.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTotalEtiq.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.lblTotalEtiq.Location = New System.Drawing.Point(23, 395)
        Me.lblTotalEtiq.Name = "lblTotalEtiq"
        Me.lblTotalEtiq.Size = New System.Drawing.Size(36, 13)
        Me.lblTotalEtiq.TabIndex = 224
        Me.lblTotalEtiq.Text = "Total"
        '
        'lblMargenLiberadoEtiq
        '
        Me.lblMargenLiberadoEtiq.AutoSize = True
        Me.lblMargenLiberadoEtiq.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMargenLiberadoEtiq.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.lblMargenLiberadoEtiq.Location = New System.Drawing.Point(451, 331)
        Me.lblMargenLiberadoEtiq.Name = "lblMargenLiberadoEtiq"
        Me.lblMargenLiberadoEtiq.Size = New System.Drawing.Size(102, 13)
        Me.lblMargenLiberadoEtiq.TabIndex = 222
        Me.lblMargenLiberadoEtiq.Text = "Margen Liberado"
        Me.lblMargenLiberadoEtiq.Visible = False
        '
        'lblCostoLiberadoEtiq
        '
        Me.lblCostoLiberadoEtiq.AutoSize = True
        Me.lblCostoLiberadoEtiq.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCostoLiberadoEtiq.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.lblCostoLiberadoEtiq.Location = New System.Drawing.Point(23, 295)
        Me.lblCostoLiberadoEtiq.Name = "lblCostoLiberadoEtiq"
        Me.lblCostoLiberadoEtiq.Size = New System.Drawing.Size(91, 13)
        Me.lblCostoLiberadoEtiq.TabIndex = 221
        Me.lblCostoLiberadoEtiq.Text = "Costo Liberado"
        '
        'lblMargenEtiq
        '
        Me.lblMargenEtiq.AutoSize = True
        Me.lblMargenEtiq.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMargenEtiq.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.lblMargenEtiq.Location = New System.Drawing.Point(22, 345)
        Me.lblMargenEtiq.Name = "lblMargenEtiq"
        Me.lblMargenEtiq.Size = New System.Drawing.Size(50, 13)
        Me.lblMargenEtiq.TabIndex = 220
        Me.lblMargenEtiq.Text = "Margen"
        '
        'lblCostoRealEtiq
        '
        Me.lblCostoRealEtiq.AutoSize = True
        Me.lblCostoRealEtiq.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCostoRealEtiq.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.lblCostoRealEtiq.Location = New System.Drawing.Point(23, 268)
        Me.lblCostoRealEtiq.Name = "lblCostoRealEtiq"
        Me.lblCostoRealEtiq.Size = New System.Drawing.Size(68, 13)
        Me.lblCostoRealEtiq.TabIndex = 219
        Me.lblCostoRealEtiq.Text = "Costo Neto"
        '
        'txtSSCR
        '
        Me.txtSSCR.BackColor = System.Drawing.SystemColors.Window
        Me.txtSSCR.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtSSCR.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtSSCR.Location = New System.Drawing.Point(289, 590)
        Me.txtSSCR.MaxLength = 8
        Me.txtSSCR.Name = "txtSSCR"
        Me.txtSSCR.ReadOnly = True
        Me.txtSSCR.Size = New System.Drawing.Size(72, 21)
        Me.txtSSCR.TabIndex = 13
        Me.txtSSCR.Tag = "Borrar"
        Me.txtSSCR.Text = "0.00"
        Me.txtSSCR.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtSSCR.Visible = False
        '
        'txtSTMargen
        '
        Me.txtSTMargen.BackColor = System.Drawing.SystemColors.Window
        Me.txtSTMargen.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtSTMargen.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtSTMargen.Location = New System.Drawing.Point(641, 595)
        Me.txtSTMargen.MaxLength = 8
        Me.txtSTMargen.Name = "txtSTMargen"
        Me.txtSTMargen.ReadOnly = True
        Me.txtSTMargen.Size = New System.Drawing.Size(72, 21)
        Me.txtSTMargen.TabIndex = 19
        Me.txtSTMargen.Tag = "Borrar"
        Me.txtSTMargen.Text = "0.00"
        Me.txtSTMargen.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtSTMargen.Visible = False
        '
        'txtSSCL
        '
        Me.txtSSCL.BackColor = System.Drawing.SystemColors.Window
        Me.txtSSCL.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtSSCL.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtSSCL.Location = New System.Drawing.Point(289, 623)
        Me.txtSSCL.MaxLength = 8
        Me.txtSSCL.Name = "txtSSCL"
        Me.txtSSCL.ReadOnly = True
        Me.txtSSCL.Size = New System.Drawing.Size(72, 21)
        Me.txtSSCL.TabIndex = 15
        Me.txtSSCL.Tag = "Borrar"
        Me.txtSSCL.Text = "0.00"
        Me.txtSSCL.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtSSCL.Visible = False
        '
        'txtSSMargen
        '
        Me.txtSSMargen.BackColor = System.Drawing.SystemColors.Window
        Me.txtSSMargen.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtSSMargen.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtSSMargen.Location = New System.Drawing.Point(289, 595)
        Me.txtSSMargen.MaxLength = 8
        Me.txtSSMargen.Name = "txtSSMargen"
        Me.txtSSMargen.ReadOnly = True
        Me.txtSSMargen.Size = New System.Drawing.Size(72, 21)
        Me.txtSSMargen.TabIndex = 14
        Me.txtSSMargen.Tag = "Borrar"
        Me.txtSSMargen.Text = "0.00"
        Me.txtSSMargen.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtSSMargen.Visible = False
        '
        'txtSTCR
        '
        Me.txtSTCR.BackColor = System.Drawing.SystemColors.Window
        Me.txtSTCR.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtSTCR.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtSTCR.Location = New System.Drawing.Point(641, 590)
        Me.txtSTCR.MaxLength = 8
        Me.txtSTCR.Name = "txtSTCR"
        Me.txtSTCR.ReadOnly = True
        Me.txtSTCR.Size = New System.Drawing.Size(72, 21)
        Me.txtSTCR.TabIndex = 18
        Me.txtSTCR.Tag = "Borrar"
        Me.txtSTCR.Text = "0.00"
        Me.txtSTCR.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtSTCR.Visible = False
        '
        'txtSTTotal
        '
        Me.txtSTTotal.BackColor = System.Drawing.SystemColors.Window
        Me.txtSTTotal.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtSTTotal.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtSTTotal.Location = New System.Drawing.Point(641, 623)
        Me.txtSTTotal.MaxLength = 8
        Me.txtSTTotal.Name = "txtSTTotal"
        Me.txtSTTotal.ReadOnly = True
        Me.txtSTTotal.Size = New System.Drawing.Size(72, 21)
        Me.txtSTTotal.TabIndex = 20
        Me.txtSTTotal.Tag = "Borrar"
        Me.txtSTTotal.Text = "0.00"
        Me.txtSTTotal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtSTTotal.Visible = False
        '
        'txtSSTotal
        '
        Me.txtSSTotal.BackColor = System.Drawing.SystemColors.Window
        Me.txtSSTotal.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtSSTotal.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtSSTotal.Location = New System.Drawing.Point(289, 677)
        Me.txtSSTotal.MaxLength = 8
        Me.txtSSTotal.Name = "txtSSTotal"
        Me.txtSSTotal.ReadOnly = True
        Me.txtSSTotal.Size = New System.Drawing.Size(72, 21)
        Me.txtSSTotal.TabIndex = 17
        Me.txtSSTotal.Tag = "Borrar"
        Me.txtSSTotal.Text = "0.00"
        Me.txtSSTotal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtSSTotal.Visible = False
        '
        'txtSSMargenLiberado
        '
        Me.txtSSMargenLiberado.BackColor = System.Drawing.SystemColors.Window
        Me.txtSSMargenLiberado.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtSSMargenLiberado.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtSSMargenLiberado.Location = New System.Drawing.Point(289, 650)
        Me.txtSSMargenLiberado.MaxLength = 8
        Me.txtSSMargenLiberado.Name = "txtSSMargenLiberado"
        Me.txtSSMargenLiberado.ReadOnly = True
        Me.txtSSMargenLiberado.Size = New System.Drawing.Size(72, 21)
        Me.txtSSMargenLiberado.TabIndex = 16
        Me.txtSSMargenLiberado.Tag = "Borrar"
        Me.txtSSMargenLiberado.Text = "0.00"
        Me.txtSSMargenLiberado.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtSSMargenLiberado.Visible = False
        '
        'txtMargenAplicado
        '
        Me.txtMargenAplicado.BackColor = System.Drawing.SystemColors.Window
        Me.txtMargenAplicado.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtMargenAplicado.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtMargenAplicado.Location = New System.Drawing.Point(109, 342)
        Me.txtMargenAplicado.MaxLength = 8
        Me.txtMargenAplicado.Name = "txtMargenAplicado"
        Me.txtMargenAplicado.ReadOnly = True
        Me.txtMargenAplicado.Size = New System.Drawing.Size(47, 21)
        Me.txtMargenAplicado.TabIndex = 11
        Me.txtMargenAplicado.Tag = "Borrar"
        Me.txtMargenAplicado.Text = "0.00"
        Me.txtMargenAplicado.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtCostoReal
        '
        Me.txtCostoReal.BackColor = System.Drawing.SystemColors.Window
        Me.txtCostoReal.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtCostoReal.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtCostoReal.Location = New System.Drawing.Point(163, 265)
        Me.txtCostoReal.MaxLength = 8
        Me.txtCostoReal.Name = "txtCostoReal"
        Me.txtCostoReal.ReadOnly = True
        Me.txtCostoReal.Size = New System.Drawing.Size(71, 21)
        Me.txtCostoReal.TabIndex = 7
        Me.txtCostoReal.Tag = "Borrar"
        Me.txtCostoReal.Text = "0.0000"
        Me.txtCostoReal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'cboCiudad
        '
        Me.cboCiudad.BackColor = System.Drawing.Color.Moccasin
        Me.cboCiudad.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboCiudad.Enabled = False
        Me.cboCiudad.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboCiudad.FormattingEnabled = True
        Me.cboCiudad.Location = New System.Drawing.Point(163, 93)
        Me.cboCiudad.Name = "cboCiudad"
        Me.cboCiudad.Size = New System.Drawing.Size(190, 21)
        Me.cboCiudad.TabIndex = 3
        Me.cboCiudad.Tag = "Borrar"
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Enabled = False
        Me.Label12.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label12.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.Label12.Location = New System.Drawing.Point(23, 96)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(45, 13)
        Me.Label12.TabIndex = 204
        Me.Label12.Text = "Ciudad"
        '
        'cboPais
        '
        Me.cboPais.BackColor = System.Drawing.Color.Moccasin
        Me.cboPais.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboPais.Enabled = False
        Me.cboPais.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboPais.FormattingEnabled = True
        Me.cboPais.Location = New System.Drawing.Point(163, 69)
        Me.cboPais.Name = "cboPais"
        Me.cboPais.Size = New System.Drawing.Size(190, 21)
        Me.cboPais.TabIndex = 2
        Me.cboPais.Tag = "Borrar"
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Enabled = False
        Me.Label13.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label13.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.Label13.Location = New System.Drawing.Point(23, 69)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(30, 13)
        Me.Label13.TabIndex = 203
        Me.Label13.Text = "País"
        '
        'cboTipoProv
        '
        Me.cboTipoProv.BackColor = System.Drawing.Color.Moccasin
        Me.cboTipoProv.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboTipoProv.Enabled = False
        Me.cboTipoProv.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboTipoProv.FormattingEnabled = True
        Me.cboTipoProv.Location = New System.Drawing.Point(163, 120)
        Me.cboTipoProv.Name = "cboTipoProv"
        Me.cboTipoProv.Size = New System.Drawing.Size(190, 21)
        Me.cboTipoProv.TabIndex = 4
        Me.cboTipoProv.Tag = "Borrar"
        '
        'Label22
        '
        Me.Label22.AutoSize = True
        Me.Label22.Enabled = False
        Me.Label22.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label22.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.Label22.Location = New System.Drawing.Point(23, 125)
        Me.Label22.Name = "Label22"
        Me.Label22.Size = New System.Drawing.Size(93, 13)
        Me.Label22.TabIndex = 165
        Me.Label22.Text = "Tipo Proveedor"
        '
        'btnProveedor
        '
        Me.btnProveedor.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnProveedor.Image = Global.SETours.My.Resources.Resources.editarpng
        Me.btnProveedor.Location = New System.Drawing.Point(931, 553)
        Me.btnProveedor.Name = "btnProveedor"
        Me.btnProveedor.Size = New System.Drawing.Size(28, 22)
        Me.btnProveedor.TabIndex = 6
        Me.ToolTip1.SetToolTip(Me.btnProveedor, "Consultar Servicio/Proveedor ")
        Me.btnProveedor.UseVisualStyleBackColor = True
        Me.btnProveedor.Visible = False
        '
        'lblProveedor
        '
        Me.lblProveedor.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.lblProveedor.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblProveedor.Location = New System.Drawing.Point(163, 149)
        Me.lblProveedor.Name = "lblProveedor"
        Me.lblProveedor.Size = New System.Drawing.Size(342, 22)
        Me.lblProveedor.TabIndex = 5
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.Label1.Location = New System.Drawing.Point(23, 154)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(66, 13)
        Me.Label1.TabIndex = 163
        Me.Label1.Text = "Proveedor"
        '
        'lblTitulo
        '
        Me.lblTitulo.BackColor = System.Drawing.SystemColors.ControlDarkDark
        Me.lblTitulo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.lblTitulo.Dock = System.Windows.Forms.DockStyle.Top
        Me.lblTitulo.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTitulo.ForeColor = System.Drawing.Color.White
        Me.lblTitulo.Location = New System.Drawing.Point(3, 17)
        Me.lblTitulo.Name = "lblTitulo"
        Me.lblTitulo.Size = New System.Drawing.Size(1126, 22)
        Me.lblTitulo.TabIndex = 108
        Me.lblTitulo.Text = "Nuevo"
        '
        'chkCena
        '
        Me.chkCena.AutoSize = True
        Me.chkCena.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkCena.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.chkCena.Location = New System.Drawing.Point(566, 459)
        Me.chkCena.Name = "chkCena"
        Me.chkCena.Size = New System.Drawing.Size(54, 17)
        Me.chkCena.TabIndex = 26
        Me.chkCena.Tag = "Borrar"
        Me.chkCena.Text = "Cena"
        Me.chkCena.UseVisualStyleBackColor = True
        '
        'TabPage2
        '
        Me.TabPage2.Controls.Add(Me.grbQuiebres)
        Me.TabPage2.Location = New System.Drawing.Point(4, 22)
        Me.TabPage2.Name = "TabPage2"
        Me.TabPage2.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage2.Size = New System.Drawing.Size(1138, 584)
        Me.TabPage2.TabIndex = 1
        Me.TabPage2.Text = "Quiebres"
        Me.TabPage2.UseVisualStyleBackColor = True
        '
        'grbQuiebres
        '
        Me.grbQuiebres.BackColor = System.Drawing.Color.White
        Me.grbQuiebres.Controls.Add(Me.GroupBox2)
        Me.grbQuiebres.Controls.Add(Me.lblRedondeoQ)
        Me.grbQuiebres.Controls.Add(Me.Label5)
        Me.grbQuiebres.Controls.Add(Me.GroupBox1)
        Me.grbQuiebres.Controls.Add(Me.lblSTTotalQ)
        Me.grbQuiebres.Controls.Add(Me.lblSTMargenQ)
        Me.grbQuiebres.Controls.Add(Me.lblSTCRQ)
        Me.grbQuiebres.Controls.Add(Me.lblSSTotalQ)
        Me.grbQuiebres.Controls.Add(Me.lblSSMLQ)
        Me.grbQuiebres.Controls.Add(Me.lblSSCLQ)
        Me.grbQuiebres.Controls.Add(Me.lblSSMargenQ)
        Me.grbQuiebres.Controls.Add(Me.lblSSCRQ)
        Me.grbQuiebres.Controls.Add(Me.lblTotalQ)
        Me.grbQuiebres.Controls.Add(Me.lblMargenLiberadoQ)
        Me.grbQuiebres.Controls.Add(Me.lblCostoLiberadoQ)
        Me.grbQuiebres.Controls.Add(Me.lblMargenQ)
        Me.grbQuiebres.Controls.Add(Me.lblTotImptoQ)
        Me.grbQuiebres.Controls.Add(Me.Label27)
        Me.grbQuiebres.Controls.Add(Me.Label24)
        Me.grbQuiebres.Controls.Add(Me.Label25)
        Me.grbQuiebres.Controls.Add(Me.Label26)
        Me.grbQuiebres.Controls.Add(Me.lblSTMargenImptoQ)
        Me.grbQuiebres.Controls.Add(Me.lblSTCRImptoQ)
        Me.grbQuiebres.Controls.Add(Me.lblSSMLImptoQ)
        Me.grbQuiebres.Controls.Add(Me.lblSSCLImptoQ)
        Me.grbQuiebres.Controls.Add(Me.lblSSMargenImptoQ)
        Me.grbQuiebres.Controls.Add(Me.lblSSCRImptoQ)
        Me.grbQuiebres.Controls.Add(Me.lblMargenLiberadoImptoQ)
        Me.grbQuiebres.Controls.Add(Me.lblCostoLiberadoImptoQ)
        Me.grbQuiebres.Controls.Add(Me.lblMargenImptoQ)
        Me.grbQuiebres.Controls.Add(Me.lblCostoRealImptoQ)
        Me.grbQuiebres.Controls.Add(Me.Label37)
        Me.grbQuiebres.Controls.Add(Me.Label38)
        Me.grbQuiebres.Controls.Add(Me.Label39)
        Me.grbQuiebres.Controls.Add(Me.Label40)
        Me.grbQuiebres.Controls.Add(Me.Label41)
        Me.grbQuiebres.Controls.Add(Me.Label42)
        Me.grbQuiebres.Controls.Add(Me.Label43)
        Me.grbQuiebres.Controls.Add(Me.Label44)
        Me.grbQuiebres.Controls.Add(Me.Label45)
        Me.grbQuiebres.Controls.Add(Me.Label47)
        Me.grbQuiebres.Controls.Add(Me.Label48)
        Me.grbQuiebres.Controls.Add(Me.Label49)
        Me.grbQuiebres.Controls.Add(Me.Label50)
        Me.grbQuiebres.Controls.Add(Me.txtMargenAplicadoQ)
        Me.grbQuiebres.Controls.Add(Me.txtCostoRealQ)
        Me.grbQuiebres.Controls.Add(Me.lblTitulo2)
        Me.grbQuiebres.Controls.Add(Me.dgvQui)
        Me.grbQuiebres.Dock = System.Windows.Forms.DockStyle.Fill
        Me.grbQuiebres.Location = New System.Drawing.Point(3, 3)
        Me.grbQuiebres.Name = "grbQuiebres"
        Me.grbQuiebres.Size = New System.Drawing.Size(1132, 578)
        Me.grbQuiebres.TabIndex = 0
        Me.grbQuiebres.TabStop = False
        '
        'GroupBox2
        '
        Me.GroupBox2.Location = New System.Drawing.Point(559, 235)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(4, 200)
        Me.GroupBox2.TabIndex = 352
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "GroupBox2"
        '
        'lblRedondeoQ
        '
        Me.lblRedondeoQ.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.lblRedondeoQ.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblRedondeoQ.Location = New System.Drawing.Point(170, 382)
        Me.lblRedondeoQ.Name = "lblRedondeoQ"
        Me.lblRedondeoQ.Size = New System.Drawing.Size(73, 21)
        Me.lblRedondeoQ.TabIndex = 351
        Me.lblRedondeoQ.Text = "0.0000"
        Me.lblRedondeoQ.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.Label5.Location = New System.Drawing.Point(18, 386)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(64, 13)
        Me.Label5.TabIndex = 350
        Me.Label5.Text = "Redondeo"
        '
        'GroupBox1
        '
        Me.GroupBox1.Location = New System.Drawing.Point(250, 235)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(4, 200)
        Me.GroupBox1.TabIndex = 349
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "GroupBox1"
        '
        'lblSTTotalQ
        '
        Me.lblSTTotalQ.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.lblSTTotalQ.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSTTotalQ.Location = New System.Drawing.Point(754, 298)
        Me.lblSTTotalQ.Name = "lblSTTotalQ"
        Me.lblSTTotalQ.Size = New System.Drawing.Size(73, 21)
        Me.lblSTTotalQ.TabIndex = 307
        Me.lblSTTotalQ.Text = "0.0000"
        Me.lblSTTotalQ.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblSTMargenQ
        '
        Me.lblSTMargenQ.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.lblSTMargenQ.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSTMargenQ.Location = New System.Drawing.Point(754, 270)
        Me.lblSTMargenQ.Name = "lblSTMargenQ"
        Me.lblSTMargenQ.Size = New System.Drawing.Size(73, 21)
        Me.lblSTMargenQ.TabIndex = 306
        Me.lblSTMargenQ.Text = "0.0000"
        Me.lblSTMargenQ.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblSTCRQ
        '
        Me.lblSTCRQ.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.lblSTCRQ.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSTCRQ.Location = New System.Drawing.Point(754, 243)
        Me.lblSTCRQ.Name = "lblSTCRQ"
        Me.lblSTCRQ.Size = New System.Drawing.Size(73, 21)
        Me.lblSTCRQ.TabIndex = 305
        Me.lblSTCRQ.Text = "0.0000"
        Me.lblSTCRQ.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblSSTotalQ
        '
        Me.lblSSTotalQ.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.lblSSTotalQ.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSSTotalQ.Location = New System.Drawing.Point(480, 353)
        Me.lblSSTotalQ.Name = "lblSSTotalQ"
        Me.lblSSTotalQ.Size = New System.Drawing.Size(73, 21)
        Me.lblSSTotalQ.TabIndex = 304
        Me.lblSSTotalQ.Text = "0.0000"
        Me.lblSSTotalQ.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblSSMLQ
        '
        Me.lblSSMLQ.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.lblSSMLQ.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSSMLQ.Location = New System.Drawing.Point(480, 323)
        Me.lblSSMLQ.Name = "lblSSMLQ"
        Me.lblSSMLQ.Size = New System.Drawing.Size(73, 21)
        Me.lblSSMLQ.TabIndex = 303
        Me.lblSSMLQ.Text = "0.0000"
        Me.lblSSMLQ.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblSSCLQ
        '
        Me.lblSSCLQ.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.lblSSCLQ.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSSCLQ.Location = New System.Drawing.Point(480, 296)
        Me.lblSSCLQ.Name = "lblSSCLQ"
        Me.lblSSCLQ.Size = New System.Drawing.Size(73, 21)
        Me.lblSSCLQ.TabIndex = 302
        Me.lblSSCLQ.Text = "0.0000"
        Me.lblSSCLQ.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblSSMargenQ
        '
        Me.lblSSMargenQ.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.lblSSMargenQ.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSSMargenQ.Location = New System.Drawing.Point(480, 269)
        Me.lblSSMargenQ.Name = "lblSSMargenQ"
        Me.lblSSMargenQ.Size = New System.Drawing.Size(73, 21)
        Me.lblSSMargenQ.TabIndex = 301
        Me.lblSSMargenQ.Text = "0.0000"
        Me.lblSSMargenQ.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblSSCRQ
        '
        Me.lblSSCRQ.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.lblSSCRQ.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSSCRQ.Location = New System.Drawing.Point(480, 243)
        Me.lblSSCRQ.Name = "lblSSCRQ"
        Me.lblSSCRQ.Size = New System.Drawing.Size(73, 21)
        Me.lblSSCRQ.TabIndex = 300
        Me.lblSSCRQ.Text = "0.0000"
        Me.lblSSCRQ.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblTotalQ
        '
        Me.lblTotalQ.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.lblTotalQ.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTotalQ.Location = New System.Drawing.Point(171, 409)
        Me.lblTotalQ.Name = "lblTotalQ"
        Me.lblTotalQ.Size = New System.Drawing.Size(73, 21)
        Me.lblTotalQ.TabIndex = 299
        Me.lblTotalQ.Text = "0.0000"
        Me.lblTotalQ.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblMargenLiberadoQ
        '
        Me.lblMargenLiberadoQ.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.lblMargenLiberadoQ.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMargenLiberadoQ.Location = New System.Drawing.Point(171, 325)
        Me.lblMargenLiberadoQ.Name = "lblMargenLiberadoQ"
        Me.lblMargenLiberadoQ.Size = New System.Drawing.Size(73, 21)
        Me.lblMargenLiberadoQ.TabIndex = 298
        Me.lblMargenLiberadoQ.Text = "0.0000"
        Me.lblMargenLiberadoQ.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblCostoLiberadoQ
        '
        Me.lblCostoLiberadoQ.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.lblCostoLiberadoQ.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCostoLiberadoQ.Location = New System.Drawing.Point(171, 272)
        Me.lblCostoLiberadoQ.Name = "lblCostoLiberadoQ"
        Me.lblCostoLiberadoQ.Size = New System.Drawing.Size(73, 21)
        Me.lblCostoLiberadoQ.TabIndex = 297
        Me.lblCostoLiberadoQ.Text = "0.0000"
        Me.lblCostoLiberadoQ.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblMargenQ
        '
        Me.lblMargenQ.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.lblMargenQ.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMargenQ.Location = New System.Drawing.Point(171, 353)
        Me.lblMargenQ.Name = "lblMargenQ"
        Me.lblMargenQ.Size = New System.Drawing.Size(73, 21)
        Me.lblMargenQ.TabIndex = 296
        Me.lblMargenQ.Text = "0.0000"
        Me.lblMargenQ.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblTotImptoQ
        '
        Me.lblTotImptoQ.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.lblTotImptoQ.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTotImptoQ.Location = New System.Drawing.Point(171, 298)
        Me.lblTotImptoQ.Name = "lblTotImptoQ"
        Me.lblTotImptoQ.Size = New System.Drawing.Size(73, 21)
        Me.lblTotImptoQ.TabIndex = 295
        Me.lblTotImptoQ.Text = "0.0000"
        Me.lblTotImptoQ.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label27
        '
        Me.Label27.AutoSize = True
        Me.Label27.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label27.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.Label27.Location = New System.Drawing.Point(18, 302)
        Me.Label27.Name = "Label27"
        Me.Label27.Size = New System.Drawing.Size(68, 13)
        Me.Label27.TabIndex = 294
        Me.Label27.Text = "Impuestos"
        '
        'Label24
        '
        Me.Label24.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label24.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.Label24.Location = New System.Drawing.Point(990, 232)
        Me.Label24.Name = "Label24"
        Me.Label24.Size = New System.Drawing.Size(73, 13)
        Me.Label24.TabIndex = 293
        Me.Label24.Text = "Impuestos"
        Me.Label24.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label25
        '
        Me.Label25.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label25.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.Label25.Location = New System.Drawing.Point(477, 443)
        Me.Label25.Name = "Label25"
        Me.Label25.Size = New System.Drawing.Size(76, 16)
        Me.Label25.TabIndex = 292
        Me.Label25.Text = "Impuestos"
        Me.Label25.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.Label25.Visible = False
        '
        'Label26
        '
        Me.Label26.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label26.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.Label26.Location = New System.Drawing.Point(172, 440)
        Me.Label26.Name = "Label26"
        Me.Label26.Size = New System.Drawing.Size(71, 19)
        Me.Label26.TabIndex = 291
        Me.Label26.Text = "Impuestos"
        Me.Label26.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.Label26.Visible = False
        '
        'lblSTMargenImptoQ
        '
        Me.lblSTMargenImptoQ.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.lblSTMargenImptoQ.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSTMargenImptoQ.Location = New System.Drawing.Point(990, 280)
        Me.lblSTMargenImptoQ.Name = "lblSTMargenImptoQ"
        Me.lblSTMargenImptoQ.Size = New System.Drawing.Size(73, 21)
        Me.lblSTMargenImptoQ.TabIndex = 290
        Me.lblSTMargenImptoQ.Text = "0.00"
        Me.lblSTMargenImptoQ.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblSTCRImptoQ
        '
        Me.lblSTCRImptoQ.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.lblSTCRImptoQ.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSTCRImptoQ.Location = New System.Drawing.Point(990, 254)
        Me.lblSTCRImptoQ.Name = "lblSTCRImptoQ"
        Me.lblSTCRImptoQ.Size = New System.Drawing.Size(73, 21)
        Me.lblSTCRImptoQ.TabIndex = 289
        Me.lblSTCRImptoQ.Text = "0.00"
        Me.lblSTCRImptoQ.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblSSMLImptoQ
        '
        Me.lblSSMLImptoQ.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.lblSSMLImptoQ.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSSMLImptoQ.Location = New System.Drawing.Point(392, 517)
        Me.lblSSMLImptoQ.Name = "lblSSMLImptoQ"
        Me.lblSSMLImptoQ.Size = New System.Drawing.Size(73, 21)
        Me.lblSSMLImptoQ.TabIndex = 288
        Me.lblSSMLImptoQ.Text = "0.00"
        Me.lblSSMLImptoQ.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.lblSSMLImptoQ.Visible = False
        '
        'lblSSCLImptoQ
        '
        Me.lblSSCLImptoQ.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.lblSSCLImptoQ.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSSCLImptoQ.Location = New System.Drawing.Point(471, 517)
        Me.lblSSCLImptoQ.Name = "lblSSCLImptoQ"
        Me.lblSSCLImptoQ.Size = New System.Drawing.Size(73, 21)
        Me.lblSSCLImptoQ.TabIndex = 287
        Me.lblSSCLImptoQ.Text = "0.0000"
        Me.lblSSCLImptoQ.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.lblSSCLImptoQ.Visible = False
        '
        'lblSSMargenImptoQ
        '
        Me.lblSSMargenImptoQ.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.lblSSMargenImptoQ.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSSMargenImptoQ.Location = New System.Drawing.Point(471, 490)
        Me.lblSSMargenImptoQ.Name = "lblSSMargenImptoQ"
        Me.lblSSMargenImptoQ.Size = New System.Drawing.Size(73, 21)
        Me.lblSSMargenImptoQ.TabIndex = 286
        Me.lblSSMargenImptoQ.Text = "0.0000"
        Me.lblSSMargenImptoQ.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.lblSSMargenImptoQ.Visible = False
        '
        'lblSSCRImptoQ
        '
        Me.lblSSCRImptoQ.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.lblSSCRImptoQ.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSSCRImptoQ.Location = New System.Drawing.Point(471, 464)
        Me.lblSSCRImptoQ.Name = "lblSSCRImptoQ"
        Me.lblSSCRImptoQ.Size = New System.Drawing.Size(73, 21)
        Me.lblSSCRImptoQ.TabIndex = 285
        Me.lblSSCRImptoQ.Text = "0.0000"
        Me.lblSSCRImptoQ.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.lblSSCRImptoQ.Visible = False
        '
        'lblMargenLiberadoImptoQ
        '
        Me.lblMargenLiberadoImptoQ.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.lblMargenLiberadoImptoQ.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMargenLiberadoImptoQ.Location = New System.Drawing.Point(91, 516)
        Me.lblMargenLiberadoImptoQ.Name = "lblMargenLiberadoImptoQ"
        Me.lblMargenLiberadoImptoQ.Size = New System.Drawing.Size(73, 21)
        Me.lblMargenLiberadoImptoQ.TabIndex = 284
        Me.lblMargenLiberadoImptoQ.Text = "0.00"
        Me.lblMargenLiberadoImptoQ.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.lblMargenLiberadoImptoQ.Visible = False
        '
        'lblCostoLiberadoImptoQ
        '
        Me.lblCostoLiberadoImptoQ.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.lblCostoLiberadoImptoQ.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCostoLiberadoImptoQ.Location = New System.Drawing.Point(170, 516)
        Me.lblCostoLiberadoImptoQ.Name = "lblCostoLiberadoImptoQ"
        Me.lblCostoLiberadoImptoQ.Size = New System.Drawing.Size(73, 21)
        Me.lblCostoLiberadoImptoQ.TabIndex = 283
        Me.lblCostoLiberadoImptoQ.Text = "0.0000"
        Me.lblCostoLiberadoImptoQ.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.lblCostoLiberadoImptoQ.Visible = False
        '
        'lblMargenImptoQ
        '
        Me.lblMargenImptoQ.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.lblMargenImptoQ.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMargenImptoQ.Location = New System.Drawing.Point(170, 489)
        Me.lblMargenImptoQ.Name = "lblMargenImptoQ"
        Me.lblMargenImptoQ.Size = New System.Drawing.Size(73, 21)
        Me.lblMargenImptoQ.TabIndex = 282
        Me.lblMargenImptoQ.Text = "0.0000"
        Me.lblMargenImptoQ.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.lblMargenImptoQ.Visible = False
        '
        'lblCostoRealImptoQ
        '
        Me.lblCostoRealImptoQ.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.lblCostoRealImptoQ.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCostoRealImptoQ.Location = New System.Drawing.Point(170, 462)
        Me.lblCostoRealImptoQ.Name = "lblCostoRealImptoQ"
        Me.lblCostoRealImptoQ.Size = New System.Drawing.Size(73, 21)
        Me.lblCostoRealImptoQ.TabIndex = 281
        Me.lblCostoRealImptoQ.Text = "0.0000"
        Me.lblCostoRealImptoQ.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.lblCostoRealImptoQ.Visible = False
        '
        'Label37
        '
        Me.Label37.AutoSize = True
        Me.Label37.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label37.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.Label37.Location = New System.Drawing.Point(575, 303)
        Me.Label37.Name = "Label37"
        Me.Label37.Size = New System.Drawing.Size(142, 13)
        Me.Label37.TabIndex = 280
        Me.Label37.Text = "Suplemento Triple Total"
        '
        'Label38
        '
        Me.Label38.AutoSize = True
        Me.Label38.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label38.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.Label38.Location = New System.Drawing.Point(575, 274)
        Me.Label38.Name = "Label38"
        Me.Label38.Size = New System.Drawing.Size(156, 13)
        Me.Label38.TabIndex = 279
        Me.Label38.Text = "Suplemento Triple Margen"
        '
        'Label39
        '
        Me.Label39.AutoSize = True
        Me.Label39.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label39.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.Label39.Location = New System.Drawing.Point(575, 247)
        Me.Label39.Name = "Label39"
        Me.Label39.Size = New System.Drawing.Size(173, 13)
        Me.Label39.TabIndex = 278
        Me.Label39.Text = "Suplemento Triple Costo Real"
        '
        'Label40
        '
        Me.Label40.AutoSize = True
        Me.Label40.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label40.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.Label40.Location = New System.Drawing.Point(260, 357)
        Me.Label40.Name = "Label40"
        Me.Label40.Size = New System.Drawing.Size(148, 13)
        Me.Label40.TabIndex = 277
        Me.Label40.Text = "Suplemento Simple Total"
        '
        'Label41
        '
        Me.Label41.AutoSize = True
        Me.Label41.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label41.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.Label41.Location = New System.Drawing.Point(260, 329)
        Me.Label41.Name = "Label41"
        Me.Label41.Size = New System.Drawing.Size(207, 13)
        Me.Label41.TabIndex = 276
        Me.Label41.Text = "Suplemento Simple Monto Liberado"
        '
        'Label42
        '
        Me.Label42.AutoSize = True
        Me.Label42.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label42.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.Label42.Location = New System.Drawing.Point(260, 302)
        Me.Label42.Name = "Label42"
        Me.Label42.Size = New System.Drawing.Size(203, 13)
        Me.Label42.TabIndex = 275
        Me.Label42.Text = "Suplemento Simple Costo Liberado"
        '
        'Label43
        '
        Me.Label43.AutoSize = True
        Me.Label43.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label43.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.Label43.Location = New System.Drawing.Point(260, 274)
        Me.Label43.Name = "Label43"
        Me.Label43.Size = New System.Drawing.Size(162, 13)
        Me.Label43.TabIndex = 274
        Me.Label43.Text = "Suplemento Simple Margen"
        '
        'Label44
        '
        Me.Label44.AutoSize = True
        Me.Label44.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label44.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.Label44.Location = New System.Drawing.Point(260, 247)
        Me.Label44.Name = "Label44"
        Me.Label44.Size = New System.Drawing.Size(179, 13)
        Me.Label44.TabIndex = 273
        Me.Label44.Text = "Suplemento Simple Costo Real"
        '
        'Label45
        '
        Me.Label45.AutoSize = True
        Me.Label45.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label45.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.Label45.Location = New System.Drawing.Point(18, 413)
        Me.Label45.Name = "Label45"
        Me.Label45.Size = New System.Drawing.Size(36, 13)
        Me.Label45.TabIndex = 272
        Me.Label45.Text = "Total"
        '
        'Label47
        '
        Me.Label47.AutoSize = True
        Me.Label47.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label47.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.Label47.Location = New System.Drawing.Point(18, 329)
        Me.Label47.Name = "Label47"
        Me.Label47.Size = New System.Drawing.Size(102, 13)
        Me.Label47.TabIndex = 270
        Me.Label47.Text = "Margen Liberado"
        '
        'Label48
        '
        Me.Label48.AutoSize = True
        Me.Label48.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label48.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.Label48.Location = New System.Drawing.Point(18, 276)
        Me.Label48.Name = "Label48"
        Me.Label48.Size = New System.Drawing.Size(91, 13)
        Me.Label48.TabIndex = 269
        Me.Label48.Text = "Costo Liberado"
        '
        'Label49
        '
        Me.Label49.AutoSize = True
        Me.Label49.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label49.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.Label49.Location = New System.Drawing.Point(18, 357)
        Me.Label49.Name = "Label49"
        Me.Label49.Size = New System.Drawing.Size(50, 13)
        Me.Label49.TabIndex = 268
        Me.Label49.Text = "Margen"
        '
        'Label50
        '
        Me.Label50.AutoSize = True
        Me.Label50.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label50.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.Label50.Location = New System.Drawing.Point(18, 251)
        Me.Label50.Name = "Label50"
        Me.Label50.Size = New System.Drawing.Size(67, 13)
        Me.Label50.TabIndex = 267
        Me.Label50.Text = "Costo Real"
        '
        'txtMargenAplicadoQ
        '
        Me.txtMargenAplicadoQ.BackColor = System.Drawing.SystemColors.Window
        Me.txtMargenAplicadoQ.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtMargenAplicadoQ.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtMargenAplicadoQ.Location = New System.Drawing.Point(113, 353)
        Me.txtMargenAplicadoQ.MaxLength = 8
        Me.txtMargenAplicadoQ.Name = "txtMargenAplicadoQ"
        Me.txtMargenAplicadoQ.ReadOnly = True
        Me.txtMargenAplicadoQ.Size = New System.Drawing.Size(53, 21)
        Me.txtMargenAplicadoQ.TabIndex = 257
        Me.txtMargenAplicadoQ.Tag = "Borrar"
        Me.txtMargenAplicadoQ.Text = "0.00"
        Me.txtMargenAplicadoQ.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtCostoRealQ
        '
        Me.txtCostoRealQ.BackColor = System.Drawing.SystemColors.Window
        Me.txtCostoRealQ.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtCostoRealQ.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtCostoRealQ.Location = New System.Drawing.Point(171, 248)
        Me.txtCostoRealQ.MaxLength = 8
        Me.txtCostoRealQ.Name = "txtCostoRealQ"
        Me.txtCostoRealQ.ReadOnly = True
        Me.txtCostoRealQ.Size = New System.Drawing.Size(72, 21)
        Me.txtCostoRealQ.TabIndex = 253
        Me.txtCostoRealQ.Tag = "Borrar"
        Me.txtCostoRealQ.Text = "0.0000"
        Me.txtCostoRealQ.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lblTitulo2
        '
        Me.lblTitulo2.BackColor = System.Drawing.SystemColors.ControlDarkDark
        Me.lblTitulo2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.lblTitulo2.Dock = System.Windows.Forms.DockStyle.Top
        Me.lblTitulo2.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTitulo2.ForeColor = System.Drawing.Color.White
        Me.lblTitulo2.Location = New System.Drawing.Point(3, 16)
        Me.lblTitulo2.Name = "lblTitulo2"
        Me.lblTitulo2.Size = New System.Drawing.Size(1126, 22)
        Me.lblTitulo2.TabIndex = 109
        Me.lblTitulo2.Text = "Nuevo"
        '
        'dgvQui
        '
        Me.dgvQui.AllowUserToAddRows = False
        Me.dgvQui.AllowUserToDeleteRows = False
        Me.dgvQui.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.ControlDarkDark
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.Color.White
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvQui.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.dgvQui.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvQui.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.IDCab_Q, Me.IDDet_Q, Me.Accion, Me.Pax, Me.Liberados, Me.Tipo_Lib, Me.CostoReal, Me.CostoRealImpto, Me.Margen, Me.MargenImpto, Me.CostoLiberado, Me.CostoLiberadoImpto, Me.MargenLiberado, Me.MargenLiberadoImpto, Me.MargenAplicado, Me.Total, Me.RedondeoTotal, Me.SSCR, Me.SSCRImpto, Me.SSMargen, Me.SSMargenImpto, Me.SSCL, Me.SSCLImpto, Me.SSML, Me.SSMargenLiberadoImpto, Me.SSTotal, Me.STCR, Me.STCRImpto, Me.STMargen, Me.STMargenImpto, Me.STTotal, Me.TotImpto})
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgvQui.DefaultCellStyle = DataGridViewCellStyle2
        Me.dgvQui.Location = New System.Drawing.Point(10, 50)
        Me.dgvQui.Name = "dgvQui"
        Me.dgvQui.ReadOnly = True
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.ControlDarkDark
        DataGridViewCellStyle3.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle3.ForeColor = System.Drawing.Color.White
        DataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvQui.RowHeadersDefaultCellStyle = DataGridViewCellStyle3
        Me.dgvQui.Size = New System.Drawing.Size(953, 169)
        Me.dgvQui.TabIndex = 1
        '
        'IDCab_Q
        '
        Me.IDCab_Q.HeaderText = "IDCab_Q"
        Me.IDCab_Q.Name = "IDCab_Q"
        Me.IDCab_Q.ReadOnly = True
        Me.IDCab_Q.Visible = False
        '
        'IDDet_Q
        '
        Me.IDDet_Q.HeaderText = "IDDet_Q"
        Me.IDDet_Q.Name = "IDDet_Q"
        Me.IDDet_Q.ReadOnly = True
        Me.IDDet_Q.Visible = False
        '
        'Accion
        '
        Me.Accion.HeaderText = "Accion"
        Me.Accion.Name = "Accion"
        Me.Accion.ReadOnly = True
        Me.Accion.Visible = False
        '
        'Pax
        '
        Me.Pax.HeaderText = "Pax"
        Me.Pax.Name = "Pax"
        Me.Pax.ReadOnly = True
        '
        'Liberados
        '
        Me.Liberados.HeaderText = "Liberados"
        Me.Liberados.Name = "Liberados"
        Me.Liberados.ReadOnly = True
        '
        'Tipo_Lib
        '
        Me.Tipo_Lib.HeaderText = "Tipo Liberado"
        Me.Tipo_Lib.Name = "Tipo_Lib"
        Me.Tipo_Lib.ReadOnly = True
        '
        'CostoReal
        '
        Me.CostoReal.HeaderText = "Costo Real"
        Me.CostoReal.Name = "CostoReal"
        Me.CostoReal.ReadOnly = True
        '
        'CostoRealImpto
        '
        Me.CostoRealImpto.HeaderText = "CostoRealImpto"
        Me.CostoRealImpto.Name = "CostoRealImpto"
        Me.CostoRealImpto.ReadOnly = True
        '
        'Margen
        '
        Me.Margen.HeaderText = "Margen"
        Me.Margen.Name = "Margen"
        Me.Margen.ReadOnly = True
        '
        'MargenImpto
        '
        Me.MargenImpto.HeaderText = "MargenImpto"
        Me.MargenImpto.Name = "MargenImpto"
        Me.MargenImpto.ReadOnly = True
        '
        'CostoLiberado
        '
        Me.CostoLiberado.HeaderText = "Costo Liberado"
        Me.CostoLiberado.Name = "CostoLiberado"
        Me.CostoLiberado.ReadOnly = True
        '
        'CostoLiberadoImpto
        '
        Me.CostoLiberadoImpto.HeaderText = "CostoLiberadoImpto"
        Me.CostoLiberadoImpto.Name = "CostoLiberadoImpto"
        Me.CostoLiberadoImpto.ReadOnly = True
        '
        'MargenLiberado
        '
        Me.MargenLiberado.HeaderText = "Margen Liberado"
        Me.MargenLiberado.Name = "MargenLiberado"
        Me.MargenLiberado.ReadOnly = True
        '
        'MargenLiberadoImpto
        '
        Me.MargenLiberadoImpto.HeaderText = "MargenLiberadoImpto"
        Me.MargenLiberadoImpto.Name = "MargenLiberadoImpto"
        Me.MargenLiberadoImpto.ReadOnly = True
        '
        'MargenAplicado
        '
        Me.MargenAplicado.HeaderText = "Margen Aplicado"
        Me.MargenAplicado.Name = "MargenAplicado"
        Me.MargenAplicado.ReadOnly = True
        '
        'Total
        '
        Me.Total.HeaderText = "Total"
        Me.Total.Name = "Total"
        Me.Total.ReadOnly = True
        '
        'RedondeoTotal
        '
        Me.RedondeoTotal.HeaderText = "Redondeo"
        Me.RedondeoTotal.Name = "RedondeoTotal"
        Me.RedondeoTotal.ReadOnly = True
        Me.RedondeoTotal.Visible = False
        '
        'SSCR
        '
        Me.SSCR.HeaderText = "Sup. Simple Costo Real"
        Me.SSCR.Name = "SSCR"
        Me.SSCR.ReadOnly = True
        '
        'SSCRImpto
        '
        Me.SSCRImpto.HeaderText = "SSCRImpto"
        Me.SSCRImpto.Name = "SSCRImpto"
        Me.SSCRImpto.ReadOnly = True
        '
        'SSMargen
        '
        Me.SSMargen.HeaderText = "Sup. Simple Margen"
        Me.SSMargen.Name = "SSMargen"
        Me.SSMargen.ReadOnly = True
        '
        'SSMargenImpto
        '
        Me.SSMargenImpto.HeaderText = "SSMargenImpto"
        Me.SSMargenImpto.Name = "SSMargenImpto"
        Me.SSMargenImpto.ReadOnly = True
        '
        'SSCL
        '
        Me.SSCL.HeaderText = "Sup. Simlple Costo Lib."
        Me.SSCL.Name = "SSCL"
        Me.SSCL.ReadOnly = True
        '
        'SSCLImpto
        '
        Me.SSCLImpto.HeaderText = "SSCLImpto"
        Me.SSCLImpto.Name = "SSCLImpto"
        Me.SSCLImpto.ReadOnly = True
        '
        'SSML
        '
        Me.SSML.HeaderText = "Sup. Simple Monto Lib."
        Me.SSML.Name = "SSML"
        Me.SSML.ReadOnly = True
        '
        'SSMargenLiberadoImpto
        '
        Me.SSMargenLiberadoImpto.HeaderText = "SSMLImpto"
        Me.SSMargenLiberadoImpto.Name = "SSMargenLiberadoImpto"
        Me.SSMargenLiberadoImpto.ReadOnly = True
        '
        'SSTotal
        '
        Me.SSTotal.HeaderText = "Sup. Simple Total"
        Me.SSTotal.Name = "SSTotal"
        Me.SSTotal.ReadOnly = True
        '
        'STCR
        '
        Me.STCR.HeaderText = "Sup. Triple Costo Real"
        Me.STCR.Name = "STCR"
        Me.STCR.ReadOnly = True
        '
        'STCRImpto
        '
        Me.STCRImpto.HeaderText = "STCRImpto"
        Me.STCRImpto.Name = "STCRImpto"
        Me.STCRImpto.ReadOnly = True
        '
        'STMargen
        '
        Me.STMargen.HeaderText = "Sup. Triple Margen"
        Me.STMargen.Name = "STMargen"
        Me.STMargen.ReadOnly = True
        '
        'STMargenImpto
        '
        Me.STMargenImpto.HeaderText = "STMargenImpto"
        Me.STMargenImpto.Name = "STMargenImpto"
        Me.STMargenImpto.ReadOnly = True
        '
        'STTotal
        '
        Me.STTotal.HeaderText = "Sup. Triple Total"
        Me.STTotal.Name = "STTotal"
        Me.STTotal.ReadOnly = True
        '
        'TotImpto
        '
        Me.TotImpto.HeaderText = "TotImpto"
        Me.TotImpto.Name = "TotImpto"
        Me.TotImpto.ReadOnly = True
        '
        'TabPage3
        '
        Me.TabPage3.Controls.Add(Me.grbMontos)
        Me.TabPage3.Location = New System.Drawing.Point(4, 22)
        Me.TabPage3.Name = "TabPage3"
        Me.TabPage3.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage3.Size = New System.Drawing.Size(1138, 584)
        Me.TabPage3.TabIndex = 2
        Me.TabPage3.Text = "Montos Calculados"
        Me.TabPage3.UseVisualStyleBackColor = True
        '
        'grbMontos
        '
        Me.grbMontos.BackColor = System.Drawing.Color.White
        Me.grbMontos.Controls.Add(Me.txtSTCR2)
        Me.grbMontos.Controls.Add(Me.txtSSCR2)
        Me.grbMontos.Controls.Add(Me.lblMargenAplicado4)
        Me.grbMontos.Controls.Add(Me.lblSTImpEtiq2)
        Me.grbMontos.Controls.Add(Me.lblSTCRImpto2)
        Me.grbMontos.Controls.Add(Me.lblMargenAplicado3)
        Me.grbMontos.Controls.Add(Me.lblSSImpEtiq2)
        Me.grbMontos.Controls.Add(Me.lblRedondeoTotal2)
        Me.grbMontos.Controls.Add(Me.Label10)
        Me.grbMontos.Controls.Add(Me.grbSep4)
        Me.grbMontos.Controls.Add(Me.lblSTCR2)
        Me.grbMontos.Controls.Add(Me.lblSTMargen2)
        Me.grbMontos.Controls.Add(Me.lblSTTotal2)
        Me.grbMontos.Controls.Add(Me.lblSSCL2)
        Me.grbMontos.Controls.Add(Me.lblSSMargen2)
        Me.grbMontos.Controls.Add(Me.lblSSCR2)
        Me.grbMontos.Controls.Add(Me.lblSSMargenLiberado2)
        Me.grbMontos.Controls.Add(Me.lblSSTotal2)
        Me.grbMontos.Controls.Add(Me.lblMargenAplicado2)
        Me.grbMontos.Controls.Add(Me.lblCostoRealImpto2)
        Me.grbMontos.Controls.Add(Me.lblTotal2)
        Me.grbMontos.Controls.Add(Me.lblCostoLiberado2)
        Me.grbMontos.Controls.Add(Me.lblMargen2)
        Me.grbMontos.Controls.Add(Me.lblCostoReal2)
        Me.grbMontos.Controls.Add(Me.lblMargenLiberado2)
        Me.grbMontos.Controls.Add(Me.GroupBox3)
        Me.grbMontos.Controls.Add(Me.lblTituloCostoTriple)
        Me.grbMontos.Controls.Add(Me.lblTituloCostoSimple)
        Me.grbMontos.Controls.Add(Me.lblCostoenDobleEtiq)
        Me.grbMontos.Controls.Add(Me.lblTitulo3)
        Me.grbMontos.Controls.Add(Me.lblTotImpto2)
        Me.grbMontos.Controls.Add(Me.Label9)
        Me.grbMontos.Controls.Add(Me.lblImpuestosEtiq22)
        Me.grbMontos.Controls.Add(Me.lblImpuestosEtiq12)
        Me.grbMontos.Controls.Add(Me.Label14)
        Me.grbMontos.Controls.Add(Me.lblSTMargenImpto2)
        Me.grbMontos.Controls.Add(Me.lblSSMargenLiberadoImpto2)
        Me.grbMontos.Controls.Add(Me.lblSSCLImpto2)
        Me.grbMontos.Controls.Add(Me.lblSSMargenImpto2)
        Me.grbMontos.Controls.Add(Me.lblSSCRImpto2)
        Me.grbMontos.Controls.Add(Me.lblMargenLiberadoImpto2)
        Me.grbMontos.Controls.Add(Me.lblCostoLiberadoImpto2)
        Me.grbMontos.Controls.Add(Me.lblMargenImpto2)
        Me.grbMontos.Controls.Add(Me.lblSTTotalEtiq2)
        Me.grbMontos.Controls.Add(Me.lblSTMargenEtiq2)
        Me.grbMontos.Controls.Add(Me.lblSTCREtiq2)
        Me.grbMontos.Controls.Add(Me.lblSSTotalEtiq2)
        Me.grbMontos.Controls.Add(Me.lblSSMLEtiq2)
        Me.grbMontos.Controls.Add(Me.lblSSCLEtiq2)
        Me.grbMontos.Controls.Add(Me.lblSSMargenEtiq2)
        Me.grbMontos.Controls.Add(Me.lblSSCREtiq2)
        Me.grbMontos.Controls.Add(Me.Label54)
        Me.grbMontos.Controls.Add(Me.Label56)
        Me.grbMontos.Controls.Add(Me.Label57)
        Me.grbMontos.Controls.Add(Me.Label58)
        Me.grbMontos.Controls.Add(Me.Label59)
        Me.grbMontos.Dock = System.Windows.Forms.DockStyle.Fill
        Me.grbMontos.Location = New System.Drawing.Point(3, 3)
        Me.grbMontos.Name = "grbMontos"
        Me.grbMontos.Size = New System.Drawing.Size(1132, 578)
        Me.grbMontos.TabIndex = 0
        Me.grbMontos.TabStop = False
        '
        'txtSTCR2
        '
        Me.txtSTCR2.BackColor = System.Drawing.SystemColors.Window
        Me.txtSTCR2.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtSTCR2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtSTCR2.Location = New System.Drawing.Point(782, 100)
        Me.txtSTCR2.MaxLength = 8
        Me.txtSTCR2.Name = "txtSTCR2"
        Me.txtSTCR2.Size = New System.Drawing.Size(66, 21)
        Me.txtSTCR2.TabIndex = 373
        Me.txtSTCR2.Tag = "Borrar"
        Me.txtSTCR2.Text = "0.0000"
        Me.txtSTCR2.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtSTCR2.Visible = False
        '
        'txtSSCR2
        '
        Me.txtSSCR2.BackColor = System.Drawing.SystemColors.Window
        Me.txtSSCR2.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtSSCR2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtSSCR2.Location = New System.Drawing.Point(499, 100)
        Me.txtSSCR2.MaxLength = 8
        Me.txtSSCR2.Name = "txtSSCR2"
        Me.txtSSCR2.Size = New System.Drawing.Size(66, 21)
        Me.txtSSCR2.TabIndex = 372
        Me.txtSSCR2.Tag = "Borrar"
        Me.txtSSCR2.Text = "0.0000"
        Me.txtSSCR2.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtSSCR2.Visible = False
        '
        'lblMargenAplicado4
        '
        Me.lblMargenAplicado4.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.lblMargenAplicado4.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMargenAplicado4.Location = New System.Drawing.Point(726, 153)
        Me.lblMargenAplicado4.Name = "lblMargenAplicado4"
        Me.lblMargenAplicado4.Size = New System.Drawing.Size(50, 21)
        Me.lblMargenAplicado4.TabIndex = 371
        Me.lblMargenAplicado4.Text = "0.00"
        Me.lblMargenAplicado4.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblSTImpEtiq2
        '
        Me.lblSTImpEtiq2.AutoSize = True
        Me.lblSTImpEtiq2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSTImpEtiq2.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.lblSTImpEtiq2.Location = New System.Drawing.Point(586, 130)
        Me.lblSTImpEtiq2.Name = "lblSTImpEtiq2"
        Me.lblSTImpEtiq2.Size = New System.Drawing.Size(133, 13)
        Me.lblSTImpEtiq2.TabIndex = 370
        Me.lblSTImpEtiq2.Text = "Supl. Triple Impuestos"
        '
        'lblSTCRImpto2
        '
        Me.lblSTCRImpto2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.lblSTCRImpto2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSTCRImpto2.Location = New System.Drawing.Point(782, 126)
        Me.lblSTCRImpto2.Name = "lblSTCRImpto2"
        Me.lblSTCRImpto2.Size = New System.Drawing.Size(66, 21)
        Me.lblSTCRImpto2.TabIndex = 335
        Me.lblSTCRImpto2.Text = "0.0000"
        Me.lblSTCRImpto2.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblMargenAplicado3
        '
        Me.lblMargenAplicado3.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.lblMargenAplicado3.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMargenAplicado3.Location = New System.Drawing.Point(443, 206)
        Me.lblMargenAplicado3.Name = "lblMargenAplicado3"
        Me.lblMargenAplicado3.Size = New System.Drawing.Size(50, 21)
        Me.lblMargenAplicado3.TabIndex = 369
        Me.lblMargenAplicado3.Text = "0.00"
        Me.lblMargenAplicado3.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblSSImpEtiq2
        '
        Me.lblSSImpEtiq2.AutoSize = True
        Me.lblSSImpEtiq2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSSImpEtiq2.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.lblSSImpEtiq2.Location = New System.Drawing.Point(282, 155)
        Me.lblSSImpEtiq2.Name = "lblSSImpEtiq2"
        Me.lblSSImpEtiq2.Size = New System.Drawing.Size(139, 13)
        Me.lblSSImpEtiq2.TabIndex = 368
        Me.lblSSImpEtiq2.Text = "Supl. Simple Impuestos"
        '
        'lblRedondeoTotal2
        '
        Me.lblRedondeoTotal2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.lblRedondeoTotal2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblRedondeoTotal2.Location = New System.Drawing.Point(174, 235)
        Me.lblRedondeoTotal2.Name = "lblRedondeoTotal2"
        Me.lblRedondeoTotal2.Size = New System.Drawing.Size(66, 21)
        Me.lblRedondeoTotal2.TabIndex = 367
        Me.lblRedondeoTotal2.Text = "0.0000"
        Me.lblRedondeoTotal2.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label10
        '
        Me.Label10.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label10.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.Label10.Location = New System.Drawing.Point(16, 235)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(71, 19)
        Me.Label10.TabIndex = 366
        Me.Label10.Text = "Redondeo"
        Me.Label10.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'grbSep4
        '
        Me.grbSep4.Location = New System.Drawing.Point(571, 51)
        Me.grbSep4.Name = "grbSep4"
        Me.grbSep4.Size = New System.Drawing.Size(4, 231)
        Me.grbSep4.TabIndex = 365
        Me.grbSep4.TabStop = False
        Me.grbSep4.Text = "GroupBox4"
        '
        'lblSTCR2
        '
        Me.lblSTCR2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.lblSTCR2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSTCR2.Location = New System.Drawing.Point(782, 100)
        Me.lblSTCR2.Name = "lblSTCR2"
        Me.lblSTCR2.Size = New System.Drawing.Size(66, 21)
        Me.lblSTCR2.TabIndex = 364
        Me.lblSTCR2.Text = "0.0000"
        Me.lblSTCR2.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblSTMargen2
        '
        Me.lblSTMargen2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.lblSTMargen2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSTMargen2.Location = New System.Drawing.Point(782, 153)
        Me.lblSTMargen2.Name = "lblSTMargen2"
        Me.lblSTMargen2.Size = New System.Drawing.Size(66, 21)
        Me.lblSTMargen2.TabIndex = 363
        Me.lblSTMargen2.Text = "0.0000"
        Me.lblSTMargen2.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblSTTotal2
        '
        Me.lblSTTotal2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.lblSTTotal2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSTTotal2.Location = New System.Drawing.Point(782, 178)
        Me.lblSTTotal2.Name = "lblSTTotal2"
        Me.lblSTTotal2.Size = New System.Drawing.Size(66, 21)
        Me.lblSTTotal2.TabIndex = 362
        Me.lblSTTotal2.Text = "0.0000"
        Me.lblSTTotal2.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblSSCL2
        '
        Me.lblSSCL2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.lblSSCL2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSSCL2.Location = New System.Drawing.Point(499, 126)
        Me.lblSSCL2.Name = "lblSSCL2"
        Me.lblSSCL2.Size = New System.Drawing.Size(66, 21)
        Me.lblSSCL2.TabIndex = 361
        Me.lblSSCL2.Text = "0.0000"
        Me.lblSSCL2.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblSSMargen2
        '
        Me.lblSSMargen2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.lblSSMargen2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSSMargen2.Location = New System.Drawing.Point(499, 206)
        Me.lblSSMargen2.Name = "lblSSMargen2"
        Me.lblSSMargen2.Size = New System.Drawing.Size(66, 21)
        Me.lblSSMargen2.TabIndex = 360
        Me.lblSSMargen2.Text = "0.0000"
        Me.lblSSMargen2.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblSSCR2
        '
        Me.lblSSCR2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.lblSSCR2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSSCR2.Location = New System.Drawing.Point(499, 99)
        Me.lblSSCR2.Name = "lblSSCR2"
        Me.lblSSCR2.Size = New System.Drawing.Size(66, 21)
        Me.lblSSCR2.TabIndex = 359
        Me.lblSSCR2.Text = "0.0000"
        Me.lblSSCR2.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblSSMargenLiberado2
        '
        Me.lblSSMargenLiberado2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.lblSSMargenLiberado2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSSMargenLiberado2.Location = New System.Drawing.Point(499, 179)
        Me.lblSSMargenLiberado2.Name = "lblSSMargenLiberado2"
        Me.lblSSMargenLiberado2.Size = New System.Drawing.Size(66, 21)
        Me.lblSSMargenLiberado2.TabIndex = 358
        Me.lblSSMargenLiberado2.Text = "0.0000"
        Me.lblSSMargenLiberado2.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblSSTotal2
        '
        Me.lblSSTotal2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.lblSSTotal2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSSTotal2.Location = New System.Drawing.Point(499, 233)
        Me.lblSSTotal2.Name = "lblSSTotal2"
        Me.lblSSTotal2.Size = New System.Drawing.Size(66, 21)
        Me.lblSSTotal2.TabIndex = 357
        Me.lblSSTotal2.Text = "0.0000"
        Me.lblSSTotal2.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblMargenAplicado2
        '
        Me.lblMargenAplicado2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.lblMargenAplicado2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMargenAplicado2.Location = New System.Drawing.Point(118, 205)
        Me.lblMargenAplicado2.Name = "lblMargenAplicado2"
        Me.lblMargenAplicado2.Size = New System.Drawing.Size(50, 21)
        Me.lblMargenAplicado2.TabIndex = 356
        Me.lblMargenAplicado2.Text = "0.00"
        Me.lblMargenAplicado2.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblCostoRealImpto2
        '
        Me.lblCostoRealImpto2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.lblCostoRealImpto2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCostoRealImpto2.Location = New System.Drawing.Point(24, 338)
        Me.lblCostoRealImpto2.Name = "lblCostoRealImpto2"
        Me.lblCostoRealImpto2.Size = New System.Drawing.Size(65, 20)
        Me.lblCostoRealImpto2.TabIndex = 355
        Me.lblCostoRealImpto2.Text = "0.0000"
        Me.lblCostoRealImpto2.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.lblCostoRealImpto2.Visible = False
        '
        'lblTotal2
        '
        Me.lblTotal2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.lblTotal2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTotal2.Location = New System.Drawing.Point(174, 261)
        Me.lblTotal2.Name = "lblTotal2"
        Me.lblTotal2.Size = New System.Drawing.Size(66, 21)
        Me.lblTotal2.TabIndex = 354
        Me.lblTotal2.Text = "0.0000"
        Me.lblTotal2.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblCostoLiberado2
        '
        Me.lblCostoLiberado2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.lblCostoLiberado2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCostoLiberado2.Location = New System.Drawing.Point(174, 126)
        Me.lblCostoLiberado2.Name = "lblCostoLiberado2"
        Me.lblCostoLiberado2.Size = New System.Drawing.Size(66, 21)
        Me.lblCostoLiberado2.TabIndex = 353
        Me.lblCostoLiberado2.Text = "0.0000"
        Me.lblCostoLiberado2.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblMargen2
        '
        Me.lblMargen2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.lblMargen2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMargen2.Location = New System.Drawing.Point(174, 205)
        Me.lblMargen2.Name = "lblMargen2"
        Me.lblMargen2.Size = New System.Drawing.Size(66, 21)
        Me.lblMargen2.TabIndex = 352
        Me.lblMargen2.Text = "0.0000"
        Me.lblMargen2.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblCostoReal2
        '
        Me.lblCostoReal2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.lblCostoReal2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCostoReal2.Location = New System.Drawing.Point(174, 100)
        Me.lblCostoReal2.Name = "lblCostoReal2"
        Me.lblCostoReal2.Size = New System.Drawing.Size(66, 21)
        Me.lblCostoReal2.TabIndex = 351
        Me.lblCostoReal2.Text = "0.0000"
        Me.lblCostoReal2.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblMargenLiberado2
        '
        Me.lblMargenLiberado2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.lblMargenLiberado2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMargenLiberado2.Location = New System.Drawing.Point(174, 177)
        Me.lblMargenLiberado2.Name = "lblMargenLiberado2"
        Me.lblMargenLiberado2.Size = New System.Drawing.Size(66, 21)
        Me.lblMargenLiberado2.TabIndex = 350
        Me.lblMargenLiberado2.Text = "0.0000"
        Me.lblMargenLiberado2.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'GroupBox3
        '
        Me.GroupBox3.Location = New System.Drawing.Point(271, 48)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(4, 231)
        Me.GroupBox3.TabIndex = 348
        Me.GroupBox3.TabStop = False
        Me.GroupBox3.Text = "GroupBox3"
        '
        'lblTituloCostoTriple
        '
        Me.lblTituloCostoTriple.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTituloCostoTriple.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.lblTituloCostoTriple.Location = New System.Drawing.Point(616, 51)
        Me.lblTituloCostoTriple.Name = "lblTituloCostoTriple"
        Me.lblTituloCostoTriple.Size = New System.Drawing.Size(255, 19)
        Me.lblTituloCostoTriple.TabIndex = 347
        Me.lblTituloCostoTriple.Text = "Costos en Triple"
        Me.lblTituloCostoTriple.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblTituloCostoSimple
        '
        Me.lblTituloCostoSimple.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTituloCostoSimple.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.lblTituloCostoSimple.Location = New System.Drawing.Point(281, 48)
        Me.lblTituloCostoSimple.Name = "lblTituloCostoSimple"
        Me.lblTituloCostoSimple.Size = New System.Drawing.Size(317, 19)
        Me.lblTituloCostoSimple.TabIndex = 346
        Me.lblTituloCostoSimple.Text = "Costos en Simple"
        Me.lblTituloCostoSimple.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblCostoenDobleEtiq
        '
        Me.lblCostoenDobleEtiq.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCostoenDobleEtiq.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.lblCostoenDobleEtiq.Location = New System.Drawing.Point(16, 48)
        Me.lblCostoenDobleEtiq.Name = "lblCostoenDobleEtiq"
        Me.lblCostoenDobleEtiq.Size = New System.Drawing.Size(255, 19)
        Me.lblCostoenDobleEtiq.TabIndex = 345
        Me.lblCostoenDobleEtiq.Text = "Costos en Doble"
        Me.lblCostoenDobleEtiq.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblTitulo3
        '
        Me.lblTitulo3.BackColor = System.Drawing.SystemColors.ControlDarkDark
        Me.lblTitulo3.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.lblTitulo3.Dock = System.Windows.Forms.DockStyle.Top
        Me.lblTitulo3.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTitulo3.ForeColor = System.Drawing.Color.White
        Me.lblTitulo3.Location = New System.Drawing.Point(3, 16)
        Me.lblTitulo3.Name = "lblTitulo3"
        Me.lblTitulo3.Size = New System.Drawing.Size(1126, 22)
        Me.lblTitulo3.TabIndex = 344
        Me.lblTitulo3.Text = "Nuevo"
        '
        'lblTotImpto2
        '
        Me.lblTotImpto2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.lblTotImpto2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTotImpto2.Location = New System.Drawing.Point(174, 151)
        Me.lblTotImpto2.Name = "lblTotImpto2"
        Me.lblTotImpto2.Size = New System.Drawing.Size(66, 21)
        Me.lblTotImpto2.TabIndex = 342
        Me.lblTotImpto2.Text = "0.0000"
        Me.lblTotImpto2.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label9.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.Label9.Location = New System.Drawing.Point(19, 155)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(68, 13)
        Me.Label9.TabIndex = 341
        Me.Label9.Text = "Impuestos"
        '
        'lblImpuestosEtiq22
        '
        Me.lblImpuestosEtiq22.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblImpuestosEtiq22.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.lblImpuestosEtiq22.Location = New System.Drawing.Point(994, 78)
        Me.lblImpuestosEtiq22.Name = "lblImpuestosEtiq22"
        Me.lblImpuestosEtiq22.Size = New System.Drawing.Size(73, 16)
        Me.lblImpuestosEtiq22.TabIndex = 339
        Me.lblImpuestosEtiq22.Text = "Impuestos"
        Me.lblImpuestosEtiq22.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblImpuestosEtiq12
        '
        Me.lblImpuestosEtiq12.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblImpuestosEtiq12.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.lblImpuestosEtiq12.Location = New System.Drawing.Point(281, 316)
        Me.lblImpuestosEtiq12.Name = "lblImpuestosEtiq12"
        Me.lblImpuestosEtiq12.Size = New System.Drawing.Size(69, 16)
        Me.lblImpuestosEtiq12.TabIndex = 338
        Me.lblImpuestosEtiq12.Text = "Impuestos"
        Me.lblImpuestosEtiq12.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.lblImpuestosEtiq12.Visible = False
        '
        'Label14
        '
        Me.Label14.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label14.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.Label14.Location = New System.Drawing.Point(18, 316)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(71, 18)
        Me.Label14.TabIndex = 337
        Me.Label14.Text = "Impuestos"
        Me.Label14.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.Label14.Visible = False
        '
        'lblSTMargenImpto2
        '
        Me.lblSTMargenImpto2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.lblSTMargenImpto2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSTMargenImpto2.Location = New System.Drawing.Point(994, 127)
        Me.lblSTMargenImpto2.Name = "lblSTMargenImpto2"
        Me.lblSTMargenImpto2.Size = New System.Drawing.Size(66, 21)
        Me.lblSTMargenImpto2.TabIndex = 336
        Me.lblSTMargenImpto2.Text = "0.00"
        Me.lblSTMargenImpto2.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblSSMargenLiberadoImpto2
        '
        Me.lblSSMargenLiberadoImpto2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.lblSSMargenLiberadoImpto2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSSMargenLiberadoImpto2.Location = New System.Drawing.Point(284, 420)
        Me.lblSSMargenLiberadoImpto2.Name = "lblSSMargenLiberadoImpto2"
        Me.lblSSMargenLiberadoImpto2.Size = New System.Drawing.Size(66, 21)
        Me.lblSSMargenLiberadoImpto2.TabIndex = 334
        Me.lblSSMargenLiberadoImpto2.Text = "0.0000"
        Me.lblSSMargenLiberadoImpto2.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.lblSSMargenLiberadoImpto2.Visible = False
        '
        'lblSSCLImpto2
        '
        Me.lblSSCLImpto2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.lblSSCLImpto2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSSCLImpto2.Location = New System.Drawing.Point(284, 393)
        Me.lblSSCLImpto2.Name = "lblSSCLImpto2"
        Me.lblSSCLImpto2.Size = New System.Drawing.Size(66, 21)
        Me.lblSSCLImpto2.TabIndex = 333
        Me.lblSSCLImpto2.Text = "0.0000"
        Me.lblSSCLImpto2.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.lblSSCLImpto2.Visible = False
        '
        'lblSSMargenImpto2
        '
        Me.lblSSMargenImpto2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.lblSSMargenImpto2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSSMargenImpto2.Location = New System.Drawing.Point(284, 365)
        Me.lblSSMargenImpto2.Name = "lblSSMargenImpto2"
        Me.lblSSMargenImpto2.Size = New System.Drawing.Size(66, 21)
        Me.lblSSMargenImpto2.TabIndex = 332
        Me.lblSSMargenImpto2.Text = "0.0000"
        Me.lblSSMargenImpto2.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.lblSSMargenImpto2.Visible = False
        '
        'lblSSCRImpto2
        '
        Me.lblSSCRImpto2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.lblSSCRImpto2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSSCRImpto2.Location = New System.Drawing.Point(499, 151)
        Me.lblSSCRImpto2.Name = "lblSSCRImpto2"
        Me.lblSSCRImpto2.Size = New System.Drawing.Size(66, 21)
        Me.lblSSCRImpto2.TabIndex = 331
        Me.lblSSCRImpto2.Text = "0.0000"
        Me.lblSSCRImpto2.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblMargenLiberadoImpto2
        '
        Me.lblMargenLiberadoImpto2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.lblMargenLiberadoImpto2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMargenLiberadoImpto2.Location = New System.Drawing.Point(24, 419)
        Me.lblMargenLiberadoImpto2.Name = "lblMargenLiberadoImpto2"
        Me.lblMargenLiberadoImpto2.Size = New System.Drawing.Size(65, 20)
        Me.lblMargenLiberadoImpto2.TabIndex = 330
        Me.lblMargenLiberadoImpto2.Text = "0.0000"
        Me.lblMargenLiberadoImpto2.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.lblMargenLiberadoImpto2.Visible = False
        '
        'lblCostoLiberadoImpto2
        '
        Me.lblCostoLiberadoImpto2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.lblCostoLiberadoImpto2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCostoLiberadoImpto2.Location = New System.Drawing.Point(24, 392)
        Me.lblCostoLiberadoImpto2.Name = "lblCostoLiberadoImpto2"
        Me.lblCostoLiberadoImpto2.Size = New System.Drawing.Size(65, 20)
        Me.lblCostoLiberadoImpto2.TabIndex = 329
        Me.lblCostoLiberadoImpto2.Text = "0.0000"
        Me.lblCostoLiberadoImpto2.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.lblCostoLiberadoImpto2.Visible = False
        '
        'lblMargenImpto2
        '
        Me.lblMargenImpto2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.lblMargenImpto2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMargenImpto2.Location = New System.Drawing.Point(24, 365)
        Me.lblMargenImpto2.Name = "lblMargenImpto2"
        Me.lblMargenImpto2.Size = New System.Drawing.Size(65, 20)
        Me.lblMargenImpto2.TabIndex = 328
        Me.lblMargenImpto2.Text = "0.0000"
        Me.lblMargenImpto2.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.lblMargenImpto2.Visible = False
        '
        'lblSTTotalEtiq2
        '
        Me.lblSTTotalEtiq2.AutoSize = True
        Me.lblSTTotalEtiq2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSTTotalEtiq2.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.lblSTTotalEtiq2.Location = New System.Drawing.Point(586, 183)
        Me.lblSTTotalEtiq2.Name = "lblSTTotalEtiq2"
        Me.lblSTTotalEtiq2.Size = New System.Drawing.Size(101, 13)
        Me.lblSTTotalEtiq2.TabIndex = 327
        Me.lblSTTotalEtiq2.Text = "Supl. Triple Total"
        '
        'lblSTMargenEtiq2
        '
        Me.lblSTMargenEtiq2.AutoSize = True
        Me.lblSTMargenEtiq2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSTMargenEtiq2.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.lblSTMargenEtiq2.Location = New System.Drawing.Point(586, 155)
        Me.lblSTMargenEtiq2.Name = "lblSTMargenEtiq2"
        Me.lblSTMargenEtiq2.Size = New System.Drawing.Size(115, 13)
        Me.lblSTMargenEtiq2.TabIndex = 326
        Me.lblSTMargenEtiq2.Text = "Supl. Triple Margen"
        '
        'lblSTCREtiq2
        '
        Me.lblSTCREtiq2.AutoSize = True
        Me.lblSTCREtiq2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSTCREtiq2.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.lblSTCREtiq2.Location = New System.Drawing.Point(586, 104)
        Me.lblSTCREtiq2.Name = "lblSTCREtiq2"
        Me.lblSTCREtiq2.Size = New System.Drawing.Size(132, 13)
        Me.lblSTCREtiq2.TabIndex = 325
        Me.lblSTCREtiq2.Text = "Supl. Triple Costo Real"
        '
        'lblSSTotalEtiq2
        '
        Me.lblSSTotalEtiq2.AutoSize = True
        Me.lblSSTotalEtiq2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSSTotalEtiq2.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.lblSSTotalEtiq2.Location = New System.Drawing.Point(281, 237)
        Me.lblSSTotalEtiq2.Name = "lblSSTotalEtiq2"
        Me.lblSSTotalEtiq2.Size = New System.Drawing.Size(107, 13)
        Me.lblSSTotalEtiq2.TabIndex = 324
        Me.lblSSTotalEtiq2.Text = "Supl. Simple Total"
        '
        'lblSSMLEtiq2
        '
        Me.lblSSMLEtiq2.AutoSize = True
        Me.lblSSMLEtiq2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSSMLEtiq2.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.lblSSMLEtiq2.Location = New System.Drawing.Point(281, 182)
        Me.lblSSMLEtiq2.Name = "lblSSMLEtiq2"
        Me.lblSSMLEtiq2.Size = New System.Drawing.Size(173, 13)
        Me.lblSSMLEtiq2.TabIndex = 323
        Me.lblSSMLEtiq2.Text = "Supl. Simple Margen Liberado"
        '
        'lblSSCLEtiq2
        '
        Me.lblSSCLEtiq2.AutoSize = True
        Me.lblSSCLEtiq2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSSCLEtiq2.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.lblSSCLEtiq2.Location = New System.Drawing.Point(281, 130)
        Me.lblSSCLEtiq2.Name = "lblSSCLEtiq2"
        Me.lblSSCLEtiq2.Size = New System.Drawing.Size(162, 13)
        Me.lblSSCLEtiq2.TabIndex = 322
        Me.lblSSCLEtiq2.Text = "Supl. Simple Costo Liberado"
        '
        'lblSSMargenEtiq2
        '
        Me.lblSSMargenEtiq2.AutoSize = True
        Me.lblSSMargenEtiq2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSSMargenEtiq2.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.lblSSMargenEtiq2.Location = New System.Drawing.Point(281, 209)
        Me.lblSSMargenEtiq2.Name = "lblSSMargenEtiq2"
        Me.lblSSMargenEtiq2.Size = New System.Drawing.Size(121, 13)
        Me.lblSSMargenEtiq2.TabIndex = 321
        Me.lblSSMargenEtiq2.Text = "Supl. Simple Margen"
        '
        'lblSSCREtiq2
        '
        Me.lblSSCREtiq2.AutoSize = True
        Me.lblSSCREtiq2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSSCREtiq2.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.lblSSCREtiq2.Location = New System.Drawing.Point(281, 103)
        Me.lblSSCREtiq2.Name = "lblSSCREtiq2"
        Me.lblSSCREtiq2.Size = New System.Drawing.Size(139, 13)
        Me.lblSSCREtiq2.TabIndex = 320
        Me.lblSSCREtiq2.Text = "Supl. Simple Costo Neto"
        '
        'Label54
        '
        Me.Label54.AutoSize = True
        Me.Label54.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label54.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.Label54.Location = New System.Drawing.Point(18, 266)
        Me.Label54.Name = "Label54"
        Me.Label54.Size = New System.Drawing.Size(36, 13)
        Me.Label54.TabIndex = 319
        Me.Label54.Text = "Total"
        '
        'Label56
        '
        Me.Label56.AutoSize = True
        Me.Label56.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label56.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.Label56.Location = New System.Drawing.Point(18, 182)
        Me.Label56.Name = "Label56"
        Me.Label56.Size = New System.Drawing.Size(102, 13)
        Me.Label56.TabIndex = 317
        Me.Label56.Text = "Margen Liberado"
        '
        'Label57
        '
        Me.Label57.AutoSize = True
        Me.Label57.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label57.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.Label57.Location = New System.Drawing.Point(18, 130)
        Me.Label57.Name = "Label57"
        Me.Label57.Size = New System.Drawing.Size(91, 13)
        Me.Label57.TabIndex = 316
        Me.Label57.Text = "Costo Liberado"
        '
        'Label58
        '
        Me.Label58.AutoSize = True
        Me.Label58.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label58.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.Label58.Location = New System.Drawing.Point(18, 208)
        Me.Label58.Name = "Label58"
        Me.Label58.Size = New System.Drawing.Size(50, 13)
        Me.Label58.TabIndex = 315
        Me.Label58.Text = "Margen"
        '
        'Label59
        '
        Me.Label59.AutoSize = True
        Me.Label59.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label59.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.Label59.Location = New System.Drawing.Point(18, 103)
        Me.Label59.Name = "Label59"
        Me.Label59.Size = New System.Drawing.Size(68, 13)
        Me.Label59.TabIndex = 314
        Me.Label59.Text = "Costo Neto"
        '
        'TabPage4
        '
        Me.TabPage4.Controls.Add(Me.grbTextos)
        Me.TabPage4.Location = New System.Drawing.Point(4, 22)
        Me.TabPage4.Name = "TabPage4"
        Me.TabPage4.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage4.Size = New System.Drawing.Size(1138, 584)
        Me.TabPage4.TabIndex = 3
        Me.TabPage4.Text = "Descripciones Servicio"
        Me.TabPage4.UseVisualStyleBackColor = True
        '
        'grbTextos
        '
        Me.grbTextos.BackColor = System.Drawing.Color.White
        Me.grbTextos.Controls.Add(Me.btnEliminarIdiomaTexto)
        Me.grbTextos.Controls.Add(Me.lblTitulo4)
        Me.grbTextos.Controls.Add(Me.dgvIdiom)
        Me.grbTextos.Dock = System.Windows.Forms.DockStyle.Fill
        Me.grbTextos.Location = New System.Drawing.Point(3, 3)
        Me.grbTextos.Name = "grbTextos"
        Me.grbTextos.Size = New System.Drawing.Size(1132, 578)
        Me.grbTextos.TabIndex = 1
        Me.grbTextos.TabStop = False
        '
        'btnEliminarIdiomaTexto
        '
        Me.btnEliminarIdiomaTexto.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnEliminarIdiomaTexto.Enabled = False
        Me.btnEliminarIdiomaTexto.Image = Global.SETours.My.Resources.Resources.page_delete
        Me.btnEliminarIdiomaTexto.Location = New System.Drawing.Point(16, 532)
        Me.btnEliminarIdiomaTexto.Name = "btnEliminarIdiomaTexto"
        Me.btnEliminarIdiomaTexto.Size = New System.Drawing.Size(27, 27)
        Me.btnEliminarIdiomaTexto.TabIndex = 200
        Me.btnEliminarIdiomaTexto.Tag = "Borrar"
        Me.ToolTip1.SetToolTip(Me.btnEliminarIdiomaTexto, "Eliminar Idioma seleccionado")
        Me.btnEliminarIdiomaTexto.UseVisualStyleBackColor = True
        '
        'lblTitulo4
        '
        Me.lblTitulo4.BackColor = System.Drawing.SystemColors.ControlDarkDark
        Me.lblTitulo4.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.lblTitulo4.Dock = System.Windows.Forms.DockStyle.Top
        Me.lblTitulo4.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTitulo4.ForeColor = System.Drawing.Color.White
        Me.lblTitulo4.Location = New System.Drawing.Point(3, 16)
        Me.lblTitulo4.Name = "lblTitulo4"
        Me.lblTitulo4.Size = New System.Drawing.Size(1126, 22)
        Me.lblTitulo4.TabIndex = 197
        Me.lblTitulo4.Text = "Nuevo"
        '
        'dgvIdiom
        '
        Me.dgvIdiom.AllowUserToAddRows = False
        Me.dgvIdiom.AllowUserToDeleteRows = False
        Me.dgvIdiom.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        DataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.ControlDarkDark
        DataGridViewCellStyle4.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle4.ForeColor = System.Drawing.Color.White
        DataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvIdiom.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle4
        Me.dgvIdiom.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvIdiom.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.English, Me.French, Me.German, Me.Spanish, Me.NoHotel, Me.TxWebHotel, Me.TxDireccHotel, Me.TxTelfHotel1, Me.TxTelfHotel2, Me.FeHoraChkInHotel, Me.FeHoraChkOutHotel, Me.CoTipoDesaHotel, Me.CoCiudadHotel})
        DataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle5.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle5.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle5.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle5.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvIdiom.DefaultCellStyle = DataGridViewCellStyle5
        Me.dgvIdiom.Location = New System.Drawing.Point(16, 44)
        Me.dgvIdiom.Name = "dgvIdiom"
        DataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle6.BackColor = System.Drawing.SystemColors.ControlDarkDark
        DataGridViewCellStyle6.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle6.ForeColor = System.Drawing.Color.White
        DataGridViewCellStyle6.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle6.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvIdiom.RowHeadersDefaultCellStyle = DataGridViewCellStyle6
        Me.dgvIdiom.Size = New System.Drawing.Size(842, 482)
        Me.dgvIdiom.TabIndex = 3
        Me.dgvIdiom.Tag = "Borrar"
        '
        'English
        '
        Me.English.DataPropertyName = "English"
        Me.English.HeaderText = "ENGLISH"
        Me.English.Name = "English"
        Me.English.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.English.Width = 150
        '
        'French
        '
        Me.French.DataPropertyName = "French"
        Me.French.HeaderText = "FRENCH"
        Me.French.Name = "French"
        Me.French.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.French.Width = 150
        '
        'German
        '
        Me.German.DataPropertyName = "German"
        Me.German.HeaderText = "GERMAN"
        Me.German.Name = "German"
        Me.German.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.German.Width = 150
        '
        'Spanish
        '
        Me.Spanish.DataPropertyName = "Spanish"
        Me.Spanish.HeaderText = "SPANISH"
        Me.Spanish.Name = "Spanish"
        Me.Spanish.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.Spanish.Width = 150
        '
        'NoHotel
        '
        Me.NoHotel.DataPropertyName = "NoHotel"
        Me.NoHotel.HeaderText = "HOTEL"
        Me.NoHotel.Name = "NoHotel"
        Me.NoHotel.Visible = False
        '
        'TxWebHotel
        '
        Me.TxWebHotel.DataPropertyName = "TxWebHotel"
        Me.TxWebHotel.HeaderText = "WEB"
        Me.TxWebHotel.Name = "TxWebHotel"
        Me.TxWebHotel.Visible = False
        '
        'TxDireccHotel
        '
        Me.TxDireccHotel.DataPropertyName = "TxDireccHotel"
        Me.TxDireccHotel.HeaderText = "TxDireccHotel"
        Me.TxDireccHotel.Name = "TxDireccHotel"
        Me.TxDireccHotel.Visible = False
        '
        'TxTelfHotel1
        '
        Me.TxTelfHotel1.DataPropertyName = "TxTelfHotel1"
        Me.TxTelfHotel1.HeaderText = "TxTelfHotel1"
        Me.TxTelfHotel1.Name = "TxTelfHotel1"
        Me.TxTelfHotel1.Visible = False
        '
        'TxTelfHotel2
        '
        Me.TxTelfHotel2.DataPropertyName = "TxTelfHotel2"
        Me.TxTelfHotel2.HeaderText = "TxTelfHotel2"
        Me.TxTelfHotel2.Name = "TxTelfHotel2"
        Me.TxTelfHotel2.Visible = False
        '
        'FeHoraChkInHotel
        '
        Me.FeHoraChkInHotel.DataPropertyName = "FeHoraChkInHotel"
        Me.FeHoraChkInHotel.HeaderText = "FeHoraChkInHotel"
        Me.FeHoraChkInHotel.Name = "FeHoraChkInHotel"
        Me.FeHoraChkInHotel.Visible = False
        '
        'FeHoraChkOutHotel
        '
        Me.FeHoraChkOutHotel.DataPropertyName = "FeHoraChkOutHotel"
        Me.FeHoraChkOutHotel.HeaderText = "FeHoraChkOutHotel"
        Me.FeHoraChkOutHotel.Name = "FeHoraChkOutHotel"
        Me.FeHoraChkOutHotel.Visible = False
        '
        'CoTipoDesaHotel
        '
        Me.CoTipoDesaHotel.DataPropertyName = "CoTipoDesaHotel"
        Me.CoTipoDesaHotel.HeaderText = "CoTipoDesaHotel"
        Me.CoTipoDesaHotel.Name = "CoTipoDesaHotel"
        Me.CoTipoDesaHotel.Visible = False
        '
        'CoCiudadHotel
        '
        Me.CoCiudadHotel.DataPropertyName = "CoCiudadHotel"
        Me.CoCiudadHotel.HeaderText = "CoCiudadHotel"
        Me.CoCiudadHotel.Name = "CoCiudadHotel"
        Me.CoCiudadHotel.Visible = False
        '
        'tpgRecordat
        '
        Me.tpgRecordat.Controls.Add(Me.grbRecordObserv)
        Me.tpgRecordat.Location = New System.Drawing.Point(4, 22)
        Me.tpgRecordat.Name = "tpgRecordat"
        Me.tpgRecordat.Size = New System.Drawing.Size(1138, 584)
        Me.tpgRecordat.TabIndex = 4
        Me.tpgRecordat.Text = "Recordatorios y Observaciones"
        Me.tpgRecordat.UseVisualStyleBackColor = True
        '
        'grbRecordObserv
        '
        Me.grbRecordObserv.BackColor = System.Drawing.Color.White
        Me.grbRecordObserv.Controls.Add(Me.Label8)
        Me.grbRecordObserv.Controls.Add(Me.txtObservInterno)
        Me.grbRecordObserv.Controls.Add(Me.Label7)
        Me.grbRecordObserv.Controls.Add(Me.txtObservBiblia)
        Me.grbRecordObserv.Controls.Add(Me.Label6)
        Me.grbRecordObserv.Controls.Add(Me.txtObservVoucher)
        Me.grbRecordObserv.Dock = System.Windows.Forms.DockStyle.Fill
        Me.grbRecordObserv.Location = New System.Drawing.Point(0, 0)
        Me.grbRecordObserv.Name = "grbRecordObserv"
        Me.grbRecordObserv.Size = New System.Drawing.Size(1138, 584)
        Me.grbRecordObserv.TabIndex = 0
        Me.grbRecordObserv.TabStop = False
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.Label8.Location = New System.Drawing.Point(15, 162)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(136, 13)
        Me.Label8.TabIndex = 345
        Me.Label8.Text = "Observaciones Interno"
        '
        'txtObservInterno
        '
        Me.txtObservInterno.BackColor = System.Drawing.SystemColors.Window
        Me.txtObservInterno.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtObservInterno.Location = New System.Drawing.Point(18, 178)
        Me.txtObservInterno.Multiline = True
        Me.txtObservInterno.Name = "txtObservInterno"
        Me.txtObservInterno.Size = New System.Drawing.Size(453, 46)
        Me.txtObservInterno.TabIndex = 344
        Me.txtObservInterno.Tag = "Borrar"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.Label7.Location = New System.Drawing.Point(15, 92)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(123, 13)
        Me.Label7.TabIndex = 343
        Me.Label7.Text = "Observaciones Biblia"
        '
        'txtObservBiblia
        '
        Me.txtObservBiblia.BackColor = System.Drawing.SystemColors.Window
        Me.txtObservBiblia.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtObservBiblia.Location = New System.Drawing.Point(18, 108)
        Me.txtObservBiblia.Multiline = True
        Me.txtObservBiblia.Name = "txtObservBiblia"
        Me.txtObservBiblia.Size = New System.Drawing.Size(453, 46)
        Me.txtObservBiblia.TabIndex = 342
        Me.txtObservBiblia.Tag = "Borrar"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.Label6.Location = New System.Drawing.Point(15, 23)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(139, 13)
        Me.Label6.TabIndex = 341
        Me.Label6.Text = "Observaciones Voucher"
        '
        'txtObservVoucher
        '
        Me.txtObservVoucher.BackColor = System.Drawing.SystemColors.Window
        Me.txtObservVoucher.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtObservVoucher.Location = New System.Drawing.Point(18, 39)
        Me.txtObservVoucher.Multiline = True
        Me.txtObservVoucher.Name = "txtObservVoucher"
        Me.txtObservVoucher.Size = New System.Drawing.Size(453, 46)
        Me.txtObservVoucher.TabIndex = 340
        Me.txtObservVoucher.Tag = "Borrar"
        '
        'tpgAcomVehi
        '
        Me.tpgAcomVehi.Controls.Add(Me.GroupBox4)
        Me.tpgAcomVehi.Location = New System.Drawing.Point(4, 22)
        Me.tpgAcomVehi.Name = "tpgAcomVehi"
        Me.tpgAcomVehi.Size = New System.Drawing.Size(1138, 584)
        Me.tpgAcomVehi.TabIndex = 5
        Me.tpgAcomVehi.Text = "Acomodo Vehículos"
        Me.tpgAcomVehi.UseVisualStyleBackColor = True
        '
        'GroupBox4
        '
        Me.GroupBox4.Controls.Add(Me.lblEtiqAcomVehPax)
        Me.GroupBox4.Controls.Add(Me.grbAcomVehicE)
        Me.GroupBox4.Controls.Add(Me.tvwAcoVeh)
        Me.GroupBox4.Controls.Add(Me.chkSelCopiaAcoVeh)
        Me.GroupBox4.Controls.Add(Me.lvwServAcomVehi)
        Me.GroupBox4.Controls.Add(Me.grbAcomVehicI)
        Me.GroupBox4.Dock = System.Windows.Forms.DockStyle.Fill
        Me.GroupBox4.Location = New System.Drawing.Point(0, 0)
        Me.GroupBox4.Name = "GroupBox4"
        Me.GroupBox4.Size = New System.Drawing.Size(1138, 584)
        Me.GroupBox4.TabIndex = 0
        Me.GroupBox4.TabStop = False
        '
        'lblEtiqAcomVehPax
        '
        Me.lblEtiqAcomVehPax.AutoSize = True
        Me.lblEtiqAcomVehPax.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEtiqAcomVehPax.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.lblEtiqAcomVehPax.Location = New System.Drawing.Point(11, 206)
        Me.lblEtiqAcomVehPax.Name = "lblEtiqAcomVehPax"
        Me.lblEtiqAcomVehPax.Size = New System.Drawing.Size(84, 13)
        Me.lblEtiqAcomVehPax.TabIndex = 381
        Me.lblEtiqAcomVehPax.Text = "Acomodo Pax"
        '
        'grbAcomVehicE
        '
        Me.grbAcomVehicE.Controls.Add(Me.Label16)
        Me.grbAcomVehicE.Controls.Add(Me.txtGruposPax)
        Me.grbAcomVehicE.Controls.Add(Me.Label15)
        Me.grbAcomVehicE.Controls.Add(Me.txtPaxTotal)
        Me.grbAcomVehicE.Controls.Add(Me.Label11)
        Me.grbAcomVehicE.Location = New System.Drawing.Point(6, 16)
        Me.grbAcomVehicE.Name = "grbAcomVehicE"
        Me.grbAcomVehicE.Size = New System.Drawing.Size(427, 59)
        Me.grbAcomVehicE.TabIndex = 4
        Me.grbAcomVehicE.TabStop = False
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label16.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.Label16.Location = New System.Drawing.Point(225, 16)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(46, 13)
        Me.Label16.TabIndex = 225
        Me.Label16.Text = "grupos"
        '
        'txtGruposPax
        '
        Me.txtGruposPax.BackColor = System.Drawing.SystemColors.Window
        Me.txtGruposPax.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtGruposPax.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtGruposPax.Location = New System.Drawing.Point(181, 13)
        Me.txtGruposPax.MaxLength = 2
        Me.txtGruposPax.Name = "txtGruposPax"
        Me.txtGruposPax.Size = New System.Drawing.Size(38, 21)
        Me.txtGruposPax.TabIndex = 224
        Me.txtGruposPax.Tag = "Borrar"
        Me.txtGruposPax.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label15.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.Label15.Location = New System.Drawing.Point(116, 16)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(59, 13)
        Me.Label15.TabIndex = 223
        Me.Label15.Text = "dividir en"
        '
        'txtPaxTotal
        '
        Me.txtPaxTotal.BackColor = System.Drawing.SystemColors.Window
        Me.txtPaxTotal.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtPaxTotal.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtPaxTotal.Location = New System.Drawing.Point(72, 13)
        Me.txtPaxTotal.MaxLength = 3
        Me.txtPaxTotal.Name = "txtPaxTotal"
        Me.txtPaxTotal.Size = New System.Drawing.Size(38, 21)
        Me.txtPaxTotal.TabIndex = 222
        Me.txtPaxTotal.Tag = "Borrar"
        Me.txtPaxTotal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label11.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.Label11.Location = New System.Drawing.Point(6, 16)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(60, 13)
        Me.Label11.TabIndex = 166
        Me.Label11.Text = "Pax Total"
        '
        'tvwAcoVeh
        '
        Me.tvwAcoVeh.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.tvwAcoVeh.CheckBoxes = True
        Me.tvwAcoVeh.ImageIndex = 0
        Me.tvwAcoVeh.ImageList = Me.ImageList1
        Me.tvwAcoVeh.Location = New System.Drawing.Point(6, 222)
        Me.tvwAcoVeh.Name = "tvwAcoVeh"
        Me.tvwAcoVeh.SelectedImageIndex = 0
        Me.tvwAcoVeh.Size = New System.Drawing.Size(318, 356)
        Me.tvwAcoVeh.TabIndex = 380
        '
        'chkSelCopiaAcoVeh
        '
        Me.chkSelCopiaAcoVeh.AutoSize = True
        Me.chkSelCopiaAcoVeh.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkSelCopiaAcoVeh.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.chkSelCopiaAcoVeh.Location = New System.Drawing.Point(517, 22)
        Me.chkSelCopiaAcoVeh.Name = "chkSelCopiaAcoVeh"
        Me.chkSelCopiaAcoVeh.Size = New System.Drawing.Size(266, 17)
        Me.chkSelCopiaAcoVeh.TabIndex = 379
        Me.chkSelCopiaAcoVeh.Tag = "Borrar"
        Me.chkSelCopiaAcoVeh.Text = "Seleccionar servicios a copiar el acomodo.."
        Me.chkSelCopiaAcoVeh.UseVisualStyleBackColor = True
        '
        'lvwServAcomVehi
        '
        Me.lvwServAcomVehi.CheckBoxes = True
        Me.lvwServAcomVehi.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lvwServAcomVehi.LargeImageList = Me.ImageList1
        Me.lvwServAcomVehi.Location = New System.Drawing.Point(517, 39)
        Me.lvwServAcomVehi.Name = "lvwServAcomVehi"
        Me.lvwServAcomVehi.Size = New System.Drawing.Size(450, 253)
        Me.lvwServAcomVehi.SmallImageList = Me.ImageList1
        Me.lvwServAcomVehi.TabIndex = 377
        Me.lvwServAcomVehi.Tag = "Borrar"
        Me.lvwServAcomVehi.UseCompatibleStateImageBehavior = False
        Me.lvwServAcomVehi.View = System.Windows.Forms.View.Details
        Me.lvwServAcomVehi.Visible = False
        '
        'grbAcomVehicI
        '
        Me.grbAcomVehicI.Controls.Add(Me.dgvAcoVeh)
        Me.grbAcomVehicI.Location = New System.Drawing.Point(6, 16)
        Me.grbAcomVehicI.Name = "grbAcomVehicI"
        Me.grbAcomVehicI.Size = New System.Drawing.Size(497, 187)
        Me.grbAcomVehicI.TabIndex = 3
        Me.grbAcomVehicI.TabStop = False
        '
        'dgvAcoVeh
        '
        Me.dgvAcoVeh.AllowUserToAddRows = False
        Me.dgvAcoVeh.AllowUserToDeleteRows = False
        Me.dgvAcoVeh.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        DataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle7.BackColor = System.Drawing.SystemColors.ControlDarkDark
        DataGridViewCellStyle7.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle7.ForeColor = System.Drawing.Color.White
        DataGridViewCellStyle7.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle7.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle7.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvAcoVeh.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle7
        Me.dgvAcoVeh.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvAcoVeh.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.QtVehiculos, Me.NuVehiculo, Me.NoVehiculo, Me.QtCapacidad, Me.QtPax, Me.FlGuia})
        DataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle9.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle9.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle9.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle9.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle9.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle9.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgvAcoVeh.DefaultCellStyle = DataGridViewCellStyle9
        Me.dgvAcoVeh.Location = New System.Drawing.Point(6, 19)
        Me.dgvAcoVeh.Name = "dgvAcoVeh"
        DataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle10.BackColor = System.Drawing.SystemColors.ControlDarkDark
        DataGridViewCellStyle10.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle10.ForeColor = System.Drawing.Color.White
        DataGridViewCellStyle10.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle10.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle10.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvAcoVeh.RowHeadersDefaultCellStyle = DataGridViewCellStyle10
        Me.dgvAcoVeh.Size = New System.Drawing.Size(477, 161)
        Me.dgvAcoVeh.TabIndex = 2
        '
        'QtVehiculos
        '
        Me.QtVehiculos.HeaderText = "Cant."
        Me.QtVehiculos.MaxInputLength = 2
        Me.QtVehiculos.Name = "QtVehiculos"
        Me.QtVehiculos.Width = 40
        '
        'NuVehiculo
        '
        Me.NuVehiculo.HeaderText = "NuVehiculo"
        Me.NuVehiculo.Name = "NuVehiculo"
        Me.NuVehiculo.ReadOnly = True
        Me.NuVehiculo.Visible = False
        '
        'NoVehiculo
        '
        Me.NoVehiculo.HeaderText = "Vehículo"
        Me.NoVehiculo.Name = "NoVehiculo"
        Me.NoVehiculo.ReadOnly = True
        Me.NoVehiculo.Width = 200
        '
        'QtCapacidad
        '
        DataGridViewCellStyle8.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        DataGridViewCellStyle8.ForeColor = System.Drawing.Color.Gray
        DataGridViewCellStyle8.SelectionBackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        DataGridViewCellStyle8.SelectionForeColor = System.Drawing.Color.Gray
        Me.QtCapacidad.DefaultCellStyle = DataGridViewCellStyle8
        Me.QtCapacidad.HeaderText = "Capacidad"
        Me.QtCapacidad.Name = "QtCapacidad"
        Me.QtCapacidad.ReadOnly = True
        Me.QtCapacidad.Width = 70
        '
        'QtPax
        '
        Me.QtPax.HeaderText = "Pax"
        Me.QtPax.MaxInputLength = 2
        Me.QtPax.Name = "QtPax"
        Me.QtPax.Width = 50
        '
        'FlGuia
        '
        Me.FlGuia.HeaderText = "Guía"
        Me.FlGuia.Name = "FlGuia"
        Me.FlGuia.ReadOnly = True
        Me.FlGuia.Visible = False
        Me.FlGuia.Width = 40
        '
        'btnReCalcular
        '
        Me.btnReCalcular.Image = Global.SETours.My.Resources.Resources.arrow_refresh
        Me.btnReCalcular.Location = New System.Drawing.Point(5, 35)
        Me.btnReCalcular.Name = "btnReCalcular"
        Me.btnReCalcular.Size = New System.Drawing.Size(27, 27)
        Me.btnReCalcular.TabIndex = 164
        Me.ToolTip1.SetToolTip(Me.btnReCalcular, "Recalcular Montos")
        Me.btnReCalcular.UseVisualStyleBackColor = True
        '
        'btnSalir
        '
        Me.btnSalir.Image = Global.SETours.My.Resources.Resources.door_out
        Me.btnSalir.Location = New System.Drawing.Point(5, 107)
        Me.btnSalir.Name = "btnSalir"
        Me.btnSalir.Size = New System.Drawing.Size(27, 27)
        Me.btnSalir.TabIndex = 162
        Me.ToolTip1.SetToolTip(Me.btnSalir, "Salir (ESC)")
        Me.btnSalir.UseVisualStyleBackColor = True
        '
        'btnAceptar
        '
        Me.btnAceptar.Enabled = False
        Me.btnAceptar.Image = Global.SETours.My.Resources.Resources.accept
        Me.btnAceptar.Location = New System.Drawing.Point(5, 5)
        Me.btnAceptar.Name = "btnAceptar"
        Me.btnAceptar.Size = New System.Drawing.Size(27, 27)
        Me.btnAceptar.TabIndex = 161
        Me.ToolTip1.SetToolTip(Me.btnAceptar, "Aceptar (F6)")
        Me.btnAceptar.UseVisualStyleBackColor = True
        '
        'ofdRuta
        '
        Me.ofdRuta.FileName = "OpenFileDialog1"
        '
        'ErrPrvSS
        '
        Me.ErrPrvSS.ContainerControl = Me
        Me.ErrPrvSS.Icon = CType(resources.GetObject("ErrPrvSS.Icon"), System.Drawing.Icon)
        '
        'frmIngDetCotizacionDatoMem
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1020, 614)
        Me.Controls.Add(Me.btnReCalcular)
        Me.Controls.Add(Me.Tab)
        Me.Controls.Add(Me.btnSalir)
        Me.Controls.Add(Me.btnAceptar)
        Me.Controls.Add(Me.btnQuiebres)
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmIngDetCotizacionDatoMem"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Ingreso de Detalle de Cotización"
        CType(Me.ErrPrv, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Tab.ResumeLayout(False)
        Me.TabPage1.ResumeLayout(False)
        Me.grbDatosGenerales.ResumeLayout(False)
        Me.grbDatosGenerales.PerformLayout()
        Me.grbBus_Guia.ResumeLayout(False)
        Me.grbBus_Guia.PerformLayout()
        Me.grbChecksVoucBiblia.ResumeLayout(False)
        Me.grbChecksVoucBiblia.PerformLayout()
        Me.grbPax.ResumeLayout(False)
        Me.grbPax.PerformLayout()
        Me.TabPage2.ResumeLayout(False)
        Me.grbQuiebres.ResumeLayout(False)
        Me.grbQuiebres.PerformLayout()
        CType(Me.dgvQui, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabPage3.ResumeLayout(False)
        Me.grbMontos.ResumeLayout(False)
        Me.grbMontos.PerformLayout()
        Me.TabPage4.ResumeLayout(False)
        Me.grbTextos.ResumeLayout(False)
        CType(Me.dgvIdiom, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tpgRecordat.ResumeLayout(False)
        Me.grbRecordObserv.ResumeLayout(False)
        Me.grbRecordObserv.PerformLayout()
        Me.tpgAcomVehi.ResumeLayout(False)
        Me.GroupBox4.ResumeLayout(False)
        Me.GroupBox4.PerformLayout()
        Me.grbAcomVehicE.ResumeLayout(False)
        Me.grbAcomVehicE.PerformLayout()
        Me.grbAcomVehicI.ResumeLayout(False)
        CType(Me.dgvAcoVeh, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ErrPrvSS, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents btnSalir As System.Windows.Forms.Button
    Friend WithEvents btnAceptar As System.Windows.Forms.Button
    Friend WithEvents ErrPrv As System.Windows.Forms.ErrorProvider
    Friend WithEvents Tab As System.Windows.Forms.TabControl
    Friend WithEvents TabPage1 As System.Windows.Forms.TabPage
    Friend WithEvents grbDatosGenerales As System.Windows.Forms.GroupBox
    Friend WithEvents txtServicio As System.Windows.Forms.TextBox
    Friend WithEvents lblServicioEtiq As System.Windows.Forms.Label
    Friend WithEvents txtRutaDocSustento As System.Windows.Forms.TextBox
    Friend WithEvents lblRutaDocSustentoEtiq As System.Windows.Forms.Label
    Friend WithEvents lblImpuestosEtiq3 As System.Windows.Forms.Label
    Friend WithEvents lblImpuestosEtiq2 As System.Windows.Forms.Label
    Friend WithEvents lblImpuestosEtiq1 As System.Windows.Forms.Label
    Friend WithEvents lblSTMargenImpto As System.Windows.Forms.Label
    Friend WithEvents lblSTCRImpto As System.Windows.Forms.Label
    Friend WithEvents lblSSMargenLiberadoImpto As System.Windows.Forms.Label
    Friend WithEvents lblSSCLImpto As System.Windows.Forms.Label
    Friend WithEvents lblSSMargenImpto As System.Windows.Forms.Label
    Friend WithEvents lblSSCRImpto As System.Windows.Forms.Label
    Friend WithEvents lblMargenLiberadoImpto As System.Windows.Forms.Label
    Friend WithEvents lblCostoLiberadoImpto As System.Windows.Forms.Label
    Friend WithEvents lblMargenImpto As System.Windows.Forms.Label
    Friend WithEvents Label19 As System.Windows.Forms.Label
    Friend WithEvents dtpDia As System.Windows.Forms.DateTimePicker
    Friend WithEvents txtMotivoEspecial As System.Windows.Forms.TextBox
    Friend WithEvents lblMotivoEspecialEtiq As System.Windows.Forms.Label
    Friend WithEvents chkEspecial As System.Windows.Forms.CheckBox
    Friend WithEvents cboIDIdioma As System.Windows.Forms.ComboBox
    Friend WithEvents lblIdiomaEtiq As System.Windows.Forms.Label
    Friend WithEvents lblSTTotalEtiq As System.Windows.Forms.Label
    Friend WithEvents lblSTMargenEtiq As System.Windows.Forms.Label
    Friend WithEvents lblSTCREtiq As System.Windows.Forms.Label
    Friend WithEvents lblSSTotalEtiq As System.Windows.Forms.Label
    Friend WithEvents lblSSMLEtiq As System.Windows.Forms.Label
    Friend WithEvents lblSSCLEtiq As System.Windows.Forms.Label
    Friend WithEvents lblSSMargenEtiq As System.Windows.Forms.Label
    Friend WithEvents lblSSCREtiq As System.Windows.Forms.Label
    Friend WithEvents lblTotalEtiq As System.Windows.Forms.Label
    Friend WithEvents lblMargenLiberadoEtiq As System.Windows.Forms.Label
    Friend WithEvents lblCostoLiberadoEtiq As System.Windows.Forms.Label
    Friend WithEvents lblMargenEtiq As System.Windows.Forms.Label
    Friend WithEvents lblCostoRealEtiq As System.Windows.Forms.Label
    Friend WithEvents txtSSCR As System.Windows.Forms.TextBox
    Friend WithEvents txtSTMargen As System.Windows.Forms.TextBox
    Friend WithEvents txtSSCL As System.Windows.Forms.TextBox
    Friend WithEvents txtSSMargen As System.Windows.Forms.TextBox
    Friend WithEvents txtSTCR As System.Windows.Forms.TextBox
    Friend WithEvents txtSTTotal As System.Windows.Forms.TextBox
    Friend WithEvents txtSSTotal As System.Windows.Forms.TextBox
    Friend WithEvents txtSSMargenLiberado As System.Windows.Forms.TextBox
    Friend WithEvents txtMargenAplicado As System.Windows.Forms.TextBox
    Friend WithEvents txtCostoReal As System.Windows.Forms.TextBox
    Friend WithEvents cboCiudad As System.Windows.Forms.ComboBox
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents cboPais As System.Windows.Forms.ComboBox
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents cboTipoProv As System.Windows.Forms.ComboBox
    Friend WithEvents Label22 As System.Windows.Forms.Label
    Friend WithEvents btnProveedor As System.Windows.Forms.Button
    Friend WithEvents lblProveedor As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents lblTitulo As System.Windows.Forms.Label
    Friend WithEvents TabPage2 As System.Windows.Forms.TabPage
    Friend WithEvents grbQuiebres As System.Windows.Forms.GroupBox
    Friend WithEvents dgvQui As System.Windows.Forms.DataGridView
    Friend WithEvents btnQuiebres As System.Windows.Forms.Button
    Friend WithEvents lblTitulo2 As System.Windows.Forms.Label
    Friend WithEvents Label24 As System.Windows.Forms.Label
    Friend WithEvents Label25 As System.Windows.Forms.Label
    Friend WithEvents Label26 As System.Windows.Forms.Label
    Friend WithEvents lblSTMargenImptoQ As System.Windows.Forms.Label
    Friend WithEvents lblSTCRImptoQ As System.Windows.Forms.Label
    Friend WithEvents lblSSMLImptoQ As System.Windows.Forms.Label
    Friend WithEvents lblSSCLImptoQ As System.Windows.Forms.Label
    Friend WithEvents lblSSMargenImptoQ As System.Windows.Forms.Label
    Friend WithEvents lblSSCRImptoQ As System.Windows.Forms.Label
    Friend WithEvents lblMargenLiberadoImptoQ As System.Windows.Forms.Label
    Friend WithEvents lblCostoLiberadoImptoQ As System.Windows.Forms.Label
    Friend WithEvents lblMargenImptoQ As System.Windows.Forms.Label
    Friend WithEvents lblCostoRealImptoQ As System.Windows.Forms.Label
    Friend WithEvents Label40 As System.Windows.Forms.Label
    Friend WithEvents Label41 As System.Windows.Forms.Label
    Friend WithEvents Label42 As System.Windows.Forms.Label
    Friend WithEvents Label43 As System.Windows.Forms.Label
    Friend WithEvents Label44 As System.Windows.Forms.Label
    Friend WithEvents Label45 As System.Windows.Forms.Label
    Friend WithEvents Label47 As System.Windows.Forms.Label
    Friend WithEvents Label48 As System.Windows.Forms.Label
    Friend WithEvents Label49 As System.Windows.Forms.Label
    Friend WithEvents Label50 As System.Windows.Forms.Label
    Friend WithEvents txtMargenAplicadoQ As System.Windows.Forms.TextBox
    Friend WithEvents txtCostoRealQ As System.Windows.Forms.TextBox
    Friend WithEvents lblTotImptoQ As System.Windows.Forms.Label
    Friend WithEvents Label27 As System.Windows.Forms.Label
    Friend WithEvents lblTotImpto As System.Windows.Forms.Label
    Friend WithEvents lblTotImptoEtiq As System.Windows.Forms.Label
    Friend WithEvents btnReCalcular As System.Windows.Forms.Button
    Friend WithEvents ToolTip1 As System.Windows.Forms.ToolTip
    Friend WithEvents btnRutaArchivo As System.Windows.Forms.Button
    Friend WithEvents ofdRuta As System.Windows.Forms.OpenFileDialog
    Friend WithEvents txtCostoRealImpto As System.Windows.Forms.TextBox
    Friend WithEvents TabPage3 As System.Windows.Forms.TabPage
    Friend WithEvents grbMontos As System.Windows.Forms.GroupBox
    Friend WithEvents lblTitulo3 As System.Windows.Forms.Label
    Friend WithEvents lblTotImpto2 As System.Windows.Forms.Label
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents lblImpuestosEtiq22 As System.Windows.Forms.Label
    Friend WithEvents lblImpuestosEtiq12 As System.Windows.Forms.Label
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents lblSTMargenImpto2 As System.Windows.Forms.Label
    Friend WithEvents lblSTCRImpto2 As System.Windows.Forms.Label
    Friend WithEvents lblSSMargenLiberadoImpto2 As System.Windows.Forms.Label
    Friend WithEvents lblSSCLImpto2 As System.Windows.Forms.Label
    Friend WithEvents lblSSMargenImpto2 As System.Windows.Forms.Label
    Friend WithEvents lblSSCRImpto2 As System.Windows.Forms.Label
    Friend WithEvents lblMargenLiberadoImpto2 As System.Windows.Forms.Label
    Friend WithEvents lblCostoLiberadoImpto2 As System.Windows.Forms.Label
    Friend WithEvents lblMargenImpto2 As System.Windows.Forms.Label
    Friend WithEvents lblSTTotalEtiq2 As System.Windows.Forms.Label
    Friend WithEvents lblSTMargenEtiq2 As System.Windows.Forms.Label
    Friend WithEvents lblSTCREtiq2 As System.Windows.Forms.Label
    Friend WithEvents lblSSTotalEtiq2 As System.Windows.Forms.Label
    Friend WithEvents lblSSMLEtiq2 As System.Windows.Forms.Label
    Friend WithEvents lblSSCLEtiq2 As System.Windows.Forms.Label
    Friend WithEvents lblSSMargenEtiq2 As System.Windows.Forms.Label
    Friend WithEvents lblSSCREtiq2 As System.Windows.Forms.Label
    Friend WithEvents Label54 As System.Windows.Forms.Label
    Friend WithEvents Label56 As System.Windows.Forms.Label
    Friend WithEvents Label57 As System.Windows.Forms.Label
    Friend WithEvents Label58 As System.Windows.Forms.Label
    Friend WithEvents Label59 As System.Windows.Forms.Label
    Friend WithEvents lblCostoenDobleEtiq As System.Windows.Forms.Label
    Friend WithEvents lblTituloCostoTriple As System.Windows.Forms.Label
    Friend WithEvents lblTituloCostoSimple As System.Windows.Forms.Label
    Friend WithEvents GroupBox3 As System.Windows.Forms.GroupBox
    Friend WithEvents lblMargen As System.Windows.Forms.Label
    Friend WithEvents lblCostoLiberado As System.Windows.Forms.Label
    Friend WithEvents lblMargenLiberado As System.Windows.Forms.Label
    Friend WithEvents lblTotal As System.Windows.Forms.Label
    Friend WithEvents lblMargenQ As System.Windows.Forms.Label
    Friend WithEvents lblCostoLiberadoQ As System.Windows.Forms.Label
    Friend WithEvents lblMargenLiberadoQ As System.Windows.Forms.Label
    Friend WithEvents lblTotalQ As System.Windows.Forms.Label
    Friend WithEvents lblSSTotalQ As System.Windows.Forms.Label
    Friend WithEvents lblSSMLQ As System.Windows.Forms.Label
    Friend WithEvents lblSSCLQ As System.Windows.Forms.Label
    Friend WithEvents lblSSMargenQ As System.Windows.Forms.Label
    Friend WithEvents lblSSCRQ As System.Windows.Forms.Label
    Friend WithEvents lblTotal2 As System.Windows.Forms.Label
    Friend WithEvents lblCostoLiberado2 As System.Windows.Forms.Label
    Friend WithEvents lblMargen2 As System.Windows.Forms.Label
    Friend WithEvents lblCostoReal2 As System.Windows.Forms.Label
    Friend WithEvents lblMargenLiberado2 As System.Windows.Forms.Label
    Friend WithEvents lblCostoRealImpto2 As System.Windows.Forms.Label
    Friend WithEvents lblMargenAplicado2 As System.Windows.Forms.Label
    Friend WithEvents lblSSCL2 As System.Windows.Forms.Label
    Friend WithEvents lblSSMargen2 As System.Windows.Forms.Label
    Friend WithEvents lblSSCR2 As System.Windows.Forms.Label
    Friend WithEvents lblSSMargenLiberado2 As System.Windows.Forms.Label
    Friend WithEvents lblSSTotal2 As System.Windows.Forms.Label
    Friend WithEvents lblSTCR2 As System.Windows.Forms.Label
    Friend WithEvents lblSTMargen2 As System.Windows.Forms.Label
    Friend WithEvents lblSTTotal2 As System.Windows.Forms.Label
    Friend WithEvents ImageList1 As System.Windows.Forms.ImageList
    Friend WithEvents chkSelPax As System.Windows.Forms.CheckBox
    Friend WithEvents TabPage4 As System.Windows.Forms.TabPage
    Friend WithEvents grbTextos As System.Windows.Forms.GroupBox
    Friend WithEvents btnEliminarIdiomaTexto As System.Windows.Forms.Button
    Friend WithEvents lblTitulo4 As System.Windows.Forms.Label
    Friend WithEvents dgvIdiom As System.Windows.Forms.DataGridView
    Friend WithEvents chkCena As System.Windows.Forms.CheckBox
    Friend WithEvents chkAlmuerzo As System.Windows.Forms.CheckBox
    Friend WithEvents chkDesayuno As System.Windows.Forms.CheckBox
    Friend WithEvents chkLonche As System.Windows.Forms.CheckBox
    Friend WithEvents cboIDUbigeoDes As System.Windows.Forms.ComboBox
    Friend WithEvents lblIDUbigeoDesEtiq As System.Windows.Forms.Label
    Friend WithEvents cboIDUbigeoOri As System.Windows.Forms.ComboBox
    Friend WithEvents lblIDUbigeoOriEtiq As System.Windows.Forms.Label
    Friend WithEvents lblTipoTransporteEtiq As System.Windows.Forms.Label
    Friend WithEvents cboTipoTransporte As System.Windows.Forms.ComboBox
    Friend WithEvents chkTransfer As System.Windows.Forms.CheckBox
    Friend WithEvents cboDestino As System.Windows.Forms.ComboBox
    Friend WithEvents lblDestinoEtiq As System.Windows.Forms.Label
    Friend WithEvents cboOrigen As System.Windows.Forms.ComboBox
    Friend WithEvents lblOrigenEtiq As System.Windows.Forms.Label
    Friend WithEvents txtSSTotalOrig As System.Windows.Forms.TextBox
    Friend WithEvents lblTotalOrig As System.Windows.Forms.Label
    Friend WithEvents txtSTTotalOrig As System.Windows.Forms.TextBox
    Friend WithEvents lblFechaOutEtiq As System.Windows.Forms.Label
    Friend WithEvents dtpFechaOut As System.Windows.Forms.DateTimePicker
    Friend WithEvents lblNoches As System.Windows.Forms.Label
    Friend WithEvents lblNochesEtiq As System.Windows.Forms.Label
    Friend WithEvents txtCantidad As System.Windows.Forms.TextBox
    Friend WithEvents lblCantidadEtiq As System.Windows.Forms.Label
    Friend WithEvents tpgRecordat As System.Windows.Forms.TabPage
    Friend WithEvents grbRecordObserv As System.Windows.Forms.GroupBox
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents txtObservVoucher As System.Windows.Forms.TextBox
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents txtObservInterno As System.Windows.Forms.TextBox
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents txtObservBiblia As System.Windows.Forms.TextBox
    Friend WithEvents dtpDiaHora As System.Windows.Forms.DateTimePicker
    Friend WithEvents grbChecksVoucBiblia As System.Windows.Forms.GroupBox
    Friend WithEvents chkVerBiblia As System.Windows.Forms.CheckBox
    Friend WithEvents chkVerVoucher As System.Windows.Forms.CheckBox
    Friend WithEvents chkIncGuia As System.Windows.Forms.CheckBox
    Friend WithEvents grbPax As System.Windows.Forms.GroupBox
    Friend WithEvents cboTipoLib As System.Windows.Forms.ComboBox
    Friend WithEvents lblTipoLibEtiq As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents txtNroLiberados As System.Windows.Forms.TextBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents txtNroPax As System.Windows.Forms.TextBox
    Friend WithEvents lvwPax As System.Windows.Forms.ListView
    Friend WithEvents lblSTTotalQ As System.Windows.Forms.Label
    Friend WithEvents lblSTMargenQ As System.Windows.Forms.Label
    Friend WithEvents lblSTCRQ As System.Windows.Forms.Label
    Friend WithEvents Label37 As System.Windows.Forms.Label
    Friend WithEvents Label38 As System.Windows.Forms.Label
    Friend WithEvents Label39 As System.Windows.Forms.Label
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents lblRedondeoTotal As System.Windows.Forms.Label
    Friend WithEvents lblRedondeo As System.Windows.Forms.Label
    Friend WithEvents IDCab_Q As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents IDDet_Q As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Accion As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Pax As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Liberados As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Tipo_Lib As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents CostoReal As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents CostoRealImpto As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Margen As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents MargenImpto As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents CostoLiberado As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents CostoLiberadoImpto As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents MargenLiberado As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents MargenLiberadoImpto As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents MargenAplicado As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Total As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents RedondeoTotal As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents SSCR As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents SSCRImpto As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents SSMargen As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents SSMargenImpto As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents SSCL As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents SSCLImpto As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents SSML As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents SSMargenLiberadoImpto As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents SSTotal As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents STCR As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents STCRImpto As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents STMargen As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents STMargenImpto As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents STTotal As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents TotImpto As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents lblRedondeoQ As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents grbSep4 As System.Windows.Forms.GroupBox
    Friend WithEvents lblRedondeoTotal2 As System.Windows.Forms.Label
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents grbBus_Guia As System.Windows.Forms.GroupBox
    Friend WithEvents lblEtiqBus As System.Windows.Forms.Label
    Friend WithEvents lblEtiqGuia As System.Windows.Forms.Label
    Friend WithEvents txtCantidadaPagar As System.Windows.Forms.TextBox
    Friend WithEvents lblCantPagarEtiq As System.Windows.Forms.Label
    Friend WithEvents lblDescVariante As System.Windows.Forms.Label
    Friend WithEvents lblVariante As System.Windows.Forms.Label
    Friend WithEvents Label17 As System.Windows.Forms.Label
    Friend WithEvents lblSSImpEtiq2 As System.Windows.Forms.Label
    Friend WithEvents lblMargenAplicado3 As System.Windows.Forms.Label
    Friend WithEvents lblMargenAplicado4 As System.Windows.Forms.Label
    Friend WithEvents lblSTImpEtiq2 As System.Windows.Forms.Label
    Friend WithEvents Label18 As System.Windows.Forms.Label
    Friend WithEvents lblTipoServicio As System.Windows.Forms.Label
    Friend WithEvents chkSelTodosPax As System.Windows.Forms.CheckBox
    Friend WithEvents lblTotalHabEtiq As System.Windows.Forms.Label
    Friend WithEvents lblIGVHabEtiq As System.Windows.Forms.Label
    Friend WithEvents lblNetoHabEtiq As System.Windows.Forms.Label
    Friend WithEvents lblTotalHab As System.Windows.Forms.Label
    Friend WithEvents txtIgvHab As System.Windows.Forms.TextBox
    Friend WithEvents txtNetoHab As System.Windows.Forms.TextBox
    Friend WithEvents lblIgvTotal As System.Windows.Forms.Label
    Friend WithEvents lblIgvTotalEtiq As System.Windows.Forms.Label
    Friend WithEvents lblNetoTotal As System.Windows.Forms.Label
    Friend WithEvents lblNetoTotalEtiq As System.Windows.Forms.Label
    Friend WithEvents lblTotalR As System.Windows.Forms.Label
    Friend WithEvents lblTotalMonEtiq As System.Windows.Forms.Label
    Friend WithEvents cboGuia As System.Windows.Forms.ComboBox
    Friend WithEvents CboBusVehiculo As System.Windows.Forms.ComboBox
    Friend WithEvents txtTotalHab As System.Windows.Forms.TextBox
    Friend WithEvents chkEditarTotalHab As System.Windows.Forms.CheckBox
    Friend WithEvents chkAcomodoEspecial As System.Windows.Forms.CheckBox
    Friend WithEvents btnVerAcomodoEspecial As System.Windows.Forms.Button
    Friend WithEvents ErrPrvSS As System.Windows.Forms.ErrorProvider
    Friend WithEvents txtSTCR2 As System.Windows.Forms.TextBox
    Friend WithEvents txtSSCR2 As System.Windows.Forms.TextBox
    Friend WithEvents lnkDocumentoSustento As System.Windows.Forms.LinkLabel
    Friend WithEvents chkServicioNoShow As System.Windows.Forms.CheckBox
    Friend WithEvents cboPaisDest As System.Windows.Forms.ComboBox
    Friend WithEvents lblEtiqPaisDest As System.Windows.Forms.Label
    Friend WithEvents cboPaisOrig As System.Windows.Forms.ComboBox
    Friend WithEvents lblEtiqPaisOrig As System.Windows.Forms.Label
    Friend WithEvents tpgAcomVehi As System.Windows.Forms.TabPage
    Friend WithEvents GroupBox4 As System.Windows.Forms.GroupBox
    Friend WithEvents dgvAcoVeh As System.Windows.Forms.DataGridView
    Friend WithEvents chkAcomVehi As System.Windows.Forms.CheckBox
    Friend WithEvents grbAcomVehicE As System.Windows.Forms.GroupBox
    Friend WithEvents Label16 As System.Windows.Forms.Label
    Friend WithEvents txtGruposPax As System.Windows.Forms.TextBox
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents txtPaxTotal As System.Windows.Forms.TextBox
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents grbAcomVehicI As System.Windows.Forms.GroupBox
    Friend WithEvents lvwServAcomVehi As System.Windows.Forms.ListView
    Friend WithEvents chkSelCopiaAcoVeh As System.Windows.Forms.CheckBox
    Friend WithEvents tvwAcoVeh As System.Windows.Forms.TreeView
    Friend WithEvents lblEtiqAcomVehPax As System.Windows.Forms.Label
    Friend WithEvents chkServNoCobrado As System.Windows.Forms.CheckBox
    Friend WithEvents QtVehiculos As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents NuVehiculo As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents NoVehiculo As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents QtCapacidad As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents QtPax As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents FlGuia As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents English As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents French As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents German As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Spanish As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents NoHotel As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents TxWebHotel As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents TxDireccHotel As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents TxTelfHotel1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents TxTelfHotel2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents FeHoraChkInHotel As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents FeHoraChkOutHotel As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents CoTipoDesaHotel As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents CoCiudadHotel As System.Windows.Forms.DataGridViewTextBoxColumn
End Class
