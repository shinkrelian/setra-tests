﻿Imports ComSEToursBL2
Imports ComSEToursBE2
Imports System.Reflection
Public Class frmIngDetCotizacionDatoMem
    Public frmRef As frmIngCotizacionDato
    'Public frmRefRes As frmIngReservasControlporFileDato
    Public strModoEdicion As Char
    Public blnDgvFilaVacia As Boolean
    'Public strIDProveedor As String = ""
    Public intIDServicio_Det As Int32 = 0
    Public strDescServicio_Det As String = ""
    Public strDescServicio As String = ""
    Public strDescServicio_Dgv As String = ""
    Dim strDescServicioDetBup As String = ""

    Public strTipoServicio_Det As String = ""
    Public strAnio As String = ""
    Public blnConAlojamiento As Boolean

    Public dblMargenCotiCab As Double = 0
    Public dblTipoCambioCotiCab As Single = 0

    Dim dblMargenDif As Double = 0
    Public strIDServicio As String = ""
    Public strIDProveedor As String = ""
    Public strIDProveedorRes As String = ""
    Public strDescTipoServ As String = ""
    Public strProveedor As String = ""

    Public intPax As Int16
    Public intLiberados As Int16
    Public strTipo_Lib As String
    Public strTipoPax As String
    Public strCoMoneda As String
    Public bytCantidad As Byte
    Public bytNoches As Byte
    Public strIDEMail As String = ""

    Public strIDTipoServ As String = ""
    Public strIDTipoProv As String = ""
    Public strIDCiudad As String = ""
    Public blnReemplazandoServiciosHotel As Boolean = False
    Public blnSelecServxServ As Boolean = True

    Public strCotizacion As String
    Public strIDIdioma As String
    Public strIDIdioma_Veh As String = ""
    Public datFechaIni As Date
    Dim intIDDet As Int32
    Dim intIDReserva_Det As String
    Public strIDDet As String
    Dim strIDDet1erDia As String = ""

    Public strIDReserva As String
    Public intIDCab As Int32 = 0
    Public strTituloFile As String
    Dim dttDetCotiServ As New DataTable("DetCotiServ")
    Dim blnCambios As Boolean = False
    Dim blnLoad As Boolean = False
    Dim dblCostoRealOrig As Double = 0
    Dim dblMargenAplicadoOrig As Double = 0
    'Dim dblCostoRealOrigFijo As Double = 0
    'Dim dblMargenAplicadoOrigFijo As Double = 0

    Dim blnActPax As Boolean = False
    Public strIDDetRel As String = ""
    Dim strIDReservas_Det_Rel As String = ""
    Dim blnCargaIniStruct As Boolean
    'Public blnCloseMantServicio As Boolean = False
    Public blnDiaSgte As Boolean
    Public strDescServicioaCambiar As String = ""
    Public lstPaxSelCambio As List(Of Int32)
    Public intNroPaxDif As Int16 = 0
    Public intNroPaxDifGrd As Int16 = 0
    Public strIDPais As String = ""

    Dim blnCambioFecha As Boolean = False
    Public datDiaEspecif As Date
    Public strModulo As String = ""
    Dim blnFueradeHorario As Boolean = False

    Dim blnCostoRealEditado As Boolean = False
    Dim blnCostoRealImptoEditado As Boolean = False
    Dim blnMargenAplicadoEditado As Boolean = False
    Dim blnSSCREditado As Boolean = False
    Dim blnSTCREditado As Boolean = False
    Dim blnServicioEditado As Boolean = False
    Dim blnRecalTarifaPendiente As Boolean = False

    Dim blnCantidadAPagarEditado As Boolean = False
    Dim blnNetoHabEditado As Boolean = False
    Dim blnIgvHabEditado As Boolean = False

    Public blnMargenCero As Boolean = False
    Public blnServicioVarios As Boolean = False
    Public blnPlanAlimenticio As Boolean = False
    Dim DtTextoIdiom As New DataTable
    Public blnLecturaReservas As Boolean = False
    Dim strServicioOrig1erdia As String = ""
    Dim strHora As String
    Public blnSoloConsulta As Boolean = False
    Dim intLiberadosBup As Int16 = 0
    Dim strTipo_LibBup As String = ""
    Public strDescripcionTipoTriple = ""
    Dim blnReCalculoCheckSelTodos As Boolean = True
    Dim blnHabilitarAcomodoEspecial As Boolean = True
    Dim blnUpdQuiebres As Boolean = False
    Public blnGrabarAcomodoEspecial As Boolean = False
    Public ListBETCamMon As List(Of clsCotizacionBE.clsCotizacionTiposCambioBE)
    Public blnReemplazandoServicio As Boolean = False
    Dim blnEliminoAcomodosEsp As Boolean = False
    Dim blnContieneAcomodoEsp As Boolean = False
    Public blnServicioTC As Boolean = False
    Public strIDServicio_V As String = ""
    Public dicValoresTC As Dictionary(Of String, Double) = Nothing

    Public strIDCliente As String = ""
    Public strIDTipoOper As String = "" 'Proveedor Externo/Interno
    Dim blnAcomoVehiReciente As Boolean = False
    Dim blnEditoChecksViaticosTC As Boolean = False
    Dim blnSelectTCLvw As Boolean = False
    'Dim blnCargaTvwAcomoVehiPaxInicial As Boolean = True
    Public intIDDetAntiguo As Integer = 0
    Public strMensajeValidacNuevoServ As String = ""

    Private Structure stAcomodoVehiculo
        Dim NuVehiculo As Int16
        Dim NoVehiculo As String
        Dim QtVehiculos As Byte
        Dim QtPax As Byte
        Dim QtCapacidad As Byte
        Dim FlGuia As Boolean
    End Structure
    Dim ListaAcomodoVehiculo As New List(Of stAcomodoVehiculo)
    Dim ListaDetAcomVehiIntDetPax As New List(Of clsCotizacionBE.clsAcomodoVehiculoIntDetPaxBE)
    Dim ListaDetAcomVehiExtDetPax As New List(Of clsCotizacionBE.clsAcomodoVehiculoExtDetPaxBE)
    Dim bytCantidadaPagarAntNoCobrado As Byte = 0
    Dim dblTotalGenAntNoCobrado As Double = 0
    Dim blnTieneSubServiciosGuia As Boolean = False

    Dim bytColHotel As Byte = 4
    Dim bytColWeb As Byte = 5

    Dim bytColTxDireccHotel As Byte = 6
    Dim bytColTxTelfHotel1 As Byte = 7
    Dim bytColTxTelfHotel2 As Byte = 8
    Dim bytColFeHoraChkInHotel As Byte = 9
    Dim bytColFeHoraChkOutHotel As Byte = 10
    Dim bytColCoTipoDesaHotel As Byte = 11
    Dim bytColCoCiudadHotel As Byte = 12
    Private Sub frmIngDetCotizacionDatoMem_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Activated
        blnLoad = True
        'If blnActDetQuiebres Then Me.Visible = False
    End Sub

    Private Sub BloquearComboIdioma()
        'Dim blnTipoServOT As Boolean = If(, True, False)
        If strIDTipoServ = "NAP" Or strIDTipoServ.Trim = "OT" Then
            cboIDIdioma.Enabled = False
        Else
            cboIDIdioma.Enabled = True
        End If
    End Sub

    Private Sub frmMantDatosAdmProveeDatoMem_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            dgvIdiom.DefaultCellStyle.SelectionBackColor = gColorSelectGrid
            dgvIdiom.DefaultCellStyle.SelectionForeColor = Color.Black
            dgvQui.DefaultCellStyle.SelectionBackColor = gColorSelectGrid
            dgvQui.DefaultCellStyle.SelectionForeColor = Color.Black

            cboCiudad.BackColor = gColorTextBoxObligat
            cboPais.BackColor = gColorTextBoxObligat
            cboTipoProv.BackColor = gColorTextBoxObligat
            cboIDIdioma.BackColor = gColorTextBoxObligat
            txtCantidad.BackColor = gColorTextBoxObligat

            'BorrarSTQuiebres()
            'BorrarSTDetPax()

            CargarCombos()
            CreaDttDetCotiServ()
            CalculaMargen()
            FormatoGridIdiomas()
            CrearListView()
            'CrearListViewPaxAcoVeh()

            tpgRecordat.Parent = Nothing
            tpgAcomVehi.Parent = Nothing


            If strModulo = "C" Then
                Me.Text = "Ingreso de Detalle de Cotización"
            ElseIf strModulo = "R" Then
                Me.Text = If(blnSoloConsulta, "Consulta de Detalle de Reserva", "Ingreso de Detalle de Reserva")
            ElseIf strModulo = "O" Then
                Me.Text = "Ingreso de Detalle de Operación"
            End If

            If strIDTipoProv = gstrTipoProveeHoteles Then
                lblCantidadEtiq.Visible = True
                txtCantidad.Visible = True
                dtpFechaOut.Visible = True
                lblFechaOutEtiq.Visible = True
                lblNochesEtiq.Visible = True
                lblNoches.Visible = True

            Else
                lblCantidadEtiq.Visible = False
                txtCantidad.Visible = False
                If (strIDTipoProv = gstrTipoProveeOperadores And blnConAlojamiento) And Not chkTransfer.Checked And strModulo <> "C" Then
                    lblCantidadEtiq.Visible = True
                    txtCantidad.Visible = True
                End If

                dtpFechaOut.Visible = False
                lblFechaOutEtiq.Visible = False
                lblNochesEtiq.Visible = False
                lblNoches.Visible = False

            End If

            If strModoEdicion = "E" Then


                lblTitulo.Text = "Servicio: " & strDescServicio_Dgv
                lblTitulo2.Text = "Servicio: " & strDescServicio_Dgv
                lblTitulo3.Text = "Servicio: " & strDescServicio_Dgv
                lblTitulo4.Text = "Servicio: " & strDescServicio_Dgv

                If strModulo = "C" Then


                    BloquearComboIdioma()
                    If Not frmRef.dgvDet.CurrentRow.Cells("IDDet").Value Is Nothing Then
                        If IsNumeric(frmRef.dgvDet.CurrentRow.Cells("IDDet").Value.ToString) Then
                            intIDDet = frmRef.dgvDet.CurrentRow.Cells("IDDet").Value.ToString
                        Else
                            intIDDet = 0
                        End If

                    Else
                        intIDDet = 0
                    End If


                    CargarControles()

                    lblCantidadEtiq.Visible = False
                    txtCantidad.Visible = False
                    dtpFechaOut.Visible = False
                    lblFechaOutEtiq.Visible = False
                    lblNochesEtiq.Visible = False
                    lblNoches.Visible = False

                    If frmRef.dgvDet.Item("Hora", frmRef.dgvDet.CurrentCell.RowIndex).Value = "" Then
                        dtpDiaHora.Text = "00:00"
                        dtpDiaHora.Visible = False
                    End If
                    grbChecksVoucBiblia.Visible = False

                    blnSelectTCLvw = blnTourConductorSelect()
                ElseIf strModulo = "R" Or strModulo = "O" Then

                    If blnLecturaReservas Then CargarControlesLecReserva()
                    If blnLecturaReservas Then Exit Sub

                    'If Not frmRefRes.DgvDetalle.CurrentRow.Cells("IDDet").Value Is Nothing Then
                    '    intIDDet = frmRefRes.DgvDetalle.CurrentRow.Cells("IDDet").Value.ToString
                    '    intIDReserva_Det = frmRefRes.DgvDetalle.CurrentRow.Cells("IDReserva_Det").Value.ToString
                    'Else
                    intIDDet = 0
                    intIDReserva_Det = 0
                    'End If
                    If strModulo = "O" Then
                        tpgRecordat.Parent = Tab
                        chkServNoCobrado.Visible = True
                    End If
                    CargarControlesReservas()
                    CargarControlesNetoHab()

                    btnReCalcular.Enabled = False : btnQuiebres.Enabled = False
                    chkIncGuia.Enabled = False

                    If strModulo = "O" And Not (cboTipoProv.SelectedValue = gstrTipoProveeHoteles And blnPlanAlimenticio = False) Then
                        grbBus_Guia.Visible = True : grbBus_Guia.Location = chkVerVoucher.Location
                        lblEtiqBus.Visible = False : CboBusVehiculo.Visible = False
                        grbBus_Guia.Width = 340 : grbBus_Guia.Height = 43
                    End If

                End If

                If cboTipoProv.SelectedValue.ToString = gstrTipoProveeHoteles And cboTipoProv.SelectedValue.ToString = gstrTipoProveeRestaurantes Then
                    TabPage4.Parent = Nothing
                Else
                    TabPage4.Parent = Tab
                End If

            ElseIf strModoEdicion = "N" Then
                lblTitulo.Text = "Nuevo Detalle"
                lblTitulo2.Text = "Nuevo Detalle"
                lblTitulo3.Text = "Nuevo Detalle"
                'dtpDia.Text = Today.Date
                cboIDIdioma.SelectedValue = strIDIdioma
                'cboPais.SelectedValue = gstrPeru
                cboPais.SelectedValue = strIDPais
                cboPais_SelectionChangeCommitted(Nothing, Nothing)

                txtNroPax.Text = intPax
                txtNroLiberados.Text = intLiberados
                cboTipoLib.SelectedValue = strTipo_Lib
                If strIDTipoProv <> "" Then cboTipoProv.SelectedValue = strIDTipoProv
                If strIDCiudad <> "" Then cboCiudad.SelectedValue = strIDCiudad

                CargarPaxLvw()

                Dim datDia As Date

                If strModulo = "C" Then

                    If blnDgvFilaVacia Then
                        datDia = datFechaIni
                    Else
                        If Not frmRef.dgvDet.CurrentRow Is Nothing Then
                            datDia = frmRef.dgvDet.CurrentRow.Cells("Dia").Value 'frmRef.dgvDet.Item("Dia", frmRef.dgvDet.Rows.Count - 1).Value
                        Else
                            datDia = datFechaIni
                        End If
                    End If
                    grbChecksVoucBiblia.Visible = False
                ElseIf strModulo = "R" Or strModulo = "O" Then
                    'If blnDgvFilaVacia Then
                    '    datDia = datFechaIni
                    'Else
                    '    datDia = frmRefRes.DgvDetalle.Item("Dia", frmRefRes.DgvDetalle.Rows.Count - 1).Value
                    'End If

                    'intIDDet = frmRefRes.DgvDetalle.CurrentRow.Cells("IDDet").Value.ToString
                    'intIDReserva_Det = frmRefRes.DgvDetalle.CurrentRow.Cells("IDReserva_Det").Value.ToString
                    'txtCantidad.Text = bytCantidad
                    'lblNoches.Text = bytNoches

                    'If strModulo = "O" Then
                    '    tpgRecordat.Parent = Tab
                    'End If
                    'btnReCalcular.Enabled = False : btnQuiebres.Enabled = False

                End If

                If datDiaEspecif = #1/1/1900# Then
                    If blnDiaSgte Then
                        datDia = DateAdd(DateInterval.Day, 1, datDia)
                    End If
                    dtpDia.Value = datDia
                Else
                    dtpDia.Value = datDiaEspecif
                End If

                OcultarMontos()


                btnProveedor_Click(Nothing, Nothing)

                If lblProveedor.Text.Trim = "" Then

                    'Dim objBT As New clsCotizacionBT
                    'objBT.Eliminar(intIDCab)
                    Me.Close()

                    'frmRef.Close()
                    'frmRef.blnEliminoCab = True

                    Exit Sub
                End If

                If strDescServicioaCambiar <> "" Then

                    strDescServicioaCambiar = strDescServicio_Det & Mid(strDescServicioaCambiar, InStrRev(strDescServicioaCambiar, " - "))


                    strDescServicio_Det = strDescServicioaCambiar
                    txtServicio.Text = strDescServicioaCambiar

                    PintaChecksLvwDetReservPaxCambio()
                End If

                If blnSelecServxServ Then btnAceptar_Click(Nothing, Nothing)
                'btnAceptar_Click(Nothing, Nothing)
            End If

            'If Not frmRefRes Is Nothing Then
            '    If strModulo = "R" Then
            '        txtServicio.ReadOnly = True
            '    End If
            'End If


            BloqueoReserva()

            'Acomodo Especia
            chkAcomodoEspecial.Visible = False
            If strModulo = "C" Then
                'DESCOMENTAR 20131210
                If blnConAlojamiento Or cboTipoProv.SelectedValue.ToString = gstrTipoProveeHoteles Then
                    CargarListaAcomodoEspecial()
                    chkAcomodoEspecial.Visible = True
                End If
            End If

            'If strModoEdicion = "E" Then txtServicio.Text = strDescServicio_Det
            TabPage2.Parent = Nothing

            If blnSoloConsulta Then
                CargarblnConsulta(btnAceptar, grbDatosGenerales, blnSoloConsulta)
                CargarblnConsulta(btnAceptar, grbTextos, blnSoloConsulta)
            End If

            If strModoEdicion = "N" Then
                If strModulo = "C" Then
                    If Not blnSelecServxServ Then Me.Close()
                End If
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message, ex.Source, MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub CargarControlesLecReserva()
        Try
            Me.Text = "Consulta de Detalle de Cotización"

            'Ocultar
            grbChecksVoucBiblia.Visible = False


            Using objBN As New clsCotizacionBN.clsDetalleCotizacionBN
                Using dr As SqlClient.SqlDataReader = objBN.ConsultarPK(CInt(strIDDet))
                    Dim objBLUbig As New clsTablasApoyoBN.clsUbigeoBN

                    dr.Read()
                    If dr.HasRows Then
                        dtpDia.Text = CDate(dr("Dia")).ToShortDateString
                        dtpDiaHora.Text = FormatDateTime(CDate(dr("Dia")), DateFormat.ShortTime)
                        chkIncGuia.Checked = dr("IncGuia")
                        cboPais.SelectedValue = strIDPais
                        cboPais_SelectionChangeCommitted(Nothing, Nothing)
                        pCargaCombosBox(cboCiudad, objBLUbig.ConsultarCbo("CIUDAD", strIDPais))
                        cboCiudad.SelectedValue = dr("IDubigeo")
                        cboTipoProv.SelectedValue = strIDTipoProv
                        lblProveedor.Text = strProveedor
                        cboIDIdioma.SelectedValue = dr("IDIdioma")
                        txtNroPax.Text = dr("NroPax")
                        If intPax.ToString <> txtNroPax.Text Then chkSelPax.Checked = True
                        txtNroLiberados.Text = If(IsDBNull(dr("NroLiberados")) = True, "", dr("NroLiberados"))
                        cboTipoLib.SelectedValue = If(IsDBNull(dr("Tipo_Lib")) = True, "", dr("Tipo_Lib"))
                        txtServicio.Text = dr("Servicio")
                        chkEspecial.Checked = dr("Especial")
                        txtMotivoEspecial.Text = If(IsDBNull(dr("MotivoEspecial")) = True, "", dr("MotivoEspecial"))
                        'txtRutaDocSustento.Text = If(IsDBNull(dr("RutaDocSustento")) = True, "", dr("RutaDocSustento"))
                        lnkDocumentoSustento.Text = If(IsDBNull(dr("RutaDocSustento")) = True, "", dr("RutaDocSustento"))
                        chkDesayuno.Checked = dr("Desayuno")
                        chkLonche.Checked = dr("Lonche")
                        chkAlmuerzo.Checked = dr("Almuerzo")
                        chkCena.Checked = dr("Cena")
                        chkTransfer.Checked = dr("Transfer")
                        If chkTransfer.Checked Then
                            CargarCombosTransfer2LectResv()
                            chkTransfer.Visible = True
                            lblOrigenEtiq.Visible = True
                            cboOrigen.Visible = True
                            lblDestinoEtiq.Visible = True
                            cboDestino.Visible = True


                            lblEtiqPaisOrig.Visible = False
                            cboPaisOrig.Visible = False
                            lblEtiqPaisDest.Visible = False
                            cboPaisDest.Visible = False
                            lblIDUbigeoOriEtiq.Visible = False
                            cboIDUbigeoOri.Visible = False
                            lblIDUbigeoDesEtiq.Visible = False
                            cboIDUbigeoDes.Visible = False
                            lblTipoTransporteEtiq.Visible = False
                            cboTipoTransporte.Visible = False
                        Else
                            chkTransfer.Visible = True
                            lblOrigenEtiq.Visible = False
                            cboOrigen.Visible = False
                            lblDestinoEtiq.Visible = False
                            cboDestino.Visible = False

                            lblEtiqPaisOrig.Visible = True
                            cboPaisOrig.Visible = True
                            lblEtiqPaisDest.Visible = True
                            cboPaisDest.Visible = True
                            lblIDUbigeoOriEtiq.Visible = True
                            cboIDUbigeoOri.Visible = True
                            lblIDUbigeoDesEtiq.Visible = True
                            cboIDUbigeoDes.Visible = True
                            lblTipoTransporteEtiq.Visible = True
                            cboTipoTransporte.Visible = True
                        End If

                        cboOrigen.SelectedValue = If(IsDBNull(dr("IDDetTransferOri")) = True, "0", dr("IDDetTransferOri"))
                        cboDestino.SelectedValue = If(IsDBNull(dr("IDDetTransferDes")) = True, "0", dr("IDDetTransferDes"))
                        Dim objUbig As New clsTablasApoyoBN.clsUbigeoBN
                        cboPaisOrig.SelectedValue = objUbig.strDevuelveIDPaisxCiudad(dr("IDUbigeoOri"))
                        cboPaisOrig_SelectionChangeCommitted(Nothing, Nothing)
                        cboIDUbigeoOri.SelectedValue = dr("IDUbigeoOri")
                        cboPaisDest.SelectedValue = objUbig.strDevuelveIDPaisxCiudad(dr("IDUbigeoDes"))
                        cboPaisDest_SelectionChangeCommitted(Nothing, Nothing)
                        cboIDUbigeoDes.SelectedValue = dr("IDUbigeoDes")

                        cboTipoTransporte.SelectedValue = If(IsDBNull(dr("TipoTransporte")) = True, "", dr("TipoTransporte"))

                        'Costos
                        txtCostoReal.Text = dr("CostoReal")
                        lblCostoLiberado.Text = dr("CostoLiberado")
                        lblTotImpto.Text = dr("TotImpto")
                        txtMargenAplicado.Text = dr("MargenAplicado")
                        lblMargen.Text = dr("Margen")
                        lblRedondeoTotal.Text = dr("RedondeoTotal")
                        lblTotal.Text = dr("Total")

                        'Montos Calculados
                        lblCostoReal2.Text = dr("CostoReal")
                        lblCostoLiberado2.Text = dr("CostoLiberado")
                        lblTotImpto2.Text = dr("TotImpto")
                        lblMargenLiberado2.Text = dr("MargenLiberado")
                        lblMargenAplicado2.Text = dr("MargenAplicado")
                        lblMargen2.Text = dr("Margen")
                        lblRedondeoTotal2.Text = dr("RedondeoTotal")
                        lblTotal2.Text = dr("Total")
                        lblSSCR2.Text = dr("SSCR")
                        txtSSCR2.Text = dr("SSCR")
                        lblSSMargen2.Text = dr("SSMargen")
                        lblSSCL2.Text = dr("SSCL")
                        lblSSMargenLiberado2.Text = dr("SSMargenLiberado")
                        lblSSTotal2.Text = dr("SSTotal")
                        lblSTCR2.Text = dr("STCR")
                        txtSTCR2.Text = dr("STCR")
                        lblSTMargen2.Text = dr("STMargen")
                        lblSTTotal2.Text = dr("STTotal")
                    End If

                    dr.Close()
                End Using
            End Using

            CargarPaxLvwLect()
            CargarQuiebresLect()
            CargarTextoIdiomaLect()

            btnQuiebres.Enabled = False
            btnReCalcular.Enabled = False
            btnAceptar.Enabled = False
            txtMotivoEspecial.Enabled = False
            'txtRutaDocSustento.Enabled = False
            'lnkDocumentoSustento.Enabled = False
            btnRutaArchivo.Enabled = False
            chkIncGuia.Enabled = False

            OcultarControlesHotelResvLect()
        Catch ex As Exception
            Throw
        End Try
    End Sub

    Private Sub CargarTextoIdiomaLect()
        Try
            Dim objBNDet As New clsCotizacionBN.clsDetalleCotizacionBN.clsDetalleCotizacionDescripServicioBN
            Dim dt As DataTable = objBNDet.ConsultarListxIDDetCopy(CInt(strIDDet))

            For Each DgvCol As DataGridViewColumn In dgvIdiom.Columns
                For Each Dr As DataRow In dt.Rows
                    If DgvCol.Name.ToUpper = Dr("IDIdioma") Then
                        dgvIdiom.Item(DgvCol.Index, 0).Value = If(IsDBNull(Dr("DescLarga")) = True, "", Dr("DescLarga"))
                        Exit For
                    End If
                Next
            Next

            If strIDTipoProv <> gstrTipoProveeHoteles Then
                For Each DgvCol As DataGridViewColumn In dgvIdiom.Columns
                    If dgvIdiom.Item(DgvCol.Index, 0).Value Is Nothing Then DgvCol.Visible = False
                    DgvCol.Width = 100
                Next
            End If

        Catch ex As Exception
            Throw
        End Try
    End Sub

    Private Sub OcultarControlesHotelResvLect()
        If strIDTipoProv = gstrTipoProveeHoteles Then
            chkTransfer.Visible = False

            If Not blnPlanAlimenticio Then dtpDiaHora.Visible = False
            lblFechaOutEtiq.Visible = False
            dtpFechaOut.Visible = False
            lblNochesEtiq.Visible = False
            lblNoches.Visible = False
            lblCantidadEtiq.Visible = False
            txtCantidad.Visible = False
            lblOrigenEtiq.Visible = False
            cboOrigen.Visible = False
            lblDestinoEtiq.Visible = False
            cboDestino.Visible = False
            lblIDUbigeoOriEtiq.Visible = False
            cboIDUbigeoOri.Visible = False
            lblIDUbigeoDesEtiq.Visible = False
            cboIDUbigeoDes.Visible = False
            lblTipoTransporteEtiq.Visible = False
            cboTipoTransporte.Visible = False
            chkIncGuia.Visible = True
            lblEtiqPaisOrig.Visible = False
            cboPaisOrig.Visible = False
            lblEtiqPaisDest.Visible = False
            cboPaisDest.Visible = False
        Else
            chkIncGuia.Visible = False
            OcultarMontosSSST(True)
        End If
    End Sub


    Private Sub BloqueoReserva()
        'If frmRefRes Is Nothing Then Exit Sub
        'grbChecksVoucBiblia.Visible = If(strModulo = "O", True, False)
        ''If frmRefRes.btnGenerarOperaciones.Enabled = False Then
        'If frmRefRes.pblnBloqueoGrabarReservas And strModulo = "R" Then
        '    'pEnableControlsGroupBox(grbDatosGenerales, False)
        '    'pEnableControlsGroupBox(grbTextos, False)
        '    'pEnableControlsGroupBox(grbMontos, False)
        '    'pEnableControlsGroupBox(grbQuiebres, False)
        '    'pEnableControlsGroupBox(grbRecordObserv, False)
        '    'pEnableControlsGroupBox(grbSepCostos, False)
        '    pEnableControlsGroupBox(grbPax, False)
        '    'btnAceptar.Enabled = False
        '    btnReCalcular.Enabled = False
        '    btnQuiebres.Enabled = False
        '    'btnEliminarIdiomaTexto.Enabled = False
        '    ErrPrv.SetError(dgvIdiom, "")
        'End If
        'If frmRefRes.DgvDetalle.CurrentRow.Cells("Hora").Value Is Nothing Then
        '    dtpDiaHora.Text = "00:00"
        '    If Not blnPlanAlimenticio Then dtpDiaHora.Visible = False
        '    Exit Sub
        'Else
        '    If frmRefRes.DgvDetalle.CurrentRow.Cells("Hora").Value.ToString = "" Then
        '        dtpDiaHora.Text = "00:00"
        '        If Not blnPlanAlimenticio Then dtpDiaHora.Visible = False
        '    End If
        'End If
    End Sub

    Private Sub OcultarMontos()
        Try
            lblCostoRealEtiq.Visible = False
            txtCostoReal.Visible = False
            lblMargenEtiq.Visible = False
            lblMargen.Visible = False
            txtMargenAplicado.Visible = False
            lblCostoLiberadoEtiq.Visible = False
            lblCostoLiberado.Visible = False
            lblMargenLiberadoEtiq.Visible = False
            lblMargenLiberado.Visible = False
            lblTotImptoEtiq.Visible = False
            lblTotImpto.Visible = False
            lblTotalEtiq.Visible = False
            lblTotal.Visible = False
            lblRedondeo.Visible = False
            lblRedondeoTotal.Visible = False

            OcultarMontosSSST()

            chkSelPax.Visible = False
            grbPax.Visible = False

            Dim iRestarTop As Integer = 166
            chkEspecial.Top -= iRestarTop
            lblMotivoEspecialEtiq.Top -= iRestarTop
            txtMotivoEspecial.Top -= iRestarTop
            lblRutaDocSustentoEtiq.Top -= iRestarTop
            'txtRutaDocSustento.Top = 242
            lnkDocumentoSustento.Top -= iRestarTop
            btnRutaArchivo.Top -= iRestarTop
            lblIdiomaEtiq.Top -= iRestarTop
            cboIDIdioma.Top -= iRestarTop


            Me.Height = 490
            ''ME.Height = 653

        Catch ex As Exception
            Throw
        End Try
    End Sub





    Private Sub CalculaMargen()
        'If intIDCab <> 0 Then
        If strModoEdicion = "E" Then
            Dim objCot As New clsCotizacionBN
            'Dim drCotCab As SqlClient.SqlDataReader = objCot.ConsultarPk(intIDCab)
            'Dim dblMargenCotiCabBD As Double = 0

            'drCotCab.Read()
            'If drCotCab.HasRows Then

            'dblMargenCotiCabBD = If(IsDBNull(drCotCab("Margen")) = True, 0, drCotCab("Margen"))

            dblMargenDif = dblMargenCotiCab '- dblMargenCotiCabBD


            'End If
        Else

            dblMargenDif = dblMargenCotiCab
        End If

    End Sub
    Private Sub CargarCombos()
        Try
            Dim objBLTPrv As New clsTablasApoyoBN.clsTipoProveedorBN
            pCargaCombosBox(cboTipoProv, objBLTPrv.ConsultarCbo)

            Dim objBLUbig As New clsTablasApoyoBN.clsUbigeoBN
            pCargaCombosBox(cboPais, objBLUbig.ConsultarCbo("PAIS"))
            'pCargaCombosBox(cboCiudad, objBLUbig.ConsultarCbo("CIUDAD"))
            pCargaCombosBox(cboPaisOrig, objBLUbig.ConsultarCbo("PAIS"))
            pCargaCombosBox(cboPaisDest, objBLUbig.ConsultarCbo("PAIS"))

            Dim objBLIdio As New clsTablasApoyoBN.clsIdiomaBN
            pCargaCombosBox(cboIDIdioma, objBLIdio.ConsultarCboConSiglas())

            Dim dttTipoLib As New DataTable("TipoLib")
            With dttTipoLib
                .Columns.Add("Tipo_Lib")
                .Columns.Add("Descripcion")

                .Rows.Add("", "NINGUNO")
                .Rows.Add("S", "SIMPLE")
                .Rows.Add("D", "DOBLE")
            End With
            pCargaCombosBox(cboTipoLib, dttTipoLib)


            'Dim dttUbiOri As DataTable = objBLUbig.ConsultarCboconCodigo(, , True)
            'pCargaCombosBox(cboIDUbigeoOri, dttUbiOri)

            'Dim dttUbiDes As DataTable = dttUbiOri.Clone
            'For i As Int32 = 0 To dttUbiOri.Rows.Count - 1
            '    dttUbiDes.ImportRow(dttUbiOri.Rows(i))
            'Next
            'pCargaCombosBox(cboIDUbigeoDes, dttUbiDes)



            Dim dttTipoTransporte As New DataTable("TipoTransporte")
            With dttTipoTransporte
                .Columns.Add("Codigo")
                .Columns.Add("Descripcion")

                .Rows.Add("", "")
                .Rows.Add("B", "BUS")
                .Rows.Add("T", "TREN")
                .Rows.Add("V", "VUELO")
            End With
            pCargaCombosBox(cboTipoTransporte, dttTipoTransporte)

            Dim objPr As New clsProveedorBN
            pCargaCombosBox(cboGuia, objPr.ConsultarCboGuias())

        Catch ex As Exception
            Throw
        End Try
    End Sub

    Public Sub cboPais_SelectionChangeCommitted(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboPais.SelectionChangeCommitted
        If cboPais.SelectedValue Is Nothing Then Exit Sub
        Try
            Dim objBLUbig As New clsTablasApoyoBN.clsUbigeoBN
            Dim dttCiudades As DataTable = objBLUbig.ConsultarCbo(, cboPais.SelectedValue.ToString)
            pCargaCombosBox(cboCiudad, dttCiudades)


            Dim dttOrigen As DataTable = dttCiudades.Clone
            Dim dttDestino As DataTable = dttCiudades.Clone
            For i As Byte = 0 To dttCiudades.Rows.Count - 1
                dttOrigen.ImportRow(dttCiudades.Rows(i))
                dttDestino.ImportRow(dttCiudades.Rows(i))
            Next
            pCargaCombosBox(cboIDUbigeoOri, dttOrigen)
            pCargaCombosBox(cboIDUbigeoDes, dttDestino)

        Catch ex As Exception
            MessageBox.Show(ex.Message, ex.Source, MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Public Sub cboPaisOrig_SelectionChangeCommitted(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboPaisOrig.SelectionChangeCommitted
        If cboPais.SelectedValue Is Nothing Then Exit Sub
        Try
            Dim objBLUbig As New clsTablasApoyoBN.clsUbigeoBN
            Dim dttCiudades As DataTable = objBLUbig.ConsultarCbo(, cboPaisOrig.SelectedValue.ToString)
            pCargaCombosBox(cboIDUbigeoOri, dttCiudades)

        Catch ex As Exception
            MessageBox.Show(ex.Message, ex.Source, MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Public Sub cboPaisDest_SelectionChangeCommitted(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboPaisDest.SelectionChangeCommitted
        If cboPais.SelectedValue Is Nothing Then Exit Sub
        Try
            Dim objBLUbig As New clsTablasApoyoBN.clsUbigeoBN
            Dim dttCiudades As DataTable = objBLUbig.ConsultarCbo(, cboPaisDest.SelectedValue.ToString)
            pCargaCombosBox(cboIDUbigeoDes, dttCiudades)

        Catch ex As Exception
            MessageBox.Show(ex.Message, ex.Source, MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Function blnActualizoDatosViaticosTC() As Boolean
        Try
            Dim blnActualizoCheckViaticos As Boolean = False
            'If chkLonche.Checked = True Or chkAlmuerzo.Checked = True Or chkCena.Checked = True Then
            If blnEditoChecksViaticosTC = True Then
                blnActualizoCheckViaticos = True
            End If

            If strModoEdicion = "E" And Not blnActualizoCheckViaticos Then
                If Not frmRef Is Nothing Then
                    If blnSelectTCLvw <> blnTourConductorSelect() Then
                        blnActualizoCheckViaticos = True
                    End If
                End If
            End If

            Return blnActualizoCheckViaticos
        Catch ex As Exception
            Throw
        End Try
    End Function

    Private Sub Grabar()
        Dim bytNuevasFila As Byte = 0
        Try
            If blnActualizoDatosViaticosTC() Then
                frmRef.blnActualizoViaticosTC = True
            End If

            With frmRef.dgvDet
                Dim objBL As New clsServicioProveedorBN.clsServiciosporDiaBN
                Dim objDiasEsp As New clsTablasApoyoBN.clsFechasEspecialesBN
                'Dim dttDias As DataTable = objBL.ConsultarListxIDServicioIdioma(strIDServicio, strIDIdioma)
                'Dim dttDias As DataTable = objBL.ConsultarListxIDServicio(strIDServicio)
                Dim drDias As SqlClient.SqlDataReader = objBL.ConsultarxIDServicioIdiomaparaCotizar(strIDServicio)

                'If dttDias.Rows.Count = 0 Then
                If Not drDias.HasRows Then
                    Dim bytFilaDgv As Byte = .RowCount
                    'If Not blnDgvFilaVacia Then
                    '.Rows.Add()
                    'Else
                    'If bytFilaDgv > 0 Then bytFilaDgv -= 1
                    'End If
                    'If .RowCount = 0 Then .Rows.Add()
                    .Rows.Add()

                    'If bytFilaDgv = 0 Then
                    '.Item("Item", bytFilaDgv).Value = Convert.ToSingle(bytFilaDgv + 1)
                    'Else
                    '    If cboTipoProv.SelectedValue.ToString <> gstrTipoProveeHoteles Then
                    '        .Item("Item", bytFilaDgv).Value = Convert.ToSingle(bytFilaDgv + 1)
                    '    Else
                    '.Item("Item", bytFilaDgv).Value = sglDevuelveItemMayorDia()
                    '    End If
                    'End If

                    If Not blnSelecServxServ Then
                        .Item("IDDet", bytFilaDgv).Value = "M" & frmRef.bytMayorIddetGenerado().ToString()
                    End If

                    '.Item("Item", bytFilaDgv).Value = sglDevuelveItemCorrecto(frmRef.dgvDet, Format(dtpDia.Value, "dd/MM/yyyy HH:mm:ss"), cboTipoProv.SelectedValue.ToString, strModoEdicion)
                    .Item("Dia", bytFilaDgv).Value = CDate(dtpDia.Text & " " & dtpDiaHora.Text)  'Format(dtpDia.Value, "dd/MM/yyyy HH:mm:ss")
                    'If cboTipoProv.SelectedValue.ToString = gstrTipoProveeHoteles And .Item("PlanAlimenticio", bytFilaDgv).Value = False Then
                    .Item("DiaFormat", bytFilaDgv).Value = dtpDia.Value.ToString("ddd dd/MM/yyyy").ToUpper
                    'Else
                    '.Item("DiaFormat", bytFilaDgv).Value = dtpDia.Value.ToString("ddd dd/MM/yyyy").ToUpper
                    'End If

                    If cboTipoProv.SelectedValue.ToString = gstrTipoProveeHoteles And blnPlanAlimenticio = False Then
                        .Item("Hora", bytFilaDgv).Value = ""
                    Else
                        .Item("Hora", bytFilaDgv).Value = dtpDiaHora.Value.ToString("HH:mm")
                    End If
                    If frmRef.dtpFechaOut.Value < dtpDia.Text Then
                        frmRef.dtpFechaOut.Value = dtpDia.Value
                    End If

                    .Item("IDPais", bytFilaDgv).Value = cboPais.SelectedValue.ToString
                    .Item("IDCiudad", bytFilaDgv).Value = cboCiudad.SelectedValue.ToString
                    .Item("IDTipoProv", bytFilaDgv).Value = cboTipoProv.SelectedValue.ToString
                    .Item("IDTipoOper", bytFilaDgv).Value = strIDTipoOper
                    .Item("Dias", bytFilaDgv).Value = 1
                    .Item("IDProveedor", bytFilaDgv).Value = strIDProveedor
                    .Item("DescProveedor", bytFilaDgv).Value = lblProveedor.Text
                    .Item("IDServicio", bytFilaDgv).Value = strIDServicio
                    .Item("IDServicio_Det", bytFilaDgv).Value = intIDServicio_Det

                    .Item("TipoTriple", bytFilaDgv).Value = strDescripcionTipoTriple

                    .Item("DescCiudad", bytFilaDgv).Value = cboCiudad.Text
                    If cboTipoProv.SelectedValue.ToString = gstrTipoProveeHoteles Or cboTipoProv.SelectedValue.ToString = gstrTipoProveeRestaurantes Then
                        .Item("DescServicioDet", bytFilaDgv).Value = strDescServicio_Det
                    Else
                        .Item("DescServicioDet", bytFilaDgv).Value = txtServicio.Text

                    End If

                    .Item("DescServicioDetBup", bytFilaDgv).Value = txtServicio.Text

                    '.Item("DescServicioDet", bytFilaDgv).Value = dr("DescripCorta")


                    .Item("Tipo", bytFilaDgv).Value = strTipoServicio_Det
                    .Item("ConAlojamiento", bytFilaDgv).Value = blnConAlojamiento
                    .Item("IncGuia", bytFilaDgv).Value = chkIncGuia.Checked

                    .Item("NroPax", bytFilaDgv).Value = txtNroPax.Text
                    .Item("NroLiberados", bytFilaDgv).Value = txtNroLiberados.Text
                    If chkIncGuia.Checked Then
                        .Item("PaxmasLiberados", bytFilaDgv).Value = "1"
                    Else
                        If txtNroLiberados.Text.Trim <> "" And txtNroLiberados.Text.Trim <> "0" Then
                            .Item("PaxmasLiberados", bytFilaDgv).Value = txtNroPax.Text.Trim & "+" & txtNroLiberados.Text.Trim
                        Else
                            .Item("PaxmasLiberados", bytFilaDgv).Value = txtNroPax.Text.Trim
                        End If
                    End If


                    .Item("Tipo_Lib", bytFilaDgv).Value = If(cboTipoLib.Text = "", "", cboTipoLib.SelectedValue.ToString)


                    .Item("CostoLiberado", bytFilaDgv).Value = lblCostoLiberado.Text
                    .Item("CostoReal", bytFilaDgv).Value = txtCostoReal.Text
                    .Item("CostoRealAnt", bytFilaDgv).Value = dblCostoRealOrig
                    .Item("Margen", bytFilaDgv).Value = lblMargen.Text
                    .Item("MargenAplicado", bytFilaDgv).Value = txtMargenAplicado.Text
                    .Item("MargenAplicadoAnt", bytFilaDgv).Value = dblMargenAplicadoOrig
                    .Item("MargenLiberado", bytFilaDgv).Value = lblMargenLiberado.Text

                    .Item("SSCL", bytFilaDgv).Value = txtSSCL.Text
                    '.Item("SSCR", bytFilaDgv).Value = txtSSCR.Text
                    'If txtSTCR2.Visible = False Then
                    .Item("SSCR", bytFilaDgv).Value = txtSSCR.Text
                    'Else
                    '.Item("SSCR", bytFilaDgv).Value = txtSSCR2.Text
                    'End If

                    .Item("SSMargen", bytFilaDgv).Value = txtSSMargen.Text
                    .Item("SSMargenLiberado", bytFilaDgv).Value = txtSSMargenLiberado.Text

                    .Item("SSTotal", bytFilaDgv).Value = txtSSTotal.Text
                    .Item("SSTotalOrig", bytFilaDgv).Value = txtSSTotalOrig.Text
                    '.Item("STCR", bytFilaDgv).Value = txtSTCR.Text
                    'If txtSTCR2.Visible = False Then
                    .Item("STCR", bytFilaDgv).Value = txtSTCR.Text
                    'Else
                    '   .Item("STCR", bytFilaDgv).Value = txtSTCR2.Text
                    'End If


                    .Item("STMargen", bytFilaDgv).Value = txtSTMargen.Text
                    .Item("STTotal", bytFilaDgv).Value = txtSTTotal.Text
                    .Item("STTotalOrig", bytFilaDgv).Value = txtSTTotalOrig.Text
                    .Item("Total", bytFilaDgv).Value = lblTotal.Text
                    .Item("RedondeoTotal", bytFilaDgv).Value = lblRedondeoTotal.Text
                    .Item("TotalOrig", bytFilaDgv).Value = lblTotalOrig.Text

                    .Item("CostoLiberadoImpto", bytFilaDgv).Value = lblCostoLiberadoImpto.Text
                    .Item("CostoRealImpto", bytFilaDgv).Value = txtCostoRealImpto.Text
                    .Item("MargenImpto", bytFilaDgv).Value = lblMargenImpto.Text
                    .Item("MargenLiberadoImpto", bytFilaDgv).Value = lblMargenLiberadoImpto.Text
                    .Item("SSCLImpto", bytFilaDgv).Value = lblSSCLImpto.Text
                    .Item("SSCRImpto", bytFilaDgv).Value = lblSSCRImpto.Text
                    .Item("SSMargenImpto", bytFilaDgv).Value = lblSSMargenImpto.Text
                    .Item("SSMargenLiberadoImpto", bytFilaDgv).Value = lblSSMargenLiberadoImpto.Text
                    .Item("STCRImpto", bytFilaDgv).Value = lblSTCRImpto.Text
                    .Item("STMargenImpto", bytFilaDgv).Value = lblSTMargenImpto.Text

                    .Item("TotImpto", bytFilaDgv).Value = lblTotImpto.Text

                    .Item("Especial", bytFilaDgv).Value = chkEspecial.Checked
                    .Item("MotivoEspecial", bytFilaDgv).Value = txtMotivoEspecial.Text
                    '.Item("RutaDocSustento", bytFilaDgv).Value = txtRutaDocSustento.Text
                    .Item("RutaDocSustento", bytFilaDgv).Value = lnkDocumentoSustento.Text

                    .Item("Desayuno", bytFilaDgv).Value = chkDesayuno.Checked
                    .Item("Lonche", bytFilaDgv).Value = chkLonche.Checked
                    .Item("Almuerzo", bytFilaDgv).Value = chkAlmuerzo.Checked
                    .Item("Cena", bytFilaDgv).Value = chkCena.Checked


                    .Item("Transfer", bytFilaDgv).Value = chkTransfer.Checked
                    If cboOrigen.Text <> "" Then
                        '.Item("OrigenTransfer", bytFilaDgv).Value = cboOrigen.SelectedValue.ToString.Trim
                        .Item("IDDetTransferOri", bytFilaDgv).Value = cboOrigen.SelectedValue.ToString.Trim
                    Else
                        '.Item("OrigenTransfer", bytFilaDgv).Value = ""
                        .Item("IDDetTransferOri", bytFilaDgv).Value = "0"
                    End If
                    If cboDestino.Text <> "" Then
                        '.Item("DestinoTransfer", bytFilaDgv).Value = cboDestino.SelectedValue.ToString.Trim
                        .Item("IDDetTransferDes", bytFilaDgv).Value = cboDestino.SelectedValue.ToString.Trim
                    Else
                        '.Item("DestinoTransfer", bytFilaDgv).Value = ""
                        .Item("IDDetTransferDes", bytFilaDgv).Value = "0"
                    End If

                    .Item("TipoTransporte", bytFilaDgv).Value = If(cboTipoTransporte.Text = "", "", cboTipoTransporte.SelectedValue.ToString)

                    If cboIDUbigeoOri.Text <> "" And cboIDUbigeoOri.Text <> "-------" Then
                        .Item("IDUbigeoOri", bytFilaDgv).Value = cboIDUbigeoOri.SelectedValue.ToString
                        .Item("DescUbigeoOri", bytFilaDgv).Value = cboIDUbigeoOri.Text
                    Else
                        .Item("IDUbigeoOri", bytFilaDgv).Value = "000001"
                        .Item("DescUbigeoOri", bytFilaDgv).Value = ""
                    End If
                    If cboIDUbigeoDes.Text <> "" And cboIDUbigeoDes.Text <> "-------" Then
                        .Item("IDUbigeoDes", bytFilaDgv).Value = cboIDUbigeoDes.SelectedValue.ToString
                        .Item("DescUbigeoDes", bytFilaDgv).Value = cboIDUbigeoDes.Text
                    Else
                        .Item("IDUbigeoDes", bytFilaDgv).Value = "000001"
                        .Item("DescUbigeoDes", bytFilaDgv).Value = ""
                    End If

                    .Item("Servicio", bytFilaDgv).Value = txtServicio.Text

                    .Item("IDTipoServ", bytFilaDgv).Value = strIDTipoServ
                    'Dim blnTipoServOT As Boolean = If(strIDTipoServ.Trim = "OT", True, False)
                    Dim strTmpIDIoma As String = ""
                    If strIDTipoServ.Trim = "NAP" Or strIDTipoServ.Trim = "OT" Then
                        strTmpIDIoma = "NO APLICA"
                    Else
                        If strIDTipoServ.Trim = "SIC" And strIDIdioma.Trim <> "SPANISH" Then
                            strTmpIDIoma = "ENGLISH"
                        Else
                            strTmpIDIoma = cboIDIdioma.SelectedValue.ToString
                        End If
                    End If

                    .Item("IDIdioma", bytFilaDgv).Value = strTmpIDIoma 'If(strIDTipoServ.Trim = "NAP" Or strIDTipoServ.Trim = "OT", "NO APLICA", cboIDIdioma.SelectedValue.ToString)
                    .Item("Siglas", bytFilaDgv).Value = strTmpIDIoma.Substring(0, 3)  'If(strIDTipoServ.Trim = "NAP" Or strIDTipoServ.Trim = "OT", "---", strDevuelveCadenaOcultaDerecha(cboIDIdioma.Text))

                    .Item("Anio", bytFilaDgv).Value = strAnio
                    .Item("DescTipoServ", bytFilaDgv).Value = strDescTipoServ


                    '.Item("IDDetRel", bytFilaDgv).Value = "M" & (bytFilaDgv + 1)
                    .Item("IDDetRel", bytFilaDgv).Value = ""
                    .Item("FueradeHorario", bytFilaDgv).Value = blnFueradeHorario

                    .Item("CostoRealEditado", bytFilaDgv).Value = blnCostoRealEditado
                    .Item("CostoRealImptoEditado", bytFilaDgv).Value = blnCostoRealImptoEditado
                    .Item("MargenAplicadoEditado", bytFilaDgv).Value = blnMargenAplicadoEditado
                    .Item("ServicioEditado", bytFilaDgv).Value = blnServicioEditado

                    .Item("FlSSCREditado", bytFilaDgv).Value = blnSSCREditado
                    .Item("FlSTCREditado", bytFilaDgv).Value = blnSTCREditado

                    .Item("PlanAlimenticio", bytFilaDgv).Value = blnPlanAlimenticio
                    .Item("DebitMemoPendiente", bytFilaDgv).Value = True
                    .Item("ServicioVarios", bytFilaDgv).Value = blnServicioVarios
                    .Item("FlTarifaCambiada", bytFilaDgv).Value = blnRecalTarifaPendiente
                    .Item("AcomodoEspecial", bytFilaDgv).Value = chkAcomodoEspecial.Checked
                    .Item("DescFechaEsp", bytFilaDgv).Value = objDiasEsp.strDevuelveDescripcionxFechaxUbigeo(dtpDia.Value.ToShortDateString, cboCiudad.SelectedValue.ToString)
                    .Item("IDCabCopia", bytFilaDgv).Value = intIDCab.ToString
                    .Item("CoMonedaServ", bytFilaDgv).Value = strCoMoneda
                    .Item("FlServicioTC", bytFilaDgv).Value = blnServicioTC
                    .Item("IDServicio_V", bytFilaDgv).Value = strIDServicio_V
                    .Item("FlAcomodosVehic", bytFilaDgv).Value = False
                    .Item("IDDetAntiguo", bytFilaDgv).Value = intIDDetAntiguo
                    GrabarDetCotiPax(bytFilaDgv)

                Else
                    Dim objBNText As New clsServicioProveedorBN.clsServiciosporDiaBN
                    DtTextoIdiom = objBNText.ConsultarListxIDServicio(strIDServicio)

                    Dim bln1erDia As Boolean = True
                    Dim datFecha As Date
                    Dim bytContHijos As Byte = 0
                    Dim bytContDias As Byte = 1
                    Dim bytContDiasTmp As Byte = 1
                    Dim strIDDetRelPadre As String = ""
                    'For Each dr As DataRow In dttDias.Rows
                    Dim dcServicio As New Dictionary(Of String, String)
                    Dim bytDiasServiciosPadHij As Byte = 1
                    While drDias.Read

                        Dim bytFilaDgv As Byte = .RowCount
                        Dim TmpBytFilaPH As Byte = bytFilaDgv
                        'If Not blnDgvFilaVacia Then
                        '    .Rows.Add()
                        'Else
                        '    If bytFilaDgv > 0 Then bytFilaDgv -= 1
                        'End If
                        .Rows.Add()

                        If bln1erDia Then
                            datFecha = dtpDia.Value
                            '.Item("Item", bytFilaDgv).Value = sglDevuelveItemCorrecto(frmRef.dgvDet, Format(dtpDia.Value, "dd/MM/yyyy HH:mm:ss"), cboTipoProv.SelectedValue.ToString, strModoEdicion)
                        Else
                            datFecha = DateAdd(DateInterval.Day, 1, datFecha)
                            '.Item("Item", bytFilaDgv).Value = sglDevuelveItemCorrecto(frmRef.dgvDet, Format(datFecha, "dd/MM/yyyy HH:mm:ss"), cboTipoProv.SelectedValue.ToString, strModoEdicion, strIDDetRelPadre)
                        End If
                        'If bytFilaDgv = 0 Then
                        '.Item("Item", bytFilaDgv).Value = Convert.ToSingle(bytFilaDgv + 1)

                        'Else
                        '    If cboTipoProv.SelectedValue.ToString <> gstrTipoProveeHoteles Then
                        '        .Item("Item", bytFilaDgv).Value = Convert.ToSingle(bytFilaDgv + 1)
                        '    Else
                        '        .Item("Item", bytFilaDgv).Value = sglDevuelveItemMayorDia()
                        '    End If
                        'End If
                        .Item("IDDet", bytFilaDgv).Value = "M" & frmRef.bytMayorIddetGenerado()

                        .Item("Dia", bytFilaDgv).Value = Format(datFecha, "dd/MM/yyyy HH:mm:ss")

                        If cboTipoProv.SelectedValue.ToString = gstrTipoProveeHoteles Then
                            .Item("DiaFormat", bytFilaDgv).Value = datFecha.ToString("ddd dd/MM/yyyy").ToUpper
                        Else
                            .Item("DiaFormat", bytFilaDgv).Value = datFecha.ToString("ddd dd/MM/yyyy").ToUpper
                        End If

                        If cboTipoProv.SelectedValue.ToString = gstrTipoProveeHoteles Then
                            .Item("Hora", bytFilaDgv).Value = ""
                        Else
                            .Item("Hora", bytFilaDgv).Value = dtpDiaHora.Value.ToString("HH:mm")
                        End If

                        frmRef.dtpFechaOut.Value = datFecha
                        .Item("IDPais", bytFilaDgv).Value = cboPais.SelectedValue.ToString
                        .Item("IDCiudad", bytFilaDgv).Value = cboCiudad.SelectedValue.ToString
                        .Item("IDTipoProv", bytFilaDgv).Value = cboTipoProv.SelectedValue.ToString
                        .Item("IDTipoOper", bytFilaDgv).Value = strIDTipoOper
                        .Item("Dias", bytFilaDgv).Value = bytContDias
                        bytContDias += 1
                        .Item("IDProveedor", bytFilaDgv).Value = strIDProveedor
                        .Item("DescProveedor", bytFilaDgv).Value = lblProveedor.Text
                        .Item("IDServicio", bytFilaDgv).Value = strIDServicio
                        .Item("IDServicio_Det", bytFilaDgv).Value = intIDServicio_Det

                        .Item("DescCiudad", bytFilaDgv).Value = cboCiudad.Text
                        '.Item("DescServicioDet", bytFilaDgv).Value = drDias("DescripCorta")
                        '.Item("DescServicioDet", bytFilaDgv).Value = dr("Descripcion")
                        .Item("Tipo", bytFilaDgv).Value = strTipoServicio_Det
                        .Item("TipoTriple", bytFilaDgv).Value = strDescripcionTipoTriple


                        .Item("NroLiberados", bytFilaDgv).Value = txtNroLiberados.Text
                        .Item("Tipo_Lib", bytFilaDgv).Value = If(cboTipoLib.Text = "", "", cboTipoLib.SelectedValue.ToString)
                        .Item("NroPax", bytFilaDgv).Value = txtNroPax.Text
                        If chkIncGuia.Checked Then
                            .Item("PaxmasLiberados", bytFilaDgv).Value = "1"
                        Else
                            If txtNroLiberados.Text.Trim <> "" And txtNroLiberados.Text.Trim <> "0" Then
                                .Item("PaxmasLiberados", bytFilaDgv).Value = txtNroPax.Text.Trim & "+" & txtNroLiberados.Text.Trim
                            Else
                                .Item("PaxmasLiberados", bytFilaDgv).Value = txtNroPax.Text.Trim
                            End If
                        End If

                        If cboTipoProv.SelectedValue.ToString = gstrTipoProveeRestaurantes Then
                            .Item("Desayuno", bytFilaDgv).Value = chkDesayuno.Checked
                            .Item("Lonche", bytFilaDgv).Value = chkLonche.Checked
                            .Item("Almuerzo", bytFilaDgv).Value = chkAlmuerzo.Checked
                            .Item("Cena", bytFilaDgv).Value = chkCena.Checked
                        Else
                            .Item("Desayuno", bytFilaDgv).Value = If(IsDBNull(drDias("Desayuno")) = True, 0, drDias("Desayuno"))
                            .Item("Lonche", bytFilaDgv).Value = If(IsDBNull(drDias("Lonche")) = True, 0, drDias("Lonche"))
                            .Item("Almuerzo", bytFilaDgv).Value = If(IsDBNull(drDias("Almuerzo")) = True, 0, drDias("Almuerzo"))
                            .Item("Cena", bytFilaDgv).Value = If(IsDBNull(drDias("Cena")) = True, 0, drDias("Cena"))
                        End If


                        .Item("Transfer", bytFilaDgv).Value = chkTransfer.Checked
                        If cboOrigen.Text <> "" Then
                            '.Item("OrigenTransfer", bytFilaDgv).Value = cboOrigen.SelectedValue.ToString.Trim
                            .Item("IDDetTransferOri", bytFilaDgv).Value = cboOrigen.SelectedValue.ToString.Trim
                        Else
                            '.Item("OrigenTransfer", bytFilaDgv).Value = ""
                            .Item("IDDetTransferOri", bytFilaDgv).Value = "0"
                        End If
                        If cboDestino.Text <> "" Then
                            '.Item("DestinoTransfer", bytFilaDgv).Value = cboDestino.SelectedValue.ToString.Trim
                            .Item("IDDetTransferDes", bytFilaDgv).Value = cboDestino.SelectedValue.ToString.Trim
                        Else
                            '.Item("DestinoTransfer", bytFilaDgv).Value = ""
                            .Item("IDDetTransferDes", bytFilaDgv).Value = "0"
                        End If

                        .Item("TipoTransporte", bytFilaDgv).Value = If(cboTipoTransporte.Text = "", "", cboTipoTransporte.SelectedValue.ToString)

                        .Item("IDUbigeoOri", bytFilaDgv).Value = If(IsDBNull(drDias("IDUbigeoOri")) = True, "000001", drDias("IDUbigeoOri"))
                        .Item("DescUbigeoOri", bytFilaDgv).Value = If(IsDBNull(drDias("DescUbigeoOri")) = True, "", drDias("DescUbigeoOri"))
                        .Item("IDUbigeoDes", bytFilaDgv).Value = If(IsDBNull(drDias("IDUbigeoDes")) = True, "000001", drDias("IDUbigeoDes"))
                        .Item("DescUbigeoDes", bytFilaDgv).Value = If(IsDBNull(drDias("DescUbigeoDes")) = True, "", drDias("DescUbigeoDes"))

                        .Item("ConAlojamiento", bytFilaDgv).Value = blnConAlojamiento
                        .Item("DescFechaEsp", bytFilaDgv).Value = objDiasEsp.strDevuelveDescripcionxFechaxUbigeo(dtpDia.Value.ToShortDateString, cboCiudad.SelectedValue.ToString)
                        .Item("IDCabCopia", bytFilaDgv).Value = intIDCab.ToString
                        .Item("CoMonedaServ", bytFilaDgv).Value = strCoMoneda
                        .Item("IDDetAntiguo", bytFilaDgv).Value = intIDDetAntiguo
                        .Item("FlServicioTC", bytFilaDgv).Value = blnServicioTC

                        If bln1erDia Then
                            strIDDet1erDia = .Item("IDDet", bytFilaDgv).Value
                            .Item("CostoLiberado", bytFilaDgv).Value = lblCostoLiberado.Text
                            .Item("CostoReal", bytFilaDgv).Value = txtCostoReal.Text
                            .Item("CostoRealAnt", bytFilaDgv).Value = dblCostoRealOrig
                            .Item("Margen", bytFilaDgv).Value = lblMargen.Text
                            .Item("MargenAplicado", bytFilaDgv).Value = txtMargenAplicado.Text
                            .Item("MargenAplicadoAnt", bytFilaDgv).Value = dblMargenAplicadoOrig
                            .Item("MargenLiberado", bytFilaDgv).Value = lblMargenLiberado.Text
                            .Item("SSCL", bytFilaDgv).Value = txtSSCL.Text
                            .Item("SSCR", bytFilaDgv).Value = txtSSCR.Text
                            .Item("SSMargen", bytFilaDgv).Value = txtSSMargen.Text
                            .Item("SSMargenLiberado", bytFilaDgv).Value = txtSSMargenLiberado.Text
                            .Item("SSTotal", bytFilaDgv).Value = txtSSTotal.Text
                            .Item("SSTotalOrig", bytFilaDgv).Value = txtSSTotalOrig.Text
                            .Item("STCR", bytFilaDgv).Value = txtSTCR.Text
                            .Item("STMargen", bytFilaDgv).Value = txtSTMargen.Text
                            .Item("STTotal", bytFilaDgv).Value = txtSTTotal.Text
                            .Item("STTotalOrig", bytFilaDgv).Value = txtSTTotalOrig.Text
                            .Item("Total", bytFilaDgv).Value = lblTotal.Text
                            .Item("RedondeoTotal", bytFilaDgv).Value = lblRedondeoTotal.Text
                            .Item("TotalOrig", bytFilaDgv).Value = lblTotalOrig.Text
                            .Item("CostoLiberadoImpto", bytFilaDgv).Value = lblCostoLiberadoImpto.Text
                            .Item("CostoRealImpto", bytFilaDgv).Value = txtCostoRealImpto.Text
                            .Item("MargenImpto", bytFilaDgv).Value = lblMargenImpto.Text
                            .Item("MargenLiberadoImpto", bytFilaDgv).Value = lblMargenLiberadoImpto.Text
                            .Item("SSCLImpto", bytFilaDgv).Value = lblSSCLImpto.Text
                            .Item("SSCRImpto", bytFilaDgv).Value = lblSSCRImpto.Text
                            .Item("SSMargenImpto", bytFilaDgv).Value = lblSSMargenImpto.Text
                            .Item("SSMargenLiberadoImpto", bytFilaDgv).Value = lblSSMargenLiberadoImpto.Text
                            .Item("STCRImpto", bytFilaDgv).Value = lblSTCRImpto.Text
                            .Item("STMargenImpto", bytFilaDgv).Value = lblSTMargenImpto.Text
                            .Item("TotImpto", bytFilaDgv).Value = lblTotImpto.Text
                            .Item("Especial", bytFilaDgv).Value = chkEspecial.Checked
                            .Item("MotivoEspecial", bytFilaDgv).Value = txtMotivoEspecial.Text
                            '.Item("RutaDocSustento", bytFilaDgv).Value = txtRutaDocSustento.Text
                            .Item("RutaDocSustento", bytFilaDgv).Value = lnkDocumentoSustento.Text
                            .Item("IncGuia", bytFilaDgv).Value = chkIncGuia.Checked
                            .Item("AcomodoEspecial", bytFilaDgv).Value = chkAcomodoEspecial.Checked
                            If Not blnExisteIDDetRel("P" & bytFilaDgv.ToString) Then
                                .Item("IDDetRel", bytFilaDgv).Value = "P" & bytFilaDgv.ToString
                                strIDDetRelPadre = .Item("IDDetRel", bytFilaDgv).Value.ToString
                            Else
                                .Item("IDDetRel", bytFilaDgv).Value = "P" & frmRef.bytMayorIDDetRelGenerado.ToString
                                strIDDetRelPadre = .Item("IDDetRel", bytFilaDgv).Value.ToString
                            End If
                        Else

                            '.Item("NroPax", bytFilaDgv).Value = txtNroPax.Text
                            .Item("CostoLiberado", bytFilaDgv).Value = "0.00"
                            .Item("CostoReal", bytFilaDgv).Value = "0.00"
                            .Item("CostoRealAnt", bytFilaDgv).Value = "0.00"
                            .Item("Margen", bytFilaDgv).Value = "0.00"
                            .Item("MargenAplicado", bytFilaDgv).Value = "0.00"
                            .Item("MargenAplicadoAnt", bytFilaDgv).Value = "0.00"
                            .Item("MargenLiberado", bytFilaDgv).Value = "0.00"
                            .Item("SSCL", bytFilaDgv).Value = "0.00"
                            .Item("SSCR", bytFilaDgv).Value = "0.00"
                            .Item("SSMargen", bytFilaDgv).Value = "0.00"
                            .Item("SSMargenLiberado", bytFilaDgv).Value = "0.00"
                            .Item("SSTotal", bytFilaDgv).Value = "0.00"
                            .Item("SSTotalOrig", bytFilaDgv).Value = "0.00"
                            .Item("STCR", bytFilaDgv).Value = "0.00"
                            .Item("STMargen", bytFilaDgv).Value = "0.00"
                            .Item("STTotal", bytFilaDgv).Value = "0.00"
                            .Item("STTotalOrig", bytFilaDgv).Value = "0.00"
                            .Item("Total", bytFilaDgv).Value = "0.00"
                            .Item("RedondeoTotal", bytFilaDgv).Value = "0.00"
                            .Item("TotalOrig", bytFilaDgv).Value = "0.00"
                            .Item("CostoLiberadoImpto", bytFilaDgv).Value = "0.00"
                            .Item("CostoRealImpto", bytFilaDgv).Value = "0.00"
                            .Item("MargenImpto", bytFilaDgv).Value = "0.00"
                            .Item("MargenLiberadoImpto", bytFilaDgv).Value = "0.00"
                            .Item("SSCLImpto", bytFilaDgv).Value = "0.00"
                            .Item("SSCRImpto", bytFilaDgv).Value = "0.00"
                            .Item("SSMargenImpto", bytFilaDgv).Value = "0.00"
                            .Item("SSMargenLiberadoImpto", bytFilaDgv).Value = "0.00"
                            .Item("STCRImpto", bytFilaDgv).Value = "0.00"
                            .Item("STMargenImpto", bytFilaDgv).Value = "0.00"
                            .Item("TotImpto", bytFilaDgv).Value = "0.00"
                            .Item("Especial", bytFilaDgv).Value = True
                            .Item("MotivoEspecial", bytFilaDgv).Value = "Día del Servicio"
                            .Item("RutaDocSustento", bytFilaDgv).Value = ""
                            .Item("IncGuia", bytFilaDgv).Value = chkIncGuia.Checked
                            .Item("AcomodoEspecial", bytFilaDgv).Value = chkAcomodoEspecial.Checked
                            '.Item("IDDet", bytFilaDgv).Value = "P" & (bytFilaDgv - 1).ToString
                            .Item("IDDetRel", bytFilaDgv).Value = strIDDetRelPadre.Replace("P", "H") '"H" & ((bytFilaDgv - bytContHijos) - 1).ToString
                            '.Item("FlServicioTC", bytFilaDgv).Value = blnServicioTC
                            bytContHijos += 1
                            bytDiasServiciosPadHij += 1
                        End If

                        'Evaluación del Idioma
                        Dim strTmpIDIoma As String = ""
                        If strIDTipoServ.Trim = "NAP" Or strIDTipoServ.Trim = "OT" Then
                            strTmpIDIoma = "NO APLICA"
                        Else
                            If strIDTipoServ.Trim = "SIC" And strIDIdioma.Trim <> "SPANISH" Then
                                strTmpIDIoma = "ENGLISH"
                            Else
                                strTmpIDIoma = cboIDIdioma.SelectedValue.ToString
                            End If
                        End If

                        cboIDIdioma.SelectedValue = strTmpIDIoma
                        .Item("IDIdioma", bytFilaDgv).Value = strTmpIDIoma 'If(strIDTipoServ.Trim = "NAP" Or strIDTipoServ.Trim = "OT", "NO APLICA", cboIDIdioma.SelectedValue.ToString)
                        .Item("Siglas", bytFilaDgv).Value = If(strTmpIDIoma = "NO APLICA", "---", strDevuelveCadenaOcultaDerecha(cboIDIdioma.Text))

                        dcServicio.Add(bytFilaDgv, strDescServicio_Det)
                        'If strIDDetRelPadre <> "" Or bytContHijos >= 1 Then
                        '    .Item("DescServicioDet", bytFilaDgv).Value = strDescServicio_Det & " - " & strNumerosDias(bytDiasServiciosPadHij)
                        '    .Item("Servicio", bytFilaDgv).Value = txtServicio.Text & " - " & strNumerosDias(bytDiasServiciosPadHij)
                        'Else
                        '    .Item("DescServicioDet", bytFilaDgv).Value = strDescServicio_Det
                        '    .Item("Servicio", bytFilaDgv).Value = txtServicio.Text
                        'End If

                        .Item("Anio", bytFilaDgv).Value = strAnio

                        .Item("IDTipoServ", bytFilaDgv).Value = strIDTipoServ
                        .Item("DescTipoServ", bytFilaDgv).Value = strDescTipoServ


                        .Item("FueradeHorario", bytFilaDgv).Value = blnFueradeHorario
                        .Item("CostoRealEditado", bytFilaDgv).Value = blnCostoRealEditado
                        .Item("CostoRealImptoEditado", bytFilaDgv).Value = blnCostoRealImptoEditado
                        .Item("MargenAplicadoEditado", bytFilaDgv).Value = blnMargenAplicadoEditado
                        .Item("FlSSCREditado", bytFilaDgv).Value = blnSSCREditado
                        .Item("FlSTCREditado", bytFilaDgv).Value = blnSTCREditado

                        .Item("ServicioEditado", bytFilaDgv).Value = blnServicioEditado
                        .Item("PlanAlimenticio", bytFilaDgv).Value = blnPlanAlimenticio
                        .Item("DebitMemoPendiente", bytFilaDgv).Value = True
                        .Item("ServicioVarios", bytFilaDgv).Value = blnServicioVarios
                        .Item("FlTarifaCambiada", bytFilaDgv).Value = blnRecalTarifaPendiente
                        .Item("FlServicioTC", bytFilaDgv).Value = blnServicioTC
                        .Item("IDServicio_V", bytFilaDgv).Value = strIDServicio_V
                        .Item("FlAcomodosVehic", bytFilaDgv).Value = False
                        bln1erDia = False

                        GrabarDetCotiPax(bytFilaDgv)
                        GrabarDetCotiDescripcion(bytFilaDgv, bytContDiasTmp)
                        bytContDiasTmp += 1
                        'Next
                    End While



                    If dcServicio.Count = 1 Then
                        For Each dc As KeyValuePair(Of String, String) In dcServicio
                            .Item("DescServicioDet", CInt(dc.Key)).Value = strDescServicio_Det
                            .Item("Servicio", CInt(dc.Key)).Value = txtServicio.Text.Trim
                            .Item("IDDetRel", CInt(dc.Key)).Value = "0"
                        Next
                    Else
                        Dim bytDays As Byte = 1
                        For Each dc As KeyValuePair(Of String, String) In dcServicio
                            .Item("DescServicioDet", CInt(dc.Key)).Value = strDescServicio_Det & " - " & strNumerosDias(bytDays)
                            .Item("Servicio", CInt(dc.Key)).Value = strDescServicio_Det & " - " & strNumerosDias(bytDays)
                            bytDays += 1
                        Next
                    End If
                End If
            End With

            For Each dr As DataRow In dttDetCotiServ.Rows
                frmRef.dttDetCotiServ.Rows.Add(dr(0), dr(1), dr(2), dr(3), dr(4), dr(5), dr(6), dr(7), dr(8), dr(9), dr(10), _
                                               dr(11), dr(12), dr(13), dr(14), dr(15), dr(16), dr(17), dr(18), dr(19), dr(20), _
                                               dr(21), dr(22), dr(23), dr(24), dr(25), dr(26), dr(27), dr(28))
            Next
            GrabarQuiebres()

        Catch ex As Exception
            Throw
        End Try
    End Sub

    Private Function blnExisteIDDetRel(ByVal vstrIDDetRel As String) As Boolean
        Try
            Dim blnExiste As Boolean = False
            With frmRef.dgvDet
                For Each Item As DataGridViewRow In .Rows
                    If Item.Cells("IDDetRel").Value IsNot Nothing Then
                        If Item.Cells("IDDetRel").Value.ToString = vstrIDDetRel Then
                            blnExiste = True
                            Exit For
                        End If
                    End If
                Next
            End With
            Return blnExiste
        Catch ex As Exception
            Throw
        End Try
    End Function

    Private Function strNumerosDias(ByVal vBytNumero As Byte) As String
        Try
            Dim strCadena As String = ""
            'Select Case vBytNumero
            '    Case 1
            '        strCadena += "1st Day"
            '    Case 2
            '        strCadena += "2nd Day"
            '    Case 3
            '        strCadena += "3rd Day"
            '    Case 4
            '        strCadena += "4th Day"
            '    Case 5
            '        strCadena += "5th Day"
            '    Case 6
            '        strCadena += "6th Day"
            '    Case 7
            '        strCadena += "7th Day"
            '    Case 8
            '        strCadena += "8th Day"
            '    Case 9
            '        strCadena += "9th Day"
            '    Case 10
            '        strCadena += "10th Day"
            'End Select

            If vBytNumero = 1 Or vBytNumero = 2 Or vBytNumero = 3 Then
                If vBytNumero = 1 Then
                    strCadena += "1st Day"
                ElseIf vBytNumero = 2 Then
                    strCadena += "2nd Day"
                ElseIf vBytNumero = 3 Then
                    strCadena += "3rd Day"
                End If
            Else
                strCadena += vBytNumero.ToString & "th Day"
            End If

            Return strCadena
        Catch ex As Exception
            Throw
        End Try
    End Function

    Private Sub Actualizar()
        Try
            If blnActualizoDatosViaticosTC() Then
                frmRef.blnActualizoViaticosTC = True
            End If

            With frmRef.dgvDet
                Dim bytFilaDgv As Byte = .CurrentRow.Index
                If blnCambioFecha Then
                    '.Item("Item", bytFilaDgv).Value = sglDevuelveItemCorrecto(frmRef.dgvDet, Format(dtpDia.Value, "dd/MM/yyyy HH:mm:ss"), cboTipoProv.SelectedValue.ToString, strModoEdicion)
                End If
                .Item("Dia", bytFilaDgv).Value = CDate(dtpDia.Text & " " & dtpDiaHora.Text)  'Format(dtpDia.Value, "dd/MM/yyyy HH:mm:ss")
                '.Item("Dia", bytFilaDgv).Value = CDate(dtpDia.Text)  'Format(dtpDia.Value, "dd/MM/yyyy HH:mm:ss")

                'If cboTipoProv.SelectedValue.ToString = gstrTipoProveeHoteles Then
                .Item("DiaFormat", bytFilaDgv).Value = dtpDia.Value.ToString("ddd dd/MM/yyyy").ToUpper
                'Else
                '.Item("DiaFormat", bytFilaDgv).Value = dtpDia.Value.ToString("ddd dd/MM/yyyy").ToUpper
                'End If

                If cboTipoProv.SelectedValue.ToString = gstrTipoProveeHoteles And .Item("PlanAlimenticio", bytFilaDgv).Value = False Then
                    .Item("Hora", bytFilaDgv).Value = ""
                Else
                    .Item("Hora", bytFilaDgv).Value = Format(dtpDiaHora.Value, "HH:mm")
                End If


                .Item("IDPais", bytFilaDgv).Value = cboPais.SelectedValue.ToString
                .Item("IDCiudad", bytFilaDgv).Value = cboCiudad.SelectedValue.ToString
                .Item("IDTipoProv", bytFilaDgv).Value = cboTipoProv.SelectedValue.ToString
                .Item("IDProveedor", bytFilaDgv).Value = strIDProveedor
                .Item("DescProveedor", bytFilaDgv).Value = lblProveedor.Text
                .Item("IDServicio", bytFilaDgv).Value = strIDServicio
                .Item("IDServicio_Det", bytFilaDgv).Value = intIDServicio_Det

                .Item("DescCiudad", bytFilaDgv).Value = cboCiudad.Text

                'If cboTipoProv.SelectedValue.ToString = gstrTipoProveeHoteles Or cboTipoProv.SelectedValue.ToString = gstrTipoProveeRestaurantes Then
                '.Item("DescServicioDet", bytFilaDgv).Value = strDescServicio_Det
                'Else

                .Item("DescServicioDet", bytFilaDgv).Value = txtServicio.Text

                'End If


                .Item("Tipo", bytFilaDgv).Value = strTipoServicio_Det
                .Item("TipoTriple", bytFilaDgv).Value = strDescripcionTipoTriple

                If intIDDet = 0 Then
                    If strIDDetRel = "" Then
                        .Item("NroPax", bytFilaDgv).Value = txtNroPax.Text
                    Else
                        .Item("NroPax", bytFilaDgv).Value = lvwPax.CheckedItems.Count
                    End If
                Else
                    .Item("NroPax", bytFilaDgv).Value = txtNroPax.Text
                End If
                .Item("NroLiberados", bytFilaDgv).Value = txtNroLiberados.Text

                If chkIncGuia.Checked Then
                    .Item("NroPax", bytFilaDgv).Value = "1"
                    .Item("NroLiberados", bytFilaDgv).Value = "0"
                End If

                If chkIncGuia.Checked Then
                    .Item("PaxmasLiberados", bytFilaDgv).Value = "1"
                Else
                    If txtNroLiberados.Text.Trim <> "" And txtNroLiberados.Text.Trim <> "0" Then
                        .Item("PaxmasLiberados", bytFilaDgv).Value = txtNroPax.Text.Trim & "+" & txtNroLiberados.Text.Trim
                    Else
                        .Item("PaxmasLiberados", bytFilaDgv).Value = txtNroPax.Text.Trim
                    End If
                End If


                .Item("Tipo_Lib", bytFilaDgv).Value = If(cboTipoLib.Text = "", "", cboTipoLib.SelectedValue.ToString)

                .Item("CostoLiberado", bytFilaDgv).Value = lblCostoLiberado.Text
                .Item("CostoReal", bytFilaDgv).Value = txtCostoReal.Text
                .Item("CostoRealAnt", bytFilaDgv).Value = dblCostoRealOrig
                .Item("Margen", bytFilaDgv).Value = lblMargen.Text
                .Item("MargenAplicado", bytFilaDgv).Value = txtMargenAplicado.Text
                .Item("MargenAplicadoAnt", bytFilaDgv).Value = dblMargenAplicadoOrig
                .Item("MargenLiberado", bytFilaDgv).Value = lblMargenLiberado.Text

                .Item("SSCL", bytFilaDgv).Value = txtSSCL.Text
                'If txtSSCR2.Visible = False Then
                .Item("SSCR", bytFilaDgv).Value = txtSSCR.Text
                'Else
                '.Item("SSCR", bytFilaDgv).Value = txtSSCR2.Text
                'End If

                .Item("SSMargen", bytFilaDgv).Value = txtSSMargen.Text
                .Item("SSMargenLiberado", bytFilaDgv).Value = txtSSMargenLiberado.Text

                .Item("SSTotal", bytFilaDgv).Value = txtSSTotal.Text
                .Item("SSTotalOrig", bytFilaDgv).Value = txtSSTotalOrig.Text
                'If txtSTCR2.Visible = False Then
                .Item("STCR", bytFilaDgv).Value = txtSTCR.Text
                'Else
                '.Item("STCR", bytFilaDgv).Value = txtSTCR2.Text
                'End If


                .Item("STMargen", bytFilaDgv).Value = txtSTMargen.Text
                .Item("STTotal", bytFilaDgv).Value = txtSTTotal.Text
                .Item("STTotalOrig", bytFilaDgv).Value = txtSTTotalOrig.Text
                .Item("Total", bytFilaDgv).Value = lblTotal.Text
                .Item("RedondeoTotal", bytFilaDgv).Value = lblRedondeoTotal.Text
                .Item("TotalOrig", bytFilaDgv).Value = lblTotalOrig.Text

                .Item("CostoLiberadoImpto", bytFilaDgv).Value = lblCostoLiberadoImpto.Text
                .Item("CostoRealImpto", bytFilaDgv).Value = txtCostoRealImpto.Text
                .Item("MargenImpto", bytFilaDgv).Value = lblMargenImpto.Text
                .Item("MargenLiberadoImpto", bytFilaDgv).Value = lblMargenLiberadoImpto.Text
                .Item("SSCLImpto", bytFilaDgv).Value = lblSSCLImpto.Text
                .Item("SSCRImpto", bytFilaDgv).Value = lblSSCRImpto.Text
                .Item("SSMargenImpto", bytFilaDgv).Value = lblSSMargenImpto.Text
                .Item("SSMargenLiberadoImpto", bytFilaDgv).Value = lblSSMargenLiberadoImpto.Text
                .Item("STCRImpto", bytFilaDgv).Value = lblSTCRImpto.Text
                .Item("STMargenImpto", bytFilaDgv).Value = lblSTMargenImpto.Text

                '.Item("TotImpto", bytFilaDgv).Value = Convert.ToDouble(lblCostoLiberadoImpto.Text) + _
                '    Convert.ToDouble(txtCostoRealImpto.Text) + Convert.ToDouble(lblMargenImpto.Text) + _
                '    Convert.ToDouble(lblMargenLiberadoImpto.Text) + Convert.ToDouble(lblSSCLImpto.Text) + _
                '    Convert.ToDouble(lblSSCRImpto.Text) + Convert.ToDouble(lblSTMargenImpto.Text) + _
                '    Convert.ToDouble(lblSSMargenLiberadoImpto.Text) + Convert.ToDouble(lblSTCRImpto.Text) + Convert.ToDouble(lblSTMargenImpto.Text)

                .Item("TotImpto", bytFilaDgv).Value = lblTotImpto.Text

                .Item("Especial", bytFilaDgv).Value = chkEspecial.Checked
                .Item("MotivoEspecial", bytFilaDgv).Value = txtMotivoEspecial.Text
                '.Item("RutaDocSustento", bytFilaDgv).Value = txtRutaDocSustento.Text
                .Item("RutaDocSustento", bytFilaDgv).Value = lnkDocumentoSustento.Text
                'If chkEspecial.Checked Then
                '    frmRef.txtObsGeneral.Text += txtMotivoEspecial.Text.Trim
                'End If

                .Item("Desayuno", bytFilaDgv).Value = chkDesayuno.Checked
                .Item("Lonche", bytFilaDgv).Value = chkLonche.Checked
                .Item("Almuerzo", bytFilaDgv).Value = chkAlmuerzo.Checked
                .Item("Cena", bytFilaDgv).Value = chkCena.Checked
                .Item("Transfer", bytFilaDgv).Value = chkTransfer.Checked

                If cboOrigen.Text <> "" Then
                    '.Item("OrigenTransfer", bytFilaDgv).Value = cboOrigen.SelectedValue.ToString.Trim
                    .Item("IDDetTransferOri", bytFilaDgv).Value = cboOrigen.SelectedValue.ToString.Trim
                Else
                    '.Item("OrigenTransfer", bytFilaDgv).Value = ""
                    .Item("IDDetTransferOri", bytFilaDgv).Value = "0"
                End If
                If cboDestino.Text <> "" Then
                    '.Item("DestinoTransfer", bytFilaDgv).Value = cboDestino.SelectedValue.ToString.Trim
                    .Item("IDDetTransferDes", bytFilaDgv).Value = cboDestino.SelectedValue.ToString.Trim
                Else
                    '.Item("DestinoTransfer", bytFilaDgv).Value = ""
                    .Item("IDDetTransferDes", bytFilaDgv).Value = "0"
                End If


                .Item("TipoTransporte", bytFilaDgv).Value = If(cboTipoTransporte.Text = "", "", cboTipoTransporte.SelectedValue.ToString)


                .Item("IDUbigeoOri", bytFilaDgv).Value = If(cboIDUbigeoOri.Text = "" Or cboIDUbigeoOri.Text = "-------", "000001", cboIDUbigeoOri.SelectedValue.ToString)
                .Item("DescUbigeoOri", bytFilaDgv).Value = cboIDUbigeoOri.Text
                .Item("IDUbigeoDes", bytFilaDgv).Value = If(cboIDUbigeoDes.Text = "" Or cboIDUbigeoDes.Text = "-------", "000001", cboIDUbigeoDes.SelectedValue.ToString)
                .Item("DescUbigeoDes", bytFilaDgv).Value = cboIDUbigeoDes.Text

                '.Item("IDIdioma", bytFilaDgv).Value = cboIDIdioma.SelectedValue.ToString
                '.Item("Siglas", bytFilaDgv).Value = strDevuelveCadenaOcultaDerecha(cboIDIdioma.Text)

                'Dim blnTipoServOT As Boolean = If(strIDTipoServ.Trim = "OT", True, False)

                'Evaluación del Idioma
                Dim strTmpIDIoma As String = ""
                If strIDTipoServ.Trim = "NAP" Or strIDTipoServ.Trim = "OT" Then
                    strTmpIDIoma = "NO APLICA"
                Else
                    strTmpIDIoma = cboIDIdioma.SelectedValue.ToString
                End If

                cboIDIdioma.SelectedValue = strTmpIDIoma
                .Item("IDIdioma", bytFilaDgv).Value = strTmpIDIoma 'If(strIDTipoServ.Trim = "NAP" Or strIDTipoServ.Trim = "OT", "NO APLICA", cboIDIdioma.SelectedValue.ToString)
                .Item("Siglas", bytFilaDgv).Value = If(strTmpIDIoma = "NO APLICA", "---", strDevuelveCadenaOcultaDerecha(cboIDIdioma.Text))

                '.Item("IDIdioma", bytFilaDgv).Value = If(strIDTipoServ = "NAP" Or strIDTipoServ.Trim = "OT", "NO APLICA", cboIDIdioma.SelectedValue.ToString)
                '.Item("Siglas", bytFilaDgv).Value = If(strIDTipoServ = "NAP" Or strIDTipoServ.Trim = "OT", "---", strDevuelveCadenaOcultaDerecha(cboIDIdioma.Text))

                .Item("Servicio", bytFilaDgv).Value = txtServicio.Text

                .Item("IncGuia", bytFilaDgv).Value = chkIncGuia.Checked
                .Item("FueradeHorario", bytFilaDgv).Value = blnFueradeHorario

                .Item("CostoRealEditado", bytFilaDgv).Value = blnCostoRealEditado
                .Item("CostoRealImptoEditado", bytFilaDgv).Value = blnCostoRealImptoEditado
                .Item("MargenAplicadoEditado", bytFilaDgv).Value = blnMargenAplicadoEditado
                .Item("ServicioEditado", bytFilaDgv).Value = blnServicioEditado
                .Item("FlTarifaCambiada", bytFilaDgv).Value = blnRecalTarifaPendiente
                .Item("AcomodoEspecial", bytFilaDgv).Value = chkAcomodoEspecial.Checked
                .Item("FlSSCREditado", bytFilaDgv).Value = blnSSCREditado
                .Item("FlSTCREditado", bytFilaDgv).Value = blnSTCREditado
                .Item("FlAcomodosVehic", bytFilaDgv).Value = chkAcomVehi.Checked
                .Columns("DiaFormat").Width = 100

                CopiaraHijos(bytFilaDgv)

            End With

            'Solo para casos de Nuevos servicios o servicios no editados.
            If Not IsNumeric(strIDDet) And dttDetCotiServ.Rows.Count = 0 Then
                Dim dblCostoReal As Double = -1
                Dim dblMargenAplicado As Double = -1
                Dim dblSSCR As Double = -1
                Dim dblSTCR As Double = -1
                Dim dblCostoRealTxt As Double = If(txtCostoReal.Text = "", "0", txtCostoReal.Text)
                Dim dblMargenAplicadoTxt As Double = If(txtMargenAplicado.Text = "", "0", txtMargenAplicado.Text)
                Dim dblSSCRTxt As Double = If(txtSSCR2.Text = "", "0", txtSSCR2.Text)
                Dim dblSTCRTxt As Double = If(txtSTCR2.Text = "", "0", txtSTCR2.Text)
                If blnCostoRealEditado Then
                    dblCostoReal = dblCostoRealTxt
                End If
                If blnMargenAplicadoEditado Then
                    dblMargenAplicado = dblMargenAplicadoTxt
                End If
                If blnSSCREditado Then
                    dblSSCR = dblSSCRTxt
                End If
                If blnSTCREditado Then
                    dblSTCR = dblSTCRTxt
                End If
                CalcularMontos(dblCostoReal, dblMargenAplicado, dblCostoReal, dblMargenAplicado, _
                            strTipoPaxNacionalExtranjero, dblSSCR, dblSTCR)
            End If
Borrar:
            For Each dr As DataRow In frmRef.dttDetCotiServ.Rows
                If dr("IDDet") = strIDDet Then
                    frmRef.dttDetCotiServ.Rows.Remove(dr)
                    GoTo Borrar
                End If
            Next

            For Each dr As DataRow In dttDetCotiServ.Rows
                frmRef.dttDetCotiServ.Rows.Add(dr(0), dr(1), dr(2), dr(3), dr(4), dr(5), dr(6), dr(7), dr(8), dr(9), dr(10), _
                                               dr(11), dr(12), dr(13), dr(14), dr(15), dr(16), dr(17), dr(18), dr(19), dr(20), _
                                               dr(21), dr(22), dr(23), dr(24), dr(25), dr(26), dr(27), dr(28))
            Next

            GrabarQuiebres()
            GrabarDetCotiPax(frmRef.dgvDet.CurrentRow.Index)

            dgvIdiom.CommitEdit(DataGridViewDataErrorContexts.LeaveControl)
            For Each FilaDgv As DataGridViewRow In dgvIdiom.Rows
                For bytFilaCol As Byte = 0 To dgvIdiom.Columns.Count - 1
                    If dgvIdiom.Columns(bytFilaCol).Visible Then
                        AgregarEditarValorStrucTextoIdioma(FilaDgv.Index, bytFilaCol, "")
                    Else
                        AgregarEditarValorStrucTextoIdioma(FilaDgv.Index, bytFilaCol, "B")
                    End If
                Next
            Next


            'If chkAcomVehi.Checked Then
            '    If strIDTipoOper = "I" Then
            '        For Each dr As DataGridViewRow In dgvAcoVeh.Rows
            '            ActualizarAcomoVehicST(dr.Index)
            '        Next
            '        'frmRef.ListaAcomoVehic = lstListaAcomodoVehiculo()
            '        GrabarListaAcomodoVehiculoInt_a_BE()
            '    ElseIf strIDTipoOper = "E" Then
            '        GrabarListaAcomodoVehiculoExt_a_BE()
            '    End If

            'End If
            GrabarListaAcomodoVehiculoBE()


        Catch ex As Exception
            Throw
        End Try
    End Sub
    Private Sub GrabarListaAcomodoVehiculoBE()
        Try
            'If chkAcomVehi.Visible Then
            If strIDTipoOper = "I" Then
                For Each dr As DataGridViewRow In dgvAcoVeh.Rows
                    ActualizarAcomoVehicST(dr.Index)
                Next
                'frmRef.ListaAcomoVehic = lstListaAcomodoVehiculo()
                GrabarListaAcomodoVehiculoInt_a_BE()
                GrabarListaDetPaxAcomodoVehiculoInt_a_BE()
            ElseIf strIDTipoOper = "E" Then
                GrabarListaAcomodoVehiculoExt_a_BE()
                GrabarListaDetPaxAcomodoVehiculoExt_a_BE()
            End If
            GrabarListaCopiaAcomodoVehiculo()
            'End If
        Catch ex As Exception
            Throw
        End Try
    End Sub
    'Private Function lstListaAcomodoVehiculo() As List(Of clsCotizacionBE.clsAcomodoVehiculoBE)
    '    Try
    '        Dim ListaAcomodoVehiculoLoc As New List(Of clsCotizacionBE.clsAcomodoVehiculoBE)
    '        For Each ST As stAcomodoVehiculo In ListaAcomodoVehiculo
    '            Dim NewItem As New clsCotizacionBE.clsAcomodoVehiculoBE With {.NuVehiculo = ST.NuVehiculo, _
    '                                                                .IDDet = intIDDet, _
    '                                                                .QtVehiculos = ST.QtVehiculos, _
    '                                                                .NoVehiculo = ST.NoVehiculo, _
    '                                                                .QtCapacidad = ST.QtCapacidad, _
    '                                                                .QtPax = ST.QtPax, _
    '                                                                .FlGuia = ST.FlGuia, _
    '                                                                .UserMod = gstrUser}
    '            ListaAcomodoVehiculoLoc.Add(NewItem)
    '        Next
    '        Return ListaAcomodoVehiculoLoc

    '    Catch ex As Exception
    '        Throw
    '    End Try
    'End Function
    Private Sub GrabarListaAcomodoVehiculoInt_a_BE()
        Try
InicioBorrar:
            For Each ItemAcVe As clsCotizacionBE.clsAcomodoVehiculoBE In frmRef.ListaAcomoVehic
                If ItemAcVe.IDDet = strIDDet Then
                    frmRef.ListaAcomoVehic.Remove(ItemAcVe)
                    GoTo InicioBorrar
                End If
            Next

            For Each ST As stAcomodoVehiculo In ListaAcomodoVehiculo
                Dim NewItem As New clsCotizacionBE.clsAcomodoVehiculoBE With {.NuVehiculo = ST.NuVehiculo, _
                                                                    .IDDet = strIDDet, _
                                                                    .QtVehiculos = ST.QtVehiculos, _
                                                                    .NoVehiculo = ST.NoVehiculo, _
                                                                    .QtCapacidad = ST.QtCapacidad, _
                                                                    .QtPax = ST.QtPax, _
                                                                    .FlGuia = ST.FlGuia, _
                                                                    .FlCopiar = chkSelCopiaAcoVeh.Checked, _
                                                                    .UserMod = gstrUser}
                frmRef.ListaAcomoVehic.Add(NewItem)
            Next


        Catch ex As Exception
            Throw
        End Try
    End Sub

    Private Sub GrabarListaDetPaxAcomodoVehiculoInt_a_BE()
        Try
InicioBorrar:
            For Each ItemAcVe As clsCotizacionBE.clsAcomodoVehiculoIntDetPaxBE In frmRef.ListaDetAcomVehiIntDetPax
                If ItemAcVe.IDDet = strIDDet Then
                    frmRef.ListaDetAcomVehiIntDetPax.Remove(ItemAcVe)
                    GoTo InicioBorrar
                End If
            Next

            For Each ST As clsCotizacionBE.clsAcomodoVehiculoIntDetPaxBE In ListaDetAcomVehiIntDetPax
                Dim NewItem As New clsCotizacionBE.clsAcomodoVehiculoIntDetPaxBE With {.NuVehiculo = ST.NuVehiculo, _
                                                                                       .NuNroVehiculo = ST.NuNroVehiculo, _
                                                                    .IDDet = strIDDet, _
                                                                                       .NuPax = ST.NuPax, _
                                                                                       .Selecc = ST.Selecc, _
                                                                                       .FlGuia = ST.FlGuia, _
                                                                                       .UserMod = gstrUser}
                frmRef.ListaDetAcomVehiIntDetPax.Add(NewItem)
            Next


        Catch ex As Exception
            Throw
        End Try
    End Sub

    Private Sub GrabarListaDetPaxAcomodoVehiculoExt_a_BE()
        Try
InicioBorrar:
            For Each ItemAcVe As clsCotizacionBE.clsAcomodoVehiculoExtDetPaxBE In frmRef.ListaDetAcomVehiExtDetPax
                If ItemAcVe.IDDet = strIDDet Then
                    frmRef.ListaDetAcomVehiExtDetPax.Remove(ItemAcVe)
                    GoTo InicioBorrar
                End If
            Next

            For Each ST As clsCotizacionBE.clsAcomodoVehiculoExtDetPaxBE In ListaDetAcomVehiExtDetPax
                Dim NewItem As New clsCotizacionBE.clsAcomodoVehiculoExtDetPaxBE With {.NuGrupo = ST.NuGrupo, _
                                                                                        .IDDet = strIDDet, _
                                                                                       .NuPax = ST.NuPax, _
                                                                                       .Selecc = ST.Selecc, _
                                                                                       .UserMod = gstrUser}
                frmRef.ListaDetAcomVehiExtDetPax.Add(NewItem)
            Next


        Catch ex As Exception
            Throw
        End Try
    End Sub
    Private Sub GrabarListaCopiaAcomodoVehiculo()
        Try
            EliminarServiciosCopiaAcoVeh()

            For i As Int16 = 0 To lvwServAcomVehi.Items.Count - 1
                If lvwServAcomVehi.Items(i).Checked Then
                    Dim NewItem As New clsCotizacionBE.clsAcomodoVehiculoCopiaBE With {.IDDetBase = strIDDet, _
                                                                                      .IDDet = lvwServAcomVehi.Items(i).SubItems(5).Text}
                    frmRef.ListaIDDetCopiarAcomVehi.Add(NewItem)
                End If
            Next
        Catch ex As Exception
            Throw
        End Try
    End Sub

    'Private Sub GrabarListaDetPaxAcomodoVehiculo()
    '    Try
    '        EliminarPaxAcoVehInt()

    '        For i As Int16 = 0 To lvwServAcomVehi.Items.Count - 1
    '            If lvwServAcomVehi.Items(i).Checked Then
    '                Dim NewItem As New clsCotizacionBE.clsAcomodoVehiculoIntDetPaxBE With {.IDDet = strIDDet, _
    '                                                                                       .NuVehiculo = dgvAcoVeh.CurrentRow.Cells("NuVehiculo").Value, _
    '                                                                                  .NuPax = lvwServAcomVehi.Items(i).SubItems(5).Text}
    '                frmRef.ListaDetAcomVehiIntDetPax.Add(NewItem)
    '            End If
    '        Next
    '    Catch ex As Exception
    '        Throw
    '    End Try
    'End Sub


    Private Sub GrabarListaAcomodoVehiculoExt_a_BE()
        Try
InicioBorrar:
            For Each ItemAcVe As clsCotizacionBE.clsAcomodoVehiculo_Ext_BE In frmRef.ListaAcomoVehicExt
                If ItemAcVe.IDDet = strIDDet Then
                    frmRef.ListaAcomoVehicExt.Remove(ItemAcVe)
                    GoTo InicioBorrar
                End If
            Next
            If IsNumeric(txtPaxTotal.Text) And IsNumeric(txtGruposPax.Text) Then
                Dim NewItem As New clsCotizacionBE.clsAcomodoVehiculo_Ext_BE With {
                                                                        .IDDet = strIDDet, _
                                                                        .QtPax = txtPaxTotal.Text, _
                                                                        .QtGrupo = txtGruposPax.Text, _
                                                                        .FlCopiar = chkSelCopiaAcoVeh.Checked, _
                                                                        .UserMod = gstrUser}
                frmRef.ListaAcomoVehicExt.Add(NewItem)
            End If

        Catch ex As Exception
            Throw
        End Try
    End Sub
    Private Sub CopiaraHijos(ByVal vbytFilaDgv As Byte)
        Try
            With frmRef.dgvDet
                Dim strDescServicio As String = .Item("DescServicioDet", vbytFilaDgv).Value.ToString()
                Dim strIDDetRel As String = .Item("IDDetRel", vbytFilaDgv).Value.ToString()
                Dim strIDDet As String = .Item("IDDet", vbytFilaDgv).Value.ToString()

                'If strIDDetRel = "0" Then Exit Sub
                Dim blnEsPadre As Boolean = False
                Dim blnNumerico As Boolean = False
                If IsNumeric(strIDDet) Then
                    blnNumerico = True
                    For Each FilaDgv As DataGridViewRow In .Rows
                        If FilaDgv.Cells("IDDetRel").Value.ToString = strIDDet Then
                            blnEsPadre = True
                            Exit For
                        End If
                    Next
                Else
                    If strIDDetRel <> "" Then
                        If strIDDetRel.Substring(0, 1) = "P" Then
                            blnEsPadre = True
                        End If
                    End If
                End If

                If Not blnEsPadre Then Exit Sub

                If blnNumerico Then
                    For Each FilaDgv As DataGridViewRow In .Rows
                        If FilaDgv.Cells("IDDetRel").Value.ToString = strIDDet Then
                            Dim strServicioOrig As String = strDescServicio
                            strServicioOrig = strServicioOrig.Substring(0, InStr(strServicioOrig, " - ")).Trim
                            Dim strServicioDest As String = FilaDgv.Cells("DescServicioDet").Value

                            Dim strServicioOrig1dia As String = strServicioOrig1erdia.Substring(0, InStr(strServicioOrig1erdia, " - ")).Trim 'strDescServicioDetBup'.Substring(0, InStr(strDescServicioDetBup, " - ")).Trim


                            strServicioDest = Replace(strServicioDest, strServicioOrig1dia, strServicioOrig)

                            FilaDgv.Cells("DescServicioDet").Value = strServicioDest

                        End If
                    Next
                Else
                    strIDDetRel = Mid(strIDDetRel, 1)
                    For Each FilaDgv As DataGridViewRow In .Rows
                        If FilaDgv.Cells("IDDetRel").Value.ToString.Length > 1 And Not IsNumeric(FilaDgv.Cells("IDDetRel").Value.ToString) Then
                            If Mid(FilaDgv.Cells("IDDetRel").Value.ToString, 2) = Mid(strIDDetRel, 2) And FilaDgv.Cells("IDDetRel").Value.ToString.Substring(0, 1) = "H" Then


                                'Dim strServicioOrig As String = strDescServicio
                                'strServicioOrig = strServicioOrig.Substring(0, InStr(strServicioOrig, " - ")).Trim
                                'Dim strServicioDest As String = FilaDgv.Cells("DescServicioDet").Value

                                'Dim strServicioOrig1diaSinDia As String = strServicioOrig1erdia.Substring(0, InStr(strServicioOrig1erdia, " - ")).Trim 'strDescServicioDetBup'.Substring(0, InStr(strDescServicioDetBup, " - ")).Trim


                                'strServicioDest = Replace(strServicioOrig1erdia, strServicioOrig1diaSinDia, strServicioOrig)

                                'FilaDgv.Cells("DescServicioDet").Value = strServicioDest

                                Dim strServicioOrig As String = strDescServicio
                                strServicioOrig = strServicioOrig.Substring(0, InStr(strServicioOrig, " - ")).Trim
                                Dim strServicioDest As String = FilaDgv.Cells("DescServicioDet").Value

                                Dim strServicioOrig1dia As String = strServicioOrig1erdia.Substring(0, InStr(strServicioOrig1erdia, " - ")).Trim 'strDescServicioDetBup'.Substring(0, InStr(strDescServicioDetBup, " - ")).Trim


                                strServicioDest = Replace(strServicioDest, strServicioOrig1dia, strServicioOrig)

                                FilaDgv.Cells("DescServicioDet").Value = strServicioDest
                            End If

                        End If
                    Next
                End If
            End With
        Catch ex As Exception
            Throw
        End Try
    End Sub
    Private Sub GrabarReserva()
        'Dim bytNuevasFila As Byte = 0
        'Try
        '    With frmRefRes.DgvDetalle
        '        Dim objBL As New clsServicioProveedorBN.clsServiciosporDiaBN
        '        'Dim dttDias As DataTable = objBL.ConsultarListxIDServicioIdioma(strIDServicio, strIDIdioma)
        '        'Dim dttDias As DataTable = objBL.ConsultarListxIDServicio(strIDServicio)
        '        Dim drDias As SqlClient.SqlDataReader = objBL.ConsultarxIDServicioIdiomaparaCotizar(strIDServicio)

        '        If strIDProveedor <> strIDProveedorRes Then
        '            If strIDTipoProv = gstrTipoProveeHoteles Then
        '                strIDReserva = "M" & (frmRefRes.DgvHotelesyEspeciales.Rows.Count + 1).ToString
        '            Else
        '                strIDReserva = "M" & (frmRefRes.DgvNoHoteles.Rows.Count + 1).ToString
        '            End If
        '        End If

        '        'If dttDias.Rows.Count = 0 Then
        '        If Not drDias.HasRows Then
        '            Dim bytFilaDgv As Byte = .RowCount
        '            'If Not blnDgvFilaVacia Then
        '            '.Rows.Add()
        '            'Else
        '            'If bytFilaDgv > 0 Then bytFilaDgv -= 1
        '            'End If
        '            'If .RowCount = 0 Then .Rows.Add()
        '            .Rows.Add()
        '            .Item("IDReserva", bytFilaDgv).Value = strIDReserva
        '            .Item("IDDet", bytFilaDgv).Value = intIDDet

        '            .Item("Dia", bytFilaDgv).Value = Format(dtpDia.Value, "dd/MM/yyyy HH:mm:ss")

        '            'If cboTipoProv.SelectedValue.ToString = gstrTipoProveeHoteles Then
        '            '.Item("DiaFormat", bytFilaDgv).Value = dtpDia.Value.ToString("ddd dd/MM/yyyy").ToUpper
        '            'Else
        '            .Item("DiaFormat", bytFilaDgv).Value = dtpDia.Value.ToString("ddd dd/MM/yyyy").ToUpper
        '            'End If
        '            frmRefRes.dtpFechaOut.Value = dtpDia.Value

        '            If cboTipoProv.SelectedValue.ToString = gstrTipoProveeHoteles And .Item("PlanAlimenticio", bytFilaDgv).Value = False Then
        '                .Item("Hora", bytFilaDgv).Value = ""
        '            Else
        '                .Item("Hora", bytFilaDgv).Value = dtpDiaHora.Value.ToString("HH:mm")
        '            End If


        '            .Item("IDPais", bytFilaDgv).Value = cboPais.SelectedValue.ToString
        '            .Item("IDubigeo", bytFilaDgv).Value = cboCiudad.SelectedValue.ToString
        '            .Item("IDTipoProv", bytFilaDgv).Value = cboTipoProv.SelectedValue.ToString
        '            .Item("IDProveedor", bytFilaDgv).Value = strIDProveedor
        '            .Item("Proveedor", bytFilaDgv).Value = lblProveedor.Text
        '            .Item("IDServicio", bytFilaDgv).Value = strIDServicio
        '            .Item("IDServicio_Det", bytFilaDgv).Value = intIDServicio_Det

        '            .Item("Ciudad", bytFilaDgv).Value = cboCiudad.Text


        '            If cboTipoProv.SelectedValue.ToString = gstrTipoProveeHoteles Or cboTipoProv.SelectedValue.ToString = gstrTipoProveeRestaurantes Then
        '                .Item("DescServicioDet", bytFilaDgv).Value = strDescServicio_Det
        '            Else
        '                .Item("DescServicioDet", bytFilaDgv).Value = txtServicio.Text

        '            End If

        '            .Item("DescServicioDetBup", bytFilaDgv).Value = txtServicio.Text


        '            '.Item("DescServicioDet", bytFilaDgv).Value = dr("DescripCorta")


        '            .Item("Tipo", bytFilaDgv).Value = strTipoServicio_Det
        '            '.Item("ConAlojamiento", bytFilaDgv).Value = blnConAlojamiento

        '            If strDescServicioaCambiar = "" Then
        '                .Item("NroPax", bytFilaDgv).Value = txtNroPax.Text
        '            Else
        '                .Item("NroPax", bytFilaDgv).Value = lvwPax.CheckedItems.Count
        '            End If


        '            .Item("NroLiberados", bytFilaDgv).Value = txtNroLiberados.Text
        '            .Item("Tipo_Lib", bytFilaDgv).Value = If(cboTipoLib.Text = "", "", cboTipoLib.SelectedValue.ToString)


        '            .Item("CostoLiberado", bytFilaDgv).Value = lblCostoLiberado.Text
        '            .Item("CostoReal", bytFilaDgv).Value = txtCostoReal.Text
        '            .Item("CostoRealAnt", bytFilaDgv).Value = dblCostoRealOrig
        '            .Item("Margen", bytFilaDgv).Value = lblMargen.Text
        '            .Item("MargenAplicado", bytFilaDgv).Value = txtMargenAplicado.Text
        '            .Item("MargenAplicadoAnt", bytFilaDgv).Value = dblMargenAplicadoOrig
        '            .Item("MargenLiberado", bytFilaDgv).Value = lblMargenLiberado.Text

        '            .Item("Total", bytFilaDgv).Value = lblTotal.Text
        '            .Item("TotalOrig", bytFilaDgv).Value = lblTotalOrig.Text

        '            .Item("CostoLiberadoImpto", bytFilaDgv).Value = lblCostoLiberadoImpto.Text
        '            .Item("CostoRealImpto", bytFilaDgv).Value = txtCostoRealImpto.Text
        '            .Item("MargenImpto", bytFilaDgv).Value = lblMargenImpto.Text
        '            .Item("MargenLiberadoImpto", bytFilaDgv).Value = lblMargenLiberadoImpto.Text

        '            .Item("TotImpto", bytFilaDgv).Value = lblTotImpto.Text

        '            .Item("Especial", bytFilaDgv).Value = chkEspecial.Checked
        '            .Item("MotivoEspecial", bytFilaDgv).Value = txtMotivoEspecial.Text
        '            '.Item("RutaDocSustento", bytFilaDgv).Value = txtRutaDocSustento.Text
        '            .Item("RutaDocSustento", bytFilaDgv).Value = lnkDocumentoSustento.Text

        '            .Item("Desayuno", bytFilaDgv).Value = chkDesayuno.Checked
        '            .Item("Lonche", bytFilaDgv).Value = chkLonche.Checked
        '            .Item("Almuerzo", bytFilaDgv).Value = chkAlmuerzo.Checked
        '            .Item("Cena", bytFilaDgv).Value = chkCena.Checked


        '            .Item("Transfer", bytFilaDgv).Value = chkTransfer.Checked
        '            If cboOrigen.Text <> "" Then
        '                '.Item("OrigenTransfer", bytFilaDgv).Value = cboOrigen.SelectedValue.ToString.Trim
        '                .Item("IDDetTransferOri", bytFilaDgv).Value = cboOrigen.SelectedValue.ToString.Trim
        '            Else
        '                '.Item("OrigenTransfer", bytFilaDgv).Value = ""
        '                .Item("IDDetTransferOri", bytFilaDgv).Value = "0"
        '            End If
        '            If cboDestino.Text <> "" Then
        '                '.Item("DestinoTransfer", bytFilaDgv).Value = cboDestino.SelectedValue.ToString.Trim
        '                .Item("IDDetTransferDes", bytFilaDgv).Value = cboDestino.SelectedValue.ToString.Trim
        '            Else
        '                '.Item("DestinoTransfer", bytFilaDgv).Value = ""
        '                .Item("IDDetTransferDes", bytFilaDgv).Value = "0"
        '            End If

        '            .Item("TipoTransporte", bytFilaDgv).Value = If(cboTipoTransporte.Text = "", "", cboTipoTransporte.SelectedValue.ToString)

        '            If cboIDUbigeoOri.Text <> "" And cboIDUbigeoOri.Text <> "-------" Then
        '                .Item("IDUbigeoOri", bytFilaDgv).Value = cboIDUbigeoOri.SelectedValue.ToString
        '                .Item("DescUbigeoOri", bytFilaDgv).Value = cboIDUbigeoOri.Text
        '            Else
        '                .Item("IDUbigeoOri", bytFilaDgv).Value = "000001"
        '                .Item("DescUbigeoOri", bytFilaDgv).Value = ""
        '            End If
        '            If cboIDUbigeoDes.Text <> "" And cboIDUbigeoDes.Text <> "-------" Then
        '                .Item("IDUbigeoDes", bytFilaDgv).Value = cboIDUbigeoDes.SelectedValue.ToString
        '                .Item("DescUbigeoDes", bytFilaDgv).Value = cboIDUbigeoDes.Text
        '            Else
        '                .Item("IDUbigeoDes", bytFilaDgv).Value = "000001"
        '                .Item("DescUbigeoDes", bytFilaDgv).Value = ""
        '            End If

        '            .Item("IDIdioma", bytFilaDgv).Value = If(cboIDIdioma.Text = "", "NO APLICA", cboIDIdioma.SelectedValue.ToString)

        '            .Item("Anio", bytFilaDgv).Value = strAnio
        '            '.Item("DescTipoServ", bytFilaDgv).Value = strDescTipoServ

        '            .Item("IDDetRel", bytFilaDgv).Value = ""
        '            .Item("IDReserva_Det_Rel", bytFilaDgv).Value = ""

        '            If dtpFechaOut.Value <> "01/01/1900" Then
        '                .Item("FechaOut", bytFilaDgv).Value = Format(dtpFechaOut.Value, "dd/MM/yyyy")
        '                If cboTipoProv.SelectedValue.ToString = gstrTipoProveeHoteles And .Item("PlanAlimenticio", bytFilaDgv).Value = False Then
        '                    .Item("FechaOutFormat", bytFilaDgv).Value = dtpFechaOut.Value.ToString("ddd dd/MM/yyyy").ToUpper
        '                Else
        '                    .Item("FechaOutFormat", bytFilaDgv).Value = dtpFechaOut.Value.ToString("ddd dd/MM/yyyy HH:mm").ToUpper
        '                End If
        '            End If
        '            .Item("Cantidad", bytFilaDgv).Value = txtCantidad.Text
        '            .Item("Noches", bytFilaDgv).Value = lblNoches.Text
        '            .Item("IDEmailNew", bytFilaDgv).Value = strIDEMail

        '            GrabarDetReservPax(bytFilaDgv)

        '        Else
        '            Dim bln1erDia As Boolean = True
        '            Dim datFecha As Date
        '            Dim bytContHijos As Byte = 0
        '            'For Each dr As DataRow In dttDias.Rows
        '            While drDias.Read

        '                Dim bytFilaDgv As Byte = .RowCount
        '                'If Not blnDgvFilaVacia Then
        '                '    .Rows.Add()
        '                'Else
        '                '    If bytFilaDgv > 0 Then bytFilaDgv -= 1
        '                'End If
        '                .Rows.Add()

        '                If bln1erDia Then
        '                    datFecha = dtpDia.Value
        '                Else
        '                    datFecha = DateAdd(DateInterval.Day, 1, datFecha)
        '                End If
        '                .Item("IDReserva", bytFilaDgv).Value = strIDReserva
        '                .Item("IDDet", bytFilaDgv).Value = intIDDet
        '                .Item("Dia", bytFilaDgv).Value = Format(datFecha, "dd/MM/yyyy HH:mm:ss")
        '                .Item("DiaFormat", bytFilaDgv).Value = datFecha.ToString("ddd dd/MM/yyyy HH:mm").ToString.ToUpper
        '                frmRef.dtpFechaOut.Value = datFecha
        '                .Item("IDPais", bytFilaDgv).Value = cboPais.SelectedValue.ToString
        '                .Item("IDCiudad", bytFilaDgv).Value = cboCiudad.SelectedValue.ToString
        '                .Item("IDTipoProv", bytFilaDgv).Value = cboTipoProv.SelectedValue.ToString
        '                .Item("IDProveedor", bytFilaDgv).Value = strIDProveedor
        '                .Item("Proveedor", bytFilaDgv).Value = lblProveedor.Text
        '                .Item("IDServicio", bytFilaDgv).Value = strIDServicio
        '                .Item("IDServicio_Det", bytFilaDgv).Value = intIDServicio_Det

        '                .Item("Ciudad", bytFilaDgv).Value = cboCiudad.Text
        '                .Item("DescServicioDet", bytFilaDgv).Value = strDescServicio_Det
        '                '.Item("DescServicioDet", bytFilaDgv).Value = drDias("DescripCorta")
        '                '.Item("DescServicioDet", bytFilaDgv).Value = dr("Descripcion")
        '                .Item("Tipo", bytFilaDgv).Value = strTipoServicio_Det


        '                .Item("NroLiberados", bytFilaDgv).Value = txtNroLiberados.Text
        '                .Item("Tipo_Lib", bytFilaDgv).Value = If(cboTipoLib.Text = "", "", cboTipoLib.SelectedValue.ToString)
        '                .Item("NroPax", bytFilaDgv).Value = txtNroPax.Text

        '                .Item("Desayuno", bytFilaDgv).Value = If(IsDBNull(drDias("Desayuno")) = True, 0, drDias("Desayuno"))
        '                .Item("Lonche", bytFilaDgv).Value = If(IsDBNull(drDias("Lonche")) = True, 0, drDias("Lonche"))
        '                .Item("Almuerzo", bytFilaDgv).Value = If(IsDBNull(drDias("Almuerzo")) = True, 0, drDias("Almuerzo"))
        '                .Item("Cena", bytFilaDgv).Value = If(IsDBNull(drDias("Cena")) = True, 0, drDias("Cena"))


        '                .Item("Transfer", bytFilaDgv).Value = chkTransfer.Checked
        '                If cboOrigen.Text <> "" Then
        '                    '.Item("OrigenTransfer", bytFilaDgv).Value = cboOrigen.SelectedValue.ToString.Trim
        '                    .Item("IDDetTransferOri", bytFilaDgv).Value = cboOrigen.SelectedValue.ToString.Trim
        '                Else
        '                    '.Item("OrigenTransfer", bytFilaDgv).Value = ""
        '                    .Item("IDDetTransferOri", bytFilaDgv).Value = "0"
        '                End If
        '                If cboDestino.Text <> "" Then
        '                    '.Item("DestinoTransfer", bytFilaDgv).Value = cboDestino.SelectedValue.ToString.Trim
        '                    .Item("IDDetTransferDes", bytFilaDgv).Value = cboDestino.SelectedValue.ToString.Trim
        '                Else
        '                    '.Item("DestinoTransfer", bytFilaDgv).Value = ""
        '                    .Item("IDDetTransferDes", bytFilaDgv).Value = "0"
        '                End If

        '                .Item("TipoTransporte", bytFilaDgv).Value = If(cboTipoTransporte.Text = "", "", cboTipoTransporte.SelectedValue.ToString)

        '                .Item("IDUbigeoOri", bytFilaDgv).Value = If(IsDBNull(drDias("IDUbigeoOri")) = True, "000001", drDias("IDUbigeoOri"))
        '                .Item("DescUbigeoOri", bytFilaDgv).Value = If(IsDBNull(drDias("DescUbigeoOri")) = True, "", drDias("DescUbigeoOri"))
        '                .Item("IDUbigeoDes", bytFilaDgv).Value = If(IsDBNull(drDias("IDUbigeoDes")) = True, "000001", drDias("IDUbigeoDes"))
        '                .Item("DescUbigeoDes", bytFilaDgv).Value = If(IsDBNull(drDias("DescUbigeoDes")) = True, "", drDias("DescUbigeoDes"))

        '                If bln1erDia Then

        '                    .Item("CostoLiberado", bytFilaDgv).Value = lblCostoLiberado.Text
        '                    .Item("CostoReal", bytFilaDgv).Value = txtCostoReal.Text
        '                    .Item("CostoRealAnt", bytFilaDgv).Value = dblCostoRealOrig
        '                    .Item("Margen", bytFilaDgv).Value = lblMargen.Text
        '                    .Item("MargenAplicado", bytFilaDgv).Value = txtMargenAplicado.Text
        '                    .Item("MargenAplicadoAnt", bytFilaDgv).Value = dblMargenAplicadoOrig
        '                    .Item("MargenLiberado", bytFilaDgv).Value = lblMargenLiberado.Text
        '                    .Item("Total", bytFilaDgv).Value = lblTotal.Text
        '                    .Item("TotalOrig", bytFilaDgv).Value = lblTotalOrig.Text
        '                    .Item("CostoLiberadoImpto", bytFilaDgv).Value = lblCostoLiberadoImpto.Text
        '                    .Item("CostoRealImpto", bytFilaDgv).Value = txtCostoRealImpto.Text
        '                    .Item("MargenImpto", bytFilaDgv).Value = lblMargenImpto.Text
        '                    .Item("MargenLiberadoImpto", bytFilaDgv).Value = lblMargenLiberadoImpto.Text
        '                    .Item("TotImpto", bytFilaDgv).Value = lblTotImpto.Text
        '                    .Item("Especial", bytFilaDgv).Value = chkEspecial.Checked
        '                    .Item("MotivoEspecial", bytFilaDgv).Value = txtMotivoEspecial.Text
        '                    '.Item("RutaDocSustento", bytFilaDgv).Value = txtRutaDocSustento.Text
        '                    .Item("RutaDocSustento", bytFilaDgv).Value = lnkDocumentoSustento.Text
        '                    .Item("IDDetRel", bytFilaDgv).Value = "P" & bytFilaDgv.ToString

        '                Else

        '                    .Item("NroPax", bytFilaDgv).Value = txtNroPax.Text
        '                    .Item("CostoLiberado", bytFilaDgv).Value = "0.00"
        '                    .Item("CostoReal", bytFilaDgv).Value = "0.00"
        '                    .Item("CostoRealAnt", bytFilaDgv).Value = "0.00"
        '                    .Item("Margen", bytFilaDgv).Value = "0.00"
        '                    .Item("MargenAplicado", bytFilaDgv).Value = "0.00"
        '                    .Item("MargenAplicadoAnt", bytFilaDgv).Value = "0.00"
        '                    .Item("MargenLiberado", bytFilaDgv).Value = "0.00"
        '                    .Item("Total", bytFilaDgv).Value = "0.00"
        '                    .Item("TotalOrig", bytFilaDgv).Value = "0.00"
        '                    .Item("CostoLiberadoImpto", bytFilaDgv).Value = "0.00"
        '                    .Item("CostoRealImpto", bytFilaDgv).Value = "0.00"
        '                    .Item("MargenImpto", bytFilaDgv).Value = "0.00"
        '                    .Item("MargenLiberadoImpto", bytFilaDgv).Value = "0.00"
        '                    .Item("TotImpto", bytFilaDgv).Value = "0.00"
        '                    .Item("Especial", bytFilaDgv).Value = True
        '                    .Item("MotivoEspecial", bytFilaDgv).Value = "Día del Servicio"
        '                    .Item("RutaDocSustento", bytFilaDgv).Value = ""

        '                    '.Item("IDDet", bytFilaDgv).Value = "P" & (bytFilaDgv - 1).ToString
        '                    .Item("IDDetRel", bytFilaDgv).Value = "H" & ((bytFilaDgv - bytContHijos) - 1).ToString
        '                    bytContHijos += 1
        '                End If
        '                .Item("IDIdioma", bytFilaDgv).Value = strIDIdioma
        '                '.Item("Servicio", bytFilaDgv).Value = drDias("DescripCorta")
        '                '.Item("Servicio", bytFilaDgv).Value = dr("Descripcion")

        '                .Item("Servicio", bytFilaDgv).Value = txtServicio.Text


        '                .Item("Anio", bytFilaDgv).Value = strAnio
        '                '.Item("DescTipoServ", bytFilaDgv).Value = strDescTipoServ


        '                .Item("FechaOut", bytFilaDgv).Value = Format(dtpFechaOut.Value, "dd/MM/yyyy")
        '                If cboTipoProv.SelectedValue.ToString = gstrTipoProveeHoteles And .Item("PlanAlimenticio", bytFilaDgv).Value = False Then
        '                    .Item("FechaOutFormat", bytFilaDgv).Value = dtpFechaOut.Value.ToString("ddd dd/MM/yyyy").ToUpper
        '                Else
        '                    .Item("FechaOutFormat", bytFilaDgv).Value = dtpFechaOut.Value.ToString("ddd dd/MM/yyyy HH:mm").ToUpper
        '                End If
        '                .Item("Cantidad", bytFilaDgv).Value = txtCantidad.Text
        '                .Item("Noches", bytFilaDgv).Value = lblNoches.Text
        '                .Item("IDEmailNew", bytFilaDgv).Value = strIDEMail

        '                bln1erDia = False

        '                'GrabarDetReservPax(bytFilaDgv)
        '                'Next
        '            End While
        '        End If



        '    End With
        '    frmRefRes.dttDetCotiServ = dttDetCotiServ

        '    'GrabarQuiebres()
        '    'GrabarDetCotiPax()

        '    'For i As Byte = 0 To lvwPax.Items.Count - 1
        '    '    AgregarEditarValorStructDetReservPax(i)
        '    'Next

        '    For Each FilaDgv As DataGridViewRow In dgvIdiom.Rows
        '        For bytFilaCol As Byte = 0 To dgvIdiom.Columns.Count - 1

        '            AgregarEditarValorStrucTextoIdioma(FilaDgv.Index, bytFilaCol, "")

        '        Next
        '    Next

        '    'frmRef.blnEliminoCab = False
        'Catch ex As Exception
        '    Throw
        'End Try
    End Sub
    Private Sub ActualizarReserva()
        'Try
        '    With frmRefRes.DgvDetalle
        '        Dim bytFilaDgv As Byte = .CurrentRow.Index

        '        .Item("Dia", bytFilaDgv).Value = dtpDia.Value.ToShortDateString & " " & Format(dtpDiaHora.Value, "HH:mm:ss")
        '        '.Item("DiaFormat", bytFilaDgv).Value = Format(dtpDia.Value, "ddd dd/MM/yyyy HH:mm")
        '        .Item("IDDet", bytFilaDgv).Value = intIDDet

        '        'If cboTipoProv.SelectedValue.ToString = gstrTipoProveeHoteles Then
        '        .Item("DiaFormat", bytFilaDgv).Value = dtpDia.Value.ToString("ddd dd/MM/yyyy").ToUpper
        '        'Else
        '        '.Item("DiaFormat", bytFilaDgv).Value = dtpDia.Value.ToString("ddd dd/MM/yyyy").ToUpper
        '        'End If

        '        If cboTipoProv.SelectedValue.ToString = gstrTipoProveeHoteles And .Item("PlanAlimenticio", bytFilaDgv).Value = False Then
        '            .Item("Hora", bytFilaDgv).Value = ""
        '        Else
        '            .Item("Hora", bytFilaDgv).Value = dtpDiaHora.Value.ToString("HH:mm")
        '        End If

        '        .Item("IDPais", bytFilaDgv).Value = cboPais.SelectedValue.ToString
        '        .Item("IDUbigeo", bytFilaDgv).Value = cboCiudad.SelectedValue.ToString
        '        .Item("IDTipoProv", bytFilaDgv).Value = cboTipoProv.SelectedValue.ToString
        '        .Item("IDProveedor", bytFilaDgv).Value = strIDProveedor
        '        .Item("Proveedor", bytFilaDgv).Value = lblProveedor.Text
        '        .Item("IDServicio", bytFilaDgv).Value = strIDServicio
        '        .Item("IDServicio_Det", bytFilaDgv).Value = intIDServicio_Det

        '        .Item("Ciudad", bytFilaDgv).Value = cboCiudad.Text

        '        'If cboTipoProv.SelectedValue.ToString = gstrTipoProveeHoteles Or cboTipoProv.SelectedValue.ToString = gstrTipoProveeRestaurantes Then
        '        '    .Item("DescServicioDet", bytFilaDgv).Value = strDescServicio_Det
        '        'Else
        '        '    .Item("DescServicioDet", bytFilaDgv).Value = txtServicio.Text

        '        'End If
        '        .Item("DescServicioDet", bytFilaDgv).Value = txtServicio.Text

        '        .Item("Tipo", bytFilaDgv).Value = strTipoServicio_Det

        '        If intIDDet = 0 Then
        '            If strIDReservas_Det_Rel = "" Then
        '                .Item("NroPax", bytFilaDgv).Value = txtNroPax.Text
        '            Else
        '                .Item("NroPax", bytFilaDgv).Value = lvwPax.CheckedItems.Count
        '            End If
        '        Else
        '            .Item("NroPax", bytFilaDgv).Value = txtNroPax.Text
        '        End If
        '        .Item("NroLiberados", bytFilaDgv).Value = txtNroLiberados.Text
        '        .Item("Tipo_Lib", bytFilaDgv).Value = If(cboTipoLib.Text = "", "", cboTipoLib.SelectedValue.ToString)
        '        .Item("PaxMasLiberados", bytFilaDgv).Value = txtNroPax.Text & " " & If(txtNroLiberados.Text.Trim = "" Or txtNroLiberados.Text.Trim = "0", "", "+" & txtNroLiberados.Text.Trim)

        '        .Item("CostoLiberado", bytFilaDgv).Value = lblCostoLiberado.Text
        '        .Item("CostoReal", bytFilaDgv).Value = txtCostoReal.Text
        '        .Item("CostoRealAnt", bytFilaDgv).Value = dblCostoRealOrig
        '        .Item("Margen", bytFilaDgv).Value = lblMargen.Text
        '        .Item("MargenAplicado", bytFilaDgv).Value = txtMargenAplicado.Text
        '        .Item("MargenAplicadoAnt", bytFilaDgv).Value = dblMargenAplicadoOrig
        '        .Item("MargenLiberado", bytFilaDgv).Value = lblMargenLiberado.Text

        '        .Item("Total", bytFilaDgv).Value = lblTotal.Text
        '        .Item("TotalOrig", bytFilaDgv).Value = lblTotalOrig.Text

        '        .Item("CostoLiberadoImpto", bytFilaDgv).Value = lblCostoLiberadoImpto.Text
        '        .Item("CostoRealImpto", bytFilaDgv).Value = txtCostoRealImpto.Text
        '        .Item("MargenImpto", bytFilaDgv).Value = lblMargenImpto.Text
        '        .Item("MargenLiberadoImpto", bytFilaDgv).Value = lblMargenLiberadoImpto.Text

        '        '.Item("TotImpto", bytFilaDgv).Value = Convert.ToDouble(lblCostoLiberadoImpto.Text) + _
        '        '    Convert.ToDouble(txtCostoRealImpto.Text) + Convert.ToDouble(lblMargenImpto.Text) + _
        '        '    Convert.ToDouble(lblMargenLiberadoImpto.Text) + Convert.ToDouble(lblSSCLImpto.Text) + _
        '        '    Convert.ToDouble(lblSSCRImpto.Text) + Convert.ToDouble(lblSTMargenImpto.Text) + _
        '        '    Convert.ToDouble(lblSSMargenLiberadoImpto.Text) + Convert.ToDouble(lblSTCRImpto.Text) + Convert.ToDouble(lblSTMargenImpto.Text)

        '        .Item("TotImpto", bytFilaDgv).Value = lblTotImpto.Text

        '        .Item("Especial", bytFilaDgv).Value = chkEspecial.Checked
        '        .Item("MotivoEspecial", bytFilaDgv).Value = txtMotivoEspecial.Text
        '        '.Item("RutaDocSustento", bytFilaDgv).Value = txtRutaDocSustento.Text
        '        .Item("RutaDocSustento", bytFilaDgv).Value = lnkDocumentoSustento.Text

        '        .Item("Desayuno", bytFilaDgv).Value = chkDesayuno.Checked
        '        .Item("Lonche", bytFilaDgv).Value = chkLonche.Checked
        '        .Item("Almuerzo", bytFilaDgv).Value = chkAlmuerzo.Checked
        '        .Item("Cena", bytFilaDgv).Value = chkCena.Checked
        '        .Item("Transfer", bytFilaDgv).Value = chkTransfer.Checked

        '        If cboOrigen.Text <> "" Then
        '            '.Item("OrigenTransfer", bytFilaDgv).Value = cboOrigen.SelectedValue.ToString.Trim
        '            .Item("IDDetTransferOri", bytFilaDgv).Value = cboOrigen.SelectedValue.ToString.Trim
        '        Else
        '            '.Item("OrigenTransfer", bytFilaDgv).Value = ""
        '            .Item("IDDetTransferOri", bytFilaDgv).Value = "0"
        '        End If
        '        If cboDestino.Text <> "" Then
        '            '.Item("DestinoTransfer", bytFilaDgv).Value = cboDestino.SelectedValue.ToString.Trim
        '            .Item("IDDetTransferDes", bytFilaDgv).Value = cboDestino.SelectedValue.ToString.Trim
        '        Else
        '            '.Item("DestinoTransfer", bytFilaDgv).Value = ""
        '            .Item("IDDetTransferDes", bytFilaDgv).Value = "0"
        '        End If


        '        .Item("TipoTransporte", bytFilaDgv).Value = If(cboTipoTransporte.Text = "", "", cboTipoTransporte.SelectedValue.ToString)


        '        .Item("IDUbigeoOri", bytFilaDgv).Value = If(cboIDUbigeoOri.Text = "" Or cboIDUbigeoOri.Text = "-------", "000001", cboIDUbigeoOri.SelectedValue.ToString)
        '        .Item("DescUbigeoOri", bytFilaDgv).Value = cboIDUbigeoOri.Text
        '        .Item("IDUbigeoDes", bytFilaDgv).Value = If(cboIDUbigeoDes.Text = "" Or cboIDUbigeoDes.Text = "-------", "000001", cboIDUbigeoDes.SelectedValue.ToString)
        '        .Item("DescUbigeoDes", bytFilaDgv).Value = cboIDUbigeoDes.Text

        '        .Item("IDIdiomaDet", bytFilaDgv).Value = If(cboIDIdioma.Text = "", "NO APLICA", cboIDIdioma.SelectedValue.ToString).Trim
        '        .Item("IDIdioma", bytFilaDgv).Value = If(cboIDIdioma.SelectedValue.ToString = "NO APLICA", "---", cboIDIdioma.SelectedValue.ToString.Substring(0, 3)) 'If(cboIDIdioma.Text = "", "NO APLICA", cboIDIdioma.SelectedValue.ToString)
        '        '.Item("Servicio", bytFilaDgv).Value = txtServicio.Text

        '        If dtpFechaOut.Value <> "01/01/1900" Then
        '            .Item("FechaOut", bytFilaDgv).Value = Format(dtpFechaOut.Value, "dd/MM/yyyy")
        '            'If cboTipoProv.SelectedValue.ToString = gstrTipoProveeHoteles Then
        '            .Item("FechaOutFormat", bytFilaDgv).Value = dtpFechaOut.Value.ToString("ddd dd/MM/yyyy").ToUpper
        '            'Else
        '            '.Item("FechaOutFormat", bytFilaDgv).Value = dtpFechaOut.Value.ToString("ddd dd/MM/yyyy").ToUpper
        '            'End If
        '        End If
        '        .Item("Cantidad", bytFilaDgv).Value = txtCantidad.Text
        '        .Item("Noches", bytFilaDgv).Value = lblNoches.Text
        '        .Item("IDEmailEdit", bytFilaDgv).Value = strIDEMail

        '        '.Item("FechaRecordatorio", bytFilaDgv).Value = dtpFechaRecordatorio.Text
        '        '.Item("Recordatorio", bytFilaDgv).Value = txtRecordatorio.Text.Trim
        '        .Item("ObservVoucher", bytFilaDgv).Value = txtObservVoucher.Text.Trim
        '        .Item("ObservBiblia", bytFilaDgv).Value = txtObservBiblia.Text.Trim
        '        .Item("ObservInterno", bytFilaDgv).Value = txtObservInterno.Text.Trim

        '        .Item("VerObserVoucher", bytFilaDgv).Value = chkVerVoucher.Checked
        '        .Item("VerObserBiblia", bytFilaDgv).Value = chkVerBiblia.Checked

        '        '.Item("DescGuia", bytFilaDgv).Value = txtDescrGuia.Text.Trim
        '        .Item("IDGuiaProveedor", bytFilaDgv).Value = If(cboGuia.SelectedValue Is Nothing, "", cboGuia.SelectedValue.ToString)

        '        '.Item("DescBus", bytFilaDgv).Value = txtDescBus.Text.Trim
        '        .Item("NuVehiculo", bytFilaDgv).Value = CboBusVehiculo.SelectedValue
        '        .Item("FlServicioNoShow", bytFilaDgv).Value = chkServicioNoShow.Checked

        '        .Item("CantidadAPagar", bytFilaDgv).Value = txtCantidadaPagar.Text.Trim
        '        .Item("FlServicioNoCobrado", bytFilaDgv).Value = chkServNoCobrado.Checked

        '        .Item("SsTotalGenAntNoCobrado", bytFilaDgv).Value = 0
        '        If chkServNoCobrado.Checked Then
        '            .Item("SsTotalGenAntNoCobrado", bytFilaDgv).Value = dblTotalGenAntNoCobrado
        '            .Item("QtCantidadaPagarAntNoCobrado", bytFilaDgv).Value = bytCantidadaPagarAntNoCobrado
        '        End If


        '        .Item("NetoHab", bytFilaDgv).Value = If(txtNetoHab.Text.Trim = "", "0.00", txtNetoHab.Text.Trim)
        '        .Item("IgvHab", bytFilaDgv).Value = If(txtIgvHab.Text.Trim = "", "0.00", txtIgvHab.Text.Trim)
        '        .Item("TotalHab", bytFilaDgv).Value = lblTotalHab.Text.Trim
        '        .Item("NetoGen", bytFilaDgv).Value = lblNetoTotal.Text.Trim
        '        .Item("IgvGen", bytFilaDgv).Value = lblIgvTotal.Text.Trim
        '        .Item("TotalGen", bytFilaDgv).Value = lblTotalR.Text.Trim

        '        .Item("CantidadAPagarEditado", bytFilaDgv).Value = blnCantidadAPagarEditado
        '        .Item("NetoHabEditado", bytFilaDgv).Value = blnNetoHabEditado
        '        .Item("IgvHabEditado", bytFilaDgv).Value = blnIgvHabEditado

        '        .Columns("DiaFormat").Width = 100
        '    End With
        '    frmRefRes.dttDetCotiServ = dttDetCotiServ

        '    'GrabarQuiebres()
        '    If strModulo = "R" Then
        '        GrabarDetReservPax(0)
        '    End If

        '    dgvIdiom.CommitEdit(DataGridViewDataErrorContexts.LeaveControl)
        '    For Each FilaDgv As DataGridViewRow In dgvIdiom.Rows
        '        For bytFilaCol As Byte = 0 To dgvIdiom.Columns.Count - 1
        '            If dgvIdiom.Columns(bytFilaCol).Visible Then
        '                AgregarEditarValorStrucTextoIdioma(FilaDgv.Index, bytFilaCol, "")
        '            Else
        '                AgregarEditarValorStrucTextoIdioma(FilaDgv.Index, bytFilaCol, "B")
        '            End If
        '        Next
        '    Next

        '    If strModulo = "O" Then
        '        If lvwPax.Items.Count > 0 Then
        '            For Indx As Byte = 0 To lvwPax.Items.Count - 1
        '                GrabarDetOperacPax(Indx)
        '            Next
        '        End If
        '    End If

        'Catch ex As Exception
        '    Throw
        'End Try
    End Sub

    Private Sub AgregarEditarValorStrucDetCotiPax(ByVal vBytNroItem As Byte, Optional ByVal vstrIDDet As String = "")
        Try
            Dim strIDPax As String = lvwPax.Items(vBytNroItem).SubItems(3).Text

            Dim RegValor As stDetCotiPax
            Dim intInd As Int16 = 0


            If vstrIDDet <> "" Then
                For Each ST As stDetCotiPax In objLstDetCotiPax
                    If ST.IDPax = strIDPax And ST.IDDet = vstrIDDet And ST.IDCAB = intIDCab Then
                        objLstDetCotiPax.RemoveAt(intInd)
                        Exit For
                    End If

                    intInd += 1
                Next
            Else
                For Each ST As stDetCotiPax In objLstDetCotiPax
                    If ST.IDPax = strIDPax And ST.IDDet = strIDDet And ST.IDCAB = intIDCab Then
                        objLstDetCotiPax.RemoveAt(intInd)
                        Exit For
                    End If

                    intInd += 1
                Next
            End If

            RegValor = New stDetCotiPax
            With RegValor
                .IDCAB = intIDCab
                .IDDet = If(vstrIDDet <> "", vstrIDDet, strIDDet)
                .IDPax = strIDPax
                .IDTipoProv = cboTipoProv.SelectedValue
                If strModoEdicion = "N" Then
                    .Selecc = True
                    .Accion = "N"
                Else
                    If intIDDet <> 0 Then
                        If IsNumeric(.IDPax) Then
                            If lvwPax.Items(vBytNroItem).Checked = True Then .Accion = "N" Else .Accion = "B"
                        Else
                            If lvwPax.Items(vBytNroItem).Checked = True Then .Accion = "N" Else .Accion = "B"
                        End If
                    Else
                        If lvwPax.Items(vBytNroItem).Checked = True Then .Accion = "N" Else .Accion = "B"
                    End If
                    .Selecc = lvwPax.Items(vBytNroItem).Checked
                End If
            End With
            objLstDetCotiPax.Add(RegValor)

        Catch ex As Exception
            Throw
        End Try
    End Sub

    Private Function bytMayorIddetSTGenerado() As Byte
        Try
            Dim bytMayor As Byte = 0
            For Each ST As stDetCotiPax In objLstDetCotiPax
                If Not IsNumeric(ST.IDDet) And ST.IDCAB = intIDCab Then
                    Dim strItem As String = ST.IDDet
                    Dim bytIddet As Byte = CByte(Mid(strItem, 2))
                    If bytIddet > bytMayor Then bytMayor = bytIddet
                End If
            Next
            Return bytMayor + 1
        Catch ex As Exception
            Throw
        End Try
    End Function

    Private Function bytMayorIddetSTGeneradoCotiDetQ() As Byte
        Try
            Dim bytMayor As Byte = 0
            For Each ST As stDetCotiQuiebres In objLstDetCotiQuiebres
                If Not IsNumeric(ST.IDDet) And ST.IDCAB = intIDCab Then
                    Dim strItem As String = ST.IDDet
                    Dim bytIddet As Byte = CByte(Mid(strItem, 2))
                    If bytIddet > bytMayor Then bytMayor = bytIddet
                End If
            Next
            Return bytMayor + 1
        Catch ex As Exception
            Throw
        End Try
    End Function

    Private Sub GrabarDetCotiDescripcion(ByVal vBytNuevaFila As Byte, ByVal vBytDia As Byte)
        Try
            Dim strIDDet As String = frmRef.dgvDet.Item("IDDet", vBytNuevaFila).Value.ToString
            Dim dtt As DataTable = dtFiltrarDataTable(DtTextoIdiom, "Dia='" & vBytDia & "'")

            For Each dtRw As DataRow In dtt.Rows
                Dim RegValor As stCotizTextoIdioma
                RegValor = New stCotizTextoIdioma
                With RegValor
                    .IDCab = intIDCab
                    .IDDet = strIDDet
                    .IDIdioma = dtRw("IDIdioma")
                    .DescCorta = If(IsDBNull(dtRw("DescripCorta")) = True, "", dtRw("DescripCorta"))
                    .DescLarga = dtRw("Descripcion")
                    .NoHotel = dtRw("NoHotel")
                    .TxWebHotel = dtRw("TxWebHotel")

                    .TxDireccHotel = dtRw("TxDireccHotel")
                    .TxTelfHotel1 = dtRw("TxTelfHotel1")
                    .TxTelfHotel2 = dtRw("TxTelfHotel2")
                    .FeHoraChkInHotel = If(IsDBNull(dtRw("FeHoraChkInHotel")), "01/01/1900", "01/01/1900 " & dtRw("FeHoraChkInHotel"))
                    .FeHoraChkOutHotel = If(IsDBNull(dtRw("FeHoraChkOutHotel")), "01/01/1900", "01/01/1900 " & dtRw("FeHoraChkOutHotel"))
                    .CoTipoDesaHotel = dtRw("CoTipoDesaHotel")
                    .CoCiudadHotel = dtRw("CoCiudadHotel")

                    .strAccion = "N"
                End With
                objLstCotizTextoIdioma.Add(RegValor)
            Next

        Catch ex As Exception
            Throw
        End Try
    End Sub

    Private Sub GrabarDetCotiPax(ByVal vbytNuevaFila As Byte)
        Try
            'Dim strIDDet As String
            Dim strIDDetRel As String = ""

            'Dim strIDDetFormRef As String = frmRef.dgvDet.Item("IDDet", vbytNuevaFila).Value.ToString
            Dim strIDDetRelFormRef As String = ""
            'If IsNumeric(strIDDetFormRef) Then

            'Else
            strIDDetRelFormRef = frmRef.dgvDet.Item("IDDetRel", vbytNuevaFila).Value.ToString
            'End If

            'If frmRef.dgvDet.Item("IDDetRel", vbytNuevaFila).Value.ToString = "" Or frmRef.dgvDet.Item("IDDetRel", vbytNuevaFila).Value.ToString = "0" Then
            If strIDDetRelFormRef = "" Or strIDDetRelFormRef = "0" Then
                'MessageBox.Show(frmRef.dgvDet.Item("IDDet", vbytNuevaFila).Value.ToString)

                If intIDDet <> 0 Then
                    strIDDet = intIDDet
                Else
                    If strModoEdicion = "E" Then
                        strIDDet = frmRef.dgvDet.Item("IDDet", vbytNuevaFila).Value
                    ElseIf strModoEdicion = "N" Then
                        If blnExisteSTCotDetPax("M" & bytMayorIddetSTGenerado().ToString) Then
                            strIDDet = frmRef.dgvDet.Item("IDDet", frmRef.dgvDet.CurrentRow.Index).Value
                        Else
                            strIDDet = "M" & bytMayorIddetSTGenerado().ToString
                        End If
                    End If

                End If
            Else
                If strModoEdicion = "E" Then
                    strIDDet = frmRef.dgvDet.Item("IDDet", vbytNuevaFila).Value.ToString
                    strIDDetRel = frmRef.dgvDet.Item("IDDetRel", vbytNuevaFila).Value.ToString
                ElseIf strModoEdicion = "N" Then
                    If blnExisteSTCotDetPax("M" & bytMayorIddetSTGenerado().ToString) Then
                        strIDDet = frmRef.dgvDet.Item("IDDet", frmRef.dgvDet.CurrentRow.Index).Value
                        strIDDetRel = frmRef.dgvDet.Item("IDDetRel", frmRef.dgvDet.CurrentRow.Index).Value
                    Else
                        strIDDet = "M" & bytMayorIddetSTGenerado().ToString
                        strIDDetRel = frmRef.dgvDet.Item("IDDetRel", vbytNuevaFila).Value 'Mid(frmRef.dgvDet.Item("IDDetRel", vbytNuevaFila).Value.ToString, 2)
                    End If
                End If
            End If


            Dim RegValor As stDetCotiPax
            For i As Byte = 0 To lvwPax.Items.Count - 1

                Dim intInd As Int16 = 0
                For Each ST As stDetCotiPax In objLstDetCotiPax

                    If ST.IDPax = lvwPax.Items(i).SubItems(3).Text And ST.IDDet = strIDDet And ST.IDCAB = intIDCab Then
                        objLstDetCotiPax.RemoveAt(intInd)
                        Exit For
                    End If

                    intInd += 1
                Next


                RegValor = New stDetCotiPax
                With RegValor
                    .IDCAB = intIDCab
                    .IDDet = strIDDet
                    .IDPax = lvwPax.Items(i).SubItems(3).Text
                    .IDTipoProv = cboTipoProv.SelectedValue
                    If chkIncGuia.Checked Then
                        .Accion = "B"
                    Else
                        If strModoEdicion = "N" Then
                            .Selecc = True
                            .Accion = "N"
                        Else
                            If intIDDet <> 0 Then
                                If IsNumeric(.IDPax) Then
                                    '.Accion = "M"
                                    If lvwPax.Items(i).Checked = True Then .Accion = "N" Else .Accion = "B"
                                Else
                                    '.Accion = "N"
                                    If lvwPax.Items(i).Checked = True Then .Accion = "N" Else .Accion = "B"
                                End If

                            Else
                                If lvwPax.Items(i).Checked = True Then .Accion = "N" Else .Accion = "B"
                                '.Accion = "N"
                            End If
                            .Selecc = lvwPax.Items(i).Checked
                            '.Accion = "M"

                        End If
                    End If
                End With


                objLstDetCotiPax.Add(RegValor)
            Next


        Catch ex As Exception
            Throw
        End Try
    End Sub

    Private Sub GrabarDetReservPax(ByVal vbytNuevaFila As Byte)
        'If lvwPax.Items.Count = 0 Then Exit Sub
        'Try
        '    Dim strIDReserva_Det As String

        '    If strDescServicioaCambiar = "" Then
        '        If frmRefRes.DgvDetalle.Item("IDReserva_Det_Rel", (vbytNuevaFila)).Value.ToString = "" Or frmRefRes.DgvDetalle.Item("IDReserva_Det_Rel", (vbytNuevaFila)).Value.ToString = "0" Then
        '            If intIDReserva_Det <> "0" And Not intIDReserva_Det Is Nothing Then
        '                strIDReserva_Det = intIDReserva_Det
        '            Else
        '                If strModoEdicion = "E" Then
        '                    strIDReserva_Det = "M" & frmRefRes.DgvDetalle.CurrentRow.Index.ToString
        '                Else

        '                    strIDReserva_Det = "M" & (vbytNuevaFila + 1).ToString
        '                End If
        '            End If
        '        Else
        '            strIDReserva_Det = Mid(frmRefRes.DgvDetalle.Item("IDReserva_Det_Rel", (vbytNuevaFila + 1)).Value.ToString, 2)
        '        End If
        '    Else
        '        strIDReserva_Det = "M" & (vbytNuevaFila + 1).ToString
        '    End If

        '    Dim RegValor As stDetReservPax
        '    For i As Byte = 0 To lvwPax.Items.Count - 1

        '        Dim intInd As Int16 = 0
        '        For Each ST As stDetReservPax In objLstDetReservPax

        '            If ST.IDPax = lvwPax.Items(i).SubItems(3).Text And ST.IDReserva_Det = strIDReserva_Det And ST.IDCab = intIDCab Then
        '                objLstDetReservPax.RemoveAt(intInd)
        '                Exit For
        '            End If

        '            intInd += 1
        '        Next


        '        RegValor = New stDetReservPax
        '        With RegValor
        '            .IDCab = intIDCab
        '            .IDReserva_Det = strIDReserva_Det
        '            .IDPax = lvwPax.Items(i).SubItems(3).Text
        '            .IDTipoProv = cboTipoProv.SelectedValue
        '            If strModoEdicion = "N" Then
        '                If strDescServicioaCambiar = "" Then
        '                    .Selecc = True
        '                Else
        '                    .Selecc = lvwPax.Items(i).Checked
        '                End If

        '                .Accion = "N"
        '            Else
        '                .Selecc = lvwPax.Items(i).Checked
        '                .Accion = "M"
        '            End If

        '        End With


        '        objLstDetReservPax.Add(RegValor)
        '    Next


        'Catch ex As Exception
        '    Throw
        'End Try
    End Sub

    Private Sub GrabarDetOperacPax(ByVal vbytItem As Byte)
        'If lvwPax.Items.Count = 0 Then Exit Sub
        'Try
        '    Dim strIDOperacion_Det As String = frmRefRes.DgvDetalle.CurrentRow.Cells("IDOperacion_Det").Value.ToString
        '    Dim strIdOperacion As String = frmRefRes.DgvDetalle.CurrentRow.Cells("IDOperacion").Value.ToString
        '    'Dim vbytNuevaFila As Byte = frmRefRes.DgvDetalle.Rows.Count

        '    'If intIDReserva_Det <> "0" And Not intIDReserva_Det Is Nothing Then
        '    '    strIDOperacion_Det = intIDReserva_Det
        '    'Else
        '    '    If strModoEdicion = "E" Then
        '    '        strIDOperacion_Det = "M" & frmRefRes.DgvDetalle.CurrentRow.Index.ToString
        '    '    Else
        '    '        strIDOperacion_Det = "M" & (vbytNuevaFila + 1).ToString
        '    '    End If
        '    'End If


        '    Dim RegValor As stDetOperacPax

        '    'For i As Byte = 0 To lvwPax.Items.Count - 1
        '    'Dim i As Byte = lvwPax.Items("").Index



        '    For Each ST As stDetOperacPax In objLstDetOperacPax
        '        If ST.IDPax = lvwPax.Items(vbytItem).SubItems(3).Text And ST.IDOperacion_Det = strIDOperacion_Det And ST.IDCab = intIDCab Then
        '            objLstDetOperacPax.Remove(ST)
        '            Exit For
        '        End If
        '    Next


        '    RegValor = New stDetOperacPax
        '    With RegValor
        '        .IDCab = intIDCab
        '        .IdOperacion = strIdOperacion
        '        .IDOperacion_Det = strIDOperacion_Det
        '        .IDPax = lvwPax.Items(vbytItem).SubItems(3).Text

        '        .Selecc = lvwPax.Items(vbytItem).Checked
        '        'If strModoEdicion = "N" Then
        '        '    .Accion = "N"
        '        'Else
        '        '    .Accion = "M"
        '        'End If

        '    End With


        '    objLstDetOperacPax.Add(RegValor)
        '    'Next


        'Catch ex As Exception
        '    Throw
        'End Try
    End Sub

    Private Sub PintaChecksLvwDetCotiPaxST()
        Try
            blnActPax = False
            'If objLstDetCotiPax.Count > 0 Then
            '    For i = 0 To lvwPax.Items.Count - 1
            '        lvwPax.Items(i).Checked = False
            '    Next
            'End If
            Dim strIDDet As String
            'If strIDDetRel = "" Then
            'strIDDet = If(intIDDet = 0, "E" & frmRef.dgvDet.CurrentRow.Index.ToString, intIDDet.ToString)
            strIDDet = frmRef.dgvDet.CurrentRow.Cells("IDDet").Value.ToString
            'Else
            '    strIDDet = Mid(strIDDetRel, 2)
            'End If

            If lvwPax.Items.Count > 0 And objLstDetCotiPax.Count > 0 Then
                For Each ST As stDetCotiPax In objLstDetCotiPax
                    For i As Byte = 0 To lvwPax.Items.Count - 1

                        If lvwPax.Items(i).SubItems(3).Text = ST.IDPax And strIDDet = ST.IDDet Then
                            lvwPax.Items(i).Checked = ST.Selecc
                            Exit For
                        End If
                    Next
                Next
            End If
            blnActPax = True


            PintarCambioNombrePaxLvw()
        Catch ex As Exception
            Throw
        End Try
    End Sub
    Private Sub PintarCambioNombrePaxLvw()
        If lvwPax.Items.Count = 0 Then Exit Sub
        Try
            For i As Byte = 0 To lvwPax.Items.Count - 1
                Dim intIDPax As String = lvwPax.Items(i).SubItems(3).Text

                For Each dgvRow As DataGridViewRow In frmRef.dgvPax.Rows
                    If intIDPax = dgvRow.Cells("IDPax").Value.ToString Then
                        lvwPax.Items(i).SubItems(0).Text = dgvRow.Cells("Apellidos").Value.ToString
                        lvwPax.Items(i).SubItems(1).Text = dgvRow.Cells("Nombres").Value.ToString
                        Exit For
                    End If
                Next

            Next
        Catch ex As Exception
            Throw
        End Try
    End Sub

    Private Sub PintarCambioNombrePaxReservaLvw()

        'For i As Byte = 0 To lvwPax.Items.Count - 1
        '    Dim intIDPax As Int32 = lvwPax.Items(i).SubItems(3).Text

        '    For Each dgvRow As DataGridViewRow In frmRefRes.dgvPax.Rows
        '        If intIDPax = dgvRow.Cells("IDPax").Value.ToString Then
        '            lvwPax.Items(i).SubItems(0).Text = dgvRow.Cells("Apellidos").Value.ToString
        '            lvwPax.Items(i).SubItems(1).Text = dgvRow.Cells("Nombres").Value.ToString
        '            Exit For
        '        End If
        '    Next

        'Next

    End Sub
    Private Sub PintaChecksLvwDetReservPaxST()
        'If lvwPax.Items.Count = 0 Then Exit Sub
        'Try
        '    blnActPax = False

        '    Dim strIDReserva_Det As String
        '    If strIDReservas_Det_Rel = "" Then
        '        strIDReserva_Det = If(intIDReserva_Det = "0" Or intIDReserva_Det Is Nothing, "M" & frmRefRes.DgvDetalle.CurrentRow.Index.ToString, intIDReserva_Det.ToString)
        '    Else
        '        strIDReserva_Det = Mid(strIDReservas_Det_Rel, 2)
        '    End If

        '    For Each ST As stDetReservPax In objLstDetReservPax

        '        For i As Byte = 0 To lvwPax.Items.Count - 1

        '            If lvwPax.Items(i).SubItems(3).Text = ST.IDPax And strIDReserva_Det = ST.IDReserva_Det And ST.IDCab = intIDCab Then
        '                lvwPax.Items(i).Checked = ST.Selecc
        '                Exit For
        '            End If
        '        Next
        '    Next
        '    blnActPax = True

        '    PintarCambioNombrePaxReservaLvw()
        'Catch ex As Exception
        '    Throw
        'End Try
    End Sub

    Private Sub PintaChecksLvwDetOperacPaxST()
        'If lvwPax.Items.Count = 0 Then Exit Sub
        'Try
        '    blnActPax = False
        '    Dim strIDOperacion_Det As String = frmRefRes.DgvDetalle.CurrentRow.Cells("IDOperacion_Det").Value.ToString
        '    'Dim strIDOperacion As String = frmRefRes.DgvDetalle.CurrentRow.Cells("IDOperacion").Value.ToString

        '    'Dim strIDReserva_Det As String
        '    'If strIDReservas_Det_Rel = "" Then
        '    '    strIDReserva_Det = If(intIDReserva_Det = "0" Or intIDReserva_Det Is Nothing, "M" & frmRefRes.DgvDetalle.CurrentRow.Index.ToString, intIDReserva_Det.ToString)
        '    'Else
        '    '    strIDReserva_Det = Mid(strIDReservas_Det_Rel, 2)
        '    'End If

        '    For Each ST As stDetOperacPax In objLstDetOperacPax

        '        For i As Byte = 0 To lvwPax.Items.Count - 1

        '            If lvwPax.Items(i).SubItems(3).Text = ST.IDPax And strIDOperacion_Det = ST.IDOperacion_Det And ST.IDCab = intIDCab Then
        '                lvwPax.Items(i).Checked = ST.Selecc
        '                Exit For
        '            End If
        '        Next
        '    Next
        '    blnActPax = True

        '    PintarCambioNombrePaxReservaLvw()
        'Catch ex As Exception
        '    Throw
        'End Try
    End Sub

    Private Sub PintaChecksLvwDetReservPaxCambio()
        'Try
        '    blnActPax = False

        '    If lstPaxSelCambio Is Nothing Then Exit Sub

        '    Dim strIDReserva_Det As String
        '    'If strIDReservas_Det_Rel = "" Then
        '    'strIDReserva_Det = If(intIDReserva_Det = "0" Or intIDReserva_Det Is Nothing, "M" & frmRefRes.DgvDetalle.CurrentRow.Index.ToString, intIDReserva_Det.ToString)
        '    strIDReserva_Det = "M" & (frmRefRes.DgvDetalle.CurrentRow.Index + 1).ToString
        '    'Else
        '    'strIDReserva_Det = Mid(strIDReservas_Det_Rel, 2)
        '    'End If

        '    For Each strIDPax In lstPaxSelCambio

        '        For i As Byte = 0 To lvwPax.Items.Count - 1

        '            If lvwPax.Items(i).SubItems(3).Text = strIDPax Then
        '                lvwPax.Items(i).Checked = True
        '                Exit For
        '            End If
        '        Next
        '    Next
        '    blnActPax = True

        'Catch ex As Exception
        '    Throw
        'End Try
    End Sub




    Private Sub GrabarQuiebres()
        Try

            'btnQuiebres_Click(Nothing, Nothing)

DeletedST:
            Dim RegValor As stDetCotiQuiebres
            For Each ST As stDetCotiQuiebres In objLstDetCotiQuiebres
                If ST.IDCAB = intIDCab And ST.IDDet = strIDDet Then
                    objLstDetCotiQuiebres.Remove(ST)
                    GoTo DeletedST 'Exit For
                End If
            Next

            For vbytNroFila As Int16 = 0 To dgvQui.Rows.Count - 1

                RegValor = New stDetCotiQuiebres
                With RegValor
                    .IDCAB = intIDCab
                    'If strModoEdicion = "N" Then
                    .IDDet = strIDDet '"M" & bytMayorIddetSTGeneradoCotiDetQ()
                    If strIDDet1erDia <> "" Then
                        .IDDet = strIDDet1erDia
                    End If
                    '.IDDet = strIDDet '"M" & bytMayorIddetSTGeneradoCotiDetQ()

                    'Else
                    '    .IDDet = strIDDet
                    'End If

                    If Not dgvQui.Item("IDCab_Q", vbytNroFila).Value Is Nothing Then
                        .IDCab_Q = If(dgvQui.Item("IDCab_Q", vbytNroFila).Value.ToString = "", "0", dgvQui.Item("IDCab_Q", vbytNroFila).Value.ToString)
                    Else
                        .IDCab_Q = 0
                    End If


                    If Not dgvQui.Item("IDDet_Q", vbytNroFila).Value Is Nothing Then
                        .IDDet_Q = If(dgvQui.Item("IDDet_Q", vbytNroFila).Value.ToString = "", "0", dgvQui.Item("IDDet_Q", vbytNroFila).Value.ToString)
                    Else
                        .IDDet_Q = 0
                    End If


                    If Not dgvQui.Item("CostoReal", vbytNroFila).Value Is Nothing Then
                        .CostoReal = If(dgvQui.Item("CostoReal", vbytNroFila).Value.ToString = "", "0", dgvQui.Item("CostoReal", vbytNroFila).Value.ToString)
                    Else
                        .CostoReal = 0
                    End If
                    If Not dgvQui.Item("CostoRealImpto", vbytNroFila).Value Is Nothing Then
                        .CostoRealImpto = If(dgvQui.Item("CostoRealImpto", vbytNroFila).Value.ToString = "", "0", dgvQui.Item("CostoRealImpto", vbytNroFila).Value.ToString)
                    Else
                        .CostoRealImpto = 0
                    End If

                    If Not dgvQui.Item("Margen", vbytNroFila).Value Is Nothing Then
                        .Margen = If(dgvQui.Item("Margen", vbytNroFila).Value.ToString = "", "0", dgvQui.Item("Margen", vbytNroFila).Value.ToString)
                    Else
                        .Margen = 0
                    End If

                    If Not dgvQui.Item("MargenImpto", vbytNroFila).Value Is Nothing Then
                        .MargenImpto = If(dgvQui.Item("MargenImpto", vbytNroFila).Value.ToString = "", "0", dgvQui.Item("MargenImpto", vbytNroFila).Value.ToString)
                    Else
                        .MargenImpto = 0
                    End If
                    If Not dgvQui.Item("CostoLiberado", vbytNroFila).Value Is Nothing Then
                        .CostoLiberado = If(dgvQui.Item("CostoLiberado", vbytNroFila).Value.ToString = "", "0", dgvQui.Item("CostoLiberado", vbytNroFila).Value.ToString)
                    Else
                        .CostoLiberado = 0
                    End If
                    If Not dgvQui.Item("CostoLiberadoImpto", vbytNroFila).Value Is Nothing Then
                        .CostoLiberadoImpto = If(dgvQui.Item("CostoLiberadoImpto", vbytNroFila).Value.ToString = "", "0", dgvQui.Item("CostoLiberadoImpto", vbytNroFila).Value.ToString)
                    Else
                        .CostoLiberadoImpto = 0
                    End If

                    If Not dgvQui.Item("MargenLiberado", vbytNroFila).Value Is Nothing Then
                        .MargenLiberado = If(dgvQui.Item("MargenLiberado", vbytNroFila).Value.ToString = "", "0", dgvQui.Item("MargenLiberado", vbytNroFila).Value.ToString)
                    Else
                        .MargenLiberado = 0
                    End If
                    If Not dgvQui.Item("MargenLiberadoImpto", vbytNroFila).Value Is Nothing Then
                        .MargenLiberadoImpto = If(dgvQui.Item("MargenLiberadoImpto", vbytNroFila).Value.ToString = "", "0", dgvQui.Item("MargenLiberadoImpto", vbytNroFila).Value.ToString)
                    Else
                        .MargenLiberadoImpto = 0
                    End If
                    If Not dgvQui.Item("MargenAplicado", vbytNroFila).Value Is Nothing Then
                        .MargenAplicado = If(dgvQui.Item("MargenAplicado", vbytNroFila).Value.ToString = "", "0", dgvQui.Item("MargenAplicado", vbytNroFila).Value.ToString)
                    Else
                        .MargenAplicado = 0
                    End If
                    If Not dgvQui.Item("Total", vbytNroFila).Value Is Nothing Then
                        .Total = If(dgvQui.Item("Total", vbytNroFila).Value.ToString = "", "0", dgvQui.Item("Total", vbytNroFila).Value.ToString)
                    Else
                        .Total = 0
                    End If
                    If Not dgvQui.Item("RedondeoTotal", vbytNroFila).Value Is Nothing Then
                        .RedondeoTotal = If(dgvQui.Item("RedondeoTotal", vbytNroFila).Value.ToString = "", "0", dgvQui.Item("RedondeoTotal", vbytNroFila).Value.ToString)
                    Else
                        .RedondeoTotal = 0
                    End If
                    If Not dgvQui.Item("SSCR", vbytNroFila).Value Is Nothing Then
                        .SSCR = If(dgvQui.Item("SSCR", vbytNroFila).Value.ToString = "", "0", dgvQui.Item("SSCR", vbytNroFila).Value.ToString)
                    Else
                        .SSCR = 0
                    End If
                    If Not dgvQui.Item("SSCRImpto", vbytNroFila).Value Is Nothing Then
                        .SSCRImpto = If(dgvQui.Item("SSCRImpto", vbytNroFila).Value.ToString = "", "0", dgvQui.Item("SSCRImpto", vbytNroFila).Value.ToString)
                    Else
                        .SSCRImpto = 0
                    End If
                    If Not dgvQui.Item("SSMargen", vbytNroFila).Value Is Nothing Then
                        .SSMargen = If(dgvQui.Item("SSMargen", vbytNroFila).Value.ToString = "", "0", dgvQui.Item("SSMargen", vbytNroFila).Value.ToString)
                    Else
                        .SSMargen = 0
                    End If
                    If Not dgvQui.Item("SSMargenImpto", vbytNroFila).Value Is Nothing Then
                        .SSMargenImpto = If(dgvQui.Item("SSMargenImpto", vbytNroFila).Value.ToString = "", "0", dgvQui.Item("SSMargenImpto", vbytNroFila).Value.ToString)
                    Else
                        .SSMargenImpto = 0
                    End If

                    If Not dgvQui.Item("SSCL", vbytNroFila).Value Is Nothing Then
                        .SSCL = If(dgvQui.Item("SSCL", vbytNroFila).Value.ToString = "", "0", dgvQui.Item("SSCL", vbytNroFila).Value.ToString)
                    Else
                        .SSCL = 0
                    End If

                    If Not dgvQui.Item("SSCLImpto", vbytNroFila).Value Is Nothing Then
                        .SSCLImpto = If(dgvQui.Item("SSCLImpto", vbytNroFila).Value.ToString = "", "0", dgvQui.Item("SSCLImpto", vbytNroFila).Value.ToString)
                    Else
                        .SSCLImpto = 0
                    End If
                    If Not dgvQui.Item("SSML", vbytNroFila).Value Is Nothing Then
                        .SSMargenLiberado = If(dgvQui.Item("SSML", vbytNroFila).Value.ToString = "", "0", dgvQui.Item("SSML", vbytNroFila).Value.ToString)
                    Else
                        .SSMargenLiberado = 0
                    End If
                    If Not dgvQui.Item("SSMargenLiberadoImpto", vbytNroFila).Value Is Nothing Then
                        .SSMargenLiberadoImpto = If(dgvQui.Item("SSMargenLiberadoImpto", vbytNroFila).Value.ToString = "", "0", dgvQui.Item("SSMargenLiberadoImpto", vbytNroFila).Value.ToString)
                    Else
                        .SSMargenLiberadoImpto = 0
                    End If
                    If Not dgvQui.Item("SSTotal", vbytNroFila).Value Is Nothing Then
                        .SSTotal = If(dgvQui.Item("SSTotal", vbytNroFila).Value.ToString = "", "0", dgvQui.Item("SSTotal", vbytNroFila).Value.ToString)
                    Else
                        .SSTotal = 0
                    End If


                    If Not dgvQui.Item("STCR", vbytNroFila).Value Is Nothing Then
                        .STCR = If(dgvQui.Item("STCR", vbytNroFila).Value.ToString = "", "0", dgvQui.Item("STCR", vbytNroFila).Value.ToString)
                    Else
                        .STCR = 0
                    End If
                    If Not dgvQui.Item("STCRImpto", vbytNroFila).Value Is Nothing Then
                        .STCRImpto = If(dgvQui.Item("STCRImpto", vbytNroFila).Value.ToString = "", "0", dgvQui.Item("STCRImpto", vbytNroFila).Value.ToString)
                    Else
                        .STCRImpto = 0
                    End If

                    If Not dgvQui.Item("STMargen", vbytNroFila).Value Is Nothing Then
                        .STMargen = If(dgvQui.Item("STMargen", vbytNroFila).Value.ToString = "", "0", dgvQui.Item("STMargen", vbytNroFila).Value.ToString)
                    Else
                        .STMargen = 0
                    End If
                    If Not dgvQui.Item("STMargenImpto", vbytNroFila).Value Is Nothing Then
                        .STMargenImpto = If(dgvQui.Item("STMargenImpto", vbytNroFila).Value.ToString = "", "0", dgvQui.Item("STMargenImpto", vbytNroFila).Value.ToString)
                    Else
                        .STMargenImpto = 0
                    End If
                    If Not dgvQui.Item("STTotal", vbytNroFila).Value Is Nothing Then
                        .STTotal = If(dgvQui.Item("STTotal", vbytNroFila).Value.ToString = "", "0", dgvQui.Item("STTotal", vbytNroFila).Value.ToString)
                    Else
                        .STTotal = 0
                    End If

                    If Not dgvQui.Item("TotImpto", vbytNroFila).Value Is Nothing Then
                        .TotImpto = If(dgvQui.Item("TotImpto", vbytNroFila).Value.ToString = "", "0", dgvQui.Item("TotImpto", vbytNroFila).Value.ToString)
                    Else
                        .TotImpto = 0
                    End If

                    If Not dgvQui.Item("Accion", vbytNroFila).Value Is Nothing Then
                        .Accion = If(dgvQui.Item("Accion", vbytNroFila).Value.ToString = "", "", dgvQui.Item("Accion", vbytNroFila).Value.ToString)
                    Else
                        .Accion = ""
                    End If
                End With
                objLstDetCotiQuiebres.Add(RegValor)
            Next

        Catch ex As Exception
            Throw
        End Try
    End Sub

    Private Sub BorrarSTQuiebres()
        If objLstDetCotiQuiebres Is Nothing Then Exit Sub
Borrar:
        Dim intInd As Int16 = 0
        For Each ST As stDetCotiQuiebres In objLstDetCotiQuiebres

            objLstDetCotiQuiebres.RemoveAt(intInd)
            GoTo Borrar
            'intInd += 1
        Next
    End Sub

    Private Sub CargarControlesReservas()
        'Try
        '    With frmRefRes.DgvDetalle
        '        Dim bytFilaDgv As Byte = .CurrentRow.Index
        '        If Not .Item("Dia", bytFilaDgv).Value Is Nothing Then
        '            dtpDia.Text = .Item("Dia", bytFilaDgv).Value.ToString
        '        End If
        '        If Not .Item("Hora", bytFilaDgv).Value Is Nothing Then
        '            dtpDiaHora.Text = .Item("Hora", bytFilaDgv).Value.ToString
        '        End If
        '        strHora = dtpDiaHora.Text
        '        If Not .Item("IDPais", bytFilaDgv).Value Is Nothing Then
        '            cboPais.SelectedValue = .Item("IDPais", bytFilaDgv).Value.ToString
        '            cboPais_SelectionChangeCommitted(Nothing, Nothing)
        '        End If
        '        If Not .Item("IDubigeo", bytFilaDgv).Value Is Nothing Then
        '            cboCiudad.SelectedValue = .Item("IDubigeo", bytFilaDgv).Value.ToString
        '        End If
        '        If Not .Item("Transfer", bytFilaDgv).Value Is Nothing Then
        '            chkTransfer.Checked = .Item("Transfer", bytFilaDgv).Value
        '        End If
        '        If Not .Item("IDTipoProv", bytFilaDgv).Value Is Nothing Then
        '            cboTipoProv.SelectedValue = .Item("IDTipoProv", bytFilaDgv).Value.ToString
        '            cboTipoProv_SelectionChangeCommitted(Nothing, Nothing)
        '        End If

        '        If Not .Item("IDProveedor", bytFilaDgv).Value Is Nothing Then
        '            strIDProveedor = .Item("IDProveedor", bytFilaDgv).Value.ToString
        '        End If
        '        If Not .Item("Proveedor", bytFilaDgv).Value Is Nothing Then
        '            lblProveedor.Text = .Item("Proveedor", bytFilaDgv).Value.ToString
        '        End If

        '        If Not .Item("IDServicio", bytFilaDgv).Value Is Nothing Then
        '            strIDServicio = .Item("IDServicio", bytFilaDgv).Value.ToString
        '        End If
        '        If Not .Item("IDServicio_Det", bytFilaDgv).Value Is Nothing Then
        '            intIDServicio_Det = .Item("IDServicio_Det", bytFilaDgv).Value.ToString
        '        End If

        '        If Not .Item("DescServicioDetBup", bytFilaDgv).Value Is Nothing Then
        '            strDescServicioDetBup = .Item("DescServicioDetBup", bytFilaDgv).Value.ToString

        '            lblTitulo.Text = "Servicio: " & strDescServicioDetBup
        '            lblTitulo2.Text = "Servicio: " & strDescServicioDetBup
        '            lblTitulo3.Text = "Servicio: " & strDescServicioDetBup
        '            lblTitulo4.Text = "Servicio: " & strDescServicioDetBup

        '        End If


        '        If Not .Item("DescServicioDet", bytFilaDgv).Value Is Nothing Then
        '            strDescServicio_Det = .Item("DescServicioDet", bytFilaDgv).Value.ToString
        '            txtServicio.Text = .Item("DescServicioDet", bytFilaDgv).Value.ToString
        '        End If


        '        If Not .Item("Tipo", bytFilaDgv).Value Is Nothing Then
        '            strTipoServicio_Det = .Item("Tipo", bytFilaDgv).Value.ToString
        '            lblVariante.Text = strTipoServicio_Det
        '        End If

        '        If Not .Item("Anio", bytFilaDgv).Value Is Nothing Then
        '            strAnio = .Item("Anio", bytFilaDgv).Value.ToString
        '        End If
        '        If Not .Item("CostoLiberado", bytFilaDgv).Value Is Nothing Then
        '            lblCostoLiberado.Text = .Item("CostoLiberado", bytFilaDgv).Value
        '            lblCostoLiberado2.Text = .Item("CostoLiberado", bytFilaDgv).Value
        '        End If

        '        If Not .Item("CostoReal", bytFilaDgv).Value Is Nothing Then
        '            txtCostoReal.Text = .Item("CostoReal", bytFilaDgv).Value
        '            lblCostoReal2.Text = .Item("CostoReal", bytFilaDgv).Value
        '        End If
        '        If Not .Item("Margen", bytFilaDgv).Value Is Nothing Then
        '            lblMargen.Text = .Item("Margen", bytFilaDgv).Value
        '            lblMargen2.Text = .Item("Margen", bytFilaDgv).Value
        '        End If
        '        If Not .Item("MargenAplicado", bytFilaDgv).Value Is Nothing Then
        '            txtMargenAplicado.Text = .Item("MargenAplicado", bytFilaDgv).Value
        '            lblMargenAplicado2.Text = .Item("MargenAplicado", bytFilaDgv).Value
        '        End If
        '        If Not .Item("MargenLiberado", bytFilaDgv).Value Is Nothing Then
        '            lblMargenLiberado.Text = .Item("MargenLiberado", bytFilaDgv).Value
        '            lblMargenLiberado2.Text = .Item("MargenLiberado", bytFilaDgv).Value
        '        End If
        '        If Not .Item("MotivoEspecial", bytFilaDgv).Value Is Nothing Then
        '            txtMotivoEspecial.Text = .Item("MotivoEspecial", bytFilaDgv).Value.ToString
        '        End If
        '        If Not .Item("RutaDocSustento", bytFilaDgv).Value Is Nothing Then
        '            ' txtRutaDocSustento.Text = .Item("RutaDocSustento", bytFilaDgv).Value.ToString
        '            lnkDocumentoSustento.Text = .Item("RutaDocSustento", bytFilaDgv).Value.ToString
        '        End If

        '        If Not .Item("Desayuno", bytFilaDgv).Value Is Nothing Then
        '            chkDesayuno.Checked = .Item("Desayuno", bytFilaDgv).Value
        '        End If
        '        If Not .Item("Lonche", bytFilaDgv).Value Is Nothing Then
        '            chkLonche.Checked = .Item("Lonche", bytFilaDgv).Value
        '        End If
        '        If Not .Item("Almuerzo", bytFilaDgv).Value Is Nothing Then
        '            chkAlmuerzo.Checked = .Item("Almuerzo", bytFilaDgv).Value
        '        End If
        '        If Not .Item("Cena", bytFilaDgv).Value Is Nothing Then
        '            chkCena.Checked = .Item("Cena", bytFilaDgv).Value
        '        End If


        '        If Not .Item("TipoTransporte", bytFilaDgv).Value Is Nothing Then
        '            cboTipoTransporte.SelectedValue = .Item("TipoTransporte", bytFilaDgv).Value.ToString
        '        End If

        '        Dim objUbig As New clsTablasApoyoBN.clsUbigeoBN
        '        If Not .Item("IDUbigeoOri", bytFilaDgv).Value Is Nothing Then
        '            cboPaisOrig.SelectedValue = objUbig.strDevuelveIDPaisxCiudad(.Item("IDUbigeoOri", bytFilaDgv).Value.ToString)
        '            cboPaisOrig_SelectionChangeCommitted(Nothing, Nothing)
        '            cboIDUbigeoOri.SelectedValue = .Item("IDUbigeoOri", bytFilaDgv).Value.ToString
        '        End If
        '        If Not .Item("IDUbigeoDes", bytFilaDgv).Value Is Nothing Then
        '            cboPaisDest.SelectedValue = objUbig.strDevuelveIDPaisxCiudad(.Item("IDUbigeoDes", bytFilaDgv).Value.ToString)
        '            cboPaisDest_SelectionChangeCommitted(Nothing, Nothing)
        '            cboIDUbigeoDes.SelectedValue = .Item("IDUbigeoDes", bytFilaDgv).Value.ToString
        '        End If


        '        If Not .Item("Total", bytFilaDgv).Value Is Nothing Then
        '            lblTotal.Text = .Item("Total", bytFilaDgv).Value
        '            lblTotal2.Text = .Item("Total", bytFilaDgv).Value
        '        End If
        '        If Not .Item("CostoLiberadoImpto", bytFilaDgv).Value Is Nothing Then
        '            lblCostoLiberadoImpto.Text = .Item("CostoLiberadoImpto", bytFilaDgv).Value
        '            lblCostoLiberadoImpto2.Text = .Item("CostoLiberadoImpto", bytFilaDgv).Value
        '        End If
        '        If Not .Item("CostoRealImpto", bytFilaDgv).Value Is Nothing Then
        '            txtCostoRealImpto.Text = .Item("CostoRealImpto", bytFilaDgv).Value
        '            lblCostoRealImpto2.Text = .Item("CostoRealImpto", bytFilaDgv).Value
        '        End If
        '        If Not .Item("MargenImpto", bytFilaDgv).Value Is Nothing Then
        '            lblMargenImpto.Text = .Item("MargenImpto", bytFilaDgv).Value
        '            lblMargenImpto2.Text = .Item("MargenImpto", bytFilaDgv).Value
        '        End If
        '        If Not .Item("MargenLiberadoImpto", bytFilaDgv).Value Is Nothing Then
        '            lblMargenLiberadoImpto.Text = .Item("MargenLiberadoImpto", bytFilaDgv).Value
        '            lblMargenLiberadoImpto2.Text = .Item("MargenLiberadoImpto", bytFilaDgv).Value
        '        End If


        '        If Not .Item("TotImpto", bytFilaDgv).Value Is Nothing Then
        '            lblTotImpto.Text = .Item("TotImpto", bytFilaDgv).Value
        '            lblTotImpto2.Text = .Item("TotImpto", bytFilaDgv).Value
        '        End If


        '        If Not .Item("Especial", bytFilaDgv).Value Is Nothing Then
        '            chkEspecial.Checked = .Item("Especial", bytFilaDgv).Value
        '        End If
        '        If Not .Item("IDIdiomaDet", bytFilaDgv).Value Is Nothing Then
        '            cboIDIdioma.SelectedValue = .Item("IDIdiomaDet", bytFilaDgv).Value.ToString
        '        End If

        '        If Not .Item("IDReserva_Det_Rel", bytFilaDgv).Value Is Nothing Then
        '            If .Item("IDReserva_Det_Rel", bytFilaDgv).Value.ToString = "0" Then
        '                strIDReservas_Det_Rel = ""
        '            Else
        '                strIDReservas_Det_Rel = .Item("IDReserva_Det_Rel", bytFilaDgv).Value.ToString
        '            End If

        '        End If

        '        If Not .Item("NroLiberados", bytFilaDgv).Value Is Nothing Then
        '            txtNroLiberados.Text = .Item("NroLiberados", bytFilaDgv).Value.ToString
        '        End If
        '        If Not .Item("Tipo_Lib", bytFilaDgv).Value Is Nothing Then
        '            cboTipoLib.SelectedValue = .Item("Tipo_Lib", bytFilaDgv).Value.ToString
        '        End If

        '        If Not .Item("IDGuiaProveedor", bytFilaDgv).Value Is Nothing Then
        '            cboGuia.SelectedValue = .Item("IDGuiaProveedor", bytFilaDgv).Value.ToString.Trim
        '        End If

        '        If Not .Item("FlServicioNoShow", bytFilaDgv).Value Is Nothing Then
        '            chkServicioNoShow.Checked = Convert.ToBoolean(.Item("FlServicioNoShow", bytFilaDgv).Value)
        '        End If

        '        If Not .Item("FlServicioNoCobrado", bytFilaDgv).Value Is Nothing Then
        '            chkServNoCobrado.Checked = Convert.ToBoolean(.Item("FlServicioNoCobrado", bytFilaDgv).Value)
        '        End If
        '        If Not .Item("CantidadaPagar", bytFilaDgv).Value Is Nothing Then
        '            If chkServNoCobrado.Checked Then
        '                If Convert.ToDouble(.Item("QtCantidadAPagarAntNoCobrado", bytFilaDgv).Value) = 0 Then
        '                    bytCantidadaPagarAntNoCobrado = .Item("CantidadaPagar", bytFilaDgv).Value
        '                Else
        '                    bytCantidadaPagarAntNoCobrado = .Item("QtCantidadAPagarAntNoCobrado", bytFilaDgv).Value
        '                End If
        '            End If

        '        End If
        '        If strModulo = "R" Or strModulo = "O" Then
        '            lblCantPagarEtiq.Visible = True
        '            txtCantidadaPagar.Visible = True
        '            If strIDTipoProv <> gstrTipoProveeHoteles Then
        '                grbBus_Guia.Visible = True

        '                grbBus_Guia.Location = lblFechaOutEtiq.Location
        '            Else
        '                'lblCantPagarEtiq.Location = New Point(grbChecksVoucBiblia.Location.X - 4, grbChecksVoucBiblia.Location.Y)
        '                'txtCantidadaPagar.Location = New Point(grbChecksVoucBiblia.Location.X + 3 + lblCantPagarEtiq.Width, dtpFechaOut.Location.Y + 22)
        '            End If


        '            CargarVehiculos()

        '            'txtDescBus.Text = .Item("DescBus", bytFilaDgv).Value.ToString


        '            Dim bytTipoVeh As Byte = 2
        '            If .Item("Transfer", bytFilaDgv).Value = True Then
        '                bytTipoVeh = 2
        '            Else
        '                bytTipoVeh = 3
        '            End If

        '            Dim strVeh As String = .Item("NuVehiculo", bytFilaDgv).Value

        '            'If IsDBNull(.Item("NuVehiculo", bytFilaDgv).Value) Or .Item("NuVehiculo", bytFilaDgv).Value = "" Or .Item("NuVehiculo", bytFilaDgv).Value = "0" Then
        '            If strVeh = "0" Then
        '                Dim bytValorV As Byte = bytValorNuVehiculo2(.Item("IDubigeo", bytFilaDgv).Value, strIDTipoServ, bytTipoVeh, CByte(.Item("CantidadAPagar", bytFilaDgv).Value))
        '                If bytValorV > 0 Then
        '                    CboBusVehiculo.SelectedValue = bytValorV
        '                End If
        '            Else
        '                CboBusVehiculo.SelectedValue = .Item("NuVehiculo", bytFilaDgv).Value
        '            End If

        '            Dim strTextVeh As String = CboBusVehiculo.Text

        '            If strTextVeh.Trim.Length = 0 Then
        '                Dim bytValorV As Byte = bytValorNuVehiculo2(.Item("IDubigeo", bytFilaDgv).Value, strIDTipoServ, bytTipoVeh, CByte(.Item("CantidadAPagar", bytFilaDgv).Value))
        '                If bytValorV > 0 Then
        '                    CboBusVehiculo.SelectedValue = bytValorV
        '                End If
        '            End If



        '            'If .Item("NuVehiculo", bytFilaDgv).Value = 0 And strIDTipoProv <> gstrTipoProveeRestaurantes Then
        '            '    If strIDTipoProv = gstrTipoProveeTransportista And cboTipoTransporte.Text <> "" Then
        '            '        CboBusVehiculo.SelectedValue = .Item("NuVehiculo", bytFilaDgv).Value
        '            '    Else
        '            '        Dim bytNroTotalPax As Int16 = intPax + intLiberados
        '            '        CboBusVehiculo.SelectedValue = bytValorNuVehiculo(blnEsOtraCiudadBuss(), bytNroTotalPax)
        '            '    End If
        '            'Else
        '            '    CboBusVehiculo.SelectedValue = .Item("NuVehiculo", bytFilaDgv).Value
        '            'End If

        '            txtCantidadaPagar.Text = .Item("CantidadAPagar", bytFilaDgv).Value.ToString
        '        End If

        '        If strModulo = "O" Then
        '            'If Not .Item("FechaRecordatorio", bytFilaDgv).Value Is Nothing Then
        '            '    dtpFechaRecordatorio.Text = Replace(.Item("FechaRecordatorio", bytFilaDgv).Value.ToString.Substring(0, 10), "0001", "1900")
        '            'End If
        '            'If Not .Item("Recordatorio", bytFilaDgv).Value Is Nothing Then
        '            '    txtRecordatorio.Text = .Item("Recordatorio", bytFilaDgv).Value.ToString
        '            'End If
        '            If Not .Item("ObservVoucher", bytFilaDgv).Value Is Nothing Then
        '                txtObservVoucher.Text = .Item("ObservVoucher", bytFilaDgv).Value.ToString
        '            End If
        '            If Not .Item("ObservBiblia", bytFilaDgv).Value Is Nothing Then
        '                txtObservBiblia.Text = .Item("ObservBiblia", bytFilaDgv).Value.ToString
        '            End If
        '            If Not .Item("ObservInterno", bytFilaDgv).Value Is Nothing Then
        '                txtObservInterno.Text = .Item("ObservInterno", bytFilaDgv).Value.ToString
        '            End If

        '            If Not .Item("IDVoucher", bytFilaDgv).Value Is Nothing Then
        '                If .Item("IDVoucher", bytFilaDgv).Value.ToString = "" Then
        '                    chkVerVoucher.Enabled = False
        '                Else
        '                    chkVerVoucher.Checked = .Item("VerObserVoucher", bytFilaDgv).Value
        '                End If

        '            End If

        '            chkVerBiblia.Checked = .Item("VerObserBiblia", bytFilaDgv).Value


        '        End If

        '        If Not .Item("IDTipoServ", bytFilaDgv).Value Is Nothing Then
        '            lblTipoServicio.Text = .Item("IDTipoServ", bytFilaDgv).Value.ToString
        '        End If

        '        If Not .Item("DetaTipo", bytFilaDgv).Value Is Nothing Then
        '            lblDescVariante.Text = .Item("DetaTipo", bytFilaDgv).Value.ToString
        '        End If

        '        blnCantidadAPagarEditado = .Item("CantidadAPagarEditado", bytFilaDgv).Value.ToString
        '        blnNetoHabEditado = .Item("NetoHabEditado", bytFilaDgv).Value.ToString
        '        blnIgvHabEditado = .Item("IgvHabEditado", bytFilaDgv).Value.ToString

        '        ColorearTextoCamposEditadosReserva()

        '        If Not .Item("NetoHab", bytFilaDgv).Value Is Nothing Then
        '            txtNetoHab.Text = .Item("NetoHab", bytFilaDgv).Value.ToString
        '        End If

        '        If Not .Item("IgvHab", bytFilaDgv).Value Is Nothing Then
        '            txtIgvHab.Text = .Item("IgvHab", bytFilaDgv).Value.ToString
        '        End If

        '        If Not .Item("TotalHab", bytFilaDgv).Value Is Nothing Then
        '            lblTotalHab.Text = Convert.ToDouble(.Item("TotalHab", bytFilaDgv).Value.ToString).ToString("##,##0.0000")
        '            txtTotalHab.Text = lblTotalHab.Text
        '        End If

        '        If Not .Item("NetoGen", bytFilaDgv).Value Is Nothing Then
        '            lblNetoTotal.Text = Convert.ToDouble(.Item("NetoGen", bytFilaDgv).Value.ToString).ToString("##,##0.0000")
        '        End If

        '        If Not .Item("IgvGen", bytFilaDgv).Value Is Nothing Then
        '            lblIgvTotal.Text = Convert.ToDouble(.Item("IgvGen", bytFilaDgv).Value.ToString).ToString("##,##0.0000")
        '        End If

        '        If Not .Item("SimboloMoneda", bytFilaDgv).Value Is Nothing Then
        '            lblTotalMonEtiq.Text = Replace(lblTotalMonEtiq.Text, "$", .Item("SimboloMoneda", bytFilaDgv).Value.ToString)
        '        End If

        '        If Not .Item("TotalGen", bytFilaDgv).Value Is Nothing Then
        '            lblTotalR.Text = Convert.ToDouble(.Item("TotalGen", bytFilaDgv).Value.ToString).ToString("##,##0.00")
        '            If chkServNoCobrado.Checked Then
        '                If Convert.ToDouble(.Item("SsTotalGenAntNoCobrado", bytFilaDgv).Value) = 0 Then
        '                    dblTotalGenAntNoCobrado = lblTotalR.Text
        '                Else
        '                    dblTotalGenAntNoCobrado = .Item("SsTotalGenAntNoCobrado", bytFilaDgv).Value
        '                End If

        '            End If

        '        End If


        '        CargarToolTip(.Item("CostoRealAnt", bytFilaDgv).Value.ToString, .Item("MargenAplicadoAnt", bytFilaDgv).Value.ToString)

        '        CargarQuiebres()

        '        CargarPaxLvw()

        '        If intIDDet = 0 Then
        '            If strIDReservas_Det_Rel = "" Then

        '                If Not .Item("NroPax", bytFilaDgv).Value Is Nothing Then
        '                    txtNroPax.Text = .Item("NroPax", bytFilaDgv).Value.ToString
        '                End If

        '            Else

        '                If strIDReservas_Det_Rel.Substring(0, 1) <> "H" Then
        '                    If Not .Item("NroPax", bytFilaDgv).Value Is Nothing Then
        '                        txtNroPax.Text = .Item("NroPax", bytFilaDgv).Value.ToString
        '                    End If
        '                Else
        '                    If chkIncGuia.Checked = False Then
        '                        'txtNroPax.Text = lvwPax.CheckedItems.Count
        '                        txtNroPax.Text = lvwPax.CheckedItems.Count - Convert.ToInt16(If(txtNroLiberados.Text = "", "0", txtNroLiberados.Text))
        '                    End If
        '                    'GroupBox1.Enabled = False
        '                    pEnableControlsGroupBox(grbDatosGenerales, False)
        '                    'pEnableControlsGroupBox(grbPax, False)
        '                    'txtServicio.Enabled = True
        '                    'btnAceptar.Enabled = False
        '                End If

        '            End If
        '        Else
        '            'If strIDDetRel <> "R1" Then
        '            'Else
        '            'txtNroPax.Text = lvwPax.CheckedItems.Count
        '            'End If
        '            If strIDReservas_Det_Rel <> "" Then
        '                'GroupBox1.Enabled = False
        '                pEnableControlsGroupBox(grbDatosGenerales, False)
        '                'pEnableControlsGroupBox(grbPax, False)
        '                'txtServicio.Enabled = True
        '                'btnAceptar.Enabled = False
        '                If chkIncGuia.Checked = False Then
        '                    'txtNroPax.Text = lvwPax.CheckedItems.Count
        '                    txtNroPax.Text = lvwPax.CheckedItems.Count - Convert.ToInt16(If(txtNroLiberados.Text = "", "0", txtNroLiberados.Text))
        '                End If
        '            Else
        '                If Not .Item("NroPax", bytFilaDgv).Value Is Nothing Then
        '                    txtNroPax.Text = .Item("NroPax", bytFilaDgv).Value.ToString
        '                End If


        '            End If
        '        End If
        '        If txtNroPax.Text <> intPax Then
        '            chkSelPax.Checked = True
        '        End If
        '        If chkTransfer.Checked = True Then
        '            'CargarCombosTransferReservas()
        '            CargarCombosTransferOrigenDestino(Nothing, dtpDia.Text, cboOrigen, cboDestino, intIDCab)

        '            'If Not .Item("IDDetTransferOri", bytFilaDgv).Value Is Nothing Then
        '            '    cboOrigen.SelectedValue = .Item("IDDetTransferOri", bytFilaDgv).Value.ToString
        '            'End If
        '            'If Not .Item("IDDetTransferDes", bytFilaDgv).Value Is Nothing Then
        '            '    cboDestino.SelectedValue = .Item("IDDetTransferDes", bytFilaDgv).Value.ToString
        '            'End If
        '            Dim intIDDetTransferOri As Integer = 0, intIDDetTransferDes As Integer = 0
        '            If Not .Item("IDDetTransferOri", bytFilaDgv).Value Is Nothing Then
        '                intIDDetTransferOri = .Item("IDDetTransferOri", bytFilaDgv).Value.ToString
        '            End If
        '            If Not .Item("IDDetTransferDes", bytFilaDgv).Value Is Nothing Then
        '                intIDDetTransferDes = .Item("IDDetTransferDes", bytFilaDgv).Value.ToString
        '            End If
        '            'If Not (intIDDetTransferOri = 0 And intIDDetTransferDes = 0) Then

        '            '    cboOrigen.SelectedValue = intIDDetTransferOri
        '            '    cboDestino.SelectedValue = intIDDetTransferDes

        '            'End If
        '            If Not (intIDDetTransferOri = 0) Then
        '                cboOrigen.SelectedValue = intIDDetTransferOri
        '            End If
        '            If Not (intIDDetTransferDes = 0) Then

        '                cboDestino.SelectedValue = intIDDetTransferDes

        '            End If
        '            If frmRefRes.DgvDetalle.CurrentRow.Index = 0 Then
        '                'cboOrigen.Enabled = False
        '                cboOrigen_SelectionChangeCommitted(cboOrigen, Nothing)
        '            End If

        '        End If


        '        If Not .Item("FechaOut", bytFilaDgv).Value Is Nothing Then
        '            dtpFechaOut.Text = .Item("FechaOut", bytFilaDgv).Value.ToString
        '        End If
        '        'MsgBox(.Item("Noches", bytFilaDgv).Value.ToString & " : " & lblNoches.Text)

        '        If Not .Item("Noches", bytFilaDgv).Value Is Nothing Then
        '            lblNoches.Text = .Item("Noches", bytFilaDgv).Value.ToString
        '        End If

        '        If Not .Item("Cantidad", bytFilaDgv).Value Is Nothing Then
        '            txtCantidad.Text = .Item("Cantidad", bytFilaDgv).Value.ToString
        '        End If

        '        If Not .Item("IncGuia", bytFilaDgv).Value Is Nothing Then
        '            chkIncGuia.Checked = .Item("IncGuia", bytFilaDgv).Value
        '        End If

        '    End With

        '    CargarDgvTextosIdioma()



        'Catch ex As Exception
        '    Throw
        'End Try
    End Sub
    Private Sub CargarControles()
        Try
            With frmRef.dgvDet
                Dim bytFilaDgv As Byte = .CurrentRow.Index
                'If Not .Item("Dia", bytFilaDgv).Value Is Nothing Then
                '    dtpDia.Text = .Item("Dia", bytFilaDgv).Value.ToString
                'End If

                dtpDia.Text = .Item("Dia", bytFilaDgv).Value.ToString
                If Not .Item("Hora", bytFilaDgv).Value Is Nothing Then
                    dtpDiaHora.Text = .Item("Hora", bytFilaDgv).Value.ToString
                End If
                strHora = dtpDiaHora.Text
                If Not .Item("IDPais", bytFilaDgv).Value Is Nothing Then
                    cboPais.SelectedValue = .Item("IDPais", bytFilaDgv).Value.ToString
                    cboPais_SelectionChangeCommitted(Nothing, Nothing)
                End If
                If Not .Item("IDCiudad", bytFilaDgv).Value Is Nothing Then
                    cboCiudad.SelectedValue = .Item("IDCiudad", bytFilaDgv).Value.ToString
                End If
                If Not .Item("Transfer", bytFilaDgv).Value Is Nothing Then
                    chkTransfer.Checked = .Item("Transfer", bytFilaDgv).Value
                End If
                If Not .Item("IDTipoProv", bytFilaDgv).Value Is Nothing Then
                    cboTipoProv.SelectedValue = .Item("IDTipoProv", bytFilaDgv).Value.ToString
                    cboTipoProv_SelectionChangeCommitted(Nothing, Nothing)
                End If

                If Not .Item("IDProveedor", bytFilaDgv).Value Is Nothing Then
                    strIDProveedor = .Item("IDProveedor", bytFilaDgv).Value.ToString
                End If
                If Not .Item("DescProveedor", bytFilaDgv).Value Is Nothing Then
                    lblProveedor.Text = .Item("DescProveedor", bytFilaDgv).Value.ToString
                End If

                If Not .Item("IDServicio", bytFilaDgv).Value Is Nothing Then
                    strIDServicio = .Item("IDServicio", bytFilaDgv).Value.ToString
                End If
                If Not .Item("IDServicio_Det", bytFilaDgv).Value Is Nothing Then
                    intIDServicio_Det = .Item("IDServicio_Det", bytFilaDgv).Value.ToString
                End If

                If Not .Item("DescServicioDet", bytFilaDgv).Value Is Nothing Then
                    strDescServicio_Det = .Item("DescServicioDet", bytFilaDgv).Value.ToString
                End If

                If Not .Item("DescServicioDetBup", bytFilaDgv).Value Is Nothing Then
                    strDescServicioDetBup = .Item("DescServicioDetBup", bytFilaDgv).Value.ToString
                    If strDescServicioDetBup.Trim = "" Then strDescServicioDetBup = strDescServicio_Det

                    lblTitulo.Text = "Servicio: " & strDescServicioDetBup
                    lblTitulo2.Text = "Servicio: " & strDescServicioDetBup
                    lblTitulo3.Text = "Servicio: " & strDescServicioDetBup
                    lblTitulo4.Text = "Servicio: " & strDescServicioDetBup

                End If

                If Not .Item("Tipo", bytFilaDgv).Value Is Nothing Then
                    strTipoServicio_Det = .Item("Tipo", bytFilaDgv).Value.ToString
                    lblVariante.Text = .Item("Tipo", bytFilaDgv).Value.ToString
                End If

                If Not .Item("DescTipoServ", bytFilaDgv).Value Is Nothing Then
                    lblTipoServicio.Text = .Item("DescTipoServ", bytFilaDgv).Value.ToString
                End If

                If Not .Item("DescVariante", bytFilaDgv).Value Is Nothing Then
                    lblDescVariante.Text = .Item("DescVariante", bytFilaDgv).Value.ToString
                End If

                If Not .Item("Anio", bytFilaDgv).Value Is Nothing Then
                    strAnio = .Item("Anio", bytFilaDgv).Value.ToString
                End If
                If Not .Item("CostoLiberado", bytFilaDgv).Value Is Nothing Then
                    lblCostoLiberado.Text = Convert.ToDouble(.Item("CostoLiberado", bytFilaDgv).Value.ToString).ToString("##,##0.0000")
                    lblCostoLiberado2.Text = .Item("CostoLiberado", bytFilaDgv).Value.ToString
                End If

                If Not .Item("CostoReal", bytFilaDgv).Value Is Nothing Then
                    txtCostoReal.Text = Convert.ToDouble(.Item("CostoReal", bytFilaDgv).Value.ToString).ToString("##,##0.0000")
                    lblCostoReal2.Text = .Item("CostoReal", bytFilaDgv).Value.ToString
                End If
                If Not .Item("Margen", bytFilaDgv).Value Is Nothing Then
                    lblMargen.Text = Convert.ToDouble(.Item("Margen", bytFilaDgv).Value.ToString).ToString("##,##0.0000")
                    lblMargen2.Text = .Item("Margen", bytFilaDgv).Value.ToString
                End If
                If Not .Item("MargenAplicado", bytFilaDgv).Value Is Nothing Then
                    txtMargenAplicado.Text = .Item("MargenAplicado", bytFilaDgv).Value.ToString
                    lblMargenAplicado2.Text = .Item("MargenAplicado", bytFilaDgv).Value.ToString
                    lblMargenAplicado3.Text = .Item("MargenAplicado", bytFilaDgv).Value.ToString
                    lblMargenAplicado4.Text = .Item("MargenAplicado", bytFilaDgv).Value.ToString
                End If
                If Not .Item("MargenLiberado", bytFilaDgv).Value Is Nothing Then
                    lblMargenLiberado.Text = .Item("MargenLiberado", bytFilaDgv).Value.ToString
                    lblMargenLiberado2.Text = .Item("MargenLiberado", bytFilaDgv).Value.ToString
                End If
                If Not .Item("MotivoEspecial", bytFilaDgv).Value Is Nothing Then
                    txtMotivoEspecial.Text = .Item("MotivoEspecial", bytFilaDgv).Value.ToString
                End If
                If Not .Item("RutaDocSustento", bytFilaDgv).Value Is Nothing Then
                    'txtRutaDocSustento.Text = .Item("RutaDocSustento", bytFilaDgv).Value.ToString
                    lnkDocumentoSustento.Text = .Item("RutaDocSustento", bytFilaDgv).Value.ToString
                End If

                If Not .Item("Desayuno", bytFilaDgv).Value Is Nothing Then
                    chkDesayuno.Checked = .Item("Desayuno", bytFilaDgv).Value
                End If
                If Not .Item("Lonche", bytFilaDgv).Value Is Nothing Then
                    chkLonche.Checked = .Item("Lonche", bytFilaDgv).Value
                End If
                If Not .Item("Almuerzo", bytFilaDgv).Value Is Nothing Then
                    chkAlmuerzo.Checked = .Item("Almuerzo", bytFilaDgv).Value
                End If
                If Not .Item("Cena", bytFilaDgv).Value Is Nothing Then
                    chkCena.Checked = .Item("Cena", bytFilaDgv).Value
                End If


                If Not .Item("TipoTransporte", bytFilaDgv).Value Is Nothing Then
                    cboTipoTransporte.SelectedValue = .Item("TipoTransporte", bytFilaDgv).Value.ToString
                End If

                Dim objUbig As New clsTablasApoyoBN.clsUbigeoBN
                If Not .Item("IDUbigeoOri", bytFilaDgv).Value Is Nothing Then
                    cboPaisOrig.SelectedValue = objUbig.strDevuelveIDPaisxCiudad(.Item("IDUbigeoOri", bytFilaDgv).Value.ToString)
                    cboPaisOrig_SelectionChangeCommitted(Nothing, Nothing)
                    cboIDUbigeoOri.SelectedValue = .Item("IDUbigeoOri", bytFilaDgv).Value.ToString
                End If

                If Not .Item("IDUbigeoDes", bytFilaDgv).Value Is Nothing Then
                    cboPaisDest.SelectedValue = objUbig.strDevuelveIDPaisxCiudad(.Item("IDUbigeoDes", bytFilaDgv).Value.ToString)
                    cboPaisDest_SelectionChangeCommitted(Nothing, Nothing)
                    cboIDUbigeoDes.SelectedValue = .Item("IDUbigeoDes", bytFilaDgv).Value.ToString
                End If

                If Not .Item("SSCL", bytFilaDgv).Value Is Nothing Then
                    txtSSCL.Text = .Item("SSCL", bytFilaDgv).Value.ToString
                    lblSSCL2.Text = .Item("SSCL", bytFilaDgv).Value.ToString
                End If
                If Not .Item("SSCR", bytFilaDgv).Value Is Nothing Then
                    txtSSCR.Text = Convert.ToDouble(.Item("SSCR", bytFilaDgv).Value.ToString).ToString("##,##0.0000")
                    lblSSCR2.Text = Convert.ToDouble(.Item("SSCR", bytFilaDgv).Value.ToString).ToString("##,##0.0000")
                    txtSSCR2.Text = Convert.ToDouble(.Item("SSCR", bytFilaDgv).Value.ToString).ToString("##,##0.0000")
                End If
                If Not .Item("SSMargen", bytFilaDgv).Value Is Nothing Then
                    txtSSMargen.Text = .Item("SSMargen", bytFilaDgv).Value.ToString
                    lblSSMargen2.Text = .Item("SSMargen", bytFilaDgv).Value.ToString
                End If
                If Not .Item("SSMargenLiberado", bytFilaDgv).Value Is Nothing Then
                    txtSSMargenLiberado.Text = .Item("SSMargenLiberado", bytFilaDgv).Value.ToString
                    lblSSMargenLiberado2.Text = .Item("SSMargenLiberado", bytFilaDgv).Value.ToString
                End If
                If Not .Item("SSTotal", bytFilaDgv).Value Is Nothing Then
                    txtSSTotal.Text = .Item("SSTotal", bytFilaDgv).Value.ToString
                    lblSSTotal2.Text = .Item("SSTotal", bytFilaDgv).Value.ToString
                End If
                If Not .Item("STCR", bytFilaDgv).Value Is Nothing Then
                    txtSTCR.Text = Convert.ToDouble(.Item("STCR", bytFilaDgv).Value.ToString).ToString("##,##0.0000")
                    lblSTCR2.Text = Convert.ToDouble(.Item("STCR", bytFilaDgv).Value.ToString).ToString("##,##0.0000")
                    txtSTCR2.Text = Convert.ToDouble(.Item("STCR", bytFilaDgv).Value.ToString).ToString("##,##0.0000")
                End If


                If Not .Item("STMargen", bytFilaDgv).Value Is Nothing Then
                    txtSTMargen.Text = .Item("STMargen", bytFilaDgv).Value.ToString
                    lblSTMargen2.Text = .Item("STMargen", bytFilaDgv).Value.ToString
                End If
                If Not .Item("STTotal", bytFilaDgv).Value Is Nothing Then
                    txtSTTotal.Text = .Item("STTotal", bytFilaDgv).Value.ToString
                    lblSTTotal2.Text = .Item("STTotal", bytFilaDgv).Value.ToString
                End If
                If Not .Item("Total", bytFilaDgv).Value Is Nothing Then
                    lblTotal.Text = Convert.ToDouble(.Item("Total", bytFilaDgv).Value.ToString).ToString("##,##0.00")
                    lblTotal2.Text = .Item("Total", bytFilaDgv).Value.ToString
                End If
                If Not .Item("RedondeoTotal", bytFilaDgv).Value Is Nothing Then
                    lblRedondeoTotal.Text = .Item("RedondeoTotal", bytFilaDgv).Value.ToString
                    lblRedondeoTotal2.Text = .Item("RedondeoTotal", bytFilaDgv).Value.ToString
                End If
                If Not .Item("CostoLiberadoImpto", bytFilaDgv).Value Is Nothing Then
                    lblCostoLiberadoImpto.Text = .Item("CostoLiberadoImpto", bytFilaDgv).Value.ToString
                    lblCostoLiberadoImpto2.Text = .Item("CostoLiberadoImpto", bytFilaDgv).Value.ToString
                End If
                If Not .Item("CostoRealImpto", bytFilaDgv).Value Is Nothing Then
                    txtCostoRealImpto.Text = .Item("CostoRealImpto", bytFilaDgv).Value.ToString
                    lblCostoRealImpto2.Text = .Item("CostoRealImpto", bytFilaDgv).Value.ToString
                End If
                If Not .Item("MargenImpto", bytFilaDgv).Value Is Nothing Then
                    lblMargenImpto.Text = .Item("MargenImpto", bytFilaDgv).Value.ToString
                    lblMargenImpto2.Text = .Item("MargenImpto", bytFilaDgv).Value.ToString
                End If
                If Not .Item("MargenLiberadoImpto", bytFilaDgv).Value Is Nothing Then
                    lblMargenLiberadoImpto.Text = .Item("MargenLiberadoImpto", bytFilaDgv).Value.ToString
                    lblMargenLiberadoImpto2.Text = .Item("MargenLiberadoImpto", bytFilaDgv).Value.ToString
                End If
                If Not .Item("SSCLImpto", bytFilaDgv).Value Is Nothing Then
                    lblSSCLImpto.Text = .Item("SSCLImpto", bytFilaDgv).Value.ToString
                    lblSSCLImpto2.Text = .Item("SSCLImpto", bytFilaDgv).Value.ToString
                End If

                If Not .Item("SSCRImpto", bytFilaDgv).Value Is Nothing Then
                    lblSSCRImpto.Text = .Item("SSCRImpto", bytFilaDgv).Value.ToString
                    lblSSCRImpto2.Text = .Item("SSCRImpto", bytFilaDgv).Value.ToString
                End If
                If Not .Item("SSMargenImpto", bytFilaDgv).Value Is Nothing Then
                    lblSSMargenImpto.Text = .Item("SSMargenImpto", bytFilaDgv).Value.ToString
                    lblSSMargenImpto2.Text = .Item("SSMargenImpto", bytFilaDgv).Value.ToString
                End If
                If Not .Item("SSMargenLiberadoImpto", bytFilaDgv).Value Is Nothing Then
                    lblSSMargenLiberadoImpto.Text = .Item("SSMargenLiberadoImpto", bytFilaDgv).Value.ToString
                    lblSSMargenLiberadoImpto2.Text = .Item("SSMargenLiberadoImpto", bytFilaDgv).Value.ToString
                End If
                If Not .Item("STCRImpto", bytFilaDgv).Value Is Nothing Then
                    lblSTCRImpto.Text = .Item("STCRImpto", bytFilaDgv).Value.ToString
                    lblSTCRImpto2.Text = .Item("STCRImpto", bytFilaDgv).Value.ToString
                End If
                If Not .Item("STMargenImpto", bytFilaDgv).Value Is Nothing Then
                    lblSTMargenImpto.Text = .Item("STMargenImpto", bytFilaDgv).Value.ToString
                    lblSTMargenImpto2.Text = .Item("STMargenImpto", bytFilaDgv).Value.ToString
                End If

                If Not .Item("TotImpto", bytFilaDgv).Value Is Nothing Then
                    lblTotImpto.Text = Convert.ToDouble(.Item("TotImpto", bytFilaDgv).Value.ToString).ToString("##,##0.0000")
                    lblTotImpto2.Text = .Item("TotImpto", bytFilaDgv).Value.ToString
                End If


                If Not .Item("Especial", bytFilaDgv).Value Is Nothing Then
                    chkEspecial.Checked = .Item("Especial", bytFilaDgv).Value
                End If
                If Not .Item("IDIdioma", bytFilaDgv).Value Is Nothing Then
                    cboIDIdioma.SelectedValue = .Item("IDIdioma", bytFilaDgv).Value.ToString
                End If

                If Not .Item("Servicio", bytFilaDgv).Value Is Nothing Then
                    txtServicio.Text = .Item("Servicio", bytFilaDgv).Value.ToString
                End If

                If Not .Item("IDDetRel", bytFilaDgv).Value Is Nothing Then
                    If .Item("IDDetRel", bytFilaDgv).Value.ToString = "0" Then
                        strIDDetRel = ""
                    Else
                        strIDDetRel = .Item("IDDetRel", bytFilaDgv).Value.ToString
                    End If

                End If
                strServicioOrig1erdia = txtServicio.Text
                If Not .Item("NroLiberados", bytFilaDgv).Value Is Nothing Then
                    txtNroLiberados.Text = .Item("NroLiberados", bytFilaDgv).Value.ToString
                End If
                If Not .Item("Tipo_Lib", bytFilaDgv).Value Is Nothing Then
                    cboTipoLib.SelectedValue = .Item("Tipo_Lib", bytFilaDgv).Value.ToString
                End If

                If Not .Item("TipoTriple", bytFilaDgv).Value Is Nothing Then
                    strDescripcionTipoTriple = .Item("TipoTriple", bytFilaDgv).Value.ToString
                End If

                blnCostoRealEditado = .Item("CostoRealEditado", bytFilaDgv).Value.ToString
                blnCostoRealImptoEditado = .Item("CostoRealImptoEditado", bytFilaDgv).Value.ToString
                blnMargenAplicadoEditado = .Item("MargenAplicadoEditado", bytFilaDgv).Value.ToString
                blnServicioEditado = .Item("ServicioEditado", bytFilaDgv).Value.ToString
                blnSSCREditado = .Item("FlSSCREditado", bytFilaDgv).Value.ToString
                blnSTCREditado = .Item("FlSTCREditado", bytFilaDgv).Value.ToString

                ColorearTextoCamposEditados()
                If blnCostoRealEditado = True Or blnCostoRealImptoEditado = True Or _
                    blnMargenAplicadoEditado = True Or blnServicioEditado = True Or _
                    blnSSCREditado = True Or blnSTCREditado = True Then
                    chkEspecial.Enabled = False
                End If

                'dblCostoRealOrigFijo = .Item("CostoRealAnt", bytFilaDgv).Value.ToString
                'dblMargenAplicadoOrigFijo = .Item("MargenAplicadoAnt", bytFilaDgv).Value.ToString
                CargarToolTip(.Item("CostoRealAnt", bytFilaDgv).Value.ToString, .Item("MargenAplicadoAnt", bytFilaDgv).Value.ToString)


                CargarQuiebres()

                'If Not .Item("ConAlojamiento", bytFilaDgv).Value Is Nothing Then
                '    If cboTipoProv.SelectedValue = gstrTipoProveeOperadores And CBool(.Item("ConAlojamiento", bytFilaDgv).Value) Then
                '        chkIncGuia.Visible = True
                '    End If
                'End If

                If Not .Item("IncGuia", bytFilaDgv).Value Is Nothing Then
                    chkIncGuia.Checked = .Item("IncGuia", bytFilaDgv).Value
                End If
                If Not .Item("FlTarifaCambiada", bytFilaDgv).Value Is Nothing Then
                    blnRecalTarifaPendiente = .Item("FlTarifaCambiada", bytFilaDgv).Value
                End If
                If Not .Item("AcomodoEspecial", bytFilaDgv).Value Is Nothing Then
                    blnHabilitarAcomodoEspecial = False
                    chkAcomodoEspecial.Checked = .Item("AcomodoEspecial", bytFilaDgv).Value
                    blnHabilitarAcomodoEspecial = True
                End If
                blnContieneAcomodoEsp = chkAcomodoEspecial.Checked

                CargarPaxLvw()

                If intIDDet = 0 Then
                    If strIDDetRel = "" Then
                        If Not .Item("NroPax", bytFilaDgv).Value Is Nothing Then
                            'txtNroPax.Text = .Item("NroPax", bytFilaDgv).Value.ToString
                            'txtNroPax.Text = Convert.ToInt16(.Item("NroPax", bytFilaDgv).Value.ToString) _
                            '    - Convert.ToInt16(If(.Item("NroLiberados", bytFilaDgv).Value.ToString = "", "0", .Item("NroLiberados", bytFilaDgv).Value.ToString))
                        End If
                    Else
                        If strIDDetRel.Substring(0, 1) <> "H" Then
                            If Not .Item("NroPax", bytFilaDgv).Value Is Nothing Then
                                txtNroPax.Text = .Item("NroPax", bytFilaDgv).Value.ToString
                                'txtNroPax.Text = Convert.ToInt16(.Item("NroPax", bytFilaDgv).Value.ToString) _
                                '    - Convert.ToInt16(If(.Item("NroLiberados", bytFilaDgv).Value.ToString = "", "0", .Item("NroLiberados", bytFilaDgv).Value.ToString))
                            End If
                        Else
                            If chkIncGuia.Checked = False Then
                                'txtNroPax.Text = lvwPax.CheckedItems.Count
                                txtNroPax.Text = lvwPax.CheckedItems.Count - Convert.ToInt16(If(txtNroLiberados.Text = "", "0", txtNroLiberados.Text))
                            End If
                            'GroupBox1.Enabled = False
                            pEnableControlsGroupBox(grbDatosGenerales, False)
                            'pEnableControlsGroupBox(grbPax, False)
                            'txtServicio.Enabled = True
                            'btnAceptar.Enabled = False
                        End If
                    End If
                Else
                    'If strIDDetRel <> "R1" Then
                    'Else
                    'txtNroPax.Text = lvwPax.CheckedItems.Count
                    'End If
                    If strIDDetRel <> "" Then
                        'GroupBox1.Enabled = False
                        pEnableControlsGroupBox(grbDatosGenerales, False)
                        pEnableControlsGroupBox(grbPax, False)
                        'txtServicio.Enabled = True
                        'btnAceptar.Enabled = False
                        If chkIncGuia.Checked = False Then
                            'txtNroPax.Text = lvwPax.CheckedItems.Count
                            txtNroPax.Text = lvwPax.CheckedItems.Count - Convert.ToInt16(If(txtNroLiberados.Text = "", "0", txtNroLiberados.Text))
                        End If
                    Else
                        If Not .Item("NroPax", bytFilaDgv).Value Is Nothing Then
                            txtNroPax.Text = .Item("NroPax", bytFilaDgv).Value.ToString
                        End If


                    End If
                End If
                If chkIncGuia.Checked = False Then
                    'txtNroPax.Text = lvwPax.CheckedItems.Count
                    txtNroPax.Text = lvwPax.CheckedItems.Count - Convert.ToInt16(If(txtNroLiberados.Text = "", "0", txtNroLiberados.Text))

                    'If txtNroPax.Text <> intPax Then
                    If lvwPax.Items.Count <> lvwPax.CheckedItems.Count Then
                        chkSelPax.Checked = True
                    End If
                Else
                    chkSelPax.Enabled = False
                End If

                'If Not .Item("Especial", bytFilaDgv).Value Is Nothing Then
                '    If .Item("Especial", bytFilaDgv).Value = True Then lvwPax.Enabled = False
                'End If

                If chkTransfer.Checked = True Then
                    CargarCombosTransferOrigenDestino(frmRef, dtpDia.Text, _
                                                      cboOrigen, cboDestino, intIDCab)

                    'If Not .Item("IDDetTransferOri", bytFilaDgv).Value Is Nothing Then
                    '    cboOrigen.SelectedValue = .Item("IDDetTransferOri", bytFilaDgv).Value.ToString
                    'End If
                    'If Not .Item("IDDetTransferDes", bytFilaDgv).Value Is Nothing Then
                    '    cboDestino.SelectedValue = .Item("IDDetTransferDes", bytFilaDgv).Value.ToString
                    'End If
                    Dim intIDDetTransferOri As Integer = 0, intIDDetTransferDes As Integer = 0
                    If Not .Item("IDDetTransferOri", bytFilaDgv).Value Is Nothing Then
                        intIDDetTransferOri = .Item("IDDetTransferOri", bytFilaDgv).Value.ToString
                    End If
                    If Not .Item("IDDetTransferDes", bytFilaDgv).Value Is Nothing Then
                        intIDDetTransferDes = .Item("IDDetTransferDes", bytFilaDgv).Value.ToString
                    End If
                    If Not (intIDDetTransferOri = 0 And intIDDetTransferDes = 0) Then

                        cboOrigen.SelectedValue = intIDDetTransferOri
                        cboDestino.SelectedValue = intIDDetTransferDes

                    End If

                    If frmRef.dgvDet.CurrentRow.Index = 0 Then
                        If chkEspecial.Checked Then
                            txtServicio.Text = .Item("DescServicioDet", bytFilaDgv).Value.ToString
                        Else
                            cboOrigen_SelectionChangeCommitted(cboOrigen, Nothing)
                        End If
                    End If

                End If

                If Not .Item("FlAcomodosVehic", bytFilaDgv).Value Is Nothing Then
                    chkAcomVehi.Checked = .Item("FlAcomodosVehic", bytFilaDgv).Value
                End If

            End With



            CargarDgvTextosIdioma2()
            'CargarDgvTextosIdioma()


            If (strIDTipoOper = "E" And strIDTipoProv <> gstrTipoProveeHoteles) Or (strIDTipoOper = "I" And strIDTipoProv = gstrTipoProveeOperadores) Then
                chkAcomVehi.Visible = True
                If strIDTipoOper = "E" Then
                    grbAcomVehicE.Visible = True
                    grbAcomVehicI.Visible = False
                    lblEtiqAcomVehPax.Top = 85
                    tvwAcoVeh.Top = 100
                ElseIf strIDTipoOper = "I" Then
                    grbAcomVehicE.Visible = False
                    grbAcomVehicI.Visible = True

                End If
            End If

        Catch ex As Exception
            Throw
        End Try
    End Sub

    Private Sub CrearListView()

        With lvwPax
            .View = View.Details
            .FullRowSelect = True
            '.GridLines = True
            .LabelEdit = False
            .HideSelection = False

            .Columns.Clear()

            .Columns.Add("Apellidos", 180, HorizontalAlignment.Left)
            .Columns.Add("Nombres", 180, HorizontalAlignment.Left)
            .Columns.Add("Nro. Docum", 100, HorizontalAlignment.Left)
            .Columns.Add("IDPax", 0, HorizontalAlignment.Left)
            .Columns.Add("IDNacionalidad", 0, HorizontalAlignment.Left)
            .Columns.Add("Residente", 0, HorizontalAlignment.Left)

        End With

    End Sub

    'Private Sub CrearListViewPaxAcoVeh()

    '    With lvwPaxAcoVeh
    '        .View = View.Details
    '        .FullRowSelect = True
    '        '.GridLines = True
    '        .LabelEdit = False
    '        .HideSelection = False

    '        .Columns.Clear()

    '        .Columns.Add("Apellidos", 180, HorizontalAlignment.Left)
    '        .Columns.Add("Nombres", 180, HorizontalAlignment.Left)
    '        .Columns.Add("Nro. Docum", 100, HorizontalAlignment.Left)
    '        .Columns.Add("IDPax", 0, HorizontalAlignment.Left)
    '        .Columns.Add("IDNacionalidad", 0, HorizontalAlignment.Left)
    '        .Columns.Add("Residente", 0, HorizontalAlignment.Left)

    '    End With



    'End Sub
    Private Function blnExisteSTCotDetPax() As Boolean
        Try
            Dim blnOk As Boolean = False
            For Each ST As stDetCotiPax In objLstDetCotiPax
                If ST.IDDet = strIDDet And ST.IDCAB = intIDCab Then
                    blnOk = True
                    Exit For
                End If
            Next
            Return blnOk
        Catch ex As Exception
            Throw
        End Try
    End Function

    Private Function blnExisteSTCotDetPax(ByVal vstrIDDet As String) As Boolean
        Try
            Dim blnOk As Boolean = False
            For Each ST As stDetCotiPax In objLstDetCotiPax
                If ST.IDDet = vstrIDDet And ST.IDCAB = intIDCab Then
                    blnOk = True
                    Exit For
                End If
            Next
            Return blnOk
        Catch ex As Exception
            Throw
        End Try
    End Function

    'Private Function blnExisteDgvCotDetPax(ByVal vstrIDDet As String) As Boolean
    '    If frmRef Is Nothing Then Exit Function
    '    Try
    '        Dim blnOk As Boolean = False
    '        For Each ItemDgv As DataGridViewRow In frmRef.dgvDet.Rows
    '            If ItemDgv.Cells("Iddet").Value = vstrIDDet Then
    '                blnOk = True
    '                Exit For
    '            End If
    '        Next
    '        Return blnOk
    '    Catch ex As Exception
    '        Throw
    '    End Try
    'End Function

    Private Function blnExisteSTReservaDetPax(ByVal vstrIDReservas_Det As String) As Boolean
        Try
            Dim blnOk As Boolean = False
            For Each ST As stDetReservPax In objLstDetReservPax
                If ST.IDCab = intIDCab And ST.IDReserva_Det = vstrIDReservas_Det Then
                    blnOk = True
                    Exit For
                End If
            Next
            Return blnOk
        Catch ex As Exception
            Throw
        End Try
    End Function

    Private Function blnExisteSTOperacionDetPax(ByVal vstrIDOperacion_Det As String) As Boolean
        Try
            Dim blnOk As Boolean = False
            For Each ST As stDetOperacPax In objLstDetOperacPax
                If ST.IDCab = intIDCab And ST.IDOperacion_Det = vstrIDOperacion_Det Then
                    blnOk = True
                    Exit For
                End If
            Next
            Return blnOk
        Catch ex As Exception
            Throw
        End Try
    End Function

    Private Sub CargarPaxLvwLect()
        Try
            Dim blnNoShow As Boolean = False
            If strModulo <> "C" Then blnNoShow = chkServicioNoShow.Checked

            Dim objBL As New clsCotizacionBN.clsCotizacionPaxBN
            Dim dt As New DataTable
            dt = objBL.ConsultarListxIDCabLvw(intIDCab, blnNoShow)
            'Dim blnEncontro As Boolean = False

            If dt.Rows.Count > 0 Then
                pCargaListView(lvwPax, dt, dt.Columns.Count - 1, 0)

                If strIDDet <> "" Then
                    Using dr As SqlClient.SqlDataReader = objBL.ConsultarDetxIDDetLvw(CInt(strIDDet), False)
                        If dr.HasRows Then
                            While dr.Read
                                For i As Byte = 0 To lvwPax.Items.Count - 1
                                    If lvwPax.Items(i).SubItems(3).Text = dr("IDPax") Then
                                        lvwPax.Items(i).Checked = True
                                        Exit For
                                    End If
                                Next
                            End While
                        End If
                        dr.Close()
                    End Using
                End If
            End If



        Catch ex As Exception
            Throw
        End Try
    End Sub


    Private Sub CargarPaxLvw()
        Try
            Dim blnNoShow As Boolean = False
            If strModulo <> "C" Then blnNoShow = chkServicioNoShow.Checked

            Dim objBL As New clsCotizacionBN.clsCotizacionPaxBN
            Dim dttPax As DataTable = objBL.ConsultarListxIDCabLvw(intIDCab, blnNoShow)

            If dttPax.Rows.Count > 0 Then

                pCargaListView(lvwPax, dttPax, dttPax.Columns.Count - 1, 0)

                'pCargaListView(lvwPaxAcoVeh, dttPax, dttPax.Columns.Count - 1, 0)
                'dttPaxAcVe = dt.Clone
                'For i As Int16 = 0 To dt.Rows.Count - 1
                '    dttPaxAcVe.ImportRow(dt.Rows(i))
                'Next


                Dim dr As SqlClient.SqlDataReader
                If strModulo = "C" Then
                    If intIDDet <> 0 Then
                        'Si mi Lista esta vacia que consulte a mi BD sino que cargue los objetos acumulados Mi Lista.
                        If Not blnExisteSTCotDetPax() Then
                            dr = objBL.ConsultarDetxIDDetLvw(intIDDet, False)
                            If dr.HasRows Then
                                blnActPax = False
                                While dr.Read
                                    For i As Byte = 0 To lvwPax.Items.Count - 1
                                        If lvwPax.Items(i).SubItems(3).Text = dr("IDPax") Then
                                            lvwPax.Items(i).Checked = True
                                            Exit For
                                        End If
                                    Next
                                End While
                                For i As Byte = 0 To lvwPax.Items.Count - 1
                                    AgregarEditarValorStrucDetCotiPax(i)
                                Next
                                blnActPax = True
                            End If
                            dr.Close()
                        Else
                            Dim i As Byte = 0
                            For Each ST As stDetCotiPax In objLstDetCotiPax
                                If strIDDet = ST.IDDet And intIDCab = ST.IDCAB Then
                                    If i <= lvwPax.Items.Count - 1 Then
                                        If lvwPax.Items(i).SubItems(3).Text = ST.IDPax And ST.Selecc = True Then
                                            lvwPax.Items(i).Checked = True
                                        End If
                                    End If
                                    i += 1
                                End If
                            Next
                        End If
                    Else
                        Dim i As Byte = 0
                        For Each ST As stDetCotiPax In objLstDetCotiPax
                            If strIDDet = ST.IDDet And intIDCab = ST.IDCAB Then
                                If i <= lvwPax.Items.Count - 1 Then
                                    If lvwPax.Items(i).SubItems(3).Text = ST.IDPax And ST.Selecc = True Then
                                        lvwPax.Items(i).Checked = True
                                    End If
                                End If
                                i += 1
                            End If
                        Next
                    End If
                ElseIf strModulo = "R" Then
                    If IsNumeric(intIDReserva_Det) Then
                        If Not blnExisteSTReservaDetPax(intIDReserva_Det) Then
                            Dim objResDetBN As New clsReservaBN.clsDetalleReservaBN
                            dr = objResDetBN.ConsultarDetPaxxIDReservaDetLvw(intIDReserva_Det, chkServicioNoShow.Checked)
                            If dr.HasRows Then
                                blnActPax = False
                                While dr.Read
                                    For i As Byte = 0 To lvwPax.Items.Count - 1
                                        If lvwPax.Items(i).SubItems(3).Text = dr("IDPax") Then
                                            lvwPax.Items(i).Checked = True
                                            Exit For
                                        End If
                                    Next
                                End While
                                blnActPax = True
                            End If
                            dr.Close()
                        End If
                    End If
                ElseIf strModulo = "O" Then
                    'Dim strIDOperacion_Det As String = frmRefRes.DgvDetalle.CurrentRow.Cells("IDOperacion_Det").Value.ToString

                    'If IsNumeric(strIDOperacion_Det) Then
                    '    If Not blnExisteSTOperacionDetPax(strIDOperacion_Det) Then
                    '        Dim objOpePaxBN As New clsPaxDetalleOperacionBN
                    '        dr = objOpePaxBN.ConsultarDetPaxxIDOperacionDetLvw(strIDOperacion_Det, chkServicioNoShow.Checked)
                    '        If dr.HasRows Then
                    '            blnActPax = False
                    '            While dr.Read
                    '                For i As Byte = 0 To lvwPax.Items.Count - 1
                    '                    If lvwPax.Items(i).SubItems(3).Text = dr("IDPax") Then
                    '                        lvwPax.Items(i).Checked = True
                    '                        Exit For
                    '                    End If
                    '                Next
                    '            End While
                    '            blnActPax = True
                    '        End If
                    '        dr.Close()
                    '    End If
                    'End If

                End If
            End If


            'Cargar los nuevos Pax agregados en edición
            If chkIncGuia.Checked = False Then
                If intNroPaxDif <> 0 Then
                    CargarNuevosPaxDif()
                Else
                    If intPax <> lvwPax.Items.Count And lvwPax.Items.Count > 0 Then
                        intNroPaxDif = intPax - lvwPax.Items.Count
                        CargarNuevosPaxDif()
                    End If
                End If
            End If



            If strModoEdicion = "E" Then

                If strModulo = "C" Then
                    PintaChecksLvwDetCotiPaxST()
                ElseIf strModulo = "R" Then
                    PintaChecksLvwDetReservPaxST()
                ElseIf strModulo = "O" Then
                    PintaChecksLvwDetOperacPaxST()
                End If
                'Else
                '    If Not frmRefRes Is Nothing Then
                '        If strDescServicioaCambiar <> "" Then
                '            PintaChecksLvwDetReservPaxCambio()
                '        End If
                '    End If
            End If

            MarcarchkSelTodosPax()


        Catch ex As Exception
            Throw
        End Try
    End Sub
    Private Sub MarcarchkSelTodosPax()
        blnReCalculoCheckSelTodos = False
        If lvwPax.CheckedItems.Count = 0 Then
            chkSelTodosPax.Checked = False
        Else
            chkSelTodosPax.Checked = True
        End If
        blnReCalculoCheckSelTodos = True
    End Sub
    Private Sub CargarNuevosPaxDif()
        Try

            If intNroPaxDifGrd > 0 Then
AgregarPax:
                Dim dttNuevosPax As New DataTable("NuevosPax")

                With dttNuevosPax
                    .Columns.Add("Nombres", GetType(String))
                    .Columns.Add("Apellidos", GetType(String))
                    .Columns.Add("NumIdentidad", GetType(String))
                    .Columns.Add("IDPax", GetType(String))
                    .Columns.Add("IDNacionalidad", GetType(String))
                End With

                Dim bytTotalPaxLvw As Byte = lvwPax.Items.Count
                Dim intHasta As Int16 = (Convert.ToInt16(frmRef.txtNroPax.Text) + Convert.ToInt16(If(txtNroLiberados.Text = "", "0", txtNroLiberados.Text))) - bytTotalPaxLvw
                Dim intNroPaxSel As Int16 = lvwPax.CheckedItems.Count

                If bytTotalPaxLvw <> intNroPaxSel Then Exit Sub


                Dim blnEntraFor As Boolean = False
                For bytIndPaxDif As Int16 = 1 To intHasta
                    dttNuevosPax.Rows.Add("Pax " & (bytTotalPaxLvw + bytIndPaxDif).ToString, _
                                          "Pax " & (bytTotalPaxLvw + bytIndPaxDif).ToString, _
                                          "", _
                                          "M" & (bytTotalPaxLvw + bytIndPaxDif).ToString, "")

                    blnEntraFor = True
                Next

                If Not blnEntraFor Then ' And intHasta >= 1 Then
                    GoTo QuitarPax
                End If



                pCargaListView(lvwPax, dttNuevosPax, dttNuevosPax.Columns.Count - 1, 0, False)
                For i As Int16 = 1 To intHasta
                    'If intNroPaxSel + intNroPaxDif = Convert.ToInt16(frmRef.txtNroPax.Text) Then
                    lvwPax.Items((bytTotalPaxLvw + i) - 1).Checked = True
                    'Else
                    'lvwPax.Items((bytTotalPaxLvw + i) - 1).Checked = False
                    'End If


                    lvwPax_ItemChecked(Nothing, Nothing)

                Next
            ElseIf intNroPaxDifGrd < 0 Then
QuitarPax:
                Dim bytTotalPaxLvw As Byte = Convert.ToInt16(frmRef.txtNroPax.Text)
                Dim intDesde As Int16 = bytTotalPaxLvw
                Dim intCantNodos As Byte = lvwPax.Items.Count
                'For i As Int16 = lvwPax.Items.Count - 1 To (lvwPax.Items.Count + intNroPaxDif) Step -1
                'For i As Int16 = intDesde To ((intDesde + intNroPaxDifGrd) + 1) Step -1
                Dim ListItemDel As New List(Of Int16)

                Dim intNroPaxSel As Int16 = lvwPax.CheckedItems.Count
                'Dim intDif As Int16 = Math.Abs((intNroPaxSel + intNroPaxDif) - Convert.ToInt16(frmRef.txtNroPax.Text))
                Dim intDif As Int16
                'If Convert.ToInt16(frmRef.txtNroPax.Text) = intPax Then

                'intDif = Math.Abs((intNroPaxSel + intNroPaxDif) - Convert.ToInt16(frmRef.txtNroPax.Text))
                'Else

                intDif = ((Convert.ToInt16(frmRef.txtNroPax.Text) - intPax))

                If intDif < 0 Then
                    If (intPax + intDif) > lvwPax.Items.Count Then GoTo AgregarPax
                End If

                'End If


                For i As Int16 = intCantNodos - 1 To (intCantNodos + intNroPaxDif) - 1 Step -1
                    lvwPax.Items(i).Checked = False
                    lvwPax_ItemChecked(Nothing, Nothing)

                    'lvwPax.Items(i).ForeColor = Color.Gray
                    'lvwPax.Items(i).Remove()
                    ListItemDel.Add(i)
                Next

                'For i As Int16 = intDesde To (((intDesde + Math.Abs(intNroPaxDifGrd)) + 1) - 2) - intDif
                '    If intCantNodos >= i Then

                '        lvwPax.Items(i).Checked = False
                '        lvwPax_ItemChecked(Nothing, Nothing)


                '        ListItemDel.Add(i)
                '    End If
                'Next


                'For intItemDel As Int16 = (ListItemDel.Count + intDesde) - 1 To intDesde Step -1

                Dim intDif2 As Int16 = intCantNodos - (intCantNodos - (Convert.ToInt16(frmRef.txtNroPax.Text) + Convert.ToInt16(If(frmRef.txtNroLiberados.Text = "", "0", frmRef.txtNroLiberados.Text))))
                For intItemDel As Int16 = (intCantNodos - 1) To intDif2 Step -1


                    lvwPax.Items(intItemDel).Remove()

                Next
            End If



        Catch ex As Exception
            Throw
        End Try
    End Sub
    Private Sub CargarQuiebres()
        Try
            Dim objBN As New clsCotizacionBN.clsDetalleCotizacionBN.clsDetalleCotizacionQuiebresBN
            Dim dtt As DataTable = objBN.ConsultarListxIDDet(intIDDet)

            For Each dr As DataRow In dtt.Rows

                dgvQui.Rows.Add(dr("IDCAB_Q"), dr("IDDET_Q"), "E", _
                dr("Pax"), dr("Liberados"), dr("Tipo_Lib"), _
                dr("CostoReal"), dr("CostoRealImpto"), dr("Margen"), dr("MargenImpto"), _
                dr("CostoLiberado"), dr("CostoLiberadoImpto"), _
                dr("MargenLiberado"), dr("MargenLiberadoImpto"), dr("MargenAplicado"), _
                dr("Total"), dr("RedondeoTotal"), dr("SSCR"), dr("SSCRImpto"), dr("SSMargen"), dr("SSMargenImpto"), _
                dr("SSCL"), dr("SSCLImpto"), _
                dr("SSMargenLiberado"), dr("SSMargenLiberadoImpto"), dr("SSTotal"), _
                dr("STCR"), dr("STCRImpto"), dr("STMargen"), dr("STMargenImpto"), dr("STTotal"), dr("TotImpto"))

            Next
            dgvQui_CellEnter(Nothing, Nothing)
        Catch ex As Exception
            Throw
        End Try
    End Sub

    Private Sub CargarQuiebresLect()
        Try
            Dim objBN As New clsCotizacionBN.clsDetalleCotizacionBN.clsDetalleCotizacionQuiebresBN
            Dim dtt As DataTable = objBN.ConsultarListxIDDet(CInt(strIDDet))

            For Each dr As DataRow In dtt.Rows

                dgvQui.Rows.Add(dr("IDCAB_Q"), dr("IDDET_Q"), "E", _
                dr("Pax"), dr("Liberados"), dr("Tipo_Lib"), _
                dr("CostoReal"), dr("CostoRealImpto"), dr("Margen"), dr("MargenImpto"), _
                dr("CostoLiberado"), dr("CostoLiberadoImpto"), _
                dr("MargenLiberado"), dr("MargenLiberadoImpto"), dr("MargenAplicado"), _
                dr("Total"), dr("RedondeoTotal"), dr("SSCR"), dr("SSCRImpto"), dr("SSMargen"), dr("SSMargenImpto"), _
                dr("SSCL"), dr("SSCLImpto"), _
                dr("SSMargenLiberado"), dr("SSMargenLiberadoImpto"), dr("SSTotal"), _
                dr("STCR"), dr("STCRImpto"), dr("STMargen"), dr("STMargenImpto"), dr("STTotal"), dr("TotImpto"))

            Next
            dgvQui_CellEnter(Nothing, Nothing)
        Catch ex As Exception
            Throw
        End Try
    End Sub

    'Private Sub dgvIdiom_CellEndEdit(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvIdiom.CellEndEdit
    '    Try
    '        If Not e Is Nothing Then
    '            If e.RowIndex < 0 Then Exit Sub
    '            If e.ColumnIndex < 0 Then Exit Sub

    '            AgregarEditarValorStrucTextoIdioma(e.RowIndex, e.ColumnIndex, "")
    '        Else
    '            If Not dgvIdiom.CurrentCell Is Nothing Then
    '                AgregarEditarValorStrucTextoIdioma(e.RowIndex, e.ColumnIndex, "")
    '            End If
    '        End If
    '    Catch ex As Exception
    '        MessageBox.Show(ex.Message, ex.Source, MessageBoxButtons.OK, MessageBoxIcon.Error)
    '    End Try


    'End Sub

    Public Sub btnAceptar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAceptar.Click
        Try
            If Not blnValidarIngresos() Then Exit Sub
            CopiarDetPaxaHijos()

            If strModoEdicion = "N" Then
                If strModulo = "C" Then
                    Grabar()
                ElseIf strModulo = "R" Or strModulo = "O" Then
                    GrabarReserva()
                End If

                'MessageBox.Show("Se creó el detalle exitosamente", gstrTituloMsg, MessageBoxButtons.OK, MessageBoxIcon.Information)


            ElseIf strModoEdicion = "E" Then
                ProcesarAcomodosEspeciales()

                If strModulo = "C" Then
                    Actualizar()
                ElseIf strModulo = "R" Or strModulo = "O" Then
                    ActualizarReserva()
                End If

                'MessageBox.Show("Se actualizó el detalle exitosamente", gstrTituloMsg, MessageBoxButtons.OK, MessageBoxIcon.Information)

            End If

            If strModulo = "C" Then
                frmRef.blnActualizoDetalle = True
            ElseIf strModulo = "R" Or strModulo = "O" Then
                'frmRefRes.blnActualizoDetalle = True
            End If

            blnCambios = False

            If blnSelecServxServ Then Me.Close()
            'Me.Close()
        Catch ex As Exception
            MessageBox.Show(ex.Message, ex.Source, MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub btnSalir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSalir.Click
        Me.Close()
    End Sub
    Private Sub frmMantBancosDato_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        Select Case e.KeyCode
            Case Keys.F6
                If btnAceptar.Enabled Then
                    txtCostoReal_LostFocus(Nothing, Nothing)
                    'txtCostoRealQ_LostFocus(Nothing, Nothing)
                    txtNroPax_LostFocus(Nothing, Nothing)

                    btnAceptar_Click(Nothing, Nothing)
                End If

            Case Keys.Escape
                btnSalir_Click(Nothing, Nothing)
        End Select
    End Sub

    Private Function blnValidarGuiaIngresosAcomodosEspecialxListaPaxSeleccion() As Boolean
        If Not blnTieneSubServiciosGuia Then Return True
        Try
            Dim blnSeMarcoGuia As Boolean = False
            For Each ST As clsCotizacionBE.clsAcomodoVehiculoIntDetPaxBE In ListaDetAcomVehiIntDetPax
                If ST.IDDet = strIDDet And ST.FlGuia = True Then
                    blnSeMarcoGuia = True
                    Exit For
                End If
            Next

            Return blnSeMarcoGuia

        Catch ex As Exception
            Throw
        End Try
    End Function

    Private Function blnValidarIngresosAcomodosEspecialxListaPaxSeleccion() As Boolean
        Try
            Dim bytCantidadPaxSeleccionados As Byte = 0
            For i As Int16 = 0 To lvwPax.Items.Count - 1
                If lvwPax.Items(i).Checked Then
                    bytCantidadPaxSeleccionados += 1
                End If
            Next

            Dim bytCantidaPax As Byte = 0
            For Each ST As stAcodomodoEspecial In objLstAcodomodoEspecial
                If ST.IDDet = strIDDet And ST.IDCab = intIDCab And ST.Accion <> "B" Then
                    bytCantidaPax += (ST.QtPax * bytDevuelveCapacidadxCoCapacidad(ST.CoCapacidad))
                End If
            Next

            If bytCantidadPaxSeleccionados <> bytCantidaPax Then
                Return False
            Else
                Return True
            End If
        Catch ex As Exception
            Throw
        End Try
    End Function

    Private Function bytDevuelveCapacidadxCoCapacidad(ByVal vstrCoCapacidad As String) As Byte
        Try
            Dim bytCapacidad As Byte = 0
            Using objBL As New clsTablasApoyoBN.clsCapacidadHabitacionBN
                Using dr As SqlClient.SqlDataReader = objBL.ConsultarPk(vstrCoCapacidad)
                    dr.Read()
                    If dr.HasRows Then
                        bytCapacidad = dr("QtCapacidad")
                    End If
                    dr.Close()
                End Using
            End Using
            Return bytCapacidad
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Function blnValidarIngresos() As Boolean

        Dim blnOk As Boolean = True


        If cboCiudad.Text = "" Then
            ErrPrv.SetError(cboCiudad, "Debe seleccionar ciudad")
            blnOk = False
        End If

        If cboTipoProv.Text = "" Then
            ErrPrv.SetError(cboTipoProv, "Debe seleccionar Tipo de Proveedor")
            blnOk = False
        End If

        If lblProveedor.Text = "" Then
            ErrPrv.SetError(btnProveedor, "Debe seleccionar Proveedor")
            blnOk = False
        End If

        If strModulo = "C" Then

            If blnOk Then
                If frmRef.cboEstado.SelectedValue = "A" Then
                    Dim objBN As New clsCotizacionBN.clsDetalleCotizacionBN
                    If intIDDet > 0 Then
                        If objBN.strConsultarEstadoReserva(intIDDet) = "RR" Then
                            MessageBox.Show("No se permite editar el servicio, ya que tiene Reserva en estado RR.", gstrTituloMsg, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                            Return False
                        End If
                    Else
                        If objBN.strConsultarEstadoReserva(intIDCab, strIDProveedor) = "RR" Then
                            strMensajeValidacNuevoServ = "No se permite ingresar el servicio, ya que tiene Reserva en estado RR."
                            If strModoEdicion <> "N" Or blnServicioVarios Then
                                MessageBox.Show(strMensajeValidacNuevoServ, gstrTituloMsg, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                                blnSelecServxServ = False
                            End If
                            Return False
                        End If
                    End If

                End If
            End If

            'If dtpDia.Value < frmRef.dtpFecInicio.Value Then
            'ErrPrv.SetError(dtpDia, "Debe ingresar Fecha mayor o igual a " & frmRef.dtpFecInicio.Value.ToString)
            'blnOk = False
            'Else
            'If strIDServicio <> "" And cboTipoProv.SelectedValue.ToString <> gstrTipoProveeRestaurantes And cboTipoProv.SelectedValue.ToString <> gstrTipoProveeHoteles Then
            If strIDServicio <> "" And cboTipoProv.SelectedValue.ToString <> gstrTipoProveeHoteles Then
                Dim objBN As New clsCotizacionBN.clsDetalleCotizacionBN
                If strModoEdicion = "N" Then
                    If Not objBN.blnVerificaHorarioServicio(strIDServicio, dtpDia.Text & " " & dtpDiaHora.Text.Trim, True) Then
                        blnFueradeHorario = True
                    End If
                ElseIf strModoEdicion = "E" Then
                    If Not objBN.blnVerificaHorarioServicio(strIDServicio, dtpDia.Text & " " & dtpDiaHora.Text.Trim, True) Then
                        blnFueradeHorario = True
                    Else
                        If Not objBN.blnVerificaHorarioServicio(strIDServicio, dtpDia.Text & " " & dtpDiaHora.Text.Trim) Then
                            blnFueradeHorario = True
                        End If
                    End If
                End If
            End If
            'End If

            blnOk = blnValidarAcomodoVehiculo()
            If blnOk Then
                blnOk = blnValidarPaxAcomodoVehi()
            End If

        ElseIf strModulo = "R" Or strModulo = "O" Then

            'If dtpDia.Value < frmRefRes.dtpFecInicio.Value Then
            'ErrPrv.SetError(dtpDia, "Debe ingresar Fecha mayor o igual a " & frmRefRes.dtpFecInicio.Value.ToString)
            'blnOk = False
            'Else
            If strIDServicio <> "" And cboTipoProv.SelectedValue.ToString <> gstrTipoProveeHoteles Then
                Dim objBN As New clsCotizacionBN.clsDetalleCotizacionBN
                If strModoEdicion = "N" Then
                    If Not objBN.blnVerificaHorarioServicio(strIDServicio, dtpDia.Text & " " & dtpDiaHora.Text.Trim, True) Then
                        blnFueradeHorario = True
                    End If
                ElseIf strModoEdicion = "E" Then
                    If Not objBN.blnVerificaHorarioServicio(strIDServicio, dtpDia.Text & " " & dtpDiaHora.Text.Trim, True) Then
                        blnFueradeHorario = True
                    Else
                        If Not objBN.blnVerificaHorarioServicio(strIDServicio, dtpDia.Text & " " & dtpDiaHora.Text.Trim) Then
                            blnFueradeHorario = True
                        End If
                    End If
                End If
            End If
            'End If

        End If


        If chkEspecial.Checked Then
            'If txtMotivoEspecial.Text.Trim = "" And txtRutaDocSustento.Text.Trim = "" Then
            If strModulo = "R" Then
                If txtMotivoEspecial.Text.Trim.Length < 5 And lnkDocumentoSustento.Text.Trim = "" Then
                    ErrPrv.SetError(txtMotivoEspecial, "Debe ingresar Motivo Especial mayor a cuatro caracteres o Ruta Documento Sustento")
                    'ErrPrv.SetError(txtRutaDocSustento, "Debe ingresar Motivo Especial o Ruta Documento Sustento")
                    ErrPrv.SetError(lnkDocumentoSustento, "Debe ingresar Motivo Especial mayor a cuatro caracteres o Ruta Documento Sustento")
                    Tab.SelectedTab = TabPage1
                    blnOk = False
                End If
            Else
                If txtMotivoEspecial.Text.Trim = "" And lnkDocumentoSustento.Text.Trim = "" Then
                    ErrPrv.SetError(txtMotivoEspecial, "Debe ingresar Motivo Especial o Ruta Documento Sustento")
                    'ErrPrv.SetError(txtRutaDocSustento, "Debe ingresar Motivo Especial o Ruta Documento Sustento")
                    ErrPrv.SetError(lnkDocumentoSustento, "Debe ingresar Motivo Especial o Ruta Documento Sustento")
                    Tab.SelectedTab = TabPage1
                    blnOk = False
                End If
            End If
        End If

        If strModulo = "C" Then
            If chkAcomodoEspecial.Checked Then
                blnOk = blnValidarIngresosAcomodosEspecialxListaPaxSeleccion()
                If Not blnOk Then
                    ErrPrv.SetError(grbPax, "Debe seleccionar la misma cantidad de pasajero que tienen en el acomodo especial")
                    Tab.SelectedTab = TabPage1
                    blnOk = False
                End If
            End If
        End If


        If txtNroPax.Text = "" Then
            ErrPrv.SetError(txtNroPax, "Debe ingresar Nro. Pax")
            blnOk = False
        Else
            If Convert.ToInt16(txtNroPax.Text) = 0 Then
                ErrPrv.SetError(txtNroPax, "Debe ingresar Nro. Pax mayor que cero")
                blnOk = False
            End If
        End If

        If blnOk And lvwPax.Items.Count > 0 Then

            If (Convert.ToInt16(txtNroPax.Text) + Convert.ToInt16(If(txtNroLiberados.Text = "", "0", txtNroLiberados.Text))) > lvwPax.Items.Count Then ' intPax Then
                ErrPrv.SetError(txtNroPax, "La suma de Pax + Liberados no debe exceder de " & lvwPax.Items.Count.ToString)
                strMensajeValidacNuevoServ = "La suma de Pax + Liberados no debe exceder de " & lvwPax.Items.Count.ToString
                blnOk = False
            End If

            If txtNroLiberados.Text <> "" Then
                If Convert.ToInt16(txtNroLiberados.Text) > Convert.ToInt16(txtNroPax.Text) Then
                    ErrPrv.SetError(txtNroLiberados, "Debe ingresar un Nro. Liberados que no exceda Nro. Pax")

                    blnOk = False
                End If
            End If

            If lvwPax.CheckedItems.Count > (Convert.ToInt16(txtNroPax.Text) + Convert.ToInt16(If(txtNroLiberados.Text = "", "0", txtNroLiberados.Text))) And chkIncGuia.Checked = False Then
                ErrPrv.SetError(lvwPax, "Debe seleccionar una cantidad de pax que no exceda Nro. Pax")
                blnOk = False
            End If

        End If

        If strIDTipoProv <> gstrTipoProveeRestaurantes And strIDTipoProv <> gstrTipoProveeHoteles Then
            If cboIDIdioma.Text.Trim = "" And strIDTipoServ <> "NAP" And strIDTipoServ.Trim <> "OT" Then
                ErrPrv.SetError(cboIDIdioma, "Debe seleccionar un idioma.")
                cboIDIdioma.Focus()
                blnOk = False
            End If

        End If


        If cboOrigen.Text <> "" And cboDestino.Text <> "" Then
            If cboOrigen.Text = Replace(cboDestino.Text, " - E2", "") Then
                ErrPrv.SetError(cboOrigen, "Debe seleccionar diferentes Origen y Destino")
                ErrPrv.SetError(cboDestino, "Debe seleccionar diferentes Origen y Destino")
                blnOk = False
            End If
        End If

        ' ''If Not frmRef Is Nothing Then
        ' ''    If blnServicioTC Then
        ' ''        For Each DgvItem As DataGridViewRow In frmRef.dgvDet.Rows
        ' ''            If Not IsNothing(DgvItem.Cells("FlServicioTC").Value) Then
        ' ''                If Not IsDBNull(DgvItem.Cells("FlServicioTC").Value) Then
        ' ''                    If Convert.ToBoolean(DgvItem.Cells("FlServicioTC").Value) Then
        ' ''                        strMensajeValidacNuevoServ = "Este servicio no puede ser insertado, pues debe ser seleccionado desde la pestaña Tour Conductor."
        ' ''                        Return False
        ' ''                    End If
        ' ''                End If
        ' ''            End If
        ' ''        Next
        ' ''    End If
        ' ''End If

        'If Not frmRefRes Is Nothing Then

        '    'If strIDTipoProv = gstrTipoProveeHoteles Or (frmRefRes.DgvDetalle.CurrentRow.Cells("Dias").Value > 1 Then
        '    If strIDTipoProv = gstrTipoProveeHoteles Or blnConAlojamiento Then
        '        If txtCantidad.Text = "" Then
        '            ErrPrv.SetError(txtCantidad, "Debe ingresar Cantidad")
        '            blnOk = False
        '        Else
        '            If Convert.ToByte(txtCantidad.Text) = 0 Then
        '                ErrPrv.SetError(txtCantidad, "Debe ingresar Cantidad")
        '                blnOk = False
        '            End If
        '        End If
        '    End If
        'End If

        If Not frmRef Is Nothing Then
            If blnFilaActualEsPadre(frmRef.dgvDet, strIDDet, strIDDetRel) Then
                If InStr(txtServicio.Text, " - ") = 0 Then
                    '"1st Day"
                    ErrPrv.SetError(txtServicio, "Debe ingresar un Servicio que termine en - [X]th Day, por ser este un servicio multi días")
                    blnOk = False
                End If
            End If
        End If

        If strModoEdicion = "E" And strModulo = "C" And Not chkIncGuia.Checked Then
            If (cboTipoProv.SelectedValue <> gstrTipoProveeHoteles And blnConAlojamiento) Or _
                (cboTipoProv.SelectedValue = gstrTipoProveeHoteles And Not blnPlanAlimenticio) Then
                If Not frmRef Is Nothing Then
                    Dim bytCantPaxCoti As Byte = If(frmRef.txtNroPax.Text.Trim = String.Empty, 0, CByte(frmRef.txtNroPax.Text)) + _
                                                 If(frmRef.txtNroLiberados.Text.Trim = String.Empty, 0, CByte(frmRef.txtNroLiberados.Text.Trim))
                    Dim bytCantPaxDet As Byte = If(txtNroPax.Text.Trim = String.Empty, 0, CByte(txtNroPax.Text)) + _
                                                 If(txtNroLiberados.Text.Trim = String.Empty, 0, CByte(txtNroLiberados.Text.Trim))
                    If (bytCantPaxCoti <> bytCantPaxDet) And Not chkAcomodoEspecial.Checked Then
                        chkAcomodoEspecial.Focus()
                        ErrPrv.SetError(chkAcomodoEspecial, "Debe seleccionar el acomodo especial para la cantidad de pax especificada")
                        blnOk = False
                    End If
                End If
            End If
        End If

        If blnOk Then blnOk = blnValidarPaxLiberados()


        If Not blnOk Then
            Return False
        Else
            Return True
        End If
    End Function

    Private Sub cboTipoProv_SelectionChangeCommitted(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboTipoProv.SelectionChangeCommitted

        Try
            If cboTipoProv.SelectedValue.ToString <> gstrTipoProveeHoteles Then
                If strModulo = "C" Then
                    If IsDBNull(frmRef.dgvDet.CurrentRow.Cells("ConAlojamiento").Value) = False Then
                        If frmRef.dgvDet.CurrentRow.Cells("ConAlojamiento").Value = False Then
                            OcultarMontosSSST()
                        End If
                    End If
                ElseIf strModulo = "R" Or strModulo = "O" Then
                    '    If frmRefRes.DgvDetalle.CurrentRow.Cells("ConAlojamiento").Value = False Then
                    OcultarMontosSSST()
                    '    End If

                End If
            Else

            End If
            If cboTipoProv.SelectedValue.ToString = gstrTipoProveeOperadores Then
                chkTransfer.Visible = True
            Else
                chkTransfer.Visible = False
                chkTransfer.Checked = False
            End If
            If cboTipoProv.SelectedValue.ToString = gstrTipoProveeOperadores Or _
                cboTipoProv.SelectedValue.ToString = gstrTipoProveeTransportista Then

                If Not chkTransfer.Checked Then
                    lblTipoTransporteEtiq.Visible = True
                    cboTipoTransporte.Visible = True

                    lblOrigenEtiq.Visible = False
                    cboOrigen.Visible = False
                    cboOrigen.SelectedIndex = -1
                    lblDestinoEtiq.Visible = False
                    cboDestino.Visible = False
                    cboDestino.SelectedIndex = -1
                Else
                    lblTipoTransporteEtiq.Visible = False
                    cboTipoTransporte.Visible = False
                    cboTipoTransporte.SelectedIndex = -1

                    lblOrigenEtiq.Visible = True
                    cboOrigen.Visible = True
                    lblDestinoEtiq.Visible = True
                    cboDestino.Visible = True
                End If
            Else
                lblTipoTransporteEtiq.Visible = False
                cboTipoTransporte.Visible = False
                cboTipoTransporte.SelectedIndex = -1

                lblOrigenEtiq.Visible = False
                cboOrigen.Visible = False
                cboOrigen.SelectedIndex = -1
                lblDestinoEtiq.Visible = False
                cboDestino.Visible = False
                cboDestino.SelectedIndex = -1
            End If

            If cboTipoProv.SelectedValue.ToString = gstrTipoProveeTransportista Or _
                (cboTipoProv.SelectedValue.ToString = gstrTipoProveeOperadores And Not chkTransfer.Checked) Then

                lblIDUbigeoOriEtiq.Visible = True
                cboIDUbigeoOri.Visible = True
                lblIDUbigeoDesEtiq.Visible = True
                cboIDUbigeoDes.Visible = True
                lblEtiqPaisOrig.Visible = True
                cboPaisOrig.Visible = True
                lblEtiqPaisDest.Visible = True
                cboPaisDest.Visible = True
            Else
                lblIDUbigeoOriEtiq.Visible = False
                cboIDUbigeoOri.Visible = False
                lblIDUbigeoDesEtiq.Visible = False
                cboIDUbigeoDes.Visible = False
                lblEtiqPaisOrig.Visible = False
                cboPaisOrig.Visible = False
                lblEtiqPaisDest.Visible = False
                cboPaisDest.Visible = False
            End If

            chkIncGuia.Checked = False
            chkIncGuia.Visible = False
            If cboTipoProv.SelectedValue.ToString = gstrTipoProveeHoteles Or _
                cboTipoProv.SelectedValue.ToString = gstrTipoProveeRestaurantes Or _
                cboTipoProv.SelectedValue.ToString = gstrTipoProveeTransportista Or _
                (cboTipoProv.SelectedValue.ToString = gstrTipoProveeOperadores) Then 'And blnConAlojamiento) Then
                chkIncGuia.Visible = True
            End If

            If strModulo = "R" Or strModulo = "O" Then
                chkServicioNoShow.Visible = True

                chkServicioNoShow.Location = chkAcomodoEspecial.Location

                If strModulo = "O" Then
                    If strIDTipoProv <> gstrTipoProveeHoteles Then
                        chkServicioNoShow.Location = New Point(677, 42)
                        'chkServicioNoShow.Enabled = False
                    End If
                End If
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message, ex.Source, MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try

    End Sub
    Private Sub OcultarMontosSSST(Optional ByVal vblnLecturaReservas As Boolean = False)
        Try
            lblImpuestosEtiq2.Visible = False
            lblImpuestosEtiq3.Visible = False

            lblSSCLEtiq.Visible = False
            txtSSCL.Visible = False
            txtSSCL.Text = "0.00"
            lblSSCLImpto.Visible = False
            lblSSCLImpto.Text = "0.00"

            lblSSCREtiq.Visible = False
            txtSSCR.Visible = False
            txtSSCR.Text = "0.00"
            lblSSCRImpto.Visible = False
            lblSSCRImpto.Text = "0.00"

            lblSSMargenEtiq.Visible = False
            txtSSMargen.Visible = False
            txtSSMargen.Text = "0.00"
            lblSSMargenImpto.Visible = False
            lblSSMargenImpto.Text = "0.00"

            lblSSMLEtiq.Visible = False
            txtSSMargenLiberado.Visible = False
            txtSSMargenLiberado.Text = "0.00"
            lblSSMargenLiberadoImpto.Visible = False
            lblSSMargenLiberadoImpto.Text = "0.00"

            lblSSTotalEtiq.Visible = False
            txtSSTotal.Visible = False
            txtSSTotal.Text = "0.00"

            lblSTCREtiq.Visible = False
            txtSTCR.Visible = False
            txtSTCR.Text = "0.00"
            lblSTCRImpto.Visible = False
            lblSTCRImpto.Text = "0.00"

            lblSTMargenEtiq.Visible = False
            txtSTMargen.Visible = False
            txtSTMargen.Text = "0.00"
            lblSTMargenImpto.Visible = False
            lblSTMargenImpto.Text = "0.00"

            lblSTTotalEtiq.Visible = False
            txtSTTotal.Visible = False
            txtSTTotal.Text = "0.00"

            '-----
            If vblnLecturaReservas Then
                lblCostoenDobleEtiq.Text = "Costos en " & txtNroPax.Text.ToString & " Pax"
            Else
                lblCostoenDobleEtiq.Text = "Costos en " & intPax.ToString & " Pax"
            End If

            lblTituloCostoSimple.Visible = False
            lblTituloCostoTriple.Visible = False
            lblImpuestosEtiq12.Visible = False
            lblImpuestosEtiq22.Visible = False
            'grbSepCostos.Visible = False
            grbSep4.Visible = False

            lblSSCLEtiq2.Visible = False
            lblSSCL2.Visible = False
            txtSSCL.Text = "0.00"
            lblSSCLImpto2.Visible = False
            lblSSCLImpto.Text = "0.00"

            lblSSCREtiq2.Visible = False
            lblSSCR2.Visible = False
            txtSSCR2.Visible = False
            txtSSCR2.Text = "0.00"
            txtSSCR.Text = "0.00"
            lblSSCRImpto2.Visible = False
            lblSSCRImpto.Text = "0.00"

            lblSSMargenEtiq2.Visible = False
            lblSSMargen2.Visible = False
            txtSSMargen.Text = "0.00"
            lblSSMargenImpto2.Visible = False
            lblSSMargenImpto.Text = "0.00"

            lblSSMLEtiq2.Visible = False
            lblSSMargenLiberado2.Visible = False
            txtSSMargenLiberado.Text = "0.00"
            lblSSMargenLiberadoImpto2.Visible = False
            lblSSMargenLiberadoImpto.Text = "0.00"

            lblSSTotalEtiq2.Visible = False
            lblSSTotal2.Visible = False
            txtSSTotal.Text = "0.00"

            lblSTCREtiq2.Visible = False
            lblSTCR2.Visible = False
            txtSTCR2.Visible = False
            txtSTCR.Text = "0.00"
            txtSTCR2.Text = "0.00"
            lblSTCRImpto2.Visible = False
            lblSTCRImpto.Text = "0.00"

            lblSTMargenEtiq2.Visible = False
            lblSTMargen2.Visible = False
            txtSTMargen.Text = "0.00"
            lblSTMargenImpto2.Visible = False
            lblSTMargenImpto.Text = "0.00"

            lblSTTotalEtiq2.Visible = False
            lblSTTotal2.Visible = False
            txtSTTotal.Text = "0.00"

            'lblTipoLibEtiq.Visible = False
            'cboTipoLib.Visible = False
            'cboTipoLib.SelectedIndex = -1

            lblSSImpEtiq2.Visible = False
            lblSTImpEtiq2.Visible = False
            lblMargenAplicado3.Visible = False
            lblMargenAplicado4.Visible = False
            lblMargenAplicado3.Text = "0.00"
            lblMargenAplicado4.Text = "0.00"
            lblSSImpEtiq2.Text = "0.00"
            lblSTImpEtiq2.Text = "0.00"
        Catch ex As Exception
            Throw
        End Try
    End Sub

    Private Sub cboCiudad_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles cboCiudad.KeyPress

    End Sub

    Private Sub dgvIdiom_CellEndEdit(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvIdiom.CellEndEdit
        dgvIdiom.Columns(dgvIdiom.CurrentCell.ColumnIndex).DefaultCellStyle.ForeColor = Color.Black
    End Sub


    Private Sub txtUsuario_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboCiudad.TextChanged, _
        cboTipoProv.TextChanged, lblProveedor.TextChanged, txtCantidadaPagar.TextChanged, _
        txtServicio.TextChanged, txtMotivoEspecial.TextChanged, _
        txtRutaDocSustento.TextChanged, lnkDocumentoSustento.TextChanged, cboIDIdioma.TextChanged, txtCostoReal.TextChanged, _
        txtCostoRealImpto.TextChanged, txtMargenAplicado.TextChanged, _
        txtNroPax.TextChanged, txtNroLiberados.TextChanged, lvwPax.ItemCheck, dgvIdiom.CellValueChanged, _
        cboOrigen.TextChanged, cboDestino.TextChanged, txtCantidad.TextChanged, dtpFechaOut.ValueChanged, _
        cboIDUbigeoOri.TextChanged, cboIDUbigeoDes.TextChanged, dtpDiaHora.ValueChanged, _
        chkDesayuno.CheckedChanged, chkAlmuerzo.CheckedChanged, chkCena.CheckedChanged, chkLonche.CheckedChanged, dtpDia.ValueChanged, _
        chkVerVoucher.CheckedChanged, chkVerBiblia.CheckedChanged, CboBusVehiculo.SelectionChangeCommitted, _
        lblTotal.TextChanged, lblCostoLiberado.TextChanged, lblTotImpto.TextChanged, _
        lblMargen.TextChanged, lblRedondeoTotal.TextChanged, txtNetoHab.TextChanged, txtIgvHab.TextChanged, cboGuia.SelectionChangeCommitted, _
        chkAcomodoEspecial.CheckedChanged, cboIDIdioma.SelectionChangeCommitted, _
        lblCostoReal2.TextChanged, lblCostoLiberado2.TextChanged, lblTotImpto2.TextChanged, lblMargenLiberado2.TextChanged, _
        lblMargenAplicado2.TextChanged, lblMargen2.TextChanged, lblRedondeoTotal2.TextChanged, lblTotal2.TextChanged, _
        lblSSCR2.TextChanged, lblSSCL2.TextChanged, lblSSCRImpto2.TextChanged, lblSSMargenLiberado2.TextChanged, lblSSMargen2.TextChanged, _
        lblMargenAplicado3.TextChanged, lblSSTotal2.TextChanged, lblSTCR2.TextChanged, lblSTCRImpto2.TextChanged, lblMargenAplicado4.TextChanged, _
        lblSTMargen2.TextChanged, lblSTTotal2.TextChanged, txtSSCR2.TextChanged, txtSTCR2.TextChanged, chkServicioNoShow.CheckedChanged, _
        txtObservVoucher.TextChanged, txtObservBiblia.TextChanged, txtObservInterno.TextChanged
        Try
            Select Case sender.name
                Case "cboCiudad"
                    ErrPrv.SetError(cboCiudad, "")

                Case "cboTipoProv"
                    ErrPrv.SetError(cboTipoProv, "")

                Case "lblProveedor"
                    ErrPrv.SetError(btnProveedor, "")

                Case "txtMotivoEspecial"
                    ErrPrv.SetError(txtMotivoEspecial, "")
                    'ErrPrv.SetError(txtRutaDocSustento, "")
                    ErrPrv.SetError(lnkDocumentoSustento, "")
                    'Case "txtRutaDocSustento"
                    '    ErrPrv.SetError(txtMotivoEspecial, "")
                    '    ErrPrv.SetError(txtRutaDocSustento, "")
                    '    ErrPrv.SetError(lnkDocumentoSustento, "")
                Case "lnkDocumentoSustento"
                    ErrPrv.SetError(txtMotivoEspecial, "")
                    'ErrPrv.SetError(txtRutaDocSustento, "")
                    ErrPrv.SetError(lnkDocumentoSustento, "")
                Case "txtNroPax"
                    ErrPrv.SetError(txtNroPax, "")
                Case "txtNroLiberados"
                    ErrPrv.SetError(txtNroLiberados, "")
                Case "lvwPax"
                    ErrPrv.SetError(lvwPax, "")
                    ErrPrv.SetError(grbPax, "")
                Case "txtCantidad"
                    ErrPrv.SetError(txtCantidad, "")
                Case "dtpFechaOut"
                    If ((strIDTipoProv <> gstrTipoProveeHoteles And blnConAlojamiento) Or _
                            (strIDTipoProv = gstrTipoProveeHoteles And Not blnPlanAlimenticio)) Then
                        lblNoches.Text = DateDiff(DateInterval.Day, dtpDia.Value, dtpFechaOut.Value)
                    End If
                Case "txtServicio"
                    ErrPrv.SetError(txtServicio, "")
                Case "txtNetoHab"
                    ErrPrv.SetError(txtNetoHab, "")
                Case "txtIgvHab"
                    ErrPrv.SetError(txtIgvHab, "")
                Case "txtSSCR2"
                    txtSSCR.Text = txtSSCR2.Text
                Case "txtSTCR2"
                    txtSTCR.Text = txtSTCR2.Text
                Case "chkServicioNoShow"
                    txtCostoReal.ReadOnly = False
                    txtMargenAplicado.ReadOnly = False
                    If strModulo = "R" Or strModulo = "O" Then
                        txtCostoReal.ReadOnly = Not chkServicioNoShow.Checked
                        txtMargenAplicado.ReadOnly = Not chkServicioNoShow.Checked
                    End If
                Case "chkAcomodoEspecial"
                    ErrPrv.SetError(chkAcomodoEspecial, "")

                Case "chkCena"
                    If blnLoad Then blnEditoChecksViaticosTC = True
                Case "chkLonche"
                    If blnLoad Then blnEditoChecksViaticosTC = True
                Case "chkAlmuerzo"
                    If blnLoad Then blnEditoChecksViaticosTC = True
            End Select

            If blnLoad Then
                blnCambios = True
                If sender.name = "dgvIdiom" Then
                    'If frmRefRes Is Nothing Then
                    btnAceptar.Enabled = True
                    'End If
                Else
                    btnAceptar.Enabled = True
                End If


                If sender.name.ToString = "txtCostoReal" Or _
                    sender.name.ToString = "txtMargenAplicado" Then
                    If Not frmRef Is Nothing Then frmRef.blnActualizoCostosDetalle = True
                End If
            End If

        Catch ex As Exception
            MessageBox.Show(ex.Message, ex.Source, MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try

    End Sub

    Private Sub BuscarDetServicio()
        Try
            Dim frmServ As New frmMantServiciosProveedor

            If strModulo = "C" Then
                With frmServ
                    .strProveedor = strProveedor
                    If cboTipoProv.Text <> "" Then .strTipoProveedor = cboTipoProv.SelectedValue.ToString
                    .strAnio = Year(datFechaIni)
                    .strAnioDiaSgte = Year(dtpDia.Value).ToString
                    .blnCotizacion = True
                    .blnServicioVarios = blnServicioVarios
                    .strIDPais = strIDPais
                    .strIDCiudad = strIDCiudad
                    .frmRefIngDetCotiDatoMem = Me
                    .blnReemplazandoServicio = blnReemplazandoServicio
                    If Not blnReemplazandoServiciosHotel And Not blnReemplazandoServiciosHotel And Not blnServicioVarios And Not blnReemplazandoServicio Then .blnNuevoServicioCot = True
                    .datFechaInsert = datFechaIni
                    If Not frmRef Is Nothing Then
                        If Not frmRef.dgvDet.CurrentRow Is Nothing Then
                            .datFechaInsert = frmRef.dgvDet.CurrentRow.Cells("Dia").Value
                            If blnDiaSgte Then .datFechaInsert = .datFechaInsert.AddDays(1)
                        End If
                    End If

                    If blnServicioVarios Then
                        .intIDCab = intIDCab
                        .strTituloFile = strTituloFile
                    End If

                    If Not blnReemplazandoServiciosHotel Then
                        .ShowDialog()
                    Else
                        .blnReemplazaServiciosHotel = True
                        .strIDServicioHotel = strIDServicio
                        .intIDServicio_DetHotel = intIDServicio_Det
                        .Visible = False
                        .Show()
                    End If

                End With
            ElseIf strModulo = "R" Or strModulo = "O" Then
                With frmServ
                    .strProveedor = strProveedor
                    If cboTipoProv.Text <> "" Then .strTipoProveedor = cboTipoProv.SelectedValue.ToString
                    .strIDPais = cboPais.SelectedValue.ToString
                    .strIDCiudad = cboCiudad.SelectedValue.ToString
                    .strAnio = Year(datFechaIni)
                    .frmRefIngDetCotiDatoMem = Me
                    .ShowDialog()
                End With
            End If

            If blnSelecServxServ Then
                If Not cboTipoProv.SelectedValue Is Nothing Then
                    If cboTipoProv.SelectedValue.ToString = gstrTipoProveeHoteles Or cboTipoProv.SelectedValue.ToString = gstrTipoProveeRestaurantes Then
                        txtServicio.Text = strDescServicio_Det
                    Else
                        txtServicio.Text = strDescServicio
                    End If
                End If

                If lblProveedor.Text.Trim <> "" Then
                    CalcularMontosNuevoServicio()
                    ''Dim dblCostoReal As Double = -1
                    ''Dim dblMargenAplicado As Double = -1
                    ''Dim dblSSCR As Double = -1
                    ''Dim dblSTCR As Double = -1
                    ''Dim dblCostoRealTxt As Double = If(txtCostoReal.Text = "", "0", txtCostoReal.Text)
                    ''Dim dblMargenAplicadoTxt As Double = If(txtMargenAplicado.Text = "", "0", txtMargenAplicado.Text)
                    ''Dim dblSSCRTxt As Double = If(txtSSCR2.Text = "", "0", txtSSCR2.Text)
                    ''Dim dblSTCRTxt As Double = If(txtSTCR2.Text = "", "0", txtSTCR2.Text)


                    ''If blnCostoRealEditado Then
                    ''    dblCostoReal = dblCostoRealTxt
                    ''End If
                    ''If blnMargenAplicadoEditado Then
                    ''    dblMargenAplicado = dblMargenAplicadoTxt
                    ''End If
                    ''If blnSSCREditado Then
                    ''    dblSSCR = dblSSCRTxt
                    ''End If
                    ''If blnSTCREditado Then
                    ''    dblSTCR = dblSTCRTxt
                    ''End If
                    ''CalcularMontos(dblCostoReal, dblMargenAplicado, _
                    ''               dblCostoReal, dblMargenAplicado, _
                    ''               strTipoPaxNacionalExtranjero, dblSSCR, dblSTCR)
                End If
            End If
        Catch ex As Exception
            Throw
        End Try
    End Sub

    Public Sub CalcularMontosNuevoServicio(Optional ByVal vblnActualizarDesc As Boolean = False)
        Try
            If vblnActualizarDesc Then
                If Not cboTipoProv.SelectedValue Is Nothing Then
                    If cboTipoProv.SelectedValue.ToString = gstrTipoProveeHoteles Or cboTipoProv.SelectedValue.ToString = gstrTipoProveeRestaurantes Then
                        txtServicio.Text = strDescServicio_Det
                    Else
                        txtServicio.Text = strDescServicio
                    End If
                End If
            End If

            Dim dblCostoReal As Double = -1
            Dim dblMargenAplicado As Double = -1
            Dim dblSSCR As Double = -1
            Dim dblSTCR As Double = -1
            Dim dblCostoRealTxt As Double = If(txtCostoReal.Text = "", "0", txtCostoReal.Text)
            Dim dblMargenAplicadoTxt As Double = If(txtMargenAplicado.Text = "", "0", txtMargenAplicado.Text)
            Dim dblSSCRTxt As Double = If(txtSSCR2.Text = "", "0", txtSSCR2.Text)
            Dim dblSTCRTxt As Double = If(txtSTCR2.Text = "", "0", txtSTCR2.Text)


            If blnCostoRealEditado Then
                dblCostoReal = dblCostoRealTxt
            End If
            If blnMargenAplicadoEditado Then
                dblMargenAplicado = dblMargenAplicadoTxt
            End If
            If blnSSCREditado Then
                dblSSCR = dblSSCRTxt
            End If
            If blnSTCREditado Then
                dblSTCR = dblSTCRTxt
            End If
            CalcularMontos(dblCostoReal, dblMargenAplicado, _
                           dblCostoReal, dblMargenAplicado, _
                           strTipoPaxNacionalExtranjero, dblSSCR, dblSTCR)
        Catch ex As Exception
            Throw
        End Try
    End Sub

    Private Sub btnProveedor_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnProveedor.Click
        Try
            'If cboCiudad.Text = "" Then
            '    ErrPrv.SetError(cboCiudad, "Debe seleccionar Ciudad")
            '    Exit Sub
            'End If

            'If cboTipoProv.Text = "" Then
            '    ErrPrv.SetError(cboTipoProv, "Debe seleccionar Tipo de Proveedor")
            '    Exit Sub
            'End If

            'Dim frmAyuda As New frmAyudas
            'With frmAyuda
            '    .strCaller = Me.Name
            '    .frmRefIngDetCotiDatoMem = Me
            '    .strTipoBusqueda = "Proveedor"

            '    .strPar = cboTipoProv.SelectedValue.ToString
            '    .strPar2 = cboCiudad.SelectedValue.ToString
            '    .Text = "Consulta de Proveedores"
            '    .ShowDialog()
            'End With


            'If lblProveedor.Text.Trim <> "" Then
            BuscarDetServicio()
            'End If

        Catch ex As Exception
            MessageBox.Show(ex.Message, ex.Source, MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub CreaDttDetCotiServ()


        'With dttDetCotiServ
        '    .Columns.Add("IDDet")
        '    .Columns.Add("IDServicio")
        '    .Columns.Add("IDServicio_Det")
        '    .Columns.Add("CostoReal")
        '    .Columns.Add("Margen")
        '    .Columns.Add("CostoLiberado")
        '    .Columns.Add("MargenLiberado")
        '    .Columns.Add("MargenAplicado")
        '    .Columns.Add("Total")
        '    .Columns.Add("RedondeoTotal")
        '    .Columns.Add("SSCR")
        '    .Columns.Add("SSMargen")
        '    .Columns.Add("SSCL")
        '    .Columns.Add("SSML")
        '    .Columns.Add("SSTotal")
        '    .Columns.Add("STCR")
        '    .Columns.Add("STMargen")
        '    .Columns.Add("STTotal")

        '    .Columns.Add("CostoRealImpto")
        '    .Columns.Add("CostoLiberadoImpto")
        '    .Columns.Add("MargenImpto")
        '    .Columns.Add("MargenLiberadoImpto")
        '    .Columns.Add("SSCRImpto")
        '    .Columns.Add("SSCLImpto")
        '    .Columns.Add("SSMargenImpto")
        '    .Columns.Add("SSMargenLiberadoImpto")
        '    .Columns.Add("STCRImpto")
        '    .Columns.Add("STMargenImpto")
        '    .Columns.Add("TotImpto")


        'End With

        dttDetCotiServ = dttCreaDttDetCotiServ()

    End Sub

    Public Function dttCreaDttDetCotiServ() As DataTable

        Dim dtt As New DataTable
        With dtt
            .Columns.Add("IDDet")
            .Columns.Add("IDServicio")
            .Columns.Add("IDServicio_Det")
            .Columns.Add("CostoReal")
            .Columns.Add("Margen")
            .Columns.Add("CostoLiberado")
            .Columns.Add("MargenLiberado")
            .Columns.Add("MargenAplicado")
            .Columns.Add("Total")
            .Columns.Add("RedondeoTotal")
            .Columns.Add("SSCR")
            .Columns.Add("SSMargen")
            .Columns.Add("SSCL")
            .Columns.Add("SSML")
            .Columns.Add("SSTotal")
            .Columns.Add("STCR")
            .Columns.Add("STMargen")
            .Columns.Add("STTotal")

            .Columns.Add("CostoRealImpto")
            .Columns.Add("CostoLiberadoImpto")
            .Columns.Add("MargenImpto")
            .Columns.Add("MargenLiberadoImpto")
            .Columns.Add("SSCRImpto")
            .Columns.Add("SSCLImpto")
            .Columns.Add("SSMargenImpto")
            .Columns.Add("SSMargenLiberadoImpto")
            .Columns.Add("STCRImpto")
            .Columns.Add("STMargenImpto")
            .Columns.Add("TotImpto")


        End With

        Return dtt
    End Function

    Private Function blnTourConductorSelect() As Boolean
        If lvwPax.Items.Count = 0 Then Return False
        Try
            Dim blnSelect As Boolean = False
            Dim strIDPaxTC As String = String.Empty
            If Not IsNothing(frmRef) Then
                For Each ItemDgv As DataGridViewRow In frmRef.dgvPax.Rows
                    If Not IsNothing(ItemDgv.Cells("TC").Value) Then
                        If CBool(ItemDgv.Cells("TC").Value) Then
                            strIDPaxTC = ItemDgv.Cells("IDPax").Value.ToString()
                            Exit For
                        End If
                    End If
                Next
            End If

            If strIDPaxTC <> String.Empty Then
                For i = 0 To lvwPax.Items.Count - 1
                    Dim blnChecked As Boolean = lvwPax.Items(i).Checked
                    Dim strIDPaxLvw As String = lvwPax.Items(i).SubItems(3).Text

                    If strIDPaxLvw = strIDPaxTC Then Return blnChecked
                Next
            End If

            Return False
        Catch ex As Exception
            Throw
        End Try
    End Function

    Private Sub CalcularMontos(Optional ByVal vdblCostoReal As Double = -1, Optional ByVal vdblMargenAplicado As Double = -1, _
                               Optional ByVal vdblCostoRealQ As Double = -1, Optional ByVal vdblMargenAplicadoQ As Double = -1, _
                               Optional ByVal vstrTipoPax As String = "", _
                               Optional ByVal vdblSSCR As Double = -1, _
                               Optional ByVal vdblSTCR As Double = -1)
        If strIDServicio = "" Then Exit Sub

        Dim strTipoPaxLoc As String
        'If vstrTipoPax = "" Then
        '    strTipoPaxLoc = strTipoPax
        'Else
        '    strTipoPaxLoc = vstrTipoPax
        'End If
        If vstrTipoPax = "" Then
            strTipoPaxLoc = "EXT"
        Else
            strTipoPaxLoc = "NCR"
        End If
        Dim strIDTipoHonorario As String = String.Empty

        Dim strRedondeo As String = ""
        If strModulo = "C" Then
            strRedondeo = frmRef.cboRedondeo.SelectedValue.ToString()
            If frmRef.chkIncluirTC.Checked Then
                If Not frmRef.cboTipoHonorarios.SelectedValue Is Nothing Then
                    strIDTipoHonorario = frmRef.cboTipoHonorarios.SelectedValue
                End If
            End If
        ElseIf strModulo = "R" Or strModulo = "O" Then
            'strRedondeo = frmRefRes.strRedondeo
        End If


        Try
            Me.Cursor = Cursors.WaitCursor
            'Dim obj As New clsCotizacionBN
            Dim strIDDet_DetCotiServ As String
            strIDDet_DetCotiServ = strIDDet
            If strIDDet Is Nothing Then
                If strModulo = "C" Then
                    'strIDDet_DetCotiServ = "M" & frmRef.dgvDet.RowCount + 1
                    strIDDet_DetCotiServ = "M" & frmRef.bytMayorIddetGenerado ' + 1
                ElseIf strModulo = "R" Or strModulo = "O" Then
                    strIDDet_DetCotiServ = ""
                End If
            End If

            Dim strIDTipoProvCalc As String = cboTipoProv.SelectedValue.ToString
            Dim intNroPax As Int16 = txtNroPax.Text
            Dim intNroLiberados As Int16 = If(txtNroLiberados.Text = "", "0", txtNroLiberados.Text)
            Dim strTipoLib As String = If(cboTipoLib.Text = "", "", cboTipoLib.SelectedValue.ToString)
            Dim blnIncGuia As Boolean = False
            If chkIncGuia.Checked Then
                strIDTipoProvCalc = gstrTipoProveeOperadores 'algún servicio
                'intNroPax = 1
                'intNroLiberados = 0
                'strTipoLib = ""
                intNroPax = intPax
                blnIncGuia = True
            End If

            If intNroPax + intNroLiberados = 0 Or intNroPax = 0 Then Exit Sub
            'Dim dc As Dictionary(Of String, Double) = obj.CalculoCotizacion( _
            '    strIDServicio, intIDServicio_Det, strAnio, strTipoServicio_Det, strIDTipoProvCalc, intNroPax, _
            '    intNroLiberados, _
            '    strTipoLib, _
            '    dblMargenDif, dblTipoCambioCotiCab, vdblCostoReal, vdblMargenAplicado, strTipoPaxLoc, strRedondeo, _
            '    dttDetCotiServ, gsglIGV, strIDDet_DetCotiServ, blnIncGuia, blnMargenCero, , , dtpDia.Text)
            If blnServicioTC Then
                For Each Item As DataGridViewRow In frmRef.dgvTCambio.Rows
                    If Item.Cells("CoMonedaTCambio").Value = gstrTipoMonedaSol Then
                        dblTipoCambioCotiCab = Item.Cells("SsTipCamTCambio").Value
                    End If
                Next
                vdblMargenAplicado = If(frmRef.txtMargenTC.Text.Trim = "", 0, CDbl(frmRef.txtMargenTC.Text.Trim))
                Dim intNroTC As Int16 = 0
                For Each Item As DataGridViewRow In frmRef.dgvPax.Rows
                    If Not IsNothing(Item.Cells("TC").Value) Then
                        If Item.Cells("TC").Value Then
                            intNroTC += 1
                        End If
                    End If
                Next
                intNroPax -= intNroTC
            End If

            Dim dc As Dictionary(Of String, Double) = CalculoCotizacionPreview(ListBETCamMon, _
            strIDServicio, intIDServicio_Det, strAnio, strTipoServicio_Det, strIDTipoProvCalc, intNroPax, _
            intNroLiberados, _
            strTipoLib, _
            dblMargenDif, dblTipoCambioCotiCab, vdblCostoReal, vdblMargenAplicado, strTipoPaxLoc, strRedondeo, _
            dttDetCotiServ, gsglIGV, strIDDet_DetCotiServ, blnIncGuia, blnMargenCero, , , dtpDia.Text, _
            vdblSSCR, vdblSTCR, blnServicioTC, strIDTipoHonorario, dicValoresTC, chkAcomVehi.Checked)


            'If dttDetCotiServ.Rows.Count = 0 Then
            For Each dic As KeyValuePair(Of String, Double) In dc
                For Each ctrl As Control In grbDatosGenerales.Controls
                    If dic.Key = Mid(ctrl.Name, 4) Then
                        If ctrl.GetType Is GetType(System.Windows.Forms.TextBox) Then
                            ctrl.Text = dic.Value.ToString("##,##0.0000")

                            If ctrl.Text.ToUpper = "NEUN" Or ctrl.Text.ToUpper = "INFINITO" Then
                                ctrl.Text = "0.0000"
                            End If

                            For Each ctrl2 As Control In grbMontos.Controls
                                If dic.Key & "2" = Mid(ctrl2.Name, 4) Then
                                    'If dic.Key = "CostoReal" Then
                                    '    ctrl2.Text = dic.Value.ToString("##,##0.0000")
                                    'Else
                                    '    ctrl2.Text = dic.Value.ToString("##,##0.00")
                                    'End If
                                    ctrl2.Text = dic.Value.ToString("##,##0.0000")

                                    If ctrl2.Text.ToUpper = "NEUN" Then
                                        ctrl2.Text = "0.0000"
                                    End If

                                End If
                            Next
                        End If
                        If ctrl.GetType Is GetType(System.Windows.Forms.Label) Then

                            ctrl.Text = dic.Value.ToString("##,##0.0000")
                            'MessageBox.Show(ctrl.Text)
                            'MessageBox.Show(dic.Key & " - " & ctrl.Text.ToUpper)

                            If ctrl.Text.ToUpper = "NEUN" Or ctrl.Text.ToUpper = "INFINITO" Then
                                ctrl.Text = "0.0000"
                            End If

                            For Each ctrl2 As Control In grbMontos.Controls
                                If dic.Key & "2" = Mid(ctrl2.Name, 4) Then
                                    'If dic.Key = "CostoReal" Then
                                    '    ctrl2.Text = dic.Value.ToString("##,##0.0000")
                                    'Else
                                    '    ctrl2.Text = dic.Value.ToString("##,##0.00")
                                    'End If
                                    ctrl2.Text = dic.Value.ToString("##,##0.0000")

                                    If ctrl2.Text.ToUpper = "NEUN" Then
                                        ctrl2.Text = "0.0000"
                                    End If

                                End If
                            Next

                        End If
                    End If
                Next
            Next
            'Else
            'For bytFilaDtt As Byte = 0 To dttDetCotiServ.Rows.Count
            '    For Each dic As DataColumn In dttDetCotiServ.Columns

            '        For Each ctrl As Control In grbDatosGenerales.Controls
            '            If dic.ColumnName = Mid(ctrl.Name, 4) Then
            '                If ctrl.GetType Is GetType(System.Windows.Forms.TextBox) Then

            '                    ctrl.Text = Format(Convert.ToDouble(dttDetCotiServ(bytFilaDtt)(dic.ColumnName)), "##,##0.00")

            '                    If ctrl.Text.ToUpper = "NEUN" Then
            '                        ctrl.Text = "0.00"
            '                    End If
            '                End If
            '                If ctrl.GetType Is GetType(System.Windows.Forms.Label) Then

            '                    ctrl.Text = dttDetCotiServ(bytFilaDtt)(dic.ColumnName).ToString("##,##0.00")

            '                    If ctrl.Text.ToUpper = "NEUN" Then
            '                        ctrl.Text = "0.00"
            '                    End If



            '                End If

            '            End If

            '        Next
            '    Next
            'Next
            'End If

            lblTotal.Text = Format(CDbl(lblTotal.Text), "###,###0.00")
            txtMargenAplicado.Text = Format(CDbl(txtMargenAplicado.Text), "###,###0.00")
            lblMargenAplicado2.Text = Format(CDbl(lblMargenAplicado2.Text), "###,###0.00")
            lblTotal2.Text = Format(CDbl(lblTotal2.Text), "###,###0.00")



            'jorge
            'copiar monto de margen2 a margen3 y 4
            lblMargenAplicado3.Text = Format(CDbl(lblMargenAplicado2.Text), "###,###0.00")
            lblMargenAplicado4.Text = Format(CDbl(lblMargenAplicado2.Text), "###,###0.00")

            CargarToolTip()

            If strModulo = "C" Then
                CalcularMontosQuiebres(vdblCostoRealQ, vdblMargenAplicadoQ, vstrTipoPax)
            End If
            Me.Cursor = Cursors.Default
        Catch ex As Exception
            Me.Cursor = Cursors.Default
            Throw
        Finally
            Me.Cursor = Cursors.Default
        End Try
    End Sub
    Private Sub CargarToolTip(Optional ByVal vdblCostoRealOrig As Double = 0, _
                              Optional ByVal vdblMargenAplicadoOrig As Double = 0)

        If strModoEdicion = "N" Then Exit Sub

        If dblCostoRealOrig = 0 Then
            If vdblCostoRealOrig <> 0 Then
                dblCostoRealOrig = vdblCostoRealOrig
                dblMargenAplicadoOrig = vdblMargenAplicadoOrig
            Else

                dblCostoRealOrig = txtCostoReal.Text
                dblMargenAplicadoOrig = txtMargenAplicado.Text
            End If
        End If

        ToolTip1.ShowAlways = True
        ToolTip1.SetToolTip(txtCostoReal, "Costo Real Original = " & dblCostoRealOrig.ToString("##,##0.00"))
        ToolTip1.SetToolTip(txtMargenAplicado, "Margen Aplicado Original = " & dblMargenAplicadoOrig.ToString("##,##0.00"))

    End Sub

    Private Sub CalcularMontosQuiebres(ByVal vdblCostoReal As Double, ByVal vdblMargenAplicado As Double, Optional ByVal vstrTipoPax As String = "")

        'If strIDServicio = "" Then Exit Sub
        Dim strTipoPaxLoc As String
        If vstrTipoPax = "" Then
            strTipoPaxLoc = strTipoPax
        Else
            strTipoPaxLoc = vstrTipoPax
        End If
        Dim strRedondeo As String = frmRef.cboRedondeo.SelectedValue.ToString

        Try
            'Dim obj As New clsCotizacionBN
            Dim dc As Dictionary(Of String, Double)
            'Dim dttQuiebres As New DataTable("Quiebres")
            'Dim strCad As String = ""

            'For Each dgvCol As DataGridViewColumn In frmRef.dgvQuie.Columns
            '    With dttQuiebres.Columns
            '        .Add(dgvCol.Name)
            '    End With
            'Next
            If intIDDet = 0 Then dgvQui.Rows.Clear()

            Dim bytFila As Byte = 0
            Dim blnExiste As Boolean
            For Each dgvRow As DataGridViewRow In frmRef.dgvQuie.Rows
                If Not dgvRow.IsNewRow Then

                    If strModoEdicion = "N" Then
                        dgvQui.Rows.Add()
                        dgvQui.Item("Accion", bytFila).Value = "N"
                        If dgvQui.Item("IDDet_Q", bytFila).Value Is Nothing Then dgvQui.Item("IDDet_Q", bytFila).Value = "M" & (dgvRow.Index + 1)
                    ElseIf strModoEdicion = "E" Then
                        blnExiste = False
                        For Each dgvRow2 As DataGridViewRow In dgvQui.Rows
                            If Not dgvRow2.Cells("IDCab_Q").Value Is Nothing Then
                                If dgvRow2.Cells("IDCab_Q").Value.ToString = dgvRow.Cells("IDCab_Q").Value.ToString And _
                                   IsNumeric(dgvRow2.Cells("IDDet_Q").Value) Then
                                    blnExiste = True
                                    Exit For
                                End If
                            End If
                        Next
                        If blnExiste Then
                            dgvQui.Item("Accion", bytFila).Value = "M"
                        Else
                            dgvQui.Rows.Add()
                            dgvQui.Item("Accion", bytFila).Value = "N"
                            If dgvQui.Item("IDDet_Q", bytFila).Value Is Nothing Then dgvQui.Item("IDDet_Q", bytFila).Value = "M" & (dgvRow.Index + 1)
                        End If
                    End If

                    'dgvQui.Rows.Add()
                    'If strModoEdicion = "N" Then
                    '    dgvQui.Rows.Add()
                    'ElseIf strModoEdicion = "E" Then
                    '    If blnExiste Then

                    '    Else
                    '        dgvQui.Rows.Add()
                    '    End If
                    'End If

                    Dim intLiberados As Int16
                    If dgvRow.Cells("Liberados").Value Is Nothing Then
                        intLiberados = 0
                    Else
                        intLiberados = If(dgvRow.Cells("Liberados").Value.ToString = "", "0", dgvRow.Cells("Liberados").Value.ToString)
                    End If
                    Dim strTipo_Lib As Char
                    If dgvRow.Cells("Tipo_lib").Value Is Nothing Then
                        strTipo_Lib = ""
                    Else
                        strTipo_Lib = If(dgvRow.Cells("Tipo_lib").Value.ToString = "", "", dgvRow.Cells("Tipo_lib").Value.ToString)
                    End If


                    'dc = obj.CalculoCotizacion( _
                    '    strIDServicio, intIDServicio_Det, strAnio, strTipoServicio_Det, cboTipoProv.SelectedValue.ToString, _
                    '    If(dgvRow.Cells("Pax").Value.ToString = "", "0", dgvRow.Cells("Pax").Value.ToString), _
                    '    intLiberados, _
                    '    strTipo_Lib, dblMargenCotiCab, dblTipoCambioCotiCab, vdblCostoReal, vdblMargenAplicado, strTipoPaxLoc, strRedondeo, dttDetCotiServ, gsglIGV, , , blnMargenCero)
                    dc = CalculoCotizacionPreview(ListBETCamMon, _
                    strIDServicio, intIDServicio_Det, strAnio, strTipoServicio_Det, cboTipoProv.SelectedValue.ToString, _
                    If(dgvRow.Cells("Pax").Value.ToString = "", "0", dgvRow.Cells("Pax").Value.ToString), _
                    intLiberados, _
                    strTipo_Lib, dblMargenCotiCab, dblTipoCambioCotiCab, vdblCostoReal, vdblMargenAplicado, strTipoPaxLoc, strRedondeo, dttDetCotiServ, gsglIGV, , , blnMargenCero)

                    If dgvRow.Cells("IDCab_Q").Value Is Nothing Then
                        dgvQui.Item("IDCab_Q", bytFila).Value = ""
                    Else
                        dgvQui.Item("IDCab_Q", bytFila).Value = dgvRow.Cells("IDCab_Q").Value.ToString

                    End If
                    dgvQui.Item("Pax", bytFila).Value = dgvRow.Cells("Pax").Value.ToString
                    'dgvQui.Item("Liberados", bytFila).Value = dgvRow.Cells("Liberados").Value.ToString
                    dgvQui.Item("Liberados", bytFila).Value = intLiberados
                    'If dgvRow.Cells("Tipo_lib").Value.ToString = "S" Then
                    If strTipo_Lib = "S" Then
                        dgvQui.Item("Tipo_lib", bytFila).Value = "SIMPLE"
                    ElseIf strTipo_Lib = "D" Then
                        dgvQui.Item("Tipo_lib", bytFila).Value = "DOBLE"
                    Else
                        dgvQui.Item("Tipo_lib", bytFila).Value = ""
                    End If


                    For Each dic As KeyValuePair(Of String, Double) In dc
                        For Each dgvCol As DataGridViewColumn In dgvQui.Columns

                            If dgvCol.Name = dic.Key Then
                                dgvQui.Item(dgvCol.Name, bytFila).Value = dic.Value.ToString("###,###0.0000")
                            End If
                            'If dic.Key & "Impto" = dgvCol.Name Then
                            ' dgvQui.Item(dgvCol.Name, bytFila).Value = (dic.Value * ((gsglIGV / 100))).ToString("##,##0.00")
                            'End If
                        Next
                    Next

                    'dgvQui.Item("TotImpto", bytFila).Value = Convert.ToDouble(dgvQui.Item("CostoLiberadoImpto", bytFila).Value.ToString) + _
                    'Convert.ToDouble(dgvQui.Item("CostoRealImpto", bytFila).Value.ToString) + Convert.ToDouble(dgvQui.Item("MargenImpto", bytFila).Value.ToString) + _
                    'Convert.ToDouble(dgvQui.Item("MargenLiberadoImpto", bytFila).Value.ToString) + Convert.ToDouble(dgvQui.Item("SSCLImpto", bytFila).Value.ToString) + _
                    'Convert.ToDouble(dgvQui.Item("SSCRImpto", bytFila).Value.ToString) + Convert.ToDouble(dgvQui.Item("STMargenImpto", bytFila).Value.ToString) + _
                    'Convert.ToDouble(dgvQui.Item("SSMLImpto", bytFila).Value.ToString) + Convert.ToDouble(dgvQui.Item("SSMLImpto", bytFila).Value.ToString) + Convert.ToDouble(dgvQui.Item("STMargenImpto", bytFila).Value.ToString)

                    bytFila += 1
                End If
            Next


            pDgvAnchoColumnas(dgvQui)
            dgvQui_CellEnter(Nothing, Nothing)



        Catch ex As Exception
            Throw
        End Try
    End Sub


    Private Sub chkEspecial_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkEspecial.CheckedChanged
        If chkEspecial.Checked Then
            ''PPMG20151027
            If blnLoad Then
                Dim lobjVoucher As New clsVoucherOperacionBN()
                Dim strUsuOpe As String = String.Empty
                Dim lstrIdDet As String = "0"
                If strModulo = "C" Then
                    lstrIdDet = intIDDet.ToString
                ElseIf strModulo = "O" Then
                    ' lstrIdDet = frmRefRes.DgvDetalle.CurrentRow.Cells("IDOperacion_Det").Value.ToString
                ElseIf strModulo = "R" Then
                    ' lstrIdDet = frmRefRes.DgvDetalle.CurrentRow.Cells("IDReserva_Det").Value.ToString
                End If
                strUsuOpe = lobjVoucher.ConsultarVoucherxIDDetEstado(lstrIdDet, strModulo, "")
                If Not strUsuOpe.Equals("") Then
                    ''ToolTip1.ShowAlways = True
                    ''ToolTip1.SetToolTip(chkEspecial, "Existe un Voucher con estado ACEPTADO")
                    MessageBox.Show("Existe relacionado un Voucher." + vbCrLf + _
                                    "Ejecutivo de Operaciones: " + strUsuOpe, gstrTituloMsg, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                    chkEspecial.Checked = Not chkEspecial.Checked
                    Exit Sub
                End If
            End If
            lblMotivoEspecialEtiq.Enabled = True
            txtMotivoEspecial.Enabled = True
            txtMotivoEspecial.BackColor = gColorTextBoxObligat
            lblRutaDocSustentoEtiq.Enabled = True
            'txtRutaDocSustento.Enabled = True
            'txtRutaDocSustento.BackColor = gColorTextBoxObligat
            lnkDocumentoSustento.BackColor = gColorTextBoxObligat
            btnRutaArchivo.Enabled = True
            txtCostoReal.ReadOnly = False
            txtMargenAplicado.ReadOnly = False
            txtCostoRealImpto.ReadOnly = False
            txtServicio.ReadOnly = False

            txtCantidadaPagar.ReadOnly = False
            txtNetoHab.ReadOnly = False
            txtIgvHab.ReadOnly = False

            'txtCostoRealQ.ReadOnly = False
            'txtMargenAplicadoQ.ReadOnly = False

            'chkEspecial.Enabled = False

            'dblCostoRealOrigFijo = If(txtCostoReal.Text = "", "0", txtCostoReal.Text)
            'dblMargenAplicadoOrigFijo = If(txtMargenAplicado.Text = "", "0", txtMargenAplicado.Text)

            '''''Cambio comentado 26/08/2014

            'If cboTipoProv.SelectedValue = gstrTipoProveeHoteles Then
            If (blnConAlojamiento Or cboTipoProv.SelectedValue.ToString = gstrTipoProveeHoteles) And strModulo = "C" Then
                If blnLoad Then ErrPrvSS.SetError(txtCostoReal, "Se ha activado la edición de Supl. Simple Costo Neto y Supl. Triple Costo Real en el Tab de Montos Calculados")
                'txtSSCR2.Text = lblSSCR2.Text
                'txtSTCR2.Text = lblSTCR2.Text
                txtSSCR2.Visible = True
                txtSTCR2.Visible = True
                lblSSCR2.Visible = False
                lblSTCR2.Visible = False

            End If
        Else

            lblMotivoEspecialEtiq.Enabled = False
            txtMotivoEspecial.Enabled = False
            txtMotivoEspecial.BackColor = Color.White
            lblRutaDocSustentoEtiq.Enabled = False
            'txtRutaDocSustento.Enabled = False
            'txtRutaDocSustento.BackColor = Color.White
            lnkDocumentoSustento.BackColor = Color.White
            btnRutaArchivo.Enabled = False

            txtCostoReal.ReadOnly = True
            txtMargenAplicado.ReadOnly = True
            txtCostoRealImpto.ReadOnly = True
            txtServicio.ReadOnly = True
            'txtCostoRealQ.ReadOnly = False
            'txtMargenAplicadoQ.ReadOnly = False

            txtCantidadaPagar.ReadOnly = True
            txtNetoHab.ReadOnly = True
            txtIgvHab.ReadOnly = True

            'dblCostoRealOrigFijo = 0
            'dblMargenAplicadoOrigFijo = 0
            ErrPrvSS.SetError(txtCostoReal, "")
            txtSSCR2.Visible = False
            txtSTCR2.Visible = False
            'If cboTipoProv.SelectedValue = gstrTipoProveeHoteles Then
            If (blnConAlojamiento Or cboTipoProv.SelectedValue.ToString = gstrTipoProveeHoteles) And strModulo = "C" Then
                lblSSCR2.Visible = True
                lblSTCR2.Visible = True

            End If

        End If
    End Sub

    Private Sub ProcesarAcomodosEspeciales()
        If strModulo.ToString <> "C" Then Exit Sub
        If Not blnContieneAcomodoEsp Then Exit Sub
        If cboTipoProv.SelectedValue.ToString = gstrTipoProveeOperadores And blnConAlojamiento Then Exit Sub
        If frmRef Is Nothing Then Exit Sub
        Try
            If blnGrabarAcomodoEspecial And blnExisteHotelxMismoServicio() Then
                If blnEliminoAcomodosEsp Then
                    If MessageBox.Show("Desea Ud. eliminar todos los acomodos especiales del " & lblProveedor.Text.Trim & "?" _
                                       , gstrTituloMsg, MessageBoxButtons.YesNo, MessageBoxIcon.Question) = Windows.Forms.DialogResult.Yes Then
                        EliminarAcomodosEspecialesxProveedor()
                    End If
                    Exit Sub
                End If


                If MessageBox.Show("Desea Ud. aplicar el acomodo especial para los demas servicios del " _
                                   & lblProveedor.Text.Trim & "?", gstrTituloMsg, MessageBoxButtons.YesNo, MessageBoxIcon.Question) = Windows.Forms.DialogResult.Yes Then
                    InsertarAcomodosEspecialesCompletos()
                End If
            End If
        Catch ex As Exception
            Throw
        End Try
    End Sub

    Private Function blnExisteHotelxMismoServicio() As Boolean
        Try
            For Each ItemDgv As DataGridViewRow In frmRef.dgvDet.Rows
                If ItemDgv.Cells("IDProveedor").Value.ToString = strIDProveedor And ItemDgv.Cells("IDDet").Value.ToString <> strIDDet And _
                   ItemDgv.Cells("IDServicio_Det").Value.ToString = intIDServicio_Det.ToString Then
                    Return True
                End If
            Next
            Return False
        Catch ex As Exception
            Throw
        End Try
    End Function

    Private Sub EliminarAcomodosEspecialesxProveedor()
        Try
            'Lista de IDDet's cuyos acomodos especiales y su distribución sera eliminada.
            Dim ListIDDet As New List(Of String)
            For Each ItemDgv As DataGridViewRow In frmRef.dgvDet.Rows
                If ItemDgv.Cells("IDProveedor").Value.ToString = strIDProveedor And ItemDgv.Cells("IDDet").Value.ToString <> strIDDet And _
                   ItemDgv.Cells("IDServicio_Det").Value.ToString = intIDServicio_Det.ToString And CBool(ItemDgv.Cells("AcomodoEspecial").Value) Then
                    ListIDDet.Add(ItemDgv.Cells("IDDet").Value.ToString)
                    ItemDgv.Cells("AcomodoEspecial").Value = "0"
                    frmRef.AgregarEditarValorStructDetCoti(ItemDgv.Index, "")
                End If
            Next

            Dim objBLAcomodo = Nothing 'As New clsCotizacionBN.clsDetalleCotizacionBN.clsDetalleCotizacionAcomodoEspecialBN
            Dim RegST As stAcodomodoEspecial
            Dim LstEliminarAcomodoEsp As New List(Of stAcodomodoEspecial)
            For Each myIDDet As String In ListIDDet
                Dim LstAcomodoxDel = stExistenAcomodosEspecialesEnStruct(myIDDet)
                If LstAcomodoxDel.Count > 0 Then
                    For Each Item As stAcodomodoEspecial In LstAcomodoxDel
                        RegST = New stAcodomodoEspecial
                        With RegST
                            .QtPax = Item.QtPax
                            .IDDet = Item.IDDet
                            .IDCab = Item.IDCab
                            .CoCapacidad = Item.CoCapacidad
                            .Almacenado = Item.Almacenado
                            .Accion = "B"
                        End With
                        LstEliminarAcomodoEsp.Add(RegST)
                    Next
                Else
                    If IsNumeric(myIDDet) Then
                        objBLAcomodo = New clsCotizacionBN.clsDetalleCotizacionBN.clsDetalleCotizacionAcomodoEspecialBN
                        Dim dtAcomodoxDel As DataTable = objBLAcomodo.ConsultarListxIDDet(myIDDet)
                        For Each dRow As DataRow In dtAcomodoxDel.Rows
                            With RegST
                                .QtPax = dRow("QtPax")
                                .IDDet = myIDDet
                                .IDCab = dRow("IDCAB")
                                .CoCapacidad = dRow("CoCapacidad")
                                .Almacenado = ""
                                .Accion = "B"
                            End With
                            LstEliminarAcomodoEsp.Add(RegST)
                        Next
                    End If
                End If
                BorrarSTAcomodoEspecialxIDDet(myIDDet, intIDCab)
            Next

            'Insertar en la Lista de Acomodos Especiales la lista de eliminación de los acmodos especiales
            For Each ItemDel As stAcodomodoEspecial In LstEliminarAcomodoEsp
                Dim RegVal As New stAcodomodoEspecial
                With RegVal
                    .QtPax = ItemDel.QtPax
                    .IDDet = ItemDel.IDDet
                    .IDCab = ItemDel.IDCab
                    .CoCapacidad = ItemDel.CoCapacidad
                    .Almacenado = ItemDel.Almacenado
                    .Accion = ItemDel.Accion
                End With
                objLstAcodomodoEspecial.Add(RegVal)
            Next
        Catch ex As Exception
            Throw
        End Try
    End Sub

    Private Sub InsertarAcomodosEspecialesCompletos()
        Try
            'Lista de IDDet's cuyos acomodos especiales y su distribución seran migrados.
            Dim ListIDDet As New List(Of String)
            For Each ItemDgv As DataGridViewRow In frmRef.dgvDet.Rows
                If ItemDgv.Cells("IDProveedor").Value.ToString = strIDProveedor And ItemDgv.Cells("IDDet").Value.ToString <> strIDDet And _
                   ItemDgv.Cells("IDServicio_Det").Value.ToString = intIDServicio_Det.ToString Then
                    ListIDDet.Add(ItemDgv.Cells("IDDet").Value.ToString)
                    ItemDgv.Cells("AcomodoEspecial").Value = "1"
                    frmRef.AgregarEditarValorStructDetCoti(ItemDgv.Index, "")
                End If
            Next

            Dim LstInsAcomodoEspecial As New List(Of stAcodomodoEspecial)
            Dim LstInsAcomodoEspecialDistrib As New List(Of stAcodomodoEspecialDistribucion)
            Dim blnEncontroST As Boolean = False
            For Each ThisIDDet As String In ListIDDet
                'Borrar Datos Acomodos especiales
                BorrarDatosAcomodosEspecialesAntiguos(ThisIDDet)

                'Migrando los Acomodos Especiales
                For Each Sitem As stAcodomodoEspecial In objLstAcodomodoEspecial
                    If Sitem.IDCab = intIDCab And Sitem.IDDet = strIDDet And _
                            (Sitem.Accion = "N" Or Sitem.Accion = Nothing) Then
                        Dim RegValST As New stAcodomodoEspecial
                        With RegValST
                            .IDCab = Sitem.IDCab
                            .CoCapacidad = Sitem.CoCapacidad
                            .IDDet = ThisIDDet
                            .QtPax = Sitem.QtPax
                            .Almacenado = Sitem.Almacenado
                            .Accion = "N" 'Sitem.Accion
                        End With
                        LstInsAcomodoEspecial.Add(RegValST)
                    End If
                Next

                'Migrando la Distribución de Acomodos Especiales
                For Each Sitem2 As stAcodomodoEspecialDistribucion In objLstAcodomodoEspecialDistribucion
                    If Sitem2.IDCab = intIDCab And Sitem2.IDDet = strIDDet And _
                       Sitem2.Accion = "N" Then
                        Dim RegValSTDst As New stAcodomodoEspecialDistribucion
                        With RegValSTDst
                            .CoCapacidad = Sitem2.CoCapacidad
                            .IDCab = intIDCab
                            .IDDet = ThisIDDet
                            .IDHabit = Sitem2.IDHabit
                            .IDPax = Sitem2.IDPax
                            .UserMod = Sitem2.UserMod
                            .Accion = "N" 'Sitem2.Accion
                        End With
                        LstInsAcomodoEspecialDistrib.Add(RegValSTDst)
                    End If
                Next

                Dim oListDetPaxTemp As New List(Of stDetCotiPax)
                'Eliminando los datos Seleccionados.
                If blnExisteSTCotDetPax(ThisIDDet) Then
                    BorrarSTDetPaxxIDCab(intIDCab, ThisIDDet)
                End If

                Dim oSTDetPax As stDetCotiPax
                'Insertando los datos Al Temp
                For i = 0 To lvwPax.Items.Count - 1
                    Dim blnChecked As Boolean = lvwPax.Items(i).Checked
                    oSTDetPax = New stDetCotiPax
                    With oSTDetPax
                        .IDDet = ThisIDDet
                        .IDTipoProv = String.Empty
                        .IDPax = lvwPax.Items(i).SubItems(3).Text
                        .IDCAB = intIDCab
                        .Selecc = blnChecked
                        .Accion = If(blnChecked, "N", "B")
                    End With
                    oListDetPaxTemp.Add(oSTDetPax)
                Next


                Dim oSTDetPax2 As stDetCotiPax
                'Insertando los datos Al Temp
                For Each ItemST As stDetCotiPax In oListDetPaxTemp
                    oSTDetPax2 = New stDetCotiPax
                    With oSTDetPax2
                        .IDDet = ItemST.IDDet
                        .IDTipoProv = ItemST.IDTipoProv
                        .IDPax = ItemST.IDPax
                        .IDCAB = intIDCab
                        .Selecc = ItemST.Selecc
                        .Accion = ItemST.Accion
                    End With
                    objLstDetCotiPax.Add(oSTDetPax2)
                Next

                'Actualizando el NroPax + Liberados
                Dim intNroPax As Int16 = CInt(txtNroPax.Text.Trim)
                Dim intNroPaxLiberados As Int16 = CInt(If(txtNroLiberados.Text.Trim = String.Empty, 0, CInt(txtNroLiberados.Text.Trim)))
                Dim strPaxMasLiberados As String = intNroPax.ToString() & If(intNroPaxLiberados > 0, "+" & intNroPaxLiberados.ToString(), String.Empty)
                For Each Item As DataGridViewRow In frmRef.dgvDet.Rows
                    If Item.Cells("IDDet").Value.ToString = ThisIDDet Then
                        Item.Cells("NroPax").Value = intNroPax.ToString()
                        Item.Cells("PaxmasLiberados").Value = strPaxMasLiberados
                        Item.Cells("NroLiberados").Value = intNroPaxLiberados.ToString()
                        frmRef.AgregarEditarValorStructDetCoti(Item.Index, String.Empty)
                    End If
                Next
            Next

            'Insertandolos a las Listas Generales
            For Each STItem As stAcodomodoEspecial In LstInsAcomodoEspecial
                Dim RegVal As New stAcodomodoEspecial
                With RegVal
                    .IDCab = STItem.IDCab
                    .QtPax = STItem.QtPax
                    .CoCapacidad = STItem.CoCapacidad
                    .IDDet = STItem.IDDet
                    .Almacenado = STItem.Almacenado
                    .Accion = STItem.Accion
                End With
                objLstAcodomodoEspecial.Add(RegVal)
            Next

            For Each STitem As stAcodomodoEspecialDistribucion In LstInsAcomodoEspecialDistrib
                Dim RegValSTDst As New stAcodomodoEspecialDistribucion
                With RegValSTDst
                    .CoCapacidad = STitem.CoCapacidad
                    .IDCab = intIDCab
                    .IDDet = STitem.IDDet
                    .IDHabit = STitem.IDHabit
                    .IDPax = STitem.IDPax
                    .UserMod = STitem.UserMod
                    .Accion = STitem.Accion
                End With
                objLstAcodomodoEspecialDistribucion.Add(RegValSTDst)
            Next
        Catch ex As Exception
            Throw
        End Try
    End Sub

    Private Function stExistenAcomodosEspecialesEnStruct(ByVal vstrIDDet As String) As List(Of stAcodomodoEspecial)
        Try
            Dim LstTempAcomodosEspeciales As New List(Of stAcodomodoEspecial)
            For Each ST As stAcodomodoEspecial In objLstAcodomodoEspecial
                If ST.IDDet = vstrIDDet And ST.IDCab = intIDCab And ST.Accion <> "B" Then
                    LstTempAcomodosEspeciales.Add(ST)
                End If
            Next
            Return LstTempAcomodosEspeciales
        Catch ex As Exception
            Throw
        End Try
    End Function

    Private Sub BorrarDatosAcomodosEspecialesAntiguos(ByVal vstrIDDet As String)
        Try
            Dim objBLAcomodo = Nothing
            Dim RegST As New stAcodomodoEspecial
            Dim LstEliminarAcomodoEsp As New List(Of stAcodomodoEspecial)
            Dim LstAcomodoxDel = stExistenAcomodosEspecialesEnStruct(vstrIDDet)
            If LstAcomodoxDel.Count > 0 Then
                For Each Item As stAcodomodoEspecial In LstAcomodoxDel
                    RegST = New stAcodomodoEspecial
                    With RegST
                        .QtPax = Item.QtPax
                        .IDDet = Item.IDDet
                        .IDCab = Item.IDCab
                        .CoCapacidad = Item.CoCapacidad
                        .Almacenado = Item.Almacenado
                        .Accion = "B"
                    End With
                    LstEliminarAcomodoEsp.Add(RegST)
                Next
            Else
                If IsNumeric(vstrIDDet) Then
                    objBLAcomodo = New clsCotizacionBN.clsDetalleCotizacionBN.clsDetalleCotizacionAcomodoEspecialBN
                    Dim dtAcomodoxDel As DataTable = objBLAcomodo.ConsultarListxIDDet(vstrIDDet)
                    For Each dRow As DataRow In dtAcomodoxDel.Rows
                        With RegST
                            .QtPax = dRow("QtPax")
                            .IDDet = vstrIDDet
                            .IDCab = dRow("IDCAB")
                            .CoCapacidad = dRow("CoCapacidad")
                            .Almacenado = ""
                            .Accion = "B"
                        End With
                        LstEliminarAcomodoEsp.Add(RegST)
                    Next
                End If
            End If
            BorrarSTAcomodoEspecialxIDDet(vstrIDDet, intIDCab)

            For Each ItemDel As stAcodomodoEspecial In LstEliminarAcomodoEsp
                Dim RegVal As New stAcodomodoEspecial
                With RegVal
                    .QtPax = ItemDel.QtPax
                    .IDDet = ItemDel.IDDet
                    .IDCab = ItemDel.IDCab
                    .CoCapacidad = ItemDel.CoCapacidad
                    .Almacenado = ItemDel.Almacenado
                    .Accion = ItemDel.Accion
                End With
                objLstAcodomodoEspecial.Add(RegVal)
            Next
        Catch ex As Exception
            Throw
        End Try
    End Sub

    Private Sub txtCostoReal_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) _
        Handles txtCostoReal.KeyPress, txtMargenAplicado.KeyPress, txtCostoRealImpto.KeyPress, txtServicio.KeyPress, _
        txtIgvHab.KeyPress, txtNetoHab.KeyPress, txtTotalHab.KeyPress, txtSSCR2.KeyPress, txtSTCR2.KeyPress
        If sender.name <> "txtServicio" And Not e Is Nothing Then
            If Asc(e.KeyChar) <> 8 Then
                If sender.name = "txtSTCR2" Then
                    If InStr("-0123456789.,", e.KeyChar) = 0 Then
                        e.KeyChar = ""
                    End If
                Else
                    If InStr("0123456789.,", e.KeyChar) = 0 Then
                        e.KeyChar = ""
                    End If
                End If
            End If
        End If

        'If chkEspecial.Checked Then
        '    Select Case sender.name
        '        Case "txtCostoReal"
        '            'txtCostoReal.BackColor = gColorTextBoxModificado
        '            If Not txtCostoReal.ReadOnly Then txtCostoReal.ForeColor = Color.Red
        '        Case "txtMargenAplicado"
        '            'txtMargenAplicado.BackColor = gColorTextBoxModificado
        '            If Not txtMargenAplicado.ReadOnly Then txtMargenAplicado.ForeColor = Color.Red
        '        Case "txtCostoRealImpto"
        '            'txtCostoRealImpto.BackColor = gColorTextBoxModificado
        '            If Not txtCostoRealImpto.ReadOnly Then txtCostoRealImpto.ForeColor = Color.Red
        '        Case "txtServicio"
        '            If Not txtServicio.ReadOnly Then txtServicio.ForeColor = Color.Red
        '    End Select
        'End If


        Select Case sender.name
            Case "txtNetoHab"
                If chkEspecial.Checked Then
                    txtNetoHab.ForeColor = Color.Red
                    blnNetoHabEditado = True
                End If
            Case "txtIgvHab"
                If chkEspecial.Checked Then
                    txtIgvHab.ForeColor = Color.Red
                    blnIgvHabEditado = True
                End If
            Case "txtCostoReal"
                If chkEspecial.Checked Then
                    blnCostoRealEditado = True
                    txtCostoReal.ForeColor = Color.Red
                End If
            Case "txtMargenAplicado"
                If chkEspecial.Checked Then
                    blnMargenAplicadoEditado = True
                    txtMargenAplicado.ForeColor = Color.Red
                End If
            Case "txtCostoRealImpto"
                If chkEspecial.Checked Then
                    blnCostoRealImptoEditado = True
                    txtCostoRealImpto.ForeColor = Color.Red
                End If
            Case "txtServicio"
                If chkEspecial.Checked Then
                    blnServicioEditado = True
                    txtServicio.ForeColor = Color.Red
                End If
            Case "txtSSCR2"
                If chkEspecial.Checked Then
                    blnSSCREditado = True
                    ErrPrvSS.SetError(txtCostoReal, "")
                End If

            Case "txtSTCR2"
                If chkEspecial.Checked Then
                    blnSTCREditado = True
                    ErrPrvSS.SetError(txtCostoReal, "")
                End If

        End Select

    End Sub

    Private Sub ColorearTextoCamposEditados()
        If blnCostoRealEditado Then
            txtCostoReal.ForeColor = Color.Red
        End If
        If blnCostoRealImptoEditado Then
            txtCostoRealImpto.ForeColor = Color.Red
        End If
        If blnMargenAplicadoEditado Then
            txtMargenAplicado.ForeColor = Color.Red
        End If
        If blnServicioEditado Then
            txtServicio.ForeColor = Color.Red
        End If
        If blnSSCREditado Then
            txtSSCR2.ForeColor = Color.Red
        End If
        If blnSTCREditado Then
            txtSTCR2.ForeColor = Color.Red
        End If
    End Sub

    Private Sub ColorearTextoCamposEditadosReserva()
        If blnCantidadAPagarEditado Then
            txtCantidadaPagar.ForeColor = Color.Red
        End If
        If blnNetoHabEditado Then
            txtNetoHab.ForeColor = Color.Red
        End If
        If blnIgvHabEditado Then
            txtIgvHab.ForeColor = Color.Red
        End If
    End Sub

    Private Sub txtCostoReal_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtCostoReal.LostFocus, txtMargenAplicado.LostFocus, _
    txtSSCR2.LostFocus, txtSTCR2.LostFocus
        If Not chkEspecial.Checked Then Exit Sub
        Try

            Dim dblCostoReal As Double = -1
            Dim dblMargenAplicado As Double = -1
            Dim dblSSCR As Double = -1
            Dim dblSTCR As Double = -1
            Dim dblCostoRealTxt As Double = If(txtCostoReal.Text = "", "0", txtCostoReal.Text)
            Dim dblMargenAplicadoTxt As Double = If(txtMargenAplicado.Text = "", "0", txtMargenAplicado.Text)
            Dim dblSSCRTxt As Double = If(txtSSCR2.Text = "", "0", txtSSCR2.Text)
            Dim dblSTCRTxt As Double = If(txtSTCR2.Text = "", "0", txtSTCR2.Text)

            'If dblCostoRealOrigFijo <> 0 Then
            '    If dblCostoRealTxt <> dblCostoRealOrigFijo Then
            '        dblCostoReal = dblCostoRealTxt
            '    End If
            'End If
            'If dblMargenAplicadoOrigFijo <> 0 Then
            '    If dblMargenAplicadoTxt <> dblMargenAplicadoOrigFijo Then
            '        dblMargenAplicado = dblMargenAplicadoTxt
            '    End If
            'End If
            If blnCostoRealEditado Then
                dblCostoReal = dblCostoRealTxt
            End If
            If blnMargenAplicadoEditado Then
                dblMargenAplicado = dblMargenAplicadoTxt
            End If
            If blnSSCREditado Then
                dblSSCR = dblSSCRTxt
            End If
            If blnSTCREditado Then
                dblSTCR = dblSTCRTxt
            End If
            CalcularMontos(dblCostoReal, dblMargenAplicado, dblCostoReal, _
                           dblMargenAplicado, strTipoPaxNacionalExtranjero, _
                           dblSSCR, dblSTCR)

            'If Not chkEspecial.Checked Then
            '    dblCostoRealOrigFijo = If(txtCostoReal.Text = "", "0", txtCostoReal.Text)
            '    dblMargenAplicadoOrigFijo = If(txtMargenAplicado.Text = "", "0", txtMargenAplicado.Text)
            'End If
            CargarToolTip()
        Catch ex As Exception
            MessageBox.Show(ex.Message, ex.Source, MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try

    End Sub

    'Private Sub txtCostoRealQ_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtCostoRealQ.LostFocus, txtMargenAplicadoQ.LostFocus
    '    If Not chkEspecial.Checked Then Exit Sub
    '    Try
    '        CalcularMontosQuiebres(If(chkEspecial.Checked, txtCostoReal.Text, "-1"), If(chkEspecial.Checked, txtMargenAplicado.Text, "-1"), strTipoPaxNacionalExtranjero)
    '    Catch ex As Exception
    '        MessageBox.Show(ex.Message, ex.Source, MessageBoxButtons.OK, MessageBoxIcon.Error)
    '    End Try

    'End Sub

    Private Sub btnQuiebres_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnQuiebres.Click
        Dim dblCostoReal As Double = -1
        Dim dblMargenAplicado As Double = -1
        Dim dblSSCR As Double = -1
        Dim dblSTCR As Double = -1
        Dim dblCostoRealTxt As Double = If(txtCostoReal.Text = "", "0", txtCostoReal.Text)
        Dim dblMargenAplicadoTxt As Double = If(txtMargenAplicado.Text = "", "0", txtMargenAplicado.Text)
        Dim dblSSCRTxt As Double = If(txtSSCR2.Text = "", "0", txtSSCR2.Text)
        Dim dblSTCRTxt As Double = If(txtSTCR2.Text = "", "0", txtSTCR2.Text)

        'If dblCostoRealOrigFijo <> 0 Then
        '    If dblCostoRealTxt <> dblCostoRealOrigFijo Then
        '        dblCostoReal = dblCostoRealTxt
        '    End If
        'End If
        'If dblMargenAplicadoOrigFijo <> 0 Then
        '    If dblMargenAplicadoTxt <> dblMargenAplicadoOrigFijo Then
        '        dblMargenAplicado = dblMargenAplicadoTxt
        '    End If
        'End If


        If intIDDet = 0 Then
            Try


                If blnCostoRealEditado Then
                    dblCostoReal = dblCostoRealTxt
                End If
                If blnMargenAplicadoEditado Then
                    dblMargenAplicado = dblMargenAplicadoTxt
                End If
                If blnSSCREditado Then
                    dblSSCR = dblSSCRTxt
                End If
                If blnSTCREditado Then
                    dblSTCR = dblSTCRTxt
                End If
                CalcularMontos(dblCostoReal, dblMargenAplicado, _
                   dblCostoReal, dblMargenAplicado, strTipoPaxNacionalExtranjero, _
                    dblSSCR, dblSTCR)

                'If Not chkEspecial.Checked Then
                '    dblCostoRealOrigFijo = If(txtCostoReal.Text = "", "0", txtCostoReal.Text)
                '    dblMargenAplicadoOrigFijo = If(txtMargenAplicado.Text = "", "0", txtMargenAplicado.Text)
                'End If
            Catch ex As Exception
                MessageBox.Show(ex.Message, ex.Source, MessageBoxButtons.OK, MessageBoxIcon.Error)
            End Try

        End If


        CalcularMontosQuiebres(dblCostoReal, dblMargenAplicadoTxt, strTipoPaxNacionalExtranjero)

        TabPage2.Parent = Tab
        Tab.SelectTab(TabPage2)
    End Sub

    Private Sub dgvQui_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvQui.CellContentClick

    End Sub
    Private Sub LimpiaCtrlsMontosQuiebres()
        txtCostoRealQ.Text = "0.0000"
        lblMargenQ.Text = "0.0000"
        txtMargenAplicadoQ.Text = "0.00"
        lblCostoLiberadoQ.Text = "0.0000"
        lblMargenLiberadoQ.Text = "0.0000"
        lblTotalQ.Text = "0.0000"
        lblSSCLQ.Text = "0.0000"
        lblSSCRQ.Text = "0.0000"
        lblSSMargenQ.Text = "0.0000"
        lblSSMLQ.Text = "0.0000"
        lblSSTotalQ.Text = "0.0000"
        lblRedondeoQ.Text = "0.0000"
        lblCostoLiberadoImptoQ.Text = "0.0000"
        lblCostoRealImptoQ.Text = "0.0000"
        lblMargenImptoQ.Text = "0.0000"
        lblMargenLiberadoImptoQ.Text = "0.0000"
        lblSSCLImptoQ.Text = "0.0000"
        lblSSCRImptoQ.Text = "0.0000"
        lblSSMargenImptoQ.Text = "0.0000"
        lblSSMLImptoQ.Text = "0.0000"
        lblSTCRImptoQ.Text = "0.0000"
        lblSTMargenImptoQ.Text = "0.0000"
        lblTotImptoQ.Text = "0.0000"

    End Sub
    Private Sub dgvQui_CellEnter(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvQui.CellEnter
        If dgvQui.CurrentRow Is Nothing Then
            LimpiaCtrlsMontosQuiebres()
            Exit Sub
        End If

        With dgvQui.CurrentRow
            If .Cells("CostoReal").Value Is Nothing Then
                LimpiaCtrlsMontosQuiebres()
                Exit Sub
            End If

            txtCostoRealQ.Text = .Cells("CostoReal").Value.ToString
            lblMargenQ.Text = .Cells("Margen").Value.ToString
            txtMargenAplicadoQ.Text = .Cells("MargenAplicado").Value.ToString
            lblCostoLiberadoQ.Text = .Cells("CostoLiberado").Value.ToString
            lblMargenLiberadoQ.Text = .Cells("MargenLiberado").Value.ToString
            lblTotalQ.Text = .Cells("Total").Value.ToString
            lblSSCLQ.Text = .Cells("SSCL").Value.ToString
            lblSSCRQ.Text = .Cells("SSCR").Value.ToString
            lblSSMargenQ.Text = .Cells("SSMargen").Value.ToString
            lblSSMLQ.Text = .Cells("SSML").Value.ToString
            lblSSTotalQ.Text = .Cells("SSTotal").Value.ToString
            lblRedondeoQ.Text = .Cells("RedondeoTotal").Value.ToString
            lblCostoLiberadoImptoQ.Text = .Cells("CostoLiberadoImpto").Value.ToString
            lblCostoRealImptoQ.Text = .Cells("CostoRealImpto").Value.ToString
            lblMargenImptoQ.Text = .Cells("MargenImpto").Value.ToString
            lblMargenLiberadoImptoQ.Text = .Cells("MargenLiberadoImpto").Value.ToString
            lblSSCLImptoQ.Text = .Cells("SSCLImpto").Value.ToString
            lblSSCRImptoQ.Text = .Cells("SSCRImpto").Value.ToString
            lblSSMargenImptoQ.Text = .Cells("SSMargenImpto").Value.ToString
            lblSSMLImptoQ.Text = .Cells("SSMargenLiberadoImpto").Value.ToString

            lblSTCRQ.Text = .Cells("STCR").Value.ToString
            lblSTMargenQ.Text = .Cells("STMargen").Value.ToString
            lblSTTotalQ.Text = .Cells("STTotal").Value.ToString

            lblSTCRImptoQ.Text = .Cells("STCRImpto").Value.ToString
            lblSTMargenImptoQ.Text = .Cells("STMargenImpto").Value.ToString
            lblTotImptoQ.Text = .Cells("TotImpto").Value.ToString

            txtMargenAplicadoQ.Text = Format(CDbl(txtMargenAplicadoQ.Text), "###,###0.00")
            lblTotalQ.Text = Format(CDbl(lblTotalQ.Text), "###,###0.00")
        End With
    End Sub


    Private Sub btnReCalcular_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnReCalcular.Click

        If MessageBox.Show("¿Está Ud. seguro de recalcular los montos en pantalla?", gstrTituloMsg, MessageBoxButtons.YesNo, MessageBoxIcon.Question) = Windows.Forms.DialogResult.No Then Exit Sub

        Try

            ''->PPMG20160330
            Dim objCotDetBN As New clsCotizacionBN.clsDetalleCotizacionBN
            Dim lDblValorZicasso As Double = 0, lBlnEsZicasso As Boolean = False
            lBlnEsZicasso = objCotDetBN.blnDetEsZicasso(strIDProveedor, _
                                               strIDServicio, intIDServicio_Det)
            If lBlnEsZicasso Then
                lDblValorZicasso = objCotDetBN.dblDetValorZicasso(intIDCab)
                txtCostoReal.Text = lDblValorZicasso.ToString
                lblTotal.Text = lDblValorZicasso.ToString
                Exit Sub
            End If
            ''<-PPMG20160330

            blnRecalTarifaPendiente = False
            Dim dblCostoReal As Double = -1
            Dim dblMargenAplicado As Double = -1
            Dim dblSSCR As Double = -1
            Dim dblSTCR As Double = -1

            Dim dblCostoRealTxt As Double = If(txtCostoReal.Text = "", "0", txtCostoReal.Text)
            Dim dblMargenAplicadoTxt As Double = If(txtMargenAplicado.Text = "", "0", txtMargenAplicado.Text)
            Dim dblSSCRTxt As Double = If(txtSSCR2.Text = "", "0", txtSSCR2.Text)
            Dim dblSTCRTxt As Double = If(txtSTCR2.Text = "", "0", txtSTCR2.Text)

            'If dblCostoRealOrigFijo <> 0 Then
            '    If dblCostoRealTxt <> dblCostoRealOrigFijo Then
            '        dblCostoReal = dblCostoRealTxt
            '    End If
            'End If
            'If dblMargenAplicadoOrigFijo <> 0 Then
            '    If dblMargenAplicadoTxt <> dblMargenAplicadoOrigFijo Then
            '        dblMargenAplicado = dblMargenAplicadoTxt
            '    End If
            'End If

            If blnCostoRealEditado Then
                dblCostoReal = dblCostoRealTxt
            End If
            If blnMargenAplicadoEditado Then
                dblMargenAplicado = dblMargenAplicadoTxt
            End If
            If blnSSCREditado Then
                dblSSCR = dblSSCRTxt
            End If
            If blnSTCREditado Then
                dblSTCR = dblSTCRTxt
            End If
            CalcularMontos(dblCostoReal, dblMargenAplicado, _
                           dblCostoReal, dblMargenAplicado, strTipoPaxNacionalExtranjero, _
                            dblSSCR, dblSTCR)

            'If Not chkEspecial.Checked Then
            '    dblCostoRealOrigFijo = If(txtCostoReal.Text = "", "0", txtCostoReal.Text)
            '    dblMargenAplicadoOrigFijo = If(txtMargenAplicado.Text = "", "0", txtMargenAplicado.Text)
            'End If

        Catch ex As Exception
            MessageBox.Show(ex.Message, ex.Source, MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub frmIngDetCotizacionDatoMem_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        If blnCambios = True Then
            If MessageBox.Show("Se han realizado cambios. ¿Está Ud. seguro de salir sin Aceptar?", "Confirmar", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = Windows.Forms.DialogResult.Yes Then
                e.Cancel = False

                If blnGrabarAcomodoEspecial Then
                    Dim str_vHabitacionId As String = ""
                    Dim int_vPax As Integer = 0
                    Dim oDatos As stAcodomodoEspecial
                    For int_vContador As Int16 = 0 To objLstAcodomodoEspecial.Count - 1
                        With objLstAcodomodoEspecial(0)
                            str_vHabitacionId = .CoCapacidad
                            int_vPax = .QtPax
                            objLstAcodomodoEspecial.RemoveAt(0)

                            oDatos.IDCab = intIDCab
                            oDatos.IDDet = intIDDet
                            oDatos.CoCapacidad = str_vHabitacionId
                            oDatos.QtPax = int_vPax
                            oDatos.Accion = ""
                            objLstAcodomodoEspecial.Add(oDatos)
                        End With
                    Next

                    Dim int_vHabitacionSecuencia As Int16 = 0
                    Dim oDatosDistribucion As stAcodomodoEspecialDistribucion
                    For int_vContador As Int16 = 0 To objLstAcodomodoEspecialDistribucion.Count - 1
                        With objLstAcodomodoEspecialDistribucion(0)
                            str_vHabitacionId = .CoCapacidad
                            int_vPax = .IDPax
                            int_vHabitacionSecuencia = .IDHabit
                            objLstAcodomodoEspecialDistribucion.RemoveAt(0)

                            oDatosDistribucion = New stAcodomodoEspecialDistribucion
                            oDatosDistribucion.IDCab = intIDCab
                            oDatosDistribucion.IDDet = intIDDet
                            oDatosDistribucion.CoCapacidad = str_vHabitacionId
                            oDatosDistribucion.IDPax = int_vPax
                            oDatosDistribucion.IDHabit = int_vHabitacionSecuencia
                            oDatosDistribucion.Accion = ""
                            objLstAcodomodoEspecialDistribucion.Add(oDatosDistribucion)
                        End With
                    Next

                End If
            Else
                e.Cancel = True
            End If
        End If

    End Sub

    Private Sub cboPais_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboPais.SelectedIndexChanged

    End Sub

    Private Sub btnRutaArchivo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnRutaArchivo.Click
        'ofdRuta.InitialDirectory = Application.LocalUserAppDataPath
        ofdRuta.FileName = ""
        ofdRuta.Filter = "Word (*.docx)|*.docx|Excel (*.xlsx)|*.xlsx|PDF (*.pdf)|*.pdf|Todos (*.*)|*.*"
        ofdRuta.ShowDialog()
        'txtRutaDocSustento.Text = ofdRuta.FileName
        lnkDocumentoSustento.Text = ofdRuta.FileName
    End Sub

    Private Sub cboTipoProv_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboTipoProv.SelectedIndexChanged

    End Sub

    Private Sub txtCostoReal_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtCostoReal.TextChanged

    End Sub

    Private Sub txtCostoRealQ_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtCostoRealQ.TextChanged, _
        txtMargenAplicadoQ.TextChanged, _
        lblSTCRQ.TextChanged, lblSTMargenQ.TextChanged, lblSTTotalQ.TextChanged, _
        lblCostoLiberadoImptoQ.TextChanged, lblCostoRealImptoQ.TextChanged, lblMargenImptoQ.TextChanged, _
        lblMargenLiberadoImptoQ.TextChanged, lblSSCLImptoQ.TextChanged, lblSSCRImptoQ.TextChanged, _
        lblSSMargenImptoQ.TextChanged, lblSSMLImptoQ.TextChanged, lblSTCRImptoQ.TextChanged, _
        lblSTMargenImptoQ.TextChanged

        If dgvQui.CurrentRow Is Nothing Then Exit Sub
        With dgvQui.CurrentRow
            Select Case sender.name
                Case "txtCostoRealQ" : .Cells("CostoReal").Value = txtCostoRealQ.Text
                Case "Margen" : .Cells("Margen").Value = lblMargenQ.Text
                Case "txtMargenAplicadoQ" : .Cells("MargenAplicado").Value = txtMargenAplicadoQ.Text
                Case "txtMargenLiberadoQ" : .Cells("MargenLiberado").Value = lblMargenLiberadoQ.Text
                Case "txtTotalQ" : .Cells("Total").Value = lblTotalQ.Text
                Case "txtSSCLQ" : .Cells("SSCL").Value = lblSSCLQ.Text
                Case "txtSSCRQ" : .Cells("SSCR").Value = lblSSCRQ.Text

                Case "txtSSMargenQ" : .Cells("SSMargen").Value = lblSSMargenQ.Text
                Case "txtSSMLQ" : .Cells("SSML").Value = lblSSMLQ.Text
                Case "lblCostoLiberadoImptoQ" : .Cells("CostoLiberadoImpto").Value = lblCostoLiberadoImptoQ.Text
                Case "lblCostoRealImptoQ" : .Cells("CostoRealImpto").Value = lblCostoRealImptoQ.Text
                Case "lblMargenImptoQ" : .Cells("MargenImpto").Value = lblMargenImptoQ.Text
                Case "lblMargenLiberadoImptoQ" : .Cells("MargenLiberadoImpto").Value = lblMargenLiberadoImptoQ.Text
                Case "lblSSCLImptoQ" : .Cells("SSCLImpto").Value = lblSSCLImptoQ.Text
                Case "lblSSCRImptoQ" : .Cells("SSCRImpto").Value = lblSSCRImptoQ.Text
                Case "lblSSMargenImptoQ" : .Cells("SSMargenImpto").Value = lblSSMargenImptoQ.Text
                Case "lblSSMLImptoQ" : .Cells("SSMargenLiberadoImpto").Value = lblSSMLImptoQ.Text
                Case "lblSTCRImptoQ" : .Cells("STCRImpto").Value = lblSTCRImptoQ.Text
                Case "lblSTMargenImptoQ" : .Cells("STMargenImpto").Value = lblSTMargenImptoQ.Text
                Case "lblTotImptoQ" : .Cells("TotImpto").Value = lblTotImptoQ.Text

                Case "lblSTCRQ" : .Cells("STCR").Value = lblSTCRQ.Text
                Case "lblSTMargenQ" : .Cells("STMargen").Value = lblSTMargenQ.Text
                Case "lblSTTotalQ" : .Cells("STTotal").Value = lblSTTotalQ.Text
            End Select

        End With

    End Sub

    Private Sub txtNroPax_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtNroPax.KeyPress, _
        txtCantidad.KeyPress, txtNroLiberados.KeyPress, txtCantidadaPagar.KeyPress, txtPaxTotal.KeyPress, txtGruposPax.KeyPress
        If Asc(e.KeyChar) <> 8 Then
            If Not IsNumeric(e.KeyChar) Then
                e.KeyChar = ""
            End If
        End If

        Select Case sender.name
            Case "txtCantidadaPagar"
                If chkEspecial.Checked Then
                    txtCantidadaPagar.ForeColor = Color.Red
                    blnCantidadAPagarEditado = True
                End If
        End Select
    End Sub

    Private Sub chkSelPax_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkSelPax.CheckedChanged
        If chkSelPax.Checked Then
            If Not chkIncGuia.Checked Then
                grbPax.Enabled = True
                grbPax.Height = 140
            End If
        Else

            'txtNroPax.Text = intPax
            'txtNroLiberados.Text = intLiberados
            'cboTipoLib.SelectedValue = strTipo_Lib

            'If Convert.ToDouble(txtCostoReal.Text) > 0 Then
            '    CalcularMontos(If(chkEspecial.Checked, txtCostoReal.Text, "0"), If(chkEspecial.Checked, txtMargenAplicado.Text, "0"), _
            '    If(chkEspecial.Checked, txtCostoRealQ.Text, "0"), If(chkEspecial.Checked, txtMargenAplicadoQ.Text, "0"))
            'End If

            'CargarPaxLvw()

            grbPax.Enabled = False
            grbPax.Height = 45
        End If
    End Sub

    Private Sub txtNroPax_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtNroPax.LostFocus, _
        txtNroLiberados.LostFocus, cboTipoLib.SelectionChangeCommitted

        If Not sender Is Nothing Then
            If sender.name = "cboTipoLib" Then
                ErrPrv.SetError(cboTipoLib, "")
            End If
        End If

        If Not chkSelPax.Checked Then Exit Sub

        If Not blnValidarPaxLiberados() Then Exit Sub


        Dim dblCostoReal As Double = -1
        Dim dblMargenAplicado As Double = -1
        Dim dblSSCR As Double = -1
        Dim dblSTCR As Double = -1
        Dim dblCostoRealTxt As Double = If(txtCostoReal.Text = "", "0", txtCostoReal.Text)
        Dim dblMargenAplicadoTxt As Double = If(txtMargenAplicado.Text = "", "0", txtMargenAplicado.Text)
        Dim dblSSCRTxt As Double = If(txtSSCR2.Text = "", "0", txtSSCR2.Text)
        Dim dblSTCRTxt As Double = If(txtSTCR2.Text = "", "0", txtSTCR2.Text)

        'If dblCostoRealOrigFijo <> 0 Then
        '    If dblCostoRealTxt <> dblCostoRealOrigFijo Then
        '        dblCostoReal = dblCostoRealTxt
        '    End If
        'End If
        'If dblMargenAplicadoOrigFijo <> 0 Then
        '    If dblMargenAplicadoTxt <> dblMargenAplicadoOrigFijo Then
        '        dblMargenAplicado = dblMargenAplicadoTxt
        '    End If
        'End If

        If blnCostoRealEditado Then
            dblCostoReal = dblCostoRealTxt
        End If
        If blnMargenAplicadoEditado Then
            dblMargenAplicado = dblMargenAplicadoTxt
        End If
        If blnSSCREditado Then
            dblSSCR = dblSSCRTxt
        End If
        If blnSTCREditado Then
            dblSTCR = dblSTCRTxt
        End If
        Try
            CalcularMontos(dblCostoReal, dblMargenAplicado, _
                   dblCostoReal, dblMargenAplicado, strTipoPaxNacionalExtranjero, _
                   dblSSCR, dblSTCR)

            'If Not chkEspecial.Checked Then
            '    dblCostoRealOrigFijo = If(txtCostoReal.Text = "", "0", txtCostoReal.Text)
            '    dblMargenAplicadoOrigFijo = If(txtMargenAplicado.Text = "", "0", txtMargenAplicado.Text)
            'End If
        Catch ex As Exception
            MessageBox.Show(ex.Message, ex.Source, MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Function blnValidarPaxLiberados() As Boolean
        If blnServicioTC Then Return True

        If txtNroPax.Text = "" Then
            ErrPrv.SetError(txtNroPax, "Debe ingresar Nro. Pax")
            txtNroPax.Focus()
            Return False
        End If

        If Convert.ToInt16(txtNroPax.Text) = 0 Then
            ErrPrv.SetError(txtNroPax, "Debe ingresar Nro. Pax mayor que cero")
            Return False
        End If

        If Convert.ToInt16(txtNroPax.Text) > intPax Then
            ErrPrv.SetError(txtNroPax, "Debe ingresar un Nro. Pax que no exceda de " & intPax.ToString)
            txtNroPax.Focus()
            Return False
        End If

        Dim intNroLiberados As Int16 = If(txtNroLiberados.Text = "", "0", txtNroLiberados.Text)
        If intNroLiberados <> 0 Then
            If intNroLiberados > Convert.ToInt16(txtNroPax.Text) Then
                ErrPrv.SetError(txtNroLiberados, "Debe ingresar un Nro. Liberados que no exceda Nro. Pax")
                txtNroLiberados.Focus()
                Return False
            End If
            Dim strTipoLib As String = If(cboTipoLib.Text = "", "", cboTipoLib.SelectedValue.ToString)
            If strTipoLib = "" Then
                ErrPrv.SetError(cboTipoLib, "Debe seleccionar un Tipo de Liberado")
                cboTipoLib.Focus()
                Return False
            End If
        ElseIf intNroLiberados = 0 Then
            Dim strTipoLib As String = If(cboTipoLib.Text = "", "", cboTipoLib.SelectedValue.ToString)
            If strTipoLib <> "" Then
                ErrPrv.SetError(cboTipoLib, "Debe seleccionar Tipo de Liberado NINGUNO")
                cboTipoLib.Focus()
                Return False
            End If
        End If

        Return True
    End Function

    Private Sub txtNroPax_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtNroPax.TextChanged

    End Sub

    Private Sub lvwPax_ItemCheck(ByVal sender As Object, ByVal e As System.Windows.Forms.ItemCheckEventArgs) Handles lvwPax.ItemCheck
        'MessageBox.Show(e.CurrentValue)
        If lvwPax.Items(e.Index).ForeColor = Color.Gray Then
            e.NewValue = CheckState.Unchecked
        End If
    End Sub

    Private Sub lvwPax_ItemChecked(ByVal sender As Object, ByVal e As System.Windows.Forms.ItemCheckedEventArgs) Handles lvwPax.ItemChecked
        Try
            If strModoEdicion = "E" And blnActPax And chkIncGuia.Checked = False Then
                'txtNroPax.Text = lvwPax.CheckedItems.Count
                Dim intNroPaxTemp As Int16 = lvwPax.CheckedItems.Count - Convert.ToInt16(If(txtNroLiberados.Text = "", "0", txtNroLiberados.Text))
                If intNroPaxTemp < 0 Then intNroPaxTemp = 0
                txtNroPax.Text = intNroPaxTemp
                If intNroPaxTemp = 0 And chkSelTodosPax.Checked = False Then
                    txtNroLiberados.Text = 0
                    cboTipoLib.SelectedValue = ""
                End If

                Dim dblCostoReal As Double = -1
                Dim dblMargenAplicado As Double = -1
                Dim dblSSCR As Double = -1
                Dim dblSTCR As Double = -1
                Dim dblCostoRealTxt As Double = If(txtCostoReal.Text = "", "0", txtCostoReal.Text)
                Dim dblMargenAplicadoTxt As Double = If(txtMargenAplicado.Text = "", "0", txtMargenAplicado.Text)
                Dim dblSSCRTxt As Double = If(txtSSCR2.Text = "", "0", txtSSCR2.Text)
                Dim dblSTCRTxt As Double = If(txtSTCR2.Text = "", "0", txtSTCR2.Text)
                If blnCostoRealEditado Then
                    dblCostoReal = dblCostoRealTxt
                End If
                If blnMargenAplicadoEditado Then
                    dblMargenAplicado = dblMargenAplicadoTxt
                End If
                If blnSSCREditado Then
                    dblSSCR = dblSSCRTxt
                End If
                If blnSTCREditado Then
                    dblSTCR = dblSTCRTxt
                End If
                CalcularMontos(dblCostoReal, dblMargenAplicado, _
                   dblCostoReal, dblMargenAplicado, strTipoPaxNacionalExtranjero, _
                   dblSSCR, dblSTCR)
            End If
            MarcarchkSelTodosPax()
        Catch ex As Exception
            MessageBox.Show(ex.Message, ex.Source, MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub


    Private Sub CopiarDetPaxaHijos()
        If frmRef Is Nothing Then Exit Sub
        If frmRef.dgvDet.CurrentRow Is Nothing Then Exit Sub
        If lvwPax.Items.Count = 0 Then Exit Sub
        Try
            Dim bytIndex As Byte = frmRef.dgvDet.CurrentRow.Index
            Dim strIDDetRelTmp As String = frmRef.dgvDet.Item("IDDetRel", bytIndex).Value.ToString
            Dim StrIDDetTmp As String = frmRef.dgvDet.Item("IDDet", bytIndex).Value.ToString
            Dim blnExistenHijosData As Boolean = False
            Dim blnExistenHijosDinam As Boolean = False
            Dim bytNroPax As Byte = 0


            For i As Byte = 0 To lvwPax.Items.Count - 1
                If lvwPax.Items(i).Checked = True Then
                    bytNroPax += 1
                End If
            Next

            If strIDDetRelTmp = "0" Then
                For Each Item As DataGridViewRow In frmRef.dgvDet.Rows
                    If Item.Index <> bytIndex Then
                        If Item.Cells("IDDetRel").Value = StrIDDetTmp Then
                            blnExistenHijosData = True
                            Exit For
                        End If
                    End If
                Next
            ElseIf InStr(strIDDetRelTmp, "P") > 0 Then

                For Each Item As DataGridViewRow In frmRef.dgvDet.Rows
                    If Item.Index <> bytIndex Then
                        StrIDDetTmp = Item.Cells("IDDetRel").Value
                        If StrIDDetTmp.Replace("H", "P") = strIDDetRelTmp Then '.Substring(0, 1) = "H" Then
                            blnExistenHijosDinam = True
                            Exit For
                        End If
                    End If
                Next
            End If

            If blnExistenHijosData Then
                For Each Item As DataGridViewRow In frmRef.dgvDet.Rows
                    If Item.Index <> bytIndex Then
                        If Item.Cells("IDDetRel").Value = StrIDDetTmp Then
                            If bytNroPax <> 0 Then
                                Dim intNroLiberados As Int16 = If(IsDBNull(Item.Cells("NroLiberados").Value), 0, Item.Cells("NroLiberados").Value)
                                Item.Cells("NroPax").Value = bytNroPax - intNroLiberados
                            Else
                                Item.Cells("NroPax").Value = If(bytNroPax = 0, intPax, bytNroPax)
                            End If
                            frmRef.dgvDet.CommitEdit(DataGridViewDataErrorContexts.LeaveControl)
                            frmRef.AgregarEditarValorStructDetCoti(Item.Index, "")
                            For i As Byte = 0 To lvwPax.Items.Count - 1
                                AgregarEditarValorStrucDetCotiPax(i, Item.Cells("IDDet").Value.ToString)
                            Next
                        End If
                    End If
                Next
            End If

            If blnExistenHijosDinam Then
                For Each Item As DataGridViewRow In frmRef.dgvDet.Rows
                    If Item.Index <> bytIndex Then
                        If Item.Cells("IDDetRel").Value = StrIDDetTmp Then
                            If bytNroPax <> 0 Then
                                Dim intNroLiberados As Int16 = If(IsDBNull(Item.Cells("NroLiberados").Value), 0, Item.Cells("NroLiberados").Value)
                                Item.Cells("NroPax").Value = bytNroPax - intNroLiberados
                            Else
                                Item.Cells("NroPax").Value = If(bytNroPax = 0, intPax, bytNroPax)
                            End If
                            frmRef.dgvDet.CommitEdit(DataGridViewDataErrorContexts.LeaveControl)
                            frmRef.AgregarEditarValorStructDetCoti(Item.Index, "")
                            For i As Byte = 0 To lvwPax.Items.Count - 1
                                AgregarEditarValorStrucDetCotiPax(i, Item.Cells("IDDet").Value.ToString)
                            Next
                        End If
                    End If
                Next
            End If

        Catch ex As Exception
            Throw
        End Try
    End Sub

    Private Function strTipoPaxNacionalExtranjero() As String
        Dim strTipoPaxLoc As String = ""
        Dim blnPeruano As Boolean = False
        For i As Int16 = 0 To lvwPax.Items.Count - 1
            If lvwPax.Items(i).SubItems.Count >= 3 Then
                'If lvwPax.Items(i).SubItems(4).Text = gstrPeru Then
                If Convert.ToBoolean(lvwPax.Items(i).SubItems(5).Text) = True Then
                    blnPeruano = True
                    Exit For
                End If
            End If

        Next
        If blnPeruano Then
            strTipoPaxLoc = "NCR"
        End If

        Return strTipoPaxLoc

    End Function

    Private Sub AgregarEditarValorStrucTextoIdioma(ByVal vbytNroFila As Byte, _
                                        ByVal vbytNroCol As Byte, _
                     ByVal vstrAccion As String, Optional ByVal ListIdiomasNuevos As List(Of String) = Nothing)


        Dim strAccion As String = "" 'N,B,M
        'Dim bytIndex As Byte = frmRef.dgvDet.CurrentRow.Index
        Dim strIDIdioma As String = dgvIdiom.Columns(vbytNroCol).Name
        'Dim strIDDet As String = frmRef.dgvDet.Item("IDDet", bytIndex).Value.ToString

        Dim RegValor As stCotizTextoIdioma
        Try

            Dim intInd As Int16 = 0
            For Each ST As stCotizTextoIdioma In objLstCotizTextoIdioma

                If ST.IDIdioma.ToUpper = strIDIdioma.ToUpper And ST.IDDet = strIDDet And ST.IDCab = intIDCab Then
                    If ST.strAccion <> "N" Then
                        strAccion = "M"
                    Else
                        strAccion = "N"
                    End If
                    objLstCotizTextoIdioma.RemoveAt(intInd)
                    Exit For
                End If

                intInd += 1
            Next
            If vstrAccion <> "B" Then 'si no es borrado de fila
            Else
                strAccion = vstrAccion
            End If

            RegValor = New stCotizTextoIdioma
            With RegValor
                .IDDet = strIDDet 'frmRef.dgvDet.Item("Iddet", frmRef.dgvDet.CurrentRow.Index).Value
                .IDIdioma = strIDIdioma
                .IDCab = intIDCab


                'If Not dgvIdiom.Item(vbytNroCol, vbytNroFila).Value Is Nothing Then
                '    .DescLarga = dgvIdiom.Item(vbytNroCol, vbytNroFila).Value.ToString
                'Else
                '    .DescLarga = ""
                'End If

                .DescLarga = ""
                .NoHotel = ""
                .TxWebHotel = ""
                .TxDireccHotel = ""
                .TxTelfHotel1 = ""
                .TxTelfHotel2 = ""
                .FeHoraChkInHotel = "01/01/1900"
                .FeHoraChkOutHotel = "01/01/1900"
                .CoTipoDesaHotel = ""
                .CoCiudadHotel = ""
                If vbytNroCol = bytColHotel Or vbytNroCol = bytColWeb Or vbytNroCol = bytColTxDireccHotel _
                     Or vbytNroCol = bytColTxTelfHotel1 Or vbytNroCol = bytColTxTelfHotel2 Or vbytNroCol = bytColFeHoraChkInHotel _
                      Or vbytNroCol = bytColFeHoraChkOutHotel Or vbytNroCol = bytColCoTipoDesaHotel Or vbytNroCol = bytColCoCiudadHotel Then


                    'If vbytNroCol = bytColHotel Then
                    '    If Not dgvIdiom.Item(vbytNroCol, vbytNroFila).Value Is Nothing Then
                    '        .NoHotel = dgvIdiom.Item(vbytNroCol, vbytNroFila).Value.ToString
                    '    End If
                    '    If Not dgvIdiom.Item(vbytNroCol + 1, vbytNroFila).Value Is Nothing Then
                    '        .TxWebHotel = dgvIdiom.Item(vbytNroCol + 1, vbytNroFila).Value.ToString
                    '    End If
                    'Else
                    '    If Not dgvIdiom.Item(vbytNroCol - 1, vbytNroFila).Value Is Nothing Then
                    '        .NoHotel = dgvIdiom.Item(vbytNroCol - 1, vbytNroFila).Value.ToString
                    '    End If
                    '    If Not dgvIdiom.Item(vbytNroCol, vbytNroFila).Value Is Nothing Then
                    '        .TxWebHotel = dgvIdiom.Item(vbytNroCol, vbytNroFila).Value.ToString
                    '    End If
                    'End If
                    For bytIndCol As Byte = bytColHotel To bytColCoCiudadHotel
                        If bytIndCol <= dgvIdiom.Columns.Count - 1 Then
                            Dim newValueProp As PropertyInfo = RegValor.GetType.GetProperty(dgvIdiom.Columns(bytIndCol).Name)
                            If Not dgvIdiom.Item(bytIndCol, vbytNroFila).Value Is Nothing And Not newValueProp Is Nothing Then
                                If dgvIdiom.Columns(bytIndCol).Name.ToString.Substring(0, 2) = "Fe" Then
                                    If IsDate(dgvIdiom.Item(bytIndCol, vbytNroFila).Value) Then
                                        newValueProp.SetValue(RegValor, Convert.ToDateTime(dgvIdiom.Item(bytIndCol, vbytNroFila).Value), Nothing)
                                    End If

                                Else
                                    newValueProp.SetValue(RegValor, dgvIdiom.Item(bytIndCol, vbytNroFila).Value.ToString, Nothing)
                                End If

                            End If
                        End If
                    Next
                Else
                    If Not dgvIdiom.Item(vbytNroCol, vbytNroFila).Value Is Nothing Then
                        .DescLarga = dgvIdiom.Item(vbytNroCol, vbytNroFila).Value.ToString
                    Else
                        .DescLarga = ""
                    End If

                End If




                If strAccion = "" Then 'si no existe en struct
                    If Not blnCargaIniStruct Then
                        .strAccion = "N"
                    Else
                        .strAccion = strAccion
                    End If

                Else
                    .strAccion = strAccion
                End If

                If Not ListIdiomasNuevos Is Nothing Then
                    If ListIdiomasNuevos.Contains(.IDIdioma.ToUpper) Then
                        .strAccion = "N"
                    End If
                End If

                If dgvIdiom.Columns(vbytNroCol).DefaultCellStyle.ForeColor = Color.Gray Then
                    .strAccion = "N"
                End If

                If dgvIdiom.Columns(vbytNroCol).Visible = False Then
                    .strAccion = "B"
                End If


                If Not blnCargaIniStruct Then
                    objLstCotizTextoIdioma.Add(RegValor)
                Else
                    If .strAccion = "B" Then
                        objLstCotizTextoIdioma.Add(RegValor)
                    Else
                        If .DescLarga.Trim = "" Then ' And .DescCorta.Trim = "" Then
                        Else
                            objLstCotizTextoIdioma.Add(RegValor)
                        End If
                    End If
                End If

            End With

        Catch ex As Exception
            Throw

        End Try
    End Sub

    Private Sub CargarDgvTextosIdioma2()
        Try
            Dim blnNoEncontro As Boolean = True
            If IsNumeric(strIDDet) Then
                CargarDgvTextosIdioma()
            Else
                'dgvIdiom.Rows.Add()
                For Each ST As stCotizTextoIdioma In objLstCotizTextoIdioma
                    If ST.IDDet = strIDDet And ST.IDCab = intIDCab Then
                        For Each DgvCol As DataGridViewColumn In dgvIdiom.Columns
                            'If DgvCol.Index = bytColHotel Then
                            '    dgvIdiom.Item(DgvCol.Index, 0).Value = ST.NoHotel
                            '    dgvIdiom.Item(DgvCol.Index + 1, 0).Value = ST.TxWebHotel
                            '    dgvIdiom.Item(DgvCol.Index, 0).Style.ForeColor = Color.Gray
                            '    dgvIdiom.Item(DgvCol.Index + 1, 0).Style.ForeColor = Color.Gray
                            '    blnNoEncontro = False
                            'ElseIf DgvCol.Index = bytColWeb Then
                            '    dgvIdiom.Item(DgvCol.Index - 1, 0).Value = ST.NoHotel
                            '    dgvIdiom.Item(DgvCol.Index, 0).Value = ST.TxWebHotel
                            '    dgvIdiom.Item(DgvCol.Index - 1, 0).Style.ForeColor = Color.Gray
                            '    dgvIdiom.Item(DgvCol.Index, 0).Style.ForeColor = Color.Gray
                            '    blnNoEncontro = False
                            'Else
                            '    If DgvCol.Name.ToString.ToUpper = ST.IDIdioma Then
                            '        dgvIdiom.Item(DgvCol.Index, 0).Value = ST.DescLarga
                            '        dgvIdiom.Item(DgvCol.Index, 0).Style.ForeColor = Color.Gray
                            '        blnNoEncontro = False
                            '        Exit For
                            '    End If
                            'End If
                            If DgvCol.Index = bytColHotel Or DgvCol.Index = bytColWeb Or DgvCol.Index = bytColTxDireccHotel _
                                Or DgvCol.Index = bytColTxTelfHotel1 Or DgvCol.Index = bytColTxTelfHotel2 Or DgvCol.Index = bytColFeHoraChkInHotel _
                                 Or DgvCol.Index = bytColFeHoraChkOutHotel Or DgvCol.Index = bytColCoTipoDesaHotel Or DgvCol.Index = bytColCoCiudadHotel Then
                                For bytIndCol As Byte = bytColHotel To bytColCoCiudadHotel
                                    If DgvCol.Index = bytIndCol Then
                                        Dim newValueProp As PropertyInfo = ST.GetType.GetProperty(dgvIdiom.Columns(bytIndCol).Name)
                                        dgvIdiom.Item(DgvCol.Index, 0).Value = newValueProp.GetValue(ST)
                                        Exit For
                                    End If
                                Next
                            Else
                                If DgvCol.Name.ToString.ToUpper = ST.IDIdioma Then
                                    dgvIdiom.Item(DgvCol.Index, 0).Value = ST.DescLarga
                                    dgvIdiom.Item(DgvCol.Index, 0).Style.ForeColor = Color.Gray
                                    blnNoEncontro = False
                                    Exit For
                                End If
                            End If
                        Next
                    End If
                Next
            End If

            For Each DgvCol As DataGridViewColumn In dgvIdiom.Columns
                If DgvCol.Index <> bytColHotel And DgvCol.Index <> bytColWeb And _
     DgvCol.Index <> bytColTxDireccHotel And DgvCol.Index <> bytColTxTelfHotel1 And _
     DgvCol.Index <> bytColTxTelfHotel2 And DgvCol.Index <> bytColFeHoraChkInHotel And _
     DgvCol.Index <> bytColFeHoraChkOutHotel And DgvCol.Index <> bytColCoTipoDesaHotel And _
     DgvCol.Index <> bytColCoCiudadHotel Then

                    If dgvIdiom.Item(DgvCol.Index, 0).Value Is Nothing Then
                        If bytCountSTIdioma(strIDDet) > 1 Then
                            DgvCol.Visible = False
                        End If
                    Else
                        DgvCol.Width = 100
                    End If
                End If
            Next

        Catch ex As Exception
            Throw
        End Try
    End Sub

    Private Function bytCountSTIdioma(ByVal vstrIDDet As String) As Byte
        If objLstCotizTextoIdioma Is Nothing Then Exit Function
        Dim bytCont As Byte = 0
        Try
            For Each ST As stCotizTextoIdioma In objLstCotizTextoIdioma
                If ST.IDCab = intIDCab And ST.IDDet = vstrIDDet Then
                    bytCont += 1
                End If
            Next
            Return bytCont
        Catch ex As Exception
            Throw
        End Try
    End Function

    Private Sub CargarDgvTextosIdioma()
        Try
            Dim objBNDet As New clsCotizacionBN.clsDetalleCotizacionBN.clsDetalleCotizacionDescripServicioBN
            Dim dtDet As New DataTable

            Dim strIDDetRelLoc As String
            If strIDDetRel = "" Then
                strIDDetRelLoc = strIDDetRel
            Else
                If IsNumeric(strIDDetRel) Then
                    strIDDetRelLoc = strIDDetRel
                Else
                    strIDDetRelLoc = Mid(strIDDetRel, 2)
                End If

            End If

            If strIDDetRelLoc = "" Then
                dtDet = objBNDet.ConsultarListxIDDet(intIDDet, strIDServicio, 1)
            Else
                Dim bytDia As Byte = 1
                Dim bytFila As Byte
                Dim strIDDetLoc As String

                For Each DgvFil As DataGridViewRow In frmRef.dgvDet.Rows
                    If DgvFil.Cells("IDDet").Value Is Nothing Then
                        strIDDetLoc = "0"
                    Else
                        strIDDetLoc = DgvFil.Cells("IDDet").Value.ToString
                    End If

                    If strIDDetRelLoc = strIDDetLoc Then
                        bytFila = DgvFil.Index
                        While bytFila < frmRef.dgvDet.CurrentRow.Index
                            bytFila += 1
                            bytDia += 1
                        End While

                        Exit For
                    End If
                Next

                dtDet = objBNDet.ConsultarListxIDDet(intIDDet, strIDServicio, bytDia)

            End If
            'Dim dtt As New DataTable("Textos")
            If dtDet.Rows.Count > 0 Then
                ' With dtt.Columns
                '.Add("ENGLISH")
                '.Add("FRENCH")
                '.Add("GERMAN")
                '.Add("SPANISH")

                'dgvIdiom.Columns.Clear()
                dgvIdiom.ColumnCount = 0

                For Each drDet As DataRow In dtDet.Rows
                    '.Add(drDet("IDIdioma").ToString.ToUpper)
                    If drDet("IDIdioma").ToString <> "HOTEL/WEB" Then
                        dgvIdiom.Columns.Add(drDet("IDIdioma").ToString, drDet("IDIdioma").ToString)
                    End If
                Next
                ''PPMG-20151203
                If strModulo <> "C" Then
                    dgvIdiom.Columns.Add("NoHotel", "HOTEL")
                    bytColHotel = dgvIdiom.ColumnCount - 1
                    dgvIdiom.Columns.Add("TxWebHotel", "WEB")
                    bytColWeb = dgvIdiom.ColumnCount - 1
                Else
                    bytColHotel = dgvIdiom.ColumnCount
                    bytColWeb = dgvIdiom.ColumnCount + 1
                End If
                FormatoGridIdiomas()
                'End With
            Else
                'With dtt.Columns
                '    .Add("ENGLISH")
                '    .Add("FRENCH")
                '    .Add("GERMAN")
                '    .Add("SPANISH")

                'End With

            End If

            'dtt.Rows.Add("") ', "", "", "")

            If dtDet.Rows.Count > 0 Then
                Dim strIDDet As String = ""
                If Not frmRef Is Nothing Then
                    strIDDet = frmRef.dgvDet.Item("IDDet", frmRef.dgvDet.CurrentRow.Index).Value
                End If


                Dim dttSel As DataTable

                For Each DgvCol As DataGridViewColumn In dgvIdiom.Columns
                    Dim strCol As String = DgvCol.Name.ToString.ToUpper.Trim
                    If strCol = "NOHOTEL" Or strCol = "TXWEBHOTEL" Or strCol = "TxDireccHotel".ToUpper Or strCol = "TxTelfHotel1".ToUpper _
                        Or strCol = "TxTelfHotel2".ToUpper Or strCol = "FeHoraChkInHotel".ToUpper Or strCol = "FeHoraChkOutHotel".ToUpper _
                        Or strCol = "CoTipoDesaHotel".ToUpper Or strCol = "CoCiudadHotel".ToUpper Then
                        strCol = "HOTEL/WEB"
                    End If
                    dttSel = dtFiltrarDataTable(dtDet, "IDIdioma='" & strCol & "'")
                    If dttSel.Rows.Count > 0 Then

                        'dgvIdiom.Item(DgvCol.Index, 0).Value = dttSel(0)("DescCorta")
                        'dgvIdiom.Item(DgvCol.Index, 0).Value = strBuscarenTextoIdioma(intIDDet, DgvCol.Name.ToString.ToUpper.Trim, False, If(IsDBNull(dttSel(0)("DescCorta")), "", dttSel(0)("DescCorta")))
                        dgvIdiom.Item(DgvCol.Index, 0).Value = ""
                        'dgvIdiom.Item(DgvCol.Index, 1).Value = dttSel(0)("DescLarga")

                        'dgvIdiom.Item(DgvCol.Index, 0).Value = strBuscarenTextoIdioma(strIDDet, DgvCol.Name.ToString.ToUpper.Trim, True, dttSel(0)("DescLarga"))

                        'dgvIdiom.Item(DgvCol.Index, 0).Value = dttSel(0)("DescLarga")
                        If strCol = "HOTEL/WEB" Then
                            'If DgvCol.Index = bytColHotel Then
                            '    dgvIdiom.Item(DgvCol.Index, 0).Value = dttSel(0)("NoHotel")
                            '    dgvIdiom.Item(DgvCol.Index + 1, 0).Value = dttSel(0)("TxWebHotel")
                            'Else
                            '    dgvIdiom.Item(DgvCol.Index - 1, 0).Value = dttSel(0)("NoHotel")
                            '    dgvIdiom.Item(DgvCol.Index, 0).Value = dttSel(0)("TxWebHotel")
                            'End If
                            For bytIndCol As Byte = bytColHotel To bytColCoCiudadHotel
                                If DgvCol.Index = bytIndCol Then
                                    If bytIndCol - 5 > 0 Then
                                        dgvIdiom.Item(bytIndCol, 0).Value = dttSel(0)(bytIndCol - 5)
                                    End If
                                    Exit For
                                End If
                            Next
                        Else
                            dgvIdiom.Item(DgvCol.Index, 0).Value = dttSel(0)("DescLarga")
                        End If


                        If DgvCol.Index <> bytColHotel And DgvCol.Index <> bytColWeb And _
                            DgvCol.Index <> bytColTxDireccHotel And DgvCol.Index <> bytColTxTelfHotel1 And _
                            DgvCol.Index <> bytColTxTelfHotel2 And DgvCol.Index <> bytColFeHoraChkInHotel And _
                            DgvCol.Index <> bytColFeHoraChkOutHotel And DgvCol.Index <> bytColCoTipoDesaHotel And _
                            DgvCol.Index <> bytColCoCiudadHotel Then

                            If dgvIdiom.Item(DgvCol.Index, 0).Value.ToString = "" Then
                                DgvCol.Visible = False
                            End If
                        End If

                        If dttSel(0)("Nuevo") = 1 Then
                            If strBuscarenTextoIdioma(intIDDet, DgvCol.Name.ToString.ToUpper.Trim, False, "") = "" Then
                                DgvCol.DefaultCellStyle.ForeColor = Color.Gray
                            End If
                        End If

                    End If
                Next


                ErrPrv.SetError(dgvIdiom, "")
                dgvIdiom.ReadOnly = False
            Else
                ErrPrv.SetError(dgvIdiom, "No existen textos registrados para este servicio")
                dgvIdiom.ReadOnly = True
            End If

            ''Idiomas que no sean los 4 obligatorios
            Dim ListIdiomasNuevos As New List(Of String)
            'Dim blnUbicado As Boolean
            'For Each drDet As DataRow In dtDet.Rows
            '    blnUbicado = False
            '    For Each DgvCol As DataGridViewColumn In dgvIdiom.Columns
            '        If drDet("IDIdioma") = DgvCol.Name.ToString.ToUpper.Trim Then
            '            blnUbicado = True
            '            Exit For
            '        End If
            '    Next
            '    If Not blnUbicado Then
            '        dgvIdiom.Columns.Add(drDet("IDIdioma").ToString, drDet("IDIdioma").ToString)

            '        dgvIdiom.Item(dgvIdiom.Columns.Count - 1, 0).Value = strBuscarenTextoIdioma(intIDDet, drDet("IDIdioma").ToString, False, drDet("DescCorta"))
            '        dgvIdiom.Item(dgvIdiom.Columns.Count - 1, 1).Value = strBuscarenTextoIdioma(intIDDet, drDet("IDIdioma").ToString, True, drDet("DescLarga"))

            '        If dgvIdiom.Item(dgvIdiom.Columns.Count - 1, 0).Value.ToString = "" Then
            '            dgvIdiom.Columns(dgvIdiom.Columns.Count - 1).Visible = False
            '        End If

            '        ListIdiomasNuevos.Add(drDet("IDIdioma"))
            '    End If
            'Next

            For Each DgvCol As DataGridViewColumn In dgvIdiom.Columns
                DgvCol.SortMode = DataGridViewColumnSortMode.NotSortable
            Next

            'BorrarInvisibles:
            '            For Each DgvCol As DataGridViewColumn In dgvIdiom.Columns
            '                If DgvCol.Visible = False Then
            '                    dgvIdiom.Columns.Remove(DgvCol)
            '                    GoTo BorrarInvisibles
            '                End If
            '            Next


            blnCargaIniStruct = True

            For Each FilaDgv As DataGridViewRow In dgvIdiom.Rows
                For bytCol As Byte = 0 To dgvIdiom.Columns.Count - 1
                    AgregarEditarValorStrucTextoIdioma(FilaDgv.Index, bytCol, "", ListIdiomasNuevos)
                Next
            Next

            blnCargaIniStruct = False

            'pDgvAnchoColumnas(dgvIdiom)
        Catch ex As Exception
            Throw
        End Try

    End Sub

    Function dtFiltrarDataTable(ByVal vDataTable As DataTable, ByVal psFiltro As String, Optional ByVal psOrder As String = "") As DataTable
        Dim loRows As DataRow()
        Dim dtNuevoDataTable As DataTable        ' Copio la estructura del DataTable original        
        dtNuevoDataTable = vDataTable.Clone()        ' Establezco el filtro y el orden        
        If psOrder = "" Then
            loRows = vDataTable.Select(psFiltro)
        Else
            loRows = vDataTable.Select(psFiltro, psOrder)
        End If        ' Cargo el nuevo DataTable con los datos filtrados        
        For Each ldrRow As DataRow In loRows
            dtNuevoDataTable.ImportRow(ldrRow)
        Next
        ' Retorno el nuevo DataTable        
        Return dtNuevoDataTable
    End Function

    Private Sub FormatoGridIdiomas()
        With dgvIdiom
            'Const FilaTitulo = 0
            Const FilaTexto = 0
            '.DefaultCellStyle.WrapMode = DataGridViewTriState.True

            .ColumnHeadersDefaultCellStyle.Font = New Font(.Font, FontStyle.Bold)
            .DefaultCellStyle.Font = New Font(.Font, FontStyle.Regular)


            .RowCount = 1

            '.Rows(FilaTitulo).Height = 20


            .Rows(FilaTexto).Height = 200
            .Rows(FilaTexto).DefaultCellStyle.Alignment = DataGridViewContentAlignment.TopLeft
            '.Rows(FilaTexto).DefaultCellStyle.WrapMode = DataGridViewTriState.True

        End With
    End Sub
    Private Sub EliminarIdiomaTexto()
        Try
            With dgvIdiom
                If .CurrentCell Is Nothing Then Exit Sub

                Try
                    If MessageBox.Show("¿Está Ud. seguro de eliminar el Idioma " & .Columns(.CurrentCell.ColumnIndex).HeaderText & " de la lista?", gstrTituloMsg, MessageBoxButtons.YesNo, MessageBoxIcon.Question) = Windows.Forms.DialogResult.Yes Then
                        'For Each DgvFila As DataGridViewRow In .Rows
                        '    AgregarEditarValorStrucTextoIdioma(DgvFila.Index, .CurrentCell.ColumnIndex, "B")

                        'Next

                        '.Columns.Remove(.Columns(.CurrentCell.ColumnIndex))
                        .Columns(.CurrentCell.ColumnIndex).Visible = False

                        If blnLoad Then
                            blnCambios = True
                            btnAceptar.Enabled = True
                        End If

                    End If
                Catch ex As Exception
                    Throw
                End Try
            End With

        Catch ex As Exception
            Throw
        End Try
    End Sub
    Private Sub btnEliminarIdiomaTexto_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEliminarIdiomaTexto.Click
        Try
            EliminarIdiomaTexto()
        Catch ex As Exception
            MessageBox.Show(ex.Message, ex.Source, MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try

    End Sub

    Private Sub dgvIdiom_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvIdiom.CellContentClick

    End Sub

    Private Sub dgvIdiom_CellEnter(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvIdiom.CellEnter
        If dgvIdiom.Columns(e.ColumnIndex).DefaultCellStyle.ForeColor = Color.Gray Then
            btnEliminarIdiomaTexto.Enabled = False
        Else
            btnEliminarIdiomaTexto.Enabled = True
        End If
    End Sub

    Private Sub chkTransfer_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkTransfer.CheckedChanged
        If chkTransfer.Checked Then
            lblTipoTransporteEtiq.Visible = False
            cboTipoTransporte.Visible = False
            cboTipoTransporte.SelectedIndex = -1

            lblOrigenEtiq.Visible = True
            cboOrigen.Visible = True

            lblDestinoEtiq.Visible = True
            cboDestino.Visible = True
            'txtServicio.ReadOnly = True
        Else
            lblTipoTransporteEtiq.Visible = True
            cboTipoTransporte.Visible = True

            lblOrigenEtiq.Visible = False
            cboOrigen.Visible = False
            cboOrigen.SelectedIndex = -1
            lblDestinoEtiq.Visible = False
            cboDestino.Visible = False
            cboDestino.SelectedIndex = -1
            'txtServicio.ReadOnly = False
        End If
    End Sub

    Private Sub CargarCombosTransfer2LectResv()

        '        Try
        '            Dim dttOrigen As New DataTable("Transfer")
        '            With dttOrigen
        '                .Columns.Add("TextoDesc")
        '                .Columns.Add("TextoVer")
        '            End With

        '            Dim datFechaDiaCotiDet As Date = Now 'frmRefRes.dgvDet.CurrentRow.Cells("CDia").Value.ToString.Substring(0, 10)
        '            Dim datFechaDiaCotiDetBusqueda As Date = DateAdd(DateInterval.Day, -1, datFechaDiaCotiDet)

        '            Dim strTextoVer As String, strTextoDesc As String
        '            Dim strProveedor As String, strOrigen As String, strDestino As String
        '            Dim strVuelo As String, strHora As String
        '            Dim blnUbicado As Boolean
        '            Dim bytContGrid As Byte = 1
        '            'VUELOS
        'Vuelos:
        '            With frmRefRes.dgvVuelos
        '                For Each DgvFila As DataGridViewRow In .Rows
        '                    blnUbicado = False
        '                    If DgvFila.Cells("DiaV").Value Is Nothing Then
        '                        If Not DgvFila.Cells("FechaRetorno").Value Is Nothing Then
        '                            If Format(DgvFila.Cells("FechaRetorno").Value, "dd/MM/yyyy") = datFechaDiaCotiDetBusqueda Then
        '                                blnUbicado = True
        '                            End If
        '                        End If
        '                    Else
        '                        If Not DgvFila.Cells("DiaV").Value Is Nothing Then
        '                            If DgvFila.Cells("DiaV").Value.ToString.Length > 10 Then
        '                                If DgvFila.Cells("DiaV").Value.ToString.Substring(4, 10) = datFechaDiaCotiDetBusqueda Then
        '                                    blnUbicado = True
        '                                End If
        '                            Else
        '                                If DgvFila.Cells("DiaV").Value.ToString = datFechaDiaCotiDetBusqueda Then
        '                                    blnUbicado = True
        '                                End If
        '                            End If

        '                        End If
        '                    End If
        '                    If blnUbicado Then
        '                        If Not DgvFila.Cells("DescProveedorV").Value Is Nothing Then
        '                            strProveedor = DgvFila.Cells("DescProveedorV").Value.ToString & " ("
        '                        Else
        '                            strProveedor = ""
        '                        End If
        '                        If Not DgvFila.Cells("RutaOAbrV").Value Is Nothing Then
        '                            strOrigen = DgvFila.Cells("RutaOAbrV").Value.ToString & "/"
        '                            'strOrigen = Replace(strOrigen, "NO APLICA/", "")
        '                        Else
        '                            strOrigen = ""
        '                        End If
        '                        If Not DgvFila.Cells("RutaDAbrV").Value Is Nothing Then
        '                            strDestino = DgvFila.Cells("RutaDAbrV").Value.ToString & ")"
        '                            'strDestino = Replace(strDestino, "NO APLICA", "")
        '                        Else
        '                            strDestino = ""
        '                        End If
        '                        strTextoVer = strProveedor & strOrigen & strDestino
        '                        If Not DgvFila.Cells("Vuelo").Value Is Nothing Then
        '                            strVuelo = DgvFila.Cells("Vuelo").Value.ToString
        '                        Else
        '                            strVuelo = ""
        '                        End If
        '                        If Not DgvFila.Cells("HrRecojo").Value Is Nothing Then
        '                            strHora = DgvFila.Cells("HrRecojo").Value.ToString
        '                        Else
        '                            strHora = ""
        '                        End If
        '                        'strTextoDesc = "AEROPUERTO " & strProveedor & strVuelo & " " & strOrigen & strDestino & " " & strHora
        '                        'strTextoDesc = strProveedor & strVuelo & " " & strOrigen & strDestino & " " & strHora
        '                        strTextoDesc = strProveedor & strOrigen & strDestino '& " " & strHora

        '                        If strTextoDesc.Trim <> "" Then
        '                            If strTextoDesc.Trim.Substring(strTextoDesc.Trim.Length - 3, 3) = "(/)" Then
        '                                strTextoDesc = strTextoDesc.Substring(0, strTextoDesc.Length - 4)
        '                            End If

        '                            Dim dttTemp As DataTable = dtFiltrarDataTable(dttOrigen, "TextoVer='" & strTextoDesc & "'")
        '                            If dttTemp.Rows.Count = 0 Then
        '                                dttOrigen.Rows.Add(DgvFila.Cells("IDDetV").Value.ToString, strTextoDesc)
        '                            End If
        '                        End If
        '                    End If
        '                Next

        '            End With
        '            bytContGrid += 1
        '            If bytContGrid = 2 Then
        '                datFechaDiaCotiDetBusqueda = datFechaDiaCotiDet
        '                GoTo Vuelos
        '            ElseIf bytContGrid = 3 Then
        '                datFechaDiaCotiDetBusqueda = DateAdd(DateInterval.Day, 1, datFechaDiaCotiDet)
        '                GoTo Vuelos
        '            End If

        '            'BUS
        '            bytContGrid = 1
        '            datFechaDiaCotiDetBusqueda = DateAdd(DateInterval.Day, -1, datFechaDiaCotiDet)
        'Buses:
        '            With frmRefRes.dgvBuss
        '                For Each DgvFila As DataGridViewRow In .Rows
        '                    blnUbicado = False
        '                    If DgvFila.Cells("DiaB").Value Is Nothing Then
        '                        If Not DgvFila.Cells("Fec_RetornoB").Value Is Nothing Then
        '                            If Format(DgvFila.Cells("Fec_RetornoB").Value, "dd/MM/yyyy") = datFechaDiaCotiDetBusqueda Then
        '                                blnUbicado = True
        '                            End If
        '                        End If
        '                    Else
        '                        If Not DgvFila.Cells("DiaB").Value Is Nothing Then
        '                            If DgvFila.Cells("DiaB").Value.ToString.Length > 10 Then
        '                                If DgvFila.Cells("DiaB").Value.ToString.Substring(4, 10) = datFechaDiaCotiDetBusqueda Then
        '                                    blnUbicado = True
        '                                End If
        '                            Else
        '                                If DgvFila.Cells("DiaB").Value.ToString = datFechaDiaCotiDetBusqueda Then
        '                                    blnUbicado = True
        '                                End If
        '                            End If
        '                        End If
        '                    End If
        '                    If blnUbicado Then
        '                        If Not DgvFila.Cells("DescProveedorB").Value Is Nothing Then
        '                            strProveedor = DgvFila.Cells("DescProveedorB").Value.ToString & " ("
        '                        Else
        '                            strProveedor = ""
        '                        End If
        '                        If Not DgvFila.Cells("RutaOAbrB").Value Is Nothing Then
        '                            strOrigen = DgvFila.Cells("RutaOAbrB").Value.ToString & "/"
        '                            'strOrigen = Replace(strOrigen, "NO APLICA/", "")
        '                        Else
        '                            strOrigen = ""
        '                        End If
        '                        If Not DgvFila.Cells("RutaDAbrB").Value Is Nothing Then
        '                            strDestino = DgvFila.Cells("RutaDAbrB").Value.ToString & ")"
        '                            'strDestino = Replace(strDestino, "NO APLICA", "")
        '                        Else
        '                            strDestino = ""
        '                        End If

        '                        strTextoVer = strProveedor & strOrigen & strDestino

        '                        If Not DgvFila.Cells("HrRecojoB").Value Is Nothing Then
        '                            strHora = DgvFila.Cells("HrRecojoB").Value.ToString
        '                        Else
        '                            strHora = ""
        '                        End If
        '                        strTextoDesc = strProveedor & strOrigen & strDestino '& " " & strHora
        '                        If strTextoDesc.Trim <> "" Then
        '                            If strTextoDesc.Trim.Substring(strTextoDesc.Trim.Length - 3, 3) = "(/)" Then
        '                                strTextoDesc = strTextoDesc.Substring(0, strTextoDesc.Length - 4)
        '                            End If

        '                            'dttOrigen.Rows.Add(strTextoDesc, strTextoVer)
        '                            Dim dttTemp As DataTable = dtFiltrarDataTable(dttOrigen, "TextoVer='" & strTextoDesc & "'")
        '                            If dttTemp.Rows.Count = 0 Then
        '                                dttOrigen.Rows.Add(DgvFila.Cells("IDDetB").Value.ToString, strTextoDesc)
        '                            End If
        '                        End If
        '                    End If
        '                Next

        '            End With
        '            bytContGrid += 1
        '            If bytContGrid = 2 Then
        '                datFechaDiaCotiDetBusqueda = datFechaDiaCotiDet
        '                GoTo Buses
        '            ElseIf bytContGrid = 3 Then
        '                datFechaDiaCotiDetBusqueda = DateAdd(DateInterval.Day, 1, datFechaDiaCotiDet)
        '                GoTo Buses
        '            End If

        '            'TREN
        '            bytContGrid = 1
        '            datFechaDiaCotiDetBusqueda = DateAdd(DateInterval.Day, -1, datFechaDiaCotiDet)
        'Trenes:
        '            With frmRefRes.dgvTren
        '                For Each DgvFila As DataGridViewRow In .Rows
        '                    blnUbicado = False
        '                    If DgvFila.Cells("DiaT").Value Is Nothing Then
        '                        If Not DgvFila.Cells("Fec_RetornoT").Value Is Nothing Then
        '                            If Format(DgvFila.Cells("Fec_RetornoT").Value, "dd/MM/yyyy") = datFechaDiaCotiDetBusqueda Then
        '                                blnUbicado = True
        '                            End If
        '                        End If
        '                    Else
        '                        If Not DgvFila.Cells("DiaT").Value Is Nothing Then
        '                            If DgvFila.Cells("DiaT").Value.ToString.Length > 10 Then
        '                                If DgvFila.Cells("DiaT").Value.ToString.Substring(4, 10) = datFechaDiaCotiDetBusqueda Then
        '                                    blnUbicado = True
        '                                End If
        '                            Else
        '                                If DgvFila.Cells("DiaT").Value.ToString = datFechaDiaCotiDetBusqueda Then
        '                                    blnUbicado = True
        '                                End If
        '                            End If
        '                        End If
        '                    End If
        '                    If blnUbicado Then
        '                        If Not DgvFila.Cells("DescProveedorT").Value Is Nothing Then
        '                            strProveedor = DgvFila.Cells("DescProveedorT").Value.ToString & " ("
        '                        Else
        '                            strProveedor = ""
        '                        End If
        '                        If Not DgvFila.Cells("RutaOAbrT").Value Is Nothing Then
        '                            strOrigen = DgvFila.Cells("RutaOAbrT").Value.ToString & "/"
        '                            'strOrigen = Replace(strOrigen, "NO APLICA/", "")
        '                        Else
        '                            strOrigen = ""
        '                        End If
        '                        If Not DgvFila.Cells("RutaDAbrT").Value Is Nothing Then
        '                            strDestino = DgvFila.Cells("RutaDAbrT").Value.ToString & ")"
        '                            'strDestino = Replace(strDestino, "NO APLICA", "")
        '                        Else
        '                            strDestino = ""
        '                        End If
        '                        strTextoVer = strProveedor & strOrigen & strDestino

        '                        If Not DgvFila.Cells("HrRecojoT").Value Is Nothing Then
        '                            strHora = DgvFila.Cells("HrRecojoT").Value.ToString
        '                        Else
        '                            strHora = ""
        '                        End If
        '                        strTextoDesc = strProveedor & strOrigen & strDestino '& " " & strHora
        '                        If strTextoDesc.Trim <> "" Then
        '                            If strTextoDesc.Trim.Substring(strTextoDesc.Trim.Length - 3, 3) = "(/)" Then
        '                                strTextoDesc = strTextoDesc.Substring(0, strTextoDesc.Length - 4)
        '                            End If

        '                            'dttOrigen.Rows.Add(strTextoDesc, strTextoVer)
        '                            Dim dttTemp As DataTable = dtFiltrarDataTable(dttOrigen, "TextoVer='" & strTextoDesc & "'")
        '                            If dttTemp.Rows.Count = 0 Then
        '                                dttOrigen.Rows.Add(DgvFila.Cells("IDDetT").Value.ToString, strTextoDesc)
        '                            End If
        '                        End If
        '                    End If
        '                Next

        '            End With
        '            bytContGrid += 1
        '            If bytContGrid = 2 Then
        '                datFechaDiaCotiDetBusqueda = datFechaDiaCotiDet
        '                GoTo Trenes
        '            ElseIf bytContGrid = 3 Then
        '                datFechaDiaCotiDetBusqueda = DateAdd(DateInterval.Day, 1, datFechaDiaCotiDet)
        '                GoTo Trenes
        '            End If

        '            'HOTELES

        '            bytContGrid = 1
        '            datFechaDiaCotiDetBusqueda = DateAdd(DateInterval.Day, -1, datFechaDiaCotiDet)
        'Hoteles:
        '            With frmRefRes.dgvDet
        '                For Each DgvFila As DataGridViewRow In .Rows
        '                    blnUbicado = False
        '                    If DgvFila.Cells("CIDTipoProv").Value.ToString = gstrTipoProveeHoteles Then
        '                        If DgvFila.Cells("CFecha").Value.ToString.Substring(4, 10) = datFechaDiaCotiDetBusqueda Then
        '                            blnUbicado = True
        '                        End If
        '                    End If
        '                    If blnUbicado Then
        '                        If Not DgvFila.Cells("CDescProveedor").Value Is Nothing Then
        '                            strProveedor = DgvFila.Cells("CDescProveedor").Value.ToString
        '                        Else
        '                            strProveedor = ""
        '                        End If

        '                        If strProveedor <> "" Then
        '                            If strProveedor.Length >= 5 Then
        '                                If strProveedor.Substring(0, 5) = "HOTEL" Then
        '                                    strTextoVer = Mid(strProveedor, 6)
        '                                    strTextoVer = "HTL " & strTextoVer.Trim
        '                                ElseIf strProveedor.Substring(0, 3) = "HTL" Then
        '                                    strTextoVer = Mid(strProveedor, 4)
        '                                    strTextoVer = "HTL " & strTextoVer.Trim

        '                                Else
        '                                    strTextoVer = "HTL " & strProveedor
        '                                End If


        '                            ElseIf strProveedor.Length >= 3 Then

        '                                If strProveedor.Substring(0, 3) = "HTL" Then
        '                                    strTextoVer = Mid(strProveedor, 4)
        '                                    strTextoVer = "HTL " & strTextoVer.Trim
        '                                ElseIf strProveedor.Substring(0, 5) = "HOTEL" Then
        '                                    strTextoVer = Mid(strProveedor, 6)
        '                                    strTextoVer = "HTL " & strTextoVer.Trim
        '                                Else
        '                                    strTextoVer = "HTL " & strProveedor
        '                                End If


        '                            Else
        '                                strTextoVer = strProveedor
        '                            End If


        '                        Else
        '                            strTextoVer = ""
        '                        End If

        '                        strTextoDesc = strTextoVer
        '                        If strTextoDesc.Trim <> "" Then
        '                            'dttOrigen.Rows.Add(strTextoDesc, strTextoVer)
        '                            If strTextoDesc.Trim.Substring(strTextoDesc.Trim.Length - 3, 3) = "- /" Then
        '                                strTextoDesc = strTextoDesc.Substring(0, strTextoDesc.Length - 4)
        '                            End If

        '                            Dim dttTemp As DataTable = dtFiltrarDataTable(dttOrigen, "TextoVer='" & strTextoDesc & "'")
        '                            If dttTemp.Rows.Count = 0 Then
        '                                dttOrigen.Rows.Add(If(DgvFila.Cells("CIDDet").Value Is Nothing, "X" & (DgvFila.Index + 1).ToString, DgvFila.Cells("CIDDet").Value.ToString), strTextoDesc)
        '                            End If
        '                        End If
        '                    End If
        '                Next

        '            End With
        '            bytContGrid += 1
        '            If bytContGrid = 2 Then
        '                datFechaDiaCotiDetBusqueda = datFechaDiaCotiDet
        '                GoTo Hoteles
        '            ElseIf bytContGrid = 3 Then
        '                datFechaDiaCotiDetBusqueda = DateAdd(DateInterval.Day, 1, datFechaDiaCotiDet)
        '                GoTo Hoteles
        '            End If


        '            'RESTAURANTE

        '            bytContGrid = 1
        '            datFechaDiaCotiDetBusqueda = DateAdd(DateInterval.Day, -1, datFechaDiaCotiDet)
        'Restaurante:
        '            With frmRefRes.dgvDet
        '                For Each DgvFila As DataGridViewRow In .Rows
        '                    blnUbicado = False
        '                    If DgvFila.Cells("CIDTipoProv").Value.ToString = gstrTipoProveeRestaurantes Then
        '                        If DgvFila.Cells("CFecha").Value.ToString.Substring(4, 10) = datFechaDiaCotiDetBusqueda Then
        '                            blnUbicado = True
        '                        End If
        '                    End If
        '                    If blnUbicado Then
        '                        If Not DgvFila.Cells("CDescProveedor").Value Is Nothing Then
        '                            strProveedor = DgvFila.Cells("CDescProveedor").Value.ToString
        '                        Else
        '                            strProveedor = ""
        '                        End If
        '                        If strProveedor <> "" Then
        '                            If strProveedor.Length >= 10 Then
        '                                If strProveedor.Substring(0, 10) = "RESTAURANT" Then
        '                                    strTextoVer = strProveedor
        '                                Else
        '                                    strTextoVer = "RESTAURANT " & strProveedor
        '                                End If
        '                            Else
        '                                strTextoVer = ""
        '                            End If
        '                        Else
        '                            strTextoVer = ""
        '                        End If
        '                        strTextoDesc = strTextoVer
        '                        If strTextoDesc.Trim <> "" Then
        '                            'dttOrigen.Rows.Add(strTextoDesc, strTextoVer)
        '                            Dim dttTemp As DataTable = dtFiltrarDataTable(dttOrigen, "TextoVer='" & strTextoDesc & "'")
        '                            If dttTemp.Rows.Count = 0 Then
        '                                dttOrigen.Rows.Add(If(DgvFila.Cells("CIDDet").Value Is Nothing, "X" & (DgvFila.Index + 1).ToString, DgvFila.Cells("CIDDet").Value.ToString), strTextoDesc)
        '                            End If
        '                        End If
        '                    End If
        '                Next

        '            End With
        '            bytContGrid += 1
        '            If bytContGrid = 2 Then
        '                datFechaDiaCotiDetBusqueda = datFechaDiaCotiDet
        '                GoTo Restaurante
        '            ElseIf bytContGrid = 3 Then
        '                datFechaDiaCotiDetBusqueda = DateAdd(DateInterval.Day, 1, datFechaDiaCotiDet)
        '                GoTo Restaurante
        '            End If

        '            If dttOrigen.Rows.Count > 0 Then
        '                dttOrigen.Rows.Add("", "")
        '                pCargaCombosBox(cboOrigen, dttOrigen)

        '                Dim dttDestino As DataTable = dttOrigen.Clone
        '                For i As Byte = 0 To dttOrigen.Rows.Count - 1
        '                    dttDestino.ImportRow(dttOrigen.Rows(i))
        '                Next
        '                pCargaCombosBox(cboDestino, dttDestino)
        '            End If
        '        Catch ex As Exception
        '            Throw
        '        End Try
    End Sub



    Private Sub CargarCombosTransferReservas()

        '        Try
        '            Dim dttOrigen As New DataTable("Transfer")
        '            With dttOrigen
        '                .Columns.Add("TextoDesc")
        '                .Columns.Add("TextoVer")
        '            End With

        '            Dim datFechaDiaCotiDet As Date = frmRefRes.DgvDetalle.CurrentRow.Cells("Dia").Value.ToString.Substring(0, 10)
        '            Dim datFechaDiaCotiDetBusqueda As Date = DateAdd(DateInterval.Day, -1, datFechaDiaCotiDet)

        '            Dim strTextoVer As String, strTextoDesc As String
        '            Dim strProveedor As String ', strOrigen As String, strDestino As String
        '            'Dim strVuelo As String, strHora As String
        '            Dim blnUbicado As Boolean
        '            Dim bytContGrid As Byte = 1

        '            'HOTELES

        '            bytContGrid = 1
        '            datFechaDiaCotiDetBusqueda = DateAdd(DateInterval.Day, -1, datFechaDiaCotiDet)
        'Hoteles:
        '            With frmRefRes.DgvDetalle
        '                For Each DgvFila As DataGridViewRow In .Rows
        '                    blnUbicado = False
        '                    If DgvFila.Cells("IDTipoProv").Value.ToString = gstrTipoProveeHoteles Then
        '                        If DgvFila.Cells("DiaFormat").Value.ToString.Substring(4, 10) = datFechaDiaCotiDetBusqueda Then
        '                            blnUbicado = True
        '                        End If
        '                    End If
        '                    If blnUbicado Then
        '                        If Not DgvFila.Cells("DescProveedor").Value Is Nothing Then
        '                            strProveedor = DgvFila.Cells("DescProveedor").Value.ToString
        '                        Else
        '                            strProveedor = ""
        '                        End If

        '                        If strProveedor <> "" Then
        '                            If strProveedor.Length >= 5 Then
        '                                If strProveedor.Substring(0, 5) = "HOTEL" Then
        '                                    strTextoVer = Mid(strProveedor, 6)
        '                                    strTextoVer = "HTL " & strTextoVer.Trim
        '                                ElseIf strProveedor.Substring(0, 3) = "HTL" Then
        '                                    strTextoVer = Mid(strProveedor, 4)
        '                                    strTextoVer = "HTL " & strTextoVer.Trim

        '                                Else
        '                                    strTextoVer = "HTL " & strProveedor
        '                                End If


        '                            ElseIf strProveedor.Length >= 3 Then

        '                                If strProveedor.Substring(0, 3) = "HTL" Then
        '                                    strTextoVer = Mid(strProveedor, 4)
        '                                    strTextoVer = "HTL " & strTextoVer.Trim
        '                                ElseIf strProveedor.Substring(0, 5) = "HOTEL" Then
        '                                    strTextoVer = Mid(strProveedor, 6)
        '                                    strTextoVer = "HTL " & strTextoVer.Trim
        '                                Else
        '                                    strTextoVer = "HTL " & strProveedor
        '                                End If


        '                            Else
        '                                strTextoVer = strProveedor
        '                            End If


        '                        Else
        '                            strTextoVer = ""
        '                        End If

        '                        strTextoDesc = strTextoVer
        '                        If strTextoDesc.Trim <> "" Then
        '                            'dttOrigen.Rows.Add(strTextoDesc, strTextoVer)
        '                            If strTextoDesc.Trim.Substring(strTextoDesc.Trim.Length - 3, 3) = "- /" Then
        '                                strTextoDesc = strTextoDesc.Substring(0, strTextoDesc.Length - 4)
        '                            End If

        '                            Dim dttTemp As DataTable = dtFiltrarDataTable(dttOrigen, "TextoVer='" & strTextoDesc & "'")
        '                            If dttTemp.Rows.Count = 0 Then
        '                                dttOrigen.Rows.Add(If(DgvFila.Cells("IDDet").Value Is Nothing, "X" & (DgvFila.Index + 1).ToString, DgvFila.Cells("IDDet").Value.ToString), strTextoDesc)
        '                            End If
        '                        End If
        '                    End If
        '                Next

        '            End With
        '            bytContGrid += 1
        '            If bytContGrid = 2 Then
        '                datFechaDiaCotiDetBusqueda = datFechaDiaCotiDet
        '                GoTo Hoteles
        '            ElseIf bytContGrid = 3 Then
        '                datFechaDiaCotiDetBusqueda = DateAdd(DateInterval.Day, 1, datFechaDiaCotiDet)
        '                GoTo Hoteles
        '            End If


        '            'RESTAURANTE

        '            bytContGrid = 1
        '            datFechaDiaCotiDetBusqueda = DateAdd(DateInterval.Day, -1, datFechaDiaCotiDet)
        'Restaurante:
        '            With frmRefRes.DgvDetalle
        '                For Each DgvFila As DataGridViewRow In .Rows
        '                    blnUbicado = False
        '                    If DgvFila.Cells("IDTipoProv").Value.ToString = gstrTipoProveeRestaurantes Then
        '                        If DgvFila.Cells("DiaFormat").Value.ToString.Substring(4, 10) = datFechaDiaCotiDetBusqueda Then
        '                            blnUbicado = True
        '                        End If
        '                    End If
        '                    If blnUbicado Then
        '                        If Not DgvFila.Cells("DescProveedor").Value Is Nothing Then
        '                            strProveedor = DgvFila.Cells("DescProveedor").Value.ToString
        '                        Else
        '                            strProveedor = ""
        '                        End If
        '                        If strProveedor <> "" Then
        '                            If strProveedor.Length >= 10 Then
        '                                If strProveedor.Substring(0, 10) = "RESTAURANT" Then
        '                                    strTextoVer = strProveedor
        '                                Else
        '                                    strTextoVer = "RESTAURANT " & strProveedor
        '                                End If
        '                            Else
        '                                strTextoVer = ""
        '                            End If
        '                        Else
        '                            strTextoVer = ""
        '                        End If
        '                        strTextoDesc = strTextoVer
        '                        If strTextoDesc.Trim <> "" Then
        '                            'dttOrigen.Rows.Add(strTextoDesc, strTextoVer)
        '                            Dim dttTemp As DataTable = dtFiltrarDataTable(dttOrigen, "TextoVer='" & strTextoDesc & "'")
        '                            If dttTemp.Rows.Count = 0 Then
        '                                dttOrigen.Rows.Add(If(DgvFila.Cells("IDDet").Value Is Nothing, "X" & (DgvFila.Index + 1).ToString, DgvFila.Cells("IDDet").Value.ToString), strTextoDesc)
        '                            End If
        '                        End If
        '                    End If
        '                Next

        '            End With
        '            bytContGrid += 1
        '            If bytContGrid = 2 Then
        '                datFechaDiaCotiDetBusqueda = datFechaDiaCotiDet
        '                GoTo Restaurante
        '            ElseIf bytContGrid = 3 Then
        '                datFechaDiaCotiDetBusqueda = DateAdd(DateInterval.Day, 1, datFechaDiaCotiDet)
        '                GoTo Restaurante
        '            End If

        '            If dttOrigen.Rows.Count > 0 Then
        '                dttOrigen.Rows.Add("", "")
        '                pCargaCombosBox(cboOrigen, dttOrigen)

        '                Dim dttDestino As DataTable = dttOrigen.Clone
        '                For i As Byte = 0 To dttOrigen.Rows.Count - 1
        '                    dttDestino.ImportRow(dttOrigen.Rows(i))
        '                Next
        '                pCargaCombosBox(cboDestino, dttDestino)
        '            End If
        '        Catch ex As Exception
        '            Throw
        '        End Try
    End Sub


    Private Sub cboOrigen_SelectionChangeCommitted(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboOrigen.SelectionChangeCommitted, cboDestino.SelectionChangeCommitted

        Try
            If cboOrigen.Text <> "" And cboDestino.Text <> "" Then
                If cboOrigen.Text = Replace(cboDestino.Text, " - E2", "") Then
                    ErrPrv.SetError(cboOrigen, "Debe seleccionar diferentes Origen y Destino")
                    ErrPrv.SetError(cboDestino, "Debe seleccionar diferentes Origen y Destino")
                    Exit Sub
                Else
                    ErrPrv.SetError(cboOrigen, "")
                    ErrPrv.SetError(cboDestino, "")
                End If
            Else
                ErrPrv.SetError(cboOrigen, "")
                ErrPrv.SetError(cboDestino, "")
            End If
            Dim strOrigenTransfer As String = cboOrigen.Text
            Dim strDestinoTransfer As String = cboDestino.Text

            Dim bytNroFila As Byte
            If strModulo = "C" Then
                bytNroFila = frmRef.dgvDet.CurrentRow.Index
            ElseIf strModulo = "R" Or strModulo = "O" Then
                bytNroFila = 0 'frmRefRes.DgvDetalle.CurrentRow.Index
            End If

            Dim blnOrigen As Boolean = False
            If sender.name = "cboOrigen" Then
                blnOrigen = True
            End If
            strPintarTransferOrigenDestino(strOrigenTransfer, strDestinoTransfer, _
                                        strDescServicioDetBup, bytNroFila, _
                                        txtServicio, dtpDiaHora, blnOrigen, strHora)
        Catch ex As Exception
            MessageBox.Show(ex.Message, ex.Source, MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub dtpDia_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dtpDia.ValueChanged
        If blnLoad Then blnCambioFecha = True
    End Sub

    Private Sub chkIncGuia_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkIncGuia.CheckedChanged
        Try
            If blnLoad Then
                If chkIncGuia.Checked Then
                    txtNroPax.Text = 1
                    txtNroLiberados.Text = 0
                    cboTipoLib.SelectedIndex = -1
                    txtNroPax.Enabled = False
                    chkSelPax.Enabled = False

                    'BorrarSTDetPaxxIDCab(intIDCab, strIDDet)

                    grbPax.Enabled = False
                    grbPax.Height = 45

                Else
                    'txtNroPax.Text = lvwPax.CheckedItems.Count
                    txtNroLiberados.Text = intLiberados
                    txtNroPax.Text = lvwPax.CheckedItems.Count - Convert.ToInt16(If(txtNroLiberados.Text = "", "0", txtNroLiberados.Text))
                    cboTipoLib.SelectedValue = strTipo_Lib
                    txtNroPax.Enabled = True
                    chkSelPax.Enabled = True

                    grbPax.Enabled = True
                    grbPax.Height = 159

                    If txtNroPax.Text = "0" Then
                        For i As Byte = 0 To lvwPax.Items.Count - 1
                            lvwPax.Items(i).Checked = True
                        Next
                        'txtNroPax.Text = lvwPax.CheckedItems.Count
                        txtNroPax.Text = lvwPax.CheckedItems.Count - Convert.ToInt16(If(txtNroLiberados.Text = "", "0", txtNroLiberados.Text))
                    End If
                End If



                Dim dblCostoReal As Double = -1
                Dim dblMargenAplicado As Double = -1
                Dim dblSSCR As Double = -1
                Dim dblSTCR As Double = -1
                Dim dblCostoRealTxt As Double = If(txtCostoReal.Text = "", "0", txtCostoReal.Text)
                Dim dblMargenAplicadoTxt As Double = If(txtMargenAplicado.Text = "", "0", txtMargenAplicado.Text)
                Dim dblSSCRTxt As Double = If(txtSSCR2.Text = "", "0", txtSSCR2.Text)
                Dim dblSTCRTxt As Double = If(txtSTCR2.Text = "", "0", txtSTCR2.Text)

                'If dblCostoRealOrigFijo <> 0 Then
                '    If dblCostoRealTxt <> dblCostoRealOrigFijo Then
                '        dblCostoReal = dblCostoRealTxt
                '    End If
                'End If
                'If dblMargenAplicadoOrigFijo <> 0 Then
                '    If dblMargenAplicadoTxt <> dblMargenAplicadoOrigFijo Then
                '        dblMargenAplicado = dblMargenAplicadoTxt
                '    End If
                'End If
                If blnCostoRealEditado Then
                    dblCostoReal = dblCostoRealTxt
                End If
                If blnMargenAplicadoEditado Then
                    dblMargenAplicado = dblMargenAplicadoTxt
                End If
                If blnSSCREditado Then
                    dblSSCR = dblSSCRTxt
                End If
                If blnSTCREditado Then
                    dblSTCR = dblSTCRTxt
                End If
                CalcularMontos(dblCostoReal, dblMargenAplicado, _
                    dblCostoReal, dblMargenAplicado, strTipoPaxNacionalExtranjero, _
                    dblSSCR, dblSTCR)

                'If Not chkEspecial.Checked Then
                '    dblCostoRealOrigFijo = If(txtCostoReal.Text = "", "0", txtCostoReal.Text)
                '    dblMargenAplicadoOrigFijo = If(txtMargenAplicado.Text = "", "0", txtMargenAplicado.Text)
                'End If
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message, ex.Source, MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub dgvIdiom_EditingControlShowing(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewEditingControlShowingEventArgs) Handles dgvIdiom.EditingControlShowing
        If InStr(e.Control.ToString, "System.Windows.Forms.DataGridViewTextBoxEditingControl") = 0 Then Exit Sub

        Dim TextB As TextBox = DirectCast(e.Control, TextBox)
        TextB.AcceptsReturn = True
        TextB.Multiline = True
        TextB.ScrollBars = ScrollBars.Vertical
    End Sub

    'Private Sub txtNroLiberados_KeyPress(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtNroLiberados.KeyPress
    '    If Asc(e.KeyChar) <> 8 Then
    '        If Not IsNumeric(e.KeyChar) Then
    '            e.Handled = True
    '        End If
    '    End If
    'End Sub
    Public Sub BloquearConsultaReserv(ByVal vblnConsulta As Boolean)
        If Not vblnConsulta Then Exit Sub
        btnAceptar.Enabled = Not vblnConsulta
        pEnableControlsGroupBox(grbDatosGenerales, Not vblnConsulta)
        pEnableControlsGroupBox(grbPax, Not vblnConsulta)
        pEnableControlsGroupBox(grbTextos, Not vblnConsulta)
        pEnableControlsGroupBox(grbBus_Guia, Not vblnConsulta)
        btnEliminarIdiomaTexto.Visible = Not vblnConsulta
    End Sub


    Private Sub lblSSMargen2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lblSSMargen2.Click

    End Sub
    Private Sub lblSSMargenEtiq2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lblSSMargenEtiq2.Click

    End Sub

    Private Sub cboTipoTransporte_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboTipoTransporte.SelectedIndexChanged

    End Sub

    Private Sub cboOrigen_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboOrigen.SelectedIndexChanged

    End Sub

    Private Sub cboIDUbigeoOri_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboIDUbigeoOri.SelectedIndexChanged

    End Sub

    Private Sub chkseleccion_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkSelTodosPax.CheckedChanged
        'If Not blnLoad Then Exit Sub
        If Not blnReCalculoCheckSelTodos Then Exit Sub
        Try
            MarcarDesmarcarListViewPax(chkSelTodosPax.Checked)
        Catch ex As Exception
            MessageBox.Show(ex.Message, ex.Source, MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub MarcarDesmarcarListViewPax(ByVal vblnSelect As Boolean)
        Try

            If Not vblnSelect Then

                intLiberadosBup = If(txtNroLiberados.Text = "", "0", txtNroLiberados.Text)
                strTipo_LibBup = If(cboTipoLib.Text = "", "", cboTipoLib.SelectedValue.ToString)
                cboTipoLib.SelectedValue = ""
                txtNroLiberados.Text = "0"
                txtNroPax.Text = "0"
                'chkSelTodosPax.Text = "Seleccionar todos"
            Else
                cboTipoLib.SelectedValue = strTipo_Lib
                If strTipo_LibBup <> "" Then
                    cboTipoLib.SelectedValue = strTipo_LibBup
                End If
                txtNroLiberados.Text = intLiberados
                If intLiberadosBup > 0 Then
                    txtNroLiberados.Text = intLiberadosBup
                End If
                txtNroPax.Text = intPax
                'chkSelTodosPax.Text = "Desmarcar todos"
            End If

            Me.Cursor = Cursors.WaitCursor
            For Each Item As ListViewItem In lvwPax.Items
                Item.Checked = vblnSelect
            Next
            Me.Cursor = Cursors.Default
        Catch ex As Exception
            Me.Cursor = Cursors.Default
            Throw
        Finally
            Me.Cursor = Cursors.Default
        End Try
    End Sub

    Private Sub CargarControlesNetoHab()
        If strModulo <> "R" And strModulo <> "O" Then Exit Sub
        Try
            Dim vblnVisible As Boolean = True
            lblNetoHabEtiq.Visible = vblnVisible
            txtNetoHab.Visible = vblnVisible
            lblIGVHabEtiq.Visible = vblnVisible
            txtIgvHab.Visible = vblnVisible
            lblTotalHabEtiq.Visible = vblnVisible
            lblTotalHab.Visible = vblnVisible
            lblNetoTotalEtiq.Visible = vblnVisible
            lblNetoTotal.Visible = vblnVisible
            lblIgvTotalEtiq.Visible = vblnVisible
            lblIgvTotal.Visible = vblnVisible
            lblTotalMonEtiq.Visible = vblnVisible
            lblTotalR.Visible = vblnVisible
            chkEditarTotalHab.Visible = vblnVisible

            If strIDTipoProv <> gstrTipoProveeHoteles Then
                lblNetoHabEtiq.Text = Replace(lblNetoHabEtiq.Text, "Hab.", "Pax")
                lblIGVHabEtiq.Text = Replace(lblIGVHabEtiq.Text, "Hab.", "Pax")
                lblTotalHabEtiq.Text = Replace(lblTotalHabEtiq.Text, "Hab.", "Pax")
            End If

        Catch ex As Exception
            Throw
        End Try
    End Sub

    Private Sub ActualizarCtrlsMontosCostos(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtNetoHab.LostFocus, _
    txtIgvHab.Validated, txtCantidadaPagar.LostFocus, txtTotalHab.LostFocus, lblNoches.TextChanged, chkServicioNoShow.CheckedChanged
        ''If Not blnLoad Then Exit Sub
        'Try
        '    If strModulo = "" Then Exit Sub
        '    Dim bytSimple As Byte = Convert.ToByte(If(frmRefRes.txtSimpleDatDef.Text = "", "0", frmRefRes.txtSimpleDatDef.Text)) _
        '        + Convert.ToByte(If(frmRefRes.txtSimpleResidente.Text = "", "0", frmRefRes.txtSimpleResidente.Text))
        '    Dim bytTwin As Byte = Convert.ToByte(If(frmRefRes.txtTwinDatDef.Text = "", "0", frmRefRes.txtTwinDatDef.Text)) _
        '        + Convert.ToByte(If(frmRefRes.txtTwinResidente.Text = "", "0", frmRefRes.txtTwinResidente.Text))
        '    Dim bytMat As Byte = Convert.ToByte(If(frmRefRes.txtMatrimDatDef.Text = "", "0", frmRefRes.txtMatrimDatDef.Text)) _
        '        + Convert.ToByte(If(frmRefRes.txtMatResidente.Text = "", "0", frmRefRes.txtMatResidente.Text))
        '    Dim bytTriple As Byte = Convert.ToByte(If(frmRefRes.txtTripleDatDef.Text = "", "0", frmRefRes.txtTripleDatDef.Text)) _
        '        + Convert.ToByte(If(frmRefRes.txtTripleResidente.Text = "", "0", frmRefRes.txtTripleResidente.Text))
        '    Dim bytCapacidadHab As Byte = 0
        '    If Not frmRefRes.DgvDetalle.CurrentRow.Cells("CapacidadHab").Value Is Nothing Then
        '        bytCapacidadHab = If(frmRefRes.DgvDetalle.CurrentRow.Cells("CapacidadHab").Value.ToString = "", "0", frmRefRes.DgvDetalle.CurrentRow.Cells("CapacidadHab").Value)
        '    End If
        '    Dim bytNoches As Byte = 0
        '    'bytNoches = If(lblNoches.Text.Trim = "", 0, CByte(lblNoches.Text.Trim))
        '    If dtpFechaOut.Value.ToShortDateString = "01/01/1900" Then
        '        If Not frmRefRes.DgvDetalle.CurrentRow.Cells("Noches").Value Is Nothing Then
        '            bytNoches = If(frmRefRes.DgvDetalle.CurrentRow.Cells("Noches").Value.ToString = "", "0", frmRefRes.DgvDetalle.CurrentRow.Cells("Noches").Value)
        '        End If
        '    Else
        '        bytNoches = If(lblNoches.Text.Trim = "", 0, CByte(lblNoches.Text.Trim))
        '    End If

        '    Dim blnEsMatrimonial As Boolean = frmRefRes.DgvDetalle.CurrentRow.Cells("EsMatrimonial").Value
        '    Dim bytDias As Byte = frmRefRes.DgvDetalle.CurrentRow.Cells("Dias").Value
        '    Dim blnConAlojamiento As Boolean = frmRefRes.DgvDetalle.CurrentRow.Cells("ConAlojamiento").Value
        '    Dim dblSSCR As Double = frmRefRes.DgvDetalle.CurrentRow.Cells("SSCR").Value


        '    Dim objCotBN As New clsCotizacionBN.clsDetalleCotizacionBN
        '    Dim drCotiDet As Object = objCotBN.ConsultarPaLiberadosxIDCab(intIDCab)
        '    Dim ListDetCot As New List(Of clsCotizacionBE.clsDetalleCotizacionBE)

        '    While drCotiDet.Read
        '        Dim objDetCot As New clsCotizacionBE.clsDetalleCotizacionBE
        '        objDetCot.IDDet = drCotiDet("IDDet")
        '        objDetCot.NroPax = drCotiDet("NroPax")
        '        objDetCot.NroLiberados = drCotiDet("NroLiberados")
        '        objDetCot.CostoRealEditado = drCotiDet("CostoRealEditado")
        '        ListDetCot.Add(objDetCot)
        '    End While

        '    'Obtener Si es CostoRealEditado
        '    Dim blnCostoRealEditado As Boolean = False
        '    For Each Item As clsCotizacionBE.clsDetalleCotizacionBE In ListDetCot
        '        If Item.IDDet = intIDDet Then
        '            blnCostoRealEditado = Item.CostoRealEditado
        '            Exit For
        '        End If
        '    Next

        '    Dim objDetResBE As New clsReservaBE.clsDetalleReservaBE
        '    With objDetResBE
        '        .IDDet = intIDDet
        '        .IDServicio_Det = intIDServicio_Det
        '        .IDTipoProv = strIDTipoProv
        '        .PlanAlimenticio = blnPlanAlimenticio
        '        .CostoReal = txtCostoReal.Text
        '        .Servicio = txtServicio.Text.Trim
        '        .CostoRealEditado = blnCostoRealEditado
        '        .CostoLiberado = lblCostoLiberado.Text
        '        .IDPais = cboPais.SelectedValue.ToString
        '        .IDProveedor = strIDProveedor
        '        .NroPax = txtNroPax.Text
        '        .NroLiberados = If(txtNroLiberados.Text = "", "0", txtNroLiberados.Text)
        '        .IncGuia = chkIncGuia.Checked
        '        .CapacidadHab = bytCapacidadHab
        '        .Noches = bytNoches
        '        .EsMatrimonial = blnEsMatrimonial
        '        .Dias = bytDias
        '        .ConAlojamiento = blnConAlojamiento
        '        .SSCR = dblSSCR
        '        .FlServicioNoShow = chkServicioNoShow.Checked
        '    End With

        '    If sender.name <> "txtTotalHab" Then
        '        Dim objResBT As New clsReservaBT
        '        objResBT.PasarDatosPropiedadesaCostosDetReservasBE(objDetResBE, _
        '                                                           bytSimple, bytTwin, _
        '                                                           bytMat, bytTriple, _
        '                                                           0, 0, 0, 0, 0, _
        '                                                           dblTipoCambioCotiCab, _
        '                                                           ListDetCot, _
        '                                                           True, _
        '                                                           txtCantidadaPagar.Text, _
        '                                                           txtNetoHab.Text, _
        '                                                           txtIgvHab.Text)

        '        txtCantidadaPagar.Text = objDetResBE.CantidadAPagar.ToString("###0")
        '        txtNetoHab.Text = objDetResBE.NetoHab.ToString("###,###0.0000")
        '        txtIgvHab.Text = objDetResBE.IgvHab.ToString("###,###0.0000")
        '        lblTotalHab.Text = objDetResBE.TotalHab.ToString("###,###0.0000")
        '        lblNetoTotal.Text = objDetResBE.NetoGen.ToString("###,###0.0000")
        '        lblIgvTotal.Text = objDetResBE.IgvGen.ToString("###,###0.0000")
        '        lblTotalR.Text = objDetResBE.TotalGen.ToString("###,###0.00")

        '    Else
        '        CalcularMontosenBaseaTotalHab(objDetResBE, _
        '                                           bytSimple, bytTwin, _
        '                                           bytMat, bytTriple, ListDetCot)
        '    End If


        'Catch ex As Exception
        '    'Throw
        '    MessageBox.Show(ex.Message, ex.Source, MessageBoxButtons.OK, MessageBoxIcon.Error)
        'End Try
    End Sub
    Private Sub CalcularMontosenBaseaTotalHab(ByVal vobjDetResBE As clsReservaBE.clsDetalleReservaBE, _
                                              ByVal vbytSimple As Byte, ByVal vbytTwin As Byte, _
                                              ByVal vbytMat As Byte, ByVal vbytTriple As Byte, _
                                              ByVal vListDetCot As List(Of clsCotizacionBE.clsDetalleCotizacionBE))

        Try
            Dim objResBT As New clsReservaBT
            objResBT.PasarDatosPropiedadesaCostosDetReservasBE(vobjDetResBE, _
                                                               vbytSimple, vbytTwin, _
                                                               vbytMat, vbytTriple, _
                                                               0, 0, 0, 0, 0, _
                                                               dblTipoCambioCotiCab, _
                                                               vListDetCot, _
                                                               False)

            Dim dblTotalHab As Double = vobjDetResBE.TotalHab
            Dim dblNetoHab As Double = vobjDetResBE.NetoHab
            Dim dblIgvHab As Double = vobjDetResBE.IgvHab

            Dim dblNetoTotal As Double = vobjDetResBE.NetoGen
            Dim dblIgvTotal As Double = vobjDetResBE.IgvGen
            Dim dblTotal As Double = vobjDetResBE.TotalGen

            If txtTotalHab.Text.Trim = "" Then txtTotalHab.Text = "0.0000"

            txtNetoHab.Text = ((Convert.ToDouble(txtTotalHab.Text) * dblNetoHab) / dblTotalHab).ToString("###,###0.0000")   'regla de 3
            txtIgvHab.Text = ((Convert.ToDouble(txtTotalHab.Text) * dblIgvHab) / dblTotalHab).ToString("###,###0.0000")   'regla de 3

            lblNetoTotal.Text = ((Convert.ToDouble(txtTotalHab.Text) * dblNetoTotal) / dblTotalHab).ToString("###,###0.0000")   'regla de 3
            lblIgvTotal.Text = ((Convert.ToDouble(txtTotalHab.Text) * dblIgvTotal) / dblTotalHab).ToString("###,###0.0000")   'regla de 3
            lblTotalR.Text = ((Convert.ToDouble(txtTotalHab.Text) * dblTotal) / dblTotalHab).ToString("###,###0.0000")   'regla de 3

            txtTotalHab.Text = Convert.ToDouble(txtTotalHab.Text).ToString("###,###0.0000")
            lblTotalHab.Text = txtTotalHab.Text
        Catch ex As Exception
            Throw
        End Try
    End Sub
    Private Function blnEsOtraCiudadBuss() As Boolean
        Dim EsOtraCiudad As Boolean = True
        If cboCiudad.SelectedValue = gstrLima Then
            EsOtraCiudad = False
        Else
            If cboCiudad.SelectedValue = gstrNasca Or cboCiudad.SelectedValue = gstrParacas Then
                If lblTipoServicio.Text.Trim = "PVT" Then
                    EsOtraCiudad = False
                End If
            End If
        End If
        Return EsOtraCiudad
    End Function

    Private Sub CargarVehiculos()
        Try
            Dim blnEsOtraCiudad As Boolean = blnEsOtraCiudadBuss()

            Dim objVi As New clsTablasApoyoBN.clsVehiculosBN
            'pCargaCombosBox(CboBusVehiculo, objVi.ConsultarList(blnEsOtraCiudad))

            Dim bytTipoVeh As Byte = 2
            If chkTransfer.Checked = True Then
                bytTipoVeh = 2
            Else
                bytTipoVeh = 3
            End If

            pCargaCombosBox(CboBusVehiculo, objVi.ConsultarList2(cboCiudad.SelectedValue, strIDTipoServ, bytTipoVeh, strIDProveedor, strTipoServicio_Det, _
                            cboIDUbigeoOri.SelectedValue.ToString, cboIDUbigeoDes.SelectedValue.ToString, strIDCliente))
        Catch ex As Exception
            Throw
        End Try
    End Sub

    Private Function bytValorNuVehiculo(ByVal blnEsOtraCiudad As Boolean, ByVal bytCantidadPax As Byte) As Byte
        Try
            Dim objBN As New clsTablasApoyoBN.clsVehiculosBN
            Return objBN.ConsultarIDxRango(blnEsOtraCiudad, bytCantidadPax)
        Catch ex As Exception
            Throw
        End Try
    End Function

    Private Function bytValorNuVehiculo2(ByVal vstrCiudad As String, ByVal vstrTipoServicio As String, ByVal vbytCoTipo As Byte, ByVal bytCantidadPax As Byte) As Byte
        Try
            Dim objBN As New clsTablasApoyoBN.clsVehiculosBN
            Return objBN.ConsultarIDxRango2(vstrCiudad, bytCantidadPax, vbytCoTipo, vstrTipoServicio, strIDProveedor, strTipoServicio_Det, cboIDUbigeoOri.SelectedValue.ToString, cboIDUbigeoDes.SelectedValue.ToString, strIDCliente)
        Catch ex As Exception
            Throw
        End Try
    End Function

    Private Sub chkEditarTotalHab_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkEditarTotalHab.CheckedChanged
        If chkEditarTotalHab.Checked Then
            txtTotalHab.Visible = True
            lblTotalHab.Visible = False
        Else
            txtTotalHab.Visible = False
            lblTotalHab.Visible = True
        End If
    End Sub

#Region "ACOMODO ESPECIAL"

    Private Sub CargarListaAcomodoEspecial()
        If Not IsNumeric(strIDDet) Then Exit Sub
        Try
            Dim objBNDet As New clsCotizacionBN.clsDetalleCotizacionBN.clsDetalleCotizacionAcomodoEspecialBN
            Dim dt As DataTable = objBNDet.ConsultarListxIDDet(CInt(strIDDet))

            Dim oAcomodoEspecial As stAcodomodoEspecial
            If dt.Rows.Count > 0 Then
                objLstAcodomodoEspecial.Clear()

                For Each Dr As DataRow In dt.Rows
                    oAcomodoEspecial = New stAcodomodoEspecial
                    oAcomodoEspecial.IDCab = intIDCab
                    oAcomodoEspecial.IDDet = CInt(strIDDet)
                    oAcomodoEspecial.QtPax = If(IsDBNull(Dr("QtPax")), 0, Dr("QtPax"))
                    oAcomodoEspecial.CoCapacidad = If(IsDBNull(Dr("CoCapacidad")), "", Dr("CoCapacidad"))
                    oAcomodoEspecial.Almacenado = "S"
                    oAcomodoEspecial.Accion = ""

                    objLstAcodomodoEspecial.Add(oAcomodoEspecial)
                Next
            Else
                For int_vContador As Int16 = 0 To objLstAcodomodoEspecial.Count - 1
                    With objLstAcodomodoEspecial(int_vContador)
                        If CInt(strIDDet) <> .IDDet Then
                            objLstAcodomodoEspecial.Clear()
                            Exit For
                        End If
                    End With
                Next
            End If

            Dim objBNDis As New clsCotizacionBN.clsDetalleCotizacionBN.clsDetalleCotizacionAcomodoEspecialBN.clsDistribucAcomodoEspecialBN
            Dim dtDis As DataTable = objBNDis.ConsultarListxIDDet(CInt(strIDDet))

            Dim oAcomodoEspecialDistribucion As stAcodomodoEspecialDistribucion
            If dtDis.Rows.Count > 0 Then
                objLstAcodomodoEspecialDistribucion.Clear()

                For Each Dr As DataRow In dtDis.Rows
                    oAcomodoEspecialDistribucion = New stAcodomodoEspecialDistribucion
                    oAcomodoEspecialDistribucion.IDCab = intIDCab
                    oAcomodoEspecialDistribucion.IDDet = CInt(strIDDet)
                    oAcomodoEspecialDistribucion.CoCapacidad = If(IsDBNull(Dr("CoCapacidad")), "", Dr("CoCapacidad"))
                    oAcomodoEspecialDistribucion.IDPax = If(IsDBNull(Dr("IDPax")), 0, Dr("IDPax"))
                    oAcomodoEspecialDistribucion.IDHabit = If(IsDBNull(Dr("IDHabit")), 0, Dr("IDHabit"))
                    oAcomodoEspecialDistribucion.Accion = ""

                    objLstAcodomodoEspecialDistribucion.Add(oAcomodoEspecialDistribucion)
                Next
            Else
                For int_vContador As Int16 = 0 To objLstAcodomodoEspecialDistribucion.Count - 1
                    With objLstAcodomodoEspecialDistribucion(int_vContador)
                        If CInt(strIDDet) <> .IDDet Then
                            objLstAcodomodoEspecialDistribucion.Clear()
                            Exit For
                        End If
                    End With
                Next
            End If
        Catch ex As Exception
            Throw
        End Try
    End Sub

    Private Sub chkAcomodoEspecial_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkAcomodoEspecial.CheckedChanged
        If chkAcomodoEspecial.Checked Then
            btnVerAcomodoEspecial.Visible = True
            If Not blnHabilitarAcomodoEspecial Then Exit Sub
            Dim frm As New frmIngDetAcomodoEspecial
            Dim strIDDet As String = frmRef.dgvDet.CurrentRow.Cells("IDDet").Value.ToString
            ErrPrv.SetError(chkAcomodoEspecial, "")
            With frm
                .chrModulo = strModulo
                .intIDCab = intIDCab
                .strIDDet = strIDDet
                .intPax = Int16.Parse(txtNroPax.Text) + Int16.Parse(If(txtNroLiberados.Text = "", "0", txtNroLiberados.Text))
                'Cargando la TablaTemp
                Dim dtPaxSelec As New DataTable
                dtPaxSelec.Columns.Add("IDPax")
                dtPaxSelec.Columns.Add("Nombre")
                'If Not IsNumeric(strIDDet) Then
                For Each Item As ListViewItem In lvwPax.Items
                    If Item.Checked Then
                        dtPaxSelec.Rows.Add(lvwPax.Items(Item.Index).SubItems(3).Text, lvwPax.Items(Item.Index).SubItems(0).Text & " " & lvwPax.Items(Item.Index).SubItems(1).Text)
                    End If
                Next
                'End If
                .dttPaxSeleccDinam = dtPaxSelec
                .ShowDialog()

                If .blnCambios Then
                    blnGrabarAcomodoEspecial = True
                    btnAceptar.Enabled = True
                    blnContieneAcomodoEsp = True
                    blnEliminoAcomodosEsp = False
                End If
            End With
            If objLstAcodomodoEspecial.Count = 0 Then
                blnHabilitarAcomodoEspecial = False
                chkAcomodoEspecial.Checked = False
                blnHabilitarAcomodoEspecial = True
            Else
                btnVerAcomodoEspecial.Visible = True
            End If

        Else
            btnVerAcomodoEspecial.Visible = False
            If Not blnHabilitarAcomodoEspecial Then Exit Sub
            If MessageBox.Show("¿Está Ud. seguro de eliminar los acomodos especiales para este servicio?", gstrTituloMsg, MessageBoxButtons.YesNo, MessageBoxIcon.Question) = Windows.Forms.DialogResult.Yes Then
                btnVerAcomodoEspecial.Visible = False
                'objLstAcodomodoEspecial.Clear()

                Dim str_vHabitacionId As String = ""
                Dim int_vPax As Integer = 0
                Dim oDatos As stAcodomodoEspecial
                For int_vContador As Int16 = 0 To objLstAcodomodoEspecial.Count - 1
                    With objLstAcodomodoEspecial(0)
                        str_vHabitacionId = .CoCapacidad
                        int_vPax = .QtPax
                        objLstAcodomodoEspecial.RemoveAt(0)

                        oDatos.IDCab = intIDCab
                        oDatos.IDDet = intIDDet
                        oDatos.CoCapacidad = str_vHabitacionId
                        oDatos.QtPax = int_vPax
                        oDatos.Accion = "B"
                        objLstAcodomodoEspecial.Add(oDatos)
                    End With
                Next


                Dim int_vHabitacionSecuencia As Int16 = 0
                Dim oDatosDistribucion As New stAcodomodoEspecialDistribucion
                For int_vContador As Int16 = 0 To objLstAcodomodoEspecialDistribucion.Count - 1
                    With objLstAcodomodoEspecialDistribucion(0)
                        str_vHabitacionId = .CoCapacidad
                        int_vPax = .IDPax
                        int_vHabitacionSecuencia = .IDHabit
                        objLstAcodomodoEspecialDistribucion.RemoveAt(0)

                        oDatosDistribucion.IDCab = intIDCab
                        oDatosDistribucion.IDDet = intIDDet
                        oDatosDistribucion.CoCapacidad = str_vHabitacionId
                        oDatosDistribucion.IDPax = int_vPax
                        oDatosDistribucion.IDHabit = int_vHabitacionSecuencia
                        oDatosDistribucion.Accion = "B"
                        objLstAcodomodoEspecialDistribucion.Add(oDatosDistribucion)
                    End With
                Next

                blnGrabarAcomodoEspecial = True
                blnEliminoAcomodosEsp = True
                Exit Sub
            Else
                blnHabilitarAcomodoEspecial = False
                chkAcomodoEspecial.Checked = True
                blnHabilitarAcomodoEspecial = True
            End If

        End If
    End Sub

    Private Sub btnVerAcomodoEspecial_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnVerAcomodoEspecial.Click
        Dim frm As New frmIngDetAcomodoEspecial
        With frm
            Dim dtPaxSelec As New DataTable
            dtPaxSelec.Columns.Add("IDPax")
            dtPaxSelec.Columns.Add("Nombre")
            For Each Item As ListViewItem In lvwPax.Items
                If Item.Checked Then
                    dtPaxSelec.Rows.Add(lvwPax.Items(Item.Index).SubItems(3).Text, lvwPax.Items(Item.Index).SubItems(0).Text & " " & lvwPax.Items(Item.Index).SubItems(1).Text)
                End If
            Next
            .dttPaxSeleccDinam = dtPaxSelec
            .chrModulo = strModulo
            .intIDCab = intIDCab
            .strIDDet = strIDDet
            .intPax = Int16.Parse(txtNroPax.Text) + Int16.Parse(If(txtNroLiberados.Text = "", "0", txtNroLiberados.Text))
            .ShowDialog()

            If .blnCambios Then
                blnGrabarAcomodoEspecial = True
                btnAceptar.Enabled = True
            End If
        End With
    End Sub

#End Region


    Private Sub lnkDocumentoSustento_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkDocumentoSustento.LinkClicked
        Try
            If lnkDocumentoSustento.Text.Trim <> "" Then
                If System.IO.File.Exists(lnkDocumentoSustento.Text.Trim) Then
                    Process.Start(lnkDocumentoSustento.Text.Trim)
                Else
                    MessageBox.Show("El archivo seleccionado ha sido eliminado o removido de su sitio recientemente.", gstrTituloMsg, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                End If
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message, ex.Source, MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    'Private Sub chkServicioNoShow_CheckedChanged(sender As Object, e As EventArgs) Handles chkServicioNoShow.CheckedChanged
    '    If Not blnLoad Then Exit Sub
    '    Try
    '        If chrModulo = "R" Or chrModulo = "O" Then
    '            CalcularMontosSegunNoShow(chkServicioNoShow.Checked)
    '        End If
    '    Catch ex As Exception
    '        MessageBox.Show(ex.Message, ex.Source, MessageBoxButtons.OK, MessageBoxIcon.Error)
    '    End Try
    'End Sub


    'Private Sub CalcularMontosSegunNoShow(ByVal vblnServNoShow As Boolean)
    '    Try
    '        Dim dblNetoHab As Double = If(txtNetoHab.Text.Trim = "", 0, Convert.ToDouble(txtNetoHab.Text.Trim))
    '        Dim dblIgvHab As Double = 0, dblTotalHab As Double = 0
    '        If ((strIDTipoProv <> gstrTipoProveeHoteles And blnConAlojamiento) Or _
    '            (strIDTipoProv = gstrTipoProveeHoteles And Not blnPlanAlimenticio)) And vblnServNoShow Then
    '            dblIgvHab = dblNetoHab * (gsglIGV / 100)
    '        End If
    '        dblTotalHab = dblNetoHab + dblIgvHab
    '        Dim dblNetoGen As Double = If(lblNetoTotal.Text.Trim = "", 0, Convert.ToDouble(lblNetoTotal.Text.Trim))
    '        'Dim dblCantPag As Double = If(txtCantidadaPagar.Text.Trim = "", 0, Convert.ToDouble(txtCantidadaPagar.Text.Trim))
    '        Dim dblIgvGen As Double = 0, dblTotalGen As Double = 0
    '        If ((strIDTipoProv <> gstrTipoProveeHoteles And blnConAlojamiento) Or _
    '            (strIDTipoProv = gstrTipoProveeHoteles And Not blnPlanAlimenticio)) And vblnServNoShow Then
    '            dblIgvGen = (dblNetoGen * (gsglIGV / 100))
    '        End If
    '        dblTotalGen = dblIgvGen + dblNetoGen

    '        txtIgvHab.Text = Format(dblIgvHab, "##,##0.0000")
    '        txtTotalHab.Text = Format(dblTotalHab, "##,##0.0000")
    '        lblTotalHab.Text = Format(dblTotalHab, "##,##0.0000")

    '        lblIgvTotal.Text = Format(dblIgvGen, "##,##0.0000")
    '        lblTotalR.Text = Format(dblTotalGen, "##,##0.0000")
    '    Catch ex As Exception
    '        Throw
    '    End Try
    'End Sub

    Private Sub CargarVehiculosAcomodo()
        Try
            dgvAcoVeh.Rows.Clear()
            Dim blnExiste As Boolean = False
            For Each ItemAcoVeh As clsCotizacionBE.clsAcomodoVehiculoBE In frmRef.ListaAcomoVehic
                If ItemAcoVeh.IDDet = strIDDet Then
                    blnExiste = True
                    Exit For
                End If
            Next
            'If frmRef.ListaAcomoVehic.Count = 0 Then
            If Not blnExiste Then
                Dim intCoTipoVehic As Int16 = 0
                If cboTipoProv.SelectedValue.ToString = gstrTipoProveeTransportista Or _
                    (cboTipoProv.SelectedValue.ToString = gstrTipoProveeOperadores And Not chkTransfer.Checked) Then
                    intCoTipoVehic = 3 'Viajes
                Else
                    If chkTransfer.Checked Then
                        intCoTipoVehic = 2 'Transporte
                    Else
                        intCoTipoVehic = 1 'Excursion
                    End If
                End If

                'Dim objBN As New clsAcomodoVehiculoBN
                'Dim dtt As DataTable = objBN.ConsultarxIDDet(intIDDet, cboCiudad.SelectedValue, strIDCliente, lblVariante.Text, intCoTipoVehic)
                Using objBN As New clsAcomodoVehiculoBN
                    Using dr As SqlClient.SqlDataReader = objBN.ConsultarxIDDet(intIDDet, cboCiudad.SelectedValue, strIDCliente, lblVariante.Text, intCoTipoVehic)
                        While dr.Read
                            dgvAcoVeh.Rows.Add()
                            dgvAcoVeh.Item("QtVehiculos", dgvAcoVeh.Rows.Count - 1).Value = dr("QtVehiculos")
                            dgvAcoVeh.Item("NuVehiculo", dgvAcoVeh.Rows.Count - 1).Value = dr("NuVehiculo")
                            dgvAcoVeh.Item("NoVehiculo", dgvAcoVeh.Rows.Count - 1).Value = dr("NoVehiculo")
                            dgvAcoVeh.Item("QtCapacidad", dgvAcoVeh.Rows.Count - 1).Value = If(IsDBNull(dr("QtCapacidad")), "", dr("QtCapacidad"))
                            dgvAcoVeh.Item("QtPax", dgvAcoVeh.Rows.Count - 1).Value = If(IsDBNull(dr("QtPax")), "", dr("QtPax"))
                            dgvAcoVeh.Item("FlGuia", dgvAcoVeh.Rows.Count - 1).Value = dr("FlGuia")

                        End While
                        dr.Close()
                    End Using
                End Using

            Else
                For Each ItemAcoVeh As clsCotizacionBE.clsAcomodoVehiculoBE In frmRef.ListaAcomoVehic
                    If ItemAcoVeh.IDDet = strIDDet Then
                        dgvAcoVeh.Rows.Add()
                        'If ItemAcoVeh.QtPax > 0 Then
                        dgvAcoVeh.Item("QtVehiculos", dgvAcoVeh.Rows.Count - 1).Value = ItemAcoVeh.QtVehiculos
                        'Else
                        '   dgvAcoVeh.Item("QtVehiculos", dgvAcoVeh.Rows.Count - 1).Value = "1"
                        'End If
                        dgvAcoVeh.Item("NuVehiculo", dgvAcoVeh.Rows.Count - 1).Value = ItemAcoVeh.NuVehiculo
                        dgvAcoVeh.Item("NoVehiculo", dgvAcoVeh.Rows.Count - 1).Value = ItemAcoVeh.NoVehiculo
                        dgvAcoVeh.Item("QtCapacidad", dgvAcoVeh.Rows.Count - 1).Value = ItemAcoVeh.QtCapacidad
                        dgvAcoVeh.Item("QtPax", dgvAcoVeh.Rows.Count - 1).Value = ItemAcoVeh.QtPax
                        If ItemAcoVeh.QtPax > 0 Then
                            dgvAcoVeh.Item("FlGuia", dgvAcoVeh.Rows.Count - 1).Value = ItemAcoVeh.FlGuia
                        Else
                            dgvAcoVeh.Item("FlGuia", dgvAcoVeh.Rows.Count - 1).Value = False
                        End If
                    End If
                Next
            End If
        Catch ex As Exception
            Throw
        End Try
    End Sub

    Private Sub CargarVehiculosAcomodoExt()
        Try
            txtPaxTotal.Text = ""
            txtGruposPax.Text = ""

            Dim blnExiste As Boolean = False
            For Each ItemAcoVeh As clsCotizacionBE.clsAcomodoVehiculo_Ext_BE In frmRef.ListaAcomoVehicExt
                If ItemAcoVeh.IDDet = strIDDet Then
                    blnExiste = True
                    Exit For
                End If
            Next
            'If frmRef.ListaAcomoVehic.Count = 0 Then
            If Not blnExiste Then
                Using objBN As New clsAcomodoVehiculoExtBN
                    Using dr As SqlClient.SqlDataReader = objBN.ConsultarPk(intIDDet)
                        If dr.HasRows Then
                            dr.Read()
                            txtPaxTotal.Text = dr("QtPax")
                            txtGruposPax.Text = dr("QtGrupo")
                        Else
                            blnAcomoVehiReciente = False
                        End If
                        dr.Close()
                    End Using
                End Using

            Else
                For Each ItemAcoVeh As clsCotizacionBE.clsAcomodoVehiculo_Ext_BE In frmRef.ListaAcomoVehicExt
                    If ItemAcoVeh.IDDet = strIDDet Then
                        dgvAcoVeh.Rows.Add()
                        txtPaxTotal.Text = ItemAcoVeh.QtPax
                        txtGruposPax.Text = ItemAcoVeh.QtGrupo
                    End If
                Next
            End If

        Catch ex As Exception
            Throw
        End Try
    End Sub

    Private Sub dgvAcoVeh_CellBeginEdit(sender As Object, e As DataGridViewCellCancelEventArgs) Handles dgvAcoVeh.CellBeginEdit
        blnCambios = True
        btnAceptar.Enabled = True
    End Sub

    Private Sub dgvAcoVeh_CellEndEdit(sender As Object, e As DataGridViewCellEventArgs) Handles dgvAcoVeh.CellEndEdit

        Try
            ErrPrv.SetError(dgvAcoVeh, "")
            dgvAcoVeh.EndEdit()

            If e.ColumnIndex <> dgvAcoVeh.Columns("QtPax").Index And
                e.ColumnIndex <> dgvAcoVeh.Columns("QtVehiculos").Index And
                e.ColumnIndex <> dgvAcoVeh.Columns("FlGuia").Index Then
                Exit Sub
            End If
            Dim bytQtPax As Byte = 0
            If Not dgvAcoVeh.CurrentRow.Cells("QtPax").Value Is Nothing Then
                If dgvAcoVeh.CurrentRow.Cells("QtPax").Value.ToString = "" Or dgvAcoVeh.CurrentRow.Cells("QtPax").Value.ToString = "0" Then
                Else
                    bytQtPax = dgvAcoVeh.CurrentRow.Cells("QtPax").Value.ToString

                    Dim bytQtVehiculos As Byte = 0
                    If Not dgvAcoVeh.CurrentRow.Cells("QtVehiculos").Value Is Nothing Then
                        If dgvAcoVeh.CurrentRow.Cells("QtVehiculos").Value.ToString = "" Or dgvAcoVeh.CurrentRow.Cells("QtVehiculos").Value.ToString = "0" Then
                        Else
                            bytQtVehiculos = dgvAcoVeh.CurrentRow.Cells("QtVehiculos").Value.ToString
                        End If
                    End If
                    If bytQtVehiculos = 0 Then
                        dgvAcoVeh.CurrentRow.Cells("QtVehiculos").Value = "1"
                    End If
                End If
            End If


            Dim bytQtCapacidad As Byte = 0
            If Not dgvAcoVeh.CurrentRow.Cells("QtCapacidad").Value Is Nothing Then
                If dgvAcoVeh.CurrentRow.Cells("QtCapacidad").Value.ToString = "" Or dgvAcoVeh.CurrentRow.Cells("QtCapacidad").Value.ToString = "0" Then
                Else
                    bytQtCapacidad = dgvAcoVeh.CurrentRow.Cells("QtCapacidad").Value.ToString
                End If
            End If
            If Not blnValidarPaxAcomodoVehiculo(bytQtPax, bytQtCapacidad) Then
                dgvAcoVeh.CurrentRow.Cells("QtPax").Value = dgvAcoVeh.CurrentRow.Cells("QtCapacidad").Value
            End If


            blnCambios = True
            btnAceptar.Enabled = True

            ActualizarAcomoVehicST(e.RowIndex)

            If blnValidarAcomodoVehiculo() Then
                CargarTreeViewsPaxAcoVehInt()
                PintarChecksTvwAcoVehInt()
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message, ex.Source, MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub


    Private Sub dgvAcoVeh_EditingControlShowing(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewEditingControlShowingEventArgs) Handles dgvAcoVeh.EditingControlShowing
        If InStr(e.Control.ToString, "System.Windows.Forms.DataGridViewTextBoxEditingControl") = 0 Then Exit Sub

        Dim txtEntero As TextBox = CType(e.Control, TextBox)

        AddHandler txtEntero.KeyPress, AddressOf ValidarNrosEnteros_Keypress
        'AddHandler txtEntero.TextChanged, AddressOf ValidarNroEnteros_TextChanged

    End Sub
    Private Sub ValidarNrosEnteros_Keypress( _
         ByVal sender As Object, _
         ByVal e As System.Windows.Forms.KeyPressEventArgs)

        Dim intColumna As Byte = dgvAcoVeh.CurrentCell.ColumnIndex

        If intColumna = dgvAcoVeh.Columns("QtVehiculos").Index Or intColumna = dgvAcoVeh.Columns("QtPax").Index Then

            Dim Caracter As Char = e.KeyChar

            ' comprobar si el caracter es un número o punto o coma o el retroceso   
            If Not IsNumeric(Caracter) And (Caracter = ChrW(Keys.Back)) = False Then
                'Me.Text = e.KeyChar   
                e.KeyChar = Chr(0)
            End If
        End If
    End Sub

    Private Function blnValidarAcomodoVehiculo() As Boolean
        If Not chkAcomVehi.Checked Then
            Return True
        End If
        Dim blnOk As Boolean = True

        If strIDTipoOper = "I" Then
            Dim intPax As Int16 = Convert.ToInt16(txtNroPax.Text) + Convert.ToInt16(If(txtNroLiberados.Text = "", 0, txtNroLiberados.Text))
            Dim intTotPax As Int16 = 0
            Dim blnCheckGuia As Boolean = False
            For Each dr As DataGridViewRow In dgvAcoVeh.Rows
                Dim bytPax As Byte = 0
                If Not dr.Cells("QtPax").Value Is Nothing Then
                    bytPax = Convert.ToByte(If(dr.Cells("QtPax").Value.ToString = "", 0, dr.Cells("QtPax").Value))
                End If
                Dim bytQtVehiculos As Byte = 0
                If Not dr.Cells("QtVehiculos").Value Is Nothing Then
                    bytQtVehiculos = Convert.ToByte(If(dr.Cells("QtVehiculos").Value.ToString = "", 0, dr.Cells("QtVehiculos").Value))
                End If
                If Not blnCheckGuia Then
                    blnCheckGuia = dr.Cells("FlGuia").Value
                End If
                intTotPax += bytPax * bytQtVehiculos
            Next
            If intTotPax <> intPax Then
                Tab.SelectTab(tpgAcomVehi)
                'ErrPrv.SetError(dgvAcoVeh, "La cantidad de pax del acomodo de vehículos ha excedido la cantidad de pax del servicio.")
                ErrPrv.SetError(dgvAcoVeh, "La cantidad de pax del acomodo de vehículos debe ser igual a la cantidad de pax del servicio.")
                blnOk = False
            Else
                'If blnTieneSubServiciosGuia And Not blnCheckGuia Then
                '    Tab.SelectTab(tpgAcomVehi)
                '    'ErrPrv.SetError(dgvAcoVeh, "La cantidad de pax del acomodo de vehículos ha excedido la cantidad de pax del servicio.")
                '    ErrPrv.SetError(dgvAcoVeh, "Debe marcar al menos un guía.")
                '    blnOk = False
                'End If
            End If
            If chkSelCopiaAcoVeh.Checked Then
                If intTotPax = 0 And lvwServAcomVehi.CheckedItems.Count > 0 Then
                    Tab.SelectTab(tpgAcomVehi)
                    ErrPrv.SetError(lvwServAcomVehi, "Debe ingresar acomodo de vehículos, ya que ha seleccionar servicio para copia.")
                    blnOk = False
                End If
            End If
        ElseIf strIDTipoOper = "E" Then
            Dim intPaxTotal As Int16 = 0

            If txtPaxTotal.Text <> "" Then
                intPaxTotal = txtPaxTotal.Text
            End If
            Dim bytGruposPax As Byte = 0
            If txtGruposPax.Text <> "" Then
                bytGruposPax = txtGruposPax.Text
            End If

            If intPaxTotal = 0 Then
                Tab.SelectTab(tpgAcomVehi)
                ErrPrv.SetError(txtPaxTotal, "Debe ingresar Pax Total del acomodo de vehículos.")
                blnOk = False
            End If
            If bytGruposPax = 0 Then
                Tab.SelectTab(tpgAcomVehi)
                ErrPrv.SetError(txtGruposPax, "Debe ingresar los grupos del acomodo de vehículos.")
                blnOk = False
            End If
            If intPaxTotal > 0 And bytGruposPax > 0 Then
                Dim intPax As Int16 = Convert.ToInt16(txtNroPax.Text) + Convert.ToInt16(If(txtNroLiberados.Text = "", 0, txtNroLiberados.Text))

                If intPaxTotal <> intPax Then
                    Tab.SelectTab(tpgAcomVehi)
                    'ErrPrv.SetError(txtPaxTotal, "Pax Total del acomodo de vehículos ha excedido la cantidad de pax del servicio.")
                    ErrPrv.SetError(txtPaxTotal, "Pax Total del acomodo de vehículos debe ser igual a la cantidad de pax del servicio.")
                    blnOk = False
                End If
                If bytGruposPax > intPaxTotal Then
                    Tab.SelectTab(tpgAcomVehi)
                    ErrPrv.SetError(txtGruposPax, "La cantidad de grupos ha excedido la cantidad de pax ingresada.")
                    blnOk = False
                Else

                    'If intPaxTotal Mod bytGruposPax <> 0 Then
                    '    Tab.SelectTab(tpgAcomVehi)
                    '    ErrPrv.SetError(txtGruposPax, "Debe ingresar una cantidad de grupos acorde con la cantidad de pax (grupo par si pax es par y viceversa).")
                    '    blnOk = False
                    'End If
                End If

            End If
            If chkSelCopiaAcoVeh.Checked Then
                If intPaxTotal = 0 And bytGruposPax = 0 And lvwServAcomVehi.CheckedItems.Count > 0 Then
                    Tab.SelectTab(tpgAcomVehi)
                    ErrPrv.SetError(lvwServAcomVehi, "Debe ingresar acomodo de vehículos, ya que ha seleccionado servicio para copia.")
                    blnOk = False
                End If
            End If

        End If
        Return blnOk
    End Function
    Private Function blnValidarPaxAcomodoVehiculo(ByVal vbytPax As Byte, ByVal vbytCapacidad As Byte) As Boolean
        If vbytCapacidad = 0 Then Return True
        If vbytPax > vbytCapacidad Then
            MessageBox.Show("Debe ingresar pax menores o iguales a la capacidad.", gstrTituloMsg, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            Return False
        End If
        Return True
    End Function
    Private Sub CargarLvwAcomVehi()
        Try
            With lvwServAcomVehi
                .View = View.Details
                .FullRowSelect = True
                '.GridLines = True
                .LabelEdit = False
                .HideSelection = False

                .Columns.Clear()

                .Columns.Add("Fecha", 120, HorizontalAlignment.Left)
                .Columns.Add("Hora", 40, HorizontalAlignment.Left)
                .Columns.Add("Servicio", 220, HorizontalAlignment.Left)
                .Columns.Add("Pax", 30, HorizontalAlignment.Left)
                .Columns.Add("Ac.Ve.", 50, HorizontalAlignment.Left)
                .Columns.Add("IDDet", 0, HorizontalAlignment.Left)


                If grbAcomVehicE.Visible Then
                    Dim objBN As New clsAcomodoVehiculoExtBN
                    'Dim dtt As DataTable = objBN.ConsultarDetVtasAcomVehiCopiaPrExterno(intIDCab, cboCiudad.SelectedValue, intIDDet)
                    Dim dtt As DataTable = objBN.ConsultarDetVtasAcomVehiCopiaPrExterno(intIDCab, intIDDet)
                    pCargaListView(lvwServAcomVehi, dtt, dtt.Columns.Count - 1, 1)
                ElseIf grbAcomVehicI.Visible Then
                    Dim objBN As New clsAcomodoVehiculoBN
                    Dim dtt As DataTable = objBN.ConsultarDetVtasAcomVehiCopiaPrInterno(intIDCab, cboCiudad.SelectedValue, lblVariante.Text, intIDDet)
                    pCargaListView(lvwServAcomVehi, dtt, dtt.Columns.Count - 1, 1)
                End If

                AgregarServiciosCopiaAcoVehMemoria(cboCiudad.SelectedValue, lblVariante.Text, strIDDet)
            End With

        Catch ex As Exception
            Throw
        End Try
    End Sub
    Private Sub AgregarServiciosCopiaAcoVehMemoria(ByVal vstrCoUbigeo As String, ByVal vstrCoVariante As String, ByVal vstrIDDet As String)
        Try
            Dim dtt As New DataTable("ServAcoVehi")
            With dtt.Columns
                .Add("Fecha")
                .Add("Hora")
                .Add("Servicio")
                .Add("Pax")
                .Add("IDDet")
            End With
            'cd.idcab=@IDCab and cd.IDubigeo=@CoUbigeo And 
            '((@CoVariante<>'ASIA') or (sd.Tipo=@CoVariante And @CoVariante='ASIA'))
            'And p.IDTipoProv='003' and p.IDTipoOper='I'
            'And cd.IDDET<>@IDDetBase

            If grbAcomVehicE.Visible Then
                For Each dr As DataGridViewRow In frmRef.dgvDet.Rows
                    If Not IsNumeric(dr.Cells("IDDet").Value) And dr.Cells("IDCiudad").Value = vstrCoUbigeo _
                        And dr.Cells("IDTipoProv").Value <> gstrTipoProveeHoteles And dr.Cells("IDTipoOper").Value = "E" _
                        And dr.Cells("IDDet").Value.ToString <> vstrIDDet Then
                        dtt.Rows.Add(dr.Cells("DiaFormat").Value, dr.Cells("Hora").Value, dr.Cells("Servicio").Value, dr.Cells("PaxmasLiberados").Value, dr.Cells("IDDet").Value)
                    End If
                Next
            ElseIf grbAcomVehicI.Visible Then
                For Each dr As DataGridViewRow In frmRef.dgvDet.Rows
                    If Not IsNumeric(dr.Cells("IDDet").Value) And dr.Cells("IDCiudad").Value = vstrCoUbigeo _
                        And ((dr.Cells("IDDet").Value <> "ASIA") Or (dr.Cells("Tipo").Value = vstrCoVariante And vstrCoVariante = "ASIA")) _
                        And dr.Cells("IDTipoProv").Value = gstrTipoProveeOperadores And dr.Cells("IDTipoOper").Value = "I" _
                        And dr.Cells("IDDet").Value.ToString <> vstrIDDet Then
                        dtt.Rows.Add(dr.Cells("DiaFormat").Value, dr.Cells("Hora").Value, dr.Cells("Servicio").Value, dr.Cells("PaxmasLiberados").Value, dr.Cells("IDDet").Value)
                    End If
                Next
            End If

            pCargaListView(lvwServAcomVehi, dtt, dtt.Columns.Count - 1, 1, False)
        Catch ex As Exception
            Throw
        End Try
    End Sub

    Private Sub chkAcomVehi_CheckedChanged(sender As Object, e As EventArgs) Handles chkAcomVehi.CheckedChanged

        Try
            If chkAcomVehi.Checked Then

                'If blnLstDetCotiQuiebresTieneDatosxIDDet(strIDDet) Then
                If blnLstCotiQuiebresTieneDatos() Then
                    If MessageBox.Show("Se han ingresado datos en Quiebres para este file/cotización. ¿Está Ud. seguro de perderlos?", gstrTituloMsg, MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation) = Windows.Forms.DialogResult.Yes Then
                        btnQuiebres.Enabled = False
                        'BorrarSTDetCotiQuiebresxIDDet(strIDDet)
                        'frmRef.objLstQuie.Clear()
                        'frmRef.dgvQuie.Rows.Clear()
                        blnCambios = True
                        btnAceptar.Enabled = True

                        tpgAcomVehi.Parent = Tab
                        'CargarLvwAcomVehi()

                        If strIDTipoOper = "I" Then
                            Dim objAco As New clsAcomodoVehiculoBN
                            blnTieneSubServiciosGuia = objAco.blnTieneSubServiciosGuia(intIDServicio_Det)

                            CargarVehiculosAcomodo()
                            'blnCargaTvwAcomoVehiPaxInicial = True
                            CargarTreeViewsPaxAcoVehInt()
                            PintarChecksTvwAcoVehInt()
                            ' blnCargaTvwAcomoVehiPaxInicial = False
                        ElseIf strIDTipoOper = "E" Then
                            blnAcomoVehiReciente = True
                            CargarVehiculosAcomodoExt()

                            CargarTreeViewsPaxAcoVehExt()
                            PintarChecksTvwAcoVehExt()
                        End If

                        If blnLoad Then
                            Tab.SelectTab(tpgAcomVehi)
                        End If
                    Else
                        chkAcomVehi.Checked = False
                    End If

                Else
                    tpgAcomVehi.Parent = Tab
                    'CargarLvwAcomVehi()

                    If strIDTipoOper = "I" Then
                        Dim objAco As New clsAcomodoVehiculoBN
                        blnTieneSubServiciosGuia = objAco.blnTieneSubServiciosGuia(intIDServicio_Det)

                        CargarVehiculosAcomodo()
                        ' blnCargaTvwAcomoVehiPaxInicial = True
                        CargarTreeViewsPaxAcoVehInt()
                        PintarChecksTvwAcoVehInt()
                        'blnCargaTvwAcomoVehiPaxInicial = False
                    ElseIf strIDTipoOper = "E" Then
                        blnAcomoVehiReciente = True
                        CargarVehiculosAcomodoExt()

                        CargarTreeViewsPaxAcoVehExt()
                        PintarChecksTvwAcoVehExt()
                    End If
                    If blnLoad Then
                        Tab.SelectTab(tpgAcomVehi)
                    End If
                End If


            Else
                blnAcomoVehiReciente = False
                If strIDTipoOper = "I" Then
                    If blnAcomVehiTieneCambios() Then
                        If MessageBox.Show("Se han ingresado datos en Acomodo Vehículos para este servicio. ¿Está Ud. seguro de perderlos?", gstrTituloMsg, MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation) = Windows.Forms.DialogResult.Yes Then
                            EliminarAcomoVehicST()
                            tpgAcomVehi.Parent = Nothing
                            blnCambios = True
                            btnAceptar.Enabled = True
                        Else
                            chkAcomVehi.Checked = True
                        End If
                    Else
                        tpgAcomVehi.Parent = Nothing
                    End If

                ElseIf strIDTipoOper = "E" Then
                    If txtPaxTotal.Text <> "" Or txtGruposPax.Text <> "" Then
                        If MessageBox.Show("Se han ingresado datos en Acomodo Vehículos. ¿Está Ud. seguro de perderlos?", gstrTituloMsg, MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation) = Windows.Forms.DialogResult.Yes Then
                            txtPaxTotal.Text = ""
                            txtGruposPax.Text = ""
                            tpgAcomVehi.Parent = Nothing
                            blnCambios = True
                            btnAceptar.Enabled = True
                        Else
                            chkAcomVehi.Checked = True
                        End If
                    Else
                        tpgAcomVehi.Parent = Nothing
                    End If
                End If
                btnQuiebres.Enabled = True
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message, ex.Source, MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub
    'Private Function blnLstDetCotiQuiebresTieneDatosxIDDet(ByVal vstrIDDet As String) As Boolean

    '    For Each ST As stDetCotiQuiebres In objLstDetCotiQuiebres
    '        If ST.IDDet = vstrIDDet Then
    '            Return True
    '        End If
    '    Next
    '    Return False
    'End Function
    Private Function blnLstCotiQuiebresTieneDatos() As Boolean
        If Not blnLoad Then Return False
        If frmRef.objLstQuie.Count > 0 Then
            Return True
        End If

        Return False
    End Function
    Private Sub EliminarAcomoVehicST()
        Try
            For Each dr As DataGridViewRow In dgvAcoVeh.Rows
                dr.Cells("QtPax").Value = 0
                ActualizarAcomoVehicST(dr.Index)
            Next
        Catch ex As Exception
            Throw
        End Try
    End Sub
    Private Sub ActualizarAcomoVehicST(ByVal vintFila As Int16)

        Try
            Dim intNuVehiculo As Int16 = dgvAcoVeh.Rows(vintFila).Cells("NuVehiculo").Value
            Dim strNoVehiculo As String = dgvAcoVeh.Rows(vintFila).Cells("NoVehiculo").Value
            Dim bytQtVehiculos As Byte = 0
            If Not dgvAcoVeh.Rows(vintFila).Cells("QtVehiculos").Value Is Nothing Then
                bytQtVehiculos = If(dgvAcoVeh.Rows(vintFila).Cells("QtVehiculos").Value.ToString = "", 0, dgvAcoVeh.Rows(vintFila).Cells("QtVehiculos").Value)
            End If
            Dim bytQtPax As Byte = 0
            If Not dgvAcoVeh.Rows(vintFila).Cells("QtPax").Value Is Nothing Then
                bytQtPax = If(dgvAcoVeh.Rows(vintFila).Cells("QtPax").Value.ToString = "", 0, dgvAcoVeh.Rows(vintFila).Cells("QtPax").Value)
            End If
            Dim bytQtCapacidad As Byte = 0
            If Not dgvAcoVeh.Rows(vintFila).Cells("QtCapacidad").Value Is Nothing Then
                bytQtCapacidad = If(dgvAcoVeh.Rows(vintFila).Cells("QtCapacidad").Value.ToString = "", 0, dgvAcoVeh.Rows(vintFila).Cells("QtCapacidad").Value)
            End If

            Dim blnFlGuia As Boolean = dgvAcoVeh.Rows(vintFila).Cells("FlGuia").Value


            For Each ST As stAcomodoVehiculo In ListaAcomodoVehiculo
                If ST.NuVehiculo = intNuVehiculo Then
                    ListaAcomodoVehiculo.Remove(ST)
                    Exit For
                End If
            Next

            Dim NewItem As New stAcomodoVehiculo With {.NuVehiculo = intNuVehiculo, _
                                                       .NoVehiculo = strNoVehiculo, _
                                                       .QtCapacidad = bytQtCapacidad, _
                                                       .QtVehiculos = bytQtVehiculos, _
                                                       .QtPax = bytQtPax, _
                                                       .FlGuia = blnFlGuia}
            ListaAcomodoVehiculo.Add(NewItem)
        Catch ex As Exception
            Throw
        End Try
    End Sub
    Private Function blnAcomVehiTieneCambios() As Boolean
        Try
            For Each dr As DataGridViewRow In dgvAcoVeh.Rows

                Dim bytQtPax As Byte = 0
                If Not dr.Cells("QtPax").Value Is Nothing Then
                    bytQtPax = If(dr.Cells("QtPax").Value.ToString = "", 0, dr.Cells("QtPax").Value)
                End If

                If Not bytQtPax = 0 Then
                    Return True
                End If
            Next
            Return False
        Catch ex As Exception
            Throw
        End Try
    End Function

    Private Sub txtPaxTotal_TextChanged(sender As Object, e As EventArgs) Handles txtPaxTotal.TextChanged, txtGruposPax.TextChanged
        If Not blnAcomoVehiReciente Then
            blnCambios = True
            btnAceptar.Enabled = True

        End If

        Select Case sender.name
            Case "txtPaxTotal"
                ErrPrv.SetError(txtPaxTotal, "")

            Case "txtGruposPax"
                ErrPrv.SetError(txtGruposPax, "")
                blnAcomoVehiReciente = False
        End Select

    End Sub
    Private Sub EliminarServiciosCopiaAcoVeh()
        Try
InicioBorrar:
            For Each ItemCopiarAcVe As clsCotizacionBE.clsAcomodoVehiculoCopiaBE In frmRef.ListaIDDetCopiarAcomVehi
                If ItemCopiarAcVe.IDDet = strIDDet Then
                    frmRef.ListaIDDetCopiarAcomVehi.Remove(ItemCopiarAcVe)
                    GoTo InicioBorrar
                End If
            Next
        Catch ex As Exception
            Throw
        End Try
    End Sub

    '    Private Sub EliminarPaxAcoVehInt()
    '        Try
    'InicioBorrar:
    '            For Each ItemDetPaxAcVe As clsCotizacionBE.clsAcomodoVehiculoIntDetPaxBE In frmRef.ListaDetAcomVehiIntDetPax
    '                If ItemDetPaxAcVe.IDDet = strIDDet Then
    '                    frmRef.ListaDetAcomVehiIntDetPax.Remove(ItemDetPaxAcVe)
    '                    GoTo InicioBorrar
    '                End If
    '            Next
    '        Catch ex As Exception
    '            Throw
    '        End Try
    '    End Sub

    Private Sub chkSelCopiaAcoVeh_CheckedChanged(sender As Object, e As EventArgs) Handles chkSelCopiaAcoVeh.CheckedChanged

        Try
            If chkSelCopiaAcoVeh.Checked Then
                If lvwServAcomVehi.Items.Count = 0 Then
                    CargarLvwAcomVehi()
                End If

                lvwServAcomVehi.Visible = True
            Else
                If lvwServAcomVehi.CheckedItems.Count > 0 Then
                    If MessageBox.Show("Se han seleccionar servicio para Copia de Acomodo Vehículos. ¿Está Ud. seguro de perder dicha selección?", gstrTituloMsg, MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation) = Windows.Forms.DialogResult.Yes Then
                        lvwServAcomVehi.Visible = False
                        EliminarServiciosCopiaAcoVeh()
                        lvwServAcomVehi.Items.Clear()
                    Else
                        chkSelCopiaAcoVeh.Checked = True
                    End If
                Else
                    lvwServAcomVehi.Visible = False
                End If

            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message, ex.Source, MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub lvwServAcomVehi_ItemCheck(sender As Object, e As ItemCheckEventArgs) Handles lvwServAcomVehi.ItemCheck
        blnCambios = True
        btnAceptar.Enabled = True
        frmRef.blnActualizoCopiaAcomVehic = True
    End Sub

    'Private Sub dgvAcoVeh_CellContentClick(sender As Object, e As DataGridViewCellEventArgs) Handles dgvAcoVeh.CellContentClick

    '    Try
    '        If e.ColumnIndex = dgvAcoVeh.Columns("btnPax").Index Then
    '            lvwPaxAcoVeh.Visible = True
    '            CargarChecksLvwPaxAcoVeh()
    '        End If
    '    Catch ex As Exception
    '        MessageBox.Show(ex.Message, ex.Source, MessageBoxButtons.OK, MessageBoxIcon.Error)
    '    End Try
    'End Sub

    'Private Sub CargarChecksLvwPaxAcoVeh()
    '    Try
    '        For i As Int16 = 0 To lvwPaxAcoVeh.Items.Count - 1
    '            lvwPaxAcoVeh.Items(i).Checked = False
    '        Next


    '        Dim intNuVehiculo As Int16 = dgvAcoVeh.CurrentRow.Cells("NuVehiculo").Value
    '        Dim ListaChecks As New List(Of Int16)
    '        For Each ItemDetPaxAcVe As clsCotizacionBE.clsAcomodoVehiculoIntDetPaxBE In frmRef.ListaDetAcomVehiIntDetPax
    '            If ItemDetPaxAcVe.IDDet = strIDDet And ItemDetPaxAcVe.NuVehiculo = intNuVehiculo Then
    '                For i As Int16 = 0 To lvwPaxAcoVeh.Items.Count - 1
    '                    If lvwPaxAcoVeh.Items(i).SubItems(3).Text = ItemDetPaxAcVe.NuPax Then
    '                        'lvwPaxAcoVeh.Items(i).Checked = True
    '                        ListaChecks.Add(i)
    '                        'Exit For
    '                    End If
    '                Next

    '            End If
    '        Next


    '        For Each intInd As Int16 In ListaChecks

    '            lvwPaxAcoVeh.Items(intInd).Checked = True
    '        Next


    '    Catch ex As Exception
    '        Throw
    '    End Try
    'End Sub

    'Private Sub lvwPaxAcoVeh_ItemChecked(sender As Object, e As ItemCheckedEventArgs)
    '    Try
    '        ActualizarListaDetPaxAcomodoVehiculo(e.Item.Index)
    '    Catch ex As Exception
    '        MessageBox.Show(ex.Message, ex.Source, MessageBoxButtons.OK, MessageBoxIcon.Error)
    '    End Try
    'End Sub
    Private Sub CargarTreeViewsPaxAcoVehInt()
        Try
            tvwAcoVeh.Nodes.Clear()
            For Each dr As DataGridViewRow In dgvAcoVeh.Rows
                Dim bytPax As Byte = 0
                If Not dr.Cells("QtPax").Value Is Nothing Then
                    bytPax = Convert.ToByte(If(dr.Cells("QtPax").Value.ToString = "", 0, dr.Cells("QtPax").Value))
                End If
                Dim bytQtVehiculos As Byte = 0
                If Not dr.Cells("QtVehiculos").Value Is Nothing Then
                    bytQtVehiculos = Convert.ToByte(If(dr.Cells("QtVehiculos").Value.ToString = "", 0, dr.Cells("QtVehiculos").Value))
                End If
                If bytPax > 0 And bytQtVehiculos > 0 Then
                    With tvwAcoVeh
                        Dim VehiNode As TreeNode
                        VehiNode = .Nodes.Add(dr.Cells("NuVehiculo").Value, dr.Cells("NoVehiculo").Value, 1)


                        Dim NroVehiNode As TreeNode
                        NroVehiNode = New TreeNode()
                        Dim bytQtVehiCont As Byte = 1
                        Do While bytQtVehiCont <= bytQtVehiculos

                            NroVehiNode = VehiNode.Nodes.Add(bytQtVehiCont.ToString, dr.Cells("NoVehiculo").Value & " - Nro. " & bytQtVehiCont.ToString, 1)

                            Dim PaxNode As TreeNode
                            PaxNode = New TreeNode()

                            For i As Int16 = 0 To lvwPax.Items.Count - 1
                                If lvwPax.Items(i).Checked Then
                                    PaxNode = NroVehiNode.Nodes.Add(lvwPax.Items(i).SubItems(3).Text, lvwPax.Items(i).SubItems(0).Text & " " & lvwPax.Items(i).SubItems(1).Text, 0)
                                    PaxNode.SelectedImageIndex = 0


                                End If
                            Next

                            If blnTieneSubServiciosGuia Then
                                PaxNode = NroVehiNode.Nodes.Add("0", "Guía", 2)
                                PaxNode.SelectedImageIndex = 2
                            End If

                            bytQtVehiCont += 1
                        Loop





                        .ExpandAll()
                    End With
                End If


            Next
        Catch ex As Exception
            Throw
        End Try

    End Sub
    Private Sub PintarChecksTvwAcoVehInt()
        Try
            Dim blnExiste As Boolean = False
            For Each ItemAcoVeh As clsCotizacionBE.clsAcomodoVehiculoIntDetPaxBE In frmRef.ListaDetAcomVehiIntDetPax
                If ItemAcoVeh.IDDet = strIDDet Then
                    blnExiste = True
                    Exit For
                End If
            Next
            If Not blnExiste Then
                Using objBN As New clsAcomodoVehiculoDetPaxIntBN
                    Using dr As SqlClient.SqlDataReader = objBN.ConsultarxIDDet(intIDDet)
                        While dr.Read
                            For i As Int16 = 0 To tvwAcoVeh.Nodes.Count - 1
                                For t As Int16 = 0 To tvwAcoVeh.Nodes(i).Nodes.Count - 1
                                    For n As Int16 = 0 To tvwAcoVeh.Nodes(i).Nodes(t).Nodes.Count - 1
                                        If Not dr("FlGuia") Then
                                            If tvwAcoVeh.Nodes(i).Nodes(t).Nodes(n).Name = dr("NuPax") And _
                                                tvwAcoVeh.Nodes(i).Nodes(t).Name = dr("NuNroVehiculo") And _
                                                tvwAcoVeh.Nodes(i).Name = dr("NuVehiculo") Then
                                                tvwAcoVeh.Nodes(i).Nodes(t).Nodes(n).Checked = True
                                                Exit For
                                            End If
                                        Else
                                            If tvwAcoVeh.Nodes(i).Nodes(t).Nodes(n).Name = "0" And _
                                                tvwAcoVeh.Nodes(i).Nodes(t).Nodes(n).Text = "Guía" And _
                                                tvwAcoVeh.Nodes(i).Nodes(t).Name = dr("NuNroVehiculo") And _
                                                tvwAcoVeh.Nodes(i).Name = dr("NuVehiculo") Then
                                                tvwAcoVeh.Nodes(i).Nodes(t).Nodes(n).Checked = True
                                                Exit For
                                            End If

                                        End If
                                    Next
                                Next
                            Next

                        End While
                        dr.Close()
                    End Using
                End Using
            Else
                For Each ItemDetPaxAcVe As clsCotizacionBE.clsAcomodoVehiculoIntDetPaxBE In frmRef.ListaDetAcomVehiIntDetPax
                    If ItemDetPaxAcVe.IDDet = strIDDet And ItemDetPaxAcVe.Selecc Then
                        For i As Int16 = 0 To tvwAcoVeh.Nodes.Count - 1
                            For t As Int16 = 0 To tvwAcoVeh.Nodes(i).Nodes.Count - 1
                                For n As Int16 = 0 To tvwAcoVeh.Nodes(i).Nodes(t).Nodes.Count - 1
                                    If Not ItemDetPaxAcVe.FlGuia Then
                                        If tvwAcoVeh.Nodes(i).Nodes(t).Nodes(n).Name = ItemDetPaxAcVe.NuPax And _
                                            tvwAcoVeh.Nodes(i).Nodes(t).Name = ItemDetPaxAcVe.NuNroVehiculo And _
                                            tvwAcoVeh.Nodes(i).Name = ItemDetPaxAcVe.NuVehiculo Then
                                            tvwAcoVeh.Nodes(i).Nodes(t).Nodes(n).Checked = True
                                            Exit For
                                        End If
                                    Else
                                        If tvwAcoVeh.Nodes(i).Nodes(t).Nodes(n).Name = "0" And _
                                            tvwAcoVeh.Nodes(i).Nodes(t).Nodes(n).Text = "Guía" And _
                                            tvwAcoVeh.Nodes(i).Nodes(t).Name = ItemDetPaxAcVe.NuNroVehiculo And _
                                            tvwAcoVeh.Nodes(i).Name = ItemDetPaxAcVe.NuVehiculo Then
                                            tvwAcoVeh.Nodes(i).Nodes(t).Nodes(n).Checked = True
                                            Exit For
                                        End If

                                    End If
                                Next
                            Next
                        Next

                    End If
                Next

            End If

        Catch ex As Exception
            Throw
        End Try
    End Sub
    'Private Sub btnPax_Click(sender As Object, e As EventArgs)

    '    Try
    '        If Not tvwAcoVeh.Visible Then
    '            tvwAcoVeh.Visible = True
    '            CargarTreeViewsPaxAcoVehInt()
    '            PintarChecksTvwAcoVehInt()
    '        Else
    '            tvwAcoVeh.Visible = False
    '        End If
    '    Catch ex As Exception
    '        MessageBox.Show(ex.Message, ex.Source, MessageBoxButtons.OK, MessageBoxIcon.Error)
    '    End Try
    'End Sub

    Private Sub tvwAcoVeh_AfterCheck(sender As Object, e As TreeViewEventArgs) Handles tvwAcoVeh.AfterCheck
        Try

            If blnLoad Then
                blnCambios = True
                btnAceptar.Enabled = True
            End If

            ErrPrv.SetError(tvwAcoVeh, "")
            If Not e.Node.Parent Is Nothing Then
                ActualizarListaDetPaxAcomodoVehiculo(e.Node.Parent.Parent.Index, e.Node.Parent.Index, e.Node.Index)
                If e.Node.Text <> "Guía" Then
                    If e.Node.Checked Then
                        If Not blnValidarMarcado(e.Node.Parent.Parent.Name, e.Node.Parent.Parent.Text) Then
                            e.Node.Checked = False
                            Exit Sub
                        End If

                        EliminarNodoTvw(e.Node.Name, e.Node.Parent.Name, e.Node.Parent.Parent.Name)
                    Else
                        AgregarNodoTvw(e.Node.Parent.Parent.Name, e.Node.Parent.Name, e.Node)
                    End If
                End If
            End If

            'If e.Node.Nodes.Count > 0 Then
            '    CheckAllChildNodes(e.Node, e.Node.Checked)
            'End If
            'PintarChecksPadres(e.Node, e.Node.Checked)
        Catch ex As Exception
            MessageBox.Show(ex.Message, ex.Source, MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub


    Private Sub ActualizarListaDetPaxAcomodoVehiculo(ByVal vintIndVehi As Int16, ByVal vintIndNroVehi As Int16, ByVal vintIndPax As Int16)
        Try
            If strIDTipoOper = "I" Then
                Dim intNuVehiculo As Int16 = tvwAcoVeh.Nodes(vintIndVehi).Name
                Dim bytNroVehi As Byte = tvwAcoVeh.Nodes(vintIndVehi).Nodes(vintIndNroVehi).Name
                Dim intNuPax As Integer = tvwAcoVeh.Nodes(vintIndVehi).Nodes(vintIndNroVehi).Nodes(vintIndPax).Name
                Dim blnGuia As Boolean = False
                If tvwAcoVeh.Nodes(vintIndVehi).Nodes(vintIndNroVehi).Nodes(vintIndPax).Text = "Guía" Then
                    intNuPax = 0

                    If tvwAcoVeh.Nodes(vintIndVehi).Nodes(vintIndNroVehi).Nodes(vintIndPax).Checked Then
                        blnGuia = True
                    End If
                End If
                Dim blnChecked As Boolean = tvwAcoVeh.Nodes(vintIndVehi).Nodes(vintIndNroVehi).Nodes(vintIndPax).Checked

                For Each ItemDetPaxAcVe As clsCotizacionBE.clsAcomodoVehiculoIntDetPaxBE In ListaDetAcomVehiIntDetPax
                    If ItemDetPaxAcVe.IDDet = strIDDet And ItemDetPaxAcVe.NuVehiculo = intNuVehiculo _
                        And ItemDetPaxAcVe.NuNroVehiculo = bytNroVehi _
                        And ItemDetPaxAcVe.NuPax = intNuPax Then
                        ListaDetAcomVehiIntDetPax.Remove(ItemDetPaxAcVe)
                        Exit For
                    End If
                Next

                Dim NewItem As New clsCotizacionBE.clsAcomodoVehiculoIntDetPaxBE With {.IDDet = strIDDet, _
                                                                                        .NuVehiculo = intNuVehiculo, _
                                                                                       .NuNroVehiculo = bytNroVehi, _
                                                                                        .NuPax = intNuPax, _
                                                                                       .FlGuia = blnGuia, _
                                                                                       .Selecc = blnChecked}
                ListaDetAcomVehiIntDetPax.Add(NewItem)

            ElseIf strIDTipoOper = "E" Then

                Dim bytNuGrupo As Byte = tvwAcoVeh.Nodes(vintIndVehi).Name
                Dim intNuPax As Integer = tvwAcoVeh.Nodes(vintIndVehi).Nodes(vintIndPax).Name
                Dim blnChecked As Boolean = tvwAcoVeh.Nodes(vintIndVehi).Nodes(vintIndPax).Checked

                For Each ItemDetPaxAcVe As clsCotizacionBE.clsAcomodoVehiculoExtDetPaxBE In ListaDetAcomVehiExtDetPax
                    If ItemDetPaxAcVe.IDDet = strIDDet And ItemDetPaxAcVe.NuGrupo = bytNuGrupo And ItemDetPaxAcVe.NuPax = intNuPax Then
                        ListaDetAcomVehiExtDetPax.Remove(ItemDetPaxAcVe)
                        Exit For
                    End If
                Next

                Dim NewItem As New clsCotizacionBE.clsAcomodoVehiculoExtDetPaxBE With {.IDDet = strIDDet, _
                                                                                        .NuGrupo = bytNuGrupo, _
                                                                                        .NuPax = intNuPax, _
                                                                                       .Selecc = blnChecked}
                ListaDetAcomVehiExtDetPax.Add(NewItem)


            End If


        Catch ex As Exception
            Throw
        End Try
    End Sub

    Private Sub EliminarNodoTvw(ByVal vstrIDNode As String, ByVal vstrIDNodePadre As String, ByVal vstrIDNodeAbuelo As String)
        'If blnCargaTvwAcomoVehiPaxInicial Then Exit Sub
        Try
Limpiar1:
            For Each Nodo As TreeNode In tvwAcoVeh.Nodes
                For Each Nodo2 As TreeNode In Nodo.Nodes
                    For Each Nodo3 As TreeNode In Nodo2.Nodes

                        If Nodo3.Name = vstrIDNode And (Nodo.Name <> vstrIDNodeAbuelo _
                                                        Or (Nodo.Name = vstrIDNodeAbuelo And Nodo2.Name <> vstrIDNodePadre)) Then
                            ActualizarListaDetPaxAcomodoVehiculo(Nodo.Index, Nodo2.Index, Nodo3.Index)
                            Nodo3.Remove()
                            GoTo Limpiar1
                        End If
                    Next
                    'If Nodo2.Name = vstrIDNode And Nodo.Name <> vstrIDNodePadre Then
                    '    ActualizarListaDetPaxAcomodoVehiculo(Nodo.Index, Nodo2.Index)
                    '    Nodo2.Remove()
                    '    GoTo Limpiar1
                    'End If

                Next
                'Limpiar1:
            Next

        Catch ex As Exception
            Throw
        End Try
    End Sub

    Private Sub AgregarNodoTvw(ByVal vstrIDNodeParentParent As String, ByVal vstrIDNodeParent As String, ByVal vndoNode As TreeNode)
        Try
            For Each Nodo As TreeNode In tvwAcoVeh.Nodes
                For Each Nodo2 As TreeNode In Nodo.Nodes
                    For Each Nodo3 As TreeNode In Nodo2.Nodes
                        If Nodo.Name <> vstrIDNodeParent And Nodo2.Name <> vstrIDNodeParentParent Then
                            If Not blnExisteEnTvw(Nodo2, vndoNode.Name) Then
                                Nodo2.Nodes.Add(vndoNode.Name, vndoNode.Text, 0).SelectedImageIndex = 0
                                Exit For
                            End If
                        End If
                    Next
                Next
            Next
            'tvwAcoVeh.Sort()
        Catch ex As Exception
            Throw
        End Try
    End Sub

    Private Function blnExisteEnTvw(ByVal vTNode As TreeNode, ByVal vstrID As String) As Boolean
        Try
            Dim blnOk As Boolean = False
            For Each Nodo As TreeNode In vTNode.Nodes
                If Nodo.Name = vstrID Then
                    blnOk = True
                    GoTo FinBuscar
                End If
            Next
FinBuscar:
            Return blnOk
        Catch ex As Exception
            Throw
        End Try
    End Function
    Private Function blnValidarPaxAcomodoVehi() As Boolean
        If Not chkAcomVehi.Checked Then
            Return True
        End If

        Try
            If strIDTipoOper = "I" Then
                For Each dr As DataGridViewRow In dgvAcoVeh.Rows
                    If Not blnValidarMarcado(dr.Cells("NuVehiculo").Value, dr.Cells("NoVehiculo").Value, True) Then
                        Return False
                    End If
                Next

                If Not blnValidarGuiaIngresosAcomodosEspecialxListaPaxSeleccion() Then
                    Tab.SelectTab(tpgAcomVehi)
                    ErrPrv.SetError(tvwAcoVeh, "Debe marcar al menos un guía.")
                    Return False
                End If
            End If


            Return True

        Catch ex As Exception
            Throw
        End Try
    End Function
    Private Function blnValidarMarcado(ByVal vintNuVehiculo As Int16, ByVal vstrNoVehiculo As String, Optional ByVal vblnTodos As Boolean = False) As Boolean
        Try
            Dim blnOk As Boolean = True
            Dim bytPax As Byte = 0
            ErrPrv.SetError(tvwAcoVeh, "")

            If strIDTipoOper = "I" Then
                For Each dr As DataGridViewRow In dgvAcoVeh.Rows
                    If dr.Cells("NuVehiculo").Value = vintNuVehiculo Then
                        Dim bytQtVehiculos As Byte = 0
                        If Not dr.Cells("QtVehiculos").Value Is Nothing Then
                            bytQtVehiculos = Convert.ToByte(If(dr.Cells("QtVehiculos").Value.ToString = "", 0, dr.Cells("QtVehiculos").Value))
                        End If
                        If Not dr.Cells("QtPax").Value Is Nothing Then
                            'bytPax = Convert.ToByte(If(dr.Cells("QtPax").Value.ToString = "", 0, dr.Cells("QtPax").Value)) * bytQtVehiculos
                            If Not vblnTodos Then
                                bytPax = Convert.ToByte(If(dr.Cells("QtPax").Value.ToString = "", 0, dr.Cells("QtPax").Value))
                            Else
                                bytPax = Convert.ToByte(If(dr.Cells("QtPax").Value.ToString = "", 0, dr.Cells("QtPax").Value)) * bytQtVehiculos
                            End If

                        End If
                        Exit For
                    End If
                Next
            ElseIf strIDTipoOper = "E" Then
                bytPax = Convert.ToByte(If(txtPaxTotal.Text = "", 0, txtPaxTotal.Text)) _
                        / Convert.ToByte(If(txtGruposPax.Text = "", 1, txtGruposPax.Text))
            End If

            Dim cntCheck As Byte = 0
            If Not vblnTodos Then
                For Each Nodo As TreeNode In tvwAcoVeh.Nodes
                    If Nodo.Name = vintNuVehiculo Then
                        For Each Nodo2 As TreeNode In Nodo.Nodes
                            cntCheck = 0
                            For Each Nodo3 As TreeNode In Nodo2.Nodes
                                If Nodo3.Checked And Nodo3.Text <> "Guía" Then cntCheck += 1
                            Next
                            If cntCheck > bytPax Then
                                blnOk = False
                                Exit For
                            End If
                        Next
                    End If
                Next
            Else
                For Each Nodo As TreeNode In tvwAcoVeh.Nodes
                    If Nodo.Name = vintNuVehiculo Then
                        cntCheck = 0
                        For Each Nodo2 As TreeNode In Nodo.Nodes
                            For Each Nodo3 As TreeNode In Nodo2.Nodes
                                If Nodo3.Checked And Nodo3.Text <> "Guía" Then cntCheck += 1
                            Next
                        Next
                        If cntCheck < bytPax Then
                            blnOk = False
                            Exit For
                        End If

                    End If
                Next

            End If


            If blnOk = False Then
                If Not vblnTodos Then
                    If strIDTipoOper = "I" Then
                        ErrPrv.SetError(tvwAcoVeh, "El vehículo " & vstrNoVehiculo & " no puede tener más de " & bytPax.ToString & " pax, según lo ingresado.")
                    ElseIf strIDTipoOper = "E" Then
                        ErrPrv.SetError(tvwAcoVeh, "El " & vstrNoVehiculo & " no puede tener más de " & bytPax.ToString & " pax, según lo ingresado.")
                    End If

                Else
                    If strIDTipoOper = "I" Then
                        ErrPrv.SetError(tvwAcoVeh, "La suma de los pax de los vehículos " & vstrNoVehiculo & " debe ser de " & bytPax.ToString & " según lo ingresado.")
                    ElseIf strIDTipoOper = "E" Then
                        ErrPrv.SetError(tvwAcoVeh, "El " & vstrNoVehiculo & " no puede tener más de " & bytPax.ToString & " pax, según lo ingresado.")
                    End If
                End If
            End If
            Return blnOk
        Catch ex As Exception
            Throw
        End Try
    End Function

    Private Sub CargarTreeViewsPaxAcoVehExt()
        Try
            tvwAcoVeh.Nodes.Clear()
            Dim intPaxTotal As Int16 = 0

            If txtPaxTotal.Text <> "" Then
                intPaxTotal = txtPaxTotal.Text
            End If
            Dim bytGruposPax As Byte = 0
            If txtGruposPax.Text <> "" Then
                bytGruposPax = txtGruposPax.Text
            End If
            If intPaxTotal > 0 And bytGruposPax > 0 Then
                For bytGrupo As Byte = 1 To bytGruposPax
                    With tvwAcoVeh
                        Dim ParentNode As TreeNode
                        ParentNode = .Nodes.Add(bytGrupo, "Grupo " & bytGrupo.ToString)

                        Dim childNode As TreeNode
                        childNode = New TreeNode()

                        For i As Int16 = 0 To lvwPax.Items.Count - 1
                            If lvwPax.Items(i).Checked Then
                                childNode = ParentNode.Nodes.Add(lvwPax.Items(i).SubItems(3).Text, lvwPax.Items(i).SubItems(0).Text & " " & lvwPax.Items(i).SubItems(1).Text, 0)
                                childNode.SelectedImageIndex = 0
                            End If
                        Next

                        .ExpandAll()
                    End With
                Next
            End If

        Catch ex As Exception
            Throw
        End Try

    End Sub

    Private Sub PintarChecksTvwAcoVehExt()
        Try
            Dim blnExiste As Boolean = False
            For Each ItemAcoVeh As clsCotizacionBE.clsAcomodoVehiculoExtDetPaxBE In frmRef.ListaDetAcomVehiExtDetPax

                If ItemAcoVeh.IDDet = strIDDet Then
                    blnExiste = True
                    Exit For
                End If
            Next
            If Not blnExiste Then
                Using objBN As New clsAcomodoVehiculoDetPaxExtBN
                    Using dr As SqlClient.SqlDataReader = objBN.ConsultarxIDDet(intIDDet)
                        While dr.Read
                            For i As Int16 = 0 To tvwAcoVeh.Nodes.Count - 1
                                For t As Int16 = 0 To tvwAcoVeh.Nodes(i).Nodes.Count - 1
                                    If tvwAcoVeh.Nodes(i).Nodes(t).Name = dr("NuPax") And _
                                        tvwAcoVeh.Nodes(i).Name = dr("NuGrupo") Then
                                        tvwAcoVeh.Nodes(i).Nodes(t).Checked = True
                                        Exit For
                                    End If
                                Next
                            Next
                        End While
                        dr.Close()
                    End Using
                End Using
            Else
                For Each ItemDetPaxAcVe As clsCotizacionBE.clsAcomodoVehiculoExtDetPaxBE In frmRef.ListaDetAcomVehiExtDetPax
                    If ItemDetPaxAcVe.IDDet = strIDDet And ItemDetPaxAcVe.Selecc Then
                        For i As Int16 = 0 To tvwAcoVeh.Nodes.Count - 1
                            For t As Int16 = 0 To tvwAcoVeh.Nodes(i).Nodes.Count - 1
                                If tvwAcoVeh.Nodes(i).Nodes(t).Name = ItemDetPaxAcVe.NuPax And _
                                    tvwAcoVeh.Nodes(i).Name = ItemDetPaxAcVe.NuGrupo Then
                                    'tvwAcoVeh.Nodes(i).Checked = True
                                    tvwAcoVeh.Nodes(i).Nodes(t).Checked = True
                                    Exit For
                                End If
                            Next
                        Next

                    End If
                Next

            End If

        Catch ex As Exception
            Throw
        End Try
    End Sub

    Private Sub txtGruposPax_Validated(sender As Object, e As EventArgs) Handles txtGruposPax.Validated, txtPaxTotal.Validated
        Try
            If blnValidarAcomodoVehiculo() Then
                CargarTreeViewsPaxAcoVehExt()
                PintarChecksTvwAcoVehExt()
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message, ex.Source, MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub chkServNoCobrado_CheckedChanged(sender As Object, e As EventArgs) Handles chkServNoCobrado.CheckedChanged
        If Not blnLoad Then Exit Sub
        If chkServNoCobrado.Checked Then
            'bytCantidadaPagarAntNoCobrado = txtCantidadaPagar.Text
            'dblTotalGenAntNoCobrado = lblTotalR.Text

            'If Convert.ToByte(frmRefRes.DgvDetalle.CurrentRow.Cells("QtCantidadAPagarAntNoCobrado").Value) = 0 Then
            '    bytCantidadaPagarAntNoCobrado = frmRefRes.DgvDetalle.CurrentRow.Cells("CantidadaPagar").Value
            'Else
            '    bytCantidadaPagarAntNoCobrado = frmRefRes.DgvDetalle.CurrentRow.Cells("QtCantidadAPagarAntNoCobrado").Value
            'End If

            'If Convert.ToDouble(frmRefRes.DgvDetalle.CurrentRow.Cells("SsTotalGenAntNoCobrado").Value) = 0 Then
            '    dblTotalGenAntNoCobrado = frmRefRes.DgvDetalle.CurrentRow.Cells("TotalGen").Value
            'Else
            '    dblTotalGenAntNoCobrado = frmRefRes.DgvDetalle.CurrentRow.Cells("SsTotalGenAntNoCobrado").Value
            'End If

            txtCantidadaPagar.Text = "0"

        Else
            txtCantidadaPagar.Text = bytCantidadaPagarAntNoCobrado
            lblTotalR.Text = dblTotalGenAntNoCobrado.ToString("##,##0.00")
        End If
        ActualizarCtrlsMontosCostos(txtCantidadaPagar, Nothing)
    End Sub

    Private Sub chkEspecial_Validating(sender As Object, e As System.ComponentModel.CancelEventArgs) Handles chkEspecial.Validating
    End Sub
End Class