﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmIngPedidoDato
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim DataGridViewCellStyle11 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle12 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmIngPedidoDato))
        Dim DataGridViewCellStyle13 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle14 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle15 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle16 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle17 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle18 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle9 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle10 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.tabPedido = New System.Windows.Forms.TabControl()
        Me.tbDatGrales = New System.Windows.Forms.TabPage()
        Me.SplitContainer1 = New System.Windows.Forms.SplitContainer()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.cboEstado = New System.Windows.Forms.ComboBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.dtpFePedido = New System.Windows.Forms.DateTimePicker()
        Me.txtTxTitulo = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.lblNuPedido = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.btnCliente = New System.Windows.Forms.Button()
        Me.lblCliente = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.chkActivos = New System.Windows.Forms.CheckBox()
        Me.cboCoEjeVta = New System.Windows.Forms.ComboBox()
        Me.btnNuevo = New System.Windows.Forms.Button()
        Me.dgv = New System.Windows.Forms.DataGridView()
        Me.EjecutivoVtas = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Fecha = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.FecInicio = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Pax = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Cotizacion = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.NuFile = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.IDCab = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Titulo = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Ruta = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.CoEstado = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DescEstado = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.btnEdit = New System.Windows.Forms.DataGridViewButtonColumn()
        Me.tbCliente = New System.Windows.Forms.TabPage()
        Me.grbPerfil = New System.Windows.Forms.GroupBox()
        Me.pcbstar3 = New System.Windows.Forms.PictureBox()
        Me.pcbstar2 = New System.Windows.Forms.PictureBox()
        Me.pcbstar1 = New System.Windows.Forms.PictureBox()
        Me.lblFechaPerfil = New System.Windows.Forms.Label()
        Me.lblUsuarioPerfil = New System.Windows.Forms.Label()
        Me.gbGuias = New System.Windows.Forms.GroupBox()
        Me.Label58 = New System.Windows.Forms.Label()
        Me.Label60 = New System.Windows.Forms.Label()
        Me.dgvCiudades = New System.Windows.Forms.DataGridView()
        Me.IDubigeo2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Pais = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Ciudad = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.CoCliente = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.IDPais2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.dgvGuias = New System.Windows.Forms.DataGridView()
        Me.CoProveedor = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.NombreCorto = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.IdCiudad2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.txtOperatividad = New System.Windows.Forms.TextBox()
        Me.Label61 = New System.Windows.Forms.Label()
        Me.cboUsuarioRealizado = New System.Windows.Forms.ComboBox()
        Me.Label62 = New System.Windows.Forms.Label()
        Me.dtpFechaUpdate = New System.Windows.Forms.DateTimePicker()
        Me.Label63 = New System.Windows.Forms.Label()
        Me.txtInformacionGeneral = New System.Windows.Forms.TextBox()
        Me.Label64 = New System.Windows.Forms.Label()
        Me.lblPerfilTit = New System.Windows.Forms.Label()
        Me.tbCorreos = New System.Windows.Forms.TabPage()
        Me.grbBandCorreos = New System.Windows.Forms.GroupBox()
        Me.SplitContainer6 = New System.Windows.Forms.SplitContainer()
        Me.tvw = New System.Windows.Forms.TreeView()
        Me.imglstBandCorreos = New System.Windows.Forms.ImageList(Me.components)
        Me.grbTexto = New System.Windows.Forms.GroupBox()
        Me.rtbMensaje = New System.Windows.Forms.RichTextBox()
        Me.grbWeb = New System.Windows.Forms.GroupBox()
        Me.webHtml = New System.Windows.Forms.WebBrowser()
        Me.dgvCorreos = New System.Windows.Forms.DataGridView()
        Me.IconoCorreo = New System.Windows.Forms.DataGridViewImageColumn()
        Me.ImgAdjuntos = New System.Windows.Forms.DataGridViewImageColumn()
        Me.NuCorreo = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.CantAdjuntos = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.EntryID = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.De = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Para = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Asunto = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Body = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.BodyHtml = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ConCopia = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.EstadoLeido = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Importance = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.BodyFormat = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.RutasAdjuntos = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.FechaCorreo = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.lblTipoCorreo = New System.Windows.Forms.Label()
        Me.ToolStrip = New System.Windows.Forms.ToolStrip()
        Me.TssBtnNuevo = New System.Windows.Forms.ToolStripSplitButton()
        Me.ToolSSMIMensaje = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator2 = New System.Windows.Forms.ToolStripSeparator()
        Me.tsnBtnImprimir = New System.Windows.Forms.ToolStripButton()
        Me.TsBtnResponder = New System.Windows.Forms.ToolStripButton()
        Me.TsBtnResponderTodos = New System.Windows.Forms.ToolStripButton()
        Me.TsBtnReenviar = New System.Windows.Forms.ToolStripButton()
        Me.ToolStripSeparator3 = New System.Windows.Forms.ToolStripSeparator()
        Me.btnActualizarBand = New System.Windows.Forms.ToolStripButton()
        Me.ToolStripSeparator1 = New System.Windows.Forms.ToolStripSeparator()
        Me.ToolStripLabel1 = New System.Windows.Forms.ToolStripLabel()
        Me.txtBusqAsunto = New System.Windows.Forms.ToolStripTextBox()
        Me.Label73 = New System.Windows.Forms.Label()
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.cmsCopiarCotiz = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.mnuCopiar = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuPegar = New System.Windows.Forms.ToolStripMenuItem()
        Me.ErrPrv = New System.Windows.Forms.ErrorProvider(Me.components)
        Me.tbArchivos = New System.Windows.Forms.TabPage()
        Me.btnNuevoArchivo = New System.Windows.Forms.Button()
        Me.dgvArchivos = New System.Windows.Forms.DataGridView()
        Me.IDArchivo = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.IdCab_Archivo = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Nombre = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.RutaOrigen = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.RutaDestino = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.CoTipo = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Tipo_Archivo = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.FlCopiado = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.FechaModificacion = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.CoUsuarioCreacion = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Usuario = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.btnEditA = New System.Windows.Forms.DataGridViewButtonColumn()
        Me.btnDelA = New System.Windows.Forms.DataGridViewButtonColumn()
        Me.tabPedido.SuspendLayout()
        Me.tbDatGrales.SuspendLayout()
        CType(Me.SplitContainer1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SplitContainer1.Panel1.SuspendLayout()
        Me.SplitContainer1.Panel2.SuspendLayout()
        Me.SplitContainer1.SuspendLayout()
        CType(Me.dgv, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tbCliente.SuspendLayout()
        Me.grbPerfil.SuspendLayout()
        CType(Me.pcbstar3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pcbstar2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pcbstar1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gbGuias.SuspendLayout()
        CType(Me.dgvCiudades, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvGuias, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tbCorreos.SuspendLayout()
        Me.grbBandCorreos.SuspendLayout()
        CType(Me.SplitContainer6, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SplitContainer6.Panel1.SuspendLayout()
        Me.SplitContainer6.Panel2.SuspendLayout()
        Me.SplitContainer6.SuspendLayout()
        Me.grbTexto.SuspendLayout()
        Me.grbWeb.SuspendLayout()
        CType(Me.dgvCorreos, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.ToolStrip.SuspendLayout()
        Me.cmsCopiarCotiz.SuspendLayout()
        CType(Me.ErrPrv, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tbArchivos.SuspendLayout()
        CType(Me.dgvArchivos, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'tabPedido
        '
        Me.tabPedido.Controls.Add(Me.tbDatGrales)
        Me.tabPedido.Controls.Add(Me.tbCliente)
        Me.tabPedido.Controls.Add(Me.tbCorreos)
        Me.tabPedido.Controls.Add(Me.tbArchivos)
        Me.tabPedido.Dock = System.Windows.Forms.DockStyle.Fill
        Me.tabPedido.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tabPedido.Location = New System.Drawing.Point(0, 0)
        Me.tabPedido.Name = "tabPedido"
        Me.tabPedido.SelectedIndex = 0
        Me.tabPedido.Size = New System.Drawing.Size(1014, 480)
        Me.tabPedido.TabIndex = 0
        '
        'tbDatGrales
        '
        Me.tbDatGrales.Controls.Add(Me.SplitContainer1)
        Me.tbDatGrales.Location = New System.Drawing.Point(4, 22)
        Me.tbDatGrales.Name = "tbDatGrales"
        Me.tbDatGrales.Padding = New System.Windows.Forms.Padding(3)
        Me.tbDatGrales.Size = New System.Drawing.Size(1006, 454)
        Me.tbDatGrales.TabIndex = 0
        Me.tbDatGrales.Text = "Datos Generales"
        Me.tbDatGrales.UseVisualStyleBackColor = True
        '
        'SplitContainer1
        '
        Me.SplitContainer1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.SplitContainer1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SplitContainer1.Location = New System.Drawing.Point(3, 3)
        Me.SplitContainer1.Name = "SplitContainer1"
        Me.SplitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal
        '
        'SplitContainer1.Panel1
        '
        Me.SplitContainer1.Panel1.Controls.Add(Me.Label4)
        Me.SplitContainer1.Panel1.Controls.Add(Me.cboEstado)
        Me.SplitContainer1.Panel1.Controls.Add(Me.Label3)
        Me.SplitContainer1.Panel1.Controls.Add(Me.dtpFePedido)
        Me.SplitContainer1.Panel1.Controls.Add(Me.txtTxTitulo)
        Me.SplitContainer1.Panel1.Controls.Add(Me.Label1)
        Me.SplitContainer1.Panel1.Controls.Add(Me.lblNuPedido)
        Me.SplitContainer1.Panel1.Controls.Add(Me.Label2)
        Me.SplitContainer1.Panel1.Controls.Add(Me.btnCliente)
        Me.SplitContainer1.Panel1.Controls.Add(Me.lblCliente)
        Me.SplitContainer1.Panel1.Controls.Add(Me.Label7)
        Me.SplitContainer1.Panel1.Controls.Add(Me.chkActivos)
        Me.SplitContainer1.Panel1.Controls.Add(Me.cboCoEjeVta)
        '
        'SplitContainer1.Panel2
        '
        Me.SplitContainer1.Panel2.Controls.Add(Me.btnNuevo)
        Me.SplitContainer1.Panel2.Controls.Add(Me.dgv)
        Me.SplitContainer1.Size = New System.Drawing.Size(1000, 448)
        Me.SplitContainer1.SplitterDistance = 108
        Me.SplitContainer1.TabIndex = 0
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.Label4.Location = New System.Drawing.Point(279, 74)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(48, 13)
        Me.Label4.TabIndex = 317
        Me.Label4.Text = "Estado:"
        '
        'cboEstado
        '
        Me.cboEstado.BackColor = System.Drawing.Color.Moccasin
        Me.cboEstado.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboEstado.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboEstado.FormattingEnabled = True
        Me.cboEstado.Location = New System.Drawing.Point(343, 68)
        Me.cboEstado.Name = "cboEstado"
        Me.cboEstado.Size = New System.Drawing.Size(128, 21)
        Me.cboEstado.TabIndex = 316
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.Label3.Location = New System.Drawing.Point(9, 74)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(43, 13)
        Me.Label3.TabIndex = 315
        Me.Label3.Text = "Fecha:"
        '
        'dtpFePedido
        '
        Me.dtpFePedido.CalendarFont = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpFePedido.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpFePedido.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpFePedido.Location = New System.Drawing.Point(88, 68)
        Me.dtpFePedido.Name = "dtpFePedido"
        Me.dtpFePedido.Size = New System.Drawing.Size(77, 21)
        Me.dtpFePedido.TabIndex = 314
        '
        'txtTxTitulo
        '
        Me.txtTxTitulo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtTxTitulo.Location = New System.Drawing.Point(343, 39)
        Me.txtTxTitulo.MaxLength = 100
        Me.txtTxTitulo.Name = "txtTxTitulo"
        Me.txtTxTitulo.Size = New System.Drawing.Size(227, 21)
        Me.txtTxTitulo.TabIndex = 313
        Me.txtTxTitulo.Tag = "Borrar"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.Label1.Location = New System.Drawing.Point(279, 42)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(42, 13)
        Me.Label1.TabIndex = 191
        Me.Label1.Text = "Título:"
        '
        'lblNuPedido
        '
        Me.lblNuPedido.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.lblNuPedido.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblNuPedido.Location = New System.Drawing.Point(88, 41)
        Me.lblNuPedido.Name = "lblNuPedido"
        Me.lblNuPedido.Size = New System.Drawing.Size(128, 18)
        Me.lblNuPedido.TabIndex = 190
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.Label2.Location = New System.Drawing.Point(9, 42)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(73, 13)
        Me.Label2.TabIndex = 189
        Me.Label2.Text = "Nro. Pedido:"
        '
        'btnCliente
        '
        Me.btnCliente.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCliente.Location = New System.Drawing.Point(587, 9)
        Me.btnCliente.Name = "btnCliente"
        Me.btnCliente.Size = New System.Drawing.Size(27, 20)
        Me.btnCliente.TabIndex = 186
        Me.btnCliente.Text = "..."
        Me.btnCliente.UseVisualStyleBackColor = True
        '
        'lblCliente
        '
        Me.lblCliente.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.lblCliente.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCliente.Location = New System.Drawing.Point(343, 9)
        Me.lblCliente.Name = "lblCliente"
        Me.lblCliente.Size = New System.Drawing.Size(238, 18)
        Me.lblCliente.TabIndex = 188
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.Label7.Location = New System.Drawing.Point(279, 11)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(49, 13)
        Me.Label7.TabIndex = 187
        Me.Label7.Text = "Cliente:"
        '
        'chkActivos
        '
        Me.chkActivos.AutoSize = True
        Me.chkActivos.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkActivos.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.chkActivos.Location = New System.Drawing.Point(9, 10)
        Me.chkActivos.Name = "chkActivos"
        Me.chkActivos.Size = New System.Drawing.Size(120, 17)
        Me.chkActivos.TabIndex = 185
        Me.chkActivos.Text = "Ejecutivo Ventas"
        Me.chkActivos.UseVisualStyleBackColor = True
        '
        'cboCoEjeVta
        '
        Me.cboCoEjeVta.BackColor = System.Drawing.Color.Moccasin
        Me.cboCoEjeVta.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboCoEjeVta.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboCoEjeVta.FormattingEnabled = True
        Me.cboCoEjeVta.Location = New System.Drawing.Point(135, 9)
        Me.cboCoEjeVta.Name = "cboCoEjeVta"
        Me.cboCoEjeVta.Size = New System.Drawing.Size(128, 21)
        Me.cboCoEjeVta.TabIndex = 184
        '
        'btnNuevo
        '
        Me.btnNuevo.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnNuevo.Image = Global.SETours.My.Resources.Resources.page_add
        Me.btnNuevo.Location = New System.Drawing.Point(966, 0)
        Me.btnNuevo.Name = "btnNuevo"
        Me.btnNuevo.Size = New System.Drawing.Size(27, 27)
        Me.btnNuevo.TabIndex = 326
        Me.ToolTip1.SetToolTip(Me.btnNuevo, "Nueva Cotización")
        Me.btnNuevo.UseVisualStyleBackColor = True
        '
        'dgv
        '
        Me.dgv.AllowUserToAddRows = False
        Me.dgv.AllowUserToDeleteRows = False
        Me.dgv.AllowUserToOrderColumns = True
        Me.dgv.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        DataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle11.BackColor = System.Drawing.SystemColors.ControlDarkDark
        DataGridViewCellStyle11.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle11.ForeColor = System.Drawing.Color.White
        DataGridViewCellStyle11.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle11.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle11.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgv.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle11
        Me.dgv.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgv.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.EjecutivoVtas, Me.Fecha, Me.FecInicio, Me.Pax, Me.Cotizacion, Me.NuFile, Me.IDCab, Me.Titulo, Me.Ruta, Me.CoEstado, Me.DescEstado, Me.btnEdit})
        Me.dgv.Location = New System.Drawing.Point(0, 0)
        Me.dgv.Name = "dgv"
        Me.dgv.ReadOnly = True
        DataGridViewCellStyle12.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle12.BackColor = System.Drawing.SystemColors.ControlDarkDark
        DataGridViewCellStyle12.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle12.ForeColor = System.Drawing.Color.White
        DataGridViewCellStyle12.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle12.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle12.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgv.RowHeadersDefaultCellStyle = DataGridViewCellStyle12
        Me.dgv.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgv.Size = New System.Drawing.Size(964, 332)
        Me.dgv.TabIndex = 325
        '
        'EjecutivoVtas
        '
        Me.EjecutivoVtas.DataPropertyName = "EjecutivoVtas"
        Me.EjecutivoVtas.HeaderText = "Ejecutivo de Ventas"
        Me.EjecutivoVtas.Name = "EjecutivoVtas"
        Me.EjecutivoVtas.ReadOnly = True
        '
        'Fecha
        '
        Me.Fecha.DataPropertyName = "Fecha"
        Me.Fecha.HeaderText = "Fecha"
        Me.Fecha.Name = "Fecha"
        Me.Fecha.ReadOnly = True
        '
        'FecInicio
        '
        Me.FecInicio.DataPropertyName = "FecInicio"
        Me.FecInicio.HeaderText = "Fec. Inicio"
        Me.FecInicio.Name = "FecInicio"
        Me.FecInicio.ReadOnly = True
        '
        'Pax
        '
        Me.Pax.DataPropertyName = "Pax"
        Me.Pax.HeaderText = "Pax"
        Me.Pax.Name = "Pax"
        Me.Pax.ReadOnly = True
        '
        'Cotizacion
        '
        Me.Cotizacion.DataPropertyName = "Cotizacion"
        Me.Cotizacion.HeaderText = "Cotización"
        Me.Cotizacion.Name = "Cotizacion"
        Me.Cotizacion.ReadOnly = True
        '
        'NuFile
        '
        Me.NuFile.DataPropertyName = "NuFile"
        Me.NuFile.HeaderText = "Nro. File"
        Me.NuFile.Name = "NuFile"
        Me.NuFile.ReadOnly = True
        '
        'IDCab
        '
        Me.IDCab.DataPropertyName = "IDCab"
        Me.IDCab.HeaderText = "IDCab"
        Me.IDCab.Name = "IDCab"
        Me.IDCab.ReadOnly = True
        Me.IDCab.Visible = False
        '
        'Titulo
        '
        Me.Titulo.DataPropertyName = "Titulo"
        Me.Titulo.HeaderText = "Título"
        Me.Titulo.Name = "Titulo"
        Me.Titulo.ReadOnly = True
        '
        'Ruta
        '
        Me.Ruta.DataPropertyName = "Ruta"
        Me.Ruta.HeaderText = "Ruta"
        Me.Ruta.Name = "Ruta"
        Me.Ruta.ReadOnly = True
        '
        'CoEstado
        '
        Me.CoEstado.DataPropertyName = "CoEstado"
        Me.CoEstado.HeaderText = "CoEstado"
        Me.CoEstado.Name = "CoEstado"
        Me.CoEstado.ReadOnly = True
        Me.CoEstado.Visible = False
        '
        'DescEstado
        '
        Me.DescEstado.DataPropertyName = "DescEstado"
        Me.DescEstado.HeaderText = "Estado"
        Me.DescEstado.Name = "DescEstado"
        Me.DescEstado.ReadOnly = True
        '
        'btnEdit
        '
        Me.btnEdit.HeaderText = ""
        Me.btnEdit.Name = "btnEdit"
        Me.btnEdit.ReadOnly = True
        '
        'tbCliente
        '
        Me.tbCliente.Controls.Add(Me.grbPerfil)
        Me.tbCliente.Location = New System.Drawing.Point(4, 22)
        Me.tbCliente.Name = "tbCliente"
        Me.tbCliente.Padding = New System.Windows.Forms.Padding(3)
        Me.tbCliente.Size = New System.Drawing.Size(1006, 454)
        Me.tbCliente.TabIndex = 1
        Me.tbCliente.Text = "Perfil Cliente"
        Me.tbCliente.UseVisualStyleBackColor = True
        '
        'grbPerfil
        '
        Me.grbPerfil.BackColor = System.Drawing.Color.White
        Me.grbPerfil.Controls.Add(Me.pcbstar3)
        Me.grbPerfil.Controls.Add(Me.pcbstar2)
        Me.grbPerfil.Controls.Add(Me.pcbstar1)
        Me.grbPerfil.Controls.Add(Me.lblFechaPerfil)
        Me.grbPerfil.Controls.Add(Me.lblUsuarioPerfil)
        Me.grbPerfil.Controls.Add(Me.gbGuias)
        Me.grbPerfil.Controls.Add(Me.txtOperatividad)
        Me.grbPerfil.Controls.Add(Me.Label61)
        Me.grbPerfil.Controls.Add(Me.cboUsuarioRealizado)
        Me.grbPerfil.Controls.Add(Me.Label62)
        Me.grbPerfil.Controls.Add(Me.dtpFechaUpdate)
        Me.grbPerfil.Controls.Add(Me.Label63)
        Me.grbPerfil.Controls.Add(Me.txtInformacionGeneral)
        Me.grbPerfil.Controls.Add(Me.Label64)
        Me.grbPerfil.Controls.Add(Me.lblPerfilTit)
        Me.grbPerfil.Dock = System.Windows.Forms.DockStyle.Fill
        Me.grbPerfil.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.grbPerfil.Location = New System.Drawing.Point(3, 3)
        Me.grbPerfil.Name = "grbPerfil"
        Me.grbPerfil.Size = New System.Drawing.Size(1000, 448)
        Me.grbPerfil.TabIndex = 4
        Me.grbPerfil.TabStop = False
        '
        'pcbstar3
        '
        Me.pcbstar3.Image = CType(resources.GetObject("pcbstar3.Image"), System.Drawing.Image)
        Me.pcbstar3.Location = New System.Drawing.Point(527, 52)
        Me.pcbstar3.Name = "pcbstar3"
        Me.pcbstar3.Size = New System.Drawing.Size(26, 19)
        Me.pcbstar3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.pcbstar3.TabIndex = 1002
        Me.pcbstar3.TabStop = False
        Me.pcbstar3.Visible = False
        '
        'pcbstar2
        '
        Me.pcbstar2.Image = CType(resources.GetObject("pcbstar2.Image"), System.Drawing.Image)
        Me.pcbstar2.Location = New System.Drawing.Point(504, 52)
        Me.pcbstar2.Name = "pcbstar2"
        Me.pcbstar2.Size = New System.Drawing.Size(26, 19)
        Me.pcbstar2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.pcbstar2.TabIndex = 1001
        Me.pcbstar2.TabStop = False
        Me.pcbstar2.Visible = False
        '
        'pcbstar1
        '
        Me.pcbstar1.Image = Global.SETours.My.Resources.Resources.star
        Me.pcbstar1.Location = New System.Drawing.Point(479, 52)
        Me.pcbstar1.Name = "pcbstar1"
        Me.pcbstar1.Size = New System.Drawing.Size(26, 19)
        Me.pcbstar1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.pcbstar1.TabIndex = 1000
        Me.pcbstar1.TabStop = False
        Me.pcbstar1.Visible = False
        '
        'lblFechaPerfil
        '
        Me.lblFechaPerfil.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.lblFechaPerfil.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblFechaPerfil.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.lblFechaPerfil.Location = New System.Drawing.Point(104, 52)
        Me.lblFechaPerfil.Name = "lblFechaPerfil"
        Me.lblFechaPerfil.Size = New System.Drawing.Size(93, 18)
        Me.lblFechaPerfil.TabIndex = 517
        '
        'lblUsuarioPerfil
        '
        Me.lblUsuarioPerfil.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.lblUsuarioPerfil.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblUsuarioPerfil.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.lblUsuarioPerfil.Location = New System.Drawing.Point(353, 52)
        Me.lblUsuarioPerfil.Name = "lblUsuarioPerfil"
        Me.lblUsuarioPerfil.Size = New System.Drawing.Size(118, 18)
        Me.lblUsuarioPerfil.TabIndex = 516
        '
        'gbGuias
        '
        Me.gbGuias.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.gbGuias.Controls.Add(Me.Label58)
        Me.gbGuias.Controls.Add(Me.Label60)
        Me.gbGuias.Controls.Add(Me.dgvCiudades)
        Me.gbGuias.Controls.Add(Me.dgvGuias)
        Me.gbGuias.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbGuias.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.gbGuias.Location = New System.Drawing.Point(8, 329)
        Me.gbGuias.Name = "gbGuias"
        Me.gbGuias.Size = New System.Drawing.Size(882, 345)
        Me.gbGuias.TabIndex = 515
        Me.gbGuias.TabStop = False
        Me.gbGuias.Text = "Guías de Preferencia"
        '
        'Label58
        '
        Me.Label58.AutoSize = True
        Me.Label58.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label58.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.Label58.Location = New System.Drawing.Point(7, 122)
        Me.Label58.Name = "Label58"
        Me.Label58.Size = New System.Drawing.Size(44, 13)
        Me.Label58.TabIndex = 518
        Me.Label58.Text = "Guías :"
        '
        'Label60
        '
        Me.Label60.AutoSize = True
        Me.Label60.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label60.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.Label60.Location = New System.Drawing.Point(7, 17)
        Me.Label60.Name = "Label60"
        Me.Label60.Size = New System.Drawing.Size(64, 13)
        Me.Label60.TabIndex = 517
        Me.Label60.Text = "Ciudades :"
        '
        'dgvCiudades
        '
        Me.dgvCiudades.AllowUserToAddRows = False
        Me.dgvCiudades.AllowUserToDeleteRows = False
        Me.dgvCiudades.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        DataGridViewCellStyle13.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle13.BackColor = System.Drawing.SystemColors.ControlDarkDark
        DataGridViewCellStyle13.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle13.ForeColor = System.Drawing.Color.White
        DataGridViewCellStyle13.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle13.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle13.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvCiudades.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle13
        Me.dgvCiudades.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvCiudades.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.IDubigeo2, Me.Pais, Me.Ciudad, Me.CoCliente, Me.IDPais2})
        Me.dgvCiudades.Location = New System.Drawing.Point(10, 33)
        Me.dgvCiudades.MultiSelect = False
        Me.dgvCiudades.Name = "dgvCiudades"
        Me.dgvCiudades.ReadOnly = True
        DataGridViewCellStyle14.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle14.BackColor = System.Drawing.SystemColors.ControlDarkDark
        DataGridViewCellStyle14.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle14.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        DataGridViewCellStyle14.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle14.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle14.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvCiudades.RowHeadersDefaultCellStyle = DataGridViewCellStyle14
        Me.dgvCiudades.RowTemplate.DefaultCellStyle.ForeColor = System.Drawing.Color.Black
        Me.dgvCiudades.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvCiudades.Size = New System.Drawing.Size(821, 76)
        Me.dgvCiudades.TabIndex = 500
        '
        'IDubigeo2
        '
        Me.IDubigeo2.HeaderText = "N°"
        Me.IDubigeo2.Name = "IDubigeo2"
        Me.IDubigeo2.ReadOnly = True
        Me.IDubigeo2.Visible = False
        '
        'Pais
        '
        Me.Pais.HeaderText = "País"
        Me.Pais.Name = "Pais"
        Me.Pais.ReadOnly = True
        Me.Pais.Width = 120
        '
        'Ciudad
        '
        Me.Ciudad.HeaderText = "Ciudad"
        Me.Ciudad.Name = "Ciudad"
        Me.Ciudad.ReadOnly = True
        Me.Ciudad.Width = 250
        '
        'CoCliente
        '
        Me.CoCliente.HeaderText = "CoCliente"
        Me.CoCliente.Name = "CoCliente"
        Me.CoCliente.ReadOnly = True
        Me.CoCliente.Visible = False
        '
        'IDPais2
        '
        Me.IDPais2.HeaderText = "IDPais"
        Me.IDPais2.Name = "IDPais2"
        Me.IDPais2.ReadOnly = True
        Me.IDPais2.Visible = False
        '
        'dgvGuias
        '
        Me.dgvGuias.AllowUserToAddRows = False
        Me.dgvGuias.AllowUserToDeleteRows = False
        Me.dgvGuias.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        DataGridViewCellStyle15.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle15.BackColor = System.Drawing.SystemColors.ControlDarkDark
        DataGridViewCellStyle15.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle15.ForeColor = System.Drawing.Color.White
        DataGridViewCellStyle15.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle15.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle15.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvGuias.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle15
        Me.dgvGuias.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvGuias.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.CoProveedor, Me.NombreCorto, Me.IdCiudad2})
        Me.dgvGuias.Location = New System.Drawing.Point(10, 139)
        Me.dgvGuias.MultiSelect = False
        Me.dgvGuias.Name = "dgvGuias"
        Me.dgvGuias.ReadOnly = True
        DataGridViewCellStyle16.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle16.BackColor = System.Drawing.SystemColors.ControlDarkDark
        DataGridViewCellStyle16.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle16.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        DataGridViewCellStyle16.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle16.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle16.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvGuias.RowHeadersDefaultCellStyle = DataGridViewCellStyle16
        Me.dgvGuias.RowTemplate.DefaultCellStyle.ForeColor = System.Drawing.Color.Black
        Me.dgvGuias.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvGuias.Size = New System.Drawing.Size(821, 325)
        Me.dgvGuias.TabIndex = 500
        '
        'CoProveedor
        '
        Me.CoProveedor.HeaderText = "N°"
        Me.CoProveedor.Name = "CoProveedor"
        Me.CoProveedor.ReadOnly = True
        Me.CoProveedor.Visible = False
        '
        'NombreCorto
        '
        Me.NombreCorto.HeaderText = "Nombre"
        Me.NombreCorto.Name = "NombreCorto"
        Me.NombreCorto.ReadOnly = True
        Me.NombreCorto.Width = 400
        '
        'IdCiudad2
        '
        Me.IdCiudad2.HeaderText = "IdCiudad"
        Me.IdCiudad2.Name = "IdCiudad2"
        Me.IdCiudad2.ReadOnly = True
        Me.IdCiudad2.Visible = False
        '
        'txtOperatividad
        '
        Me.txtOperatividad.BackColor = System.Drawing.SystemColors.Window
        Me.txtOperatividad.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtOperatividad.Location = New System.Drawing.Point(9, 223)
        Me.txtOperatividad.MaxLength = 0
        Me.txtOperatividad.Multiline = True
        Me.txtOperatividad.Name = "txtOperatividad"
        Me.txtOperatividad.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtOperatividad.Size = New System.Drawing.Size(882, 100)
        Me.txtOperatividad.TabIndex = 514
        Me.txtOperatividad.Tag = "Borrar"
        '
        'Label61
        '
        Me.Label61.AutoSize = True
        Me.Label61.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label61.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.Label61.Location = New System.Drawing.Point(6, 207)
        Me.Label61.Name = "Label61"
        Me.Label61.Size = New System.Drawing.Size(134, 13)
        Me.Label61.TabIndex = 513
        Me.Label61.Text = "Sobre la operatividad :"
        '
        'cboUsuarioRealizado
        '
        Me.cboUsuarioRealizado.BackColor = System.Drawing.Color.White
        Me.cboUsuarioRealizado.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboUsuarioRealizado.Enabled = False
        Me.cboUsuarioRealizado.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboUsuarioRealizado.FormattingEnabled = True
        Me.cboUsuarioRealizado.Location = New System.Drawing.Point(650, 49)
        Me.cboUsuarioRealizado.Name = "cboUsuarioRealizado"
        Me.cboUsuarioRealizado.Size = New System.Drawing.Size(265, 21)
        Me.cboUsuarioRealizado.TabIndex = 512
        Me.cboUsuarioRealizado.Tag = "Borrar"
        Me.cboUsuarioRealizado.Visible = False
        '
        'Label62
        '
        Me.Label62.AutoSize = True
        Me.Label62.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label62.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.Label62.Location = New System.Drawing.Point(257, 52)
        Me.Label62.Name = "Label62"
        Me.Label62.Size = New System.Drawing.Size(90, 13)
        Me.Label62.TabIndex = 511
        Me.Label62.Text = "Realizado por :"
        '
        'dtpFechaUpdate
        '
        Me.dtpFechaUpdate.Enabled = False
        Me.dtpFechaUpdate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpFechaUpdate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpFechaUpdate.Location = New System.Drawing.Point(921, 49)
        Me.dtpFechaUpdate.Name = "dtpFechaUpdate"
        Me.dtpFechaUpdate.Size = New System.Drawing.Size(75, 21)
        Me.dtpFechaUpdate.TabIndex = 510
        Me.dtpFechaUpdate.Visible = False
        '
        'Label63
        '
        Me.Label63.AutoSize = True
        Me.Label63.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label63.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.Label63.Location = New System.Drawing.Point(8, 52)
        Me.Label63.Name = "Label63"
        Me.Label63.Size = New System.Drawing.Size(90, 13)
        Me.Label63.TabIndex = 509
        Me.Label63.Text = "Fecha Update :"
        '
        'txtInformacionGeneral
        '
        Me.txtInformacionGeneral.BackColor = System.Drawing.SystemColors.Window
        Me.txtInformacionGeneral.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtInformacionGeneral.Location = New System.Drawing.Point(8, 99)
        Me.txtInformacionGeneral.MaxLength = 0
        Me.txtInformacionGeneral.Multiline = True
        Me.txtInformacionGeneral.Name = "txtInformacionGeneral"
        Me.txtInformacionGeneral.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtInformacionGeneral.Size = New System.Drawing.Size(882, 105)
        Me.txtInformacionGeneral.TabIndex = 508
        Me.txtInformacionGeneral.Tag = "Borrar"
        '
        'Label64
        '
        Me.Label64.AutoSize = True
        Me.Label64.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label64.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.Label64.Location = New System.Drawing.Point(5, 83)
        Me.Label64.Name = "Label64"
        Me.Label64.Size = New System.Drawing.Size(129, 13)
        Me.Label64.TabIndex = 507
        Me.Label64.Text = "Información General :"
        '
        'lblPerfilTit
        '
        Me.lblPerfilTit.BackColor = System.Drawing.SystemColors.ControlDarkDark
        Me.lblPerfilTit.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.lblPerfilTit.Dock = System.Windows.Forms.DockStyle.Top
        Me.lblPerfilTit.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPerfilTit.ForeColor = System.Drawing.Color.White
        Me.lblPerfilTit.Location = New System.Drawing.Point(3, 17)
        Me.lblPerfilTit.Name = "lblPerfilTit"
        Me.lblPerfilTit.Size = New System.Drawing.Size(994, 22)
        Me.lblPerfilTit.TabIndex = 92
        Me.lblPerfilTit.Text = "Perfil de Cliente"
        '
        'tbCorreos
        '
        Me.tbCorreos.Controls.Add(Me.grbBandCorreos)
        Me.tbCorreos.Location = New System.Drawing.Point(4, 22)
        Me.tbCorreos.Name = "tbCorreos"
        Me.tbCorreos.Size = New System.Drawing.Size(1006, 454)
        Me.tbCorreos.TabIndex = 2
        Me.tbCorreos.Text = "Bandeja de Correos"
        Me.tbCorreos.UseVisualStyleBackColor = True
        '
        'grbBandCorreos
        '
        Me.grbBandCorreos.BackColor = System.Drawing.Color.White
        Me.grbBandCorreos.Controls.Add(Me.SplitContainer6)
        Me.grbBandCorreos.Controls.Add(Me.ToolStrip)
        Me.grbBandCorreos.Controls.Add(Me.Label73)
        Me.grbBandCorreos.Dock = System.Windows.Forms.DockStyle.Fill
        Me.grbBandCorreos.Location = New System.Drawing.Point(0, 0)
        Me.grbBandCorreos.Name = "grbBandCorreos"
        Me.grbBandCorreos.Size = New System.Drawing.Size(1006, 454)
        Me.grbBandCorreos.TabIndex = 4
        Me.grbBandCorreos.TabStop = False
        '
        'SplitContainer6
        '
        Me.SplitContainer6.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.SplitContainer6.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SplitContainer6.Location = New System.Drawing.Point(3, 42)
        Me.SplitContainer6.Name = "SplitContainer6"
        '
        'SplitContainer6.Panel1
        '
        Me.SplitContainer6.Panel1.Controls.Add(Me.tvw)
        '
        'SplitContainer6.Panel2
        '
        Me.SplitContainer6.Panel2.Controls.Add(Me.grbTexto)
        Me.SplitContainer6.Panel2.Controls.Add(Me.grbWeb)
        Me.SplitContainer6.Panel2.Controls.Add(Me.dgvCorreos)
        Me.SplitContainer6.Panel2.Controls.Add(Me.lblTipoCorreo)
        Me.SplitContainer6.Size = New System.Drawing.Size(1000, 409)
        Me.SplitContainer6.SplitterDistance = 205
        Me.SplitContainer6.TabIndex = 237
        '
        'tvw
        '
        Me.tvw.Dock = System.Windows.Forms.DockStyle.Fill
        Me.tvw.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tvw.ImageIndex = 0
        Me.tvw.ImageList = Me.imglstBandCorreos
        Me.tvw.Location = New System.Drawing.Point(0, 0)
        Me.tvw.Name = "tvw"
        Me.tvw.SelectedImageIndex = 0
        Me.tvw.Size = New System.Drawing.Size(201, 405)
        Me.tvw.TabIndex = 234
        '
        'imglstBandCorreos
        '
        Me.imglstBandCorreos.ImageStream = CType(resources.GetObject("imglstBandCorreos.ImageStream"), System.Windows.Forms.ImageListStreamer)
        Me.imglstBandCorreos.TransparentColor = System.Drawing.Color.Transparent
        Me.imglstBandCorreos.Images.SetKeyName(0, "outlook_folder_home.png")
        Me.imglstBandCorreos.Images.SetKeyName(1, "outlook_folder_inbox.png")
        Me.imglstBandCorreos.Images.SetKeyName(2, "outlook_folder_sent.png")
        Me.imglstBandCorreos.Images.SetKeyName(3, "user_green.png")
        Me.imglstBandCorreos.Images.SetKeyName(4, "user_orange.png")
        Me.imglstBandCorreos.Images.SetKeyName(5, "user_red.png")
        Me.imglstBandCorreos.Images.SetKeyName(6, "folder_user.png")
        Me.imglstBandCorreos.Images.SetKeyName(7, "cup.png")
        '
        'grbTexto
        '
        Me.grbTexto.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.grbTexto.Controls.Add(Me.rtbMensaje)
        Me.grbTexto.Location = New System.Drawing.Point(7, 219)
        Me.grbTexto.Name = "grbTexto"
        Me.grbTexto.Size = New System.Drawing.Size(779, 341)
        Me.grbTexto.TabIndex = 244
        Me.grbTexto.TabStop = False
        '
        'rtbMensaje
        '
        Me.rtbMensaje.Dock = System.Windows.Forms.DockStyle.Fill
        Me.rtbMensaje.Font = New System.Drawing.Font("Lucida Console", 9.75!)
        Me.rtbMensaje.Location = New System.Drawing.Point(3, 17)
        Me.rtbMensaje.Name = "rtbMensaje"
        Me.rtbMensaje.ReadOnly = True
        Me.rtbMensaje.Size = New System.Drawing.Size(773, 321)
        Me.rtbMensaje.TabIndex = 238
        Me.rtbMensaje.Text = ""
        '
        'grbWeb
        '
        Me.grbWeb.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.grbWeb.Controls.Add(Me.webHtml)
        Me.grbWeb.Location = New System.Drawing.Point(7, 218)
        Me.grbWeb.Name = "grbWeb"
        Me.grbWeb.Size = New System.Drawing.Size(778, 341)
        Me.grbWeb.TabIndex = 243
        Me.grbWeb.TabStop = False
        '
        'webHtml
        '
        Me.webHtml.AllowWebBrowserDrop = False
        Me.webHtml.Dock = System.Windows.Forms.DockStyle.Fill
        Me.webHtml.IsWebBrowserContextMenuEnabled = False
        Me.webHtml.Location = New System.Drawing.Point(3, 17)
        Me.webHtml.MinimumSize = New System.Drawing.Size(20, 20)
        Me.webHtml.Name = "webHtml"
        Me.webHtml.Size = New System.Drawing.Size(772, 321)
        Me.webHtml.TabIndex = 242
        '
        'dgvCorreos
        '
        Me.dgvCorreos.AllowUserToAddRows = False
        Me.dgvCorreos.AllowUserToDeleteRows = False
        Me.dgvCorreos.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        DataGridViewCellStyle17.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle17.BackColor = System.Drawing.SystemColors.ControlDarkDark
        DataGridViewCellStyle17.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle17.ForeColor = System.Drawing.Color.White
        DataGridViewCellStyle17.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle17.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle17.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvCorreos.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle17
        Me.dgvCorreos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvCorreos.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.IconoCorreo, Me.ImgAdjuntos, Me.NuCorreo, Me.CantAdjuntos, Me.EntryID, Me.De, Me.Para, Me.Asunto, Me.Body, Me.BodyHtml, Me.ConCopia, Me.EstadoLeido, Me.Importance, Me.BodyFormat, Me.RutasAdjuntos, Me.FechaCorreo})
        Me.dgvCorreos.Location = New System.Drawing.Point(7, 35)
        Me.dgvCorreos.MultiSelect = False
        Me.dgvCorreos.Name = "dgvCorreos"
        Me.dgvCorreos.ReadOnly = True
        DataGridViewCellStyle18.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle18.BackColor = System.Drawing.SystemColors.ControlDarkDark
        DataGridViewCellStyle18.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle18.ForeColor = System.Drawing.Color.White
        DataGridViewCellStyle18.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle18.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle18.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvCorreos.RowHeadersDefaultCellStyle = DataGridViewCellStyle18
        Me.dgvCorreos.RowTemplate.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgvCorreos.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect
        Me.dgvCorreos.Size = New System.Drawing.Size(777, 178)
        Me.dgvCorreos.TabIndex = 104
        Me.dgvCorreos.Tag = "Borrar"
        '
        'IconoCorreo
        '
        Me.IconoCorreo.DataPropertyName = "IconoCorreo"
        Me.IconoCorreo.HeaderText = ""
        Me.IconoCorreo.Name = "IconoCorreo"
        Me.IconoCorreo.ReadOnly = True
        Me.IconoCorreo.Width = 25
        '
        'ImgAdjuntos
        '
        Me.ImgAdjuntos.DataPropertyName = "ImgAdjuntos"
        Me.ImgAdjuntos.HeaderText = ""
        Me.ImgAdjuntos.Name = "ImgAdjuntos"
        Me.ImgAdjuntos.ReadOnly = True
        Me.ImgAdjuntos.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.ImgAdjuntos.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic
        Me.ImgAdjuntos.Width = 25
        '
        'NuCorreo
        '
        Me.NuCorreo.DataPropertyName = "NuCorreo"
        Me.NuCorreo.HeaderText = "NuCorreo"
        Me.NuCorreo.Name = "NuCorreo"
        Me.NuCorreo.ReadOnly = True
        Me.NuCorreo.Visible = False
        '
        'CantAdjuntos
        '
        Me.CantAdjuntos.DataPropertyName = "CantAdjuntos"
        Me.CantAdjuntos.HeaderText = "CantAdjuntos"
        Me.CantAdjuntos.Name = "CantAdjuntos"
        Me.CantAdjuntos.ReadOnly = True
        Me.CantAdjuntos.Visible = False
        '
        'EntryID
        '
        Me.EntryID.DataPropertyName = "EntryID"
        Me.EntryID.HeaderText = "EntryID"
        Me.EntryID.Name = "EntryID"
        Me.EntryID.ReadOnly = True
        Me.EntryID.Visible = False
        '
        'De
        '
        Me.De.DataPropertyName = "De"
        Me.De.FillWeight = 65.65144!
        Me.De.HeaderText = "De"
        Me.De.Name = "De"
        Me.De.ReadOnly = True
        Me.De.Width = 143
        '
        'Para
        '
        Me.Para.DataPropertyName = "Para"
        Me.Para.FillWeight = 65.65144!
        Me.Para.HeaderText = "Para"
        Me.Para.Name = "Para"
        Me.Para.ReadOnly = True
        Me.Para.Width = 121
        '
        'Asunto
        '
        Me.Asunto.DataPropertyName = "Asunto"
        Me.Asunto.HeaderText = "Asunto"
        Me.Asunto.Name = "Asunto"
        Me.Asunto.ReadOnly = True
        Me.Asunto.Width = 185
        '
        'Body
        '
        Me.Body.DataPropertyName = "Body"
        Me.Body.HeaderText = "Body"
        Me.Body.Name = "Body"
        Me.Body.ReadOnly = True
        Me.Body.Visible = False
        '
        'BodyHtml
        '
        Me.BodyHtml.DataPropertyName = "BodyHtml"
        Me.BodyHtml.HeaderText = "BodyHtml"
        Me.BodyHtml.Name = "BodyHtml"
        Me.BodyHtml.ReadOnly = True
        Me.BodyHtml.Visible = False
        '
        'ConCopia
        '
        Me.ConCopia.DataPropertyName = "ConCopia"
        Me.ConCopia.HeaderText = "ConCopia"
        Me.ConCopia.Name = "ConCopia"
        Me.ConCopia.ReadOnly = True
        Me.ConCopia.Visible = False
        '
        'EstadoLeido
        '
        Me.EstadoLeido.DataPropertyName = "EstadoLeido"
        Me.EstadoLeido.HeaderText = "EstadoLeido"
        Me.EstadoLeido.Name = "EstadoLeido"
        Me.EstadoLeido.ReadOnly = True
        Me.EstadoLeido.Visible = False
        '
        'Importance
        '
        Me.Importance.DataPropertyName = "Importance"
        Me.Importance.HeaderText = "Importance"
        Me.Importance.Name = "Importance"
        Me.Importance.ReadOnly = True
        Me.Importance.Visible = False
        '
        'BodyFormat
        '
        Me.BodyFormat.DataPropertyName = "BodyFormat"
        Me.BodyFormat.HeaderText = "BodyFormat"
        Me.BodyFormat.Name = "BodyFormat"
        Me.BodyFormat.ReadOnly = True
        Me.BodyFormat.Visible = False
        '
        'RutasAdjuntos
        '
        Me.RutasAdjuntos.DataPropertyName = "RutasAdjuntos"
        Me.RutasAdjuntos.HeaderText = "RutasAdjuntos"
        Me.RutasAdjuntos.Name = "RutasAdjuntos"
        Me.RutasAdjuntos.ReadOnly = True
        Me.RutasAdjuntos.Visible = False
        '
        'FechaCorreo
        '
        Me.FechaCorreo.DataPropertyName = "Fecha"
        Me.FechaCorreo.HeaderText = "Fecha  "
        Me.FechaCorreo.Name = "FechaCorreo"
        Me.FechaCorreo.ReadOnly = True
        Me.FechaCorreo.Width = 71
        '
        'lblTipoCorreo
        '
        Me.lblTipoCorreo.BackColor = System.Drawing.Color.White
        Me.lblTipoCorreo.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTipoCorreo.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.lblTipoCorreo.ImageIndex = 0
        Me.lblTipoCorreo.Location = New System.Drawing.Point(4, 2)
        Me.lblTipoCorreo.Name = "lblTipoCorreo"
        Me.lblTipoCorreo.Size = New System.Drawing.Size(810, 30)
        Me.lblTipoCorreo.TabIndex = 236
        Me.lblTipoCorreo.Text = "     Elementos Enviados"
        Me.lblTipoCorreo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'ToolStrip
        '
        Me.ToolStrip.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.TssBtnNuevo, Me.ToolStripSeparator2, Me.tsnBtnImprimir, Me.TsBtnResponder, Me.TsBtnResponderTodos, Me.TsBtnReenviar, Me.ToolStripSeparator3, Me.btnActualizarBand, Me.ToolStripSeparator1, Me.ToolStripLabel1, Me.txtBusqAsunto})
        Me.ToolStrip.Location = New System.Drawing.Point(3, 17)
        Me.ToolStrip.Name = "ToolStrip"
        Me.ToolStrip.Size = New System.Drawing.Size(1000, 25)
        Me.ToolStrip.TabIndex = 233
        Me.ToolStrip.Text = "ToolStrip1"
        '
        'TssBtnNuevo
        '
        Me.TssBtnNuevo.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.TssBtnNuevo.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolSSMIMensaje})
        Me.TssBtnNuevo.Image = Global.SETours.My.Resources.Resources.outlook_new_message
        Me.TssBtnNuevo.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.TssBtnNuevo.Name = "TssBtnNuevo"
        Me.TssBtnNuevo.Size = New System.Drawing.Size(74, 22)
        Me.TssBtnNuevo.Text = "&Nuevo"
        '
        'ToolSSMIMensaje
        '
        Me.ToolSSMIMensaje.Image = Global.SETours.My.Resources.Resources.outlook_send
        Me.ToolSSMIMensaje.Name = "ToolSSMIMensaje"
        Me.ToolSSMIMensaje.Size = New System.Drawing.Size(150, 22)
        Me.ToolSSMIMensaje.Text = "Mensaje Email"
        '
        'ToolStripSeparator2
        '
        Me.ToolStripSeparator2.Name = "ToolStripSeparator2"
        Me.ToolStripSeparator2.Size = New System.Drawing.Size(6, 25)
        '
        'tsnBtnImprimir
        '
        Me.tsnBtnImprimir.Image = Global.SETours.My.Resources.Resources.printer
        Me.tsnBtnImprimir.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.tsnBtnImprimir.Name = "tsnBtnImprimir"
        Me.tsnBtnImprimir.Size = New System.Drawing.Size(73, 22)
        Me.tsnBtnImprimir.Text = "Imprimir"
        '
        'TsBtnResponder
        '
        Me.TsBtnResponder.Image = Global.SETours.My.Resources.Resources.outlook_response
        Me.TsBtnResponder.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.TsBtnResponder.Name = "TsBtnResponder"
        Me.TsBtnResponder.Size = New System.Drawing.Size(83, 22)
        Me.TsBtnResponder.Text = "Re&sponder"
        '
        'TsBtnResponderTodos
        '
        Me.TsBtnResponderTodos.Image = Global.SETours.My.Resources.Resources.outlook_icon_msg_signed_reply
        Me.TsBtnResponderTodos.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.TsBtnResponderTodos.Name = "TsBtnResponderTodos"
        Me.TsBtnResponderTodos.Size = New System.Drawing.Size(125, 22)
        Me.TsBtnResponderTodos.Text = "Respon&der a todos"
        '
        'TsBtnReenviar
        '
        Me.TsBtnReenviar.Image = Global.SETours.My.Resources.Resources.outlook_msg_forward
        Me.TsBtnReenviar.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.TsBtnReenviar.Name = "TsBtnReenviar"
        Me.TsBtnReenviar.Size = New System.Drawing.Size(72, 22)
        Me.TsBtnReenviar.Text = "&Reenviar"
        '
        'ToolStripSeparator3
        '
        Me.ToolStripSeparator3.Name = "ToolStripSeparator3"
        Me.ToolStripSeparator3.Size = New System.Drawing.Size(6, 25)
        '
        'btnActualizarBand
        '
        Me.btnActualizarBand.Image = Global.SETours.My.Resources.Resources.outlook_send_and_received
        Me.btnActualizarBand.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnActualizarBand.Name = "btnActualizarBand"
        Me.btnActualizarBand.Size = New System.Drawing.Size(107, 22)
        Me.btnActualizarBand.Text = "Enviar y Recibir"
        '
        'ToolStripSeparator1
        '
        Me.ToolStripSeparator1.Name = "ToolStripSeparator1"
        Me.ToolStripSeparator1.Size = New System.Drawing.Size(6, 25)
        '
        'ToolStripLabel1
        '
        Me.ToolStripLabel1.Name = "ToolStripLabel1"
        Me.ToolStripLabel1.Size = New System.Drawing.Size(48, 22)
        Me.ToolStripLabel1.Text = "Asunto:"
        '
        'txtBusqAsunto
        '
        Me.txtBusqAsunto.Name = "txtBusqAsunto"
        Me.txtBusqAsunto.Size = New System.Drawing.Size(300, 25)
        '
        'Label73
        '
        Me.Label73.AutoSize = True
        Me.Label73.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label73.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.Label73.Location = New System.Drawing.Point(6, 100)
        Me.Label73.Name = "Label73"
        Me.Label73.Size = New System.Drawing.Size(0, 13)
        Me.Label73.TabIndex = 94
        '
        'cmsCopiarCotiz
        '
        Me.cmsCopiarCotiz.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuCopiar, Me.mnuPegar})
        Me.cmsCopiarCotiz.Name = "cmsEditFechas"
        Me.cmsCopiarCotiz.Size = New System.Drawing.Size(110, 48)
        '
        'mnuCopiar
        '
        Me.mnuCopiar.Image = Global.SETours.My.Resources.Resources.add_source_copy
        Me.mnuCopiar.Name = "mnuCopiar"
        Me.mnuCopiar.Size = New System.Drawing.Size(109, 22)
        Me.mnuCopiar.Text = "Copiar"
        '
        'mnuPegar
        '
        Me.mnuPegar.Image = Global.SETours.My.Resources.Resources.page_paste
        Me.mnuPegar.Name = "mnuPegar"
        Me.mnuPegar.Size = New System.Drawing.Size(109, 22)
        Me.mnuPegar.Text = "Pegar"
        '
        'ErrPrv
        '
        Me.ErrPrv.ContainerControl = Me
        '
        'tbArchivos
        '
        Me.tbArchivos.Controls.Add(Me.btnNuevoArchivo)
        Me.tbArchivos.Controls.Add(Me.dgvArchivos)
        Me.tbArchivos.Location = New System.Drawing.Point(4, 22)
        Me.tbArchivos.Name = "tbArchivos"
        Me.tbArchivos.Size = New System.Drawing.Size(1006, 454)
        Me.tbArchivos.TabIndex = 3
        Me.tbArchivos.Text = "Archivos"
        Me.tbArchivos.UseVisualStyleBackColor = True
        '
        'btnNuevoArchivo
        '
        Me.btnNuevoArchivo.Image = Global.SETours.My.Resources.Resources.page_add
        Me.btnNuevoArchivo.Location = New System.Drawing.Point(709, 4)
        Me.btnNuevoArchivo.Name = "btnNuevoArchivo"
        Me.btnNuevoArchivo.Size = New System.Drawing.Size(27, 27)
        Me.btnNuevoArchivo.TabIndex = 508
        Me.btnNuevoArchivo.Text = " "
        Me.btnNuevoArchivo.UseVisualStyleBackColor = True
        '
        'dgvArchivos
        '
        Me.dgvArchivos.AllowDrop = True
        Me.dgvArchivos.AllowUserToAddRows = False
        Me.dgvArchivos.AllowUserToDeleteRows = False
        DataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle9.BackColor = System.Drawing.SystemColors.ControlDarkDark
        DataGridViewCellStyle9.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle9.ForeColor = System.Drawing.Color.White
        DataGridViewCellStyle9.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle9.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle9.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvArchivos.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle9
        Me.dgvArchivos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvArchivos.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.IDArchivo, Me.IdCab_Archivo, Me.Nombre, Me.RutaOrigen, Me.RutaDestino, Me.CoTipo, Me.Tipo_Archivo, Me.FlCopiado, Me.FechaModificacion, Me.CoUsuarioCreacion, Me.Usuario, Me.btnEditA, Me.btnDelA})
        Me.dgvArchivos.Location = New System.Drawing.Point(4, 5)
        Me.dgvArchivos.Name = "dgvArchivos"
        Me.dgvArchivos.ReadOnly = True
        DataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle10.BackColor = System.Drawing.SystemColors.ControlDarkDark
        DataGridViewCellStyle10.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle10.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        DataGridViewCellStyle10.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle10.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle10.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvArchivos.RowHeadersDefaultCellStyle = DataGridViewCellStyle10
        Me.dgvArchivos.RowTemplate.DefaultCellStyle.ForeColor = System.Drawing.Color.Black
        Me.dgvArchivos.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.dgvArchivos.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvArchivos.Size = New System.Drawing.Size(700, 340)
        Me.dgvArchivos.TabIndex = 507
        '
        'IDArchivo
        '
        Me.IDArchivo.HeaderText = "N°"
        Me.IDArchivo.Name = "IDArchivo"
        Me.IDArchivo.ReadOnly = True
        Me.IDArchivo.Visible = False
        '
        'IdCab_Archivo
        '
        Me.IdCab_Archivo.HeaderText = "IDCAB"
        Me.IdCab_Archivo.Name = "IdCab_Archivo"
        Me.IdCab_Archivo.ReadOnly = True
        Me.IdCab_Archivo.Visible = False
        '
        'Nombre
        '
        Me.Nombre.HeaderText = "Nombre"
        Me.Nombre.Name = "Nombre"
        Me.Nombre.ReadOnly = True
        '
        'RutaOrigen
        '
        Me.RutaOrigen.HeaderText = "RutaOrigen"
        Me.RutaOrigen.Name = "RutaOrigen"
        Me.RutaOrigen.ReadOnly = True
        Me.RutaOrigen.Visible = False
        '
        'RutaDestino
        '
        Me.RutaDestino.HeaderText = "RutaDestino"
        Me.RutaDestino.Name = "RutaDestino"
        Me.RutaDestino.ReadOnly = True
        Me.RutaDestino.Visible = False
        '
        'CoTipo
        '
        Me.CoTipo.HeaderText = "CoTipo"
        Me.CoTipo.Name = "CoTipo"
        Me.CoTipo.ReadOnly = True
        Me.CoTipo.Visible = False
        '
        'Tipo_Archivo
        '
        Me.Tipo_Archivo.HeaderText = "Tipo"
        Me.Tipo_Archivo.Name = "Tipo_Archivo"
        Me.Tipo_Archivo.ReadOnly = True
        '
        'FlCopiado
        '
        Me.FlCopiado.HeaderText = "FlCopiado"
        Me.FlCopiado.Name = "FlCopiado"
        Me.FlCopiado.ReadOnly = True
        Me.FlCopiado.Visible = False
        '
        'FechaModificacion
        '
        Me.FechaModificacion.HeaderText = "Fecha Mod."
        Me.FechaModificacion.Name = "FechaModificacion"
        Me.FechaModificacion.ReadOnly = True
        '
        'CoUsuarioCreacion
        '
        Me.CoUsuarioCreacion.HeaderText = "CoUsuarioCreacion"
        Me.CoUsuarioCreacion.Name = "CoUsuarioCreacion"
        Me.CoUsuarioCreacion.ReadOnly = True
        Me.CoUsuarioCreacion.Visible = False
        '
        'Usuario
        '
        Me.Usuario.HeaderText = "Usuario"
        Me.Usuario.Name = "Usuario"
        Me.Usuario.ReadOnly = True
        '
        'btnEditA
        '
        Me.btnEditA.HeaderText = ""
        Me.btnEditA.Name = "btnEditA"
        Me.btnEditA.ReadOnly = True
        '
        'btnDelA
        '
        Me.btnDelA.HeaderText = ""
        Me.btnDelA.Name = "btnDelA"
        Me.btnDelA.ReadOnly = True
        '
        'frmIngPedidoDato
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1014, 480)
        Me.Controls.Add(Me.tabPedido)
        Me.KeyPreview = True
        Me.Name = "frmIngPedidoDato"
        Me.Tag = "Pedido"
        Me.Text = "Ingreso de Pedido"
        Me.tabPedido.ResumeLayout(False)
        Me.tbDatGrales.ResumeLayout(False)
        Me.SplitContainer1.Panel1.ResumeLayout(False)
        Me.SplitContainer1.Panel1.PerformLayout()
        Me.SplitContainer1.Panel2.ResumeLayout(False)
        CType(Me.SplitContainer1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.SplitContainer1.ResumeLayout(False)
        CType(Me.dgv, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tbCliente.ResumeLayout(False)
        Me.grbPerfil.ResumeLayout(False)
        Me.grbPerfil.PerformLayout()
        CType(Me.pcbstar3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pcbstar2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pcbstar1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gbGuias.ResumeLayout(False)
        Me.gbGuias.PerformLayout()
        CType(Me.dgvCiudades, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvGuias, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tbCorreos.ResumeLayout(False)
        Me.grbBandCorreos.ResumeLayout(False)
        Me.grbBandCorreos.PerformLayout()
        Me.SplitContainer6.Panel1.ResumeLayout(False)
        Me.SplitContainer6.Panel2.ResumeLayout(False)
        CType(Me.SplitContainer6, System.ComponentModel.ISupportInitialize).EndInit()
        Me.SplitContainer6.ResumeLayout(False)
        Me.grbTexto.ResumeLayout(False)
        Me.grbWeb.ResumeLayout(False)
        CType(Me.dgvCorreos, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ToolStrip.ResumeLayout(False)
        Me.ToolStrip.PerformLayout()
        Me.cmsCopiarCotiz.ResumeLayout(False)
        CType(Me.ErrPrv, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tbArchivos.ResumeLayout(False)
        CType(Me.dgvArchivos, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents tabPedido As System.Windows.Forms.TabControl
    Friend WithEvents tbDatGrales As System.Windows.Forms.TabPage
    Friend WithEvents tbCliente As System.Windows.Forms.TabPage
    Friend WithEvents tbCorreos As System.Windows.Forms.TabPage
    Friend WithEvents SplitContainer1 As System.Windows.Forms.SplitContainer
    Friend WithEvents chkActivos As System.Windows.Forms.CheckBox
    Friend WithEvents cboCoEjeVta As System.Windows.Forms.ComboBox
    Friend WithEvents btnCliente As System.Windows.Forms.Button
    Friend WithEvents lblCliente As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents lblNuPedido As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents dgv As System.Windows.Forms.DataGridView
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents txtTxTitulo As System.Windows.Forms.TextBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents dtpFePedido As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents cboEstado As System.Windows.Forms.ComboBox
    Friend WithEvents btnNuevo As System.Windows.Forms.Button
    Friend WithEvents ToolTip1 As System.Windows.Forms.ToolTip
    Friend WithEvents grbPerfil As System.Windows.Forms.GroupBox
    Friend WithEvents pcbstar3 As System.Windows.Forms.PictureBox
    Friend WithEvents pcbstar2 As System.Windows.Forms.PictureBox
    Friend WithEvents pcbstar1 As System.Windows.Forms.PictureBox
    Friend WithEvents lblFechaPerfil As System.Windows.Forms.Label
    Friend WithEvents lblUsuarioPerfil As System.Windows.Forms.Label
    Friend WithEvents gbGuias As System.Windows.Forms.GroupBox
    Friend WithEvents Label58 As System.Windows.Forms.Label
    Friend WithEvents Label60 As System.Windows.Forms.Label
    Friend WithEvents dgvCiudades As System.Windows.Forms.DataGridView
    Friend WithEvents IDubigeo2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Pais As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Ciudad As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents CoCliente As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents IDPais2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgvGuias As System.Windows.Forms.DataGridView
    Friend WithEvents CoProveedor As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents NombreCorto As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents IdCiudad2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents txtOperatividad As System.Windows.Forms.TextBox
    Friend WithEvents Label61 As System.Windows.Forms.Label
    Friend WithEvents cboUsuarioRealizado As System.Windows.Forms.ComboBox
    Friend WithEvents Label62 As System.Windows.Forms.Label
    Friend WithEvents dtpFechaUpdate As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label63 As System.Windows.Forms.Label
    Friend WithEvents txtInformacionGeneral As System.Windows.Forms.TextBox
    Friend WithEvents Label64 As System.Windows.Forms.Label
    Friend WithEvents lblPerfilTit As System.Windows.Forms.Label
    Friend WithEvents grbBandCorreos As System.Windows.Forms.GroupBox
    Friend WithEvents SplitContainer6 As System.Windows.Forms.SplitContainer
    Friend WithEvents tvw As System.Windows.Forms.TreeView
    Friend WithEvents grbTexto As System.Windows.Forms.GroupBox
    Friend WithEvents rtbMensaje As System.Windows.Forms.RichTextBox
    Friend WithEvents grbWeb As System.Windows.Forms.GroupBox
    Friend WithEvents webHtml As System.Windows.Forms.WebBrowser
    Public WithEvents dgvCorreos As System.Windows.Forms.DataGridView
    Friend WithEvents lblTipoCorreo As System.Windows.Forms.Label
    Friend WithEvents ToolStrip As System.Windows.Forms.ToolStrip
    Friend WithEvents TssBtnNuevo As System.Windows.Forms.ToolStripSplitButton
    Friend WithEvents ToolSSMIMensaje As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator2 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents tsnBtnImprimir As System.Windows.Forms.ToolStripButton
    Friend WithEvents TsBtnResponder As System.Windows.Forms.ToolStripButton
    Friend WithEvents TsBtnResponderTodos As System.Windows.Forms.ToolStripButton
    Friend WithEvents TsBtnReenviar As System.Windows.Forms.ToolStripButton
    Friend WithEvents ToolStripSeparator3 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents btnActualizarBand As System.Windows.Forms.ToolStripButton
    Friend WithEvents ToolStripSeparator1 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents ToolStripLabel1 As System.Windows.Forms.ToolStripLabel
    Friend WithEvents txtBusqAsunto As System.Windows.Forms.ToolStripTextBox
    Friend WithEvents Label73 As System.Windows.Forms.Label
    Friend WithEvents imglstBandCorreos As System.Windows.Forms.ImageList
    Friend WithEvents IconoCorreo As System.Windows.Forms.DataGridViewImageColumn
    Friend WithEvents ImgAdjuntos As System.Windows.Forms.DataGridViewImageColumn
    Friend WithEvents NuCorreo As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents CantAdjuntos As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents EntryID As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents De As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Para As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Asunto As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Body As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents BodyHtml As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ConCopia As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents EstadoLeido As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Importance As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents BodyFormat As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents RutasAdjuntos As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents FechaCorreo As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents cmsCopiarCotiz As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents mnuCopiar As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuPegar As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ErrPrv As System.Windows.Forms.ErrorProvider
    Friend WithEvents EjecutivoVtas As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Fecha As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents FecInicio As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Pax As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Cotizacion As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents NuFile As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents IDCab As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Titulo As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Ruta As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents CoEstado As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DescEstado As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents btnEdit As System.Windows.Forms.DataGridViewButtonColumn
    Friend WithEvents tbArchivos As System.Windows.Forms.TabPage
    Friend WithEvents btnNuevoArchivo As System.Windows.Forms.Button
    Friend WithEvents dgvArchivos As System.Windows.Forms.DataGridView
    Friend WithEvents IDArchivo As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents IdCab_Archivo As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Nombre As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents RutaOrigen As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents RutaDestino As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents CoTipo As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Tipo_Archivo As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents FlCopiado As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents FechaModificacion As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents CoUsuarioCreacion As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Usuario As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents btnEditA As System.Windows.Forms.DataGridViewButtonColumn
    Friend WithEvents btnDelA As System.Windows.Forms.DataGridViewButtonColumn
End Class
