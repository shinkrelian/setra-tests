﻿Imports ComSEToursBL2
Public Class frmMantDetServiciosProveedor

    Dim blnNuevoEnabled As Boolean = True
    Dim blnGrabarEnabled As Boolean = False
    Dim blnDeshacerEnabled As Boolean = False
    Dim blnEditarEnabled As Boolean = False
    Dim blnEliminarEnabled As Boolean = False
    Dim blnSalirEnabled As Boolean = True

    Dim blnGrabarEnabledBD As Boolean = False
    Dim blnEditarEnabledBD As Boolean = False
    Dim blnEliminarEnabledBD As Boolean = False

    Public strIDServicio As String
    Public strDescServicio As String
    Public strIDTipoProv As String

    Public FormRef As frmMantServiciosProveedor
    Dim objBN As New clsServicioProveedorBN.clsDetalleServicioProveedorBN

    Dim frmmain As Object = Nothing
    Private Sub btnBuscar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBuscar.Click, txtDescripcion.TextChanged
        Try
            Buscar()
            dgv_Click(Nothing, Nothing)
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Error en " & Err.Source, MessageBoxButtons.OK, MessageBoxIcon.Error)

        End Try

    End Sub
    Private Sub CargarSeguridad()
        Try
            Dim strNombreForm As String
            strNombreForm = Replace(Me.Name, "frm", "mnu")
            strNombreForm = Replace(strNombreForm, "Dato", "")

            Using objSeg As New clsSeguridadBN.clsOpcionesBN.clsUsuarioBN
                Using dr As SqlClient.SqlDataReader = objSeg.ConsultarOpcionesForm(gstrUser, strNombreForm)
                    If dr.HasRows Then
                        dr.Read()
                        blnGrabarEnabledBD = dr("Grabar")
                        blnEditarEnabledBD = dr("Actualizar")
                        blnEliminarEnabledBD = dr("Eliminar")
                    End If
                    dr.Close()
                End Using
            End Using
        Catch ex As Exception
            Throw
        End Try
    End Sub
    Private Sub ActivarDesactivarBotones()

        'frmMain.btnNuevo.Enabled = blnNuevoEnabled
        If blnNuevoEnabled = True Then
            If blnGrabarEnabledBD Then
                frmMain.btnNuevo.Enabled = blnNuevoEnabled
            Else
                frmMain.btnNuevo.Enabled = False
                blnNuevoEnabled = blnGrabarEnabledBD
            End If
        Else
            frmMain.btnNuevo.Enabled = blnNuevoEnabled
        End If

        'frmMain.btnGrabar.Enabled = blnGrabarEnabled
        If blnGrabarEnabled = True Then
            If blnGrabarEnabledBD Then
                frmMain.btnGrabar.Enabled = blnGrabarEnabled
            Else
                frmMain.btnGrabar.Enabled = False
                blnGrabarEnabled = blnGrabarEnabledBD
            End If
        Else
            frmMain.btnGrabar.Enabled = blnGrabarEnabled
        End If

        frmMain.btnDeshacer.Enabled = blnDeshacerEnabled

        'frmMain.btnEditar.Enabled = blnEditarEnabled
        If blnEditarEnabled = True Then
            If blnEditarEnabledBD Then
                frmMain.btnEditar.Enabled = blnEditarEnabled
            Else
                frmMain.btnEditar.Enabled = False
                blnEditarEnabled = blnEditarEnabledBD
            End If
        Else
            frmMain.btnEditar.Enabled = blnEditarEnabled
        End If

        'frmMain.btnEliminar.Enabled = blnEliminarEnabled
        If blnEliminarEnabled = True Then
            If blnEliminarEnabledBD Then
                frmMain.btnEliminar.Enabled = blnEliminarEnabled
            Else
                frmMain.btnEliminar.Enabled = False
                blnEliminarEnabled = blnEliminarEnabledBD
            End If
        Else
            frmMain.btnEliminar.Enabled = blnEliminarEnabled
        End If

        frmMain.btnSalir.Enabled = blnSalirEnabled

    End Sub

    Private Sub frmMantUsuarios_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Activated
        'AddHandler frmMain.btnSalir.Click, AddressOf pSalir
        frmMain.btnSalir.Enabled = blnSalirEnabled
        'AddHandler frmMain.btnNuevo.Click, AddressOf pNuevo
        frmMain.btnNuevo.Enabled = blnNuevoEnabled
        'AddHandler frmMain.btnEliminar.Click, AddressOf pEliminar
        frmMain.btnEliminar.Enabled = blnEliminarEnabled
        'AddHandler frmMain.btnEditar.Click, AddressOf pEditar
        frmMain.btnEditar.Enabled = blnEditarEnabled

    End Sub

    Private Sub frmMantUsuarios_Deactivate(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Deactivate
        'RemoveHandler frmMain.btnSalir.Click, AddressOf pSalir
        'RemoveHandler frmMain.btnNuevo.Click, AddressOf pNuevo
        'RemoveHandler frmMain.btnEliminar.Click, AddressOf pEliminar
        'RemoveHandler frmMain.btnEditar.Click, AddressOf pEditar

    End Sub

    Private Sub frmMantBancos_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        frmMain.DesabledBtns()
    End Sub

    Private Sub frmMantBancos_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) _
    Handles Me.KeyDown

        Select Case e.KeyCode
            Case Keys.F2
                pNuevo(Nothing, Nothing)
            Case Keys.F3
                pEditar(Nothing, Nothing)
            Case Keys.F4
                pEliminar(Nothing, Nothing)
            Case Keys.F5
                btnBuscar_Click(Nothing, Nothing)
            Case Keys.Escape
                pSalir(Nothing, Nothing)
        End Select
    End Sub

    Private Sub frmMantUsuarios_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Me.Top = 0 : Me.Left = 0

        grb.Text = "Servicio: " & strDescServicio

        CargarSeguridad()
        ActivarDesactivarBotones()
        dgv.ColumnHeadersDefaultCellStyle.Font = New Font(dgv.Font, FontStyle.Bold)
        dgv.DefaultCellStyle.Font = New Font(dgv.Font, FontStyle.Regular)

        txtAnio.Text = Today.Year

        If strIDTipoProv = gstrTipoProveeHoteles Then
            dgv.Columns("Monto_sgl").HeaderText = "Monto Habitación Simple $"
        Else
            dgv.Columns("Monto_sgl").HeaderText = "Monto $"
        End If

    End Sub
    Private Sub pSalir(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Me.Close()
        'frmMain.DesabledBtns()
    End Sub

    Private Sub pEditar(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dgv.DoubleClick
        If dgv.CurrentCell Is Nothing Then Exit Sub
        Try
            Editar()
        Catch ex As Exception
            MessageBox.Show(ex.Message, ex.Source, MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub
    Private Sub Editar()
        Try
            Dim frm As New frmMantDetServiciosProveedorDato
            With frm
                .strIDServicio_Det = dgv.Item("IDServicio_Det", dgv.CurrentCell.RowIndex).Value.ToString
                .strDescripcion = dgv.Item("Descripcion", dgv.CurrentCell.RowIndex).Value.ToString
                .strIDServicio = strIDServicio
                .strDescServicio = strDescServicio
                .strIDTipoProv = strIDTipoProv

                .MdiParent = frmMain
                .FormRef = Me
                .Show()
            End With
        Catch ex As Exception
            Throw
        End Try
    End Sub
    Private Sub pNuevo(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim frm As New frmMantDetServiciosProveedorDato
        With frm
            .strIDServicio = strIDServicio
            .strIDTipoProv = strIDTipoProv
            .strDescServicio = strDescServicio
            .MdiParent = frmMain
            .FormRef = Me
            .Show()
            'Buscar()

        End With
    End Sub
    Public Sub Buscar()
        Try

            dgv.DataSource = objBN.ConsultarList(strIDServicio, txtAnio.Text, txtDescripcion.Text)
            pDgvAnchoColumnas(dgv)

        Catch ex As Exception
            Throw
        End Try
    End Sub
    Private Sub pEliminar(ByVal sender As System.Object, ByVal e As System.EventArgs)
        If dgv.CurrentCell Is Nothing Then Exit Sub
        Try

            If MessageBox.Show("¿Está Ud. seguro de eliminar el Detalle de Servicio " & dgv.Item("Descripcion", dgv.CurrentCell.RowIndex).Value.ToString & "?", _
                               gstrTituloMsg, MessageBoxButtons.YesNo, MessageBoxIcon.Question) = Windows.Forms.DialogResult.No Then Exit Sub

            Dim objBT As New clsServicioProveedorBT.clsDetalleServicioProveedorBT
            objBT.Eliminar(dgv.Item("IDServicio_Det", dgv.CurrentCell.RowIndex).Value.ToString)

            Buscar()
            dgv_Click(Nothing, Nothing)
        Catch ex As Exception
            MessageBox.Show(ex.Message, ex.Source, MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub dgv_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgv.CellContentClick
        If dgv.CurrentCell.ColumnIndex = dgv.Columns("btnEdit").Index Then
            Editar()
        ElseIf dgv.CurrentCell.ColumnIndex = dgv.Columns("btnDel").Index Then
            pEliminar(Nothing, Nothing)
        End If
    End Sub

    Private Sub dgv_CellPainting(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellPaintingEventArgs) Handles dgv.CellPainting
        If e.ColumnIndex = -1 Then Exit Sub
        If e.RowIndex = -1 Then Exit Sub
        If dgv.Columns(e.ColumnIndex).Name = "btnDel" Then
            e.Paint(e.CellBounds, DataGridViewPaintParts.All)
            Dim celBoton As DataGridViewButtonCell = CType(dgv.Rows(e.RowIndex).Cells("btnDel"), DataGridViewButtonCell)
            Dim icoAtomico As Image = My.Resources.Resources.eliminarpng
            e.Graphics.DrawImage(icoAtomico, e.CellBounds.Left + 3, e.CellBounds.Top + 3)
            dgv.Rows(e.RowIndex).Height = 21
            dgv.Columns(e.ColumnIndex).Width = icoAtomico.Width + 5
            e.Handled = True
        End If

        If dgv.Columns(e.ColumnIndex).Name = "btnEdit" Then
            e.Paint(e.CellBounds, DataGridViewPaintParts.All)
            Dim celBoton As DataGridViewButtonCell = CType(dgv.Rows(e.RowIndex).Cells("btnEdit"), DataGridViewButtonCell)
            Dim icoAtomico As Image = My.Resources.Resources.editarpng
            e.Graphics.DrawImage(icoAtomico, e.CellBounds.Left + 3, e.CellBounds.Top + 3)
            dgv.Rows(e.RowIndex).Height = 21
            dgv.Columns(e.ColumnIndex).Width = icoAtomico.Width + 5
            e.Handled = True
        End If

    End Sub

    Public Sub dgv_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles dgv.Click
        If dgv.RowCount > 0 Then

            blnEditarEnabled = True
            blnEliminarEnabled = True
            ActivarDesactivarBotones()

        Else
            blnEditarEnabled = False
            blnEliminarEnabled = False
            ActivarDesactivarBotones()
        End If


    End Sub

    Private Sub txtAnio_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtAnio.KeyPress
        If Asc(e.KeyChar) <> 8 Then
            If Not IsNumeric(e.KeyChar) Then
                e.KeyChar = ""
            End If
        End If

    End Sub

End Class