﻿Imports ComSEToursBL2
Public Class frmIngPedido
    Dim blnNuevoEnabled As Boolean = True
    Dim blnGrabarEnabled As Boolean = False
    Dim blnDeshacerEnabled As Boolean = False
    Dim blnEditarEnabled As Boolean = False
    Dim blnEliminarEnabled As Boolean = False
    Dim blnSalirEnabled As Boolean = True
    Dim blnImprimirEnabled As Boolean = False
    Dim blnConsultarEnabledBD As Boolean = False
    Dim blnGrabarEnabledBD As Boolean = False
    Dim blnEditarEnabledBD As Boolean = False
    Dim blnEliminarEnabledBD As Boolean = False
    Dim dtUsuarios_Combo As DataTable
    Dim blnEditarCotizac As Boolean = False

    Private Sub frmIngPedido_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Activated
        'AddHandler frmMain.btnSalir.Click, AddressOf pSalir
        'frmMain.btnSalir.Enabled = blnSalirEnabled
        'AddHandler frmMain.btnNuevo.Click, AddressOf pNuevo
        'frmMain.btnNuevo.Enabled = blnNuevoEnabled
        'AddHandler frmMain.btnEliminar.Click, AddressOf pEliminar
        'frmMain.btnEliminar.Enabled = blnEliminarEnabled
        'AddHandler frmMain.btnEditar.Click, AddressOf pEditar
        'frmMain.btnEditar.Enabled = blnEditarEnabled

        'frmMain.btnDeshacer.Enabled = blnDeshacerEnabled
        'frmMain.btnGrabar.Enabled = blnGrabarEnabled

        'frmMain.btnImprimir.Enabled = blnImprimirEnabled
        ''AddHandler frmMain.btnImprimir.Click, AddressOf pImprimir
        pCambiarTitulosToolBar(Me)
    End Sub

    Private Sub frmIngPedido_Deactivate(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Deactivate
        'RemoveHandler frmMain.btnSalir.Click, AddressOf pSalir
        'RemoveHandler frmMain.btnNuevo.Click, AddressOf pNuevo
        'RemoveHandler frmMain.btnEliminar.Click, AddressOf pEliminar
        'RemoveHandler frmMain.btnEditar.Click, AddressOf pEditar
        'RemoveHandler frmMain.btnImprimir.Click, AddressOf pImprimir
        pCambiarTitulosToolBar(Me, True)
    End Sub
    Private Sub btnBuscar_Click(sender As Object, e As EventArgs) Handles btnBuscar.Click, txtCliente.TextChanged, txtTxTitulo.TextChanged, _
        txtNuCotizacion.TextChanged, cboCoEjeVta.SelectionChangeCommitted, chkFilesArchivados.CheckedChanged, _
        txtNuFile.TextChanged

        Try
            Buscar()
            dgv_Click(Nothing, Nothing)
            CargarDgvColumnasTm()

        Catch ex As Exception
            MessageBox.Show(ex.Message, ex.Source, MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub CargarDgvColumnasTm()
        Try
            dgv.Columns("NuPedido").Width = 70
            dgv.Columns("FePedido").Width = 70
            dgv.Columns("RazonComercial").Width = 200
            dgv.Columns("TxTitulo").Width = 300

            dgvDet.Columns("Fecha").Width = 70
            dgvDet.Columns("NuFile").Width = 70
            dgvDet.Columns("Cotizacion").Width = 70
            dgvDet.Columns("Titulo").Width = 300
            dgvDet.Columns("Ruta").Width = 130
        Catch ex As Exception
            Throw
        End Try
    End Sub


    Private Sub pSalir(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Me.Close()
    End Sub

    Private Sub dgv_CellEnter(sender As Object, e As DataGridViewCellEventArgs) Handles dgv.CellEnter

        Try
            Dim intNuPedInt As Integer = 0
            If Not dgv.CurrentRow Is Nothing Then
                intNuPedInt = dgv.CurrentRow.Cells("NuPedInt").Value
            End If
            Dim objBN As New clsPedidoBN
            dgvDet.DataSource = objBN.ConsultarCotizaciones(intNuPedInt)
            pDgvAnchoColumnas(dgvDet)
            CargarDgvColumnasTm()

        Catch ex As Exception
            MessageBox.Show(ex.Message, ex.Source, MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub dgv_CellPainting(sender As Object, e As DataGridViewCellPaintingEventArgs) Handles dgv.CellPainting
        If e.ColumnIndex = -1 Then Exit Sub
        If e.RowIndex = -1 Then Exit Sub

        If dgv.Columns(e.ColumnIndex).Name = "btnEdit" Then
            e.Paint(e.CellBounds, DataGridViewPaintParts.All)
            Dim celBoton As DataGridViewButtonCell = CType(dgv.Rows(e.RowIndex).Cells("btnEdit"), DataGridViewButtonCell)
            Dim icoAtomico As Image = Nothing
            icoAtomico = My.Resources.Resources.zoom
            If (blnEditarEnabledBD Or blnGrabarEnabledBD) Then
                icoAtomico = My.Resources.Resources.editarpng
            End If
            e.Graphics.DrawImage(icoAtomico, e.CellBounds.Left + 3, e.CellBounds.Top + 3)
            dgv.Rows(e.RowIndex).Height = 21
            dgv.Columns(e.ColumnIndex).Width = icoAtomico.Width + 5
            e.Handled = True
        End If

    End Sub
    Private Sub pEditar(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dgv.DoubleClick

        Try
            Dim frm As New frmIngPedidoDato

            With frm
                .intNuPedInt = dgv.CurrentRow.Cells("NuPedInt").Value
                .strNuPedido = dgv.CurrentRow.Cells("NuPedido").Value
                .blnEditarCotizac = blnEditarCotizac
                .MdiParent = frmMenuTesting
                .FormRef = Me
                .WindowState = FormWindowState.Maximized
                .Show()

            End With
        Catch ex As Exception
            MessageBox.Show(ex.Message, ex.Source, MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub pEliminar(ByVal sender As System.Object, ByVal e As System.EventArgs)
        If dgv.CurrentCell Is Nothing Then Exit Sub
        Try

            If MessageBox.Show("¿Está Ud. seguro de anular el Pedido " & dgv.Item("NuPedido", dgv.CurrentCell.RowIndex).Value.ToString & "?", _
                               gstrTituloMsg, MessageBoxButtons.YesNo, MessageBoxIcon.Question) = Windows.Forms.DialogResult.No Then Exit Sub

            'Dim objBT As New clsCotizacionBT
            'objBT.Anular(dgv.Item("IDCab", dgv.CurrentCell.RowIndex).Value.ToString, gstrUser)

            MessageBox.Show("Se anuló el Pedido " & dgv.Item("NuPedido", dgv.CurrentCell.RowIndex).Value.ToString & " exitosamente", gstrTituloMsg, MessageBoxButtons.OK, MessageBoxIcon.Information)

            Buscar()
            dgv_Click(Nothing, Nothing)
        Catch ex As Exception
            MessageBox.Show(ex.Message, ex.Source, MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub
    Public Sub dgv_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles dgv.Click
        If dgv.RowCount > 0 Then
            blnEditarEnabled = True
            'blnEliminarEnabled = True
            ActivarDesactivarBotones()

        Else
            blnEditarEnabled = False
            blnEliminarEnabled = False
            ActivarDesactivarBotones()
        End If

    End Sub
    Private Sub pNuevo(ByVal sender As System.Object, ByVal e As System.EventArgs)
        'Dim frm As New frmIngPedidoDato
        'With frm
        '    .MdiParent = frmMain
        '    .blnEditarCotizac = blnEditarCotizac
        '    .FormRef = Me
        '    .WindowState = FormWindowState.Maximized
        '    .Show()

        'End With
    End Sub

    Public Sub Buscar()
        Try

            Dim intNuPedInt As Integer = 0
            If Not dgv.CurrentRow Is Nothing Then
                intNuPedInt = dgv.CurrentRow.Cells("NuPedInt").Value
            End If

            Dim objBN As New clsPedidoBN
            dgv.DataSource = objBN.ConsultarList(If(cboCoEjeVta.Text = "", "", cboCoEjeVta.SelectedValue.ToString), txtNuPedido.Text, _
                                                 txtNuFile.Text, txtNuCotizacion.Text, txtTxTitulo.Text, txtCliente.Text, _
                                                 chkFilesArchivados.Checked)
            AlinearFilaGrid(dgv, "NuPedInt", intNuPedInt, dgv.Columns("EjecutivoVtas").Index)
            pDgvAnchoColumnas(dgv)

            dgv_CellEnter(Nothing, Nothing)
        Catch ex As Exception
            Throw
        End Try
    End Sub
    Private Sub ActivarDesactivarBotones()

        ''frmMain.btnNuevo.Enabled = blnNuevoEnabled
        'If blnNuevoEnabled = True Then
        '    If blnGrabarEnabledBD Then
        '        frmMain.btnNuevo.Enabled = blnNuevoEnabled
        '    Else
        '        frmMain.btnNuevo.Enabled = False
        '        blnNuevoEnabled = blnGrabarEnabledBD
        '    End If
        'Else
        '    frmMain.btnNuevo.Enabled = blnNuevoEnabled
        'End If

        ''frmMain.btnGrabar.Enabled = blnGrabarEnabled
        'If blnGrabarEnabled = True Then
        '    If blnGrabarEnabledBD Then
        '        frmMain.btnGrabar.Enabled = blnGrabarEnabled
        '    Else
        '        frmMain.btnGrabar.Enabled = False
        '        blnGrabarEnabled = blnGrabarEnabledBD
        '    End If
        'Else
        '    frmMain.btnGrabar.Enabled = blnGrabarEnabled
        'End If

        'frmMain.btnDeshacer.Enabled = blnDeshacerEnabled

        ''frmMain.btnEditar.Enabled = blnEditarEnabled
        'If blnEditarEnabled = True Then
        '    If blnEditarEnabledBD Then
        '        frmMain.btnEditar.Enabled = blnEditarEnabled
        '    Else
        '        frmMain.btnEditar.Enabled = False
        '        blnEditarEnabled = blnEditarEnabledBD
        '    End If
        'Else
        '    frmMain.btnEditar.Enabled = blnEditarEnabled
        'End If

        ''frmMain.btnEliminar.Enabled = blnEliminarEnabled
        ''If blnEliminarEnabled = True Then
        ''    If blnEliminarEnabledBD Then
        ''        frmMain.btnEliminar.Enabled = blnEliminarEnabled
        ''    Else
        ''        frmMain.btnEliminar.Enabled = False
        ''        blnEliminarEnabled = blnEliminarEnabledBD
        ''    End If
        ''Else
        ''    frmMain.btnEliminar.Enabled = blnEliminarEnabled
        ''End If

        ''If chrModulo = "C" Then
        '' If Not dgv.Columns("btnDel") Is Nothing Then dgv.Columns("btnDel").Visible = blnEliminarEnabled
        ''End If

        'frmMain.btnImprimir.Enabled = blnImprimirEnabled
        'frmMain.btnSalir.Enabled = blnSalirEnabled

    End Sub
    Private Sub CargarSeguridad()
        Try
            Dim strNombreForm As String = Me.Name

            strNombreForm = Replace(strNombreForm, "frm", "mnu")

            Using objSeg As New clsSeguridadBN.clsOpcionesBN.clsUsuarioBN
                Using dr As SqlClient.SqlDataReader = objSeg.ConsultarOpcionesForm(gstrUser, strNombreForm)
                    If dr.HasRows Then
                        dr.Read()
                        blnGrabarEnabledBD = dr("Grabar")
                        blnEditarEnabledBD = dr("Actualizar")
                        blnEliminarEnabledBD = dr("Eliminar")
                        blnConsultarEnabledBD = dr("Consultar")
                    End If
                    dr.Close()
                End Using

                Using dr As SqlClient.SqlDataReader = objSeg.ConsultarOpcionesForm(gstrUser, "mnuIngCotizacion")
                    If dr.HasRows Then
                        dr.Read()
                        blnEditarCotizac = dr("Grabar") Or dr("Actualizar")
                    End If
                    dr.Close()
                End Using

            End Using


        Catch ex As Exception
            Throw
        End Try
    End Sub
    Private Sub CargarCombos()
        Try
            CargarCombo_Usuario_Activos()

        Catch ex As Exception
            Throw
        End Try
    End Sub
    Private Sub CargarCombo_Usuario_Activos()
        Try
            Dim strActivos As String = ""
            If chkActivos.Checked Then
                strActivos = "A"
            End If
            Dim strNombreForm As String = Me.Name
            strNombreForm = Replace(strNombreForm, "frm", "mnu")
            strNombreForm = Replace(strNombreForm, "Dato", "")
            Dim objBLUser As New clsUsuarioBN
            Dim dtCombo As DataTable = objBLUser.ConsultarCboxOpcion_Activos(strNombreForm, True, strActivos)
            dtUsuarios_Combo = New DataTable
            dtUsuarios_Combo = dtCombo.Clone
            For Each row As DataRow In dtCombo.Rows
                dtUsuarios_Combo.ImportRow(row)
            Next

            pCargaCombosBox(cboCoEjeVta, dtCombo, True)
        Catch ex As Exception
            Throw
        End Try
    End Sub

    Private Sub frmIngPedido_FormClosed(sender As Object, e As FormClosedEventArgs) Handles Me.FormClosed
        'frmMain.DesabledBtns()
    End Sub

    Private Sub frmIngPedido_KeyDown(sender As Object, e As KeyEventArgs) Handles Me.KeyDown
        'Select Case e.KeyCode
        '    Case Keys.F2
        '        If frmMain.btnNuevo.Enabled Then pNuevo(Nothing, Nothing)
        '    Case Keys.F3
        '        If frmMain.btnEditar.Enabled Then pEditar(Nothing, Nothing)
        '    Case Keys.F4
        '        If frmMain.btnEliminar.Enabled Then pEliminar(Nothing, Nothing)
        '    Case Keys.F5
        '        btnBuscar_Click(btnBuscar, Nothing)
        '    Case Keys.F9
        '        'If frmMain.btnImprimir.Enabled Then pImprimir(Nothing, Nothing)
        '    Case Keys.Escape
        '        pSalir(Nothing, Nothing)
        'End Select
    End Sub

    Private Sub frmIngPedido_Load(sender As Object, e As EventArgs) Handles Me.Load

        Me.Top = 0 : Me.Left = 0
        Try
            Me.WindowState = FormWindowState.Maximized
            cboCoEjeVta.DrawMode = DrawMode.OwnerDrawFixed
            CargarCombos()
            chkActivos.Checked = True
            cboCoEjeVta.SelectedValue = gstrUser

            CargarSeguridad()
            ActivarDesactivarBotones()

            dgv.ColumnHeadersDefaultCellStyle.Font = New Font(dgv.Font, FontStyle.Bold)
            dgv.DefaultCellStyle.SelectionBackColor = gColorSelectGrid
            dgv.DefaultCellStyle.SelectionForeColor = System.Drawing.Color.Black

            dgvDet.ColumnHeadersDefaultCellStyle.Font = New Font(dgvDet.Font, FontStyle.Bold)
            dgvDet.DefaultCellStyle.SelectionBackColor = gColorSelectGrid
            dgvDet.DefaultCellStyle.SelectionForeColor = System.Drawing.Color.Black
        Catch ex As Exception
            MessageBox.Show(ex.Message, ex.Source, MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub chkActivos_CheckedChanged(sender As Object, e As EventArgs) Handles chkActivos.CheckedChanged

        Try
            CargarCombo_Usuario_Activos()
        Catch ex As Exception
            MessageBox.Show(ex.Message, ex.Source, MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub
    Private Sub cboCoEjeVta_DrawItem(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DrawItemEventArgs) Handles cboCoEjeVta.DrawItem
        Try
            pColorear_ComboBox_Usuarios(cboCoEjeVta, dtUsuarios_Combo, sender, e)

        Catch ex As Exception
            Throw
        End Try
    End Sub
    Private Sub dgv_CellContentClick(sender As Object, e As DataGridViewCellEventArgs) Handles dgv.CellContentClick

        If dgv.CurrentCell Is Nothing Then Exit Sub
        If dgv.CurrentCell.ColumnIndex = dgv.Columns("btnEdit").Index Then
            pEditar(Nothing, Nothing)
        End If

    End Sub

    Private Sub dgvDet_CellPainting(sender As Object, e As DataGridViewCellPaintingEventArgs) Handles dgvDet.CellPainting
        If e.ColumnIndex = -1 Then Exit Sub
        If e.RowIndex = -1 Then Exit Sub

        If dgvDet.Columns(e.ColumnIndex).Name = "btnEditDet" Then
            e.Paint(e.CellBounds, DataGridViewPaintParts.All)
            Dim celBoton As DataGridViewButtonCell = CType(dgvDet.Rows(e.RowIndex).Cells("btnEditDet"), DataGridViewButtonCell)
            Dim icoAtomico As Image = Nothing
            icoAtomico = My.Resources.Resources.zoom
            If blnEditarCotizac Then
                icoAtomico = My.Resources.Resources.editarpng
            End If
            e.Graphics.DrawImage(icoAtomico, e.CellBounds.Left + 3, e.CellBounds.Top + 3)
            dgvDet.Rows(e.RowIndex).Height = 21
            dgvDet.Columns(e.ColumnIndex).Width = icoAtomico.Width + 5
            e.Handled = True
        End If
    End Sub
    Private Sub EditarCotiz()

        'Try
        '    Dim frm As frmIngCotizacionDato

        '    frm = New frmIngCotizacionDato
        '    With frm
        '        .intIDCab = dgvDet.CurrentRow.Cells("IDCab").Value.ToString
        '        .strCotizacion = dgvDet.CurrentRow.Cells("Cotizacion").Value.ToString
        '        '.strCorreoReservas = dgvFila.Cells("CorreoEjecReservas").Value.ToString
        '        .MdiParent = frmMain
        '        .FormRefPedidoCons = Me
        '        '.FormRefPedido.WindowState = FormWindowState.Maximized
        '        .Show()

        '        If blnPantallaFileAbierto(.Name, .intIDCab) Then
        '            .Close()
        '        End If

        '    End With

        'Catch ex As Exception
        '    Throw
        'End Try
    End Sub
    Private Sub dgvDet_CellContentClick(sender As Object, e As DataGridViewCellEventArgs) Handles dgvDet.CellContentClick
        If dgvDet.CurrentCell Is Nothing Then Exit Sub

        Try
            If dgvDet.CurrentCell.ColumnIndex = dgvDet.Columns("btnEditDet").Index Then
                EditarCotiz()
            End If

        Catch ex As Exception
            MessageBox.Show(ex.Message, ex.Source, MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub dgvDet_DoubleClick(sender As Object, e As EventArgs) Handles dgvDet.DoubleClick
        If dgvDet.CurrentCell Is Nothing Then Exit Sub
        Try
            EditarCotiz()
        Catch ex As Exception
            MessageBox.Show(ex.Message, ex.Source, MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub
End Class