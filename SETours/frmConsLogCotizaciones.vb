﻿Imports ComSEToursBL2
Imports ComSEToursBE2
Public Class frmConsLogCotizaciones
    Public intIDCab As Integer
    Public strIDProveedor As String
    Public strDescProveedor As String
    Public chrModulo As Char
    Public chrAccion As Char
    Public strIDFile As String
    'Public strServicio As String

    Public strIDReserva As String = String.Empty

    Public strIDProveedorProtecc As String
    Public intIDReservaProtecc As Integer

    Public strIDOperacion As String
    Public intIDServicio_Det As Integer
    Public blnDgvTipoProvSelHotel As Boolean
    Public frmRef As Object 'frmIngReservasControlporFileDato
    Public frmRefProg As Object 'frmIngOperacionProgramacion

    Public frmRefCoti As frmIngCotizacionDato

    Public strTitulo As String
    Public intNroPax As Int16
    Public strIDEstadoLog As String

    Public blnEnvioCorreo As Boolean

    Dim blnNuevoEnabled As Boolean = False
    Dim blnGrabarEnabled As Boolean = False
    Dim blnDeshacerEnabled As Boolean = False
    Dim blnEditarEnabled As Boolean = False
    Dim blnEliminarEnabled As Boolean = False
    Dim blnSalirEnabled As Boolean = True
    Dim blnImprimirEnabled As Boolean = False
    Dim blnGrabarEnabledBD As Boolean = False
    Dim blnEditarEnabledBD As Boolean = False
    Dim blnEliminarEnabledBD As Boolean = False

    Public blnEsAlojamiento As Boolean = False
    'Public strIDTipoOper As String = String.Empty
    'Public strIDTipoProv As String = String.Empty
    'Public blnConAlojamiento As Boolean = False
    Public blnSoloConsulta As Boolean = False
    Dim blnCambios As Boolean = False
    Dim blnAnulacionComRes As Boolean = False
    Dim frmmain As Object = Nothing

    Private Sub frmConsLogCotizaciones_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        If Not blnCambios Then
            If MessageBox.Show("No se han grabado los cambios. ¿Está Ud. seguro de salir?", "Confirmar", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = Windows.Forms.DialogResult.Yes Then

                e.Cancel = False
            Else
                e.Cancel = True
            End If

        End If
    End Sub

    Private Sub frmConsLogCotizaciones_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            dgvDetOriginal.ColumnHeadersDefaultCellStyle.Font = New Font(dgvDetOriginal.Font, FontStyle.Bold)
            dgvDetModificado.ColumnHeadersDefaultCellStyle.Font = New Font(dgvDetModificado.Font, FontStyle.Bold)
            dgvDetOriginal.DefaultCellStyle.SelectionForeColor = System.Drawing.Color.Black
            dgvDetOriginal.DefaultCellStyle.SelectionBackColor = gColorSelectGrid
            dgvDetModificado.DefaultCellStyle.SelectionForeColor = System.Drawing.Color.Black
            dgvDetModificado.DefaultCellStyle.SelectionBackColor = gColorSelectGrid

            dgvAntes.DefaultCellStyle.SelectionForeColor = System.Drawing.Color.Black
            dgvAntes.DefaultCellStyle.SelectionBackColor = gColorSelectGrid
            dgvDespues.DefaultCellStyle.SelectionForeColor = System.Drawing.Color.Black
            dgvDespues.DefaultCellStyle.SelectionBackColor = gColorSelectGrid

            Me.Cursor = Cursors.WaitCursor
            If chrModulo = "R" Then
                Me.Text = "Consulta de Cambios en Detalle de Cotizaciones del File Nro. " & strIDFile & " Proveedor: " & strDescProveedor
                'BuscarLogCoti()
                dgvAntes.Columns("NoEstadoTarifa").Visible = False
                CargarAntes()

                If strIDEstadoLog = "M" Then
                    Dim objResBT As New clsReservaBT
                    If intIDReservaProtecc = 0 Then
                        objResBT.RegenerarReserva_TMP(strIDReserva, strIDProveedor, blnEsAlojamiento, intIDCab, 0, gsglIGV, gstrUser)
                    Else
                        objResBT.RegenerarReserva_TMP(strIDReserva, strIDProveedorProtecc, blnEsAlojamiento, intIDCab, intIDReservaProtecc, gsglIGV, gstrUser)
                    End If

                    CargarDespues()
                    EdicionColumnaFlagOMEN()
                    CargarCambiosDetallePaxReservas()
                    CambiosPorFechaHora()
                End If
            ElseIf chrModulo = "O" Then
                Me.Text = "Consulta de Cambios en Detalle de Reservas del File Nro. " & strIDFile & " Proveedor: " & strDescProveedor
                'BuscarLogReserv()

                CargarAntesOperaciones()

                If strIDEstadoLog = "M" Then
                    Dim objResBT As New clsOperacionBT
                    objResBT.RegenerarOperacion_TMP(strIDOperacion, strIDProveedor, intIDCab, gstrUser)

                    CargarDespuesOperaciones()
                    EdicionColumnaFlagOMEN()
                    CambiosPorFechaHora()
                End If
            End If
            ActualizarFlagViaticos()

            'ActivarDesactivarBotones()
            If blnSoloConsulta Then CargarblnConsulta(btnAceptar, grb, blnSoloConsulta)
            If Not blnSoloConsulta Then EnabledAceptar()

            Me.Cursor = Cursors.Default
        Catch ex As Exception
            Me.Cursor = Cursors.Default
            MessageBox.Show(ex.Message, "Error en " & Err.Source, MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Function blnExisteDetalleAnexadoOper(ByVal intIDReserva_Det As Integer, ByRef rdatFecha As Date, ByRef rstrHora As String, _
                                             ByRef intIndexMod As Integer) As Boolean
        Try
            For Each ItemDgv As DataGridViewRow In dgvDespues.Rows
                Dim LocalintIDReserva_Det As Integer = If(Not IsNothing(ItemDgv.Cells("IDReserva_Det2").Value), ItemDgv.Cells("IDReserva_Det2").Value, 0)

                If LocalintIDReserva_Det = intIDReserva_Det Then
                    rdatFecha = ItemDgv.Cells("Dia2").Value
                    rstrHora = ItemDgv.Cells("Hora2").Value
                    intIndexMod = ItemDgv.Index
                    Return True
                End If
            Next

            Return False
        Catch ex As Exception
            Throw
        End Try
    End Function

    Private Function blnExisteDetalleAnexadoRes(ByVal vintIDDet As Integer, ByVal vintIDServicio_Det As Integer, ByRef rdatFecha As Date, _
                                                ByRef rstrHora As String, ByRef intIndexMod As Integer) As Boolean
        Try
            For Each ItemDgv As DataGridViewRow In dgvDespues.Rows
                Dim LocalintIDDet As Integer = If(Not IsNothing(ItemDgv.Cells("IDDet2").Value), ItemDgv.Cells("IDDet2").Value, 0)
                Dim LocalintIDServicio_Det As Integer = If(Not IsNothing(ItemDgv.Cells("IDServicio_Det2").Value), ItemDgv.Cells("IDServicio_Det2").Value, 0)

                If LocalintIDDet = vintIDDet And LocalintIDServicio_Det = vintIDServicio_Det Then
                    rdatFecha = ItemDgv.Cells("Dia2").Value
                    rstrHora = ItemDgv.Cells("Hora2").Value
                    intIndexMod = ItemDgv.Index
                    Return True
                End If
            Next

            Return False
        Catch ex As Exception
            Throw
        End Try
    End Function

    Private Sub CambiosPorFechaHora()
        Try
            If chrModulo = "O" Then
                For Each dgvItem As DataGridViewRow In dgvAntes.Rows
                    Dim LocalintIDReserva_Det As Integer = If(Not IsNothing(dgvItem.Cells("IDReserva_Det").Value), dgvItem.Cells("IDReserva_Det").Value, 0)
                    If LocalintIDReserva_Det <> 0 Then
                        Dim datFecha2 As Date = #1/1/1900#
                        Dim strHora2 As String = String.Empty

                        Dim datFecha1 As Date = dgvItem.Cells("Dia").Value
                        Dim strHora1 As String = dgvItem.Cells("Hora").Value
                        Dim intIndexDgv2 As Integer = -1

                        If blnExisteDetalleAnexadoOper(LocalintIDReserva_Det, datFecha2, strHora2, intIndexDgv2) Then
                            Dim blnChanged As Boolean = False
                            If datFecha1.ToShortDateString() <> datFecha2.ToShortDateString() Then
                                dgvItem.Cells("DiaFormat").Style.BackColor = Color.Pink
                                dgvItem.Cells("DiaFormat").Style.SelectionBackColor = Color.Pink
                                dgvDespues.Rows(intIndexDgv2).Cells("DiaFormat2").Style.BackColor = Color.Pink
                                dgvDespues.Rows(intIndexDgv2).Cells("DiaFormat2").Style.SelectionBackColor = Color.Pink
                                blnChanged = True
                            End If

                            If strHora1 <> strHora2 Then
                                dgvItem.Cells("Hora").Style.BackColor = Color.Pink
                                dgvItem.Cells("Hora").Style.SelectionBackColor = Color.Pink
                                dgvDespues.Rows(intIndexDgv2).Cells("Hora2").Style.BackColor = Color.Pink
                                dgvDespues.Rows(intIndexDgv2).Cells("Hora2").Style.SelectionBackColor = Color.Pink
                                blnChanged = True
                            End If

                            If blnChanged Then
                                If IsNothing(dgvItem.Cells("FlagOMEN").Value) Then
                                    If String.IsNullOrEmpty(dgvItem.Cells("FlagOMEN").Value) Then
                                        dgvItem.Cells("FlagOMEN").Style.BackColor = Color.Pink
                                        dgvItem.Cells("FlagOMEN").Style.SelectionBackColor = Color.Pink
                                        dgvDespues.Rows(intIndexDgv2).Cells("FlagOMEN2").Style.BackColor = Color.Pink
                                        dgvDespues.Rows(intIndexDgv2).Cells("FlagOMEN2").Style.SelectionBackColor = Color.Pink

                                        dgvItem.Cells("FlagOMEN").Value = "Actualizar"
                                        dgvDespues.Rows(intIndexDgv2).Cells("FlagOMEN2").Value = "Actualizar"
                                    End If
                                End If
                            End If
                        End If
                    End If
                Next
            ElseIf chrModulo = "R" Then
                For Each dgvItem As DataGridViewRow In dgvAntes.Rows
                    Dim localintIDDet As Integer = If(Not IsNothing(dgvItem.Cells("IDDet").Value), dgvItem.Cells("IDDet").Value, 0)
                    Dim localintIDServicio_Det As Integer = If(Not IsNothing(dgvItem.Cells("IDServicio_Det").Value), dgvItem.Cells("IDServicio_Det").Value, 0)

                    If localintIDDet <> 0 And localintIDServicio_Det Then
                        Dim datFecha2 As Date = #1/1/1900#
                        Dim strHora2 As String = String.Empty

                        Dim datFecha1 As Date = dgvItem.Cells("Dia").Value
                        Dim strHora1 As String = dgvItem.Cells("Hora").Value
                        Dim intIndexDgv2 As Integer = -1

                        If blnExisteDetalleAnexadoRes(localintIDDet, localintIDServicio_Det, datFecha2, strHora2, intIndexDgv2) Then
                            Dim blnChanged As Boolean = False
                            If datFecha1.ToShortDateString() <> datFecha2.ToShortDateString() Then
                                dgvItem.Cells("DiaFormat").Style.BackColor = Color.Pink
                                dgvItem.Cells("DiaFormat").Style.SelectionBackColor = Color.Pink
                                dgvDespues.Rows(intIndexDgv2).Cells("DiaFormat2").Style.BackColor = Color.Pink
                                dgvDespues.Rows(intIndexDgv2).Cells("DiaFormat2").Style.SelectionBackColor = Color.Pink
                                blnChanged = True
                            End If

                            If strHora1 <> strHora2 Then
                                dgvItem.Cells("Hora").Style.BackColor = Color.Pink
                                dgvItem.Cells("Hora").Style.SelectionBackColor = Color.Pink
                                dgvDespues.Rows(intIndexDgv2).Cells("Hora2").Style.BackColor = Color.Pink
                                dgvDespues.Rows(intIndexDgv2).Cells("Hora2").Style.SelectionBackColor = Color.Pink
                                blnChanged = True
                            End If

                            If blnChanged Then
                                If IsNothing(dgvItem.Cells("FlagOMEN").Value) Then
                                    If String.IsNullOrEmpty(dgvItem.Cells("FlagOMEN").Value) Then
                                        dgvItem.Cells("FlagOMEN").Style.BackColor = Color.Pink
                                        dgvItem.Cells("FlagOMEN").Style.SelectionBackColor = Color.Pink
                                        dgvDespues.Rows(intIndexDgv2).Cells("FlagOMEN2").Style.BackColor = Color.Pink
                                        dgvDespues.Rows(intIndexDgv2).Cells("FlagOMEN2").Style.SelectionBackColor = Color.Pink

                                        dgvItem.Cells("FlagOMEN").Value = "Actualizar"
                                        dgvDespues.Rows(intIndexDgv2).Cells("FlagOMEN2").Value = "Actualizar"
                                    End If
                                End If
                            End If
                        End If
                    End If
                Next
            End If

        Catch ex As Exception
            Throw
        End Try
    End Sub

    Private Sub CargarCambiosDetallePaxReservas()
        If strIDReserva = String.Empty Then Exit Sub
        Try
            Dim objBLRes As New clsReservaBN()
            If objBLRes.blnExisteCambioPaxNivelDetalle(strIDReserva) Then
                'ToolTip1.SetToolTip(btnModificarReservas, "Se han realizado cambios por pax en algun detalle.")
                Dim blnExisteOtraAccion As Boolean = False
                For Each ItemDgv As DataGridViewRow In dgvDespues.Rows
                    Dim strAccion As String = If(IsNothing(ItemDgv.Cells("FlagOMEN2").Value), "", ItemDgv.Cells("FlagOMEN2").Value.ToString())
                    If strAccion <> String.Empty Then
                        blnExisteOtraAccion = True
                        Exit For
                    End If
                Next

                If Not blnExisteOtraAccion Then
                    lblTituloDespues.Text = lblTituloDespues.Text.Replace("...", " (Hay cambios por detalle de pasajeros)...")
                End If
            End If
        Catch ex As Exception
            Throw
        End Try
    End Sub

    Private Sub EnabledAceptar()
        Try
            btnAceptar.Enabled = True
            'If dgvDetModificado.Rows.Count > 0 Then
            If dgvDespues.Rows.Count > 0 Then
                'btnAceptar.Enabled = True
            Else
                blnCambios = True
            End If
        Catch ex As Exception
            Throw
        End Try
    End Sub

    Private Sub ColorearNuevoModificadoEliminado()
        Try
            For Each DgvRow As DataGridViewRow In dgvDetOriginal.Rows
                DgvRow.DefaultCellStyle.ForeColor = Color.Black
                DgvRow.DefaultCellStyle.SelectionForeColor = Color.Black
                If Convert.ToBoolean(DgvRow.Cells("AtendidoOrig").Value) = True Then
                    DgvRow.DefaultCellStyle.ForeColor = Color.Gray
                    DgvRow.DefaultCellStyle.SelectionForeColor = Color.Gray
                End If
            Next


            For Each DgvRow As DataGridViewRow In dgvDetModificado.Rows
                If DgvRow.Cells("AccionMod").Value.ToString = "Nuevo" Then
                    DgvRow.DefaultCellStyle.ForeColor = Color.Green
                    DgvRow.DefaultCellStyle.SelectionForeColor = Color.Green
                ElseIf DgvRow.Cells("AccionMod").Value.ToString = "Modificado" Then
                    'dgvDetModificado.Rows(DgvRow.Index).DefaultCellStyle.ForeColor = Color.DarkBlue
                    DgvRow.DefaultCellStyle.ForeColor = Color.DarkBlue
                    DgvRow.DefaultCellStyle.SelectionForeColor = Color.DarkBlue
                ElseIf DgvRow.Cells("AccionMod").Value.ToString = "Eliminado" Then
                    DgvRow.DefaultCellStyle.ForeColor = Color.DarkRed
                    DgvRow.DefaultCellStyle.SelectionForeColor = Color.DarkRed
                End If
            Next
        Catch ex As Exception
            Throw
        End Try
    End Sub
    'Private Sub BuscarLogReserv()
    '    Try
    '        Me.Cursor = Cursors.WaitCursor
    '        dgvDetOriginal.Rows.Clear()
    '        dgvDetModificado.Rows.Clear()

    '        Dim objDetCot As New clsReservaBN.clsDetalleReservaBN
    '        Dim dtt As DataTable = objDetCot.ConsultarListLog(intIDCab, strIDProveedor, _
    '                                                          If(chrAccion = " ", intIDServicio_Det, 0), _
    '                                                          chrAccion)
    '        If chrAccion = " " Then
    '            Dim dttOriginal As DataTable = dtFiltrarDataTable(dtt, "Accion='Original'")
    '            For Each DgvRow As DataRow In dttOriginal.Rows
    '                dgvDetOriginal.Rows.Add(DgvRow("FechaLog"), DgvRow("Usuario"), DgvRow("Accion"), _
    '                                        DgvRow("IDDET"), DgvRow("DiaFormat"), DgvRow("DescCiudad"), DgvRow("DescServicioDet"), _
    '                                        DgvRow("DescProveedor"), DgvRow("DescTipoServ"), DgvRow("NroPax"), DgvRow("IDIdioma"), _
    '                                        DgvRow("Anio"), DgvRow("Tipo"), DgvRow("Total"), DgvRow("Atendido"), DgvRow("DescAtendido"))
    '            Next
    '        End If
    '        Dim dttModificado As DataTable = dtFiltrarDataTable(dtt, "Accion In('Modificado','Eliminado','Nuevo')")
    '        For Each DgvRow As DataRow In dttModificado.Rows
    '            dgvDetModificado.Rows.Add(DgvRow("FechaLog"), DgvRow("Usuario"), DgvRow("Accion"), _
    '                                    DgvRow("IDDET"), DgvRow("DiaFormat"), DgvRow("DescCiudad"), DgvRow("DescServicioDet"), _
    '                                    DgvRow("DescProveedor"), DgvRow("DescTipoServ"), DgvRow("NroPax"), DgvRow("IDIdioma"), _
    '                                    DgvRow("Anio"), DgvRow("Tipo"), DgvRow("Total"), DgvRow("Atendido"), DgvRow("DescAtendido"))
    '        Next

    '        pDgvAnchoColumnas(dgvDetOriginal)
    '        dgvDetOriginal.Columns("FechaLogOrig").Width = 140
    '        dgvDetOriginal.Columns("DiaFormatOrig").Width = 140

    '        pDgvAnchoColumnas(dgvDetModificado)
    '        dgvDetModificado.Columns("FechaLogMod").Width = 140
    '        dgvDetModificado.Columns("FechaMod").Width = 140

    '        ColorearNuevoModificadoEliminado()


    '        If Not blnSoloConsulta Then EnabledAceptar()

    '        Me.Cursor = Cursors.Default
    '    Catch ex As Exception
    '        Me.Cursor = Cursors.Default
    '        Throw
    '    End Try
    'End Sub
    Private Sub BuscarLogCoti()
        Try
            Me.Cursor = Cursors.WaitCursor
            dgvDetOriginal.Rows.Clear()
            dgvDetModificado.Rows.Clear()

            Dim objDetCot As New clsCotizacionBN.clsDetalleCotizacionBN
            Dim dtt As DataTable = objDetCot.ConsultarListLog(intIDCab, strIDProveedor)
            Dim dttOriginal As DataTable = dtFiltrarDataTable(dtt, "Accion='Original'")
            For Each DgvRow As DataRow In dttOriginal.Rows
                dgvDetOriginal.Rows.Add(DgvRow("FechaLog"), DgvRow("Usuario"), DgvRow("Accion"), _
                                        DgvRow("IDDET"), DgvRow("DiaFormat"), DgvRow("DescCiudad"), DgvRow("DescServicioDet"), _
                                        DgvRow("DescProveedor"), DgvRow("DescTipoServ"), DgvRow("NroPax"), DgvRow("IDIdioma"), _
                                        DgvRow("Anio"), DgvRow("Tipo"), DgvRow("Total"), DgvRow("Atendido"), DgvRow("DescAtendido"))
            Next

            Dim dttModificado As DataTable = dtFiltrarDataTable(dtt, "Accion In('Modificado','Eliminado','Nuevo')")
            For Each DgvRow As DataRow In dttModificado.Rows
                dgvDetModificado.Rows.Add(DgvRow("FechaLog"), DgvRow("Usuario"), DgvRow("Accion"), _
                                        DgvRow("IDDET"), DgvRow("DiaFormat"), DgvRow("DescCiudad"), DgvRow("DescServicioDet"), _
                                        DgvRow("DescProveedor"), DgvRow("DescTipoServ"), DgvRow("NroPax"), DgvRow("IDIdioma"), _
                                        DgvRow("Anio"), DgvRow("Tipo"), DgvRow("Total"), DgvRow("Atendido"), DgvRow("DescAtendido"))
            Next

            pDgvAnchoColumnas(dgvDetOriginal)
            dgvDetOriginal.Columns("FechaLogOrig").Width = 140
            dgvDetOriginal.Columns("DiaFormatOrig").Width = 100

            pDgvAnchoColumnas(dgvDetModificado)
            dgvDetModificado.Columns("FechaLogMod").Width = 140
            dgvDetModificado.Columns("FechaMod").Width = 100

            ColorearNuevoModificadoEliminado()


            If Not blnSoloConsulta Then EnabledAceptar()

            Me.Cursor = Cursors.Default
        Catch ex As Exception
            Me.Cursor = Cursors.Default
            Throw
        End Try
    End Sub

    'Private Sub ColorearAtendidos()

    '    For Each dgvRow As DataGridViewRow In dgvDet.Rows
    '        If dgvRow.Cells("Atendido").Value = False Then
    '            dgvRow.DefaultCellStyle.BackColor = Color.LightGray

    '        End If
    '    Next

    'End Sub

    'Private Sub ActivarDesactivarBotones()

    '    'frmMain.btnNuevo.Enabled = blnNuevoEnabled
    '    If blnNuevoEnabled = True Then
    '        If blnGrabarEnabledBD Then
    '            frmMain.btnNuevo.Enabled = blnNuevoEnabled
    '        Else
    '            frmMain.btnNuevo.Enabled = False
    '            blnNuevoEnabled = blnGrabarEnabledBD
    '        End If
    '    Else
    '        frmMain.btnNuevo.Enabled = blnNuevoEnabled
    '    End If

    '    'frmMain.btnGrabar.Enabled = blnGrabarEnabled
    '    If blnGrabarEnabled = True Then
    '        If blnGrabarEnabledBD Then
    '            frmMain.btnGrabar.Enabled = blnGrabarEnabled
    '        Else
    '            frmMain.btnGrabar.Enabled = False
    '            blnGrabarEnabled = blnGrabarEnabledBD
    '        End If
    '    Else
    '        frmMain.btnGrabar.Enabled = blnGrabarEnabled
    '    End If

    '    frmMain.btnDeshacer.Enabled = blnDeshacerEnabled

    '    'frmMain.btnEditar.Enabled = blnEditarEnabled
    '    If blnEditarEnabled = True Then
    '        If blnEditarEnabledBD Then
    '            frmMain.btnEditar.Enabled = blnEditarEnabled
    '        Else
    '            frmMain.btnEditar.Enabled = False
    '            blnEditarEnabled = blnEditarEnabledBD
    '        End If
    '    Else
    '        frmMain.btnEditar.Enabled = blnEditarEnabled
    '    End If

    '    'frmMain.btnEliminar.Enabled = blnEliminarEnabled
    '    If blnEliminarEnabled = True Then
    '        If blnEliminarEnabledBD Then
    '            frmMain.btnEliminar.Enabled = blnEliminarEnabled
    '        Else
    '            frmMain.btnEliminar.Enabled = False
    '            blnEliminarEnabled = blnEliminarEnabledBD
    '        End If
    '    Else
    '        frmMain.btnEliminar.Enabled = blnEliminarEnabled
    '    End If

    '    frmMain.btnImprimir.Enabled = blnImprimirEnabled
    '    frmMain.btnSalir.Enabled = blnSalirEnabled

    'End Sub

    'Private Sub frmConsLogCotizaciones_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Activated
    '    AddHandler frmMain.btnSalir.Click, AddressOf pSalir
    '    frmMain.btnSalir.Enabled = blnSalirEnabled

    '    frmMain.btnNuevo.Enabled = blnNuevoEnabled

    '    frmMain.btnEliminar.Enabled = blnEliminarEnabled

    '    frmMain.btnEditar.Enabled = blnEditarEnabled

    '    frmMain.btnDeshacer.Enabled = blnDeshacerEnabled
    '    frmMain.btnGrabar.Enabled = blnGrabarEnabled

    '    frmMain.btnImprimir.Enabled = blnImprimirEnabled


    'End Sub
    Private Sub frmConsLogCotizaciones_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        Select Case e.KeyCode
            Case Keys.F6
                If btnAceptar.Enabled Then btnAceptar_Click(Nothing, Nothing)
            Case Keys.Escape
                pSalir(Nothing, Nothing)
        End Select
    End Sub
    Private Sub frmConsLogCotizaciones_Deactivate(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Deactivate
        'RemoveHandler frmMain.btnSalir.Click, AddressOf pSalir
    End Sub
    Private Sub frmConsLogCotizaciones_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        frmMain.DesabledBtns()

        Try
            If chrModulo = "R" Then
                Dim objBT As New clsReservaBT
                objBT.EliminarRESERVAS_DET_TMP(strIDReserva)
            ElseIf chrModulo = "O" Then
                Dim objBT As New clsOperacionBT
                objBT.EliminarOPERACIONES_DET_TMP(strIDOperacion)
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Error en " & Err.Source, MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub
    Private Sub pSalir(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Me.Close()
    End Sub

    'Private Sub dgvDet_CellPainting(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellPaintingEventArgs) Handles dgvDetOriginal.CellPainting
    '    If e.RowIndex < 0 Then Exit Sub
    '    If e.ColumnIndex < 0 Then Exit Sub
    '    If dgvDetOriginal.Rows(e.RowIndex).Cells("AtendidoOrig").Value = True Then
    '        dgvDetOriginal.Rows(e.RowIndex).DefaultCellStyle.ForeColor = Color.Gray
    '    Else
    '        dgvDetOriginal.Rows(e.RowIndex).DefaultCellStyle.ForeColor = Color.Black
    '    End If
    '    'ColorearNuevoModificadoEliminado()
    'End Sub

    Private Sub dgvDet_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvDetOriginal.CellContentClick

    End Sub

    'Private Sub mnuAtender_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuAtender.Click
    '    Try
    '        Dim strEstado As String = If(dgvDet.CurrentRow.Cells("Atendido").Value = True, "Pendiente", "Atendido")
    '        Dim strServicio As String = dgvDet.CurrentRow.Cells("DescServicioDet").Value.ToString

    '        If MessageBox.Show("¿Está Ud. seguro de cambiar de estado a " & strEstado & " al servicio: " & strServicio & " ?", gstrTituloMsg, MessageBoxButtons.YesNo, MessageBoxIcon.Question) = Windows.Forms.DialogResult.Yes Then

    '            Dim objDetRes As New clsReservaBT.clsDetalleReservaBT
    '            objDetRes.AtenderCambioCotizacion(dgvDet.CurrentRow.Cells("IDDet").Value.ToString, _
    '                                              Not dgvDet.CurrentRow.Cells("Atendido").Value, gstrUser)

    '            MessageBox.Show("Se cambió el estado exitosamente", gstrTituloMsg, MessageBoxButtons.OK, MessageBoxIcon.Information)

    '            Buscar()
    '        End If
    '    Catch ex As Exception
    '        MessageBox.Show(ex.Message, "Error en " & Err.Source, MessageBoxButtons.OK, MessageBoxIcon.Error)
    '    End Try
    'End Sub

    'Private Sub dgvDet_MouseDown(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles dgvDet.MouseDown
    '    If e.Button = Windows.Forms.MouseButtons.Right Then

    '        If dgvDet.CurrentRow.Cells("DescAtendido").Value.ToString.Trim = "" Then
    '            dgvDet.ContextMenuStrip = Nothing
    '            Exit Sub
    '        End If


    '        Dim strEstado As String = If(dgvDet.CurrentRow.Cells("Atendido").Value = True, "Cambiar a Pendiente", "Cambiar a Atendido")

    '        mnuAtender.Text = strEstado

    '        dgvDet.ContextMenuStrip = cmsEdit
    '        cmsEdit.Show()



    '    End If
    'End Sub

    Private Sub LlamarFormCorreos(ByVal vblnModificarReserva As Boolean, ByVal vblnClickNuevo As Boolean)
        Try
            Dim frmCorr As Object = Nothing 'New frmCorreos
            With frmCorr
                .strIDReserva = strIDReserva
                If Not (frmRefCoti Is Nothing) Then
                    .strIDProveedor = frmRefCoti.dgvDet.CurrentRow.Cells("IDProveedor").Value.ToString
                    .strDescProveedor = frmRefCoti.dgvDet.CurrentRow.Cells("DescProveedor").Value.ToString
                    .strCorreoProveedor = frmRefCoti.dgvDet.CurrentRow.Cells("CorreoReserva1").Value.ToString
                    .strCodReservaProv = frmRefCoti.dgvDet.CurrentRow.Cells("CodReservaProv").Value.ToString
                Else
                    If blnDgvTipoProvSelHotel Then
                        .strIDProveedor = frmRef.DgvHotelesyEspeciales.CurrentRow.Cells("IDProveedorHotel").Value.ToString
                        .strDescProveedor = frmRef.DgvHotelesyEspeciales.CurrentRow.Cells("DescripcionHotel").Value.ToString
                        .strCorreoProveedor = frmRef.DgvHotelesyEspeciales.CurrentRow.Cells("CorreoHotel").Value.ToString
                        .strCodReservaProv = frmRef.DgvHotelesyEspeciales.CurrentRow.Cells("CodReservaProvHotel").Value.ToString
                    Else
                        .strIDProveedor = frmRef.DgvNoHoteles.CurrentRow.Cells("IDProveedorNoHotel").Value.ToString
                        .strDescProveedor = frmRef.DgvNoHoteles.CurrentRow.Cells("DescripcionNoHotel").Value.ToString
                        .strCorreoProveedor = frmRef.DgvNoHoteles.CurrentRow.Cells("CorreoNoHotel").Value.ToString
                        .strCodReservaProv = frmRef.DgvNoHoteles.CurrentRow.Cells("CodReservaProvNoHotel").Value.ToString
                    End If
                End If
                .strIDFile = strIDFile
                .intIDCab = intIDCab
                '.strIDTarea = dgvDeadLines.CurrentRow.Cells("IDTarea").Value.ToString
                .blnDgvTipoProvSelHotel = blnDgvTipoProvSelHotel
                .intNroPax = intNroPax
                .strTitulo = strTitulo
                .frmRefConsLogCotizaciones = Me
                '.MdiParent = frmMain
                .blnModificarReserva = vblnModificarReserva
                blnEnvioCorreo = False
                .blnClickNuevo = vblnClickNuevo
                .strModulo = "R"
                .ShowDialog()
                If blnEnvioCorreo Then
                    If Not (frmRefCoti Is Nothing) Then
                        frmRefCoti.dgvDet.CurrentRow.Cells("CotiDetLog").Value = "Cambios en Cotización Atendidos"
                        Dim myDataGridViewLinkCell As DataGridViewLinkCell = _
                            CType(frmRefCoti.dgvDet.CurrentRow.Cells("CotiDetLog"), DataGridViewLinkCell)
                        myDataGridViewLinkCell.LinkColor = Color.Blue
                        myDataGridViewLinkCell.VisitedLinkColor = Color.Blue
                        btnCrearCorreo.Enabled = False
                    Else
                        If blnDgvTipoProvSelHotel Then
                            frmRef.DgvHotelesyEspeciales.CurrentRow.Cells("CotiDetLogHotel").Value = "Cambios en Cotización Atendidos"
                            Dim myDataGridViewLinkCell As DataGridViewLinkCell = _
                                CType(frmRef.DgvHotelesyEspeciales.CurrentRow.Cells("CotiDetLogHotel"), DataGridViewLinkCell)
                            myDataGridViewLinkCell.LinkColor = Color.Blue
                            myDataGridViewLinkCell.VisitedLinkColor = Color.Blue
                        Else
                            frmRef.DgvNoHoteles.CurrentRow.Cells("CotiDetLogNoHotel").Value = "Cambios en Cotización Atendidos"

                            Dim myDataGridViewLinkCell As DataGridViewLinkCell = _
                                CType(frmRef.DgvNoHoteles.CurrentRow.Cells("CotiDetLogNoHotel"), DataGridViewLinkCell)
                            myDataGridViewLinkCell.LinkColor = Color.Blue
                            myDataGridViewLinkCell.VisitedLinkColor = Color.Blue
                        End If
                    End If
                    btnCrearCorreo.Enabled = True
                    frmRef.ActualizarFileenPantalla()
                End If
                BuscarLogCoti()
            End With
        Catch ex As Exception
            Throw
        End Try
    End Sub

    Private Sub btnModificarReservas_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnModificarReservas.Click, btnCrearCorreo.Click
        Try
            Dim chrAccion As Char = ""

            If sender.name = "btnModificarReservas" Then

                For Each dgvRow As DataGridViewRow In dgvDetOriginal.Rows
                    If dgvRow.Cells("Atendido").Value = False And dgvRow.Cells("Accion").Value.ToString.Trim <> "Antes de Cambios" Then
                        chrAccion = dgvRow.Cells("Accion").Value.ToString.Substring(0, 1)
                        Exit For
                    End If
                Next


                If chrAccion = "N" Then
                    If MessageBox.Show("¿Está Ud. seguro de Crear la reserva automáticamente?", gstrTituloMsg, MessageBoxButtons.YesNo, MessageBoxIcon.Question) = Windows.Forms.DialogResult.No Then Exit Sub
                    LlamarFormCorreos(True, True)
                Else
                    If MessageBox.Show("¿Está Ud. seguro de Modificar la reserva automáticamente?", gstrTituloMsg, MessageBoxButtons.YesNo, MessageBoxIcon.Question) = Windows.Forms.DialogResult.No Then Exit Sub
                    LlamarFormCorreos(True, False)
                End If


            ElseIf sender.name = "btnCrearCorreo" Then
                For Each dgvRow As DataGridViewRow In dgvDetOriginal.Rows
                    If dgvRow.Cells("Atendido").Value = True And dgvRow.Cells("Accion").Value.ToString.Trim <> "Antes de Cambios" Then
                        chrAccion = dgvRow.Cells("Accion").Value.ToString.Substring(0, 1)
                        Exit For
                    End If
                Next


                If chrAccion = "N" Then
                    LlamarFormCorreos(False, True)
                Else
                    LlamarFormCorreos(False, False)
                End If

            End If



            If sender.name = "btnModificarReservas" Then
                MessageBox.Show("Las reservas se crearon exitosamente", gstrTituloMsg, MessageBoxButtons.OK, MessageBoxIcon.Information)
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Error en " & Err.Source, MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub
    Private Sub ActualizarOperacionesxLog()
        Try
            'If MessageBox.Show("¿Está Ud. seguro de actualizar los servicios modificados por Reservas en Operaciones?. Esto puede reemplazar trabajo realizado.", gstrTituloMsg, MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation) = Windows.Forms.DialogResult.No Then Exit Sub
            Me.Cursor = Cursors.WaitCursor

            Dim objOpeBN As New clsOperacionBN
            Dim blnUpdateVoucherTEMP As Boolean = False
            If objOpeBN.blnExisteVoucherxIDOperacion(strIDOperacion) Then
                If MessageBox.Show("¿Desea mantener los vouchers, con los nuevos servicios por ingresar?", gstrTituloMsg, MessageBoxButtons.YesNo, MessageBoxIcon.Question) = Windows.Forms.DialogResult.Yes Then
                    blnUpdateVoucherTEMP = True
                End If
            End If


            Dim objOpeBT As New clsOperacionBT
            'objOpeBT.ActualizarCambiosdeReservas(strIDProveedor, _
            '                                    intIDCab, chrAccion, gstrUser)
            objOpeBT.MatchDetOperacionesRealyTMP(intIDCab, strIDOperacion, strIDProveedor, gstrUser, ListaStServiciosCambiosVentasReservas, blnUpdateVoucherTEMP)

            Me.Cursor = Cursors.Default
            blnCambios = True
            MessageBox.Show("Los cambios de los servicios modificados por Reservas fueron actualizados en Operaciones exitosamente", gstrTituloMsg, MessageBoxButtons.OK, MessageBoxIcon.Information)
            Me.Close()
        Catch ex As Exception
            Throw
        End Try
    End Sub

    Private Sub ActualizarReservasxLog()

        Try

            'ActualizarFlagCambiosVtasAceptadas()
            'ActualizarEstado()
            'Dim blnValidar As Boolean = False
            'For Each dgvRow As DataGridViewRow In dgvDetModificado.Rows
            '    If dgvRow.Cells("AccionMod").Value.ToString.Trim = "Eliminado" Then
            '        blnValidar = True
            '        Exit For
            '    End If
            'Next
            'If blnValidar Then
            '    Dim objOpeBN As New clsOperacionBN
            '    If objOpeBN.blnExistenOperacionesxIDReserva(strIDReserva) Then
            '        MessageBox.Show("No se puede actualizar. Se ha encontrado información generada en Operaciones para esta Reserva, antes debe hacerlo desde dicho módulo", gstrTituloMsg, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            '        Exit Sub
            '    End If
            'End If

            'If MessageBox.Show("¿Está Ud. seguro de actualizar los servicios modificados por Ventas, en Reservas?. Esto puede reemplazar trabajo realizado.", gstrTituloMsg, MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation) = Windows.Forms.DialogResult.No Then Exit Sub
            Me.Cursor = Cursors.WaitCursor
            Dim objResBT As New clsReservaBT

            'objResBT.ActualizarCambiosdeVentas(strIDReserva, strIDProveedor, intIDCab, gstrUser)
            'objResBT.RegenerarReserva_TMP(strIDReserva, strIDProveedor, intIDCab, gstrUser)

            'If strIDReservaProtecc = "" Then
            objResBT.MatchDetReservasRealyTMP(intIDCab, strIDReserva, strIDProveedor, blnEsAlojamiento, _
                                              gstrUser, intIDReservaProtecc, ListaStServiciosCambiosVentasReservas, _
                                              blnAnulacionComRes)
            'Else
            'objResBT.MatchDetReservasRealyTMP(intIDCab, strIDReservaProtecc, strIDProveedorProtecc, gstrUser, ListaStServiciosCambiosVentasReservas)
            'End If



            Me.Cursor = Cursors.Default

            'If Not objResBT.ActualizarxIDDetLogVentas(intIDCab, strIDProveedor, strIDReserva, gstrUser) Then

            'MessageBox.Show("No se encontraron cambios pendientes para esta reserva, debe actualizar pantalla", gstrTituloMsg, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            'Else
            blnCambios = True
            'If strIDEstadoLog <> "E" Then
            MessageBox.Show("Los cambios de Ventas fueron actualizados en la Reserva pasando a Estado Nuevo (NV) o eliminando el proveedor, exitosamente", gstrTituloMsg, MessageBoxButtons.OK, MessageBoxIcon.Information)
            'Else
            'MessageBox.Show("Los cambios de Ventas fueron actualizados en la Reserva eliminando el proveedor", gstrTituloMsg, MessageBoxButtons.OK, MessageBoxIcon.Information)
            'End If

            'End If

            'frmRef.CargarDatosReservasDespuesdeGrabar()
            'frmRef.ActualizarFlagCambiosVtasAceptadasenForm(True)
            'frmRef.ActualizarEstadoFormNV()



            Me.Close()
        Catch ex As Exception
            Me.Cursor = Cursors.Default
            'If InStr(ex.Message, "FK_") > 0 Then
            '    MessageBox.Show("No se puede regenerar, tiene información de operaciones generadas, debe ingresar a dicho módulo para eliminarla", gstrTituloMsg, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            'Else
            MessageBox.Show(ex.Message, ex.Source, MessageBoxButtons.OK, MessageBoxIcon.Error)
            'End If

        End Try

    End Sub
    Private Function ListaStServiciosCambiosVentasReservas() As List(Of clsReservaBE.clsServiciosCambiosVentasReservasBE)

        Try
            Dim ListaServCambiosReturn As New List(Of clsReservaBE.clsServiciosCambiosVentasReservasBE)

            For Each dgvRowUp As DataGridViewRow In dgvAntes.Rows
                If dgvRowUp.Cells("FlagOMEN").Value = "Eliminar" Or dgvRowUp.Cells("FlagOMEN").Value = "Actualizar" Then
                    Dim ItemServ As New clsReservaBE.clsServiciosCambiosVentasReservasBE With _
                    {.Accion = dgvRowUp.Cells("FlagOMEN").Value, _
                     .CapacidadHab = dgvRowUp.Cells("CapacidadHab").Value, _
                     .EsMatrimonial = dgvRowUp.Cells("EsMatrimonial").Value, _
                     .Dia = dgvRowUp.Cells("Dia").Value, _
                     .IDServicio_Det = dgvRowUp.Cells("IDServicio_Det").Value, _
                     .IDReserva_Det = dgvRowUp.Cells("IDReserva_Det").Value, _
                     .IDOperacion_Det = If(dgvRowUp.Cells("IDOperacion_Det").Value.ToString = "", "0", dgvRowUp.Cells("IDOperacion_Det").Value), _
                     .NuevaTarifa = If(dgvRowUp.Cells("CoEstadoTarifa").Value.ToString = "PD", True, False)}

                    ListaServCambiosReturn.Add(ItemServ)

                End If
            Next

            For Each dgvRowDw As DataGridViewRow In dgvDespues.Rows
                If dgvRowDw.Cells("FlagOMEN2").Value = "Nuevo" Then
                    Dim ItemServ As New clsReservaBE.clsServiciosCambiosVentasReservasBE With _
                    {.Accion = dgvRowDw.Cells("FlagOMEN2").Value, _
                     .CapacidadHab = dgvRowDw.Cells("CapacidadHab2").Value, _
                     .EsMatrimonial = dgvRowDw.Cells("EsMatrimonial2").Value, _
                     .Dia = dgvRowDw.Cells("Dia2").Value, _
                     .IDServicio_Det = dgvRowDw.Cells("IDServicio_Det2").Value, _
                    .IDReserva_Det = dgvRowDw.Cells("IDReserva_Det2").Value, _
                     .IDOperacion_Det = If(dgvRowDw.Cells("IDOperacion_Det2").Value.ToString = "", "0", dgvRowDw.Cells("IDOperacion_Det2").Value)}
                    ListaServCambiosReturn.Add(ItemServ)

                End If
            Next

            Return ListaServCambiosReturn
        Catch ex As Exception
            Throw
        End Try



    End Function

    Private Sub ActualizarFlagViaticos()
        If chrModulo <> "R" Then Exit Sub
        Try
            'Borrar los detalles existente [Borrar]
            For Each Item As DataGridViewRow In dgvAntes.Rows
                Dim intNuVitaticoTC As Integer = CInt(If(IsDBNull(Item.Cells("NuViaticoTC").Value) = True, 0, Item.Cells("NuViaticoTC").Value))
                If intNuVitaticoTC > 0 And Item.Cells("FlagOMEN").Value = "Actualizar" Then
                    Item.Cells("FlagOMEN").Value = "Eliminar"
                    Item.DefaultCellStyle.BackColor = Color.Pink
                    Item.DefaultCellStyle.SelectionBackColor = Color.Pink
                End If
            Next

            'Borrar los detalles existente [Nuevo]
            For Each Item As DataGridViewRow In dgvDespues.Rows
                Dim intNuVitaticoTC As Integer = CInt(If(IsDBNull(Item.Cells("NuViaticoTC2").Value) = True, 0, Item.Cells("NuViaticoTC2").Value))
                If intNuVitaticoTC > 0 And Item.Cells("FlagOMEN2").Value = "Actualizar" Then
                    Item.Cells("FlagOMEN2").Value = "Nuevo"
                    Item.DefaultCellStyle.BackColor = Color.Pink
                    Item.DefaultCellStyle.SelectionBackColor = Color.Pink
                End If
            Next
        Catch ex As Exception
            Throw
        End Try
    End Sub

    Private Sub EdicionColumnaFlagOMEN(Optional ByVal vintIndexAntes As Integer = -1, _
                                       Optional ByVal vintIndexDespues As Integer = -1, _
                                       Optional ByVal vblnNoActualizarAntes As Boolean = True)
        Try
            If chrModulo = "R" Then
                If dgvAntes.RowCount > 0 And dgvDespues.RowCount = 0 Then
                    For Each ItemDgv As DataGridViewRow In dgvAntes.Rows
                        ItemDgv.Cells("FlagOMEN").Value = "Anular Res."
                        ItemDgv.DefaultCellStyle.BackColor = Color.Pink
                        ItemDgv.DefaultCellStyle.SelectionBackColor = Color.Pink
                        blnAnulacionComRes = True
                    Next
                    Exit Sub
                End If
            End If

            If vblnNoActualizarAntes Then
                If vintIndexAntes = -1 Then
                    For Each dgvRowUp As DataGridViewRow In dgvAntes.Rows
                        Dim blnFind As Boolean = False
                        Dim blnNuevaTarifa As Boolean = False
                        For Each dgvRowDown2 As DataGridViewRow In dgvDespues.Rows
                            Dim intIDServicio_Det As Integer = dgvRowUp.Cells("IDServicio_Det").Value
                            Dim intIDServicio_Det2 As Integer = dgvRowDown2.Cells("IDServicio_Det2").Value
                            Dim intIDDet As Integer = dgvRowUp.Cells("IDDet").Value
                            Dim intIDDet_2 As Integer = dgvRowDown2.Cells("IDDet2").Value
                            If intIDReservaProtecc <> 0 Then
                                intIDServicio_Det = 0
                                intIDServicio_Det2 = 0
                            End If
                            'dgvRowDown2.Cells("IDServicio_Det2").Value = dgvRowUp.Cells("IDServicio_Det").Value And _
                            If (intIDServicio_Det = intIDServicio_Det2 And intIDDet = intIDDet_2) And _
                                Format(dgvRowDown2.Cells("Dia2").Value, "dd/MM/yyyy") = Format(dgvRowUp.Cells("Dia").Value, "dd/MM/yyyy") And _
                                dgvRowDown2.Cells("CapacidadHab2").Value = dgvRowUp.Cells("CapacidadHab").Value And _
                                dgvRowDown2.Cells("EsMatrimonial2").Value = dgvRowUp.Cells("EsMatrimonial").Value And _
                                dgvRowDown2.Cells("FlagComparado2").Value = 0 Then

                                blnFind = True
                                dgvRowDown2.Cells("FlagComparado2").Value = 1
                                Exit For
                            End If

                        Next
                        If Not blnFind Then
                            dgvRowUp.Cells("FlagOMEN").Value = "Eliminar"
                            dgvRowUp.DefaultCellStyle.SelectionBackColor = Color.Pink
                            dgvRowUp.DefaultCellStyle.BackColor = Color.Pink
                        End If

                        If dgvRowUp.Cells("CoEstadoTarifa").Value = "PD" Then
                            dgvRowUp.Cells("NoEstadoTarifa").Value = "Nueva(s) Tarifa(s)"
                            dgvRowUp.Cells("FlagOMEN").Value = "Actualizar"
                            dgvRowUp.Cells("NoEstadoTarifa").Style.SelectionBackColor = Color.Pink
                            dgvRowUp.Cells("NoEstadoTarifa").Style.BackColor = Color.Pink
                        Else
                            dgvRowUp.Cells("NoEstadoTarifa").Value = "Actualizado"

                        End If
                    Next
                Else
                    For Each dgvRowUp As DataGridViewRow In dgvAntes.Rows
                        If dgvRowUp.Index = vintIndexAntes Then
                            Dim blnFind As Boolean = False
                            Dim blnNuevaTarifa As Boolean = False
                            For Each dgvRowDown2 As DataGridViewRow In dgvDespues.Rows
                                Dim intIDServicio_Det As Integer = dgvRowUp.Cells("IDServicio_Det").Value
                                Dim intIDServicio_Det2 As Integer = dgvRowDown2.Cells("IDServicio_Det2").Value
                                If intIDReservaProtecc <> 0 Then
                                    intIDServicio_Det = 0
                                    intIDServicio_Det2 = 0
                                End If
                                'dgvRowDown2.Cells("IDServicio_Det2").Value = dgvRowUp.Cells("IDServicio_Det").Value And _
                                If intIDServicio_Det2 = intIDServicio_Det And _
                                Format(dgvRowDown2.Cells("Dia2").Value, "dd/MM/yyyy") = Format(dgvRowUp.Cells("Dia").Value, "dd/MM/yyyy") And _
                                dgvRowDown2.Cells("CapacidadHab2").Value = dgvRowUp.Cells("CapacidadHab").Value And _
                                    dgvRowDown2.Cells("EsMatrimonial2").Value = dgvRowUp.Cells("EsMatrimonial").Value And _
                                    dgvRowDown2.Cells("FlagComparado2").Value = 0 Then

                                    blnFind = True
                                    dgvRowDown2.Cells("FlagComparado2").Value = 1
                                    Exit For
                                End If

                            Next
                            If Not blnFind Then
                                dgvRowUp.Cells("FlagOMEN").Value = "Eliminar"
                                dgvRowUp.DefaultCellStyle.SelectionBackColor = Color.Pink
                                dgvRowUp.DefaultCellStyle.BackColor = Color.Pink
                            End If

                        End If
                    Next
                End If
            End If


            Dim bytContDown As Byte = 1
            Dim datFechaAnt As Date = "01/01/1900"
            If vintIndexDespues = -1 Then
                For Each dgvRowDown As DataGridViewRow In dgvDespues.Rows
                    Dim blnFind As Boolean = False
                    Dim blnNuevaTarifa As Boolean = False
                    Dim bytContUp As Byte = 1

                    For Each dgvRowUp2 As DataGridViewRow In dgvAntes.Rows
                        Dim intIDServicio_Det As Integer = dgvRowUp2.Cells("IDServicio_Det").Value
                        Dim intIDServicio_Det2 As Integer = dgvRowDown.Cells("IDServicio_Det2").Value
                        Dim intIDDet As Integer = dgvRowUp2.Cells("IDDet").Value
                        Dim intIDDet_2 As Integer = dgvRowDown.Cells("IDDet2").Value
                        If intIDReservaProtecc <> 0 Then
                            intIDServicio_Det = 0
                            intIDServicio_Det2 = 0
                        End If
                        'dgvRowUp2.Cells("IDServicio_Det").Value = dgvRowDown.Cells("IDServicio_Det2").Value And _

                        'If Format(dgvRowUp2.Cells("Dia").Value, "dd/MM/yyyy") = "29/11/2013" Then
                        '    MessageBox.Show("2 " & dgvRowUp2.Cells("Dia").Value)
                        'End If

                        If ((intIDServicio_Det = intIDServicio_Det2 And intIDDet = intIDDet_2) And _
                        Format(dgvRowUp2.Cells("Dia").Value, "dd/MM/yyyy") = Format(dgvRowDown.Cells("Dia2").Value, "dd/MM/yyyy") And _
                        dgvRowUp2.Cells("CapacidadHab").Value = dgvRowDown.Cells("CapacidadHab2").Value And _
                            dgvRowUp2.Cells("EsMatrimonial").Value = dgvRowDown.Cells("EsMatrimonial2").Value And _
                            dgvRowUp2.Cells("FlagComparado").Value = 0) Or dgvRowUp2.Cells("CoEstadoTarifa").Value = "PD" Then

                            blnFind = True
                            dgvRowUp2.Cells("FlagComparado").Value = 1

                            Dim blnColUpd As Boolean = False
                            For i As Byte = 0 To dgvAntes.Columns.Count - 1
                                Dim blnCambiar As Boolean = False
                                If chrModulo = "R" Then
                                    If dgvAntes.Columns(i).Name <> "IDReserva_Det" _
                                    And dgvAntes.Columns(i).Name <> "Hora" _
                                    And dgvAntes.Columns(i).Name <> "NoEstadoTarifa" _
                                    And dgvAntes.Columns(i).Visible = True Then
                                        'And dgvAntes.Columns(i).Name <> "FlagComparado" _
                                        'And dgvAntes.Columns(i).Name <> "IDServicio_Det" _

                                        blnCambiar = True
                                    End If
                                ElseIf chrModulo = "O" Then
                                    If dgvAntes.Columns(i).Name <> "IDOperacion_Det" _
                                    And dgvAntes.Columns(i).Name <> "Hora" _
                                    And dgvAntes.Columns(i).Name <> "NoEstadoTarifa" _
                                    And dgvAntes.Columns(i).Visible = True Then
                                        'And dgvAntes.Columns(i).Name <> "FlagComparado" _
                                        'And dgvAntes.Columns(i).Name <> "IDServicio_Det" Then

                                        blnCambiar = True
                                    End If
                                End If
                                If dgvRowUp2.Cells("Transfer").Value = True And chrModulo = "R" Then
                                    If dgvAntes.Columns(i).Name = "DescServicioDet" Then
                                        blnCambiar = False
                                    End If
                                End If

                                If blnCambiar = True Then


                                    Dim strValorCol1 As String = ""
                                    Dim strValorCol2 As String = ""
                                    If Not dgvRowUp2.Cells(i).Value Is Nothing Then
                                        strValorCol1 = dgvRowUp2.Cells(i).Value.ToString
                                    End If
                                    If Not dgvRowDown.Cells(i).Value Is Nothing Then
                                        strValorCol2 = dgvRowDown.Cells(i).Value.ToString
                                    End If
                                    If strValorCol1.Trim <> strValorCol2.Trim Then
                                        'If (bytContUp <> bytContDown) And (dgvAntes.Rows.Count = dgvDespues.Rows.Count) Then
                                        '    GoTo NextDown
                                        'End If
                                        dgvRowUp2.Cells(i).Style.BackColor = Color.Pink
                                        dgvRowUp2.Cells(i).Style.SelectionBackColor = Color.Pink
                                        dgvRowDown.Cells(i).Style.BackColor = Color.Pink
                                        dgvRowDown.Cells(i).Style.SelectionBackColor = Color.Pink

                                        blnColUpd = True
                                    End If
                                Else

                                End If

                            Next
                            If blnColUpd Then
                                dgvRowUp2.Cells("FlagOMEN").Value = "Actualizar"
                                dgvRowDown.Cells("FlagOMEN2").Value = "Actualizar"

                                dgvRowDown.Cells("Hora2").Value = dgvRowUp2.Cells("Hora").Value
                                If dgvRowUp2.Cells("Transfer").Value = True And chrModulo = "R" And dgvRowDown.Cells("IDReserva_DetOrigAcomVehi2").Value = "0" Then
                                    dgvRowDown.Cells("DescServicioDet2").Value = dgvRowUp2.Cells("DescServicioDet").Value
                                End If
                            End If

                            'GoTo NextDown
                            'Else
                            '   dgvRowUp2.Cells("FlagComparado").Value = 0

                            Exit For
                        End If

                        If dgvRowUp2.Cells("CoEstadoTarifa").Value = "PD" Then
                            blnNuevaTarifa = True
                        End If
NextUp:
                        bytContUp += 1
                    Next
                    If Not blnFind And Not blnNuevaTarifa Then
                        dgvRowDown.Cells("FlagOMEN2").Value = "Nuevo"


                        dgvRowDown.DefaultCellStyle.SelectionBackColor = Color.Pink
                        dgvRowDown.DefaultCellStyle.BackColor = Color.Pink

                    End If
                    If blnNuevaTarifa Then
                        dgvRowDown.Cells("FlagOMEN2").Value = "Actualizar"
                    End If

NextDown:
                    bytContDown += 1
                Next
            Else
                For Each dgvRowDown As DataGridViewRow In dgvDespues.Rows
                    Dim blnFind As Boolean = False
                    Dim blnNuevaTarifa As Boolean = False
                    Dim bytContUp As Byte = 1

                    If dgvRowDown.Index = vintIndexDespues Then
                        For Each dgvRowUp2 As DataGridViewRow In dgvAntes.Rows
                            Dim intIDServicio_Det As Integer = dgvRowUp2.Cells("IDServicio_Det").Value
                            Dim intIDServicio_Det2 As Integer = dgvRowDown.Cells("IDServicio_Det2").Value
                            If intIDReservaProtecc <> 0 Then
                                intIDServicio_Det = 0
                                intIDServicio_Det2 = 0
                            End If
                            'dgvRowUp2.Cells("IDServicio_Det").Value = dgvRowDown.Cells("IDServicio_Det2").Value And _

                            'If Format(dgvRowUp2.Cells("Dia").Value, "dd/MM/yyyy") = "29/11/2013" Then
                            '    MessageBox.Show("2 " & dgvRowUp2.Cells("Dia").Value)
                            'End If

                            If intIDServicio_Det = intIDServicio_Det2 And _
                            Format(dgvRowUp2.Cells("Dia").Value, "dd/MM/yyyy") = Format(dgvRowDown.Cells("Dia2").Value, "dd/MM/yyyy") And _
                            dgvRowUp2.Cells("CapacidadHab").Value = dgvRowDown.Cells("CapacidadHab2").Value And _
                                dgvRowUp2.Cells("EsMatrimonial").Value = dgvRowDown.Cells("EsMatrimonial2").Value And _
                                dgvRowUp2.Cells("FlagComparado").Value = 0 Then

                                blnFind = True
                                dgvRowUp2.Cells("FlagComparado").Value = 1

                                Dim blnColUpd As Boolean = False
                                For i As Byte = 0 To dgvAntes.Columns.Count - 1
                                    Dim blnCambiar As Boolean = False
                                    If chrModulo = "R" Then
                                        If dgvAntes.Columns(i).Name <> "IDReserva_Det" _
                                        And dgvAntes.Columns(i).Name <> "Hora" _
                                        And dgvAntes.Columns(i).Visible = True Then
                                            'And dgvAntes.Columns(i).Name <> "FlagComparado" _
                                            'And dgvAntes.Columns(i).Name <> "IDServicio_Det" _

                                            blnCambiar = True
                                        End If
                                    ElseIf chrModulo = "O" Then
                                        If dgvAntes.Columns(i).Name <> "IDOperacion_Det" _
                                        And dgvAntes.Columns(i).Name <> "Hora" _
                                        And dgvAntes.Columns(i).Visible = True Then
                                            'And dgvAntes.Columns(i).Name <> "FlagComparado" _
                                            'And dgvAntes.Columns(i).Name <> "IDServicio_Det" Then

                                            blnCambiar = True
                                        End If
                                    End If
                                    If dgvRowUp2.Cells("Transfer").Value = True And chrModulo = "R" Then
                                        If dgvAntes.Columns(i).Name = "DescServicioDet" Then
                                            blnCambiar = False
                                        End If
                                    End If

                                    If blnCambiar = True Then


                                        Dim strValorCol1 As String = ""
                                        Dim strValorCol2 As String = ""
                                        If Not dgvRowUp2.Cells(i).Value Is Nothing Then
                                            strValorCol1 = dgvRowUp2.Cells(i).Value.ToString
                                        End If
                                        If Not dgvRowDown.Cells(i).Value Is Nothing Then
                                            strValorCol2 = dgvRowDown.Cells(i).Value.ToString
                                        End If
                                        If strValorCol1.Trim <> strValorCol2.Trim Then
                                            'If (bytContUp <> bytContDown) And (dgvAntes.Rows.Count = dgvDespues.Rows.Count) Then
                                            '    GoTo NextDown
                                            'End If
                                            dgvRowUp2.Cells(i).Style.BackColor = Color.Pink
                                            dgvRowUp2.Cells(i).Style.SelectionBackColor = Color.Pink
                                            dgvRowDown.Cells(i).Style.BackColor = Color.Pink
                                            dgvRowDown.Cells(i).Style.SelectionBackColor = Color.Pink

                                            blnColUpd = True
                                        End If
                                    End If

                                Next
                                If blnColUpd Then
                                    dgvRowUp2.Cells("FlagOMEN").Value = "Actualizar"
                                    dgvRowDown.Cells("FlagOMEN2").Value = "Actualizar"

                                    dgvRowDown.Cells("Hora2").Value = dgvRowUp2.Cells("Hora").Value
                                    If dgvRowUp2.Cells("Transfer").Value = True And chrModulo = "R" Then
                                        dgvRowDown.Cells("DescServicioDet2").Value = dgvRowUp2.Cells("DescServicioDet").Value
                                    End If
                                End If

                                'GoTo NextDown
                                'Else
                                '   dgvRowUp2.Cells("FlagComparado").Value = 0

                                Exit For
                            End If
                            If dgvRowUp2.Cells("CoEstadoTarifa").Value = "PD" Then
                                blnNuevaTarifa = True
                            End If
NextUp2:
                            bytContUp += 1
                        Next
                        If Not blnFind And Not blnNuevaTarifa Then
                            dgvRowDown.Cells("FlagOMEN2").Value = "Nuevo"


                            dgvRowDown.DefaultCellStyle.SelectionBackColor = Color.Pink
                            dgvRowDown.DefaultCellStyle.BackColor = Color.Pink

                        End If
                        If blnNuevaTarifa Then
                            dgvRowDown.Cells("FlagOMEN2").Value = "Actualizar"
                        End If
NextDown2:
                        bytContDown += 1
                    End If
                Next
            End If

        Catch ex As Exception
            Throw
        End Try
    End Sub

    Private Sub ActualizarPostCambioenDespues(ByVal vdgvRowAntes As DataGridViewRow, _
                                              Optional ByVal vblnCheckedTrue As Boolean = False)
        Try
            'Datos de Cabecera
            Dim intIDServicio_Det As Integer = vdgvRowAntes.Cells("IDServicio_Det").Value
            Dim strDia As String = Format(vdgvRowAntes.Cells("Dia").Value, "dd/MM/yyyy")
            Dim strCapacidadHab As String = vdgvRowAntes.Cells("CapacidadHab").Value
            Dim strEsMatrimonial As String = vdgvRowAntes.Cells("EsMatrimonial").Value

            'Evaluación por Detalle
            For Each ItemDgv As DataGridViewRow In dgvDespues.Rows
                Dim intIDServicio_Det2 As Integer = ItemDgv.Cells("IDServicio_Det2").Value
                Dim strDia2 As String = Format(ItemDgv.Cells("Dia2").Value, "dd/MM/yyyy")
                Dim strCapacidadHab2 As String = ItemDgv.Cells("CapacidadHab2").Value
                Dim strEsMatrimonial2 As String = ItemDgv.Cells("EsMatrimonial2").Value
                Dim strFlagComparado As String = ItemDgv.Cells("FlagComparado2").Value

                If intIDReservaProtecc <> 0 Then
                    intIDServicio_Det = 0
                    intIDServicio_Det2 = 0
                End If

                If intIDServicio_Det = intIDServicio_Det2 And _
                    strDia = strDia2 And strCapacidadHab = strCapacidadHab2 And _
                    strEsMatrimonial = strEsMatrimonial2 Then
                    If Not vblnCheckedTrue Then
                        ItemDgv.Cells("FlagOMEN2").Value = ""
                        BlanquearFondoCeldasxColumna(dgvDespues, ItemDgv.Index)
                    End If
                    ItemDgv.Cells("chkRealizarAcc2").Value = vblnCheckedTrue
                    Exit For
                End If
            Next
        Catch ex As Exception
            Throw
        End Try
    End Sub

    Private Sub btnAceptar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAceptar.Click
        Try
            If chrModulo = "R" Then
                ActualizarReservasxLog()
            ElseIf chrModulo = "O" Then

                ActualizarOperacionesxLog()
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message, ex.Source, MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    'Private Sub ActualizarFlagCambiosVtasAceptadas()
    '    Try
    '        Dim objRes As New clsReservaBT
    '        objRes.ActualizarFlagCambiosVtasAceptadas(strIDReserva, True, gstrUser)

    '    Catch ex As Exception
    '        Throw
    '    End Try
    'End Sub

    'Private Sub ActualizarEstado()
    '    Try
    '        Dim objRes As New clsReservaBT
    '        objRes.ActualizarEstado(strIDReserva, "NV", gstrUser, "", "")

    '    Catch ex As Exception
    '        Throw
    '    End Try

    'End Sub

    Private Sub btnSalir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSalir.Click
        Me.Close()
    End Sub

    Private Sub CargarAntes()

        Dim objBNRes As New clsReservaBN.clsDetalleReservaBN
        Dim dtDetRes As DataTable = objBNRes.ConsultarListxIDCabIDProveedor(intIDCab, strIDProveedor)
        'dtDetRes = objBNRes.ConsultarListxIDFile(strIDFile)


        For Each drDet As DataRow In dtDetRes.Rows
            dgvAntes.Rows.Add()

            For i As Byte = 0 To dtDetRes.Columns.Count - 1
                'If dgvAntes.Columns(i).Name = "Total" Then
                'dgvAntes.Item(i, dgvAntes.Rows.Count - 1).Value = Format(drDet(i), "##,##0.00")
                'Else
                dgvAntes.Item(i, dgvAntes.Rows.Count - 1).Value = drDet(i)
                'End If
            Next

            dgvAntes.Item("FlagComparado", dgvAntes.Rows.Count - 1).Value = 0
            dgvAntes.Item("chkRealizaAcc", dgvAntes.Rows.Count - 1).Value = True
        Next

        'dgvAntes.DataSource = dtDetRes
        'pDgvAnchoColumnas(dgvAntes)
        'dgvAntes.Columns("DiaFormat").Width = 110

        'dgvAntes.Columns("FechaOutFormat").Width = 100
        SepararDiasDgvAntes()

    End Sub

    Private Sub CargarAntesOperaciones()

        Dim objBNRes As New clsDetalleOperacionBN
        Dim dtDetRes As DataTable = objBNRes.ConsultarListxIDCabIDProveedor(intIDCab, strIDProveedor)
        'dtDetRes = objBNRes.ConsultarListxIDFile(strIDFile)


        For Each drDet As DataRow In dtDetRes.Rows
            dgvAntes.Rows.Add()

            For i As Byte = 0 To dtDetRes.Columns.Count - 1
                'If dgvAntes.Columns(i).Name = "Total" Then
                'dgvAntes.Item(i, dgvAntes.Rows.Count - 1).Value = Format(drDet(i), "##,##0.00")
                'Else
                dgvAntes.Item(i, dgvAntes.Rows.Count - 1).Value = drDet(i)
                'End If
            Next

            'dgvAntes.Item("InactivoDet", dgvAntes.Rows.Count - 1).Value = 0
            dgvAntes.Item("FlagComparado", dgvAntes.Rows.Count - 1).Value = 0
            dgvAntes.Item("chkRealizaAcc", dgvAntes.Rows.Count - 1).Value = True
        Next

        'dgvAntes.DataSource = dtDetRes
        'pDgvAnchoColumnas(dgvAntes)
        'dgvAntes.Columns("DiaFormat").Width = 110

        'dgvAntes.Columns("FechaOutFormat").Width = 100
        SepararDiasDgvAntes()

    End Sub

    Private Sub CargarDespues()

        Dim objBNRes As New clsReservaBN.clsDetalleReservaBN
        Dim dtDetRes As DataTable = objBNRes.ConsultarListxIDCabIDProveedorTMP(intIDCab, strIDProveedor)
        'dtDetRes = objBNRes.ConsultarListxIDFile(strIDFile)


        For Each drDet As DataRow In dtDetRes.Rows
            dgvDespues.Rows.Add()

            For i As Byte = 0 To dtDetRes.Columns.Count - 1
                'If dgvDespues.Columns(i).Name = "Total" Then
                'dgvDespues.Item(i, dgvDespues.Rows.Count - 1).Value = Format(drDet(i), "##,##0.00")
                'Else
                dgvDespues.Item(i, dgvDespues.Rows.Count - 1).Value = drDet(i)
                'End If
            Next

            'dgvDespues.Item("InactivoDet", dgvDespues.Rows.Count - 1).Value = 0
            dgvDespues.Item("chkRealizarAcc2", dgvDespues.Rows.Count - 1).Value = True
        Next

        'dgvDespues.DataSource = dtDetRes
        'pDgvAnchoColumnas(dgvDespues)
        'dgvDespues.Columns("DiaFormat2").Width = 110

        'dgvDespues.Columns("FechaOutFormat2").Width = 100

        For i As Byte = 0 To dgvAntes.Columns.Count - 1
            If dgvAntes.Columns(i).Name <> "FlagComparado" And dgvAntes.Columns(i).Name <> "NoEstadoTarifa" Then
                dgvDespues.Columns(i).Width = dgvAntes.Columns(i).Width
                dgvDespues.Columns(i).HeaderText = dgvAntes.Columns(i).HeaderText
                dgvDespues.Columns(i).Visible = dgvAntes.Columns(i).Visible
            End If
        Next

        SepararDiasDgvDespues()

    End Sub

    Private Sub CargarDespuesOperaciones()

        Dim objBNRes As New clsDetalleOperacionBN
        Dim dtDetRes As DataTable = objBNRes.ConsultarListxIDCabIDProveedorTMP(intIDCab, strIDProveedor)
        'dtDetRes = objBNRes.ConsultarListxIDFile(strIDFile)


        For Each drDet As DataRow In dtDetRes.Rows
            dgvDespues.Rows.Add()

            For i As Byte = 0 To dtDetRes.Columns.Count - 1
                'If dgvDespues.Columns(i).Name = "Total" Then
                'dgvDespues.Item(i, dgvDespues.Rows.Count - 1).Value = Format(drDet(i), "##,##0.00")
                'Else
                dgvDespues.Item(i, dgvDespues.Rows.Count - 1).Value = drDet(i)
                'End If
            Next

            'dgvDespues.Item("InactivoDet", dgvDespues.Rows.Count - 1).Value = 0
            dgvDespues.Item("chkRealizarAcc2", dgvDespues.Rows.Count - 1).Value = True
        Next

        'dgvDespues.DataSource = dtDetRes
        'pDgvAnchoColumnas(dgvDespues)
        'dgvDespues.Columns("DiaFormat2").Width = 110

        'dgvDespues.Columns("FechaOutFormat2").Width = 100
        For i As Byte = 0 To dgvAntes.Columns.Count - 1
            If dgvAntes.Columns(i).Name <> "FlagComparado" And dgvAntes.Columns(i).Name <> "NoEstadoTarifa" Then
                dgvDespues.Columns(i).Width = dgvAntes.Columns(i).Width
                dgvDespues.Columns(i).HeaderText = dgvAntes.Columns(i).HeaderText
                dgvDespues.Columns(i).Visible = dgvAntes.Columns(i).Visible
            End If
        Next
        SepararDiasDgvDespues()

    End Sub

    Private Sub SepararDiasDgvAntes()
        Try
            Dim datDiaFila As String = "01/01/1900"

            For Each DgvFila As DataGridViewRow In dgvAntes.Rows

                'If Format(DgvFila.Cells("Dia").Value, "dd/MM/yyyy") <> datDiaFila Then
                If DgvFila.Cells("DiaFormat").Value.ToString.Substring(0, 13) <> datDiaFila Then

                    ''DgvFila.DefaultCellStyle.Font = New Font("Tahoma", 8, FontStyle.Bold, GraphicsUnit.Point)
                    For Each DgvCol As DataGridViewColumn In dgvAntes.Columns
                        If DgvCol.Name = "DiaFormat" Then
                            DgvCol.DefaultCellStyle.Font = New Font("Tahoma", 8, FontStyle.Bold, GraphicsUnit.Point)
                        End If
                    Next



                    'datDiaFila = Format(DgvFila.Cells("Dia").Value.ToString, "dd/MM/yyyy")
                    datDiaFila = DgvFila.Cells("DiaFormat").Value.ToString.Substring(0, 13)
                Else
                    DgvFila.DefaultCellStyle.Font = New Font("Tahoma", 8, FontStyle.Regular, GraphicsUnit.Point)
                End If

            Next

        Catch ex As Exception
            Throw
        End Try
    End Sub

    Private Sub SepararDiasDgvDespues()
        Try
            Dim datDiaFila As String = "01/01/1900"

            For Each DgvFila As DataGridViewRow In dgvDespues.Rows

                'If Format(DgvFila.Cells("Dia").Value, "dd/MM/yyyy") <> datDiaFila Then
                If DgvFila.Cells("DiaFormat2").Value.ToString.Substring(0, 13) <> datDiaFila Then

                    ''DgvFila.DefaultCellStyle.Font = New Font("Tahoma", 8, FontStyle.Bold, GraphicsUnit.Point)
                    For Each DgvCol As DataGridViewColumn In dgvAntes.Columns
                        If DgvCol.Name = "DiaFormat2" Then
                            DgvCol.DefaultCellStyle.Font = New Font("Tahoma", 8, FontStyle.Bold, GraphicsUnit.Point)
                        End If
                    Next



                    'datDiaFila = Format(DgvFila.Cells("Dia").Value.ToString, "dd/MM/yyyy")
                    datDiaFila = DgvFila.Cells("DiaFormat2").Value.ToString.Substring(0, 13)
                Else
                    DgvFila.DefaultCellStyle.Font = New Font("Tahoma", 8, FontStyle.Regular, GraphicsUnit.Point)
                End If

            Next

        Catch ex As Exception
            Throw
        End Try
    End Sub

    Private Sub BlanquearFondoCeldasxColumna(ByRef rDgv As DataGridView, ByVal vintNuFila As Integer)
        Try
            For Each dgvRow As DataGridViewRow In rDgv.Rows
                If dgvRow.Index = vintNuFila Then
                    For Each dgvCol As DataGridViewColumn In rDgv.Columns
                        rDgv.Item(dgvCol.Index, dgvRow.Index).Style.SelectionBackColor = Color.White
                        rDgv.Item(dgvCol.Index, dgvRow.Index).Style.BackColor = Color.White
                    Next
                    Exit For
                End If
            Next
        Catch ex As Exception
            Throw
        End Try
    End Sub

    Private Sub dgvAntes_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvAntes.CellContentClick
        If e.ColumnIndex = -1 Then Exit Sub
        If e.RowIndex = -1 Then Exit Sub
        Try
            dgvAntes.CommitEdit(DataGridViewDataErrorContexts.LeaveControl)
            If e.ColumnIndex = dgvAntes.Columns("chkRealizaAcc").Index Then
                Dim blnRealizarAccion As Boolean = dgvAntes.Item("chkRealizaAcc", e.RowIndex).Value
                Dim strAccion As String = dgvAntes.Item("FlagOMEN", e.RowIndex).Value
                'If chrModulo = "R" Then
                Dim blnUpd As Boolean = False
                If Not blnRealizarAccion Then
                    If strAccion = "Eliminar" Or strAccion = "Nuevo" Then
                        dgvAntes.Rows(e.RowIndex).DefaultCellStyle.SelectionBackColor = Color.White
                        dgvAntes.Rows(e.RowIndex).DefaultCellStyle.BackColor = Color.White
                    ElseIf strAccion = "Actualizar" Then
                        BlanquearFondoCeldasxColumna(dgvAntes, e.RowIndex)
                        ActualizarPostCambioenDespues(dgvAntes.Rows(e.RowIndex))
                        blnUpd = True
                    End If
                    dgvAntes.Item("FlagOMEN", e.RowIndex).Value = ""
                Else
                    If strAccion = "" Then
                        ActualizarPostCambioenDespues(dgvAntes.Rows(e.RowIndex), True)
                    End If
                End If

                If blnRealizarAccion And Not blnUpd Then
                    ActualizarFlagSegundaEvalDespues()
                    EdicionColumnaFlagOMEN(e.RowIndex)
                End If
                'End If
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message, ex.Source, MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub ActualizarFlagSegundaEvalDespues()
        Try
            For Each DgvItem As DataGridViewRow In dgvAntes.Rows
                DgvItem.Cells("FlagComparado").Value = 0
            Next

            For Each DgvItem As DataGridViewRow In dgvDespues.Rows
                DgvItem.Cells("FlagComparado2").Value = 0
            Next
        Catch ex As Exception
            Throw
        End Try
    End Sub

    Private Sub dgvDespues_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvDespues.CellContentClick
        If e.RowIndex = -1 Then Exit Sub
        If e.ColumnIndex = -1 Then Exit Sub
        Try
            dgvDespues.CommitEdit(DataGridViewDataErrorContexts.LeaveControl)
            If e.ColumnIndex = dgvDespues.Columns("chkRealizarAcc2").Index Then
                Dim blnRealizarAccion As Boolean = dgvDespues.Item("chkRealizarAcc2", e.RowIndex).Value
                Dim strAccion As String = dgvDespues.Item("FlagOMEN2", e.RowIndex).Value
                If Not blnRealizarAccion Then
                    If strAccion = "Nuevo" Then
                        dgvDespues.Rows(e.RowIndex).DefaultCellStyle.SelectionBackColor = Color.White
                        dgvDespues.Rows(e.RowIndex).DefaultCellStyle.BackColor = Color.White
                    ElseIf strAccion = "Actualizar" Then
                        BlanquearFondoCeldasxColumna(dgvDespues, e.RowIndex)
                        'ActualizarPostCambioenDespues(dgvAntes.Rows(e.RowIndex), False)
                    End If
                    dgvDespues.Item("FlagOMEN2", e.RowIndex).Value = ""
                End If

                If blnRealizarAccion Then
                    ActualizarFlagSegundaEvalDespues()
                    EdicionColumnaFlagOMEN(-1, e.RowIndex, False)
                End If
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message, ex.Source, MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub
End Class