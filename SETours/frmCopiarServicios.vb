﻿Imports ComSEToursBL2
Public Class frmCopiarServicios
    Public strIDServicio As String
    Public strDescripcion As String
    Public strIDTipoProv As String

    Public strIDProveedor As String = ""
    Public strDescProveedor As String = ""
    Public strIDUbigeo As String = ""
    Public strIDPais As String = ""

    Dim strIDProveedorOrig As String
    Dim strDescProveedorOrig As String
    Dim strIDUbigeoOrig As String
    Dim strIDPaisOrig As String

    Dim blnLoad As Boolean = False
    Dim blnCopio As Boolean = False
    Dim strIDServicioCopiado As String = ""
    Public frmRefMantServiciosProveedor As frmMantServiciosProveedor

    Dim blnDoubleClickLvw As Boolean = False
    Dim blnCamiosDescripcion As Boolean = False

    Private Sub frmCopiarServicios_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Activated
        blnLoad = True
    End Sub

    Private Sub frmCopiarServicios_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Me.Text = "Copia del Servicio: " & strDescripcion

        CrearListView()
        lblProveedor.Text = strDescProveedor
        'txtAnio.Text = Today.Year
        txtDescripcion.BackColor = gColorTextBoxObligat
        'txtAnio.BackColor = gColorTextBoxObligat
        strIDProveedorOrig = strIDProveedor
        strDescProveedorOrig = strDescProveedor
        strIDPaisOrig = strIDPais
        strIDUbigeoOrig = strIDUbigeo


        Try
            CargarCombos()
            cboCiudad.SelectedValue = strIDUbigeo

            Dim objBN As New clsServicioProveedorBN
            dgv.DataSource = objBN.ConsultarCopias(strIDServicio)
            pDgvAnchoColumnas(dgv)
            blnCamiosDescripcion = False
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Error en " & Err.Source, MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub btnSalir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSalir.Click
        'Me.Close()
        Me.Dispose()
    End Sub
    Private Sub frmCopiarServicios_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        Select Case e.KeyCode
            Case Keys.F6
                btnAceptar_Click(Nothing, Nothing)
            Case Keys.Escape
                btnSalir_Click(Nothing, Nothing)
        End Select
    End Sub

    Private Sub CargarCombos()
        Try
            Dim objBLUbig As New clsTablasApoyoBN.clsUbigeoBN
            pCargaCombosBox(cboPais, objBLUbig.ConsultarCbo("PAIS"))

            cboPais.SelectedValue = strIDPais
            cboPais_SelectionChangeCommitted(Nothing, Nothing)

        Catch ex As Exception
            Throw
        End Try
    End Sub

    Private Sub cboPais_SelectionChangeCommitted(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboPais.SelectionChangeCommitted
        If cboPais.SelectedValue Is Nothing Then Exit Sub
        Try
            Dim objBLUbig As New clsTablasApoyoBN.clsUbigeoBN
            pCargaCombosBox(cboCiudad, objBLUbig.ConsultarCboparaProvee(, cboPais.SelectedValue.ToString))

        Catch ex As Exception
            MessageBox.Show(ex.Message, ex.Source, MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub btnProveedor_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnProveedor.Click

        Dim frmAyuda As New frmAyudas
        With frmAyuda
            .strCaller = Me.Name
            .frmRefCopiaServicios = Me
            .strTipoBusqueda = "Proveedor"

            .strPar = strIDTipoProv
            .Text = "Consulta de Proveedores"
            .ShowDialog()
        End With

    End Sub

    Private Sub btnAceptar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAceptar.Click
        Try
            If Not blnValidarIngresos() Then Exit Sub

            Dim objBL As New clsServicioProveedorBT

            dgv.DataSource = objBL.CopiarServicios(strIDServicio, txtDescripcion.Text, strIDProveedor, _
                                                   strIDUbigeo, _
                                                   txtAnio.Text, _
                                                   If(txtTipo.Text.Trim = "", "*SF*", txtTipo.Text), _
                                                   gstrUser)
            pDgvAnchoColumnas(dgv)

            'btnAceptar.Enabled = False
            MessageBox.Show("Copia realizada exitosamente", gstrTituloMsg, MessageBoxButtons.OK, MessageBoxIcon.Information)

            blnCopio = True
            'If dgv.Rows.Count > 0 Then
            'strIDServicioCopiado = dgv.Item("IDServicio", 0).Value.ToString
            For Each dgvRow As DataGridViewRow In dgv.Rows
                If dgvRow.Cells("Descripcion").Value.ToString = txtDescripcion.Text Then
                    strIDServicioCopiado = dgvRow.Cells("IDServicio").Value.ToString
                    Exit For
                End If
            Next
            'End If
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Error en " & Err.Source, MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub
    Private Function blnValidarIngresos() As Boolean
        Dim blnOk As Boolean = True

        If txtDescripcion.Text.Trim = "" Then
            ErrPrv.SetError(txtDescripcion, "Debe ingresar Descripción")

            blnOk = False
        End If

        'If txtAnio.Text.Trim = "" Then
        '    ErrPrv.SetError(txtAnio, "Debe ingresar Año")

        '    blnOk = False
        'End If

        If lblProveedor.Text.Trim = "" Then
            ErrPrv.SetError(btnProveedor, "Debe seleccionar Proveedor")

            blnOk = False
        End If
        If cboCiudad.Text = "" Then
            ErrPrv.SetError(cboCiudad, "Debe seleccionar Ubigeo")

            blnOk = False
        End If

        If blnCamiosDescripcion Then
            blnDoubleClickLvw = False
            txtDescripcion_TextChanged(txtDescripcion, Nothing)
        End If

        If blnOk = True Then
            If lvw.Visible = True Then
                If MessageBox.Show("Existen servicios similares ya registradas, Desea Ud. grabar de todas formas?", gstrTituloMsg, MessageBoxButtons.YesNo, MessageBoxIcon.Question) = Windows.Forms.DialogResult.Yes Then
                    blnOk = True
                Else
                    blnOk = False
                End If
            End If
        End If

        If Not blnOk Then
            Return False
        Else
            Return True
        End If
    End Function

    Private Sub CrearListView()

        With lvw
            .View = View.Details
            .FullRowSelect = True
            '.GridLines = True
            .LabelEdit = False
            .HideSelection = False

            .Columns.Clear()
            .Columns.Add("Descripción", 250, HorizontalAlignment.Left)

        End With
    End Sub

    Private Sub txtDescripcion_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtDescripcion.TextChanged, _
        lblProveedor.TextChanged

        Select Case sender.name
            Case "txtDescripcion"
                ErrPrv.SetError(txtDescripcion, "")
                blnCamiosDescripcion = True
                If blnLoad Then
                    If txtDescripcion.Text.Length >= 3 Then
                        Dim objBL As New clsServicioProveedorBN
                        Dim dt As New DataTable
                        dt = objBL.ConsultarLvw(strIDServicio, txtDescripcion.Text)
                        pCargaListView(lvw, dt, dt.Columns.Count - 1, 0)
                        If dt.Rows.Count > 0 Then
                            If Not blnDoubleClickLvw Then
                                lvw.Visible = True
                                ErrPrv.SetError(lvw, "Existen descripciones similares ya registradas")
                            End If
                        Else
                            lvw.Visible = False
                            ErrPrv.SetError(lvw, "")
                        End If
                    Else
                        lvw.Visible = False
                        ErrPrv.SetError(lvw, "")
                    End If
                End If

                'Case "txtAnio"
                '    ErrPrv.SetError(txtAnio, "")
            Case "lblProveedor"
                ErrPrv.SetError(btnProveedor, "")
        End Select

    End Sub

    Private Sub txtAnio_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtAnio.KeyPress
        If Asc(e.KeyChar) <> 8 Then
            If Not IsNumeric(e.KeyChar) Then
                e.KeyChar = ""
            End If
        End If
    End Sub

    Private Sub txtAnio_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtAnio.TextChanged

    End Sub

    Private Sub chkMismoProvee_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkMismoProvee.CheckedChanged
        If chkMismoProvee.Checked Then
            btnProveedor.Enabled = False
            strIDProveedor = strIDProveedorOrig
            strDescProveedor = strDescProveedorOrig
            lblProveedor.Text = strDescProveedorOrig
        Else
            btnProveedor.Enabled = True
            lblProveedor.Text = ""
            strIDProveedor = ""
        End If
    End Sub

    Private Sub frmCopiarServicios_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        frmRefMantServiciosProveedor.blnCopio = blnCopio
        frmRefMantServiciosProveedor.strIDServicioCopiado = strIDServicioCopiado

    End Sub

    Private Sub lvw_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles lvw.DoubleClick
        Try
            lvw.Visible = False
            blnDoubleClickLvw = True

            Dim strSelItem As String = lvw.SelectedItems.Item(0).Text
            txtDescripcion.Text = strSelItem

            blnDoubleClickLvw = False
        Catch ex As Exception
            MessageBox.Show(ex.Message, ex.Source, MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub chkMismoUbigeo_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkMismoUbigeo.CheckedChanged
        If Not blnLoad Then Exit Sub
        If chkMismoUbigeo.Checked Then
            strIDUbigeo = strIDUbigeoOrig
            cboPais.SelectedValue = strIDPaisOrig
            cboCiudad.SelectedValue = strIDUbigeoOrig
            cboPais.Enabled = False
            cboCiudad.Enabled = False
        Else
            'cboPais.SelectedIndex = -1
            cboCiudad.SelectedIndex = -1
            strIDUbigeo = ""
            cboPais.Enabled = True
            cboCiudad.Enabled = True

        End If
    End Sub

    Private Sub cboCiudad_SelectionChangeCommitted(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboCiudad.SelectionChangeCommitted
        strIDUbigeo = cboCiudad.SelectedValue.ToString
    End Sub

    Private Sub lvw_SelectedIndexChanged(sender As Object, e As EventArgs) Handles lvw.SelectedIndexChanged

    End Sub
End Class