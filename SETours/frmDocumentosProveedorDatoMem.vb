﻿Imports ComSEToursBL2
Imports ComSEToursBE2
Public Class frmDocumentosProveedorDatoMem
    Public frmRef As frmDocumentosProveedorDato
    Public frmRef_FF As Object 'frmDocumentosProveedorDato_FondoFijo
    'Public frmRefC As frmIngNotaVentaCounter
    Public bytNuSegmento As Byte = 0
    Public strDescripcion As String = ""
    Dim strModoEdicion As String = ""
    Public strIDProv_LA As String = ""
    Dim blnCambios As Boolean = False
    Dim blnLoad As Boolean = False
    Public strCoProducto As String = ""
    Dim strCoAlmacen As String = ""
    Private Sub frmIngTicketDatoMem_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        Select Case e.KeyCode
            Case Keys.F6
                If btnAceptar.Enabled Then btnAceptar_Click(Nothing, Nothing)
            Case Keys.Escape
                pSalir(Nothing, Nothing)
        End Select
    End Sub

    Private Sub frmIngTicketDato_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        If blnCambios Then
            If MessageBox.Show("Se han realizado cambios. ¿Está Ud. seguro de salir sin grabar?", "Confirmar", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = Windows.Forms.DialogResult.Yes Then
                e.Cancel = False
            Else
                e.Cancel = True
            End If
        End If
    End Sub

    Private Sub pSalir(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Me.Close()
    End Sub

    Private Sub frmIngTicketDatoMem_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            'btnTicket.Visible = False

            ColorearControlesObligatorios()
            CargarCombos()
            txtIGV.Text = "0.00"
            If bytNuSegmento = 0 Then
                ModoNuevo()
            Else
                ModoEditar()
                CargarControles()
            End If
            blnLoad = True
        Catch ex As Exception
            MessageBox.Show(ex.Message, ex.Source, MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub


    Private Sub ValidarIGV()

        Try

            If frmRef Is Nothing Then
                If Not frmRef_FF Is Nothing Then
                    With frmRef_FF
                        If .chkIGV.Text = "IGV:" Then
                            If .strTipOpe = "001" Or .strTipOpe = "012" Or .strTipOpe = "002" Then
                                '.chkIGV.Checked = True
                                txtIGV.Enabled = True
                            Else
                                '.chkIGV.Checked = False
                                txtIGV.Enabled = False
                            End If
                        End If

                    End With
                End If
            Else
                With frmRef

                    With frmRef

                        If .chkIGV.Text = "IGV:" Then
                            If .strTipOpe = "001" Or .strTipOpe = "012" Or .strTipOpe = "002" Then
                                '.chkIGV.Checked = True
                                txtIGV.Enabled = True
                            Else
                                '.chkIGV.Checked = False
                                txtIGV.Enabled = False
                            End If
                        End If

                    End With


                End With
            End If



        Catch ex As Exception
            Throw
        End Try

    End Sub
    Private Sub CargarCombos()
        Try
            Dim objAlm As New clsMAAlmacenBN
            pCargaCombosBox(cboAlmacen, objAlm.ConsultarCbo(), True)
        Catch ex As Exception
            Throw
        End Try
    End Sub

    Private Sub CargarControles()
        Try

            If frmRef Is Nothing Then
                If Not frmRef_FF Is Nothing Then
                    With frmRef_FF
                        Dim vintIndex As Integer = frmRef_FF.dgvDetalle.CurrentRow.Index
                        With frmRef_FF.dgvDetalle

                            txtProducto.Text = .Item("Producto", vintIndex).Value.ToString.Trim
                            strCoProducto = .Item("CoProducto", vintIndex).Value.ToString.Trim
                            'cboAlmacen.SelectedValue = .Item("coalmacen", vintIndex).Value.ToString.Trim
                            strCoAlmacen = .Item("coalmacen", vintIndex).Value.ToString.Trim
                            txtAlmacen.Text = .Item("Almacen", vintIndex).Value.ToString.Trim
                            txtSSMonto.Text = Format(CDbl(.Item("SsMonto", vintIndex).Value.ToString.Trim), "###,##0.00")
                            txtQtCantidad.Text = .Item("QtCantidad", vintIndex).Value.ToString.Trim
                            txtPrecio.Text = .Item("SSPrecUni", vintIndex).Value.ToString.Trim
                            txtIGV.Text = .Item("SsIGV", vintIndex).Value.ToString.Trim
                            txtTotal.Text = .Item("SsTotal", vintIndex).Value.ToString.Trim

                        End With


                    End With
                End If
            Else
                With frmRef
                    Dim vintIndex As Integer = frmRef.dgvDetalle.CurrentRow.Index
                    With frmRef.dgvDetalle


                        txtProducto.Text = .Item("Producto", vintIndex).Value.ToString.Trim
                        strCoProducto = .Item("CoProducto", vintIndex).Value.ToString.Trim
                        'cboAlmacen.SelectedValue = .Item("coalmacen", vintIndex).Value.ToString.Trim
                        strCoAlmacen = .Item("coalmacen", vintIndex).Value.ToString.Trim
                        txtAlmacen.Text = .Item("Almacen", vintIndex).Value.ToString.Trim
                        txtSSMonto.Text = Format(CDbl(.Item("SsMonto", vintIndex).Value.ToString.Trim), "###,##0.00")
                        txtQtCantidad.Text = .Item("QtCantidad", vintIndex).Value.ToString.Trim
                        txtPrecio.Text = .Item("SSPrecUni", vintIndex).Value.ToString.Trim
                        txtIGV.Text = .Item("SsIGV", vintIndex).Value.ToString.Trim
                        txtTotal.Text = .Item("SsTotal", vintIndex).Value.ToString.Trim

                    End With


                End With
            End If



        Catch ex As Exception
            Throw
        End Try
    End Sub

    Private Sub ModoEditar()
        strModoEdicion = "E"
        ' If frmRefC Is Nothing Then
        lblTitulo.Text = "Edición del Item: " & strDescripcion
        Me.Text = "Edición del Item: " & strDescripcion
      
    End Sub

    Private Sub ModoNuevo()
        strModoEdicion = "N"
    End Sub

    Private Sub Grabar()
        Try
            If Not frmRef Is Nothing Then
                With frmRef

                    .dgvDetalle.Rows.Add()
                    Dim vintIndex As Integer = .dgvDetalle.Rows.Count - 1
                    With .dgvDetalle

                        .Item("NuDocumProvDet", vintIndex).Value = frmRef.bytGenerarNuItem
                        .Item("CoProducto", vintIndex).Value = strCoProducto
                        .Item("Producto", vintIndex).Value = txtProducto.Text.Trim
                        .Item("coalmacen", vintIndex).Value = strCoAlmacen 'cboAlmacen.SelectedValue
                        .Item("Almacen", vintIndex).Value = txtAlmacen.Text.Trim 'cboAlmacen.Text
                        .Item("QtCantidad", vintIndex).Value = txtQtCantidad.Text.Trim 'Format(CDbl(txtQtCantidad.Text.Trim), "###,##0.00")
                        .Item("SsMonto", vintIndex).Value = Format(CDbl(txtSSMonto.Text.Trim), "###,##0.00")
                        .Item("SSPrecUni", vintIndex).Value = Format(CDbl(txtPrecio.Text.Trim), "###,##0.00")
                        .Item("SsIGV", vintIndex).Value = Format(CDbl(txtIGV.Text.Trim), "###,##0.00")
                        .Item("SsTotal", vintIndex).Value = Format(CDbl(txtTotal.Text.Trim), "###,##0.00")

                    End With
                End With

            Else
                If Not frmRef_FF Is Nothing Then
                    With frmRef_FF

                        .dgvDetalle.Rows.Add()
                        Dim vintIndex As Integer = .dgvDetalle.Rows.Count - 1
                        With .dgvDetalle

                            .Item("NuDocumProvDet", vintIndex).Value = frmRef_FF.bytGenerarNuItem
                            .Item("CoProducto", vintIndex).Value = strCoProducto
                            .Item("Producto", vintIndex).Value = txtProducto.Text.Trim
                            '.Item("coalmacen", vintIndex).Value = cboAlmacen.SelectedValue
                            '.Item("Almacen", vintIndex).Value = cboAlmacen.Text
                            .Item("coalmacen", vintIndex).Value = strCoAlmacen 'cboAlmacen.SelectedValue
                            .Item("Almacen", vintIndex).Value = txtAlmacen.Text.Trim 'cboAlmacen.Text
                            .Item("QtCantidad", vintIndex).Value = txtQtCantidad.Text.Trim 'Format(CDbl(txtQtCantidad.Text.Trim), "###,##0.00")
                            .Item("SsMonto", vintIndex).Value = Format(CDbl(txtSSMonto.Text.Trim), "###,##0.00")
                            .Item("SSPrecUni", vintIndex).Value = Format(CDbl(txtPrecio.Text.Trim), "###,##0.00")
                            .Item("SsIGV", vintIndex).Value = Format(CDbl(txtIGV.Text.Trim), "###,##0.00")
                            .Item("SsTotal", vintIndex).Value = Format(CDbl(txtTotal.Text.Trim), "###,##0.00")



                        End With
                    End With

                End If
            End If
           
        Catch ex As Exception
            Throw
        End Try
    End Sub

    Private Sub Actualizar()
        Try
            If Not frmRef Is Nothing Then
                With frmRef
                    Dim vintIndex As Integer = .dgvDetalle.CurrentRow.Index
                    With .dgvDetalle

                        '.Item("NuDocumProvDet", vintIndex).Value = frmRef.bytGenerarNuItem
                        .Item("CoProducto", vintIndex).Value = strCoProducto
                        .Item("Producto", vintIndex).Value = txtProducto.Text.Trim
                        '.Item("coalmacen", vintIndex).Value = cboAlmacen.SelectedValue
                        '.Item("Almacen", vintIndex).Value = cboAlmacen.Text
                        .Item("coalmacen", vintIndex).Value = strCoAlmacen 'cboAlmacen.SelectedValue
                        .Item("Almacen", vintIndex).Value = txtAlmacen.Text.Trim 'cboAlmacen.Text
                        .Item("QtCantidad", vintIndex).Value = txtQtCantidad.Text.Trim 'Format(CDbl(txtQtCantidad.Text.Trim), "###,##0.00")
                        .Item("SsMonto", vintIndex).Value = Format(CDbl(txtSSMonto.Text.Trim), "###,##0.00")
                        .Item("SSPrecUni", vintIndex).Value = Format(CDbl(txtPrecio.Text.Trim), "###,##0.00")
                        .Item("SsIGV", vintIndex).Value = Format(CDbl(txtIGV.Text.Trim), "###,##0.00")
                        .Item("SsTotal", vintIndex).Value = Format(CDbl(txtTotal.Text.Trim), "###,##0.00")

                    End With
                End With
            Else
                If Not frmRef_FF Is Nothing Then
                    With frmRef_FF

                        ' .dgvDetalle.Rows.Add()
                        Dim vintIndex As Integer = .dgvDetalle.CurrentRow.Index
                        With .dgvDetalle

                            '.Item("NuDocumProvDet", vintIndex).Value = frmRef.bytGenerarNuItem
                            .Item("CoProducto", vintIndex).Value = strCoProducto
                            .Item("Producto", vintIndex).Value = txtProducto.Text.Trim
                            '.Item("coalmacen", vintIndex).Value = cboAlmacen.SelectedValue
                            '.Item("Almacen", vintIndex).Value = cboAlmacen.Text
                            .Item("coalmacen", vintIndex).Value = strCoAlmacen 'cboAlmacen.SelectedValue
                            .Item("Almacen", vintIndex).Value = txtAlmacen.Text.Trim 'cboAlmacen.Text
                            .Item("QtCantidad", vintIndex).Value = txtQtCantidad.Text.Trim 'Format(CDbl(txtQtCantidad.Text.Trim), "###,##0.00")
                            .Item("SsMonto", vintIndex).Value = Format(CDbl(txtSSMonto.Text.Trim), "###,##0.00")
                            .Item("SSPrecUni", vintIndex).Value = Format(CDbl(txtPrecio.Text.Trim), "###,##0.00")
                            .Item("SsIGV", vintIndex).Value = Format(CDbl(txtIGV.Text.Trim), "###,##0.00")
                            .Item("SsTotal", vintIndex).Value = Format(CDbl(txtTotal.Text.Trim), "###,##0.00")



                        End With
                    End With

                End If
            End If
           
        Catch ex As Exception
            Throw
        End Try
    End Sub

    Private Sub btnAceptar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAceptar.Click
        If Not blnValidarIngresos() Then Exit Sub
        Try
            If strModoEdicion = "N" Then
                Grabar()
            ElseIf strModoEdicion = "E" Then
                Actualizar()
            End If
            If Not frmRef Is Nothing Then
                frmRef.blnActualizoItinerario = True
            Else
                If Not frmRef_FF Is Nothing Then
                    frmRef_FF.blnActualizoItinerario = True
                End If
            End If

            blnCambios = False
            Me.Close()
        Catch ex As Exception
            MessageBox.Show(ex.Message, ex.Source, MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Function blnValidarIngresos() As Boolean
        Try
            Dim blnOk As Boolean = True
         

            If txtProducto.Text.Trim = "" Then
                txtProducto.Focus()
                ErrProv.SetError(txtProducto, "Debe ingresar un producto")
                blnOk = False
            End If

            If txtQtCantidad.Text.Trim = "" Then
                txtQtCantidad.Focus()
                ErrProv.SetError(txtQtCantidad, "Debe ingresar una cantidad")
                blnOk = False
            End If

            If txtPrecio.Text.Trim = "" Then
                txtPrecio.Focus()
                ErrProv.SetError(txtPrecio, "Debe ingresar un precio unitario")
                blnOk = False
            End If

            If txtSSMonto.Text.Trim = "" Then
                txtSSMonto.Focus()
                ErrProv.SetError(txtSSMonto, "Debe ingresar un monto")
                blnOk = False
            End If

         
            Return blnOk
        Catch ex As Exception
            Throw
        End Try
    End Function


    Private Sub ColorearControlesObligatorios()
        Try
            'cboCoCiudadLle.BackColor = gColorTextBoxObligat
            txtProducto.BackColor = gColorTextBoxObligat
            'txtNuDocumento.BackColor = gColorTextBoxObligat
            'lblCodProveedor.BackColor = gColorTextBoxObligat
            txtAlmacen.BackColor = gColorTextBoxObligat
            txtSSMonto.BackColor = gColorTextBoxObligat
            txtQtCantidad.BackColor = gColorTextBoxObligat
            txtPrecio.BackColor = gColorTextBoxObligat

        Catch ex As Exception
            Throw
        End Try
    End Sub

    Private Sub btnSalir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSalir.Click
        pSalir(Nothing, Nothing)
    End Sub

    'Private Sub txtCoLineaAerea_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtSSMonto.TextChanged
    '    If sender Is Nothing Then Exit Sub
    '    ErrProv.SetError(DirectCast(sender, Control), "")

    'End Sub

    Private Sub CalcularIGV()
        Try
            Dim intPax As Integer = 0
            Dim dblUnidad As Double = 0
            Dim dblMonto As Double = 0
            Dim dblTotal As Double = 0
            Dim dblIGV As Double = 0
            If txtQtCantidad.Text.Length > 0 Then
                intPax = CInt(txtQtCantidad.Text)
            End If

            If txtPrecio.Text.Length > 0 Then
                dblUnidad = CDbl(txtPrecio.Text)
            End If

            dblMonto = intPax * dblUnidad

            If frmRef Is Nothing Then
                If Not frmRef_FF Is Nothing Then
                    With frmRef_FF
                        If .chkIGV.Text = "IGV:" Then
                            If .strTipOpe = "001" Or .strTipOpe = "012" Or .strTipOpe = "002" Then
                                '.chkIGV.Checked = True
                                txtIGV.Enabled = True
                                Dim objBN As New clsTablasApoyoBN.clsParametroBN
                                Dim dtParametros As New DataTable
                                dtParametros = objBN.ConsultarList()
                                If dtParametros.Rows.Count > 0 Then
                                    If txtIGV.Enabled = True Then
                                        dblIGV = CDbl(dtParametros.Rows(0).Item("NuIGV").ToString)
                                        dblIGV = dblMonto * (dblIGV / 100)
                                    End If
                                End If
                            Else
                                '.chkIGV.Checked = False
                                txtIGV.Enabled = False
                                txtIGV.Text = "0.00"
                            End If
                        End If

                    End With
                End If
            Else
                With frmRef

                    With frmRef

                        If .chkIGV.Text = "IGV:" Then
                            If .strTipOpe = "001" Or .strTipOpe = "012" Or .strTipOpe = "002" Then
                                '.chkIGV.Checked = True
                                txtIGV.Enabled = True
                                Dim objBN As New clsTablasApoyoBN.clsParametroBN
                                Dim dtParametros As New DataTable
                                dtParametros = objBN.ConsultarList()
                                If dtParametros.Rows.Count > 0 Then
                                    If txtIGV.Enabled = True Then
                                        dblIGV = CDbl(dtParametros.Rows(0).Item("NuIGV").ToString)
                                        dblIGV = dblMonto * (dblIGV / 100)
                                    End If
                                End If
                            Else
                                '.chkIGV.Checked = False
                                txtIGV.Enabled = False
                                txtIGV.Text = "0.00"
                            End If
                        End If

                    End With


                End With
            End If

            txtIGV.Text = Format(dblIGV, "###,##0.00")

        Catch ex As Exception
            Throw
        End Try
    End Sub

    Private Sub pCalcularTotal()
        Dim intPax As Integer = 0
        Dim dblUnidad As Double = 0
        Dim dblMonto As Double = 0
        Dim dblTotal As Double = 0
        Dim dblIGV As Double = 0
        If txtQtCantidad.Text.Length > 0 Then
            intPax = CInt(txtQtCantidad.Text)
        End If

        If txtPrecio.Text.Length > 0 Then
            dblUnidad = CDbl(txtPrecio.Text)
        End If

        dblMonto = intPax * dblUnidad

        ''calcular igv
        'Dim objBN As New clsTablasApoyoBN.clsParametroBN
        'Dim dtParametros As New DataTable
        'dtParametros = objBN.ConsultarList()
        'If dtParametros.Rows.Count > 0 Then
        '    If txtIGV.Enabled = True Then
        '        dblIGV = CDbl(dtParametros.Rows(0).Item("NuIGV").ToString)
        '        dblIGV = dblMonto * (dblIGV / 100)
        '    End If
        'End If


        ''fin calcular igv


        If txtIGV.Text.Length > 0 Then
            dblIGV = CDbl(txtIGV.Text)
        End If


        dblTotal = dblMonto + dblIGV

        'txtSSTotal.Text = dblTotal
        txtSSMonto.Text = Format(dblMonto, "###,##0.00")
        txtTotal.Text = Format(dblTotal, "###,##0.00")
    End Sub

    Private Sub pCalcularTotal2()

        Dim dblMonto As Double = 0
        Dim dblTotal As Double = 0
        Dim dblIGV As Double = 0
     
        If txtSSMonto.Text.Length > 0 Then
            dblMonto = CDbl(txtSSMonto.Text)
        End If

        If txtIGV.Text.Length > 0 Then
            dblIGV = CDbl(txtIGV.Text)
        End If

        dblTotal = dblMonto + dblIGV

        'txtSSTotal.Text = dblTotal
        txtSSMonto.Text = Format(dblMonto, "###,##0.00")
        txtTotal.Text = Format(dblTotal, "###,##0.00")
    End Sub


    Private Sub btnTicket_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Try
          
        Catch ex As Exception
            MessageBox.Show(ex.Message, ex.Source, MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub


    Private Sub Importes_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles _
                                   txtSSMonto.KeyPress, txtQtCantidad.KeyPress, txtPrecio.KeyPress, txtIGV.KeyPress



        Dim strCaracteres As String
        strCaracteres = "0123456789."

        'If sender.name = "txtIDServicio_Det" Then
        '    strCaracteres = "0123456789"
        'End If
        If sender.name = "txtQtCantidad" Then
            strCaracteres = "0123456789"
        End If
        If Asc(e.KeyChar) <> 8 Then
            If InStr(strCaracteres, e.KeyChar) = 0 Then
                e.KeyChar = ""
            End If
        End If


    End Sub

    Private Sub Validaciones_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtSSMonto.TextChanged, txtQtCantidad.TextChanged, _
       txtProducto.TextChanged, txtIGV.TextChanged
        If Not blnLoad Then Exit Sub
        Try
            If sender IsNot Nothing Then ErrProv.SetError(CType(sender, Control), "")
            blnCambios = True



        Catch ex As Exception
            MessageBox.Show(ex.Message, ex.Source, MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub txtCoLineaAerea_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtQtCantidad.TextChanged, txtSSMonto.TextChanged, txtPrecio.TextChanged, txtTotal.TextChanged, txtIGV.TextChanged
        If sender Is Nothing Then Exit Sub
        ErrProv.SetError(DirectCast(sender, Control), "")

        If sender.Name = "txtQtCantidad" Or sender.Name = "txtPrecio" Then
            CalcularIGV()
            pCalcularTotal()

        Else
            If sender.Name = "txtSSMonto" Or sender.Name = "txtIGV" Then
                pCalcularTotal2()
            End If
            'If IsNumeric(txtSSUnidad.Text) Then


            '    txtSSUnidad.Text = Format(CDbl(txtSSUnidad.Text), "###,##0.00")
            '    txtSSTotal.Text = Format(CDbl(txtSSTotal.Text), "###,##0.00")
            'End If
        End If
    End Sub

    Private Sub btnTicket_Click_1(sender As Object, e As EventArgs) Handles btnTicket.Click
        Dim frmAyuda As New frmAyudas
        With frmAyuda
            .strCaller = Me.Name
            .strTipoBusqueda = "Producto"
            .frmRefDocumentosProveedorDatoMem = Me
            .strPar = "" 'gstrTipoProveeAdminist

            .Text = "Consulta de Productos"
            .ShowDialog()
            If strCoProducto <> "" Then
                ErrProv.SetError(btnTicket, "")
                'almacen
                Using objBN As New clsProductosBN
                    Using dr As SqlClient.SqlDataReader = objBN.ConsultarPk(strCoProducto)
                        dr.Read()
                        If dr.HasRows Then
                            txtAlmacen.Text = dr("NoAlmacen").ToString
                            strCoAlmacen = dr("CoAlmacen").ToString
                        End If
                        dr.Close()
                    End Using
                End Using

                'DevolverDatosProveedor_SAP()
            End If

        End With
    End Sub
End Class