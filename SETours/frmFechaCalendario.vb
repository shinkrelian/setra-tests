﻿
Public Class frmFechaCalendario
    Public frmRefIngCotizacionDato As frmIngCotizacionDato
    '  Public frmRefIngReservasControlporFileDato As frmIngReservasControlporFileDato
    'Dim datFecha As Date = Today.Date

    Public datFecha As Date
    Dim blnClick As Boolean = False

    Dim timHoraIni As System.TimeSpan, timHoraFin As System.TimeSpan

    Private Sub frmFechaCalendario_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        Select Case e.KeyCode
            Case Keys.Escape
                Me.Dispose()
        End Select
    End Sub
    Private Sub frmFechaCalendario_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        '   calFecha.TodayDate = datFecha

        'calFecha.SelectionRange.Start = datFecha

        calFecha.SetDate(datFecha)

    End Sub

    Private Sub calFecha_DateChanged(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DateRangeEventArgs) Handles calFecha.DateChanged

    End Sub

    Private Sub calFecha_DateSelected(ByVal sender As Object, ByVal e As System.Windows.Forms.DateRangeEventArgs) Handles calFecha.DateSelected
        'MessageBox.Show(calFecha.SelectionRange.Start)

        'If datFecha <> calFecha.SelectionRange.Start Then
        '    datFecha = calFecha.SelectionRange.Start
        '    blnClick = False
        'End If
    End Sub

    Private Sub calFecha_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles calFecha.KeyDown
        Select Case e.KeyCode
            Case Keys.Enter
                DobleClickSeleccFecha()
            Case Keys.Space
                DobleClickSeleccFecha()
        End Select
    End Sub

    Private Sub calFecha_MouseDown(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles calFecha.MouseDown
        '1ero

        If e.Button = Windows.Forms.MouseButtons.Left Then

            If datFecha <> calFecha.SelectionRange.Start Then
                datFecha = calFecha.SelectionRange.Start
                blnClick = False

                'intHoraIni = Mid(Now.TimeOfDay.ToString, InStr(Now.TimeOfDay.ToString, ".") + 1)
                timHoraIni = Now.TimeOfDay
            End If



            If blnClick Then
                'MessageBox.Show("Doble Click!!!!!!")
                timHoraFin = Now.TimeOfDay

                'If calFecha.SelectionRange.Start = datFecha Then GoTo Paso

                'If (intHoraFin - intHoraIni) < 2000000 Then

                If Replace(timHoraFin.ToString, ":", "") - Replace(timHoraIni.ToString, ":", "") <= 1 Then
                    'Paso:
                    DobleClickSeleccFecha()
                End If

                blnClick = False
            Else
                blnClick = True
            End If

        End If
    End Sub

    Private Sub DobleClickSeleccFecha()
        If Not frmRefIngCotizacionDato Is Nothing Then
            With frmRefIngCotizacionDato
                .datFechaSel = calFecha.SelectionRange.Start
            End With
        Else
            'With frmRefIngReservasControlporFileDato
            '    .datFechaSel = calFecha.SelectionRange.Start
            'End With
        End If
        'Me.Dispose()
        Me.Close()
    End Sub

End Class