﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmDocumentosProveedorDato
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle5 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle10 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle6 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle7 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle8 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle9 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmDocumentosProveedorDato))
        Me.gprLinea1 = New System.Windows.Forms.GroupBox()
        Me.btnGrabar = New System.Windows.Forms.Button()
        Me.btnLimpiarFile = New System.Windows.Forms.Button()
        Me.lblIDFile = New System.Windows.Forms.Label()
        Me.btnConsultarFile = New System.Windows.Forms.Button()
        Me.lblEtiqFile = New System.Windows.Forms.Label()
        Me.cboGastos = New System.Windows.Forms.ComboBox()
        Me.cboTipoSAP = New System.Windows.Forms.ComboBox()
        Me.Label24 = New System.Windows.Forms.Label()
        Me.gbxDocumentoVinculado = New System.Windows.Forms.GroupBox()
        Me.txtSerie_Ref = New System.Windows.Forms.TextBox()
        Me.Label23 = New System.Windows.Forms.Label()
        Me.dtpFechaEmision_Ref = New System.Windows.Forms.DateTimePicker()
        Me.Label20 = New System.Windows.Forms.Label()
        Me.txtNroDocumento_DocumVinc = New System.Windows.Forms.TextBox()
        Me.Label21 = New System.Windows.Forms.Label()
        Me.cboTipoDocumento_DocumVinc = New System.Windows.Forms.ComboBox()
        Me.Label22 = New System.Windows.Forms.Label()
        Me.cboCentroControl = New System.Windows.Forms.ComboBox()
        Me.lblCentroControl = New System.Windows.Forms.Label()
        Me.txtPercepcion = New System.Windows.Forms.TextBox()
        Me.lblPercepcion = New System.Windows.Forms.Label()
        Me.dtpFechaVencimiento = New System.Windows.Forms.DateTimePicker()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.txtConcepto = New System.Windows.Forms.TextBox()
        Me.lblGlosa = New System.Windows.Forms.Label()
        Me.cboCentroCostos = New System.Windows.Forms.ComboBox()
        Me.lblCentroCostos = New System.Windows.Forms.Label()
        Me.cboCuentaContable = New System.Windows.Forms.ComboBox()
        Me.txtSerie = New System.Windows.Forms.TextBox()
        Me.lblCuenta = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.cboTipoOperacion = New System.Windows.Forms.ComboBox()
        Me.lblEtiqProveedor = New System.Windows.Forms.Label()
        Me.lblDestino = New System.Windows.Forms.Label()
        Me.lblProveedor = New System.Windows.Forms.Label()
        Me.btnSelProveedor = New System.Windows.Forms.Button()
        Me.grbTipoCambioFormaEgreso = New System.Windows.Forms.GroupBox()
        Me.txtTipoCambioFEgreso = New System.Windows.Forms.TextBox()
        Me.txtTotalFEgreso = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.chkIGV = New System.Windows.Forms.CheckBox()
        Me.txtSsDetraccion = New System.Windows.Forms.TextBox()
        Me.lblDetracciones = New System.Windows.Forms.Label()
        Me.txtSsOtrCargos = New System.Windows.Forms.TextBox()
        Me.lblOtrosCargos = New System.Windows.Forms.Label()
        Me.txtSsNeto = New System.Windows.Forms.TextBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.chkFechaEmision = New System.Windows.Forms.CheckBox()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.lblNroInterno = New System.Windows.Forms.Label()
        Me.txtIGV = New System.Windows.Forms.TextBox()
        Me.cboTipoDetraccion = New System.Windows.Forms.ComboBox()
        Me.lblTipoDetraccion = New System.Windows.Forms.Label()
        Me.dtpFechaRecepcion = New System.Windows.Forms.DateTimePicker()
        Me.lblNroObligPago = New System.Windows.Forms.Label()
        Me.txtNuDocumento = New System.Windows.Forms.TextBox()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.lblNuVoucher = New System.Windows.Forms.Label()
        Me.cboTipoDocumento = New System.Windows.Forms.ComboBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.cboMoneda = New System.Windows.Forms.ComboBox()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.txtMonto = New System.Windows.Forms.TextBox()
        Me.lblImporte = New System.Windows.Forms.Label()
        Me.dtpFechaEmision = New System.Windows.Forms.DateTimePicker()
        Me.txtCoPtoVentaEmision = New System.Windows.Forms.TextBox()
        Me.txtBoletoNro = New System.Windows.Forms.TextBox()
        Me.lblEtiqCoLAServicio = New System.Windows.Forms.Label()
        Me.txtCoLAServicio = New System.Windows.Forms.TextBox()
        Me.lblTitulo = New System.Windows.Forms.Label()
        Me.grbTicket = New System.Windows.Forms.GroupBox()
        Me.grbDetalle = New System.Windows.Forms.GroupBox()
        Me.TabControl1 = New System.Windows.Forms.TabControl()
        Me.TabPage1 = New System.Windows.Forms.TabPage()
        Me.cboMoneda_Multiple = New System.Windows.Forms.ComboBox()
        Me.lblEtiqMonedaMultiple = New System.Windows.Forms.Label()
        Me.cboFormaEgreso = New System.Windows.Forms.ComboBox()
        Me.lblFormaEgreso = New System.Windows.Forms.Label()
        Me.dgvDetalle = New System.Windows.Forms.DataGridView()
        Me.NuDocumProvDet = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.CoProducto = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Producto = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.coalmacen = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Almacen = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.CoCtaContab = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.CuentaContable = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.QtCantidad = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.SSPrecUni = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.SsIGV = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.SsMonto = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.SSTotal = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.btnEdit = New System.Windows.Forms.DataGridViewButtonColumn()
        Me.btnDel = New System.Windows.Forms.DataGridViewButtonColumn()
        Me.dgvVouchers = New System.Windows.Forms.DataGridView()
        Me.Codigo = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.IDVoucher = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Titulo = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.IDCab = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.IDFile = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.IDMoneda = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Simbolo = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.SaldoIni = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.SaldoOrig = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Monto = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Gastos = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.IGV_Multiple = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.MontoFinal = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Total_TC = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.chkAsignar = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        Me.chkAceptar = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        Me.btnNuevoDetalleItem = New System.Windows.Forms.Button()
        Me.lblEtiqIndVenta = New System.Windows.Forms.Label()
        Me.lblEtiqCoPtoVentaEmision = New System.Windows.Forms.Label()
        Me.lblIDPax = New System.Windows.Forms.Label()
        Me.lblIDCliente = New System.Windows.Forms.Label()
        Me.txtIndVenta = New System.Windows.Forms.TextBox()
        Me.lblIDCab = New System.Windows.Forms.Label()
        Me.txtCodTransEmision = New System.Windows.Forms.TextBox()
        Me.lblEtiqCodTransEmision = New System.Windows.Forms.Label()
        Me.lblEtiqCodLARecord = New System.Windows.Forms.Label()
        Me.txtCoIATA = New System.Windows.Forms.TextBox()
        Me.txtCodLARecord = New System.Windows.Forms.TextBox()
        Me.lblEtiqCoIATA = New System.Windows.Forms.Label()
        Me.txtCodLAEmision = New System.Windows.Forms.TextBox()
        Me.lblEtiqCodLAEmision = New System.Windows.Forms.Label()
        Me.lblEtiqNroBoleto = New System.Windows.Forms.Label()
        Me.ErrPrv = New System.Windows.Forms.ErrorProvider(Me.components)
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.ErrPrvSS = New System.Windows.Forms.ErrorProvider(Me.components)
        Me.gprLinea1.SuspendLayout()
        Me.gbxDocumentoVinculado.SuspendLayout()
        Me.grbTipoCambioFormaEgreso.SuspendLayout()
        Me.grbTicket.SuspendLayout()
        Me.grbDetalle.SuspendLayout()
        Me.TabControl1.SuspendLayout()
        Me.TabPage1.SuspendLayout()
        CType(Me.dgvDetalle, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvVouchers, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ErrPrv, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ErrPrvSS, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'gprLinea1
        '
        Me.gprLinea1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.gprLinea1.Controls.Add(Me.btnGrabar)
        Me.gprLinea1.Controls.Add(Me.btnLimpiarFile)
        Me.gprLinea1.Controls.Add(Me.lblIDFile)
        Me.gprLinea1.Controls.Add(Me.btnConsultarFile)
        Me.gprLinea1.Controls.Add(Me.lblEtiqFile)
        Me.gprLinea1.Controls.Add(Me.cboGastos)
        Me.gprLinea1.Controls.Add(Me.cboTipoSAP)
        Me.gprLinea1.Controls.Add(Me.Label24)
        Me.gprLinea1.Controls.Add(Me.gbxDocumentoVinculado)
        Me.gprLinea1.Controls.Add(Me.cboCentroControl)
        Me.gprLinea1.Controls.Add(Me.lblCentroControl)
        Me.gprLinea1.Controls.Add(Me.txtPercepcion)
        Me.gprLinea1.Controls.Add(Me.lblPercepcion)
        Me.gprLinea1.Controls.Add(Me.dtpFechaVencimiento)
        Me.gprLinea1.Controls.Add(Me.Label16)
        Me.gprLinea1.Controls.Add(Me.txtConcepto)
        Me.gprLinea1.Controls.Add(Me.lblGlosa)
        Me.gprLinea1.Controls.Add(Me.cboCentroCostos)
        Me.gprLinea1.Controls.Add(Me.lblCentroCostos)
        Me.gprLinea1.Controls.Add(Me.cboCuentaContable)
        Me.gprLinea1.Controls.Add(Me.txtSerie)
        Me.gprLinea1.Controls.Add(Me.lblCuenta)
        Me.gprLinea1.Controls.Add(Me.Label4)
        Me.gprLinea1.Controls.Add(Me.cboTipoOperacion)
        Me.gprLinea1.Controls.Add(Me.lblEtiqProveedor)
        Me.gprLinea1.Controls.Add(Me.lblDestino)
        Me.gprLinea1.Controls.Add(Me.lblProveedor)
        Me.gprLinea1.Controls.Add(Me.btnSelProveedor)
        Me.gprLinea1.Controls.Add(Me.grbTipoCambioFormaEgreso)
        Me.gprLinea1.Controls.Add(Me.chkIGV)
        Me.gprLinea1.Controls.Add(Me.txtSsDetraccion)
        Me.gprLinea1.Controls.Add(Me.lblDetracciones)
        Me.gprLinea1.Controls.Add(Me.txtSsOtrCargos)
        Me.gprLinea1.Controls.Add(Me.lblOtrosCargos)
        Me.gprLinea1.Controls.Add(Me.txtSsNeto)
        Me.gprLinea1.Controls.Add(Me.Label5)
        Me.gprLinea1.Controls.Add(Me.chkFechaEmision)
        Me.gprLinea1.Controls.Add(Me.Label7)
        Me.gprLinea1.Controls.Add(Me.lblNroInterno)
        Me.gprLinea1.Controls.Add(Me.txtIGV)
        Me.gprLinea1.Controls.Add(Me.cboTipoDetraccion)
        Me.gprLinea1.Controls.Add(Me.lblTipoDetraccion)
        Me.gprLinea1.Controls.Add(Me.dtpFechaRecepcion)
        Me.gprLinea1.Controls.Add(Me.lblNroObligPago)
        Me.gprLinea1.Controls.Add(Me.txtNuDocumento)
        Me.gprLinea1.Controls.Add(Me.Label12)
        Me.gprLinea1.Controls.Add(Me.lblNuVoucher)
        Me.gprLinea1.Controls.Add(Me.cboTipoDocumento)
        Me.gprLinea1.Controls.Add(Me.Label1)
        Me.gprLinea1.Controls.Add(Me.Label2)
        Me.gprLinea1.Controls.Add(Me.cboMoneda)
        Me.gprLinea1.Controls.Add(Me.Label9)
        Me.gprLinea1.Controls.Add(Me.txtMonto)
        Me.gprLinea1.Controls.Add(Me.lblImporte)
        Me.gprLinea1.Controls.Add(Me.dtpFechaEmision)
        Me.gprLinea1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gprLinea1.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.gprLinea1.Location = New System.Drawing.Point(6, 42)
        Me.gprLinea1.Name = "gprLinea1"
        Me.gprLinea1.Size = New System.Drawing.Size(1233, 359)
        Me.gprLinea1.TabIndex = 377
        Me.gprLinea1.TabStop = False
        Me.gprLinea1.Text = "Datos Generales"
        '
        'btnGrabar
        '
        Me.btnGrabar.BackgroundImage = Global.SETours.My.Resources.Resources.disk
        Me.btnGrabar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnGrabar.Location = New System.Drawing.Point(6, 14)
        Me.btnGrabar.Name = "btnGrabar"
        Me.btnGrabar.Size = New System.Drawing.Size(35, 23)
        Me.btnGrabar.TabIndex = 574
        Me.ToolTip1.SetToolTip(Me.btnGrabar, "Proveedor vacío")
        Me.btnGrabar.UseVisualStyleBackColor = True
        '
        'btnLimpiarFile
        '
        Me.btnLimpiarFile.BackgroundImage = Global.SETours.My.Resources.Resources.page
        Me.btnLimpiarFile.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnLimpiarFile.Location = New System.Drawing.Point(535, 155)
        Me.btnLimpiarFile.Name = "btnLimpiarFile"
        Me.btnLimpiarFile.Size = New System.Drawing.Size(35, 23)
        Me.btnLimpiarFile.TabIndex = 573
        Me.ToolTip1.SetToolTip(Me.btnLimpiarFile, "Proveedor vacío")
        Me.btnLimpiarFile.UseVisualStyleBackColor = True
        '
        'lblIDFile
        '
        Me.lblIDFile.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.lblIDFile.Location = New System.Drawing.Point(415, 155)
        Me.lblIDFile.Name = "lblIDFile"
        Me.lblIDFile.Size = New System.Drawing.Size(90, 23)
        Me.lblIDFile.TabIndex = 572
        '
        'btnConsultarFile
        '
        Me.btnConsultarFile.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnConsultarFile.Location = New System.Drawing.Point(507, 155)
        Me.btnConsultarFile.Name = "btnConsultarFile"
        Me.btnConsultarFile.Size = New System.Drawing.Size(28, 23)
        Me.btnConsultarFile.TabIndex = 570
        Me.btnConsultarFile.Text = "..."
        Me.btnConsultarFile.UseVisualStyleBackColor = True
        '
        'lblEtiqFile
        '
        Me.lblEtiqFile.AutoSize = True
        Me.lblEtiqFile.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEtiqFile.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.lblEtiqFile.Location = New System.Drawing.Point(355, 157)
        Me.lblEtiqFile.Name = "lblEtiqFile"
        Me.lblEtiqFile.Size = New System.Drawing.Size(51, 13)
        Me.lblEtiqFile.TabIndex = 571
        Me.lblEtiqFile.Text = "Nro. File"
        '
        'cboGastos
        '
        Me.cboGastos.BackColor = System.Drawing.Color.White
        Me.cboGastos.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboGastos.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboGastos.FormattingEnabled = True
        Me.cboGastos.Location = New System.Drawing.Point(134, 154)
        Me.cboGastos.Name = "cboGastos"
        Me.cboGastos.Size = New System.Drawing.Size(110, 21)
        Me.cboGastos.TabIndex = 569
        Me.cboGastos.Tag = "Borrar"
        '
        'cboTipoSAP
        '
        Me.cboTipoSAP.BackColor = System.Drawing.Color.White
        Me.cboTipoSAP.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboTipoSAP.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboTipoSAP.FormattingEnabled = True
        Me.cboTipoSAP.Location = New System.Drawing.Point(520, 14)
        Me.cboTipoSAP.Name = "cboTipoSAP"
        Me.cboTipoSAP.Size = New System.Drawing.Size(94, 21)
        Me.cboTipoSAP.TabIndex = 568
        Me.cboTipoSAP.Tag = "Borrar"
        '
        'Label24
        '
        Me.Label24.AutoSize = True
        Me.Label24.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label24.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.Label24.Location = New System.Drawing.Point(475, 17)
        Me.Label24.Name = "Label24"
        Me.Label24.Size = New System.Drawing.Size(37, 13)
        Me.Label24.TabIndex = 544
        Me.Label24.Text = "Tipo :"
        '
        'gbxDocumentoVinculado
        '
        Me.gbxDocumentoVinculado.Controls.Add(Me.txtSerie_Ref)
        Me.gbxDocumentoVinculado.Controls.Add(Me.Label23)
        Me.gbxDocumentoVinculado.Controls.Add(Me.dtpFechaEmision_Ref)
        Me.gbxDocumentoVinculado.Controls.Add(Me.Label20)
        Me.gbxDocumentoVinculado.Controls.Add(Me.txtNroDocumento_DocumVinc)
        Me.gbxDocumentoVinculado.Controls.Add(Me.Label21)
        Me.gbxDocumentoVinculado.Controls.Add(Me.cboTipoDocumento_DocumVinc)
        Me.gbxDocumentoVinculado.Controls.Add(Me.Label22)
        Me.gbxDocumentoVinculado.Location = New System.Drawing.Point(968, 69)
        Me.gbxDocumentoVinculado.Name = "gbxDocumentoVinculado"
        Me.gbxDocumentoVinculado.Size = New System.Drawing.Size(236, 133)
        Me.gbxDocumentoVinculado.TabIndex = 566
        Me.gbxDocumentoVinculado.TabStop = False
        Me.gbxDocumentoVinculado.Text = "Doc. Referencia"
        Me.gbxDocumentoVinculado.Visible = False
        '
        'txtSerie_Ref
        '
        Me.txtSerie_Ref.Location = New System.Drawing.Point(107, 46)
        Me.txtSerie_Ref.MaxLength = 31
        Me.txtSerie_Ref.Name = "txtSerie_Ref"
        Me.txtSerie_Ref.Size = New System.Drawing.Size(61, 21)
        Me.txtSerie_Ref.TabIndex = 567
        Me.txtSerie_Ref.Tag = "Borrar"
        '
        'Label23
        '
        Me.Label23.AutoSize = True
        Me.Label23.Location = New System.Drawing.Point(61, 49)
        Me.Label23.Name = "Label23"
        Me.Label23.Size = New System.Drawing.Size(39, 13)
        Me.Label23.TabIndex = 568
        Me.Label23.Text = "Serie:"
        '
        'dtpFechaEmision_Ref
        '
        Me.dtpFechaEmision_Ref.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpFechaEmision_Ref.Location = New System.Drawing.Point(107, 97)
        Me.dtpFechaEmision_Ref.Name = "dtpFechaEmision_Ref"
        Me.dtpFechaEmision_Ref.ShowCheckBox = True
        Me.dtpFechaEmision_Ref.Size = New System.Drawing.Size(118, 21)
        Me.dtpFechaEmision_Ref.TabIndex = 345
        Me.dtpFechaEmision_Ref.Value = New Date(1900, 1, 1, 0, 0, 0, 0)
        '
        'Label20
        '
        Me.Label20.AutoSize = True
        Me.Label20.Location = New System.Drawing.Point(13, 102)
        Me.Label20.Name = "Label20"
        Me.Label20.Size = New System.Drawing.Size(89, 13)
        Me.Label20.TabIndex = 344
        Me.Label20.Text = "Fecha Emisión:"
        '
        'txtNroDocumento_DocumVinc
        '
        Me.txtNroDocumento_DocumVinc.Location = New System.Drawing.Point(107, 73)
        Me.txtNroDocumento_DocumVinc.MaxLength = 11
        Me.txtNroDocumento_DocumVinc.Name = "txtNroDocumento_DocumVinc"
        Me.txtNroDocumento_DocumVinc.Size = New System.Drawing.Size(118, 21)
        Me.txtNroDocumento_DocumVinc.TabIndex = 343
        '
        'Label21
        '
        Me.Label21.AutoSize = True
        Me.Label21.Location = New System.Drawing.Point(23, 24)
        Me.Label21.Name = "Label21"
        Me.Label21.Size = New System.Drawing.Size(79, 13)
        Me.Label21.TabIndex = 339
        Me.Label21.Text = "Tipo Docum.:"
        '
        'cboTipoDocumento_DocumVinc
        '
        Me.cboTipoDocumento_DocumVinc.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboTipoDocumento_DocumVinc.FormattingEnabled = True
        Me.cboTipoDocumento_DocumVinc.Location = New System.Drawing.Point(107, 21)
        Me.cboTipoDocumento_DocumVinc.Name = "cboTipoDocumento_DocumVinc"
        Me.cboTipoDocumento_DocumVinc.Size = New System.Drawing.Size(118, 21)
        Me.cboTipoDocumento_DocumVinc.TabIndex = 340
        '
        'Label22
        '
        Me.Label22.AutoSize = True
        Me.Label22.Location = New System.Drawing.Point(41, 77)
        Me.Label22.Name = "Label22"
        Me.Label22.Size = New System.Drawing.Size(59, 13)
        Me.Label22.TabIndex = 342
        Me.Label22.Text = "Nro. Doc.:"
        '
        'cboCentroControl
        '
        Me.cboCentroControl.BackColor = System.Drawing.Color.White
        Me.cboCentroControl.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboCentroControl.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboCentroControl.FormattingEnabled = True
        Me.cboCentroControl.Location = New System.Drawing.Point(578, 135)
        Me.cboCentroControl.Name = "cboCentroControl"
        Me.cboCentroControl.Size = New System.Drawing.Size(384, 21)
        Me.cboCentroControl.TabIndex = 564
        Me.cboCentroControl.Tag = "Borrar"
        '
        'lblCentroControl
        '
        Me.lblCentroControl.AutoSize = True
        Me.lblCentroControl.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCentroControl.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.lblCentroControl.Location = New System.Drawing.Point(578, 119)
        Me.lblCentroControl.Name = "lblCentroControl"
        Me.lblCentroControl.Size = New System.Drawing.Size(106, 13)
        Me.lblCentroControl.TabIndex = 565
        Me.lblCentroControl.Text = "Centro de Control"
        '
        'txtPercepcion
        '
        Me.txtPercepcion.BackColor = System.Drawing.SystemColors.Window
        Me.txtPercepcion.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtPercepcion.Location = New System.Drawing.Point(136, 181)
        Me.txtPercepcion.MaxLength = 10
        Me.txtPercepcion.Name = "txtPercepcion"
        Me.txtPercepcion.Size = New System.Drawing.Size(90, 21)
        Me.txtPercepcion.TabIndex = 562
        Me.txtPercepcion.Tag = "Borrar"
        Me.txtPercepcion.Text = "0.00"
        Me.txtPercepcion.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lblPercepcion
        '
        Me.lblPercepcion.AutoSize = True
        Me.lblPercepcion.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPercepcion.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.lblPercepcion.Location = New System.Drawing.Point(45, 184)
        Me.lblPercepcion.Name = "lblPercepcion"
        Me.lblPercepcion.Size = New System.Drawing.Size(72, 13)
        Me.lblPercepcion.TabIndex = 563
        Me.lblPercepcion.Text = "Percepción:"
        '
        'dtpFechaVencimiento
        '
        Me.dtpFechaVencimiento.Enabled = False
        Me.dtpFechaVencimiento.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpFechaVencimiento.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpFechaVencimiento.Location = New System.Drawing.Point(281, 92)
        Me.dtpFechaVencimiento.Name = "dtpFechaVencimiento"
        Me.dtpFechaVencimiento.Size = New System.Drawing.Size(108, 21)
        Me.dtpFechaVencimiento.TabIndex = 561
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label16.Location = New System.Drawing.Point(278, 76)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(92, 13)
        Me.Label16.TabIndex = 560
        Me.Label16.Text = "F. Vencimiento:"
        '
        'txtConcepto
        '
        Me.txtConcepto.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtConcepto.Location = New System.Drawing.Point(581, 278)
        Me.txtConcepto.MaxLength = 50
        Me.txtConcepto.Multiline = True
        Me.txtConcepto.Name = "txtConcepto"
        Me.txtConcepto.Size = New System.Drawing.Size(384, 54)
        Me.txtConcepto.TabIndex = 558
        '
        'lblGlosa
        '
        Me.lblGlosa.AutoSize = True
        Me.lblGlosa.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblGlosa.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.lblGlosa.Location = New System.Drawing.Point(534, 281)
        Me.lblGlosa.Name = "lblGlosa"
        Me.lblGlosa.Size = New System.Drawing.Size(41, 13)
        Me.lblGlosa.TabIndex = 559
        Me.lblGlosa.Text = "Glosa:"
        '
        'cboCentroCostos
        '
        Me.cboCentroCostos.BackColor = System.Drawing.Color.White
        Me.cboCentroCostos.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboCentroCostos.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboCentroCostos.FormattingEnabled = True
        Me.cboCentroCostos.Location = New System.Drawing.Point(578, 90)
        Me.cboCentroCostos.Name = "cboCentroCostos"
        Me.cboCentroCostos.Size = New System.Drawing.Size(384, 21)
        Me.cboCentroCostos.TabIndex = 10
        Me.cboCentroCostos.Tag = "Borrar"
        '
        'lblCentroCostos
        '
        Me.lblCentroCostos.AutoSize = True
        Me.lblCentroCostos.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCentroCostos.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.lblCentroCostos.Location = New System.Drawing.Point(578, 74)
        Me.lblCentroCostos.Name = "lblCentroCostos"
        Me.lblCentroCostos.Size = New System.Drawing.Size(103, 13)
        Me.lblCentroCostos.TabIndex = 552
        Me.lblCentroCostos.Text = "Centro de Costos"
        '
        'cboCuentaContable
        '
        Me.cboCuentaContable.BackColor = System.Drawing.Color.White
        Me.cboCuentaContable.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboCuentaContable.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboCuentaContable.FormattingEnabled = True
        Me.cboCuentaContable.Location = New System.Drawing.Point(578, 221)
        Me.cboCuentaContable.Name = "cboCuentaContable"
        Me.cboCuentaContable.Size = New System.Drawing.Size(384, 21)
        Me.cboCuentaContable.TabIndex = 12
        Me.cboCuentaContable.Tag = "Borrar"
        '
        'txtSerie
        '
        Me.txtSerie.Location = New System.Drawing.Point(926, 13)
        Me.txtSerie.MaxLength = 31
        Me.txtSerie.Name = "txtSerie"
        Me.txtSerie.Size = New System.Drawing.Size(61, 21)
        Me.txtSerie.TabIndex = 1
        Me.txtSerie.Tag = "Borrar"
        '
        'lblCuenta
        '
        Me.lblCuenta.AutoSize = True
        Me.lblCuenta.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCuenta.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.lblCuenta.Location = New System.Drawing.Point(578, 205)
        Me.lblCuenta.Name = "lblCuenta"
        Me.lblCuenta.Size = New System.Drawing.Size(100, 13)
        Me.lblCuenta.TabIndex = 512
        Me.lblCuenta.Text = "Cuenta Contable"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(887, 16)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(39, 13)
        Me.Label4.TabIndex = 550
        Me.Label4.Text = "Serie:"
        '
        'cboTipoOperacion
        '
        Me.cboTipoOperacion.BackColor = System.Drawing.Color.White
        Me.cboTipoOperacion.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboTipoOperacion.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboTipoOperacion.FormattingEnabled = True
        Me.cboTipoOperacion.Location = New System.Drawing.Point(578, 177)
        Me.cboTipoOperacion.Name = "cboTipoOperacion"
        Me.cboTipoOperacion.Size = New System.Drawing.Size(384, 21)
        Me.cboTipoOperacion.TabIndex = 11
        Me.cboTipoOperacion.Tag = "Borrar"
        '
        'lblEtiqProveedor
        '
        Me.lblEtiqProveedor.AutoSize = True
        Me.lblEtiqProveedor.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEtiqProveedor.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.lblEtiqProveedor.Location = New System.Drawing.Point(506, 253)
        Me.lblEtiqProveedor.Name = "lblEtiqProveedor"
        Me.lblEtiqProveedor.Size = New System.Drawing.Size(69, 13)
        Me.lblEtiqProveedor.TabIndex = 548
        Me.lblEtiqProveedor.Text = "Proveedor:"
        Me.lblEtiqProveedor.Visible = False
        '
        'lblDestino
        '
        Me.lblDestino.AutoSize = True
        Me.lblDestino.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDestino.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.lblDestino.Location = New System.Drawing.Point(578, 161)
        Me.lblDestino.Name = "lblDestino"
        Me.lblDestino.Size = New System.Drawing.Size(127, 13)
        Me.lblDestino.TabIndex = 510
        Me.lblDestino.Text = "Destino de Operación"
        '
        'lblProveedor
        '
        Me.lblProveedor.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.lblProveedor.Location = New System.Drawing.Point(581, 250)
        Me.lblProveedor.Name = "lblProveedor"
        Me.lblProveedor.Size = New System.Drawing.Size(322, 22)
        Me.lblProveedor.TabIndex = 546
        Me.lblProveedor.Visible = False
        '
        'btnSelProveedor
        '
        Me.btnSelProveedor.Enabled = False
        Me.btnSelProveedor.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSelProveedor.Location = New System.Drawing.Point(909, 248)
        Me.btnSelProveedor.Name = "btnSelProveedor"
        Me.btnSelProveedor.Size = New System.Drawing.Size(27, 21)
        Me.btnSelProveedor.TabIndex = 13
        Me.btnSelProveedor.Tag = "Borrar"
        Me.btnSelProveedor.Text = "..."
        Me.btnSelProveedor.UseVisualStyleBackColor = True
        Me.btnSelProveedor.Visible = False
        '
        'grbTipoCambioFormaEgreso
        '
        Me.grbTipoCambioFormaEgreso.Controls.Add(Me.txtTipoCambioFEgreso)
        Me.grbTipoCambioFormaEgreso.Controls.Add(Me.txtTotalFEgreso)
        Me.grbTipoCambioFormaEgreso.Controls.Add(Me.Label3)
        Me.grbTipoCambioFormaEgreso.Controls.Add(Me.Label15)
        Me.grbTipoCambioFormaEgreso.Location = New System.Drawing.Point(396, 76)
        Me.grbTipoCambioFormaEgreso.Name = "grbTipoCambioFormaEgreso"
        Me.grbTipoCambioFormaEgreso.Size = New System.Drawing.Size(176, 73)
        Me.grbTipoCambioFormaEgreso.TabIndex = 18
        Me.grbTipoCambioFormaEgreso.TabStop = False
        Me.grbTipoCambioFormaEgreso.Text = "Cambio Mon. Forma Egreso"
        Me.grbTipoCambioFormaEgreso.Visible = False
        '
        'txtTipoCambioFEgreso
        '
        Me.txtTipoCambioFEgreso.BackColor = System.Drawing.SystemColors.Window
        Me.txtTipoCambioFEgreso.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtTipoCambioFEgreso.Location = New System.Drawing.Point(44, 19)
        Me.txtTipoCambioFEgreso.MaxLength = 10
        Me.txtTipoCambioFEgreso.Name = "txtTipoCambioFEgreso"
        Me.txtTipoCambioFEgreso.Size = New System.Drawing.Size(90, 21)
        Me.txtTipoCambioFEgreso.TabIndex = 0
        Me.txtTipoCambioFEgreso.Tag = "Borrar"
        Me.txtTipoCambioFEgreso.Text = "0.00"
        Me.txtTipoCambioFEgreso.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtTotalFEgreso
        '
        Me.txtTotalFEgreso.BackColor = System.Drawing.SystemColors.Window
        Me.txtTotalFEgreso.Enabled = False
        Me.txtTotalFEgreso.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtTotalFEgreso.Location = New System.Drawing.Point(44, 46)
        Me.txtTotalFEgreso.MaxLength = 15
        Me.txtTotalFEgreso.Name = "txtTotalFEgreso"
        Me.txtTotalFEgreso.Size = New System.Drawing.Size(90, 21)
        Me.txtTotalFEgreso.TabIndex = 1
        Me.txtTotalFEgreso.Tag = "Borrar"
        Me.txtTotalFEgreso.Text = "0.00"
        Me.txtTotalFEgreso.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.Label3.Location = New System.Drawing.Point(5, 22)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(30, 13)
        Me.Label3.TabIndex = 544
        Me.Label3.Text = "T/C:"
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label15.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.Label15.Location = New System.Drawing.Point(5, 49)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(39, 13)
        Me.Label15.TabIndex = 542
        Me.Label15.Text = "Total:"
        '
        'chkIGV
        '
        Me.chkIGV.AutoSize = True
        Me.chkIGV.Location = New System.Drawing.Point(47, 129)
        Me.chkIGV.Name = "chkIGV"
        Me.chkIGV.Size = New System.Drawing.Size(49, 17)
        Me.chkIGV.TabIndex = 15
        Me.chkIGV.Text = "IGV:"
        Me.chkIGV.UseVisualStyleBackColor = True
        '
        'txtSsDetraccion
        '
        Me.txtSsDetraccion.BackColor = System.Drawing.SystemColors.Window
        Me.txtSsDetraccion.Enabled = False
        Me.txtSsDetraccion.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtSsDetraccion.Location = New System.Drawing.Point(1058, 40)
        Me.txtSsDetraccion.MaxLength = 5
        Me.txtSsDetraccion.Name = "txtSsDetraccion"
        Me.txtSsDetraccion.Size = New System.Drawing.Size(63, 21)
        Me.txtSsDetraccion.TabIndex = 7
        Me.txtSsDetraccion.Tag = "Borrar"
        Me.txtSsDetraccion.Text = "0.00"
        Me.txtSsDetraccion.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lblDetracciones
        '
        Me.lblDetracciones.AutoSize = True
        Me.lblDetracciones.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDetracciones.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.lblDetracciones.Location = New System.Drawing.Point(939, 43)
        Me.lblDetracciones.Name = "lblDetracciones"
        Me.lblDetracciones.Size = New System.Drawing.Size(113, 13)
        Me.lblDetracciones.TabIndex = 538
        Me.lblDetracciones.Text = "Detracciones (S/.):"
        '
        'txtSsOtrCargos
        '
        Me.txtSsOtrCargos.BackColor = System.Drawing.SystemColors.Window
        Me.txtSsOtrCargos.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtSsOtrCargos.Location = New System.Drawing.Point(250, 154)
        Me.txtSsOtrCargos.MaxLength = 7
        Me.txtSsOtrCargos.Name = "txtSsOtrCargos"
        Me.txtSsOtrCargos.Size = New System.Drawing.Size(90, 21)
        Me.txtSsOtrCargos.TabIndex = 16
        Me.txtSsOtrCargos.Tag = "Borrar"
        Me.txtSsOtrCargos.Text = "0.00"
        Me.txtSsOtrCargos.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lblOtrosCargos
        '
        Me.lblOtrosCargos.AutoSize = True
        Me.lblOtrosCargos.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblOtrosCargos.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.lblOtrosCargos.Location = New System.Drawing.Point(45, 157)
        Me.lblOtrosCargos.Name = "lblOtrosCargos"
        Me.lblOtrosCargos.Size = New System.Drawing.Size(83, 13)
        Me.lblOtrosCargos.TabIndex = 536
        Me.lblOtrosCargos.Text = "Otros Cargos:"
        '
        'txtSsNeto
        '
        Me.txtSsNeto.BackColor = System.Drawing.SystemColors.Window
        Me.txtSsNeto.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtSsNeto.Location = New System.Drawing.Point(136, 100)
        Me.txtSsNeto.MaxLength = 20
        Me.txtSsNeto.Name = "txtSsNeto"
        Me.txtSsNeto.Size = New System.Drawing.Size(90, 21)
        Me.txtSsNeto.TabIndex = 14
        Me.txtSsNeto.Tag = "Borrar"
        Me.txtSsNeto.Text = "0.00"
        Me.txtSsNeto.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.Label5.Location = New System.Drawing.Point(45, 103)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(36, 13)
        Me.Label5.TabIndex = 534
        Me.Label5.Text = "Neto:"
        '
        'chkFechaEmision
        '
        Me.chkFechaEmision.AutoSize = True
        Me.chkFechaEmision.Location = New System.Drawing.Point(48, 47)
        Me.chkFechaEmision.Name = "chkFechaEmision"
        Me.chkFechaEmision.Size = New System.Drawing.Size(84, 17)
        Me.chkFechaEmision.TabIndex = 3
        Me.chkFechaEmision.Text = "F. Emisión:"
        Me.chkFechaEmision.UseVisualStyleBackColor = True
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(278, 17)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(78, 13)
        Me.Label7.TabIndex = 531
        Me.Label7.Text = "Nro. Interno:"
        '
        'lblNroInterno
        '
        Me.lblNroInterno.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.lblNroInterno.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.lblNroInterno.Location = New System.Drawing.Point(361, 13)
        Me.lblNroInterno.Name = "lblNroInterno"
        Me.lblNroInterno.Size = New System.Drawing.Size(108, 21)
        Me.lblNroInterno.TabIndex = 530
        Me.lblNroInterno.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtIGV
        '
        Me.txtIGV.BackColor = System.Drawing.SystemColors.Window
        Me.txtIGV.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtIGV.Location = New System.Drawing.Point(136, 127)
        Me.txtIGV.MaxLength = 10
        Me.txtIGV.Name = "txtIGV"
        Me.txtIGV.Size = New System.Drawing.Size(90, 21)
        Me.txtIGV.TabIndex = 15
        Me.txtIGV.Tag = "Borrar"
        Me.txtIGV.Text = "0.00"
        Me.txtIGV.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'cboTipoDetraccion
        '
        Me.cboTipoDetraccion.BackColor = System.Drawing.Color.White
        Me.cboTipoDetraccion.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboTipoDetraccion.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboTipoDetraccion.FormattingEnabled = True
        Me.cboTipoDetraccion.Location = New System.Drawing.Point(557, 44)
        Me.cboTipoDetraccion.Name = "cboTipoDetraccion"
        Me.cboTipoDetraccion.Size = New System.Drawing.Size(325, 21)
        Me.cboTipoDetraccion.TabIndex = 6
        Me.cboTipoDetraccion.Tag = "Borrar"
        '
        'lblTipoDetraccion
        '
        Me.lblTipoDetraccion.AutoSize = True
        Me.lblTipoDetraccion.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTipoDetraccion.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.lblTipoDetraccion.Location = New System.Drawing.Point(474, 47)
        Me.lblTipoDetraccion.Name = "lblTipoDetraccion"
        Me.lblTipoDetraccion.Size = New System.Drawing.Size(84, 13)
        Me.lblTipoDetraccion.TabIndex = 525
        Me.lblTipoDetraccion.Text = "T. Detracción:"
        '
        'dtpFechaRecepcion
        '
        Me.dtpFechaRecepcion.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpFechaRecepcion.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpFechaRecepcion.Location = New System.Drawing.Point(361, 44)
        Me.dtpFechaRecepcion.Name = "dtpFechaRecepcion"
        Me.dtpFechaRecepcion.Size = New System.Drawing.Size(108, 21)
        Me.dtpFechaRecepcion.TabIndex = 5
        '
        'lblNroObligPago
        '
        Me.lblNroObligPago.AutoSize = True
        Me.lblNroObligPago.Location = New System.Drawing.Point(47, 17)
        Me.lblNroObligPago.Name = "lblNroObligPago"
        Me.lblNroObligPago.Size = New System.Drawing.Size(81, 13)
        Me.lblNroObligPago.TabIndex = 522
        Me.lblNroObligPago.Text = "Nro. Voucher:"
        '
        'txtNuDocumento
        '
        Me.txtNuDocumento.Location = New System.Drawing.Point(1058, 13)
        Me.txtNuDocumento.MaxLength = 31
        Me.txtNuDocumento.Name = "txtNuDocumento"
        Me.txtNuDocumento.Size = New System.Drawing.Size(87, 21)
        Me.txtNuDocumento.TabIndex = 2
        Me.txtNuDocumento.Tag = "Borrar"
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label12.Location = New System.Drawing.Point(279, 47)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(80, 13)
        Me.Label12.TabIndex = 520
        Me.Label12.Text = "F. Recepción:"
        '
        'lblNuVoucher
        '
        Me.lblNuVoucher.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.lblNuVoucher.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.lblNuVoucher.Location = New System.Drawing.Point(136, 13)
        Me.lblNuVoucher.Name = "lblNuVoucher"
        Me.lblNuVoucher.Size = New System.Drawing.Size(137, 21)
        Me.lblNuVoucher.TabIndex = 0
        Me.lblNuVoucher.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboTipoDocumento
        '
        Me.cboTipoDocumento.BackColor = System.Drawing.Color.Moccasin
        Me.cboTipoDocumento.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboTipoDocumento.FormattingEnabled = True
        Me.cboTipoDocumento.Location = New System.Drawing.Point(703, 13)
        Me.cboTipoDocumento.Name = "cboTipoDocumento"
        Me.cboTipoDocumento.Size = New System.Drawing.Size(179, 21)
        Me.cboTipoDocumento.TabIndex = 0
        Me.cboTipoDocumento.Tag = "Borrar"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(993, 16)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(59, 13)
        Me.Label1.TabIndex = 518
        Me.Label1.Text = "Nro. Doc.:"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(633, 16)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(61, 13)
        Me.Label2.TabIndex = 515
        Me.Label2.Text = "Tipo Doc.:"
        '
        'cboMoneda
        '
        Me.cboMoneda.BackColor = System.Drawing.Color.White
        Me.cboMoneda.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboMoneda.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboMoneda.FormattingEnabled = True
        Me.cboMoneda.Location = New System.Drawing.Point(136, 73)
        Me.cboMoneda.Name = "cboMoneda"
        Me.cboMoneda.Size = New System.Drawing.Size(108, 21)
        Me.cboMoneda.TabIndex = 8
        Me.cboMoneda.Tag = "Borrar"
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label9.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.Label9.Location = New System.Drawing.Point(45, 76)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(55, 13)
        Me.Label9.TabIndex = 508
        Me.Label9.Text = "Moneda:"
        '
        'txtMonto
        '
        Me.txtMonto.BackColor = System.Drawing.SystemColors.Window
        Me.txtMonto.Enabled = False
        Me.txtMonto.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtMonto.Location = New System.Drawing.Point(136, 208)
        Me.txtMonto.MaxLength = 10
        Me.txtMonto.Name = "txtMonto"
        Me.txtMonto.Size = New System.Drawing.Size(90, 21)
        Me.txtMonto.TabIndex = 17
        Me.txtMonto.Tag = "Borrar"
        Me.txtMonto.Text = "0.00"
        Me.txtMonto.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lblImporte
        '
        Me.lblImporte.AutoSize = True
        Me.lblImporte.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblImporte.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.lblImporte.Location = New System.Drawing.Point(47, 211)
        Me.lblImporte.Name = "lblImporte"
        Me.lblImporte.Size = New System.Drawing.Size(39, 13)
        Me.lblImporte.TabIndex = 497
        Me.lblImporte.Text = "Total:"
        '
        'dtpFechaEmision
        '
        Me.dtpFechaEmision.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpFechaEmision.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpFechaEmision.Location = New System.Drawing.Point(136, 44)
        Me.dtpFechaEmision.Name = "dtpFechaEmision"
        Me.dtpFechaEmision.Size = New System.Drawing.Size(108, 21)
        Me.dtpFechaEmision.TabIndex = 4
        '
        'txtCoPtoVentaEmision
        '
        Me.txtCoPtoVentaEmision.BackColor = System.Drawing.Color.Moccasin
        Me.txtCoPtoVentaEmision.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtCoPtoVentaEmision.Location = New System.Drawing.Point(674, 46)
        Me.txtCoPtoVentaEmision.MaxLength = 3
        Me.txtCoPtoVentaEmision.Name = "txtCoPtoVentaEmision"
        Me.txtCoPtoVentaEmision.Size = New System.Drawing.Size(20, 21)
        Me.txtCoPtoVentaEmision.TabIndex = 19
        Me.txtCoPtoVentaEmision.Tag = "Borrar"
        Me.txtCoPtoVentaEmision.Visible = False
        '
        'txtBoletoNro
        '
        Me.txtBoletoNro.BackColor = System.Drawing.Color.Moccasin
        Me.txtBoletoNro.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtBoletoNro.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtBoletoNro.Location = New System.Drawing.Point(453, 46)
        Me.txtBoletoNro.MaxLength = 14
        Me.txtBoletoNro.Name = "txtBoletoNro"
        Me.txtBoletoNro.Size = New System.Drawing.Size(39, 21)
        Me.txtBoletoNro.TabIndex = 12
        Me.txtBoletoNro.Tag = "Borrar"
        Me.txtBoletoNro.Visible = False
        '
        'lblEtiqCoLAServicio
        '
        Me.lblEtiqCoLAServicio.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEtiqCoLAServicio.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.lblEtiqCoLAServicio.Location = New System.Drawing.Point(566, 52)
        Me.lblEtiqCoLAServicio.Name = "lblEtiqCoLAServicio"
        Me.lblEtiqCoLAServicio.Size = New System.Drawing.Size(22, 18)
        Me.lblEtiqCoLAServicio.TabIndex = 451
        Me.lblEtiqCoLAServicio.Text = "Cód. L. A. Servicio"
        Me.lblEtiqCoLAServicio.Visible = False
        '
        'txtCoLAServicio
        '
        Me.txtCoLAServicio.BackColor = System.Drawing.Color.Moccasin
        Me.txtCoLAServicio.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtCoLAServicio.Location = New System.Drawing.Point(594, 49)
        Me.txtCoLAServicio.MaxLength = 5
        Me.txtCoLAServicio.Name = "txtCoLAServicio"
        Me.txtCoLAServicio.Size = New System.Drawing.Size(39, 21)
        Me.txtCoLAServicio.TabIndex = 13
        Me.txtCoLAServicio.Tag = "Borrar"
        Me.txtCoLAServicio.Visible = False
        '
        'lblTitulo
        '
        Me.lblTitulo.BackColor = System.Drawing.SystemColors.ControlDarkDark
        Me.lblTitulo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.lblTitulo.Dock = System.Windows.Forms.DockStyle.Top
        Me.lblTitulo.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTitulo.ForeColor = System.Drawing.Color.White
        Me.lblTitulo.Location = New System.Drawing.Point(3, 16)
        Me.lblTitulo.Name = "lblTitulo"
        Me.lblTitulo.Size = New System.Drawing.Size(1240, 22)
        Me.lblTitulo.TabIndex = 346
        Me.lblTitulo.Text = "Documentos de Proveedor"
        '
        'grbTicket
        '
        Me.grbTicket.BackColor = System.Drawing.Color.White
        Me.grbTicket.Controls.Add(Me.grbDetalle)
        Me.grbTicket.Controls.Add(Me.gprLinea1)
        Me.grbTicket.Controls.Add(Me.lblEtiqIndVenta)
        Me.grbTicket.Controls.Add(Me.lblEtiqCoPtoVentaEmision)
        Me.grbTicket.Controls.Add(Me.lblTitulo)
        Me.grbTicket.Controls.Add(Me.txtCoLAServicio)
        Me.grbTicket.Controls.Add(Me.txtCoPtoVentaEmision)
        Me.grbTicket.Controls.Add(Me.txtBoletoNro)
        Me.grbTicket.Controls.Add(Me.lblEtiqCoLAServicio)
        Me.grbTicket.Controls.Add(Me.lblIDPax)
        Me.grbTicket.Controls.Add(Me.lblIDCliente)
        Me.grbTicket.Controls.Add(Me.txtIndVenta)
        Me.grbTicket.Controls.Add(Me.lblIDCab)
        Me.grbTicket.Controls.Add(Me.txtCodTransEmision)
        Me.grbTicket.Controls.Add(Me.lblEtiqCodTransEmision)
        Me.grbTicket.Controls.Add(Me.lblEtiqCodLARecord)
        Me.grbTicket.Controls.Add(Me.txtCoIATA)
        Me.grbTicket.Controls.Add(Me.txtCodLARecord)
        Me.grbTicket.Controls.Add(Me.lblEtiqCoIATA)
        Me.grbTicket.Controls.Add(Me.txtCodLAEmision)
        Me.grbTicket.Controls.Add(Me.lblEtiqCodLAEmision)
        Me.grbTicket.Controls.Add(Me.lblEtiqNroBoleto)
        Me.grbTicket.Dock = System.Windows.Forms.DockStyle.Fill
        Me.grbTicket.Location = New System.Drawing.Point(0, 0)
        Me.grbTicket.Name = "grbTicket"
        Me.grbTicket.Size = New System.Drawing.Size(1246, 664)
        Me.grbTicket.TabIndex = 166
        Me.grbTicket.TabStop = False
        '
        'grbDetalle
        '
        Me.grbDetalle.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.grbDetalle.Controls.Add(Me.TabControl1)
        Me.grbDetalle.Controls.Add(Me.btnNuevoDetalleItem)
        Me.grbDetalle.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.grbDetalle.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.grbDetalle.Location = New System.Drawing.Point(6, 407)
        Me.grbDetalle.Name = "grbDetalle"
        Me.grbDetalle.Size = New System.Drawing.Size(1121, 251)
        Me.grbDetalle.TabIndex = 503
        Me.grbDetalle.TabStop = False
        Me.grbDetalle.Text = "Detalles"
        '
        'TabControl1
        '
        Me.TabControl1.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.TabControl1.Controls.Add(Me.TabPage1)
        Me.TabControl1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TabControl1.Location = New System.Drawing.Point(5, 20)
        Me.TabControl1.Name = "TabControl1"
        Me.TabControl1.SelectedIndex = 0
        Me.TabControl1.Size = New System.Drawing.Size(1080, 225)
        Me.TabControl1.TabIndex = 0
        '
        'TabPage1
        '
        Me.TabPage1.BackColor = System.Drawing.Color.White
        Me.TabPage1.Controls.Add(Me.cboMoneda_Multiple)
        Me.TabPage1.Controls.Add(Me.lblEtiqMonedaMultiple)
        Me.TabPage1.Controls.Add(Me.cboFormaEgreso)
        Me.TabPage1.Controls.Add(Me.lblFormaEgreso)
        Me.TabPage1.Controls.Add(Me.dgvDetalle)
        Me.TabPage1.Controls.Add(Me.dgvVouchers)
        Me.TabPage1.Location = New System.Drawing.Point(4, 22)
        Me.TabPage1.Name = "TabPage1"
        Me.TabPage1.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage1.Size = New System.Drawing.Size(1072, 199)
        Me.TabPage1.TabIndex = 0
        Me.TabPage1.Text = "Detalle"
        Me.TabPage1.UseVisualStyleBackColor = True
        '
        'cboMoneda_Multiple
        '
        Me.cboMoneda_Multiple.BackColor = System.Drawing.Color.White
        Me.cboMoneda_Multiple.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboMoneda_Multiple.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboMoneda_Multiple.FormattingEnabled = True
        Me.cboMoneda_Multiple.Location = New System.Drawing.Point(349, 6)
        Me.cboMoneda_Multiple.Name = "cboMoneda_Multiple"
        Me.cboMoneda_Multiple.Size = New System.Drawing.Size(108, 21)
        Me.cboMoneda_Multiple.TabIndex = 574
        Me.cboMoneda_Multiple.Tag = "Borrar"
        '
        'lblEtiqMonedaMultiple
        '
        Me.lblEtiqMonedaMultiple.AutoSize = True
        Me.lblEtiqMonedaMultiple.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEtiqMonedaMultiple.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.lblEtiqMonedaMultiple.Location = New System.Drawing.Point(290, 9)
        Me.lblEtiqMonedaMultiple.Name = "lblEtiqMonedaMultiple"
        Me.lblEtiqMonedaMultiple.Size = New System.Drawing.Size(55, 13)
        Me.lblEtiqMonedaMultiple.TabIndex = 575
        Me.lblEtiqMonedaMultiple.Text = "Moneda:"
        '
        'cboFormaEgreso
        '
        Me.cboFormaEgreso.BackColor = System.Drawing.Color.White
        Me.cboFormaEgreso.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboFormaEgreso.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboFormaEgreso.FormattingEnabled = True
        Me.cboFormaEgreso.Location = New System.Drawing.Point(123, 6)
        Me.cboFormaEgreso.Name = "cboFormaEgreso"
        Me.cboFormaEgreso.Size = New System.Drawing.Size(141, 21)
        Me.cboFormaEgreso.TabIndex = 569
        Me.cboFormaEgreso.Tag = "Borrar"
        '
        'lblFormaEgreso
        '
        Me.lblFormaEgreso.AutoSize = True
        Me.lblFormaEgreso.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblFormaEgreso.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.lblFormaEgreso.Location = New System.Drawing.Point(10, 9)
        Me.lblFormaEgreso.Name = "lblFormaEgreso"
        Me.lblFormaEgreso.Size = New System.Drawing.Size(107, 13)
        Me.lblFormaEgreso.TabIndex = 502
        Me.lblFormaEgreso.Text = "Forma de Egreso :"
        '
        'dgvDetalle
        '
        Me.dgvDetalle.AllowUserToAddRows = False
        Me.dgvDetalle.AllowUserToDeleteRows = False
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.ControlDarkDark
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.Color.White
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvDetalle.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.dgvDetalle.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvDetalle.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.NuDocumProvDet, Me.CoProducto, Me.Producto, Me.coalmacen, Me.Almacen, Me.CoCtaContab, Me.CuentaContable, Me.QtCantidad, Me.SSPrecUni, Me.SsIGV, Me.SsMonto, Me.SSTotal, Me.btnEdit, Me.btnDel})
        Me.dgvDetalle.Location = New System.Drawing.Point(2, 3)
        Me.dgvDetalle.Name = "dgvDetalle"
        Me.dgvDetalle.ReadOnly = True
        DataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.ControlDarkDark
        DataGridViewCellStyle4.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        DataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvDetalle.RowHeadersDefaultCellStyle = DataGridViewCellStyle4
        Me.dgvDetalle.RowTemplate.DefaultCellStyle.ForeColor = System.Drawing.Color.Black
        Me.dgvDetalle.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.dgvDetalle.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvDetalle.Size = New System.Drawing.Size(1041, 190)
        Me.dgvDetalle.TabIndex = 500
        '
        'NuDocumProvDet
        '
        Me.NuDocumProvDet.HeaderText = "N°"
        Me.NuDocumProvDet.Name = "NuDocumProvDet"
        Me.NuDocumProvDet.ReadOnly = True
        '
        'CoProducto
        '
        Me.CoProducto.HeaderText = "CoProducto"
        Me.CoProducto.Name = "CoProducto"
        Me.CoProducto.ReadOnly = True
        Me.CoProducto.Visible = False
        '
        'Producto
        '
        Me.Producto.HeaderText = "Producto"
        Me.Producto.Name = "Producto"
        Me.Producto.ReadOnly = True
        '
        'coalmacen
        '
        Me.coalmacen.HeaderText = "coalmacen"
        Me.coalmacen.Name = "coalmacen"
        Me.coalmacen.ReadOnly = True
        Me.coalmacen.Visible = False
        '
        'Almacen
        '
        Me.Almacen.HeaderText = "Almacen"
        Me.Almacen.Name = "Almacen"
        Me.Almacen.ReadOnly = True
        '
        'CoCtaContab
        '
        Me.CoCtaContab.HeaderText = "CoCtaContab"
        Me.CoCtaContab.Name = "CoCtaContab"
        Me.CoCtaContab.ReadOnly = True
        Me.CoCtaContab.Visible = False
        '
        'CuentaContable
        '
        Me.CuentaContable.HeaderText = "C. Contable"
        Me.CuentaContable.Name = "CuentaContable"
        Me.CuentaContable.ReadOnly = True
        Me.CuentaContable.Visible = False
        '
        'QtCantidad
        '
        DataGridViewCellStyle2.Format = "N2"
        Me.QtCantidad.DefaultCellStyle = DataGridViewCellStyle2
        Me.QtCantidad.HeaderText = "Cantidad"
        Me.QtCantidad.Name = "QtCantidad"
        Me.QtCantidad.ReadOnly = True
        '
        'SSPrecUni
        '
        Me.SSPrecUni.HeaderText = "P. Unitario"
        Me.SSPrecUni.Name = "SSPrecUni"
        Me.SSPrecUni.ReadOnly = True
        '
        'SsIGV
        '
        Me.SsIGV.HeaderText = "IGV"
        Me.SsIGV.Name = "SsIGV"
        Me.SsIGV.ReadOnly = True
        '
        'SsMonto
        '
        DataGridViewCellStyle3.Format = "N2"
        Me.SsMonto.DefaultCellStyle = DataGridViewCellStyle3
        Me.SsMonto.HeaderText = "Monto"
        Me.SsMonto.Name = "SsMonto"
        Me.SsMonto.ReadOnly = True
        '
        'SSTotal
        '
        Me.SSTotal.HeaderText = "Total"
        Me.SSTotal.Name = "SSTotal"
        Me.SSTotal.ReadOnly = True
        '
        'btnEdit
        '
        Me.btnEdit.HeaderText = ""
        Me.btnEdit.Name = "btnEdit"
        Me.btnEdit.ReadOnly = True
        '
        'btnDel
        '
        Me.btnDel.HeaderText = ""
        Me.btnDel.Name = "btnDel"
        Me.btnDel.ReadOnly = True
        '
        'dgvVouchers
        '
        Me.dgvVouchers.AllowUserToAddRows = False
        Me.dgvVouchers.AllowUserToDeleteRows = False
        Me.dgvVouchers.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        DataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle5.BackColor = System.Drawing.SystemColors.ControlDarkDark
        DataGridViewCellStyle5.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle5.ForeColor = System.Drawing.Color.White
        DataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle5.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvVouchers.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle5
        Me.dgvVouchers.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvVouchers.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Codigo, Me.IDVoucher, Me.Titulo, Me.IDCab, Me.IDFile, Me.IDMoneda, Me.Simbolo, Me.SaldoIni, Me.SaldoOrig, Me.Monto, Me.Gastos, Me.IGV_Multiple, Me.MontoFinal, Me.Total_TC, Me.chkAsignar, Me.chkAceptar})
        Me.dgvVouchers.Location = New System.Drawing.Point(3, 31)
        Me.dgvVouchers.Name = "dgvVouchers"
        DataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle10.BackColor = System.Drawing.SystemColors.ControlDarkDark
        DataGridViewCellStyle10.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle10.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        DataGridViewCellStyle10.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle10.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle10.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvVouchers.RowHeadersDefaultCellStyle = DataGridViewCellStyle10
        Me.dgvVouchers.Size = New System.Drawing.Size(1041, 162)
        Me.dgvVouchers.TabIndex = 501
        Me.dgvVouchers.Visible = False
        '
        'Codigo
        '
        Me.Codigo.HeaderText = "Codigo"
        Me.Codigo.Name = "Codigo"
        Me.Codigo.ReadOnly = True
        Me.Codigo.Visible = False
        '
        'IDVoucher
        '
        Me.IDVoucher.FillWeight = 97.06465!
        Me.IDVoucher.HeaderText = "Voucher"
        Me.IDVoucher.Name = "IDVoucher"
        Me.IDVoucher.ReadOnly = True
        '
        'Titulo
        '
        Me.Titulo.FillWeight = 18.29198!
        Me.Titulo.HeaderText = "Título"
        Me.Titulo.Name = "Titulo"
        Me.Titulo.ReadOnly = True
        '
        'IDCab
        '
        Me.IDCab.HeaderText = "IDCab"
        Me.IDCab.Name = "IDCab"
        Me.IDCab.ReadOnly = True
        Me.IDCab.Visible = False
        '
        'IDFile
        '
        Me.IDFile.FillWeight = 13.70265!
        Me.IDFile.HeaderText = "File"
        Me.IDFile.Name = "IDFile"
        Me.IDFile.ReadOnly = True
        '
        'IDMoneda
        '
        Me.IDMoneda.HeaderText = "IDMoneda"
        Me.IDMoneda.Name = "IDMoneda"
        Me.IDMoneda.ReadOnly = True
        Me.IDMoneda.Visible = False
        '
        'Simbolo
        '
        Me.Simbolo.FillWeight = 104.681!
        Me.Simbolo.HeaderText = ""
        Me.Simbolo.Name = "Simbolo"
        Me.Simbolo.ReadOnly = True
        '
        'SaldoIni
        '
        DataGridViewCellStyle6.Format = "N2"
        DataGridViewCellStyle6.NullValue = Nothing
        Me.SaldoIni.DefaultCellStyle = DataGridViewCellStyle6
        Me.SaldoIni.HeaderText = "Saldo Inicial"
        Me.SaldoIni.Name = "SaldoIni"
        Me.SaldoIni.ReadOnly = True
        Me.SaldoIni.Visible = False
        '
        'SaldoOrig
        '
        DataGridViewCellStyle7.Format = "N2"
        DataGridViewCellStyle7.NullValue = Nothing
        Me.SaldoOrig.DefaultCellStyle = DataGridViewCellStyle7
        Me.SaldoOrig.HeaderText = "Saldo Original"
        Me.SaldoOrig.Name = "SaldoOrig"
        Me.SaldoOrig.ReadOnly = True
        Me.SaldoOrig.Visible = False
        '
        'Monto
        '
        DataGridViewCellStyle8.Format = "N2"
        DataGridViewCellStyle8.NullValue = "0.00"
        Me.Monto.DefaultCellStyle = DataGridViewCellStyle8
        Me.Monto.FillWeight = 109.5061!
        Me.Monto.HeaderText = "Neto"
        Me.Monto.Name = "Monto"
        Me.Monto.ReadOnly = True
        '
        'Gastos
        '
        Me.Gastos.FillWeight = 113.6395!
        Me.Gastos.HeaderText = "G. Adicionales"
        Me.Gastos.Name = "Gastos"
        '
        'IGV_Multiple
        '
        Me.IGV_Multiple.HeaderText = "IGV"
        Me.IGV_Multiple.Name = "IGV_Multiple"
        Me.IGV_Multiple.ReadOnly = True
        Me.IGV_Multiple.Visible = False
        '
        'MontoFinal
        '
        DataGridViewCellStyle9.Format = "N2"
        DataGridViewCellStyle9.NullValue = Nothing
        Me.MontoFinal.DefaultCellStyle = DataGridViewCellStyle9
        Me.MontoFinal.FillWeight = 113.6395!
        Me.MontoFinal.HeaderText = "Total"
        Me.MontoFinal.Name = "MontoFinal"
        '
        'Total_TC
        '
        Me.Total_TC.FillWeight = 113.6395!
        Me.Total_TC.HeaderText = "Total en (S/.)"
        Me.Total_TC.Name = "Total_TC"
        '
        'chkAsignar
        '
        Me.chkAsignar.FillWeight = 0.4563586!
        Me.chkAsignar.HeaderText = ""
        Me.chkAsignar.Name = "chkAsignar"
        Me.chkAsignar.ToolTipText = "Seleccionar"
        '
        'chkAceptar
        '
        Me.chkAceptar.FillWeight = 17.82868!
        Me.chkAceptar.HeaderText = ""
        Me.chkAceptar.Name = "chkAceptar"
        Me.chkAceptar.ToolTipText = "Aceptar"
        Me.chkAceptar.Visible = False
        '
        'btnNuevoDetalleItem
        '
        Me.btnNuevoDetalleItem.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnNuevoDetalleItem.Image = Global.SETours.My.Resources.Resources.page_add
        Me.btnNuevoDetalleItem.Location = New System.Drawing.Point(1085, 41)
        Me.btnNuevoDetalleItem.Name = "btnNuevoDetalleItem"
        Me.btnNuevoDetalleItem.Size = New System.Drawing.Size(30, 32)
        Me.btnNuevoDetalleItem.TabIndex = 475
        Me.btnNuevoDetalleItem.Tag = "Borrar"
        Me.btnNuevoDetalleItem.UseVisualStyleBackColor = True
        '
        'lblEtiqIndVenta
        '
        Me.lblEtiqIndVenta.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEtiqIndVenta.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.lblEtiqIndVenta.Location = New System.Drawing.Point(1100, 49)
        Me.lblEtiqIndVenta.Name = "lblEtiqIndVenta"
        Me.lblEtiqIndVenta.Size = New System.Drawing.Size(40, 13)
        Me.lblEtiqIndVenta.TabIndex = 470
        Me.lblEtiqIndVenta.Text = "Ind. Venta"
        Me.lblEtiqIndVenta.Visible = False
        '
        'lblEtiqCoPtoVentaEmision
        '
        Me.lblEtiqCoPtoVentaEmision.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEtiqCoPtoVentaEmision.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.lblEtiqCoPtoVentaEmision.Location = New System.Drawing.Point(643, 46)
        Me.lblEtiqCoPtoVentaEmision.Name = "lblEtiqCoPtoVentaEmision"
        Me.lblEtiqCoPtoVentaEmision.Size = New System.Drawing.Size(25, 30)
        Me.lblEtiqCoPtoVentaEmision.TabIndex = 454
        Me.lblEtiqCoPtoVentaEmision.Text = "Cód. Pto. Venta y Emisión"
        Me.lblEtiqCoPtoVentaEmision.Visible = False
        '
        'lblIDPax
        '
        Me.lblIDPax.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblIDPax.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.lblIDPax.Location = New System.Drawing.Point(563, 217)
        Me.lblIDPax.Name = "lblIDPax"
        Me.lblIDPax.Size = New System.Drawing.Size(28, 13)
        Me.lblIDPax.TabIndex = 430
        '
        'lblIDCliente
        '
        Me.lblIDCliente.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblIDCliente.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.lblIDCliente.Location = New System.Drawing.Point(3, 38)
        Me.lblIDCliente.Name = "lblIDCliente"
        Me.lblIDCliente.Size = New System.Drawing.Size(46, 13)
        Me.lblIDCliente.TabIndex = 431
        '
        'txtIndVenta
        '
        Me.txtIndVenta.BackColor = System.Drawing.Color.White
        Me.txtIndVenta.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtIndVenta.Location = New System.Drawing.Point(1143, 46)
        Me.txtIndVenta.MaxLength = 2
        Me.txtIndVenta.Name = "txtIndVenta"
        Me.txtIndVenta.Size = New System.Drawing.Size(22, 21)
        Me.txtIndVenta.TabIndex = 469
        Me.txtIndVenta.Tag = "Borrar"
        Me.txtIndVenta.Visible = False
        '
        'lblIDCab
        '
        Me.lblIDCab.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblIDCab.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.lblIDCab.Location = New System.Drawing.Point(54, 36)
        Me.lblIDCab.Name = "lblIDCab"
        Me.lblIDCab.Size = New System.Drawing.Size(51, 13)
        Me.lblIDCab.TabIndex = 428
        '
        'txtCodTransEmision
        '
        Me.txtCodTransEmision.BackColor = System.Drawing.Color.Moccasin
        Me.txtCodTransEmision.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtCodTransEmision.Location = New System.Drawing.Point(815, 46)
        Me.txtCodTransEmision.MaxLength = 10
        Me.txtCodTransEmision.Name = "txtCodTransEmision"
        Me.txtCodTransEmision.Size = New System.Drawing.Size(26, 21)
        Me.txtCodTransEmision.TabIndex = 18
        Me.txtCodTransEmision.Tag = "Borrar"
        Me.txtCodTransEmision.Visible = False
        '
        'lblEtiqCodTransEmision
        '
        Me.lblEtiqCodTransEmision.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEtiqCodTransEmision.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.lblEtiqCodTransEmision.Location = New System.Drawing.Point(772, 44)
        Me.lblEtiqCodTransEmision.Name = "lblEtiqCodTransEmision"
        Me.lblEtiqCodTransEmision.Size = New System.Drawing.Size(37, 26)
        Me.lblEtiqCodTransEmision.TabIndex = 450
        Me.lblEtiqCodTransEmision.Text = "Cód. Trans. Emisión"
        Me.lblEtiqCodTransEmision.Visible = False
        '
        'lblEtiqCodLARecord
        '
        Me.lblEtiqCodLARecord.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEtiqCodLARecord.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.lblEtiqCodLARecord.Location = New System.Drawing.Point(498, 46)
        Me.lblEtiqCodLARecord.Name = "lblEtiqCodLARecord"
        Me.lblEtiqCodLARecord.Size = New System.Drawing.Size(26, 26)
        Me.lblEtiqCodLARecord.TabIndex = 447
        Me.lblEtiqCodLARecord.Text = "Cód. L. A. Record"
        Me.lblEtiqCodLARecord.Visible = False
        '
        'txtCoIATA
        '
        Me.txtCoIATA.BackColor = System.Drawing.Color.Moccasin
        Me.txtCoIATA.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtCoIATA.Location = New System.Drawing.Point(737, 46)
        Me.txtCoIATA.MaxLength = 14
        Me.txtCoIATA.Name = "txtCoIATA"
        Me.txtCoIATA.Size = New System.Drawing.Size(29, 21)
        Me.txtCoIATA.TabIndex = 6
        Me.txtCoIATA.Tag = "Borrar"
        Me.txtCoIATA.Visible = False
        '
        'txtCodLARecord
        '
        Me.txtCodLARecord.BackColor = System.Drawing.Color.Moccasin
        Me.txtCodLARecord.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtCodLARecord.Location = New System.Drawing.Point(530, 49)
        Me.txtCodLARecord.MaxLength = 10
        Me.txtCodLARecord.Name = "txtCodLARecord"
        Me.txtCodLARecord.Size = New System.Drawing.Size(39, 21)
        Me.txtCodLARecord.TabIndex = 15
        Me.txtCodLARecord.Tag = "Borrar"
        Me.txtCodLARecord.Visible = False
        '
        'lblEtiqCoIATA
        '
        Me.lblEtiqCoIATA.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEtiqCoIATA.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.lblEtiqCoIATA.Location = New System.Drawing.Point(692, 49)
        Me.lblEtiqCoIATA.Name = "lblEtiqCoIATA"
        Me.lblEtiqCoIATA.Size = New System.Drawing.Size(39, 13)
        Me.lblEtiqCoIATA.TabIndex = 444
        Me.lblEtiqCoIATA.Text = "Cód. IATA"
        Me.lblEtiqCoIATA.Visible = False
        '
        'txtCodLAEmision
        '
        Me.txtCodLAEmision.BackColor = System.Drawing.Color.Moccasin
        Me.txtCodLAEmision.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtCodLAEmision.Location = New System.Drawing.Point(1110, 46)
        Me.txtCodLAEmision.MaxLength = 10
        Me.txtCodLAEmision.Name = "txtCodLAEmision"
        Me.txtCodLAEmision.Size = New System.Drawing.Size(24, 21)
        Me.txtCodLAEmision.TabIndex = 16
        Me.txtCodLAEmision.Tag = "Borrar"
        Me.txtCodLAEmision.Visible = False
        '
        'lblEtiqCodLAEmision
        '
        Me.lblEtiqCodLAEmision.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEtiqCodLAEmision.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.lblEtiqCodLAEmision.Location = New System.Drawing.Point(847, 49)
        Me.lblEtiqCodLAEmision.Name = "lblEtiqCodLAEmision"
        Me.lblEtiqCodLAEmision.Size = New System.Drawing.Size(52, 13)
        Me.lblEtiqCodLAEmision.TabIndex = 446
        Me.lblEtiqCodLAEmision.Text = "Cód. L. A. Emisora"
        Me.lblEtiqCodLAEmision.Visible = False
        '
        'lblEtiqNroBoleto
        '
        Me.lblEtiqNroBoleto.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEtiqNroBoleto.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.lblEtiqNroBoleto.Location = New System.Drawing.Point(410, 49)
        Me.lblEtiqNroBoleto.Name = "lblEtiqNroBoleto"
        Me.lblEtiqNroBoleto.Size = New System.Drawing.Size(37, 13)
        Me.lblEtiqNroBoleto.TabIndex = 500
        Me.lblEtiqNroBoleto.Text = "N° de Boleto"
        Me.lblEtiqNroBoleto.Visible = False
        '
        'ErrPrv
        '
        Me.ErrPrv.ContainerControl = Me
        '
        'ToolTip1
        '
        Me.ToolTip1.ShowAlways = True
        '
        'ErrPrvSS
        '
        Me.ErrPrvSS.ContainerControl = Me
        Me.ErrPrvSS.Icon = CType(resources.GetObject("ErrPrvSS.Icon"), System.Drawing.Icon)
        '
        'frmDocumentosProveedorDato
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1246, 664)
        Me.Controls.Add(Me.grbTicket)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmDocumentosProveedorDato"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Tag = "Documento de Proveedor"
        Me.Text = "Recepción de Documentos: Edición de Documentos de Proveedor"
        Me.gprLinea1.ResumeLayout(False)
        Me.gprLinea1.PerformLayout()
        Me.gbxDocumentoVinculado.ResumeLayout(False)
        Me.gbxDocumentoVinculado.PerformLayout()
        Me.grbTipoCambioFormaEgreso.ResumeLayout(False)
        Me.grbTipoCambioFormaEgreso.PerformLayout()
        Me.grbTicket.ResumeLayout(False)
        Me.grbTicket.PerformLayout()
        Me.grbDetalle.ResumeLayout(False)
        Me.TabControl1.ResumeLayout(False)
        Me.TabPage1.ResumeLayout(False)
        Me.TabPage1.PerformLayout()
        CType(Me.dgvDetalle, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvVouchers, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ErrPrv, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ErrPrvSS, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents gprLinea1 As System.Windows.Forms.GroupBox
    Friend WithEvents cboMoneda As System.Windows.Forms.ComboBox
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents txtMonto As System.Windows.Forms.TextBox
    Friend WithEvents lblImporte As System.Windows.Forms.Label
    Friend WithEvents dtpFechaEmision As System.Windows.Forms.DateTimePicker
    Friend WithEvents txtCoPtoVentaEmision As System.Windows.Forms.TextBox
    Friend WithEvents txtBoletoNro As System.Windows.Forms.TextBox
    Friend WithEvents lblEtiqCoLAServicio As System.Windows.Forms.Label
    Friend WithEvents txtCoLAServicio As System.Windows.Forms.TextBox
    Friend WithEvents lblTitulo As System.Windows.Forms.Label
    Friend WithEvents grbTicket As System.Windows.Forms.GroupBox
    Friend WithEvents lblEtiqIndVenta As System.Windows.Forms.Label
    Friend WithEvents lblEtiqCoPtoVentaEmision As System.Windows.Forms.Label
    Friend WithEvents lblIDPax As System.Windows.Forms.Label
    Friend WithEvents lblIDCliente As System.Windows.Forms.Label
    Friend WithEvents txtIndVenta As System.Windows.Forms.TextBox
    Friend WithEvents lblIDCab As System.Windows.Forms.Label
    Friend WithEvents txtCodTransEmision As System.Windows.Forms.TextBox
    Friend WithEvents lblEtiqCodTransEmision As System.Windows.Forms.Label
    Friend WithEvents lblEtiqCodLARecord As System.Windows.Forms.Label
    Friend WithEvents txtCoIATA As System.Windows.Forms.TextBox
    Friend WithEvents txtCodLARecord As System.Windows.Forms.TextBox
    Friend WithEvents lblEtiqCoIATA As System.Windows.Forms.Label
    Friend WithEvents txtCodLAEmision As System.Windows.Forms.TextBox
    Friend WithEvents lblEtiqCodLAEmision As System.Windows.Forms.Label
    Friend WithEvents lblEtiqNroBoleto As System.Windows.Forms.Label
    Friend WithEvents ErrPrv As System.Windows.Forms.ErrorProvider
    Friend WithEvents ToolTip1 As System.Windows.Forms.ToolTip
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents cboTipoDocumento As System.Windows.Forms.ComboBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents lblNroObligPago As System.Windows.Forms.Label
    Friend WithEvents txtNuDocumento As System.Windows.Forms.TextBox
    Friend WithEvents lblNuVoucher As System.Windows.Forms.Label
    Friend WithEvents cboTipoDetraccion As System.Windows.Forms.ComboBox
    Friend WithEvents lblTipoDetraccion As System.Windows.Forms.Label
    Friend WithEvents dtpFechaRecepcion As System.Windows.Forms.DateTimePicker
    Friend WithEvents txtIGV As System.Windows.Forms.TextBox
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents lblNroInterno As System.Windows.Forms.Label
    Friend WithEvents chkFechaEmision As System.Windows.Forms.CheckBox
    Friend WithEvents txtSsOtrCargos As System.Windows.Forms.TextBox
    Friend WithEvents lblOtrosCargos As System.Windows.Forms.Label
    Friend WithEvents txtSsNeto As System.Windows.Forms.TextBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents txtSsDetraccion As System.Windows.Forms.TextBox
    Friend WithEvents lblDetracciones As System.Windows.Forms.Label
    Friend WithEvents chkIGV As System.Windows.Forms.CheckBox
    Friend WithEvents cboCuentaContable As System.Windows.Forms.ComboBox
    Friend WithEvents lblCuenta As System.Windows.Forms.Label
    Friend WithEvents cboTipoOperacion As System.Windows.Forms.ComboBox
    Friend WithEvents lblDestino As System.Windows.Forms.Label
    Friend WithEvents ErrPrvSS As System.Windows.Forms.ErrorProvider
    Friend WithEvents grbTipoCambioFormaEgreso As System.Windows.Forms.GroupBox
    Friend WithEvents txtTipoCambioFEgreso As System.Windows.Forms.TextBox
    Friend WithEvents txtTotalFEgreso As System.Windows.Forms.TextBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents lblEtiqProveedor As System.Windows.Forms.Label
    Friend WithEvents lblProveedor As System.Windows.Forms.Label
    Friend WithEvents btnSelProveedor As System.Windows.Forms.Button
    Friend WithEvents txtSerie As System.Windows.Forms.TextBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents cboCentroCostos As System.Windows.Forms.ComboBox
    Friend WithEvents lblCentroCostos As System.Windows.Forms.Label
    Friend WithEvents txtConcepto As System.Windows.Forms.TextBox
    Friend WithEvents lblGlosa As System.Windows.Forms.Label
    Friend WithEvents dtpFechaVencimiento As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label16 As System.Windows.Forms.Label
    Friend WithEvents txtPercepcion As System.Windows.Forms.TextBox
    Friend WithEvents lblPercepcion As System.Windows.Forms.Label
    Friend WithEvents cboCentroControl As System.Windows.Forms.ComboBox
    Friend WithEvents lblCentroControl As System.Windows.Forms.Label
    Friend WithEvents cboTipoSAP As System.Windows.Forms.ComboBox
    Friend WithEvents Label24 As System.Windows.Forms.Label
    Friend WithEvents gbxDocumentoVinculado As System.Windows.Forms.GroupBox
    Friend WithEvents txtSerie_Ref As System.Windows.Forms.TextBox
    Friend WithEvents Label23 As System.Windows.Forms.Label
    Friend WithEvents dtpFechaEmision_Ref As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label20 As System.Windows.Forms.Label
    Friend WithEvents txtNroDocumento_DocumVinc As System.Windows.Forms.TextBox
    Friend WithEvents Label21 As System.Windows.Forms.Label
    Friend WithEvents cboTipoDocumento_DocumVinc As System.Windows.Forms.ComboBox
    Friend WithEvents Label22 As System.Windows.Forms.Label
    Friend WithEvents grbDetalle As System.Windows.Forms.GroupBox
    Friend WithEvents TabControl1 As System.Windows.Forms.TabControl
    Friend WithEvents TabPage1 As System.Windows.Forms.TabPage
    Friend WithEvents dgvDetalle As System.Windows.Forms.DataGridView
    Friend WithEvents btnNuevoDetalleItem As System.Windows.Forms.Button
    Friend WithEvents NuDocumProvDet As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents CoProducto As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Producto As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents coalmacen As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Almacen As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents CoCtaContab As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents CuentaContable As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents QtCantidad As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents SSPrecUni As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents SsIGV As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents SsMonto As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents SSTotal As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents btnEdit As System.Windows.Forms.DataGridViewButtonColumn
    Friend WithEvents btnDel As System.Windows.Forms.DataGridViewButtonColumn
    Friend WithEvents cboGastos As System.Windows.Forms.ComboBox
    Friend WithEvents lblIDFile As System.Windows.Forms.Label
    Friend WithEvents btnConsultarFile As System.Windows.Forms.Button
    Friend WithEvents lblEtiqFile As System.Windows.Forms.Label
    Friend WithEvents btnLimpiarFile As System.Windows.Forms.Button
    Friend WithEvents dgvVouchers As System.Windows.Forms.DataGridView
    Friend WithEvents cboFormaEgreso As System.Windows.Forms.ComboBox
    Friend WithEvents lblFormaEgreso As System.Windows.Forms.Label
    Friend WithEvents cboMoneda_Multiple As System.Windows.Forms.ComboBox
    Friend WithEvents lblEtiqMonedaMultiple As System.Windows.Forms.Label
    Friend WithEvents Codigo As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents IDVoucher As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Titulo As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents IDCab As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents IDFile As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents IDMoneda As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Simbolo As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents SaldoIni As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents SaldoOrig As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Monto As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Gastos As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents IGV_Multiple As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents MontoFinal As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Total_TC As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents chkAsignar As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents chkAceptar As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents btnGrabar As System.Windows.Forms.Button
End Class
