﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmMantDetServiciosProveedorDato
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.ErrPrv = New System.Windows.Forms.ErrorProvider(Me.components)
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.btnEliminarFila = New System.Windows.Forms.Button()
        Me.btnCalculo = New System.Windows.Forms.Button()
        Me.btnBuscarDetServicio = New System.Windows.Forms.Button()
        Me.lvw = New System.Windows.Forms.ListView()
        Me.btnEliminarFilaAPT = New System.Windows.Forms.Button()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.txtAnio = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.txtTipo = New System.Windows.Forms.TextBox()
        Me.txtDetaTipo = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.txtDescripcion = New System.Windows.Forms.TextBox()
        Me.chkAfecto = New System.Windows.Forms.CheckBox()
        Me.lblMontoSimpleEtiq = New System.Windows.Forms.Label()
        Me.txtMonto_sgls = New System.Windows.Forms.TextBox()
        Me.lblMontoDobleEtiq = New System.Windows.Forms.Label()
        Me.txtMonto_dbls = New System.Windows.Forms.TextBox()
        Me.lblMontoTripleEtiq = New System.Windows.Forms.Label()
        Me.txtMonto_tris = New System.Windows.Forms.TextBox()
        Me.chkPoliticaLiberado = New System.Windows.Forms.CheckBox()
        Me.cboTipoDesayuno = New System.Windows.Forms.ComboBox()
        Me.lblTipoDesayunoEtiq = New System.Windows.Forms.Label()
        Me.lblLiberadoEtiq = New System.Windows.Forms.Label()
        Me.txtLiberado = New System.Windows.Forms.TextBox()
        Me.cboTipoLib = New System.Windows.Forms.ComboBox()
        Me.lblTipoLibEtiq = New System.Windows.Forms.Label()
        Me.lblMontoLEtiq = New System.Windows.Forms.Label()
        Me.txtMontoL = New System.Windows.Forms.TextBox()
        Me.chkTarifario = New System.Windows.Forms.CheckBox()
        Me.txtTituloGrupo = New System.Windows.Forms.TextBox()
        Me.lblTituloGrupoEtiq = New System.Windows.Forms.Label()
        Me.cboIdTipoOC = New System.Windows.Forms.ComboBox()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.txtCtaContableC = New System.Windows.Forms.TextBox()
        Me.lblMontoSimpleEtiq2 = New System.Windows.Forms.Label()
        Me.txtMonto_sgl = New System.Windows.Forms.TextBox()
        Me.lblMontoDobleEtiq2 = New System.Windows.Forms.Label()
        Me.txtMonto_dbl = New System.Windows.Forms.TextBox()
        Me.lblMontoTripleEtiq2 = New System.Windows.Forms.Label()
        Me.txtMonto_tri = New System.Windows.Forms.TextBox()
        Me.lblCostosEtiq = New System.Windows.Forms.Label()
        Me.cboCodxTriple = New System.Windows.Forms.ComboBox()
        Me.lblCodxTripleEtiq = New System.Windows.Forms.Label()
        Me.lblTCEtiq = New System.Windows.Forms.Label()
        Me.txtTC = New System.Windows.Forms.TextBox()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.GroupBox3 = New System.Windows.Forms.GroupBox()
        Me.GroupBox4 = New System.Windows.Forms.GroupBox()
        Me.cboTipoGasto = New System.Windows.Forms.ComboBox()
        Me.lblTipoGastoEtiq = New System.Windows.Forms.Label()
        Me.lblCorrelativo = New System.Windows.Forms.Label()
        Me.lblCorrelativoEtiq = New System.Windows.Forms.Label()
        Me.lblMaximoLiberado = New System.Windows.Forms.Label()
        Me.txtMaximoLiberado = New System.Windows.Forms.TextBox()
        Me.dgv = New System.Windows.Forms.DataGridView()
        Me.Correlativo = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.PaxDesde = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.PaxHasta = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Monto = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Sel = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        Me.chkSelTodos = New System.Windows.Forms.CheckBox()
        Me.chkRangos = New System.Windows.Forms.CheckBox()
        Me.lblIDServicio_Det_V = New System.Windows.Forms.Label()
        Me.GroupBox5 = New System.Windows.Forms.GroupBox()
        Me.lblTitulo = New System.Windows.Forms.Label()
        Me.grb = New System.Windows.Forms.GroupBox()
        Me.chkDetraccion = New System.Windows.Forms.CheckBox()
        Me.cboCentroCostos = New System.Windows.Forms.ComboBox()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.chkPoliticaLibNivelDetalle = New System.Windows.Forms.CheckBox()
        Me.txtCodTarifa = New System.Windows.Forms.TextBox()
        Me.cboCuentaContable = New System.Windows.Forms.ComboBox()
        Me.cboHabitTriple = New System.Windows.Forms.ComboBox()
        Me.lblDefinicionTripleEtiq = New System.Windows.Forms.Label()
        Me.chkVerDetaTipo = New System.Windows.Forms.CheckBox()
        Me.chkLonche = New System.Windows.Forms.CheckBox()
        Me.lblIncluyeEtiq = New System.Windows.Forms.Label()
        Me.chkCena = New System.Windows.Forms.CheckBox()
        Me.chkAlmuerzo = New System.Windows.Forms.CheckBox()
        Me.chkDesayuno = New System.Windows.Forms.CheckBox()
        Me.chkPlanAlimenticio = New System.Windows.Forms.CheckBox()
        Me.txtDiferST = New System.Windows.Forms.TextBox()
        Me.lblDiferSTEtiq = New System.Windows.Forms.Label()
        Me.txtDiferSS = New System.Windows.Forms.TextBox()
        Me.lblDiferSSEtiq = New System.Windows.Forms.Label()
        Me.chkConAlojamiento = New System.Windows.Forms.CheckBox()
        Me.Toolbar = New System.Windows.Forms.ToolStrip()
        Me.btnNuevo = New System.Windows.Forms.ToolStripButton()
        Me.btnEditar = New System.Windows.Forms.ToolStripButton()
        Me.btnDeshacer = New System.Windows.Forms.ToolStripButton()
        Me.btnGrabar = New System.Windows.Forms.ToolStripButton()
        Me.btnEliminar = New System.Windows.Forms.ToolStripButton()
        Me.btnImprimir = New System.Windows.Forms.ToolStripButton()
        Me.btnSalir = New System.Windows.Forms.ToolStripButton()
        Me.tbc = New System.Windows.Forms.TabControl()
        Me.tbgDatos = New System.Windows.Forms.TabPage()
        Me.tbgRangosAPT = New System.Windows.Forms.TabPage()
        Me.GroupBox6 = New System.Windows.Forms.GroupBox()
        Me.gpbAPT = New System.Windows.Forms.GroupBox()
        Me.chkPolLibAPT = New System.Windows.Forms.CheckBox()
        Me.gpbPolLibAPT = New System.Windows.Forms.GroupBox()
        Me.txtMaximoLiberadoAPT = New System.Windows.Forms.TextBox()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.txtMontoLAPT = New System.Windows.Forms.TextBox()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.cboTipoLibAPT = New System.Windows.Forms.ComboBox()
        Me.txtLiberadoAPT = New System.Windows.Forms.TextBox()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.txtSinpIndi = New System.Windows.Forms.TextBox()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.txtDescAlter = New System.Windows.Forms.TextBox()
        Me.cboTemporada = New System.Windows.Forms.ComboBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.dgvAPT = New System.Windows.Forms.DataGridView()
        Me.Correlativo2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.PaxDesde2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.PaxHasta2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Monto2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Sel2 = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        Me.chkSelTodosAPT = New System.Windows.Forms.CheckBox()
        Me.lblTitulo2 = New System.Windows.Forms.Label()
        CType(Me.ErrPrv, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgv, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.grb.SuspendLayout()
        Me.Toolbar.SuspendLayout()
        Me.tbc.SuspendLayout()
        Me.tbgDatos.SuspendLayout()
        Me.tbgRangosAPT.SuspendLayout()
        Me.GroupBox6.SuspendLayout()
        Me.gpbAPT.SuspendLayout()
        Me.gpbPolLibAPT.SuspendLayout()
        CType(Me.dgvAPT, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'ErrPrv
        '
        Me.ErrPrv.ContainerControl = Me
        '
        'btnEliminarFila
        '
        Me.btnEliminarFila.Enabled = False
        Me.btnEliminarFila.Image = Global.SETours.My.Resources.Resources.textfield_delete
        Me.btnEliminarFila.Location = New System.Drawing.Point(784, 241)
        Me.btnEliminarFila.Name = "btnEliminarFila"
        Me.btnEliminarFila.Size = New System.Drawing.Size(27, 27)
        Me.btnEliminarFila.TabIndex = 182
        Me.btnEliminarFila.Tag = "Borrar"
        Me.ToolTip1.SetToolTip(Me.btnEliminarFila, "Eliminar Fila(s)")
        Me.btnEliminarFila.UseVisualStyleBackColor = True
        '
        'btnCalculo
        '
        Me.btnCalculo.Enabled = False
        Me.btnCalculo.Image = Global.SETours.My.Resources.Resources.calculator_edit
        Me.btnCalculo.Location = New System.Drawing.Point(749, 241)
        Me.btnCalculo.Name = "btnCalculo"
        Me.btnCalculo.Size = New System.Drawing.Size(27, 27)
        Me.btnCalculo.TabIndex = 184
        Me.btnCalculo.Tag = "Borrar"
        Me.ToolTip1.SetToolTip(Me.btnCalculo, "Cálculo automático")
        Me.btnCalculo.UseVisualStyleBackColor = True
        '
        'btnBuscarDetServicio
        '
        Me.btnBuscarDetServicio.Image = Global.SETours.My.Resources.Resources.application_put
        Me.btnBuscarDetServicio.Location = New System.Drawing.Point(25, 45)
        Me.btnBuscarDetServicio.Name = "btnBuscarDetServicio"
        Me.btnBuscarDetServicio.Size = New System.Drawing.Size(27, 27)
        Me.btnBuscarDetServicio.TabIndex = 185
        Me.btnBuscarDetServicio.Tag = "Borrar"
        Me.ToolTip1.SetToolTip(Me.btnBuscarDetServicio, "Buscar Detalle de Servicio existente")
        Me.btnBuscarDetServicio.UseVisualStyleBackColor = True
        '
        'lvw
        '
        Me.lvw.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lvw.Location = New System.Drawing.Point(252, 120)
        Me.lvw.Name = "lvw"
        Me.lvw.Size = New System.Drawing.Size(250, 82)
        Me.lvw.TabIndex = 157
        Me.ToolTip1.SetToolTip(Me.lvw, "Doble click para desaparecer lista.")
        Me.lvw.UseCompatibleStateImageBehavior = False
        Me.lvw.Visible = False
        '
        'btnEliminarFilaAPT
        '
        Me.btnEliminarFilaAPT.Image = Global.SETours.My.Resources.Resources.textfield_delete
        Me.btnEliminarFilaAPT.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnEliminarFilaAPT.Location = New System.Drawing.Point(479, 87)
        Me.btnEliminarFilaAPT.Name = "btnEliminarFilaAPT"
        Me.btnEliminarFilaAPT.Size = New System.Drawing.Size(88, 27)
        Me.btnEliminarFilaAPT.TabIndex = 8
        Me.btnEliminarFilaAPT.Tag = "Borrar"
        Me.btnEliminarFilaAPT.Text = "Eliminar Fila"
        Me.btnEliminarFilaAPT.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.ToolTip1.SetToolTip(Me.btnEliminarFilaAPT, "Eliminar Fila(s)")
        Me.btnEliminarFilaAPT.UseVisualStyleBackColor = True
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.Label2.Location = New System.Drawing.Point(20, 86)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(29, 13)
        Me.Label2.TabIndex = 24
        Me.Label2.Text = "Año"
        '
        'txtAnio
        '
        Me.txtAnio.BackColor = System.Drawing.Color.Moccasin
        Me.txtAnio.Location = New System.Drawing.Point(23, 101)
        Me.txtAnio.MaxLength = 4
        Me.txtAnio.Name = "txtAnio"
        Me.txtAnio.Size = New System.Drawing.Size(72, 21)
        Me.txtAnio.TabIndex = 1
        Me.txtAnio.Tag = "Borrar"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.Label1.Location = New System.Drawing.Point(22, 128)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(55, 13)
        Me.Label1.TabIndex = 26
        Me.Label1.Text = "Variante"
        '
        'txtTipo
        '
        Me.txtTipo.BackColor = System.Drawing.SystemColors.Window
        Me.txtTipo.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtTipo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtTipo.Location = New System.Drawing.Point(25, 144)
        Me.txtTipo.MaxLength = 7
        Me.txtTipo.Name = "txtTipo"
        Me.txtTipo.Size = New System.Drawing.Size(70, 21)
        Me.txtTipo.TabIndex = 3
        Me.txtTipo.Tag = "Borrar"
        '
        'txtDetaTipo
        '
        Me.txtDetaTipo.BackColor = System.Drawing.SystemColors.Window
        Me.txtDetaTipo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtDetaTipo.Location = New System.Drawing.Point(150, 144)
        Me.txtDetaTipo.MaxLength = 500
        Me.txtDetaTipo.Multiline = True
        Me.txtDetaTipo.Name = "txtDetaTipo"
        Me.txtDetaTipo.Size = New System.Drawing.Size(352, 38)
        Me.txtDetaTipo.TabIndex = 4
        Me.txtDetaTipo.Tag = "Borrar"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.Label3.Location = New System.Drawing.Point(147, 128)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(115, 13)
        Me.Label3.TabIndex = 29
        Me.Label3.Text = "Detalle de Variante"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.Label4.Location = New System.Drawing.Point(147, 85)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(72, 13)
        Me.Label4.TabIndex = 30
        Me.Label4.Text = "Descripción"
        '
        'txtDescripcion
        '
        Me.txtDescripcion.BackColor = System.Drawing.Color.Moccasin
        Me.txtDescripcion.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtDescripcion.Location = New System.Drawing.Point(150, 101)
        Me.txtDescripcion.MaxLength = 200
        Me.txtDescripcion.Name = "txtDescripcion"
        Me.txtDescripcion.Size = New System.Drawing.Size(352, 21)
        Me.txtDescripcion.TabIndex = 2
        Me.txtDescripcion.Tag = "Borrar"
        '
        'chkAfecto
        '
        Me.chkAfecto.AutoSize = True
        Me.chkAfecto.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkAfecto.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.chkAfecto.Location = New System.Drawing.Point(23, 248)
        Me.chkAfecto.Name = "chkAfecto"
        Me.chkAfecto.Size = New System.Drawing.Size(96, 17)
        Me.chkAfecto.TabIndex = 7
        Me.chkAfecto.Tag = "Borrar"
        Me.chkAfecto.Text = "Afecto a IGV"
        Me.chkAfecto.UseVisualStyleBackColor = True
        '
        'lblMontoSimpleEtiq
        '
        Me.lblMontoSimpleEtiq.AutoSize = True
        Me.lblMontoSimpleEtiq.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMontoSimpleEtiq.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.lblMontoSimpleEtiq.Location = New System.Drawing.Point(160, 268)
        Me.lblMontoSimpleEtiq.Name = "lblMontoSimpleEtiq"
        Me.lblMontoSimpleEtiq.Size = New System.Drawing.Size(127, 13)
        Me.lblMontoSimpleEtiq.TabIndex = 126
        Me.lblMontoSimpleEtiq.Text = "Habitación Simple S/."
        '
        'txtMonto_sgls
        '
        Me.txtMonto_sgls.BackColor = System.Drawing.Color.Moccasin
        Me.txtMonto_sgls.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtMonto_sgls.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtMonto_sgls.Location = New System.Drawing.Point(161, 284)
        Me.txtMonto_sgls.MaxLength = 12
        Me.txtMonto_sgls.Name = "txtMonto_sgls"
        Me.txtMonto_sgls.Size = New System.Drawing.Size(97, 21)
        Me.txtMonto_sgls.TabIndex = 6
        Me.txtMonto_sgls.Tag = "Borrar"
        Me.txtMonto_sgls.Text = "0.0000"
        Me.txtMonto_sgls.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lblMontoDobleEtiq
        '
        Me.lblMontoDobleEtiq.AutoSize = True
        Me.lblMontoDobleEtiq.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMontoDobleEtiq.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.lblMontoDobleEtiq.Location = New System.Drawing.Point(297, 268)
        Me.lblMontoDobleEtiq.Name = "lblMontoDobleEtiq"
        Me.lblMontoDobleEtiq.Size = New System.Drawing.Size(121, 13)
        Me.lblMontoDobleEtiq.TabIndex = 128
        Me.lblMontoDobleEtiq.Text = "Habitación Doble S/."
        '
        'txtMonto_dbls
        '
        Me.txtMonto_dbls.BackColor = System.Drawing.Color.Moccasin
        Me.txtMonto_dbls.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtMonto_dbls.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtMonto_dbls.Location = New System.Drawing.Point(300, 284)
        Me.txtMonto_dbls.MaxLength = 12
        Me.txtMonto_dbls.Name = "txtMonto_dbls"
        Me.txtMonto_dbls.Size = New System.Drawing.Size(97, 21)
        Me.txtMonto_dbls.TabIndex = 7
        Me.txtMonto_dbls.Tag = "Borrar"
        Me.txtMonto_dbls.Text = "0.0000"
        Me.txtMonto_dbls.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lblMontoTripleEtiq
        '
        Me.lblMontoTripleEtiq.AutoSize = True
        Me.lblMontoTripleEtiq.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMontoTripleEtiq.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.lblMontoTripleEtiq.Location = New System.Drawing.Point(426, 268)
        Me.lblMontoTripleEtiq.Name = "lblMontoTripleEtiq"
        Me.lblMontoTripleEtiq.Size = New System.Drawing.Size(121, 13)
        Me.lblMontoTripleEtiq.TabIndex = 130
        Me.lblMontoTripleEtiq.Text = "Habitación Triple S/."
        '
        'txtMonto_tris
        '
        Me.txtMonto_tris.BackColor = System.Drawing.SystemColors.Window
        Me.txtMonto_tris.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtMonto_tris.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtMonto_tris.Location = New System.Drawing.Point(429, 284)
        Me.txtMonto_tris.MaxLength = 12
        Me.txtMonto_tris.Name = "txtMonto_tris"
        Me.txtMonto_tris.Size = New System.Drawing.Size(97, 21)
        Me.txtMonto_tris.TabIndex = 8
        Me.txtMonto_tris.Tag = "Borrar"
        Me.txtMonto_tris.Text = "0.0000"
        Me.txtMonto_tris.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'chkPoliticaLiberado
        '
        Me.chkPoliticaLiberado.AutoSize = True
        Me.chkPoliticaLiberado.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkPoliticaLiberado.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.chkPoliticaLiberado.Location = New System.Drawing.Point(25, 397)
        Me.chkPoliticaLiberado.Name = "chkPoliticaLiberado"
        Me.chkPoliticaLiberado.Size = New System.Drawing.Size(136, 17)
        Me.chkPoliticaLiberado.TabIndex = 14
        Me.chkPoliticaLiberado.Tag = "Borrar"
        Me.chkPoliticaLiberado.Text = "Política de Liberado"
        Me.chkPoliticaLiberado.UseVisualStyleBackColor = True
        '
        'cboTipoDesayuno
        '
        Me.cboTipoDesayuno.BackColor = System.Drawing.SystemColors.Window
        Me.cboTipoDesayuno.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboTipoDesayuno.FormattingEnabled = True
        Me.cboTipoDesayuno.Location = New System.Drawing.Point(150, 201)
        Me.cboTipoDesayuno.Name = "cboTipoDesayuno"
        Me.cboTipoDesayuno.Size = New System.Drawing.Size(152, 21)
        Me.cboTipoDesayuno.TabIndex = 5
        Me.cboTipoDesayuno.Tag = "Borrar"
        '
        'lblTipoDesayunoEtiq
        '
        Me.lblTipoDesayunoEtiq.AutoSize = True
        Me.lblTipoDesayunoEtiq.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTipoDesayunoEtiq.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.lblTipoDesayunoEtiq.Location = New System.Drawing.Point(147, 185)
        Me.lblTipoDesayunoEtiq.Name = "lblTipoDesayunoEtiq"
        Me.lblTipoDesayunoEtiq.Size = New System.Drawing.Size(123, 13)
        Me.lblTipoDesayunoEtiq.TabIndex = 136
        Me.lblTipoDesayunoEtiq.Text = "Desayuno que aplica"
        '
        'lblLiberadoEtiq
        '
        Me.lblLiberadoEtiq.AutoSize = True
        Me.lblLiberadoEtiq.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblLiberadoEtiq.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.lblLiberadoEtiq.Location = New System.Drawing.Point(22, 417)
        Me.lblLiberadoEtiq.Name = "lblLiberadoEtiq"
        Me.lblLiberadoEtiq.Size = New System.Drawing.Size(57, 13)
        Me.lblLiberadoEtiq.TabIndex = 137
        Me.lblLiberadoEtiq.Text = "Cantidad"
        '
        'txtLiberado
        '
        Me.txtLiberado.BackColor = System.Drawing.SystemColors.Window
        Me.txtLiberado.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtLiberado.Enabled = False
        Me.txtLiberado.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtLiberado.Location = New System.Drawing.Point(23, 433)
        Me.txtLiberado.MaxLength = 2
        Me.txtLiberado.Name = "txtLiberado"
        Me.txtLiberado.Size = New System.Drawing.Size(44, 21)
        Me.txtLiberado.TabIndex = 15
        Me.txtLiberado.Tag = "Borrar"
        Me.txtLiberado.Text = "0"
        Me.txtLiberado.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'cboTipoLib
        '
        Me.cboTipoLib.BackColor = System.Drawing.SystemColors.Window
        Me.cboTipoLib.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboTipoLib.Enabled = False
        Me.cboTipoLib.FormattingEnabled = True
        Me.cboTipoLib.Location = New System.Drawing.Point(208, 435)
        Me.cboTipoLib.Name = "cboTipoLib"
        Me.cboTipoLib.Size = New System.Drawing.Size(121, 21)
        Me.cboTipoLib.TabIndex = 16
        Me.cboTipoLib.Tag = "Borrar"
        '
        'lblTipoLibEtiq
        '
        Me.lblTipoLibEtiq.AutoSize = True
        Me.lblTipoLibEtiq.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTipoLibEtiq.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.lblTipoLibEtiq.Location = New System.Drawing.Point(205, 419)
        Me.lblTipoLibEtiq.Name = "lblTipoLibEtiq"
        Me.lblTipoLibEtiq.Size = New System.Drawing.Size(83, 13)
        Me.lblTipoLibEtiq.TabIndex = 140
        Me.lblTipoLibEtiq.Text = "Tipo Liberado"
        '
        'lblMontoLEtiq
        '
        Me.lblMontoLEtiq.AutoSize = True
        Me.lblMontoLEtiq.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMontoLEtiq.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.lblMontoLEtiq.Location = New System.Drawing.Point(355, 419)
        Me.lblMontoLEtiq.Name = "lblMontoLEtiq"
        Me.lblMontoLEtiq.Size = New System.Drawing.Size(95, 13)
        Me.lblMontoLEtiq.TabIndex = 142
        Me.lblMontoLEtiq.Text = "Monto Liberado"
        '
        'txtMontoL
        '
        Me.txtMontoL.BackColor = System.Drawing.SystemColors.Window
        Me.txtMontoL.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtMontoL.Enabled = False
        Me.txtMontoL.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtMontoL.Location = New System.Drawing.Point(356, 435)
        Me.txtMontoL.MaxLength = 12
        Me.txtMontoL.Name = "txtMontoL"
        Me.txtMontoL.Size = New System.Drawing.Size(109, 21)
        Me.txtMontoL.TabIndex = 17
        Me.txtMontoL.Tag = "Borrar"
        Me.txtMontoL.Text = "0.0000"
        Me.txtMontoL.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'chkTarifario
        '
        Me.chkTarifario.AutoSize = True
        Me.chkTarifario.Enabled = False
        Me.chkTarifario.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkTarifario.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.chkTarifario.Location = New System.Drawing.Point(23, 475)
        Me.chkTarifario.Name = "chkTarifario"
        Me.chkTarifario.Size = New System.Drawing.Size(153, 17)
        Me.chkTarifario.TabIndex = 19
        Me.chkTarifario.Tag = "Borrar"
        Me.chkTarifario.Text = "Utilizado para tarifario"
        Me.chkTarifario.UseVisualStyleBackColor = True
        '
        'txtTituloGrupo
        '
        Me.txtTituloGrupo.BackColor = System.Drawing.SystemColors.Window
        Me.txtTituloGrupo.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtTituloGrupo.Enabled = False
        Me.txtTituloGrupo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtTituloGrupo.Location = New System.Drawing.Point(208, 495)
        Me.txtTituloGrupo.MaxLength = 30
        Me.txtTituloGrupo.Multiline = True
        Me.txtTituloGrupo.Name = "txtTituloGrupo"
        Me.txtTituloGrupo.Size = New System.Drawing.Size(300, 21)
        Me.txtTituloGrupo.TabIndex = 20
        Me.txtTituloGrupo.Tag = "Borrar"
        '
        'lblTituloGrupoEtiq
        '
        Me.lblTituloGrupoEtiq.AutoSize = True
        Me.lblTituloGrupoEtiq.Enabled = False
        Me.lblTituloGrupoEtiq.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTituloGrupoEtiq.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.lblTituloGrupoEtiq.Location = New System.Drawing.Point(205, 479)
        Me.lblTituloGrupoEtiq.Name = "lblTituloGrupoEtiq"
        Me.lblTituloGrupoEtiq.Size = New System.Drawing.Size(158, 13)
        Me.lblTituloGrupoEtiq.TabIndex = 146
        Me.lblTituloGrupoEtiq.Text = "Texto Grupo en el Tarifario"
        '
        'cboIdTipoOC
        '
        Me.cboIdTipoOC.BackColor = System.Drawing.SystemColors.Window
        Me.cboIdTipoOC.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboIdTipoOC.FormattingEnabled = True
        Me.cboIdTipoOC.Location = New System.Drawing.Point(662, 544)
        Me.cboIdTipoOC.Name = "cboIdTipoOC"
        Me.cboIdTipoOC.Size = New System.Drawing.Size(272, 21)
        Me.cboIdTipoOC.TabIndex = 22
        Me.cboIdTipoOC.Tag = "Borrar"
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label13.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.Label13.Location = New System.Drawing.Point(659, 528)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(117, 13)
        Me.Label13.TabIndex = 148
        Me.Label13.Text = "Operación Contable"
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label15.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.Label15.Location = New System.Drawing.Point(353, 528)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(100, 13)
        Me.Label15.TabIndex = 149
        Me.Label15.Text = "Cuenta Contable"
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label16.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.Label16.Location = New System.Drawing.Point(940, 533)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(153, 13)
        Me.Label16.TabIndex = 151
        Me.Label16.Text = "Cuenta Contable Compras"
        Me.Label16.Visible = False
        '
        'txtCtaContableC
        '
        Me.txtCtaContableC.BackColor = System.Drawing.SystemColors.Window
        Me.txtCtaContableC.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtCtaContableC.Location = New System.Drawing.Point(943, 549)
        Me.txtCtaContableC.MaxLength = 20
        Me.txtCtaContableC.Name = "txtCtaContableC"
        Me.txtCtaContableC.Size = New System.Drawing.Size(133, 21)
        Me.txtCtaContableC.TabIndex = 24
        Me.txtCtaContableC.Tag = "Borrar"
        Me.txtCtaContableC.Visible = False
        '
        'lblMontoSimpleEtiq2
        '
        Me.lblMontoSimpleEtiq2.AutoSize = True
        Me.lblMontoSimpleEtiq2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMontoSimpleEtiq2.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.lblMontoSimpleEtiq2.Location = New System.Drawing.Point(160, 308)
        Me.lblMontoSimpleEtiq2.Name = "lblMontoSimpleEtiq2"
        Me.lblMontoSimpleEtiq2.Size = New System.Drawing.Size(118, 13)
        Me.lblMontoSimpleEtiq2.TabIndex = 158
        Me.lblMontoSimpleEtiq2.Text = "Habitación Simple $"
        '
        'txtMonto_sgl
        '
        Me.txtMonto_sgl.BackColor = System.Drawing.Color.Moccasin
        Me.txtMonto_sgl.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtMonto_sgl.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtMonto_sgl.Location = New System.Drawing.Point(161, 324)
        Me.txtMonto_sgl.MaxLength = 12
        Me.txtMonto_sgl.Name = "txtMonto_sgl"
        Me.txtMonto_sgl.Size = New System.Drawing.Size(97, 21)
        Me.txtMonto_sgl.TabIndex = 11
        Me.txtMonto_sgl.Tag = "Borrar"
        Me.txtMonto_sgl.Text = "0.0000"
        Me.txtMonto_sgl.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lblMontoDobleEtiq2
        '
        Me.lblMontoDobleEtiq2.AutoSize = True
        Me.lblMontoDobleEtiq2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMontoDobleEtiq2.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.lblMontoDobleEtiq2.Location = New System.Drawing.Point(297, 308)
        Me.lblMontoDobleEtiq2.Name = "lblMontoDobleEtiq2"
        Me.lblMontoDobleEtiq2.Size = New System.Drawing.Size(112, 13)
        Me.lblMontoDobleEtiq2.TabIndex = 160
        Me.lblMontoDobleEtiq2.Text = "Habitación Doble $"
        '
        'txtMonto_dbl
        '
        Me.txtMonto_dbl.BackColor = System.Drawing.Color.Moccasin
        Me.txtMonto_dbl.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtMonto_dbl.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtMonto_dbl.Location = New System.Drawing.Point(300, 324)
        Me.txtMonto_dbl.MaxLength = 12
        Me.txtMonto_dbl.Name = "txtMonto_dbl"
        Me.txtMonto_dbl.Size = New System.Drawing.Size(97, 21)
        Me.txtMonto_dbl.TabIndex = 12
        Me.txtMonto_dbl.Tag = "Borrar"
        Me.txtMonto_dbl.Text = "0.0000"
        Me.txtMonto_dbl.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lblMontoTripleEtiq2
        '
        Me.lblMontoTripleEtiq2.AutoSize = True
        Me.lblMontoTripleEtiq2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMontoTripleEtiq2.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.lblMontoTripleEtiq2.Location = New System.Drawing.Point(426, 308)
        Me.lblMontoTripleEtiq2.Name = "lblMontoTripleEtiq2"
        Me.lblMontoTripleEtiq2.Size = New System.Drawing.Size(112, 13)
        Me.lblMontoTripleEtiq2.TabIndex = 162
        Me.lblMontoTripleEtiq2.Text = "Habitación Triple $"
        '
        'txtMonto_tri
        '
        Me.txtMonto_tri.BackColor = System.Drawing.SystemColors.Window
        Me.txtMonto_tri.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtMonto_tri.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtMonto_tri.Location = New System.Drawing.Point(429, 324)
        Me.txtMonto_tri.MaxLength = 12
        Me.txtMonto_tri.Name = "txtMonto_tri"
        Me.txtMonto_tri.Size = New System.Drawing.Size(97, 21)
        Me.txtMonto_tri.TabIndex = 13
        Me.txtMonto_tri.Tag = "Borrar"
        Me.txtMonto_tri.Text = "0.0000"
        Me.txtMonto_tri.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lblCostosEtiq
        '
        Me.lblCostosEtiq.AutoSize = True
        Me.lblCostosEtiq.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCostosEtiq.Location = New System.Drawing.Point(20, 268)
        Me.lblCostosEtiq.Name = "lblCostosEtiq"
        Me.lblCostosEtiq.Size = New System.Drawing.Size(48, 13)
        Me.lblCostosEtiq.TabIndex = 164
        Me.lblCostosEtiq.Text = "Costos:"
        '
        'cboCodxTriple
        '
        Me.cboCodxTriple.BackColor = System.Drawing.SystemColors.Window
        Me.cboCodxTriple.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboCodxTriple.FormattingEnabled = True
        Me.cboCodxTriple.Location = New System.Drawing.Point(560, 324)
        Me.cboCodxTriple.Name = "cboCodxTriple"
        Me.cboCodxTriple.Size = New System.Drawing.Size(183, 21)
        Me.cboCodxTriple.TabIndex = 10
        Me.cboCodxTriple.Tag = "Borrar"
        Me.cboCodxTriple.Visible = False
        '
        'lblCodxTripleEtiq
        '
        Me.lblCodxTripleEtiq.AutoSize = True
        Me.lblCodxTripleEtiq.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCodxTripleEtiq.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.lblCodxTripleEtiq.Location = New System.Drawing.Point(557, 308)
        Me.lblCodxTripleEtiq.Name = "lblCodxTripleEtiq"
        Me.lblCodxTripleEtiq.Size = New System.Drawing.Size(105, 13)
        Me.lblCodxTripleEtiq.TabIndex = 166
        Me.lblCodxTripleEtiq.Text = "Cod. Monto Triple"
        Me.lblCodxTripleEtiq.Visible = False
        '
        'lblTCEtiq
        '
        Me.lblTCEtiq.AutoSize = True
        Me.lblTCEtiq.Enabled = False
        Me.lblTCEtiq.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTCEtiq.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.lblTCEtiq.Location = New System.Drawing.Point(557, 268)
        Me.lblTCEtiq.Name = "lblTCEtiq"
        Me.lblTCEtiq.Size = New System.Drawing.Size(93, 13)
        Me.lblTCEtiq.TabIndex = 167
        Me.lblTCEtiq.Text = "Tipo de Cambio"
        '
        'txtTC
        '
        Me.txtTC.BackColor = System.Drawing.SystemColors.Window
        Me.txtTC.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtTC.Enabled = False
        Me.txtTC.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtTC.Location = New System.Drawing.Point(560, 284)
        Me.txtTC.MaxLength = 8
        Me.txtTC.Name = "txtTC"
        Me.txtTC.Size = New System.Drawing.Size(72, 21)
        Me.txtTC.TabIndex = 9
        Me.txtTC.Tag = "Borrar"
        Me.txtTC.Text = "0.0000"
        Me.txtTC.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'GroupBox1
        '
        Me.GroupBox1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GroupBox1.Location = New System.Drawing.Point(23, 228)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(990, 9)
        Me.GroupBox1.TabIndex = 170
        Me.GroupBox1.TabStop = False
        '
        'GroupBox2
        '
        Me.GroupBox2.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GroupBox2.Location = New System.Drawing.Point(25, 387)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(773, 9)
        Me.GroupBox2.TabIndex = 171
        Me.GroupBox2.TabStop = False
        '
        'GroupBox3
        '
        Me.GroupBox3.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GroupBox3.Location = New System.Drawing.Point(22, 467)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(991, 9)
        Me.GroupBox3.TabIndex = 172
        Me.GroupBox3.TabStop = False
        '
        'GroupBox4
        '
        Me.GroupBox4.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GroupBox4.Location = New System.Drawing.Point(22, 516)
        Me.GroupBox4.Name = "GroupBox4"
        Me.GroupBox4.Size = New System.Drawing.Size(991, 9)
        Me.GroupBox4.TabIndex = 173
        Me.GroupBox4.TabStop = False
        '
        'cboTipoGasto
        '
        Me.cboTipoGasto.BackColor = System.Drawing.SystemColors.Window
        Me.cboTipoGasto.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboTipoGasto.FormattingEnabled = True
        Me.cboTipoGasto.Location = New System.Drawing.Point(23, 324)
        Me.cboTipoGasto.Name = "cboTipoGasto"
        Me.cboTipoGasto.Size = New System.Drawing.Size(132, 21)
        Me.cboTipoGasto.TabIndex = 11
        Me.cboTipoGasto.Tag = "Borrar"
        Me.cboTipoGasto.Visible = False
        '
        'lblTipoGastoEtiq
        '
        Me.lblTipoGastoEtiq.AutoSize = True
        Me.lblTipoGastoEtiq.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTipoGastoEtiq.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.lblTipoGastoEtiq.Location = New System.Drawing.Point(20, 308)
        Me.lblTipoGastoEtiq.Name = "lblTipoGastoEtiq"
        Me.lblTipoGastoEtiq.Size = New System.Drawing.Size(67, 13)
        Me.lblTipoGastoEtiq.TabIndex = 175
        Me.lblTipoGastoEtiq.Text = "Tipo Gasto"
        Me.lblTipoGastoEtiq.Visible = False
        '
        'lblCorrelativo
        '
        Me.lblCorrelativo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.lblCorrelativo.Location = New System.Drawing.Point(814, 101)
        Me.lblCorrelativo.Name = "lblCorrelativo"
        Me.lblCorrelativo.Size = New System.Drawing.Size(55, 22)
        Me.lblCorrelativo.TabIndex = 176
        Me.lblCorrelativo.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.lblCorrelativo.Visible = False
        '
        'lblCorrelativoEtiq
        '
        Me.lblCorrelativoEtiq.AutoSize = True
        Me.lblCorrelativoEtiq.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCorrelativoEtiq.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.lblCorrelativoEtiq.Location = New System.Drawing.Point(811, 85)
        Me.lblCorrelativoEtiq.Name = "lblCorrelativoEtiq"
        Me.lblCorrelativoEtiq.Size = New System.Drawing.Size(70, 13)
        Me.lblCorrelativoEtiq.TabIndex = 177
        Me.lblCorrelativoEtiq.Text = "Correlativo"
        Me.lblCorrelativoEtiq.Visible = False
        '
        'lblMaximoLiberado
        '
        Me.lblMaximoLiberado.AutoSize = True
        Me.lblMaximoLiberado.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMaximoLiberado.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.lblMaximoLiberado.Location = New System.Drawing.Point(507, 419)
        Me.lblMaximoLiberado.Name = "lblMaximoLiberado"
        Me.lblMaximoLiberado.Size = New System.Drawing.Size(104, 13)
        Me.lblMaximoLiberado.TabIndex = 179
        Me.lblMaximoLiberado.Text = "Máximo Liberado"
        '
        'txtMaximoLiberado
        '
        Me.txtMaximoLiberado.BackColor = System.Drawing.SystemColors.Window
        Me.txtMaximoLiberado.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtMaximoLiberado.Enabled = False
        Me.txtMaximoLiberado.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtMaximoLiberado.Location = New System.Drawing.Point(508, 435)
        Me.txtMaximoLiberado.MaxLength = 2
        Me.txtMaximoLiberado.Name = "txtMaximoLiberado"
        Me.txtMaximoLiberado.Size = New System.Drawing.Size(44, 21)
        Me.txtMaximoLiberado.TabIndex = 18
        Me.txtMaximoLiberado.Tag = "Borrar"
        Me.txtMaximoLiberado.Text = "0"
        Me.txtMaximoLiberado.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'dgv
        '
        Me.dgv.AllowUserToDeleteRows = False
        Me.dgv.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.dgv.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.DisplayedCells
        Me.dgv.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgv.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Correlativo, Me.PaxDesde, Me.PaxHasta, Me.Monto, Me.Sel})
        Me.dgv.Enabled = False
        Me.dgv.Location = New System.Drawing.Point(749, 274)
        Me.dgv.Name = "dgv"
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dgv.RowsDefaultCellStyle = DataGridViewCellStyle1
        Me.dgv.RowTemplate.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgv.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect
        Me.dgv.Size = New System.Drawing.Size(264, 192)
        Me.dgv.TabIndex = 180
        Me.dgv.Tag = "CellSelect"
        '
        'Correlativo
        '
        Me.Correlativo.DataPropertyName = "Correlativo"
        Me.Correlativo.HeaderText = "ID"
        Me.Correlativo.Name = "Correlativo"
        Me.Correlativo.ReadOnly = True
        Me.Correlativo.Visible = False
        Me.Correlativo.Width = 25
        '
        'PaxDesde
        '
        Me.PaxDesde.DataPropertyName = "PaxDesde"
        Me.PaxDesde.HeaderText = "Desde"
        Me.PaxDesde.Name = "PaxDesde"
        Me.PaxDesde.Width = 50
        '
        'PaxHasta
        '
        Me.PaxHasta.DataPropertyName = "PaxHasta"
        Me.PaxHasta.HeaderText = "Hasta"
        Me.PaxHasta.Name = "PaxHasta"
        Me.PaxHasta.Width = 50
        '
        'Monto
        '
        Me.Monto.DataPropertyName = "Monto"
        Me.Monto.HeaderText = "Monto (US$)"
        Me.Monto.Name = "Monto"
        '
        'Sel
        '
        Me.Sel.HeaderText = ""
        Me.Sel.Name = "Sel"
        Me.Sel.Width = 20
        '
        'chkSelTodos
        '
        Me.chkSelTodos.AutoSize = True
        Me.chkSelTodos.Enabled = False
        Me.chkSelTodos.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkSelTodos.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.chkSelTodos.Location = New System.Drawing.Point(832, 247)
        Me.chkSelTodos.Name = "chkSelTodos"
        Me.chkSelTodos.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.chkSelTodos.Size = New System.Drawing.Size(126, 17)
        Me.chkSelTodos.TabIndex = 181
        Me.chkSelTodos.Tag = "Borrar"
        Me.chkSelTodos.Text = "Seleccionar todos"
        Me.chkSelTodos.UseVisualStyleBackColor = True
        '
        'chkRangos
        '
        Me.chkRangos.AutoSize = True
        Me.chkRangos.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkRangos.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.chkRangos.Location = New System.Drawing.Point(617, 247)
        Me.chkRangos.Name = "chkRangos"
        Me.chkRangos.Size = New System.Drawing.Size(126, 17)
        Me.chkRangos.TabIndex = 183
        Me.chkRangos.Tag = "Borrar"
        Me.chkRangos.Text = "Rangos de Costos"
        Me.chkRangos.UseVisualStyleBackColor = True
        '
        'lblIDServicio_Det_V
        '
        Me.lblIDServicio_Det_V.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.lblIDServicio_Det_V.Location = New System.Drawing.Point(63, 47)
        Me.lblIDServicio_Det_V.Name = "lblIDServicio_Det_V"
        Me.lblIDServicio_Det_V.Size = New System.Drawing.Size(80, 22)
        Me.lblIDServicio_Det_V.TabIndex = 186
        Me.lblIDServicio_Det_V.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'GroupBox5
        '
        Me.GroupBox5.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GroupBox5.Location = New System.Drawing.Point(23, 72)
        Me.GroupBox5.Name = "GroupBox5"
        Me.GroupBox5.Size = New System.Drawing.Size(990, 9)
        Me.GroupBox5.TabIndex = 187
        Me.GroupBox5.TabStop = False
        '
        'lblTitulo
        '
        Me.lblTitulo.BackColor = System.Drawing.SystemColors.ControlDarkDark
        Me.lblTitulo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.lblTitulo.Dock = System.Windows.Forms.DockStyle.Top
        Me.lblTitulo.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTitulo.ForeColor = System.Drawing.Color.White
        Me.lblTitulo.Location = New System.Drawing.Point(3, 17)
        Me.lblTitulo.Name = "lblTitulo"
        Me.lblTitulo.Size = New System.Drawing.Size(1019, 22)
        Me.lblTitulo.TabIndex = 188
        Me.lblTitulo.Text = "Nuevo"
        '
        'grb
        '
        Me.grb.BackColor = System.Drawing.Color.White
        Me.grb.Controls.Add(Me.chkDetraccion)
        Me.grb.Controls.Add(Me.cboCentroCostos)
        Me.grb.Controls.Add(Me.Label10)
        Me.grb.Controls.Add(Me.chkPoliticaLibNivelDetalle)
        Me.grb.Controls.Add(Me.txtCodTarifa)
        Me.grb.Controls.Add(Me.cboCuentaContable)
        Me.grb.Controls.Add(Me.cboHabitTriple)
        Me.grb.Controls.Add(Me.lblDefinicionTripleEtiq)
        Me.grb.Controls.Add(Me.chkVerDetaTipo)
        Me.grb.Controls.Add(Me.chkLonche)
        Me.grb.Controls.Add(Me.lblIncluyeEtiq)
        Me.grb.Controls.Add(Me.chkCena)
        Me.grb.Controls.Add(Me.chkAlmuerzo)
        Me.grb.Controls.Add(Me.chkDesayuno)
        Me.grb.Controls.Add(Me.chkPlanAlimenticio)
        Me.grb.Controls.Add(Me.txtDiferST)
        Me.grb.Controls.Add(Me.lblDiferSTEtiq)
        Me.grb.Controls.Add(Me.txtDiferSS)
        Me.grb.Controls.Add(Me.lblDiferSSEtiq)
        Me.grb.Controls.Add(Me.chkConAlojamiento)
        Me.grb.Controls.Add(Me.lblTitulo)
        Me.grb.Controls.Add(Me.GroupBox5)
        Me.grb.Controls.Add(Me.lblIDServicio_Det_V)
        Me.grb.Controls.Add(Me.btnBuscarDetServicio)
        Me.grb.Controls.Add(Me.btnCalculo)
        Me.grb.Controls.Add(Me.chkRangos)
        Me.grb.Controls.Add(Me.btnEliminarFila)
        Me.grb.Controls.Add(Me.chkSelTodos)
        Me.grb.Controls.Add(Me.dgv)
        Me.grb.Controls.Add(Me.txtMaximoLiberado)
        Me.grb.Controls.Add(Me.lblMaximoLiberado)
        Me.grb.Controls.Add(Me.lblCorrelativoEtiq)
        Me.grb.Controls.Add(Me.lblCorrelativo)
        Me.grb.Controls.Add(Me.lblTipoGastoEtiq)
        Me.grb.Controls.Add(Me.cboTipoGasto)
        Me.grb.Controls.Add(Me.GroupBox4)
        Me.grb.Controls.Add(Me.GroupBox3)
        Me.grb.Controls.Add(Me.GroupBox2)
        Me.grb.Controls.Add(Me.GroupBox1)
        Me.grb.Controls.Add(Me.txtTC)
        Me.grb.Controls.Add(Me.lblTCEtiq)
        Me.grb.Controls.Add(Me.lblCodxTripleEtiq)
        Me.grb.Controls.Add(Me.cboCodxTriple)
        Me.grb.Controls.Add(Me.lblCostosEtiq)
        Me.grb.Controls.Add(Me.txtMonto_tri)
        Me.grb.Controls.Add(Me.lblMontoTripleEtiq2)
        Me.grb.Controls.Add(Me.txtMonto_dbl)
        Me.grb.Controls.Add(Me.lblMontoDobleEtiq2)
        Me.grb.Controls.Add(Me.txtMonto_sgl)
        Me.grb.Controls.Add(Me.lblMontoSimpleEtiq2)
        Me.grb.Controls.Add(Me.lvw)
        Me.grb.Controls.Add(Me.txtCtaContableC)
        Me.grb.Controls.Add(Me.Label16)
        Me.grb.Controls.Add(Me.Label15)
        Me.grb.Controls.Add(Me.Label13)
        Me.grb.Controls.Add(Me.cboIdTipoOC)
        Me.grb.Controls.Add(Me.lblTituloGrupoEtiq)
        Me.grb.Controls.Add(Me.txtTituloGrupo)
        Me.grb.Controls.Add(Me.chkTarifario)
        Me.grb.Controls.Add(Me.txtMontoL)
        Me.grb.Controls.Add(Me.lblMontoLEtiq)
        Me.grb.Controls.Add(Me.lblTipoLibEtiq)
        Me.grb.Controls.Add(Me.cboTipoLib)
        Me.grb.Controls.Add(Me.txtLiberado)
        Me.grb.Controls.Add(Me.lblLiberadoEtiq)
        Me.grb.Controls.Add(Me.lblTipoDesayunoEtiq)
        Me.grb.Controls.Add(Me.cboTipoDesayuno)
        Me.grb.Controls.Add(Me.chkPoliticaLiberado)
        Me.grb.Controls.Add(Me.txtMonto_tris)
        Me.grb.Controls.Add(Me.lblMontoTripleEtiq)
        Me.grb.Controls.Add(Me.txtMonto_dbls)
        Me.grb.Controls.Add(Me.lblMontoDobleEtiq)
        Me.grb.Controls.Add(Me.txtMonto_sgls)
        Me.grb.Controls.Add(Me.lblMontoSimpleEtiq)
        Me.grb.Controls.Add(Me.chkAfecto)
        Me.grb.Controls.Add(Me.txtDescripcion)
        Me.grb.Controls.Add(Me.Label4)
        Me.grb.Controls.Add(Me.Label3)
        Me.grb.Controls.Add(Me.txtDetaTipo)
        Me.grb.Controls.Add(Me.txtTipo)
        Me.grb.Controls.Add(Me.Label1)
        Me.grb.Controls.Add(Me.txtAnio)
        Me.grb.Controls.Add(Me.Label2)
        Me.grb.Dock = System.Windows.Forms.DockStyle.Fill
        Me.grb.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.grb.Location = New System.Drawing.Point(3, 3)
        Me.grb.Name = "grb"
        Me.grb.Size = New System.Drawing.Size(1025, 570)
        Me.grb.TabIndex = 0
        Me.grb.TabStop = False
        '
        'chkDetraccion
        '
        Me.chkDetraccion.AutoSize = True
        Me.chkDetraccion.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkDetraccion.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.chkDetraccion.Location = New System.Drawing.Point(543, 482)
        Me.chkDetraccion.Name = "chkDetraccion"
        Me.chkDetraccion.Size = New System.Drawing.Size(87, 17)
        Me.chkDetraccion.TabIndex = 555
        Me.chkDetraccion.Tag = "Borrar"
        Me.chkDetraccion.Text = "Detracción"
        Me.chkDetraccion.UseVisualStyleBackColor = True
        '
        'cboCentroCostos
        '
        Me.cboCentroCostos.BackColor = System.Drawing.Color.White
        Me.cboCentroCostos.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboCentroCostos.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboCentroCostos.FormattingEnabled = True
        Me.cboCentroCostos.Location = New System.Drawing.Point(24, 544)
        Me.cboCentroCostos.Name = "cboCentroCostos"
        Me.cboCentroCostos.Size = New System.Drawing.Size(326, 21)
        Me.cboCentroCostos.TabIndex = 553
        Me.cboCentroCostos.Tag = "Borrar"
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label10.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.Label10.Location = New System.Drawing.Point(24, 528)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(103, 13)
        Me.Label10.TabIndex = 554
        Me.Label10.Text = "Centro de Costos"
        '
        'chkPoliticaLibNivelDetalle
        '
        Me.chkPoliticaLibNivelDetalle.AutoSize = True
        Me.chkPoliticaLibNivelDetalle.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkPoliticaLibNivelDetalle.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.chkPoliticaLibNivelDetalle.Location = New System.Drawing.Point(208, 397)
        Me.chkPoliticaLibNivelDetalle.Name = "chkPoliticaLibNivelDetalle"
        Me.chkPoliticaLibNivelDetalle.Size = New System.Drawing.Size(208, 17)
        Me.chkPoliticaLibNivelDetalle.TabIndex = 320
        Me.chkPoliticaLibNivelDetalle.Tag = "Borrar"
        Me.chkPoliticaLibNivelDetalle.Text = "Política de Liberado nivel detalle"
        Me.chkPoliticaLibNivelDetalle.UseVisualStyleBackColor = True
        '
        'txtCodTarifa
        '
        Me.txtCodTarifa.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtCodTarifa.Location = New System.Drawing.Point(508, 161)
        Me.txtCodTarifa.MaxLength = 500
        Me.txtCodTarifa.Name = "txtCodTarifa"
        Me.txtCodTarifa.Size = New System.Drawing.Size(303, 21)
        Me.txtCodTarifa.TabIndex = 319
        '
        'cboCuentaContable
        '
        Me.cboCuentaContable.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboCuentaContable.FormattingEnabled = True
        Me.cboCuentaContable.Location = New System.Drawing.Point(356, 544)
        Me.cboCuentaContable.Name = "cboCuentaContable"
        Me.cboCuentaContable.Size = New System.Drawing.Size(300, 21)
        Me.cboCuentaContable.TabIndex = 318
        Me.cboCuentaContable.Tag = "Borrar"
        '
        'cboHabitTriple
        '
        Me.cboHabitTriple.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboHabitTriple.FormattingEnabled = True
        Me.cboHabitTriple.Location = New System.Drawing.Point(560, 369)
        Me.cboHabitTriple.Name = "cboHabitTriple"
        Me.cboHabitTriple.Size = New System.Drawing.Size(97, 21)
        Me.cboHabitTriple.TabIndex = 317
        Me.cboHabitTriple.Tag = "Borrar"
        '
        'lblDefinicionTripleEtiq
        '
        Me.lblDefinicionTripleEtiq.AutoSize = True
        Me.lblDefinicionTripleEtiq.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDefinicionTripleEtiq.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.lblDefinicionTripleEtiq.Location = New System.Drawing.Point(557, 353)
        Me.lblDefinicionTripleEtiq.Name = "lblDefinicionTripleEtiq"
        Me.lblDefinicionTripleEtiq.Size = New System.Drawing.Size(97, 13)
        Me.lblDefinicionTripleEtiq.TabIndex = 316
        Me.lblDefinicionTripleEtiq.Text = "Definición Triple"
        Me.lblDefinicionTripleEtiq.Visible = False
        '
        'chkVerDetaTipo
        '
        Me.chkVerDetaTipo.AutoSize = True
        Me.chkVerDetaTipo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkVerDetaTipo.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.chkVerDetaTipo.Location = New System.Drawing.Point(510, 144)
        Me.chkVerDetaTipo.Name = "chkVerDetaTipo"
        Me.chkVerDetaTipo.Size = New System.Drawing.Size(156, 17)
        Me.chkVerDetaTipo.TabIndex = 5
        Me.chkVerDetaTipo.Tag = "Borrar"
        Me.chkVerDetaTipo.Text = "Ver Detalle de Variante"
        Me.chkVerDetaTipo.UseVisualStyleBackColor = True
        '
        'chkLonche
        '
        Me.chkLonche.AutoSize = True
        Me.chkLonche.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkLonche.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.chkLonche.Location = New System.Drawing.Point(486, 203)
        Me.chkLonche.Name = "chkLonche"
        Me.chkLonche.Size = New System.Drawing.Size(83, 17)
        Me.chkLonche.TabIndex = 7
        Me.chkLonche.Tag = "Borrar"
        Me.chkLonche.Text = "Box Lunch"
        Me.chkLonche.UseVisualStyleBackColor = True
        '
        'lblIncluyeEtiq
        '
        Me.lblIncluyeEtiq.AutoSize = True
        Me.lblIncluyeEtiq.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblIncluyeEtiq.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.lblIncluyeEtiq.Location = New System.Drawing.Point(326, 204)
        Me.lblIncluyeEtiq.Name = "lblIncluyeEtiq"
        Me.lblIncluyeEtiq.Size = New System.Drawing.Size(52, 13)
        Me.lblIncluyeEtiq.TabIndex = 315
        Me.lblIncluyeEtiq.Text = "Incluye:"
        Me.lblIncluyeEtiq.Visible = False
        '
        'chkCena
        '
        Me.chkCena.AutoSize = True
        Me.chkCena.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkCena.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.chkCena.Location = New System.Drawing.Point(657, 203)
        Me.chkCena.Name = "chkCena"
        Me.chkCena.Size = New System.Drawing.Size(54, 17)
        Me.chkCena.TabIndex = 9
        Me.chkCena.Tag = "Borrar"
        Me.chkCena.Text = "Cena"
        Me.chkCena.UseVisualStyleBackColor = True
        '
        'chkAlmuerzo
        '
        Me.chkAlmuerzo.AutoSize = True
        Me.chkAlmuerzo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkAlmuerzo.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.chkAlmuerzo.Location = New System.Drawing.Point(570, 203)
        Me.chkAlmuerzo.Name = "chkAlmuerzo"
        Me.chkAlmuerzo.Size = New System.Drawing.Size(80, 17)
        Me.chkAlmuerzo.TabIndex = 8
        Me.chkAlmuerzo.Tag = "Borrar"
        Me.chkAlmuerzo.Text = "Almuerzo"
        Me.chkAlmuerzo.UseVisualStyleBackColor = True
        '
        'chkDesayuno
        '
        Me.chkDesayuno.AutoSize = True
        Me.chkDesayuno.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkDesayuno.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.chkDesayuno.Location = New System.Drawing.Point(398, 203)
        Me.chkDesayuno.Name = "chkDesayuno"
        Me.chkDesayuno.Size = New System.Drawing.Size(82, 17)
        Me.chkDesayuno.TabIndex = 6
        Me.chkDesayuno.Tag = "Borrar"
        Me.chkDesayuno.Text = "Desayuno"
        Me.chkDesayuno.UseVisualStyleBackColor = True
        '
        'chkPlanAlimenticio
        '
        Me.chkPlanAlimenticio.AutoSize = True
        Me.chkPlanAlimenticio.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkPlanAlimenticio.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.chkPlanAlimenticio.Location = New System.Drawing.Point(161, 248)
        Me.chkPlanAlimenticio.Name = "chkPlanAlimenticio"
        Me.chkPlanAlimenticio.Size = New System.Drawing.Size(116, 17)
        Me.chkPlanAlimenticio.TabIndex = 8
        Me.chkPlanAlimenticio.Tag = "Borrar"
        Me.chkPlanAlimenticio.Text = "Plan Alimenticio"
        Me.chkPlanAlimenticio.UseVisualStyleBackColor = True
        '
        'txtDiferST
        '
        Me.txtDiferST.BackColor = System.Drawing.SystemColors.Window
        Me.txtDiferST.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtDiferST.Enabled = False
        Me.txtDiferST.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtDiferST.Location = New System.Drawing.Point(300, 369)
        Me.txtDiferST.MaxLength = 12
        Me.txtDiferST.Name = "txtDiferST"
        Me.txtDiferST.Size = New System.Drawing.Size(97, 21)
        Me.txtDiferST.TabIndex = 192
        Me.txtDiferST.Tag = "Borrar"
        Me.txtDiferST.Text = "0.0000"
        Me.txtDiferST.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lblDiferSTEtiq
        '
        Me.lblDiferSTEtiq.AutoSize = True
        Me.lblDiferSTEtiq.Enabled = False
        Me.lblDiferSTEtiq.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDiferSTEtiq.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.lblDiferSTEtiq.Location = New System.Drawing.Point(297, 353)
        Me.lblDiferSTEtiq.Name = "lblDiferSTEtiq"
        Me.lblDiferSTEtiq.Size = New System.Drawing.Size(102, 13)
        Me.lblDiferSTEtiq.TabIndex = 193
        Me.lblDiferSTEtiq.Text = "Difer. Supl. Triple"
        '
        'txtDiferSS
        '
        Me.txtDiferSS.BackColor = System.Drawing.SystemColors.Window
        Me.txtDiferSS.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtDiferSS.Enabled = False
        Me.txtDiferSS.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtDiferSS.Location = New System.Drawing.Point(161, 369)
        Me.txtDiferSS.MaxLength = 12
        Me.txtDiferSS.Name = "txtDiferSS"
        Me.txtDiferSS.Size = New System.Drawing.Size(97, 21)
        Me.txtDiferSS.TabIndex = 190
        Me.txtDiferSS.Tag = "Borrar"
        Me.txtDiferSS.Text = "0.0000"
        Me.txtDiferSS.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lblDiferSSEtiq
        '
        Me.lblDiferSSEtiq.AutoSize = True
        Me.lblDiferSSEtiq.Enabled = False
        Me.lblDiferSSEtiq.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDiferSSEtiq.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.lblDiferSSEtiq.Location = New System.Drawing.Point(160, 353)
        Me.lblDiferSSEtiq.Name = "lblDiferSSEtiq"
        Me.lblDiferSSEtiq.Size = New System.Drawing.Size(108, 13)
        Me.lblDiferSSEtiq.TabIndex = 191
        Me.lblDiferSSEtiq.Text = "Difer. Supl. Simple"
        '
        'chkConAlojamiento
        '
        Me.chkConAlojamiento.AutoSize = True
        Me.chkConAlojamiento.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkConAlojamiento.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.chkConAlojamiento.Location = New System.Drawing.Point(23, 361)
        Me.chkConAlojamiento.Name = "chkConAlojamiento"
        Me.chkConAlojamiento.Size = New System.Drawing.Size(119, 17)
        Me.chkConAlojamiento.TabIndex = 189
        Me.chkConAlojamiento.Tag = "Borrar"
        Me.chkConAlojamiento.Text = "Con Alojamiento"
        Me.chkConAlojamiento.UseVisualStyleBackColor = True
        '
        'Toolbar
        '
        Me.Toolbar.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.btnNuevo, Me.btnEditar, Me.btnDeshacer, Me.btnGrabar, Me.btnEliminar, Me.btnImprimir, Me.btnSalir})
        Me.Toolbar.Location = New System.Drawing.Point(0, 0)
        Me.Toolbar.Name = "Toolbar"
        Me.Toolbar.Size = New System.Drawing.Size(970, 25)
        Me.Toolbar.TabIndex = 6
        Me.Toolbar.Text = "Toolbar"
        Me.Toolbar.Visible = False
        '
        'btnNuevo
        '
        Me.btnNuevo.Enabled = False
        Me.btnNuevo.Image = Global.SETours.My.Resources.Resources.page
        Me.btnNuevo.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnNuevo.Name = "btnNuevo"
        Me.btnNuevo.Size = New System.Drawing.Size(62, 22)
        Me.btnNuevo.Text = "Nuevo"
        Me.btnNuevo.ToolTipText = "Nuevo (F2)"
        '
        'btnEditar
        '
        Me.btnEditar.Enabled = False
        Me.btnEditar.Image = Global.SETours.My.Resources.Resources.editarpng
        Me.btnEditar.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnEditar.Name = "btnEditar"
        Me.btnEditar.Size = New System.Drawing.Size(57, 22)
        Me.btnEditar.Text = "Editar"
        Me.btnEditar.ToolTipText = "Editar (F3)"
        '
        'btnDeshacer
        '
        Me.btnDeshacer.Enabled = False
        Me.btnDeshacer.Image = Global.SETours.My.Resources.Resources.arrow_undo
        Me.btnDeshacer.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnDeshacer.Name = "btnDeshacer"
        Me.btnDeshacer.Size = New System.Drawing.Size(75, 22)
        Me.btnDeshacer.Text = "Deshacer"
        Me.btnDeshacer.ToolTipText = "Deshacer (ESC)"
        '
        'btnGrabar
        '
        Me.btnGrabar.Enabled = False
        Me.btnGrabar.Image = Global.SETours.My.Resources.Resources.disk
        Me.btnGrabar.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnGrabar.Name = "btnGrabar"
        Me.btnGrabar.Size = New System.Drawing.Size(62, 22)
        Me.btnGrabar.Text = "Grabar"
        Me.btnGrabar.ToolTipText = "Grabar (F6)"
        '
        'btnEliminar
        '
        Me.btnEliminar.Enabled = False
        Me.btnEliminar.Image = Global.SETours.My.Resources.Resources.eliminarpng
        Me.btnEliminar.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnEliminar.Name = "btnEliminar"
        Me.btnEliminar.Size = New System.Drawing.Size(70, 22)
        Me.btnEliminar.Text = "Eliminar"
        Me.btnEliminar.ToolTipText = "Eliminar (F4)"
        '
        'btnImprimir
        '
        Me.btnImprimir.Enabled = False
        Me.btnImprimir.Image = Global.SETours.My.Resources.Resources.printer
        Me.btnImprimir.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnImprimir.Name = "btnImprimir"
        Me.btnImprimir.Size = New System.Drawing.Size(73, 22)
        Me.btnImprimir.Text = "Imprimir"
        '
        'btnSalir
        '
        Me.btnSalir.Image = Global.SETours.My.Resources.Resources.door_out
        Me.btnSalir.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnSalir.Name = "btnSalir"
        Me.btnSalir.Size = New System.Drawing.Size(49, 22)
        Me.btnSalir.Text = "Salir"
        '
        'tbc
        '
        Me.tbc.Controls.Add(Me.tbgDatos)
        Me.tbc.Controls.Add(Me.tbgRangosAPT)
        Me.tbc.Dock = System.Windows.Forms.DockStyle.Fill
        Me.tbc.Location = New System.Drawing.Point(0, 0)
        Me.tbc.Name = "tbc"
        Me.tbc.SelectedIndex = 0
        Me.tbc.Size = New System.Drawing.Size(1039, 602)
        Me.tbc.TabIndex = 321
        '
        'tbgDatos
        '
        Me.tbgDatos.Controls.Add(Me.grb)
        Me.tbgDatos.Location = New System.Drawing.Point(4, 22)
        Me.tbgDatos.Name = "tbgDatos"
        Me.tbgDatos.Padding = New System.Windows.Forms.Padding(3)
        Me.tbgDatos.Size = New System.Drawing.Size(1031, 576)
        Me.tbgDatos.TabIndex = 0
        Me.tbgDatos.Text = "Datos"
        Me.tbgDatos.UseVisualStyleBackColor = True
        '
        'tbgRangosAPT
        '
        Me.tbgRangosAPT.Controls.Add(Me.GroupBox6)
        Me.tbgRangosAPT.Location = New System.Drawing.Point(4, 22)
        Me.tbgRangosAPT.Name = "tbgRangosAPT"
        Me.tbgRangosAPT.Padding = New System.Windows.Forms.Padding(3)
        Me.tbgRangosAPT.Size = New System.Drawing.Size(1031, 576)
        Me.tbgRangosAPT.TabIndex = 1
        Me.tbgRangosAPT.Text = "Rangos APT"
        Me.tbgRangosAPT.UseVisualStyleBackColor = True
        '
        'GroupBox6
        '
        Me.GroupBox6.BackColor = System.Drawing.Color.White
        Me.GroupBox6.Controls.Add(Me.gpbAPT)
        Me.GroupBox6.Controls.Add(Me.lblTitulo2)
        Me.GroupBox6.Dock = System.Windows.Forms.DockStyle.Fill
        Me.GroupBox6.Location = New System.Drawing.Point(3, 3)
        Me.GroupBox6.Name = "GroupBox6"
        Me.GroupBox6.Size = New System.Drawing.Size(1025, 570)
        Me.GroupBox6.TabIndex = 0
        Me.GroupBox6.TabStop = False
        '
        'gpbAPT
        '
        Me.gpbAPT.Controls.Add(Me.chkPolLibAPT)
        Me.gpbAPT.Controls.Add(Me.gpbPolLibAPT)
        Me.gpbAPT.Controls.Add(Me.txtSinpIndi)
        Me.gpbAPT.Controls.Add(Me.Label7)
        Me.gpbAPT.Controls.Add(Me.Label6)
        Me.gpbAPT.Controls.Add(Me.txtDescAlter)
        Me.gpbAPT.Controls.Add(Me.cboTemporada)
        Me.gpbAPT.Controls.Add(Me.Label5)
        Me.gpbAPT.Controls.Add(Me.dgvAPT)
        Me.gpbAPT.Controls.Add(Me.btnEliminarFilaAPT)
        Me.gpbAPT.Controls.Add(Me.chkSelTodosAPT)
        Me.gpbAPT.Location = New System.Drawing.Point(6, 52)
        Me.gpbAPT.Name = "gpbAPT"
        Me.gpbAPT.Size = New System.Drawing.Size(804, 364)
        Me.gpbAPT.TabIndex = 0
        Me.gpbAPT.TabStop = False
        '
        'chkPolLibAPT
        '
        Me.chkPolLibAPT.AutoSize = True
        Me.chkPolLibAPT.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkPolLibAPT.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.chkPolLibAPT.Location = New System.Drawing.Point(20, 225)
        Me.chkPolLibAPT.Name = "chkPolLibAPT"
        Me.chkPolLibAPT.Size = New System.Drawing.Size(136, 17)
        Me.chkPolLibAPT.TabIndex = 6
        Me.chkPolLibAPT.Tag = "Borrar"
        Me.chkPolLibAPT.Text = "Política de Liberado"
        Me.chkPolLibAPT.UseVisualStyleBackColor = True
        '
        'gpbPolLibAPT
        '
        Me.gpbPolLibAPT.Controls.Add(Me.txtMaximoLiberadoAPT)
        Me.gpbPolLibAPT.Controls.Add(Me.Label8)
        Me.gpbPolLibAPT.Controls.Add(Me.txtMontoLAPT)
        Me.gpbPolLibAPT.Controls.Add(Me.Label9)
        Me.gpbPolLibAPT.Controls.Add(Me.Label11)
        Me.gpbPolLibAPT.Controls.Add(Me.cboTipoLibAPT)
        Me.gpbPolLibAPT.Controls.Add(Me.txtLiberadoAPT)
        Me.gpbPolLibAPT.Controls.Add(Me.Label12)
        Me.gpbPolLibAPT.Enabled = False
        Me.gpbPolLibAPT.Location = New System.Drawing.Point(13, 230)
        Me.gpbPolLibAPT.Name = "gpbPolLibAPT"
        Me.gpbPolLibAPT.Size = New System.Drawing.Size(449, 82)
        Me.gpbPolLibAPT.TabIndex = 7
        Me.gpbPolLibAPT.TabStop = False
        '
        'txtMaximoLiberadoAPT
        '
        Me.txtMaximoLiberadoAPT.BackColor = System.Drawing.SystemColors.Window
        Me.txtMaximoLiberadoAPT.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtMaximoLiberadoAPT.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtMaximoLiberadoAPT.Location = New System.Drawing.Point(343, 37)
        Me.txtMaximoLiberadoAPT.MaxLength = 2
        Me.txtMaximoLiberadoAPT.Name = "txtMaximoLiberadoAPT"
        Me.txtMaximoLiberadoAPT.Size = New System.Drawing.Size(44, 21)
        Me.txtMaximoLiberadoAPT.TabIndex = 7
        Me.txtMaximoLiberadoAPT.Tag = "Borrar"
        Me.txtMaximoLiberadoAPT.Text = "0"
        Me.txtMaximoLiberadoAPT.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.Label8.Location = New System.Drawing.Point(340, 16)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(104, 13)
        Me.Label8.TabIndex = 6
        Me.Label8.Text = "Máximo Liberado"
        '
        'txtMontoLAPT
        '
        Me.txtMontoLAPT.BackColor = System.Drawing.SystemColors.Window
        Me.txtMontoLAPT.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtMontoLAPT.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtMontoLAPT.Location = New System.Drawing.Point(217, 37)
        Me.txtMontoLAPT.MaxLength = 12
        Me.txtMontoLAPT.Name = "txtMontoLAPT"
        Me.txtMontoLAPT.Size = New System.Drawing.Size(109, 21)
        Me.txtMontoLAPT.TabIndex = 5
        Me.txtMontoLAPT.Tag = "Borrar"
        Me.txtMontoLAPT.Text = "0.0000"
        Me.txtMontoLAPT.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label9.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.Label9.Location = New System.Drawing.Point(214, 16)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(95, 13)
        Me.Label9.TabIndex = 4
        Me.Label9.Text = "Monto Liberado"
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label11.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.Label11.Location = New System.Drawing.Point(78, 16)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(83, 13)
        Me.Label11.TabIndex = 2
        Me.Label11.Text = "Tipo Liberado"
        '
        'cboTipoLibAPT
        '
        Me.cboTipoLibAPT.BackColor = System.Drawing.SystemColors.Window
        Me.cboTipoLibAPT.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboTipoLibAPT.FormattingEnabled = True
        Me.cboTipoLibAPT.Location = New System.Drawing.Point(79, 37)
        Me.cboTipoLibAPT.Name = "cboTipoLibAPT"
        Me.cboTipoLibAPT.Size = New System.Drawing.Size(121, 21)
        Me.cboTipoLibAPT.TabIndex = 3
        Me.cboTipoLibAPT.Tag = "Borrar"
        '
        'txtLiberadoAPT
        '
        Me.txtLiberadoAPT.BackColor = System.Drawing.SystemColors.Window
        Me.txtLiberadoAPT.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtLiberadoAPT.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtLiberadoAPT.Location = New System.Drawing.Point(18, 37)
        Me.txtLiberadoAPT.MaxLength = 2
        Me.txtLiberadoAPT.Name = "txtLiberadoAPT"
        Me.txtLiberadoAPT.Size = New System.Drawing.Size(44, 21)
        Me.txtLiberadoAPT.TabIndex = 1
        Me.txtLiberadoAPT.Tag = "Borrar"
        Me.txtLiberadoAPT.Text = "0"
        Me.txtLiberadoAPT.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label12.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.Label12.Location = New System.Drawing.Point(15, 16)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(57, 13)
        Me.Label12.TabIndex = 0
        Me.Label12.Text = "Cantidad"
        '
        'txtSinpIndi
        '
        Me.txtSinpIndi.BackColor = System.Drawing.SystemColors.Window
        Me.txtSinpIndi.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtSinpIndi.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtSinpIndi.Location = New System.Drawing.Point(13, 186)
        Me.txtSinpIndi.MaxLength = 12
        Me.txtSinpIndi.Name = "txtSinpIndi"
        Me.txtSinpIndi.Size = New System.Drawing.Size(72, 21)
        Me.txtSinpIndi.TabIndex = 5
        Me.txtSinpIndi.Tag = "Borrar"
        Me.txtSinpIndi.Text = "0.0000"
        Me.txtSinpIndi.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.Label7.Location = New System.Drawing.Point(10, 170)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(137, 13)
        Me.Label7.TabIndex = 4
        Me.Label7.Text = "Suplemento Individual:"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.Label6.Location = New System.Drawing.Point(10, 65)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(120, 13)
        Me.Label6.TabIndex = 2
        Me.Label6.Text = "Descripción Alterna:"
        '
        'txtDescAlter
        '
        Me.txtDescAlter.BackColor = System.Drawing.SystemColors.Window
        Me.txtDescAlter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtDescAlter.Location = New System.Drawing.Point(13, 81)
        Me.txtDescAlter.MaxLength = 200
        Me.txtDescAlter.Multiline = True
        Me.txtDescAlter.Name = "txtDescAlter"
        Me.txtDescAlter.ScrollBars = System.Windows.Forms.ScrollBars.Both
        Me.txtDescAlter.Size = New System.Drawing.Size(352, 77)
        Me.txtDescAlter.TabIndex = 3
        Me.txtDescAlter.Tag = "Borrar"
        '
        'cboTemporada
        '
        Me.cboTemporada.BackColor = System.Drawing.SystemColors.Window
        Me.cboTemporada.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboTemporada.FormattingEnabled = True
        Me.cboTemporada.Location = New System.Drawing.Point(91, 24)
        Me.cboTemporada.Name = "cboTemporada"
        Me.cboTemporada.Size = New System.Drawing.Size(248, 21)
        Me.cboTemporada.TabIndex = 1
        Me.cboTemporada.Tag = "Borrar"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.Label5.Location = New System.Drawing.Point(10, 27)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(75, 13)
        Me.Label5.TabIndex = 0
        Me.Label5.Text = "Temporada:"
        '
        'dgvAPT
        '
        Me.dgvAPT.AllowUserToDeleteRows = False
        Me.dgvAPT.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.dgvAPT.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.DisplayedCells
        Me.dgvAPT.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvAPT.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Correlativo2, Me.PaxDesde2, Me.PaxHasta2, Me.Monto2, Me.Sel2})
        Me.dgvAPT.Location = New System.Drawing.Point(479, 120)
        Me.dgvAPT.Name = "dgvAPT"
        DataGridViewCellStyle2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dgvAPT.RowsDefaultCellStyle = DataGridViewCellStyle2
        Me.dgvAPT.RowTemplate.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvAPT.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect
        Me.dgvAPT.Size = New System.Drawing.Size(292, 192)
        Me.dgvAPT.TabIndex = 10
        Me.dgvAPT.Tag = "CellSelect"
        '
        'Correlativo2
        '
        Me.Correlativo2.DataPropertyName = "Correlativo"
        Me.Correlativo2.HeaderText = "ID"
        Me.Correlativo2.Name = "Correlativo2"
        Me.Correlativo2.ReadOnly = True
        Me.Correlativo2.Visible = False
        Me.Correlativo2.Width = 25
        '
        'PaxDesde2
        '
        Me.PaxDesde2.DataPropertyName = "PaxDesde"
        Me.PaxDesde2.HeaderText = "Desde"
        Me.PaxDesde2.Name = "PaxDesde2"
        Me.PaxDesde2.Width = 50
        '
        'PaxHasta2
        '
        Me.PaxHasta2.DataPropertyName = "PaxHasta"
        Me.PaxHasta2.HeaderText = "Hasta"
        Me.PaxHasta2.Name = "PaxHasta2"
        Me.PaxHasta2.Width = 50
        '
        'Monto2
        '
        Me.Monto2.DataPropertyName = "Monto"
        Me.Monto2.HeaderText = "Monto (US$)"
        Me.Monto2.Name = "Monto2"
        '
        'Sel2
        '
        Me.Sel2.HeaderText = ""
        Me.Sel2.Name = "Sel2"
        Me.Sel2.Width = 20
        '
        'chkSelTodosAPT
        '
        Me.chkSelTodosAPT.AutoSize = True
        Me.chkSelTodosAPT.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkSelTodosAPT.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.chkSelTodosAPT.Location = New System.Drawing.Point(645, 93)
        Me.chkSelTodosAPT.Name = "chkSelTodosAPT"
        Me.chkSelTodosAPT.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.chkSelTodosAPT.Size = New System.Drawing.Size(126, 17)
        Me.chkSelTodosAPT.TabIndex = 9
        Me.chkSelTodosAPT.Tag = "Borrar"
        Me.chkSelTodosAPT.Text = "Seleccionar todos"
        Me.chkSelTodosAPT.UseVisualStyleBackColor = True
        '
        'lblTitulo2
        '
        Me.lblTitulo2.BackColor = System.Drawing.SystemColors.ControlDarkDark
        Me.lblTitulo2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.lblTitulo2.Dock = System.Windows.Forms.DockStyle.Top
        Me.lblTitulo2.Font = New System.Drawing.Font("Tahoma", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTitulo2.ForeColor = System.Drawing.Color.White
        Me.lblTitulo2.Location = New System.Drawing.Point(3, 16)
        Me.lblTitulo2.Name = "lblTitulo2"
        Me.lblTitulo2.Size = New System.Drawing.Size(1019, 22)
        Me.lblTitulo2.TabIndex = 0
        Me.lblTitulo2.Text = "Nuevo"
        '
        'frmMantDetServiciosProveedorDato
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1039, 602)
        Me.Controls.Add(Me.tbc)
        Me.Controls.Add(Me.Toolbar)
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmMantDetServiciosProveedorDato"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Tag = "Detalle Servicio"
        Me.Text = "Ingreso de Detalle de Servicio de Proveedor"
        CType(Me.ErrPrv, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgv, System.ComponentModel.ISupportInitialize).EndInit()
        Me.grb.ResumeLayout(False)
        Me.grb.PerformLayout()
        Me.Toolbar.ResumeLayout(False)
        Me.Toolbar.PerformLayout()
        Me.tbc.ResumeLayout(False)
        Me.tbgDatos.ResumeLayout(False)
        Me.tbgRangosAPT.ResumeLayout(False)
        Me.GroupBox6.ResumeLayout(False)
        Me.gpbAPT.ResumeLayout(False)
        Me.gpbAPT.PerformLayout()
        Me.gpbPolLibAPT.ResumeLayout(False)
        Me.gpbPolLibAPT.PerformLayout()
        CType(Me.dgvAPT, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents ErrPrv As System.Windows.Forms.ErrorProvider
    Friend WithEvents ToolTip1 As System.Windows.Forms.ToolTip
    Friend WithEvents grb As System.Windows.Forms.GroupBox
    Friend WithEvents GroupBox5 As System.Windows.Forms.GroupBox
    Friend WithEvents lblIDServicio_Det_V As System.Windows.Forms.Label
    Friend WithEvents btnBuscarDetServicio As System.Windows.Forms.Button
    Friend WithEvents btnCalculo As System.Windows.Forms.Button
    Friend WithEvents chkRangos As System.Windows.Forms.CheckBox
    Friend WithEvents btnEliminarFila As System.Windows.Forms.Button
    Friend WithEvents chkSelTodos As System.Windows.Forms.CheckBox
    Friend WithEvents dgv As System.Windows.Forms.DataGridView
    Friend WithEvents txtMaximoLiberado As System.Windows.Forms.TextBox
    Friend WithEvents lblMaximoLiberado As System.Windows.Forms.Label
    Friend WithEvents lblCorrelativoEtiq As System.Windows.Forms.Label
    Friend WithEvents lblCorrelativo As System.Windows.Forms.Label
    Friend WithEvents lblTipoGastoEtiq As System.Windows.Forms.Label
    Friend WithEvents cboTipoGasto As System.Windows.Forms.ComboBox
    Friend WithEvents GroupBox4 As System.Windows.Forms.GroupBox
    Friend WithEvents GroupBox3 As System.Windows.Forms.GroupBox
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents txtTC As System.Windows.Forms.TextBox
    Friend WithEvents lblTCEtiq As System.Windows.Forms.Label
    Friend WithEvents lblCodxTripleEtiq As System.Windows.Forms.Label
    Friend WithEvents cboCodxTriple As System.Windows.Forms.ComboBox
    Friend WithEvents lblCostosEtiq As System.Windows.Forms.Label
    Friend WithEvents txtMonto_tri As System.Windows.Forms.TextBox
    Friend WithEvents lblMontoTripleEtiq2 As System.Windows.Forms.Label
    Friend WithEvents txtMonto_dbl As System.Windows.Forms.TextBox
    Friend WithEvents lblMontoDobleEtiq2 As System.Windows.Forms.Label
    Friend WithEvents txtMonto_sgl As System.Windows.Forms.TextBox
    Friend WithEvents lblMontoSimpleEtiq2 As System.Windows.Forms.Label
    Friend WithEvents lvw As System.Windows.Forms.ListView
    Friend WithEvents txtCtaContableC As System.Windows.Forms.TextBox
    Friend WithEvents Label16 As System.Windows.Forms.Label
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents cboIdTipoOC As System.Windows.Forms.ComboBox
    Friend WithEvents lblTituloGrupoEtiq As System.Windows.Forms.Label
    Friend WithEvents txtTituloGrupo As System.Windows.Forms.TextBox
    Friend WithEvents chkTarifario As System.Windows.Forms.CheckBox
    Friend WithEvents txtMontoL As System.Windows.Forms.TextBox
    Friend WithEvents lblMontoLEtiq As System.Windows.Forms.Label
    Friend WithEvents lblTipoLibEtiq As System.Windows.Forms.Label
    Friend WithEvents cboTipoLib As System.Windows.Forms.ComboBox
    Friend WithEvents txtLiberado As System.Windows.Forms.TextBox
    Friend WithEvents lblLiberadoEtiq As System.Windows.Forms.Label
    Friend WithEvents lblTipoDesayunoEtiq As System.Windows.Forms.Label
    Friend WithEvents cboTipoDesayuno As System.Windows.Forms.ComboBox
    Friend WithEvents chkPoliticaLiberado As System.Windows.Forms.CheckBox
    Friend WithEvents txtMonto_tris As System.Windows.Forms.TextBox
    Friend WithEvents lblMontoTripleEtiq As System.Windows.Forms.Label
    Friend WithEvents txtMonto_dbls As System.Windows.Forms.TextBox
    Friend WithEvents lblMontoDobleEtiq As System.Windows.Forms.Label
    Friend WithEvents txtMonto_sgls As System.Windows.Forms.TextBox
    Friend WithEvents lblMontoSimpleEtiq As System.Windows.Forms.Label
    Friend WithEvents chkAfecto As System.Windows.Forms.CheckBox
    Friend WithEvents txtDescripcion As System.Windows.Forms.TextBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents txtDetaTipo As System.Windows.Forms.TextBox
    Friend WithEvents txtTipo As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents txtAnio As System.Windows.Forms.TextBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents chkConAlojamiento As System.Windows.Forms.CheckBox
    Friend WithEvents txtDiferSS As System.Windows.Forms.TextBox
    Friend WithEvents lblDiferSSEtiq As System.Windows.Forms.Label
    Friend WithEvents txtDiferST As System.Windows.Forms.TextBox
    Friend WithEvents lblDiferSTEtiq As System.Windows.Forms.Label
    Friend WithEvents chkPlanAlimenticio As System.Windows.Forms.CheckBox
    Friend WithEvents chkLonche As System.Windows.Forms.CheckBox
    Friend WithEvents lblIncluyeEtiq As System.Windows.Forms.Label
    Friend WithEvents chkCena As System.Windows.Forms.CheckBox
    Friend WithEvents chkAlmuerzo As System.Windows.Forms.CheckBox
    Friend WithEvents chkDesayuno As System.Windows.Forms.CheckBox
    Friend WithEvents chkVerDetaTipo As System.Windows.Forms.CheckBox
    Friend WithEvents cboHabitTriple As System.Windows.Forms.ComboBox
    Friend WithEvents lblDefinicionTripleEtiq As System.Windows.Forms.Label
    Friend WithEvents cboCuentaContable As System.Windows.Forms.ComboBox
    Private WithEvents lblTitulo As System.Windows.Forms.Label
    Friend WithEvents Toolbar As System.Windows.Forms.ToolStrip
    Friend WithEvents btnNuevo As System.Windows.Forms.ToolStripButton
    Friend WithEvents btnEditar As System.Windows.Forms.ToolStripButton
    Friend WithEvents btnDeshacer As System.Windows.Forms.ToolStripButton
    Friend WithEvents btnGrabar As System.Windows.Forms.ToolStripButton
    Friend WithEvents btnEliminar As System.Windows.Forms.ToolStripButton
    Friend WithEvents btnImprimir As System.Windows.Forms.ToolStripButton
    Friend WithEvents btnSalir As System.Windows.Forms.ToolStripButton
    Friend WithEvents txtCodTarifa As System.Windows.Forms.TextBox
    Friend WithEvents chkPoliticaLibNivelDetalle As System.Windows.Forms.CheckBox
    Friend WithEvents Correlativo As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents PaxDesde As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents PaxHasta As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Monto As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Sel As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents tbc As System.Windows.Forms.TabControl
    Friend WithEvents tbgDatos As System.Windows.Forms.TabPage
    Friend WithEvents tbgRangosAPT As System.Windows.Forms.TabPage
    Friend WithEvents btnEliminarFilaAPT As System.Windows.Forms.Button
    Friend WithEvents chkSelTodosAPT As System.Windows.Forms.CheckBox
    Friend WithEvents dgvAPT As System.Windows.Forms.DataGridView
    Friend WithEvents Correlativo2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents PaxDesde2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents PaxHasta2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Monto2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Sel2 As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents GroupBox6 As System.Windows.Forms.GroupBox
    Private WithEvents lblTitulo2 As System.Windows.Forms.Label
    Friend WithEvents cboCentroCostos As System.Windows.Forms.ComboBox
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents chkDetraccion As System.Windows.Forms.CheckBox
    Friend WithEvents gpbAPT As System.Windows.Forms.GroupBox
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents txtDescAlter As System.Windows.Forms.TextBox
    Friend WithEvents cboTemporada As System.Windows.Forms.ComboBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents txtSinpIndi As System.Windows.Forms.TextBox
    Friend WithEvents chkPolLibAPT As System.Windows.Forms.CheckBox
    Friend WithEvents gpbPolLibAPT As System.Windows.Forms.GroupBox
    Friend WithEvents txtMaximoLiberadoAPT As System.Windows.Forms.TextBox
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents txtMontoLAPT As System.Windows.Forms.TextBox
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents cboTipoLibAPT As System.Windows.Forms.ComboBox
    Friend WithEvents txtLiberadoAPT As System.Windows.Forms.TextBox
    Friend WithEvents Label12 As System.Windows.Forms.Label
End Class
