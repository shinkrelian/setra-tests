﻿Public Class frmMenuTesting

    Private Sub mnuCotizacion_Click(sender As Object, e As EventArgs) Handles mnuCotizacion.Click
        Dim frm As New frmIngCotizacion
        frm.MdiParent = Me
        frm.Show()
    End Sub

    Private Sub mnuPedidos_Click(sender As Object, e As EventArgs) Handles mnuPedidos.Click
        Dim frm As New frmIngPedido
        frm.MdiParent = Me
        frm.Show()
    End Sub

    Private Sub mnuDocumentos_Click(sender As Object, e As EventArgs) Handles mnuDocumentos.Click
        Dim frm As New frmDocumentosProveedorDato
        frm.MdiParent = Me
        frm.Show()
    End Sub

    Private Sub mnuSalir_Click(sender As Object, e As EventArgs) Handles mnuSalir.Click
        Me.Close()
    End Sub
End Class